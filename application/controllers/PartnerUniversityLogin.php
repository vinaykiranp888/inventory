<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class PartnerUniversityLogin extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
            // echo "Not Login";exit;
        $this->checkPartnerUniversityLoggedIn();
    }
    

    function checkPartnerUniversityLoggedIn()
    {
        $isPartnerUniversityLoggedIn = $this->session->userdata('isPartnerUniversityLoggedIn');
        
        if(!isset($isPartnerUniversityLoggedIn) || $isPartnerUniversityLoggedIn != TRUE)
        {
            // echo "Not Login";exit;
            $this->load->view('partner_university_login');
        }
        else
        {
            // echo "Login";exit;
            
            redirect('/partner_university/student/welcome');
            // redirect('partner_university/scheme/list');
        }
    }


    public function partnerUniLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        // $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[32]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->partner_university_login_model->loginPartnerUniversity($email, $password);
            
            // echo "<Pre>";print_r($result); exit;
            
            if(!empty($result))
            {
                
                    
                $lastLogin = $this->partner_university_login_model->partnerUniversityLastLoginInfo($result->id);

                if($lastLogin == '')
                {
                    $partner_university_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $partner_university_login = $lastLogin->created_dt_tm;
                }


                $image = $result->image;
                    
                if($image == '')
                {
                    $image = 'user_profile.jpg  ';
                }


                $sessionArray = array('id_partner_university'=>$result->id,
                                        'partner_university_name'=>$result->name,
                                        'partner_university_code'=>$result->code,
                                        'partner_university_login_id'=>$result->login_id,
                                        'partner_university_image'=>$image,
                                        'partner_university_last_login'=> $partner_university_login,
                                        'isPartnerUniversityLoggedIn' => TRUE
                                );
                
                // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_partner_university'], $sessionArray['isPartnerUniversityLoggedIn'], $sessionArray['partner_university_last_login']);

                $loginInfo = array("id_partner_university"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_partner_university_session_id", md5($uniqueId));


                $this->partner_university_login_model->addPartnerUniversityLastLogin($loginInfo);

                // echo "<Pre>"; print_r($this->session->userdata());exit();
                // echo "Login";exit();
                redirect('/partner_university/student/welcome');
                }
            else
            {
                $this->session->set_flashdata('error', 'Login ID or Password Mismatch');
                
                $this->index();
            }
        }
    }



    public function registration()
    {

       if($this->input->post())
        {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $billing_to = $this->security->xss_clean($this->input->post('billing_to'));
                $login_id = $this->security->xss_clean($this->input->post('login_id'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $pay_as_agent = $this->security->xss_clean($this->input->post('pay_as_agent'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $account_number = $this->security->xss_clean($this->input->post('account_number'));
                $swift_code = $this->security->xss_clean($this->input->post('swift_code'));
                $bank_address = $this->security->xss_clean($this->input->post('bank_address'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'id_partner_category' => $id_partner_category,
                    'id_partner_university' => $id_partner_university,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    // 'certificate' => $certificate,
                    'email' => $email,
                    'billing_to' => $billing_to,
                    'login_id' => $login_id,
                    'password' => md5($password),
                    'pay_as_agent' => $pay_as_agent,
                    'id_bank' => $id_bank,
                    'account_number' => $account_number,
                    'swift_code' => $swift_code,
                    'bank_address' => $bank_address,
                    'status' => $status,
                    'created_by' => 0
                );

                $result = $this->partner_university_login_model->addNewPartnerUniversity($data);
                $this->session->set_flashdata('success', 'Partner Registered Successfully');
                redirect('/partnerUniversityLogin');
        }

        // $data['salutationList'] = $this->partner_university_login_model->getSalutationByStatus('1');

        $this->global['pageTitle'] = 'Partner University Portal : Partner Registration';
        $this->loadViews("partner_university_registration", $this->global, NULL, NULL);
    }


    function checkUserExist()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo '<Pre>';print_r($tempData);exit;

        $status = '';

        $result = $this->partner_university_login_model->checkUserExist($tempData);

        if($result)
        {
            $status = $result->status;
        }

        echo $status;exit;
    }

    function getPartnerUniversityDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->partner_university_login_model->getPartnerUniversityDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }       
    }
}

?>