<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class CompanyUserLogin extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('company_user_model');
    }

    public function index()
    {
        // echo "<Pre>";print_r("dasdsa");exit();
        $this->checkCompanyUserLoggedIn();
    }
    

    function checkCompanyUserLoggedIn()
    {
        $isCompanyUserLoggedIn = $this->session->userdata('isCompanyUserLoggedIn');
        
        if(!isset($isCompanyUserLoggedIn) || $isCompanyUserLoggedIn != TRUE)
        {
            $this->companyUserLogin();
        }
        else
        {
            redirect('company_user/profile');
        }
    }

    public function companyUserLogin()
    {
        $formData = $this->input->post();

        if($formData)
        {

            // echo "<Pre>"; print_r($formData);exit;
            $domain = $this->getDomainName();

            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            {
                
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                
                $result = $this->company_user_model->loginCompanyUser($email, $password);
                
                // echo "<Pre>"; print_r($result);exit;

                if(!empty($result))
                {
                    
                    $lastLogin = $this->company_user_model->companyUserLastLoginInfo($result->id);

                    // echo "<Pre>"; print_r($lastLogin);exit;

                    if($lastLogin == '')
                    {
                        $company_user_login = date('Y-m-d h:i:s');
                    }
                    else
                    {
                        $company_user_login = $lastLogin->created_dt_tm;
                    }

                    $image = $result->image;
                    
                    if($image == '')
                    {
                        $image = 'user_profile.jpg  ';
                    }
                    

                    $sessionArray = array(
                        'id_company_user'=>$result->id,
                        'id_company'=>$result->id_company,
                        'company_user_full_name'=>$result->name,
                        'company_user_name'=>$result->user_name,
                        'company_user_email'=>$result->email,
                        'company_user_phone'=>$result->phone,
                        'company_user_designation'=>$result->designation,
                        'company_user_role'=>$result->user_role,
                        'company_image'=>$image,
                        'id_company_user_role'=>$result->id_user_role,
                        'company_user_last_login'=> $company_user_login,
                        'isCompanyUserLoggedIn' => TRUE
                    );

                    // echo "<Pre>";print_r($sessionArray);exit();

                    $this->session->set_userdata($sessionArray);

                    unset($sessionArray['id_company_user'], $sessionArray['isCompanyUserLoggedIn'], $sessionArray['company_user_last_login']);

                    $loginInfo = array("id_company_user"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                    $uniqueId = rand(0000000000,9999999999);
                    $this->session->set_userdata("company_user_session_id", md5($uniqueId));


                    $this->company_user_model->addCompanyUserLastLogin($loginInfo);

                    // echo "<Pre>"; print_r($this->session->userdata());exit();
                    $entered_corporate_user_url = $this->session->userdata('entered_corporate_user_url');

                    if($entered_corporate_user_url)
                    {
                        $urlarray = explode ('/',$entered_corporate_user_url);

                        $urlmodule = $urlarray['1'];
                        $urlcontroller = $urlarray['2'];
                        $urlview = '';

                        if($urlmodule == 'company_user' && $urlcontroller == 'profile')
                        {
                            $urlview = $urlarray['3'];
                        }
                        // echo "<Pre>";print_r($urlview); exit();

                        if($urlview != 'logout')
                        {
                            redirect($entered_corporate_user_url);
                        }
                    }
                    else
                    {
                        redirect('/company_user/profile');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'Email or password mismatch');
                }
            }
        }

        $this->load->view('company_user_login');
    }


    public function registration()
    {
       if($this->input->post())
        {

            // echo "<Pre>"; print_r($this->input->post());exit;


            $level = $this->security->xss_clean($this->input->post('level'));
            $id_parent_company = $this->security->xss_clean($this->input->post('id_parent_company'));
            $registration_number = $this->security->xss_clean($this->input->post('registration_number'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $name = $this->security->xss_clean($this->input->post('name'));
            $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
            $website = $this->security->xss_clean($this->input->post('website'));
            $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
            $chairman = $this->security->xss_clean($this->input->post('chairman'));
            $ceo = $this->security->xss_clean($this->input->post('ceo'));
            $address1 = $this->security->xss_clean($this->input->post('address1'));
            $address2 = $this->security->xss_clean($this->input->post('address2'));
            $id_country = $this->security->xss_clean($this->input->post('id_country'));
            $id_state = $this->security->xss_clean($this->input->post('id_state'));
            $city = $this->security->xss_clean($this->input->post('city'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $country_code = $this->security->xss_clean($this->input->post('country_code'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $corporate_membership_status = $this->security->xss_clean($this->input->post('corporate_membership_status'));
            $staff_strength = $this->security->xss_clean($this->input->post('staff_strength'));
            $debtor_code = $this->security->xss_clean($this->input->post('debtor_code'));
            $credit_term = $this->security->xss_clean($this->input->post('credit_term'));
            $control_account = $this->security->xss_clean($this->input->post('control_account'));
            $credit_term_code = $this->security->xss_clean($this->input->post('credit_term_code'));
            $primary_contact_name = $this->security->xss_clean($this->input->post('primary_contact_name'));
            $primary_contact_designation = $this->security->xss_clean($this->input->post('primary_contact_designation'));
            $primary_contact_email = $this->security->xss_clean($this->input->post('primary_contact_email'));
            $primary_country_code = $this->security->xss_clean($this->input->post('primary_country_code'));
            $primary_phone = $this->security->xss_clean($this->input->post('primary_phone'));
            $second_contact_name = $this->security->xss_clean($this->input->post('second_contact_name'));
            $second_contact_designation = $this->security->xss_clean($this->input->post('second_contact_designation'));
            $second_contact_email = $this->security->xss_clean($this->input->post('second_contact_email'));
            $second_country_code = $this->security->xss_clean($this->input->post('second_country_code'));
            $second_phone = $this->security->xss_clean($this->input->post('second_phone'));
            $staff_credit = $this->security->xss_clean($this->input->post('staff_credit'));
            $status = $this->security->xss_clean($this->input->post('status'));



        
            $data = array(
                'level' => 'Parent',
                'id_parent_company' => $id_parent_company,
                'registration_number' => $registration_number,
                'name' => $name,
                'password' => md5($password),
                'joined_date' => date('Y-m-d'),
                'website' => $website,
                'institution_type' => $institution_type,
                'chairman' => $chairman,
                'ceo' => $ceo,
                'address1' => $address1,
                'address2' => $address2,
                'id_country' => $id_country,
                'id_state' => $id_state,
                'city' => $city,
                'zipcode' => $zipcode,
                'country_code' => $country_code,
                'phone' => $phone,
                'corporate_membership_status' => $corporate_membership_status,
                'staff_strength' => $staff_strength,
                'debtor_code' => $debtor_code,
                'credit_term' => $credit_term,
                'control_account' => $control_account,
                'credit_term_code' => $credit_term_code,
                'primary_contact_name' => $primary_contact_name,
                'primary_contact_designation' => $primary_contact_designation,
                'primary_contact_email' => $primary_contact_email,
                'primary_country_code' => $primary_country_code,
                'primary_phone' => $primary_phone,
                'second_contact_name' => $second_contact_name,
                'second_contact_designation' => $second_contact_designation,
                'second_contact_email' => $second_contact_email,
                'second_country_code' => $second_country_code,
                'second_phone' => $second_phone,
                'staff_credit' => $staff_credit,
                'status' => 0
            );

            // echo "<Pre>"; print_r($image_file);exit;

            // if($image_file)
            // {
            //     $data['image'] = $image_file;
            // }

            $id_company = $this->company_user_model->addNewCompany($data);

            if($id_company)
            {
                $name = $this->security->xss_clean($this->input->post('corporate_user_name'));
                $phone = $this->security->xss_clean($this->input->post('user_number'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $designation = $this->security->xss_clean($this->input->post('user_designation'));
                $user_name = $this->security->xss_clean($this->input->post('user_name'));
                $id_user_role = $this->security->xss_clean($this->input->post('id_user_role'));
                $status = $this->security->xss_clean($this->input->post('active_status'));
                $password = $this->security->xss_clean($this->input->post('user_password'));

            
                $user_data = array(
                    'id_company' => $id_company,
                    'name' => $name,
                    'phone' => $phone,
                    'designation' => $designation,
                    'user_name' => $user_name,
                    'id_user_role' => $id_user_role,
                    'email' => $email,
                    'password' => md5($password),
                    'status' => 1
                );

                // echo "<Pre>"; print_r($user_data);exit;

                $id_company_user = $this->company_user_model->addNewCompanyUser($user_data);

            }

            $this->session->set_flashdata('success', 'Company Registered Successfully');
            redirect('/companyUserLogin');
        }

        $data['parentCompanyList'] = $this->company_user_model->parentCompanyList('Parent');
        $data['companyUserRoleList'] = $this->company_user_model->companyUserRoleListByStatus('1');


        $this->global['pageTitle'] = 'Partner University Portal : Partner Registration';
        $this->loadViews("company_user_registration", $this->global, $data, NULL);
    }

    function forgotPassword()
    {

       if($this->input->post())
        {

            // echo "<Pre>"; print_r($this->input->post());exit;

            $email = $this->security->xss_clean($this->input->post('email'));
            
            $uniqueId = rand(0000000000,9999999999);

            $uniq_id = md5($uniqueId);
        
            $user_data = array(
                'email' => $email,
                'type' => 'CompanyUser',
                'activation_id' => $uniq_id,
                'agent' => getBrowserAgent(),
                'agent_string' => $this->agent->agent_string(),
                'platform' => $this->agent->platform(),
                'client_ip' => $_SERVER['REMOTE_ADDR'],
                'status' => 0
            );

            // echo "<Pre>"; print_r($user_data);exit;


            $id_company_user = $this->company_user_model->addNewResetPassword($user_data);

            $this->session->set_flashdata('success', 'Company User Password Reset Email Sent Successfully');
            redirect('/companyUserLogin');
        }


        $this->global['pageTitle'] = 'Corporate User Portal : Forgot Password';
        $this->loadViews("company_user_forgot_password", $this->global, NULL, NULL);
    }

    function resetPassword($uniq_id)
    {
        if($uniq_id == NULL)
        {
            $this->session->set_flashdata('error', 'Company User Reset Password Not Authorised');
            redirect('/companyUserLogin');
        }
        else
        {

            $reset_password = $this->company_user_model->getForgorPasswordByActivationId($uniq_id);

            if($reset_password)
            {
                $status = $reset_password->status;
                $email = $reset_password->email;

                if($status == '0')
                {
                    // echo "<Pre>"; print_r($reset_password);exit;

                    if($this->input->post())
                    {
                        // echo "<Pre>"; print_r($this->input->post());exit;

                        $password = $this->security->xss_clean($this->input->post('password'));
                        

                        $password = md5($password);
                    
                        $user_data = array(
                            'password' => $password,
                            'status' => 1
                        );

                        // echo "<Pre>"; print_r($user_data);exit;

                        $update_data['email'] = $email;
                        $update_data['type'] = 'CompanyUser';


                        $id_company_user = $this->company_user_model->updateResetPassword($user_data,$update_data);

                        if($id_company_user)
                        {
                            $update_company_user_data['password'] = $password;
                            $id_company_user = $this->company_user_model->updateCompanyUser($update_company_user_data,$email);
                            $this->session->set_flashdata('success', 'Company User Password Reset Successfully');
                            redirect('/companyUserLogin');
                        }

                    }

                    $data['email'] = $email;


                    $this->global['pageTitle'] = 'Corporate User Portal : Reset Password';
                    $this->loadViews("company_user_reset_password", $this->global, $data, NULL);

                }
                else
                {
                    $this->session->set_flashdata('error', 'Already Password Reseted Succesfully For The Given Link, Unauthorised Access');
                    redirect('/companyUserLogin');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Password Reset Link Unauthorised');
                redirect('/companyUserLogin');
            }
        }
    }

    function getCompanyEmailIdDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id = $tempData['id_employee'];
        // $email_id = $tempData['email_id'];

        $result = $this->company_user_model->getCompanyEmailIdDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }       
    }

    function getRegistrationNumberDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id = $tempData['id_employee'];
        // $email_id = $tempData['email_id'];

        $result = $this->company_user_model->getCompanyEmailIdDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }
    
    function getCompanyUserEmailIdDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id = $tempData['id_employee'];
        // $email_id = $tempData['email_id'];

        $result = $this->company_user_model->getCompanyUserEmailIdDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }


    function checkUserExist()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo '<Pre>';print_r($tempData);exit;

        // $company_user = $this->company_user_model->checkCompanyUserExist($tempData);

        // if($company_user)
        // {
        //     $status = $company_user->status;

        //     if($status == '0')
        //     {
        //         echo '2';exit;
        //     }
        // }

        $status = '';

        $result = $this->company_user_model->checkUserExist($tempData);

        if($result)
        {
            $status = $result->status;
        }

        echo $status;exit;
    }
}

?>