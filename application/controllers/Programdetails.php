<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Programdetails extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
                $this->load->model('register_model');
                $this->load->model('course_model');

    }

    /**
     * Index Page for this controller.
     */
   public function index($id)
    {
        $data['programmeList'] = $this->register_model->getProgramById($id);
        $data['programmeAimList'] = $this->register_model->programmeAimDetailsById($id);
        $data['programmeStructureList'] = $this->register_model->programmeStructureById($id);
        // $data['programmeCertificateList'] = $this->register_model->programmeCertificateById($id);
        $data['programmemodulesList'] = $this->register_model->programmeHasModules($id);
   $data['staffDetails'] = $this->register_model->getStaffDetailsForProgramId($id);

        // print_r($data['programmemodulesList']);exit;
        $this->loadViews('programdetails/index',$this->global,$data,NULL);
    }



      public function tempbuynow($id,$amount) {
        $data['id_programme'] = $id;//print_R($_SESSION);
        $data['amount'] = $amount;
        //fee_structure_main
        $data['id_session'] = session_id();
        //check if the id already exist 
        $idpresent = $this->register_model->checkid($data);
        if($idpresent) {
        } else {
        $this->register_model->addtotemp($data);
        }
    }


    public function deletecourses($id) {
                $id_session = session_id();

        $idpresent = $this->register_model->deleteFromTemp($id);

        $dataCoursesList = $this->register_model->gerProgrammeFromSession($id_session);
        if(count($dataCoursesList)>0) {
            echo 1;
        } else {
            echo 0;
        }

    }





    public function fb(){

        $fb = new Facebook\Facebook([
 'app_id' => 'xxxxxxxxxx',
 'app_secret' => 'xxxxxxxxxx',
 'default_graph_version' => 'v2.2',
]);


    }

    
}