<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
                $this->load->model('register_model');
        $this->load->model('login_model');

    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {

        if($this->input->post())
        {
            $full_name = strtolower($this->security->xss_clean($this->input->post('full_name')));
            $user_email = strtolower($this->security->xss_clean($this->input->post('user_email')));
            $nric = strtolower($this->security->xss_clean($this->input->post('nric')));
            $rawpassword = $this->security->xss_clean($this->input->post('confirm_password'));

            $passworddec = md5($rawpassword);



            $student = array(
                'full_name' =>$full_name,
                'email_id' =>$user_email,
                'nric' =>$nric,
                'password' =>$passworddec,
            );
            $this->login_model->insertStudent($student);
            $result = $this->login_model->loginMe($user_email, $rawpassword);

            $functionName = 'core_user_create_users';
                $rand = rand(00000000,999999999);

                $user1 = new stdClass();
                $user1->username = $nric;
                $user1->password = 'Abc12345678*';
                $user1->firstname = $full_name;
                $user1->lastname = $full_name;
                $user1->email = $user_email;


                $users = array($user1);
                $params = array('users' => $users);

                /// REST CALL
                $restformat = "json";
                $serverurl = 'https://degreebybits.aeu.edu.my/webservice/rest/server.php?wstoken='.TOKEN.'&wsfunction=' . $functionName. '&moodlewsrestformat=' . $restformat;
                require_once ('curl.php');
                $curl = new curl();


                $resp = $curl->post($serverurl, $params);


                ///echo '</br>************************** Server Response    createUser()**************************</br></br>';
                ///echo $serverurl . '</br></br>';

                $responseArray = json_decode($resp);
                ///print_R($responseArray);exit;
                //print_R($responseArray[0]->id);
                $studentdata = array();
                $studentdata['moodle_id'] = $responseArray[0]->id;
                $this->login_model->editStudent($studentdata, $result->id);
                if($result)
            {
                $sessionArray = array(
                    'id_student'=>$result->id,
                    'student_name'=>$result->full_name,
                    'student_first_name'=>$result->first_name,
                    'student_last_name'=>$result->last_name,
                    'student_email'=>$result->email_id,
                    'studentLoggedIn' => TRUE
                    );

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_student'], $sessionArray['isStudentLoggedIn'], $sessionArray['student_last_login']);

                $loginInfo = array("id_student"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());


                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("student_session_id", md5($uniqueId));

                $this->login_model->addStudentLastLogin($loginInfo);

                redirect('/profile/dashboard/index');

           }
            


                
        }
                $data['name'] = 'asdf';

                $this->loadViews('register/index',$this->global,$data,NULL);

    }


    public function duplicateemail($email = null) {


        $student_list_data = $this->register_model->duplicateCheck($email);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }


    
}