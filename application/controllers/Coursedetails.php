<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Coursedetails extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
                $this->load->model('register_model');
                $this->load->model('course_model');

    }

    /**
     * Index Page for this controller.
     */
   public function index($id)
    {

 $data['programmeAwardList'] = $this->register_model->programmeAwardList($id);
 $data['programmeList'] = $this->register_model->getProgramById($id);
 $data['topicList'] = $this->register_model->getTopicByProgramId($id);
 $data['assessmentList'] = $this->register_model->getAssessmentByProgramId($id);
 $data['overview'] = $this->register_model->getProgramOverview($id);
  $data['syllabusList'] = $this->register_model->getProgramSyllabus($id);

   $data['referencesList'] = $this->register_model->getProgramReferences($id);

   $data['staffDetails'] = $this->register_model->getStaffDetailsForProgramId($id);

 // $data['accredationList'] = $this->register_model->getAccredationByProgramId($id);
 // $data['facilitatorList'] = $this->register_model->getFacilitatorByProgramId($id);
 // print_R($data['programmeList']);exit;
                $this->loadViews('coursedetails/index',$this->global,$data,NULL);

        
    }



      public function tempbuynow($id,$amount) {
        $data['id_programme'] = $id;//print_R($_SESSION);
        $data['amount'] = $amount;
        //fee_structure_main
        $data['id_session'] = session_id();
        //check if the id already exist 
        $idpresent = $this->register_model->checkid($data);
        if($idpresent) {
        } else {
        $this->register_model->addtotemp($data);
        }
    }


    public function deletecourses($id) {
                $id_session = session_id();

        $idpresent = $this->register_model->deleteFromTemp($id);

        $dataCoursesList = $this->register_model->gerProgrammeFromSession($id_session);
        if(count($dataCoursesList)>0) {
            echo 1;
        } else {
            echo 0;
        }

    }





    public function fb(){

        $fb = new Facebook\Facebook([
 'app_id' => 'xxxxxxxxxx',
 'app_secret' => 'xxxxxxxxxx',
 'default_graph_version' => 'v2.2',
]);


    }

    
}