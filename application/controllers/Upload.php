<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
   
   class Upload extends BaseController
   {
	
      public function __construct() { 
         parent::__construct(); 
         $this->load->helper(array('form', 'url')); 
         // $autoload['libraries'] = array('database', 'session');

      }
		
      public function index()
      { 
         $this->load->view('upload_form'); 
      } 
		
      public function do_upload()
      { 
         $config['upload_path']   = './uploads/'; 
         $config['allowed_types'] = 'gif|jpg|png'; 
         $config['max_size']      = 100; 
         $config['max_width']     = 1024; 
         $config['max_height']    = 768;  
         $this->load->library('upload', $config);

         $extensions= array("jpeg","jpg","png");
      
         if(isset($_FILES['image']))
         {
            $errors= array();
            $file_name = $_FILES['image']['name'];
            $file_size =$_FILES['image']['size'];
            $file_tmp =$_FILES['image']['tmp_name'];
            $file_type=$_FILES['image']['type'];
            $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
            
            $extensions= array("jpeg","jpg","png");
            
            if(in_array($file_ext,$extensions)=== false)
            {
               $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }
      
            if($file_size > 2097152)
            {
               $errors[]='File size must be excately 2 MB';
            }
            
            if(empty($errors)==true)
            {
               move_uploaded_file($file_tmp,"images/".$file_name);
               echo "Success";
            }
            else
            {
               print_r($errors);
            }
         }
      }  
   }