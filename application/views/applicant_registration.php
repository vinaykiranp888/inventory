<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Applicant Portal | Applicant Registration</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css">
</head>

<body>
  <div class="login-container">
    <h3 class="login-title">Applicant Registration</h3>
    <h2>
      <?php
      $this->load->helper('form');
      $error = $this->session->flashdata('error');
      if ($error) {
      ?>
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?php echo $error; ?>
        </div>
      <?php }
      $success = $this->session->flashdata('success');
      $entered_url = $this->session->flashdata('entered_url');
      // print_r($success);exit();

      if ($success)
      {
      ?>
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo $success; ?>
          </div>
      <?php
      }
      ?>
    </h2>
    <form action="<?php echo base_url(); ?>applicantLogin/applicantRegistration" method="post">

            <div class="form-group">
                <label>Salutation <span class='error-text'>*</span></label>
                <select name="salutation" id="salutation" class="form-control" required>
                    <option value="">Select</option>
                    <option value="Mr">Mr</option>
                    <option value="Miss">Miss</option>
                    <option value="Mrs">Mrs</option>
                </select>
            </div>


            <div class="form-group">
                <label>First Name <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="first_name" name="first_name" required>
            </div>



            <div class="form-group">
                <label>Last Name <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="last_name" name="last_name" required>
            </div>


            <div class="form-group">
                <label>Phone Number <span class='error-text'>*</span></label>
                <input type="number" class="form-control" id="phone" name="phone" minlength="10" maxlength="10" required>
            </div>

            <div class="form-group">
                <label>NRIC <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="nric" name="nric" required>
            </div>


            <div class="form-group">
                <label>Email <span class='error-text'>*</span></label>
                <input type="email" class="form-control" id="email_id" name="email_id" required>
            </div>


            <div class="form-group">
                <label>Password <span class='error-text'>*</span></label>
                <input type="password" class="form-control" id="password" name="password" required>
            </div>

            <br>

        <button type="submit" class="btn btn-primary btn-block">Save</button>

          <div class="login-links">
            <hr />
            <p>Have an account? <a href="../applicantLogin">Login</a></p>
          </div>


    </form>
  </div>

  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>
</html>