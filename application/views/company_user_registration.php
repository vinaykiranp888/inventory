<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Partner University Portal | Partner University Registration</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css">
</head>
<div class="container d-flex flex-column">

    <div class="row">
      <div class="col-sm-12">
          <!-- Card -->
        <form action="#" method="POST">

          <br>

            <div class="card shadow">


              <div class="mb-4">
                <h3 class="login-title">Company Registration</h3>
              </div>


              <div class="form-container">
                <h4 class="form-group-title">Company Details</h4>   


                <div class="row">

                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="form-label"
                              >Company Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" required>
                      </div>
                    </div>

                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="form-label"
                              >Registration Number <span class='error-text'>*</span></label>
                             <input type="text" class="form-control" id="registration_number" name="registration_number" onblur="getRegistrationNumberDuplication()" required>
                      </div>
                   </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="form-label"
                              >Website URL <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="website" name="website" required>
                      </div>
                   </div>

                    <div class="col-sm-6">
                         <div class="form-group">
                            <label class="form-label"
                              >Company Email <span class='error-text'>*</span></label>
                             <input type="text" class="form-control" id="primary_contact_email" name="primary_contact_email" onblur="getCompanyEmailIdDuplication()" required>
                      </div>
                   </div>
                </div>

                <div class="row">

                      <div class="col-sm-6">
                           <div class="form-group">
                              <label class="form-label"
                                >Contact Number <span class='error-text'>*</span></label>
                               <input type="number" class="form-control" id="user_number" name="user_number" required>
                        </div>
                     </div>
                  </div>

              </div>

              <br>



              <div class="form-container">
                <h4 class="form-group-title">Company User Details</h4>                   


                  <div class="row">

                      <div class="col-sm-6">
                         <div class="form-group">
                            <label class="form-label"
                              >Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="corporate_user_name" name="corporate_user_name" required>
                        </div>
                      </div>

                      <div class="col-sm-6">
                           <div class="form-group">
                              <label class="form-label"
                                >User Contact Email <span class='error-text'>*</span></label>
                              <input type="email" class="form-control" id="email" name="email" onblur="getCompanyUserEmailIdDuplication()" required>
                        </div>
                     </div>

                      
                  </div>


                  <div class="row">

                      <div class="col-sm-6">
                           <div class="form-group">
                              <label class="form-label"
                                >User Password <span class='error-text'>*</span></label>
                               <input type="password" class="form-control" id="user_password" name="user_password" required>
                        </div>
                      </div>


                      <div class="col-sm-6">
                           <div class="form-group">
                              <label class="form-label"
                                >User Designation <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="user_designation" name="user_designation" required>
                        </div>
                      </div>


                  </div>


                  <div class="row">


                      <div class="col-sm-6">
                           <div class="form-group">
                              <label class="form-label"
                                >User Role <span class='error-text'>*</span></label>
                               <select name="id_user_role" id="id_user_role" class="form-control" required>
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($companyUserRoleList))
                                  {
                                      foreach ($companyUserRoleList as $record)
                                      {?>
                                          <option value="<?php echo $record->id;?>"><?php echo $record->name;?>
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                        </div>
                      </div>

                  </div>

              </div>


              <div class="row">
                <div class="col-sm-6">
                  <button type="submit" class="btn btn-primary btn-block">Register Now</button>
                </div>
              </div>

                  
            </div>
           
        
        </form>

      </div>

    </div>

</div>


<script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script>

  $('select').select2();


  function showParentCompany()
  {
      var value = $("#level").val();
      // alert(value);
      if(value=='Parent')
      {
          $("#partnerdropdown").hide();
      }
      else if(value=='Branch')
      {
          $("#partnerdropdown").show();
      }
  }

  function getCompanyEmailIdDuplication()
    {
      var primary_contact_email = $("#primary_contact_email").val()

      if(primary_contact_email != '')
      {

        var tempPR = {};
        tempPR['primary_contact_email'] = primary_contact_email;
        tempPR['registration_number'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/companyUserLogin/getCompanyEmailIdDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate E-Mail Id Not Allowed, Corporate Already Registered With The Given Email Id : '+ tempPR['primary_contact_email'] );
                  $("#primary_contact_email").val('');
              }
           }
        });
      }
    }


    function getRegistrationNumberDuplication()
    {
      var registration_number = $("#registration_number").val()

      // alert(registration_number);
      
      if(registration_number != '')
      {

        var tempPR = {};
        tempPR['primary_contact_email'] = '';
        tempPR['registration_number'] = registration_number;
        

        $.ajax(
        {
           url: '/companyUserLogin/getRegistrationNumberDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Registration Number Not Allowed, Corporate Already Registered With The Given Registration Number : '+ tempPR['registration_number'] );
                  $("#registration_number").val('');
              }
           }
        });
      }
    }

    function getCompanyUserEmailIdDuplication()
    {
      var email = $("#email").val()

      // alert(registration_number);
      
      if(email != '')
      {

        var tempPR = {};
        tempPR['email'] = email;
        

        $.ajax(
        {
           url: '/companyUserLogin/getCompanyUserEmailIdDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate User E-Mail Id Not Allowed, Corporate User  Already Registered With The Given Email-Id : '+ email );
                  $("#email").val('');
              }
           }
        });
      }
    }

</script>



