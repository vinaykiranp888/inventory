   <!--PAGE HEADER STARTS HERE-->

    <div class="course-lists">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-xl-12 col-lg-12 col-md-12 col-12">
            <div>
              <h1 class="mb-0 text-white display-4">List Page</h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--PAGE HEADER ENDS HERE-->

    <!--LIST PAGE  STARTS HERE-->

    <div class="filter-wrapper py-2">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-auto filter-text">Filter By:</div>
          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="categoryFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Category <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                    <?php for($i=0;$i<count($categoryList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $categoryList[$i]->id;?>"                    
                    id="courseType<?php echo $categoryList[$i]->id;?>"
                    name="courseType[]"

                  />
                  <label class="custom-control-label" for="courseType<?php echo $categoryList[$i]->id;?>"
                    ><?php echo $categoryList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 
              
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>
        
          <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="studyLevelFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Skill Level <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                <?php for($i=0;$i<count($studyLevelList);$i++) { ?> 

                <div class="custom-control custom-checkbox">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $studyLevelList[$i]->id;?>"                    
                    id="studyType<?php echo $studyLevelList[$i]->id;?>"
                    name="studyType[]"

                  />
                  <label class="custom-control-label" for="studyType<?php echo $studyLevelList[$i]->id;?>"
                    ><?php echo $studyLevelList[$i]->name;?></label
                  >
                </div>
                   <?php }?> 


                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>


           <div class="col-md-auto">
            <div class="btn-group filter-dropdown" id="productTypeFilter">
              <button
                type="button"
                class="btn btn-outline-secondary dropdown-toggle"
                data-toggle="dropdown"
                data-display="static"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Course Type <span class="value badge badge-primary"></span>
              </button>
              <div class="dropdown-menu">
                  <?php for($i=0;$i<count($productTypeList);$i++) { ?> 
                <div class="custom-control custom-checkbox mb-1">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    value="<?php echo $productTypeList[$i]->id;?>"                    
                    id="productType<?php echo $productTypeList[$i]->id;?>"
                    name="productType[]"
  <?php if($productTypeList[$i]->id==$catid) { ?>
                      checked=""

                    <?php } ?> 


                  />
                  <label class="custom-control-label" for="productType<?php echo $productTypeList[$i]->id;?>"
                    ><?php echo $productTypeList[$i]->name;?></label
                  >
                </div>

              <?php } ?> 
                
                <div class="dropdown-divider"></div>
                <button class="dropdown-item btn btn-link clear-btn">
                  Clear
                </button>
              </div>
            </div>
          </div>

          <div class="col-md-auto">
            <div class="btn-group filter-dropdown">
              <button
                type="button"
                class="btn btn-primary"
                onclick="getProducts()"
              >
                Apply Now
              </button>
            </div>
          </div>


        </div>
      </div>
    </div>
  <div class="py-6 course-lists-container">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="tab-content">
              <div class="tab-pane fade pb-4 active show" id="tabPaneGrid" role="tabpanel" aria-labelledby="tabPaneGrid">
                <div class="row" id="appendTableDiv">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="pt-5" id="loadingicon" style="display: none;">
      <div class="container">
        <div class="row">
          <div class="col-lg-10 offset-lg-1 mb-5">
            <div class="card mb-3">
              <div class="card-body text-center">
                <span class="loader-icon">
                  <i class="fe fe-loader"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--LIST ENDS  STARTS HERE-->

    <!-- FEATURES WRAPPERS STARTS HERE-->
    <div class="bg-white py-4 shadow-sm features-wrapper">
      <div class="container">
        <div class="row align-items-center no-gutters">
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-video"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">
                  30,000 online courses
                </h4>
                <p class="mb-0">Enjoy a variety of fresh topics</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-users"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Expert instruction</h4>
                <p class="mb-0">Find the right instructor for you</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-clock"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Lifetime access</h4>
                <p class="mb-0">Learn on your schedule</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- FEATURES WRAPPERS ENDS HERE-->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->




    <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="/website/js/main.js"></script>

   <script>

    $( document ).ready(function() {
      var cn = '<?php echo $type;?>';
      if(cn=='course') {
        var productIds = '0';
        var courseIds = '0,1';
         $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
      }

      else if(cn=='program') {
        var productIds = '0';
        var courseIds = '0,2';
         $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
      }

       else {

         $.post('/course/getprogramme', {productname:cn}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
      }
  });

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

     function getProducts()
    {

      $("#loadingicon").show();
      var productIds = '0';
        $.each($("input[name='productType[]']:checked"), function() {
          productIds = productIds+","+$(this).val();
        });


         var courseIds = '0';
        $.each($("input[name='courseType[]']:checked"), function() {
          courseIds = courseIds+","+$(this).val();
        });


         var studyIds = '0';
        $.each($("input[name='studyType[]']:checked"), function() {
          studyIds = studyIds+","+$(this).val();
        });



       $.post('/course/getprogramme', {productyType:productIds, courseType:courseIds,studyType:studyIds}, function(response){ 
          $("#loadingicon").hide();
           $("#appendTableDiv").html(response);
        });
       
    }

  </script>
  


  </body>
</html>
