

    <!-- BANNER START HERE -->
    <div class="banner-wrapper d-flex align-items-center">
      <img src="/website/img/banner_img.jpg" class="hero-img" />
      <div class="container">
        <!-- Hero Section -->
        <div class="py-5">
          <h1 class="text-white font-weight-bold">
            The Best Online Learning Solution
          </h1>
          <p class="text-white mb-5 lead">
            By connecting students all over the world to the best instructors
          </p>
          <a href="/course/index" class="btn btn-primary btn-lg">Get Started</a>
        </div>
      </div>
    </div>
    <!-- BANNER ENDS HERE -->

    <!-- FEATURES WRAPPERS STARTS HERE-->
    <div class="bg-white py-4 shadow-sm features-wrapper">
      <div class="container">
        <div class="row align-items-center no-gutters">
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-video"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">
                  Variety of topics
                </h4>
                <p class="mb-0">Enjoy a variety of fresh topics</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-6 mb-lg-0 mb-4">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-users"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Expert instruction</h4>
                <p class="mb-0">Find the right instructor for you</p>
              </div>
            </div>
          </div>
          <!-- Features -->
          <div class="col-xl-4 col-lg-4 col-md-12">
            <div class="d-flex align-items-center">
              <span
                class="icon-sahpe icon-lg bg-light-warning rounded-circle text-center text-dark-warning font-size-md"
              >
                <i class="fe fe-clock"> </i
              ></span>
              <div class="ml-3">
                <h4 class="mb-0 font-weight-semi-bold">Flexible learning</h4>
                <p class="mb-0">Learn anytime anywhere</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- FEATURES WRAPPERS ENDS HERE-->

    <!--content-->
    <div class="pt-lg-12 pb-lg-3 pt-8 pb-6 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Latest Programme</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 

            for($i=0;$i<count($popularprogrammeList);$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $popularprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$popularprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $popularprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $popularprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $popularprogrammeList[$i]->max_duration . " - " . $popularprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM - <?php echo $popularprogrammeList[$i]->amount;?></div>
                    <div class="ml-auto">
                      <span class="rating-star mr-1">
                        <img src="/website/img/star_icon.svg" alt="star" />
                      </span>
                      <span class="text-warning">4.5</span>
                      <span class="font-size-xs text-muted">(7,700)</span>
                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="javascript:buynow(<?php echo $popularprogrammeList[$i]->id;?>,<?php echo $popularprogrammeList[$i]->amount;?>)" class="btn btn-outline-primary btn-sm">Buy Now</a>
                  <a href="javascript:buynow(<?php echo $popularprogrammeList[$i]->id;?>,<?php echo $popularprogrammeList[$i]->amount;?>)" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  >
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($popularprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $popularprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $popularprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


                  <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<2;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$recomendedprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $recomendedprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $recomendedprogrammeList[$i]->max_duration . " - " . $recomendedprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM - <?php echo $recomendedprogrammeList[$i]->amount;?></div>
                    <div class="ml-auto">
                      <span class="rating-star mr-1">
                        <img src="/website/img/star_icon.svg" alt="star" />
                      </span>
                      <span class="text-warning">4.5</span>
                      <span class="font-size-xs text-muted">(7,700)</span>
                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="javascript:buynow(<?php echo $recomendedprogrammeList[$i]->id;?>,<?php echo $recomendedprogrammeList[$i]->amount;?>)" class="btn btn-outline-primary btn-sm">Buy Now</a>
                  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  >
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($recomendedprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $recomendedprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $recomendedprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 



        </div>
      </div>
    </div>

     <!--TOP CATEGORIES STARTS HERE -->
    <div class="pt-lg-12 pb-lg-3 pt-5">
      <div class="container">
        <h2 class="h4 mb-4">Top Categories</h2>
        <div class="row align-items-center">
          <div class="col-md-3">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_1.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Design</span>
            </a>
          </div>
          <div class="col-md-3">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_2.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Development</span>
            </a>
          </div>
          <div class="col-md-3">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_3.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Data Science</span>
            </a>
          </div>
          <div class="col-md-3">
            <a href="#" class="card category-card mb-4">
              <img src="/website/img/category_4.jpg" class="img-fluid" alt="..." />
              <span class="category-title">Business</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="pb-lg-3 pt-lg-3 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Recommended for you</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<4;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$recomendedprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $recomendedprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $recomendedprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $recomendedprogrammeList[$i]->max_duration . " - " . $recomendedprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM - <?php echo $recomendedprogrammeList[$i]->amount;?></div>
                    <div class="ml-auto">
                      <span class="rating-star mr-1">
                        <img src="/website/img/star_icon.svg" alt="star" />
                      </span>
                      <span class="text-warning">4.5</span>
                      <span class="font-size-xs text-muted">(7,700)</span>
                    </div>
                  </div>
                </div>
                

                <div class="d-flex mt-2">
                  <a href="javascript:buynow(<?php echo $recomendedprogrammeList[$i]->id;?>,<?php echo $recomendedprogrammeList[$i]->amount;?>)" class="btn btn-outline-primary btn-sm">Buy Now</a>
                  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  >
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($recomendedprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $recomendedprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $recomendedprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>


      <div class="pb-lg-3 pt-lg-3 course-card">
      <div class="container">
        <div class="row d-lg-flex align-items-center mb-4">
          <div class="col">
            <h2 class="mb-0">Browse for Free</h2>
          </div>
          <div class="col-auto">
            <a href="/course/index" class="btn btn-outline-primary btn-sm">View All</a>
          </div>
        </div>
        <div class="row">
          <?php 
          // print_r($programmeList);exit;

                    for($i=0;$i<4;$i++) { 
          



            ?>


          <div class="col-lg-3 col-md-6 col-12">
            <div class="card mb-4 card-hover">
              <a href="/coursedetails/index/<?php echo $latestprogrammeList[$i]->id;?>" class="card-img-top">
                     <img
                  src="<?php echo "/assets/images/".$latestprogrammeList[$i]->image;?>"
                  alt
                  class="rounded-top card-img-top"
                />
              </a>
              <div class="card-body">
                <h4 class="mb-2 text-truncate-line-2" class="text-inherit">
                  <a href="/coursedetails/index/<?php echo $latestprogrammeList[$i]->id;?>" class="text-inherit"
                    ><?php echo $latestprogrammeList[$i]->name;?></a
                  >
                </h4>
                <!--list-->
                  <ul class="mb-2 list-inline">
                  <li class="list-inline-item">
                    <i class="fe fe-clock mr-1"></i><?php echo $latestprogrammeList[$i]->max_duration . " - " . $latestprogrammeList[$i]->duration_type ?>
                  </li>
                  <li class="list-inline-item">
                    <i class="fe fe-bar-chart mr-1"></i>Advance
                  </li>
                </ul>
                <!-- <div class="1h-1">
                  <div class="d-flex">
                    <div class="h5">RM - <?php echo $popularprogrammeList[$i]->amount;?></div>
                    <div class="ml-auto">
                      <span class="rating-star mr-1">
                        <img src="/website/img/star_icon.svg" alt="star" />
                      </span>
                      <span class="text-warning">4.5</span>
                      <span class="font-size-xs text-muted">(7,700)</span>
                    </div>
                  </div>
                </div> -->
                

                <div class="d-flex mt-2">
                  <a href="javascript:buynow(<?php echo $latestprogrammeList[$i]->id;?>,<?php echo $latestprogrammeList[$i]->amount;?>)" class="btn btn-outline-primary btn-sm">Buy Now</a>
                  <a href="#" class="btn btn-outline-info btn-sm ml-auto"
                    >Add to Cart</a
                  >
                </div>
              </div>
              <!--footer card-->
              <div class="card-footer">
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <?php if($latestprogrammeList[$i]->staffimage!='') {?>
                    <img
                      src="/website/staff/<?php echo $latestprogrammeList[$i]->staffimage;?>"
                      class="rounded-circle avatar-xs"
                    />
                  <?php } ?> 
                  </div>
                  <div class="col ml-2">
                    <span><?php echo $latestprogrammeList[$i]->staffname;?></span>
                  </div>
                 
                </div>
              </div>
            </div>
          </div>

                  <?php } ?> 


        </div>
      </div>
    </div>



    <!-- TESTIMONIAL STARTS HERE -->
    <div class="testimonial-wrapper mx-lg-10 text-center">
      <div class="container">
        <h1 class="text-white pt-5 pb-4">Testimonials</h1>
        <div
          id="testimonialCarousel"
          class="carousel slide"
          data-ride="carousel"
        >
          <ol class="carousel-indicators">
            <li
              data-target="#testimonialCarousel"
              data-slide-to="0"
              class="active"
            ></li>
            <li data-target="#testimonialCarousel" data-slide-to="1"></li>
            <li data-target="#testimonialCarousel" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner pb-5">
            <div class="carousel-item active">
              <div class="w-lg-75 mx-lg-auto mb-5">
                <blockquote class="h4 text-white font-weight-normal">
                  "This platform and courses offered was what I needed to push my career into the next level. Everything was really comprehensible and  full of useful content"
                </blockquote>
                <div class="w-lg-50 mx-lg-auto mt-5">
                  <div class="avatar avatar-circle mb-3">
                    <img
                      class="avatar-img"
                      src="/website/testimonial/Syahirah.png"
                      alt="Image Description"
                    />
                  </div>
                  <h4 class="text-white mb-0">Syahirah</h4>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="w-lg-75 mx-lg-auto mb-5">
                <blockquote class="h4 text-white font-weight-normal">
                  "I am improving my professional and personal development. Thanks for all who made it available online. It’s very excellent"
                </blockquote>
                <div class="w-lg-50 mx-lg-auto mt-5">
                  <div class="avatar avatar-circle mb-3">
                    <img
                      class="avatar-img"
                      src="/website/testimonial/nazmi.jpg"
                      alt="Image Description"
                    />
                  </div>
                  <h4 class="text-white mb-0">Nazmi Nabil</h4>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="w-lg-75 mx-lg-auto mb-5">
                <blockquote class="h4 text-white font-weight-normal">
                  "This platform is highly practical. Highly recommend - the course provides a diverse range of topics, straightforward and clear"
                </blockquote>
                <div class="w-lg-50 mx-lg-auto mt-5">
                  <div class="avatar avatar-circle mb-3">
                    <img
                      class="avatar-img"
                      src="/website/testimonial/adib.jpg"
                      alt="Image Description"
                    />
                  </div>
                  <h4 class="text-white mb-0">Adib Khusyairi</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- TESTIMONIAL ENDS HERE -->

    <!-- OUR PARTNERS STARTS HERE -->
    <div class="our-partners container text-center">
      <h1 class="pt-5">Our Partners</h1>
      <div id="ourPartnersCarousel" class="carousel slide" data-ride="carousel">
        <!-- <ol class="carousel-indicators">
          <li
            data-target="#ourPartnersCarousel"
            data-slide-to="0"
            class="active"
          ></li>
          <li data-target="#ourPartnersCarousel" data-slide-to="1"></li>
          <li data-target="#ourPartnersCarousel" data-slide-to="2"></li>
        </ol> -->
        <div class="carousel-inner py-3">
          <div class="carousel-item active">
            <div class="row justify-content-between align-items-center text-center">
              <div class="col-4">
                <img src="/website/img/itrain_logo.png" class="img-fluid" />
              </div>
              <div class="col-4">
                <img src="/website/img/AeU_New_Logo.png" class="img-fluid" />
              </div>
              <div class="col-4">
                <img src="/website/img/speed_logo.svg" class="img-fluid" />
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- OUR PARTNERS STARTS HERE -->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script
      src="//code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
    <script src="<?php echo BASE_PATH;?>website/js/jquery-1.12.4.min.js"></script>

     <script>

   function buynow(id,amount)
    {
      $.noConflict();

        jQuery.get("/coursedetails/tempbuynow/"+id+"/"+amount, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>

  </body>
</html>

  