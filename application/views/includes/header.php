<?php 

error_reporting(0);
$this->load->model('register_model');
$namesArray = $this->register_model->getCertificateNames(2);

$productList = $this->register_model->getCategoryList();

       $id_session = session_id();


        $listOfCourses = $this->register_model->gerProgrammeFromSession($id_session);


?>

  <!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />

    <!-- Bootstrap CSS -->
    <link
      rel="stylesheet"
      href="//cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
      integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
      crossorigin="anonymous"
    />

    <title>Home | Speed</title>

    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/select2.min.css" rel="stylesheet" />


    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>website/css/feather.css" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>website/css/main.css" />
  </head>
   


  <body>
    <!-- NAV BAR STARTS HERE -->
    <nav class="navbar navbar-expand-lg navbar-default main-header">
      <div class="container-fluid px-0">
        <a class="navbar-brand" href="/index"
          ><img src="<?php echo BASE_PATH; ?>website/img/speed_logo.svg" alt=""
        /></a>
        <!-- Mobile view nav wrap -->
        <ul
          class="navbar-nav navbar-right-wrap ml-auto d-lg-none d-flex nav-top-wrap"
        >
          <li class="dropdown ml-2">
            <a
              class="rounded-circle"
              href="#!"
              role="button"
              id="dropdownUser"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <div class="avatar avatar-md avatar-indicators avatar-online">
                <img
                  alt="avatar"
                  src="./img/avatar-1.jpg"
                  class="rounded-circle"
                />
              </div>
            </a>
            <div
              class="dropdown-menu dropdown-menu-right"
              aria-labelledby="dropdownUser"
            >
              <div class="dropdown-item">
                <div class="d-flex">
                  <div class="avatar avatar-md avatar-indicators avatar-online">
                    <img
                      alt="avatar"
                      src="./img/avatar-1.jpg"
                      class="rounded-circle"
                    />
                  </div>
                  <div class="ml-3 lh-1">
                    <h5 class="mb-1">John Smith</h5>
                    <p class="mb-0 text-muted">john@smith.com</p>
                  </div>
                </div>
              </div>
              <div class="dropdown-divider"></div>
              <ul class="list-unstyled">
                <li>
                  <a class="dropdown-item" href="#">
                    <i class="fe fe-user mr-2"></i>Profile
                  </a>
                </li>
              </ul>
              <div class="dropdown-divider"></div>
              <ul class="list-unstyled">
                <li>
                  <a class="dropdown-item" href="#">
                    <i class="fe fe-power nav-icon"></i>Sign Out
                  </a>
                </li>
              </ul>
            </div>
          </li>
        </ul>
        <!-- Button -->
        <button
          class="navbar-toggler collapsed"
          type="button"
          data-toggle="collapse"
          data-target="#navbar-default"
          aria-controls="navbar-default"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="icon-bar top-bar mt-0"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbar-default">
          <ul class="navbar-nav">




            <li class="nav-item dropdown">
              <a
                class="nav-link dropdown-toggle"
                href="#"
                id="navbarBrowse"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                data-display="static"
                onclick="redirectcourse()"
              >
                Explore
              </a>
              <ul
                class="dropdown-menu dropdown-menu-arrow"
                aria-labelledby="navbarBrowse"
              >

              <?php for($p=0;$p<count($productList);$p++) { 


                $programmeList = $this->register_model->getProgrammeByProductTypeId($productList[$p]->id);


                  ?>
                <li class="dropdown-submenu dropright">
                  <a
                    class="dropdown-item dropdown-list-group-item dropdown-toggle"
                    href="#"
                    onclick="getCategories(<?php echo $productList[$p]->id;?>)"
                  >
                    <?php echo ucfirst($productList[$p]->name);?>
                  </a>
                  <ul class="dropdown-menu">
                    <?php for($m=0;$m<count($programmeList);$m++) { ?> 
                    <li>
                      <a
                        class="dropdown-item"
                        href="/coursedetails/index/<?php echo $programmeList[$m]->id;?>"
                      >
                        <?php echo $programmeList[$m]->name;?></a
                      >
                    </li>
                  <?php } ?> 
                    
                  </ul>
                </li>

              <?php } ?> 

             <li class="dropdown-submenu dropright">
                  <a
                    class="dropdown-item dropdown-list-group-item dropdown-toggle"
                    href="#"
                    onclick="getCategories(<?php echo $productList[$p]->id;?>)"
                  >
                    Professional Certificate
                  </a>
                  <ul class="dropdown-menu">

              <?php for($i=0;$i<count($namesArray);$i++) { ?>
                <li class="dropdown-submenu dropright">
                  <a
                    class="dropdown-item dropdown-list-group-item"
                    href="<?php echo "/programdetails/index/".$namesArray[$i]->id;?>"
                  >
                    <?php echo $namesArray[$i]->name;?>
                  </a>
                </li>
              <?php } ?> 
            </ul>
          </li>
        </ul>


         
            </li>
          </ul>
          <form method="POST" action='/course/index' 
            class="mt-3 mt-lg-0 d-flex align-items-center col-lg-4 col-xl-5 search-form"
          >
            <span class="position-absolute pl-3 search-icon">
              <i class="fe fe-search"></i>
            </span>

            <input
              type="text"
              class="form-control pl-6"
              placeholder="What do you want to learn"
              name='search'
              id='search'
            />
          </form>
          <ul class="navbar-nav navbar-right-wrap ml-auto">


            <li class="nav-item">
              <a
                class="nav-link"
                href="/partnerUniversityLogin"
              >
                Partner
              </a>
            </li>


            <li class="nav-item">
              <a
                class="nav-link"
                href="/companyUserLogin"
              >
                Corporate Client
              </a>
            </li>




            <li class="nav-item">
              <a
                class="nav-link"
                href="/login"
              >
                 Student
              </a>
            </li>

            <li class="nav-item">
              <a
                class="nav-link position-relative cart-link"
                href="/index/checkout"
                title="cart"
              >
                <i class="fe fe-shopping-cart mr-2"></i>
                <?php if(count($listOfCourses)>0) {?>
                <span class="cart-count"><?php echo count($listOfCourses);?></span>
              <?php } ?> 
              </a>
            </li>
          </ul>

          <!-- after login code -->
         <!--  <ul class="navbar-nav navbar-right-wrap d-none d-lg-block">
            <li class="dropdown ml-2 d-inline-block">
              <a
                class="rounded-circle"
                href="#!"
                role="button"
                id="dropdownUserProfile"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                <div class="avatar avatar-md avatar-indicators avatar-online">
                  <img
                    alt="avatar"
                    src="./img/avatar-1.jpg"
                    class="rounded-circle"
                  />
                </div>
              </a>
              <div
                class="dropdown-menu dropdown-menu-right"
                aria-labelledby="dropdownUserProfile"
              >
                <div class="dropdown-item">
                  <div class="d-flex">
                    <div
                      class="avatar avatar-md avatar-indicators avatar-online"
                    >
                      <img
                        alt="avatar"
                        src="./img/avatar-1.jpg"
                        class="rounded-circle"
                      />
                    </div>
                    <div class="ml-3 lh-1">
                      <h5 class="mb-1">John Smith</h5>
                      <p class="mb-0 text-muted">john@smith.com</p>
                    </div>
                  </div>
                </div>
                <div class="dropdown-divider"></div>
                <ul class="list-unstyled">
                  <li>
                    <a class="dropdown-item" href="#">
                      <i class="fe fe-user mr-2"></i>Profile
                    </a>
                  </li>
                </ul>
                <div class="dropdown-divider"></div>
                <ul class="list-unstyled">
                  <li>
                    <a class="dropdown-item" href="#">
                      <i class="fe fe-power nav-icon"></i>Sign Out
                    </a>
                  </li>
                </ul>
              </div>
            </li>
          </ul> -->
          <!-- -->
        </div>
      </div>
    </nav>
    <!-- NAV BAR ENDS HERE-->

<script type="text/javascript">
  function redirectcourse(){
    parent.location='/course/index/course';
  }

  function redirectprogram(){
    parent.location='/course/index/program';
  }

  function getCategories(id){
    parent.location='/course/index/'+id;    
    //alert(id);
  }
</script>