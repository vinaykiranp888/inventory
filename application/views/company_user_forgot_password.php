<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Speed | Company User Forgot Password</title>
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
</head>

<body>
  <div class="login-wrapper">
    <di class="container">
      <div class="login-container" > 
        <div class="text-center">
          <a href="/"><img src="<?php echo BASE_PATH; ?>assets/img/speed_logo.svg" /></a>     
        </div>
        <h3 class="login-title">Company User Login</h3>
        <div>
          <?php
          $this->load->helper('form');
          $error = $this->session->flashdata('error');
          if ($error) {
          ?>
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <?php echo $error; ?>
            </div>
          <?php }
          $success = $this->session->flashdata('success');
          $entered_url = $this->session->flashdata('entered_url');
          // print_r($success);exit();
    
          if ($success)
          {
          ?>
              <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $success; ?>
              </div>
          <?php
          }
          ?>
        </div>
        <div>
          <form method="post">
            <div class="form-group">
              <label>Email Address</label>
              <input type="email" class="form-control" placeholder="Email" id="email" name="email" required onblur="checkUserExist()">
            </div>
            <button type="submit" class="btn btn-primary btn-block">Send Email</button>
            <div class="login-links">              
              <p>Have Password <a href="/companyUserLogin/companyUserLogin">Login Here</a></p>
              <p>Dont Have an account? <a href="/companyUserLogin/registration">Register Here</a></p>
            </div>
          </form>
        </div>
      </div>
    </di>
  </div>

  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
</body>

</html>

<script>
  
  function checkUserExist()
  {
    if($("#email").val() != '')
    {
      var tempPR = {};
      tempPR['email'] = $("#email").val();
      tempPR['password'] = $("#password").val();

      // alert(tempPR['email']);
          $.ajax(
          {
             url: '/companyUserLogin/checkUserExist',
             type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              // alert(result);

                if(result == '0')
                {
                  alert('Corporate Account Not Activated, Wait till the Approval');
                  window.location.reload();
                  // $("#email").val('');
                  // $("#password").val('');
                }
                else if(result == '2')
                {
                  alert('Corporate User Account Not Verified, Check Your Mail For Verification');
                  window.location.reload();
                  // $("#email").val('');
                  // $("#password").val('');
                }
             }
          });
    }
  }

</script>