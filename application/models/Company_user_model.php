<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_user_model extends CI_Model
{
    
    function loginCompanyUser($email, $password)
    {
        $this->db->select('stu.*, cur.name as user_role, com.image');
        $this->db->from('company_users as stu');
        $this->db->join('company_user_role as cur', 'stu.id_user_role = cur.id');
        $this->db->join('company as com', 'com.id = stu.id_company');
        $this->db->where('stu.email', $email);
        $query = $this->db->get();
        
        $user = $query->row();
        
        if(!empty($user))
        {
            if($user->password == md5($password))
            {
                return $user;
            }else
            {
                return array();
            }
        }
        else
        {
            return array();
        }
    }

    function companyUserLastLoginInfo($id_company_user)
    {
        $this->db->select('ll.created_dt_tm');
        $this->db->where('ll.id_company_user', $id_company_user);
        $this->db->order_by('ll.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('company_user_last_login as ll');

        return $query->row();
    }

    function companyUserRoleListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('company_user_role');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function addCompanyUserLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('company_user_last_login', $loginInfo);
        $this->db->trans_complete();
    }

    function addNewCompany($data)
    {
        $this->db->trans_start();
        $this->db->insert('company', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewCompanyUser($data)
    {
        $this->db->trans_start();
        $this->db->insert('company_users', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function parentCompanyList($level)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('level', $level);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function checkCompanyUserExist($data)
    {
        $this->db->select('c.*');
        $this->db->from('company_users as c');
        $this->db->where('c.email', $data['email']);
        $query = $this->db->get();

        return $query->row();
    }

    function checkUserExist($data)
    {
        $this->db->select('c.*');
        $this->db->from('company as c');
        $this->db->join('company_users as cu', 'cu.id_company = c.id');
        // $this->db->where('isDeleted', 0);
        $this->db->where('cu.email', $data['email']);
        $query = $this->db->get();

        return $query->row();
    }

    function getForgorPasswordByActivationId($activation_id)
    {
        $this->db->select('c.*');
        $this->db->from('reset_user_password as c');
        $this->db->where('c.type', 'CompanyUser');
        $this->db->where('c.activation_id', $activation_id);
        $this->db->order_by('c.id', 'DESC');
        $query = $this->db->get();

        return $query->row();
    }

    function addNewResetPassword($data)
    {
        $this->db->trans_start();
        $this->db->insert('reset_user_password', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateResetPassword($data,$update_data)
    {
        $this->db->where('email', $update_data['email']);
        $this->db->where('type', $update_data['type']);
        $this->db->update('reset_user_password', $data);
        return TRUE;
    }

    function updateCompanyUser($data,$email)
    {
        $this->db->where('email', $email);
        $this->db->update('company_users', $data);
        return TRUE;
    }


    function getCompanyEmailIdDuplication($data)
    {
        $this->db->select('st.*');
        $this->db->from('company as st');
        if($data['primary_contact_email'] != '')
        {
            $this->db->where('st.primary_contact_email', $data['primary_contact_email']);
        }
        if($data['registration_number'] != '')
        {
            $this->db->where('st.registration_number', $data['registration_number']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getCompanyUserEmailIdDuplication($data)
    {
        $this->db->select('st.*');
        $this->db->from('company_users as st');
        if($data['email'] != '')
        {
            $this->db->where('st.email', $data['email']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}
?>