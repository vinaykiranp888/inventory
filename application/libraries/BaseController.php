<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class : BaseController
 * Base Class to control over all the classes
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class BaseController extends MX_Controller
{
    protected $role = '';
    protected $vendorId = '';
    protected $name = '';
    protected $roleText = '';
    protected $global = array();
    protected $lastLogin = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('role_model');
        // $this->isLoggedIn();
    }
    /**
     * Takes mixed data and optionally a status code, then creates the response
     *
     * @access public
     * @param array|NULL $data
     *        	Data to output to the user
     *        	running the script; otherwise, exit
     */
    public function response($data = NULL)
    {
        $this->output->set_status_header(200)->set_content_type('application/json', 'utf-8')->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))->_display();
        exit();
    }

    function isLoggedIn()
    {
        // $enteredURL = current_url(); //http://myhost/main
        $enteredURL = $_SERVER['REQUEST_URI'];
        // echo $enteredURL;exit();

        $this->load->library('session');
        $this->session->set_userdata('entered_admin_url', $enteredURL);

        $isLoggedIn = $this->session->userdata('isLoggedIn');

        if (!isset($isLoggedIn) || $isLoggedIn != TRUE)
        {
            redirect('adminLogin');
        }
        else
        {
            $this->role = $this->session->userdata('role');
            $this->vendorId = $this->session->userdata('userId');
            $this->name = $this->session->userdata('name');
            $this->roleText = $this->session->userdata('roleText');
            $this->lastLogin = $this->session->userdata('lastLogin');
            $this->id_user = $this->session->userdata('userId');
            $this->user_image = $this->session->userdata('user_image');

            $this->global['id_user'] = $this->id_user;
            $this->global['name'] = $this->name;
            $this->global['role'] = $this->role;
            $this->global['role_text'] = $this->roleText;
            $this->global['last_login'] = $this->lastLogin;
            $this->global['user_image'] = $this->user_image;
        }
    }

    function isStudentLoggedIn()
    {
        // echo "string";exit();
        $this->load->library('session');
        $isStudentLoggedIn = $this->session->userdata('isStudentLoggedIn');

        if (!isset($isStudentLoggedIn) || $isStudentLoggedIn != TRUE) {
            redirect('studentLogin');
        } else {
            $this->student_name = $this->session->userdata('student_name');
            $this->id_student = $this->session->userdata('id_student');
            $this->student_last_login = $this->session->userdata('student_last_login');

            $this->global['student_name'] = $this->student_name;
            $this->global['id_student'] = $this->id_student;
            $this->global['student_last_login'] = $this->student_last_login;
        }
    }

    function isApplicantLoggedIn()
    {
        // echo "string";exit();
        $this->load->library('session');
        $isApplicantLoggedIn = $this->session->userdata('isApplicantLoggedIn');

        if (!isset($isApplicantLoggedIn) || $isApplicantLoggedIn != TRUE) {
            redirect('applicantLogin');
        } else {
            $this->applicant_name = $this->session->userdata('applicant_name');
            $this->id_applicant = $this->session->userdata('id_applicant');
            $this->applicant_last_login = $this->session->userdata('applicant_last_login');

            $this->global['applicant_name'] = $this->applicant_name;
            $this->global['id_applicant'] = $this->id_applicant;
            $this->global['applicant_last_login'] = $this->applicant_last_login;
        }
    }

    function isCompanyUserLoggedIn()
    {
        $entered_corporate_user_url = $_SERVER['REQUEST_URI'];
        // echo $enteredURL;exit();

        $this->load->library('session');
        $this->session->set_userdata('entered_corporate_user_url', $entered_corporate_user_url);

        // echo "string";exit();
        $isCompanyUserLoggedIn = $this->session->userdata('isCompanyUserLoggedIn');

        if (!isset($isCompanyUserLoggedIn) || $isCompanyUserLoggedIn != TRUE)
        {
            redirect('companyUserLogin');
        }
        else
        {
            $this->id_company_user = $this->session->userdata('id_company_user');
            $this->id_company = $this->session->userdata('id_company');
            $this->company_user_full_name = $this->session->userdata('company_user_full_name');
            $this->company_user_name = $this->session->userdata('company_user_name');
            $this->company_user_email = $this->session->userdata('company_user_email');
            $this->company_image = $this->session->userdata('company_image');

            $this->global['id_company_user'] = $this->id_company_user;
            $this->global['id_company'] = $this->id_company;
            $this->global['company_user_full_name'] = $this->company_user_full_name;
            $this->global['company_user_name'] = $this->company_user_name;
            $this->global['company_user_email'] = $this->company_user_email;
            $this->global['company_image'] = $this->company_image;
        }
    }

    function isPartnerUniversityLoggedIn()
    {
        $this->load->library('session');
        $isPartnerUniversityLoggedIn = $this->session->userdata('isPartnerUniversityLoggedIn');
        
        if (! isset ( $isPartnerUniversityLoggedIn ) || $isPartnerUniversityLoggedIn != TRUE)
        {
            // echo $isPartnerUniversityLoggedIn;exit;
            redirect('partnerUniversityLogin');
        }
        else
        {
            // echo "ii";exit;
            $this->partner_university_name = $this->session->userdata ( 'partner_university_name' );
            $this->partner_university_code = $this->session->userdata ( 'partner_university_code' );
            $this->partner_university_login_id = $this->session->userdata ( 'partner_university_login_id' );
            $this->id_partner_university = $this->session->userdata ( 'id_partner_university' );
            $this->partner_university_last_login = $this->session->userdata ( 'partner_university_last_login' );
            $this->partner_university_image = $this->session->userdata ( 'partner_university_image' );
            
            
            $this->global ['partner_university_code'] = $this->partner_university_code;
            $this->global ['partner_university_login_id'] = $this->partner_university_login_id;
            $this->global ['partner_university_name'] = $this->partner_university_name;
            $this->global ['id_partner_university'] = $this->id_partner_university;
            $this->global ['partner_university_last_login'] = $this->partner_university_last_login;
            $this->global ['partner_university_image'] = $this->partner_university_image;
        }
    }


    function checkAccess($code)
    {
        // return 0;
        $id_role = $this->role;

        // echo $id_role;exit();

        if($id_role == 1)
        {
            return 1;
        }
        else
        {
            // echo $id_role;exit();

            $this->load->model('setup/role_model');
            $canAccess = $this->role_model->checkAccess($id_role,$code);
            // echo $canAccess;exit();
            
            // echo $canAccess;exit();
            
            return $canAccess;
        }
    }
    function hasExamActive()
    {
        $this->load->model('setup/role_model');
        $canAccess = $this->role_model->hasExamActive($this->session->id_student);
        return $canAccess;
    }


    function isTicketter()
    {
        if ($this->role != ROLE_ADMIN || $this->role != ROLE_MANAGER) {
            return true;
        } else {
            return false;
        }
    }

    function loadAccessRestricted()
    {
        $this->global['pageTitle'] = 'Examination Management System : Access Denied';

        $this->load->view('includes/header', $this->global);
        $this->load->view('access');
        $this->load->view('includes/footer');
    }
    function loadNoExams()
    {
        $this->global['pageTitle'] = 'Examination Management System : No Exams';

        $this->load->view('includes/header', $this->global);
        $this->load->view('noexams');
        $this->load->view('includes/footer');
    }
    function logout()
    {
        $this->session->sess_destroy();

        redirect('login');
    }

    function loadViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL)
    {

        $this->load->view('includes/header', $headerInfo);
        $this->load->view('includes/sidebar', $headerInfo);
        $this->load->view('includes/footer', $footerInfo);
        $this->load->view($viewName, $pageInfo);
    }
    function loadExamViews($viewName = "", $headerInfo = NULL, $pageInfo = NULL, $footerInfo = NULL)
    {

        $this->load->view('includes/examheader', $headerInfo);
        $this->load->view('includes/sidebar', $headerInfo);
        $this->load->view('includes/examfooter', $footerInfo);
        $this->load->view($viewName, $pageInfo);
    }

    function paginationCompress($link, $count, $perPage = 10, $segment = SEGMENT)
    {
        $this->load->library('pagination');

        $config['base_url'] = base_url() . $link;
        $config['total_rows'] = $count;
        $config['uri_segment'] = $segment;
        $config['per_page'] = $perPage;
        $config['num_links'] = 5;
        $config['full_tag_open'] = '<nav><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_tag_open'] = '<li class="arrow">';
        $config['first_link'] = 'First';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="arrow">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li class="arrow">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="arrow">';
        $config['last_link'] = 'Last';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $page = $config['per_page'];
        $segment = $this->uri->segment($segment);

        return array(
            "page" => $page,
            "segment" => $segment
        );
    }

    function getDomainName()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'] . '/';
        return $protocol . $domainName;
    }

    // 3 Parameters 
    // 1. File Name
    // 2. File Temp Name
    // 2. File Key (To Show Erroe msz)

    function uploadFile($file, $file_tmp, $key)
    {
        // echo microtime();exit;
        // $date = date('dmY_his');

       if($file) {
            $uniqueId = rand(0000000000, 9999999999);
            $upload_name = md5($uniqueId);

            $domain = $this->getDomainName();

            $root = $_SERVER['DOCUMENT_ROOT'];
            // echo $root;exit;

            $upload_path = $root . '/assets/images/';


            $fileinfo = pathinfo($file);

            $extension = $fileinfo['extension'];
            $file_name = $fileinfo['filename'];

            $uploaded_file_name = $upload_name . '.' . $extension;
            $file_path = $upload_path . $uploaded_file_name;

            // $data['certificate'] = $uploaded_file_name;
            // echo "<Pre>";print_r($data);exit();


            if (move_uploaded_file($file_tmp, $file_path)) {
                return $uploaded_file_name;
            } else {
                echo "Unable To Uplaod" . $key . " Try After Some Time";
            }
        }
    }

    // For Validation Of Size & Formats
    // 1. File Extension
    // 2. File Size
    // 2. File Key (To Show Erroe msz)


    function fileFormatNSizeValidation($file_ext, $file_size, $key)
    {
        $extensions = array("jpeg", "jpg", "png", "docx", "pdf");

        if (in_array($file_ext, $extensions) === false) {
            $errors[] = $key . "File Format Not Allowed, Choose a JPEG/ PNG/ JPG/ DOCX/ PDF file.";
        }

        if ($file_size > 2097152) {
            $errors[] = $key . 'File size must be Below 2 MB';
        }
    }


    function deleteServerFile($file)
    {
        $root = $_SERVER['DOCUMENT_ROOT'];
        $path = $root . '/assets/images/ ' . $file;
        // $path="uploads/file_01.jpg";
        if (unlink($path)) {
            echo "File Deleted";
        } else {
            echo "Not Deleted";
        }
    }

    function getMpdfLibrary()
    {
        $base_url = $_SERVER['HTTP_HOST'];

        // $main_invoice = $this->main_invoice_model->getApplicantInformation('28');
        // print_r($base_url);exit;
        
        switch ($base_url)
        {
            case 'inventory.com':
            case 'http://inventory.com':
            case 'https://inventory.com':
            
            include("/var/www/html/inventory/assets/mpdf/vendor/autoload.php");
            break;

            case 'inventory.camsedu.com':
            case 'http://eagspeed.camsedu.com/':
            case 'https://eagspeed.camsedu.com/':

            include("/home/inventory.camsedu.com/public_html/assets/mpdf/vendor/autoload.php");
            break;
            
            default:
            include("/home/inventory.camsedu.com/assets/mpdf/vendor/autoload.php");
            break;
        }
    }

    function getCurlLibrary()
    {
        $base_url = $_SERVER['HTTP_HOST'];

        // $main_invoice = $this->main_invoice_model->getApplicantInformation('28');
        // print_r($base_url);exit;
        
        switch ($base_url)
        {
            case 'speed.com':
            case 'https://speed.com':

            include('/var/www/html/speed/assets/vendor/curl.php');
            break;
            

            case 'speed.camsedu.com':
            case 'https://speed.camsedu.com':
            include("/home/speed.camsedu/public_html/assets/vendor/curl.php");

            break;
    
            default:
            break;
        }
    }

    
    function getAmountWordings($number)
    {
       $no = floor($number);
       $point = round($number - $no, 2) * 100;
       $hundred = null;
       $digits_1 = strlen($no);
       $i = 0;
       $str = array();
       $words = array(
        '0' => '', '1' => 'one', '2' => 'two','3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six','7' => 'seven', '8' => 'eight', '9' => 'nine','10' => 'ten','11' => 'eleven', '12' => 'twelve','13' => 'thirteen', '14' => 'fourteen','15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen','18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty','30' => 'thirty', '40' => 'forty', '50' => 'fifty','60' => 'sixty', '70' => 'seventy','80' => 'eighty', '90' => 'ninety');

       $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');

       while ($i < $digits_1)
       {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
      }
      
      $str = array_reverse($str);
      $result = implode('', $str);
      $points = ($point) ?
        " . " . $words[$point / 10] . " " . 
              $words[$point = $point % 10] : '';
      return $result . " " . $points . "";
    }
}
