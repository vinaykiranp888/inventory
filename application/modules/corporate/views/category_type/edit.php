<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Category Type</h3>
        </div>
        <form id="form_category" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Course Type Details</h4>


            <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $categoryTypeDetails->name; ?>">
                    </div>
                  </div>
                </div>


                <div class="col-lg-6">
                  <div class="form-group row align-items-center">
                    <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($categoryTypeDetails->status == 1){
                            echo "checked=checked";
                        } ?> >
                        <label class="custom-control-label" for="customRadioInline1">Active</label>
                      </div>
                      <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($categoryTypeDetails->status == 0){
                            echo "checked=checked";
                        } ?> >
                        <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                      </div>
                    </div>
                  </div>
                </div>




                        
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_programgrade").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>