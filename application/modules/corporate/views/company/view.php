<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Approve Company </h3>
        </div>





            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_1" class="nav-link border rounded text-center"
                            aria-controls="tab_1" aria-selected="true"
                            role="tab" data-toggle="tab">Company Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center" aria-controls="tab_2" role="tab" data-toggle="tab">Company Users</a>
                    </li>

                    <!-- <li role="presentation"><a href="#tab_3" class="nav-link border rounded text-center" aria-controls="tab_3" role="tab" data-toggle="tab">Course Overview</a>
                    </li>

                    <li role="presentation"><a href="#tab_4" class="nav-link border rounded text-center" aria-controls="tab_4" role="tab" data-toggle="tab">Delivery Mode</a>
                    </li>

                    <li role="presentation"><a href="#tab_5" class="nav-link border rounded text-center" aria-controls="tab_5" role="tab" data-toggle="tab">Assesment</a>
                    </li>

                    <li role="presentation"><a href="#tab_6" class="nav-link border rounded text-center" aria-controls="tab_6" role="tab" data-toggle="tab">Syllabus</a>
                    </li> -->
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="tab_1">
                        <div class="col-12 mt-4">




                        <form id="form_programme" action="" method="post" enctype="multipart/form-data">
                            <div class="form-container">
                                <h4 class="form-group-title">Company Details</h4>
                                    

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Company Level <span class='error-text'>*</span></label>
                                                <select name="level" id="level" class="form-control" onchange="showParentCompany()">
                                                    <option value="">Select</option>
                                                    <option value="Parent"
                                                    <?php
                                                    if($companyDetails->level == 'Parent')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                    >Parent</option>
                                                    <!-- <option value="Branch"
                                                    <?php
                                                    if($companyDetails->level == 'Branch')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>>Branch</option> -->
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                                            <div class="form-group">
                                                <label>Parent Company <span class='error-text'>*</span></label>
                                                <select name="id_parent_company" id="id_parent_company" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($parentCompanyList))
                                                    {
                                                        foreach ($parentCompanyList as $record)
                                                        {
                                                            if ($record->id != $companyDetails->id)
                                                            {
                                                            ?>
                                                            <option value="<?php echo $record->id;  ?>"
                                                            <?php
                                                            if($companyDetails->id_parent_company == $record->id)
                                                            {
                                                                echo 'selected';
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->registration_number . " - " . $record->name;  ?>
                                                            </option>
                                                    <?php
                                                            }
                                                        }
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                        </div>

                                        
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Name <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $companyDetails->name; ?>">
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Registration Number <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="registration_number" name="registration_number" value="<?php echo $companyDetails->registration_number; ?>">
                                            </div>
                                        </div>


                                        <!-- <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Password <span class='error-text'>*</span></label>
                                                <input type="password" class="form-control" id="password" name="password" value="<?php echo $companyDetails->password; ?>" readonly>
                                            </div>
                                        </div> -->


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Joined Date <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" value="<?php echo $companyDetails->joined_date; ?>" readonly>
                                            </div>
                                        </div>




                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Website <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="website" name="website" value="<?php echo $companyDetails->website; ?>">
                                            </div>
                                        </div>




                                    </div>

                                    <div class="row">





                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Chairman </label>
                                                <input type="text" class="form-control" id="chairman" name="chairman" value="<?php echo $companyDetails->chairman; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>CEO </label>
                                                <input type="text" class="form-control" id="ceo" name="ceo" value="<?php echo $companyDetails->ceo; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Address 1 </label>
                                                <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $companyDetails->address1; ?>">
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Address 2 </label>
                                                <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $companyDetails->address2; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select Country </label>
                                                <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($countryList))
                                                    {
                                                        foreach ($countryList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;?>"
                                                            <?php
                                                            if($companyDetails->id_country == $record->id)
                                                            {
                                                                echo 'selected';
                                                            }
                                                            ?>
                                                            ><?php echo $record->name;?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                           <div class="form-group">
                                              <label>Select State </label>
                                              <span id='view_state'>
                                                   <select name="id_state" id="id_state" class="form-control">
                                                    <option value=''>Select</option>
                                                   </select>
                                              </span>
                                           </div>
                                        </div>



                                    </div>

                                    <div class="row">




                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>City </label>
                                                <input type="text" class="form-control" id="city" name="city" value="<?php echo $companyDetails->city; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Postal Code </label>
                                                <input type="number" class="form-control" id="zipcode" name="zipcode" value="<?php echo $companyDetails->zipcode; ?>">
                                            </div>
                                        </div>




                                        <div class="col-sm-4">
                                            <label>Telephone Number </label>
                                              <div class="row">
                                                 <div class="col-sm-4">

                                                   <select name="country_code" id="country_code" class="form-control">
                                                    <option value="">Select</option>                    
                                                    <?php
                                                        if (!empty($countryList))
                                                        {
                                                          foreach ($countryList as $record)
                                                          {
                                                            if($record->phone_code != '')
                                                            {
                                                        ?>
                                                     <option value="<?php echo $record->phone_code;  ?>"
                                                        <?php
                                                        if($companyDetails->country_code == $record->phone_code)
                                                        {
                                                            echo 'selected';
                                                        }
                                                        ?>
                                                        >
                                                        <?php echo $record->phone_code . " - " . $record->name;  ?>
                                                     </option>
                                                       <?php
                                                            }
                                                        }
                                                      }
                                                    ?>
                                                  </select>
                                            </div>
                                            <div class="col-sm-8">
                                              <input type="number" class="form-control" id="phone" name="phone" value="<?php echo $companyDetails->phone; ?>">
                                            </div>
                                            </div>

                                        </div>



                                    </div>

                                    <div class="row">




                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Staff Strength </label>
                                                <input type="number" class="form-control" id="staff_strength" name="staff_strength" value="<?php echo $companyDetails->staff_strength; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Corporate Membership Status </label>
                                                <select name="corporate_membership_status" id="corporate_membership_status" class="form-control">
                                                    <option value="">Select</option>
                                                    <option value="Active"
                                                    <?php
                                                    if($companyDetails->corporate_membership_status == 'Active')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>>Active</option>
                                                    <option value="In-Active"
                                                    <?php
                                                    if($companyDetails->corporate_membership_status == 'In-Active')
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>>In-Active</option>
                                                    </select>
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Debtor Code </label>
                                                <input type="number" class="form-control" id="debtor_code" name="debtor_code" value="<?php echo $companyDetails->debtor_code; ?>">
                                            </div>
                                        </div>



                                        

                                    </div>

                                    <div class="row">



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Credit Terms(DAYS) </label>
                                                <input type="number" class="form-control" id="credit_term" name="credit_term" value="<?php echo $companyDetails->credit_term; ?>">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Control Account </label>
                                                <input type="text" class="form-control" id="control_account" name="control_account" value="<?php echo $companyDetails->control_account; ?>">
                                            </div>
                                        </div>


                                    
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Credit Term Code </label>
                                                <input type="text" class="form-control" id="credit_term_code" name="credit_term_code" value="<?php echo $companyDetails->credit_term_code; ?>">
                                            </div>
                                        </div>




                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-events">
                                                <p>Staff Credit Acceptable <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="staff_credit" id="staff_credit" value="1" <?php if($companyDetails->staff_credit=='1') {
                                                     echo "checked=checked";
                                                  };?>><span class="check-radio"></span> Yes
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="staff_credit" id="staff_credit" value="0" <?php if($companyDetails->staff_credit=='0') {
                                                     echo "checked=checked";
                                                  };?>>
                                                  <span class="check-radio"></span> No
                                                </label>                              
                                            </div>                         
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Status <span class='error-text'>*</span></label>
                                                <select name="status" id="status" class="form-control" onchange="showRejectReason(this.value)">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($statusListByType))
                                                    {
                                                        foreach ($statusListByType as $record)
                                                        {?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php if($companyDetails->status == $record->id)
                                                        {
                                                            echo "selected";
                                                        };?>
                                                        >
                                                        <?php echo $record->name;?>
                                                    </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-sm-4" id="view_reject" style="display: none">
                                            <div class="form-group">
                                                <label>Reason <span class='error-text'>*</span></label>
                                                <input type="text" id="reason" name="reason" class="form-control">
                                            </div>
                                        </div>
                                        


                
                                        <!-- <div class="col-sm-4">
                                            <div class="form-events">
                                                <p>Status <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="1" <?php if($companyDetails->status=='1') {
                                                     echo "checked=checked";
                                                  };?>><span class="check-radio"></span> Active
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="0" <?php if($companyDetails->status=='0') {
                                                     echo "checked=checked";
                                                  };?>>
                                                  <span class="check-radio"></span> In-Active
                                                </label>                              
                                            </div>                         
                                        </div> -->

                                    


                                    </div>


                                </div>

                                <br>



                                <div class="form-container">
                                 <h4 class="form-group-title">Primary Contact Details</h4>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Name </label>
                                          <input type="text" class="form-control" id="primary_contact_name" name="primary_contact_name" value="<?php echo $companyDetails->primary_contact_name; ?>">
                                       </div>
                                    </div>

                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Designation </label>
                                          <input type="text" class="form-control" id="primary_contact_designation" name="primary_contact_designation" value="<?php echo $companyDetails->primary_contact_designation; ?>">
                                       </div>
                                    </div>


                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Email <span class='error-text'>*</span></label>
                                          <input type="text" class="form-control" id="primary_contact_email" name="primary_contact_email" value="<?php echo $companyDetails->primary_contact_email; ?>">
                                       </div>
                                    </div>
                                    
                                    
                                 
                                 </div>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                        <label>Telephone Number </label>
                                          <div class="row">
                                             <div class="col-sm-4">

                                               <select name="primary_country_code" id="primary_country_code" class="form-control">
                                                <option value="">Select</option>                    
                                                <?php
                                                    if (!empty($countryList))
                                                    {
                                                      foreach ($countryList as $record)
                                                      {
                                                        if($record->phone_code != '')
                                                        {
                                                    ?>
                                                 <option value="<?php echo $record->phone_code;  ?>"
                                                    <?php
                                                    if($companyDetails->primary_country_code == $record->phone_code)
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                    >
                                                    <?php echo $record->phone_code . "  " . $record->name;  ?>
                                                 </option>
                                                   <?php
                                                        }
                                                    }
                                                  }
                                                ?>
                                              </select>
                                        </div>
                                        <div class="col-sm-8">
                                          <input type="number" class="form-control" id="primary_phone" name="primary_phone" value="<?php echo $companyDetails->primary_phone; ?>">
                                        </div>
                                        </div>

                                    </div>


                                 </div>
                              
                              </div>




                              <div class="form-container">
                                 <h4 class="form-group-title">Secondary Contact Details</h4>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Name </label>
                                          <input type="text" class="form-control" id="second_contact_name" name="second_contact_name" value="<?php echo $companyDetails->second_contact_name; ?>">
                                       </div>
                                    </div>

                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Designation </label>
                                          <input type="text" class="form-control" id="second_contact_designation" name="second_contact_designation" value="<?php echo $companyDetails->second_contact_designation; ?>">
                                       </div>
                                    </div>


                                    <div class="col-sm-4">
                                       <div class="form-group">
                                          <label>Email </label>
                                          <input type="text" class="form-control" id="second_contact_email" name="second_contact_email" value="<?php echo $companyDetails->second_contact_email; ?>">
                                       </div>
                                    </div>
                                    
                                    
                                 
                                 </div>

                                 <div class="row">
                                 
                                    <div class="col-sm-4">
                                        <label>Telephone Number </label>
                                          <div class="row">
                                             <div class="col-sm-4">

                                               <select name="second_country_code" id="second_country_code" class="form-control">
                                                <option value="">Select</option>                    
                                                <?php
                                                    if (!empty($countryList))
                                                    {
                                                      foreach ($countryList as $record)
                                                      {
                                                        if($record->phone_code != '')
                                                        {
                                                    ?>
                                                 <option value="<?php echo $record->phone_code;  ?>"
                                                    <?php
                                                    if($companyDetails->second_country_code == $record->phone_code)
                                                    {
                                                        echo 'selected';
                                                    }
                                                    ?>
                                                    >
                                                    <?php echo $record->phone_code . " - " . $record->name;  ?>
                                                 </option>
                                                   <?php
                                                        }
                                                    }
                                                  }
                                                ?>
                                              </select>
                                        </div>
                                        <div class="col-sm-8">
                                          <input type="number" class="form-control" id="second_phone" name="second_phone" value="<?php echo $companyDetails->second_phone; ?>">
                                        </div>
                                        </div>

                                    </div>


                                 </div>
                              
                              </div>





                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                    <a href="../list" class="btn btn-link">Back</a>
                                </div>
                            </div>


                        </form>


                        



                        </div> 
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_2">
                        <div class="mt-4">


                        <form id="form_tab_2" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Company User Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="user_name1" name="user_name1">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Contact Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="user_number" name="user_number">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="email" name="email">
                                        </div>
                                    </div>


                                </div>



                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Password <span class='error-text'>*</span></label>
                                            <input type="password" class="form-control" id="user_password" name="user_password">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Designation <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="user_designation" name="user_designation">
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>User  Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="user_name" name="user_name">
                                        </div>
                                    </div> -->




                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Role <span class='error-text'>*</span></label>
                                            <select name="id_user_role" id="id_user_role" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($companyUserRoleList))
                                                {
                                                    foreach ($companyUserRoleList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"><?php echo $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>



                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Status <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="active_status" id="active_status" value="1" checked="checked"><span class="check-radio"></span> Active
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="active_status" id="active_status" value="0"><span class="check-radio"></span> Inactive
                                            </label>
                                        </div>
                                    </div>



                                </div>




                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveCompanyUserDetails()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                            </form>



                            <?php

                            if(!empty($companyUsersList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Company User Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>Contact Number</th>
                                                 <th>Email</th>
                                                 <th>Designation</th>
                                                 <!-- <th>User Name</th> -->
                                                 <th>Role</th>
                                                 <th>Status</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($companyUsersList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $companyUsersList[$i]->name;?></td>
                                                <td><?php echo $companyUsersList[$i]->phone;?></td>
                                                <td><?php echo $companyUsersList[$i]->email;?></td>
                                                <td><?php echo $companyUsersList[$i]->designation;?></td>
                                                <!-- <td><?php echo $companyUsersList[$i]->user_name;?></td> -->
                                                <td><?php echo $companyUsersList[$i]->user_role;?></td>
                                                <td><?php
                                                if($companyUsersList[$i]->status == 1)
                                                {
                                                  echo 'Single Mode';
                                                }elseif ($companyUsersList[$i]->status == 0)
                                                {
                                                  echo 'Multiple Mode';
                                                }
                                                ?>                                                    
                                                </td>
                                                <td>
                                                <a onclick="deleteProgramObjectiveDetails(<?php echo $companyUsersList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>














                    <div role="tabpanel" class="tab-pane" id="tab_3">
                        <div class="mt-4">


                        <form id="form_program_overview" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Course Overview Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="overview_name" name="overview_name">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveCourseOverviewData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                            </form>



                            <?php

                            if(!empty($programmeCourseOverviewList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Course Overview Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeCourseOverviewList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeCourseOverviewList[$i]->name;?></td>
                                                <td>
                                                <a onclick="deleteCourseOverviewDetails(<?php echo $programmeCourseOverviewList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>










                    <div role="tabpanel" class="tab-pane" id="tab_4">
                        <div class="mt-4">


                        <form id="form_program_delivery_mode" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Delivery Mode Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="delivery_mode_name" name="delivery_mode_name">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveDeliveryModeData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                            </form>



                            <?php

                            if(!empty($programmeDeliveryModeList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Delivery Mode Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeDeliveryModeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeDeliveryModeList[$i]->name;?></td>
                                                <td>
                                                <a onclick="deleteDeliveryModeDetails(<?php echo $programmeDeliveryModeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>












                    <div role="tabpanel" class="tab-pane" id="tab_5">
                        <div class="mt-4">


                        <form id="form_program_assesment" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Assesment Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="assesment_name" name="assesment_name">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveAssesmentData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                            </form>



                            <?php

                            if(!empty($programmeAssesmentList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Assesment Details Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeAssesmentList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeAssesmentList[$i]->name;?></td>
                                                <td>
                                                <a onclick="deleteAssesmentDetails(<?php echo $programmeAssesmentList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>










                    <div role="tabpanel" class="tab-pane" id="tab_6">
                        <div class="mt-4">


                        <form id="form_program_syllabus" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Syllabus Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="syllabus_name" name="syllabus_name">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="button" class="btn btn-primary btn-lg" onclick="saveSyllabusData()">Add</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>



                        </form>



                            <?php

                            if(!empty($programmeSyllabusList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Syllabus Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programmeSyllabusList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programmeSyllabusList[$i]->name;?></td>
                                                <td>
                                                <a onclick="deleteSyllabusDetails(<?php echo $programmeSyllabusList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>









                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script type="text/javascript">

   $('select').select2();


    $(function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });

    function showParentCompany()
    {
        var value = $("#level").val();
        if(value=='Parent')
        {
            $("#partnerdropdown").hide();
        }
        else if(value=='Branch')
        {
            $("#partnerdropdown").show();
        }
    }



    function getStateByCountry(id)
    {
        if(id != '')
        {
            $.get("/corporate/company/getStateByCountry/"+id, function(data, status)
            {
                $("#view_state").html(data);
            });
        }
    }

    function showRejectReason(status)
    {
        if(status != '' && status == '1')
        {
            $("#view_reject").hide();
        }
        if(status > '1')
        {
            $("#view_reject").show();
        }
    }


    function saveCompanyUserDetails()
    {
        if($('#form_tab_2').valid())
        {

        var tempPR = {};
        tempPR['name'] = $("#user_name1").val();
        tempPR['phone'] = $("#user_number").val();
        tempPR['email'] = $("#email").val();
        tempPR['designation'] = $("#user_designation").val();
        tempPR['user_name'] = $("#user_name").val();
        tempPR['id_user_role'] = $("#id_user_role").val();
        tempPR['status'] = $("#active_status").val();
        tempPR['password'] = $("#user_password").val();
        tempPR['id_company'] = <?php echo $id_company;?>;
            $.ajax(
            {
               url: '/corporate/company/saveCompanyUserDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                window.location.reload();
               }
            });
        }
    }



    function deleteCompanyUsers(id)
    {
        $.ajax(
        {
           url: '/corporate/company/deleteCompanyUsers/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
                // $("#view").html(result);
                // alert('Deleted Sessessfully');
                window.location.reload();
           }
        });
    }



    $(document).ready(function()
    {
        var value = "<?php echo $companyDetails->level ?>";
        // alert(value);
        if(value == 'Branch')
        {
            $("#partnerdropdown").show();
        }


        var id_country = "<?php echo $companyDetails->id_country ?>";

        // alert(id_country);
        if(id_country != '')
        {
            $.get("/corporate/company/getStateByCountry/"+id_country, function(data, status)
            {
                var id_state = "<?php echo $companyDetails->id_state ?>";
                
                // alert(id_state);

                $("#view_state").html(data);
                $("#id_state").find('option[value="'+id_state+'"]').attr('selected',true);
                $('select').select2();
            });
        }


        $("#form_programme").validate({
            rules: {
                level: {
                    required: true
                },
                id_parent_company: {
                    required: true
                },
                name: {
                    required: true
                },
                registration_number: {
                    required: true
                },
                joined_date: {
                    required: true
                },
                website: {
                    required: true
                },
                staff_credit: {
                    required: true
                },
                reason: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                level: {
                    required: "<p class='error-text'>Level Required</p>",
                },
                id_parent_company: {
                    required: "<p class='error-text'>Parent Company Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                registration_number: {
                    required: "<p class='error-text'>Registration Number Required</p>",
                },
                joined_date: {
                    required: "<p class='error-text'>Joined Date Required</p>",
                },
                website: {
                    required: "<p class='error-text'>Website Required</p>",
                },
                staff_credit: {
                    required: "<p class='error-text'>Select Staff Credit Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
    {
        $("#form_tab_2").validate({
            rules: {
                user_name1: {
                    required: true
                },
                user_number: {
                    required: true
                },
                email: {
                    required: true
                },
                user_designation: {
                    required: true
                },
                user_name: {
                    required: true
                },
                id_user_role: {
                    required: true
                },
                active_status: {
                    required: true
                },
                user_password: {
                    required: true
                }
            },
            messages: {
                user_name1: {
                    required: "<p class='error-text'>Select Scheme</p>",
                },
                user_number: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                user_designation: {
                    required: "<p class='error-text'>Designation Required</p>",
                },
                user_name: {
                    required: "<p class='error-text'>User Name Required</p>",
                },
                id_user_role: {
                    required: "<p class='error-text'>Select User Role</p>",
                },
                active_status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                user_password: {
                    required: "<p class='error-text'>Password Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>