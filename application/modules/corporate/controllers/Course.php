<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Course extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $data['departmentList'] = $this->department_model->departmentList();
            // $data['staffList'] = $this->staff_model->staffList();
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_course_type'] = $this->security->xss_clean($this->input->post('id_course_type'));
            $formData['id_faculty_program'] = $this->security->xss_clean($this->input->post('id_faculty_program'));
            // $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));

            $data['searchParam'] = $formData;
            $data['courseList'] = $this->course_model->courseListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : Course List';
            //print_r($subjectDetails);exit;
            $this->loadViews("course/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
            
            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
            $id_staff_coordinator = $this->security->xss_clean($this->input->post('id_staff_coordinator'));
            $id_department = $this->security->xss_clean($this->input->post('id_department'));
            $credit_hours = $this->security->xss_clean($this->input->post('credit_hours'));
            $id_course_type = $this->security->xss_clean($this->input->post('id_course_type'));
            $id_course_description = $this->security->xss_clean($this->input->post('id_course_description'));
            $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
            $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'name_optional_language' => $name_optional_language,
                    'id_staff_coordinator' => $id_staff_coordinator,
                    'id_department' => $id_department,
                    'id_course_type' => $id_course_type,
                    'id_course_description' => $id_course_description,
                    'id_faculty_program' => $id_faculty_program,
                    'credit_hours' => $credit_hours,
                    'status' => $status
                );
                
                $result = $this->course_model->addNewCourse($data);
                redirect('/prdtm/course/edit/'. $result);
            }
            //print_r($data['stateList']);exit;

            $data['staffList'] = $this->course_model->staffListByStatus('1');
            // $data['courseTypeList'] = $this->course_model->courseTypeListByStatus('1');
            // $data['courseDescriptionList'] = $this->course_model->courseDescriptionListByStatus('1');
            $data['facultyProgramList'] = $this->course_model->facultyProgramListByStatus('1');
            $data['departmentList'] = $this->course_model->departmentListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Course';
            $this->loadViews("course/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/course/list');
            }
            if($this->input->post())
            {

            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
            $id_staff_coordinator = $this->security->xss_clean($this->input->post('id_staff_coordinator'));
            $id_department = $this->security->xss_clean($this->input->post('id_department'));
            $credit_hours = $this->security->xss_clean($this->input->post('credit_hours'));
            $id_course_type = $this->security->xss_clean($this->input->post('id_course_type'));
            $id_course_description = $this->security->xss_clean($this->input->post('id_course_description'));
            $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
            $status = $this->security->xss_clean($this->input->post('status'));



            $class_total_hr = $this->security->xss_clean($this->input->post('class_total_hr'));
            $class_recurrence = $this->security->xss_clean($this->input->post('class_recurrence'));
            $class_recurrence_period = $this->security->xss_clean($this->input->post('class_recurrence_period'));
            $tutorial_total_hr = $this->security->xss_clean($this->input->post('tutorial_total_hr'));
            $tutorial_recurrence = $this->security->xss_clean($this->input->post('tutorial_recurrence'));
            $tutorial_recurrence_period = $this->security->xss_clean($this->input->post('tutorial_recurrence_period'));
            $lab_total_hr = $this->security->xss_clean($this->input->post('lab_total_hr'));
            $lab_recurrence = $this->security->xss_clean($this->input->post('lab_recurrence'));
            $lab_recurrence_period = $this->security->xss_clean($this->input->post('lab_recurrence_period'));
            $exam_total_hr = $this->security->xss_clean($this->input->post('exam_total_hr'));
            

                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'name_optional_language' => $name_optional_language,
                    'id_staff_coordinator' => $id_staff_coordinator,
                    'id_department' => $id_department,
                    'id_course_type' => $id_course_type,
                    'id_course_description' => $id_course_description,
                    'id_faculty_program' => $id_faculty_program,
                    'credit_hours' => $credit_hours,
                    'class_total_hr' => $class_total_hr,
                    'class_recurrence' => $class_recurrence,
                    'class_recurrence_period' => $class_recurrence_period,
                    'tutorial_total_hr' => $tutorial_total_hr,
                    'tutorial_recurrence' => $tutorial_recurrence,
                    'tutorial_recurrence_period' => $tutorial_recurrence_period,
                    'lab_total_hr' => $lab_total_hr,
                    'lab_recurrence' => $lab_recurrence,
                    'lab_recurrence_period' => $lab_recurrence_period,
                    'exam_total_hr' => $exam_total_hr,
                    'status' => $status
                );
                
                $result = $this->course_model->editCourse($data,$id);
                redirect('/prdtm/course/list');
            }

            $data['departmentList'] = $this->course_model->departmentListByStatus('1');
            $data['staffList'] = $this->course_model->staffListByStatus('1');
            $data['courseDetails'] = $this->course_model->getCourse($id);
            $data['facultyProgramList'] = $this->course_model->facultyProgramListByStatus('1');
            // $data['courseTypeList'] = $this->course_model->courseTypeListByStatus('1');
            // $data['courseDescriptionList'] = $this->course_model->courseDescriptionListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Edit Course';
            $this->loadViews("course/edit", $this->global, $data, NULL);
        }
    }
}