<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Barring</h3>
        </div>
        <form id="form_barring" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>    


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Program <span class='error-text'>*</span></label>
                            <select name="id_programme" id="id_programme" class="form-control" onchange="getStudentByProgNIntake()">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label style="display: none;" id="display_intake">Select Intake <span class='error-text'>*</span></label>
                            <span id='view_intake' ></span>
                        </div>
                    </div> -->


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label id="display_student">Select Student <span class='error-text'>*</span></label>
                            <span id='view_student'>
                               <select name="id_student" id="id_student" class="form-control">
                                <option value=''>Select</option>
                               </select>
                            </span>
                        </div>
                    </div>



                </div>

            </div>


            <div class="form-container">
                <h4 class="form-group-title">Barring Details</h4>    

                <div class="row">

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Semester <span class='error-text'>*</span></label>
                            <select name="id_semester" id="id_semester" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($semesterList))
                                {
                                    foreach ($semesterList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>      -->

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Barring Type <span class='error-text'>*</span></label>
                            <select name="id_barring_type" id="id_barring_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($barringTypeList))
                                {
                                    foreach ($barringTypeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason">
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Barring From Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="barring_date" name="barring_date" autocomplete="off">
                        </div>
                    </div> 

                 
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Barring To Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="barring_to_date" name="barring_to_date" autocomplete="off">
                        </div>
                    </div>                    

                         

                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>


            <div class="form-container" id="course_student_view" style="display: none;">
                <h4 class="form-group-title">Student & Course Details</h4>

                <div class="custom-table">
                      <div id="view"></div>
                </div>

            </div>




            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    function getStudentByProgNIntake()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/records/barring/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
                    $("#display_intake").show();
               }
            });
    }


    function getStudentByProgNIntake()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        // tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_programme']);
            $.ajax(
            {
               url: '/records/barring/getStudentByProgNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {
                    $("#display_student").show();
                    $("#view_student").html(result);
                }
               }
            });
    }

    function displayStudentData()
    {

        if($("#id_intake").val() != '' && $("#id_programme").val() != '' && $("#id_student").val() != '')
        {
                var id_intake = $("#id_intake").val();
                var id_programme = $("#id_programme").val();
                var id_student = $("#id_student").val();

            $.ajax(
            {
               url: '/records/barring/displayStudentData',
                type: 'POST',
               data:
               {
                'id_intake': id_intake,
                'id_programme': id_programme,
                'id_student':id_student

               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result != '')
                {
                    $("#course_student_view").show();
                    $("#view").html(result);
                }
               }
            });
        }
    }


    $(document).ready(function()
    {
        $("#form_barring").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_barring_type:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                barring_date:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_intake:
                {
                    required: true
                },
                barring_to_date:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_barring_type:
                {
                    required: "<p class='error-text'>Select Barring Type</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                barring_date:
                {
                    required: "<p class='error-text'>Select Barring Date</p>",
                },
                id_programme:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                barring_to_date:
                {
                    required: "<p class='error-text'>Select To Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>