<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="clearfix">            
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="font-size:14px;">Inventory Management </a>             
                </div>
                <ul class="nav navbar-nav navbar-right top-nav">
                    <li>Welcome<a > <?php echo $name; ?> </a></li>
                    <li>Last login <?php echo $last_login; ?> </li>
                    <li><a href="/setup/user/logout">Logout</a></li>
                </ul>               
            </div>



            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav main-nav">
                    <li><a href="/setup/welcome">Setup</a></li>
                    <li><a href="/prdtm/welcome">Product</a></li>
                    <li><a href="/pm/welcome">Partner</a></li>
                    <li><a href="/af/welcome">Facilitator</a></li>
                    <li><a href="/corporate/welcome">Corporate</a></li>
                    <li><a href="/registration/welcome">Registration</a></li>
                    <li><a href="/examination/welcome">Assessment</a></li>
                    <li class="active"><a href="/records/welcome">Records</a></li>     
                    <li><a href="/chatboat/welcome">Chatbot</a></li>                                   
                    <li><a href="/finance/welcome">Finance</a></li>
                    <li><a href="/mrktngm/welcome">Marketing</a></li>
                    <li><a href="/communication/welcome">Communication</a></li>
                    <li><a href="/af/welcome">Moodle</a></li>
                    <li><a href="/reports/welcome">Reporting</a></li>
                </ul>
            </nav>



            
            <!-- <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/setup/welcome">Setup</a></li>
                    <li><a href="/prdtm/welcome">Product Management</a></li>
                    <li><a href="/mrktngm/welcome">Marketing Management</a></li>
                    <li class="active"><a href="/pm/welcome">Partner Management</a></li>
                    <li><a href="/registration/welcome">Registration</a></li>
                    <li>
                        <button class="dropdown-toggle more-link" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <li><a href="/examination/welcome">Assessment</a></li>
                            <li><a href="/corporate/welcome">Corporate</a></li>
                            <li><a href="/records/welcome">Records</a></li>
                            <li><a href="/af/welcome">Facilitator Management</a></li>
                            <li><a href="/finance/welcome">Finance</a></li>
                            <li><a href="/communication/welcome">Communication</a></li>
                            <li><a href="/chatboat/welcome">Smart Chatboat</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </nav> -->

            
        </div>
    </header>
</body>



    <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">

                <br>

                <form action="/setup/organisation/uploadUserImage" id="form_upload_image" action="" method="post" enctype="multipart/form-data">


                  <div class="form-container">
                    <h4 class="form-group-title"> Upload Image</h4>

                      <div class="container">
                      <!-- Page Heading -->
                          <div class="row">
                              <div class="col-6 offset-md-3">
                                

                                    <input type="hidden" name="id" id="id" value="<?php echo $id_user; ?>">                                    
                                    
                           
                                    <div class="form-group">
                                      <label for="exampleInputFile">File input</label>
                                      <input type="file" class="dropify" name="image" id="image" data-height="300" required>
                                       
                                    </div>
                       
                                  
                                   
                              </div>
                          </div>
                               
                      </div>


                  <button type="submit" class="btn btn-primary">Upload</button>

                  </div>
              
              </form>

            </div>
            </div>

          </div>
      

    </div>

<script>
  
  function showModelPopUp()
  {
    $('#myModal').modal('show');
  }
 
</script>