<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Student Profile</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>



            <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Learning Mode :</dt>
                                <dd><?php echo $getStudentData->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $getStudentData->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Organisation :</dt>
                                <dd><?php

                                if($getStudentData->id_university != 1 && $getStudentData->id_university != 0)
                                {
                                    echo $getStudentData->partner_university_code , " - " . $getStudentData->partner_university_name;
                                }
                                else
                                {
                                    echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                                }
                                
                                ?></dd>
                            </dl> 
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php 
                                if($getStudentData->advisor_type == '0')
                                {
                                    echo 'External';
                                }
                                elseif($getStudentData->advisor_type == '1')
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->advisor_name ; ?></dd>
                            </dl>
                            
                            <?php 
                            if($getStudentData->qualification_name == 'POSTGRADUATE')
                            {
                            ?>

                            <dl>
                                <dt>Research Supervisor :</dt>
                                <dd><?php 
                                if($getStudentData->supervisor_type == '0')
                                {
                                    echo 'External';
                                }
                                elseif($getStudentData->supervisor_type == '1')
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->supervisor_name ; ?></dd>
                            </dl>

                            <?php 
                            }
                            ?>
                            <?php 
                            if($getStudentData->qualification_name == 'MASTER' || $getStudentData->qualification_name == 'POSTGRADUATE')
                            {
                            ?>

                               <dl>
                                  <dt>Current Phd Duration :</dt>
                                  <dd><?php echo $getStudentData->phd_duration ?></dd>
                               </dl>

                            <?php
                            }
                            ?>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->programme_code . " - " . $getStudentData->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $getStudentData->scheme_code . " - " .  $getStudentData->scheme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Passport Number :</dt>
                                <dd><?php echo $getStudentData->passport_number; ?></dd>
                            </dl>
                            <dl>
                                <dt>Branch :</dt>
                                <dd><?php
                                
                                    echo $getStudentData->branch_code . " - " . $getStudentData->branch_name;
                                  ?></dd>
                            </dl> 
                            <dl>
                                <dt>Education Level :</dt>
                                <dd><?php echo $getStudentData->qualification_name; ?></dd>
                            </dl>

                            <dl>
                                <dt>Program Structure Type :</dt>
                                <dd><?php echo $getStudentData->program_structure_code . " - " . $getStudentData->program_structure_name; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>


            </div>
    <br>


<form id="form_profile" action="" method="post"> 
    
   <div>
     <div class="m-auto text-center">
       <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
     </div>
    <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Education Details</a>
            </li>
            <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">English Proficiency Details</a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Employment Status</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Profile Details</a>
            </li>
            <li role="presentation"><a href="#tab_emergency_contact_details" class="nav-link border rounded text-center"
                    aria-controls="tab_emergency_contact_details" role="tab" data-toggle="tab">Emergency Contact Details</a>
            </li>
            <li role="presentation"><a href="#visa" class="nav-link border rounded text-center"
                    aria-controls="visa" role="tab" data-toggle="tab">Visa Details</a>
            </li>
            <li role="presentation"><a href="#other" class="nav-link border rounded text-center"
                    aria-controls="other" role="tab" data-toggle="tab">Other Documents</a>
            </li>
            <li role="presentation"><a href="#course" class="nav-link border rounded text-center"
                    aria-controls="course" role="tab" data-toggle="tab">Course Registration</a>
            </li>
        </ul>
        <div class="tab-content offers-tab-content">
          <div role="tabpanel" class="tab-pane active" id="education">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
                <h4 class="form-group-title">Education Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Qualification Level <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="qualification_level" name="qualification_level">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Degree Awarded <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="degree_awarded" name="degree_awarded" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Major / Specialization <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="specialization" name="specialization">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Class Degree <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="class_degree" name="class_degree"  >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Result / CGPA <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="result" name="result" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Year Of Graduation <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="year" name="year" >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Medium Of Instruction <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="medium" name="medium" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country of University/College <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="college_country" name="college_country"  >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name of University/College <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="college_name" name="college_name" >
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Certificate <span class='error-text'>*</span> </label>
                            <input type="file" class="form-control" id="certificate" name="certificate" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Transcript <span class='error-text'>*</span> </label>
                            <input type="file" class="form-control" id="transcript" name="transcript" >
                        </div>
                    </div>
                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="1">Save</button>
                </div>
            </div>


    <div class="form-container">
            <h4 class="form-group-title">Education Qualification Details</h4>

        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Qualification Level</th>
                <th>Degree Awarded</th>
                <th>Result / CGPA</th>
                <th>Year Of Graduation</th>
                <th>College Name</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($examDetails))
              {
                $i=1;
                foreach ($examDetails as $record)
                {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->qualification_level ?></td>
                    <td><?php echo $record->degree_awarded ?></td>
                    <td><?php echo $record->result ?></td>
                    <td><?php echo $record->year ?></td>
                    <td><?php echo $record->college_name ?></td>
                    <td class="text-center"><?php echo anchor('admission/student/delete_exam?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                  </tr>
              <?php
                $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="proficiency">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">English Proficiency Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Test <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="test" name="test">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control datepicker" id="date" name="date" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Score <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="score" name="score">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload File <span class='error-text'>*</span> </label>
                            <input type="file" class="form-control" id="file" name="file"  >
                        </div>
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg"  name="btn_submit" value="2">Save</button>
                </div>
            </div>



    <div class="form-container">
            <h4 class="form-group-title">English Proficiency Details</h4>

        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Test</th>
                <th>Date</th>
                <th>Score</th>
                <th>File</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($proficiencyDetails))
              {
                $i=1;
                foreach ($proficiencyDetails as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->test ?></td>
                    <td><?php echo $record->date ?></td>
                    <td><?php echo $record->score ?></td>
                    <td><?php echo $record->file ?></td>
                    <td class="text-center"><?php echo anchor('admission/student/delete_english?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
             
         </div> <!-- END col-12 -->  
        </div>


        <div role="tabpanel" class="tab-pane" id="employment">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
                <h4 class="form-group-title">Employmet Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Company Name <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="company_name" name="company_name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Company Address <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="company_address" name="company_address" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Telephone Number <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="telephone" name="telephone">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax Number <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="fax_num" name="fax_num">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Designation <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="designation" name="designation" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Position Level <span class='error-text'>*</span> </label>
                            <select class="form-control" id="position" name="position" style="width: 405px;">
                                <option value="">SELECT</option>
                                <option value="Senior Manager">Senior Manager</option>
                                <option value="Manager">Manager</option>
                                <option value="Senior Executive">Senior Executive</option>
                                <option value="Junior Executive">Junior Executive</option>
                                <option value="Non-Executive">Non-Executive</option>
                                <option value="Fresh Entry">Fresh Entry</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Year Of Service <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="service_year" name="service_year">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Industry <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="industry" name="industry" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Job Description <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="job_desc" name="job_desc">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Employment Letter <span class='error-text'>*</span> </label>
                            <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                        </div>
                    </div>
                </div>
            </div>
                
            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="3">Save</button>
                </div>
            </div>

    <div class="form-container">
            <h4 class="form-group-title">Employment Details</h4>

        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Company Name</th>
                <th>Company Address</th>
                <th>Designation</th>
                <th>Position</th>
                <th>Year Of Service</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($employmentDetails))
              {
                $i=1;
                foreach ($employmentDetails as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->company_name ?></td>
                    <td><?php echo $record->company_address ?></td>
                    <td><?php echo $record->designation ?></td>
                    <td><?php echo $record->position ?></td>
                    <td><?php if($record->service_year == ""){ echo "0";} else {
                        echo $record->service_year; } ?></td>
                    <td class="text-center"><?php echo anchor('admission/student/delete_employment?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Profile Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Salutation <span class='error-text'>*</span> </label>
                        <select name="salutation" id="salutation" class="form-control" style="width: 405px;">
                            <?php
                                if (!empty($salutationList)) {
                                    foreach ($salutationList as $record) {
                                ?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php if($getStudentData->salutation==$record->id)
                                            {
                                                echo "selected";
                                            }
                                            ?>
                                            >
                                            <?php echo $record->name;  ?>        
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                        </select>
                    </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php if(!empty($getStudentData->first_name)){ echo $getStudentData->first_name; } ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php if(!empty($getStudentData->last_name)){ echo $getStudentData->last_name; } ?>">
                        </div>
                    </div>
                </div>


                <div class="row">
                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>ID Type <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="id_type" name="id_type" value="<?php if(!empty($profileDetails->id_type)){ echo $profileDetails->id_type; } ?>">
                        </div>
                    </div> -->

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php if(!empty($getStudentData->email_id)){ echo $getStudentData->email_id; } ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC / Passport Number <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php if(!empty($getStudentData->nric)){ echo $getStudentData->nric; } ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Passport Expiry Date <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control datepicker" autocomplete="off" id="passport_expiry_date" name="passport_expiry_date" value="<?php if(!empty($profileDetails->passport_expiry_date)){ echo date('d-m-Y', strtotime($profileDetails->passport_expiry_date)); } ?>">
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getStudentData->phone ?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span> </label>
                            <select class="form-control" id="gender" name="gender" style="width: 405px;">
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getStudentData->gender == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getStudentData->gender == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others" <?php if($getStudentData->gender == "Others"){ echo "selected=selected"; } ?>>OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php if(!empty($getStudentData->date_of_birth)){ echo date('d-m-Y', strtotime($getStudentData->date_of_birth)); } ?>" autocomplete="off">
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status <span class='error-text'>*</span> </label>
                            <select class="form-control" id="martial_status" name="martial_status" style="width: 405px;">
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getStudentData->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getStudentData->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getStudentData->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span> </label>
                            <select name="religion" id="religion" class="form-control" style="width: 405px;">
                            <?php
                                if (!empty($religionList))
                                {
                                    foreach ($religionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getStudentData->religion)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                            ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                        <label>Type Of Nationality <span class='error-text'>*</span> </label>

                         <select name="nationality" id="nationality" class="form-control" style="width: 405px;">
                            <option value="">Select</option>
                            <option value="<?php echo 'Malaysian';?>"
                                <?php 
                                if ($getStudentData->nationality == 'Malaysian')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Malaysian";  ?>
                            </option>

                            <option value="<?php echo 'Other';?>"
                                <?php 
                                if ($getStudentData->nationality == 'Other')
                                {
                                    echo "selected=selected";
                                } ?>>
                                        <?php echo "Other";  ?>
                            </option>
                        </select>


                        </div>
                    </div>



                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Nationality <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="nationality" name="nationality" value="<?php if(!empty($getStudentData->nationality)){ echo $getStudentData->nationality; } ?>">
                        </div>
                    </div> -->
                </div>


                <div class="row">

                    <div class="col-sm-4">
                    <div class="form-group">
                        <label> Race  <span class='error-text'>*</span> </label>
                        <select name="id_race" id="id_race" class="form-control" style="width: 405px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($raceList))
                            {
                                foreach ($raceList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $getStudentData->id_race)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email ID <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php if(!empty($getStudentData->email_id)){ echo $getStudentData->email_id; } ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Upload Employment Letter <span class='error-text'>*</span> </label>
                            <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                        </div>
                    </div>
                </div>


        </div>



        <br>


        <div class="form-container">
            <h4 class="form-group-title">Mailing Address</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Address 1 <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getStudentData->mail_address1 ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Address 2 <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getStudentData->mail_address2 ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Country <span class='error-text'>*</span> </label>
                        <select name="mailing_country" id="mailing_country" class="form-control" style="width: 405px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php 
                             if($getStudentData->mailing_country == $record->id)
                                {
                                 echo "selected"; 
                                } ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing State <span class='error-text'>*</span> </label>
                        <select name="mailing_state" id="mailing_state" class="form-control" style="width: 405px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php 
                             if($getStudentData->mailing_state==$record->id)
                                {
                                 echo "selected"; 
                                } ?>
                                >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing City <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getStudentData->mailing_city ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mailing Zipcode <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getStudentData->mailing_zipcode ?>">
                    </div>
                </div>
            </div>

        </div>

        
        <br>

        <div class="form-container">
            <h4 class="form-group-title">Permanent Address</h4>
            
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Address 1 <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getStudentData->permanent_address1 ?>">
                    </div>
                </div><div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Address 2 <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getStudentData->permanent_address2 ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Country <span class='error-text'>*</span> </label>
                        <select name="permanent_country" id="permanent_country" class="form-control" style="width: 405px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($getStudentData->permanent_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent State <span class='error-text'>*</span> </label>
                        <select name="permanent_state" id="permanent_state" class="form-control" style="width: 405px;">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($getStudentData->permanent_state==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent City <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getStudentData->permanent_city ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Permanent Zipcode <span class='error-text'>*</span> </label>
                        <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getStudentData->permanent_zipcode ?>">
                    </div>
                </div>
            </div>

        </div>


        

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="4">Save</button>
                </div>
            </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="visa">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Visa Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do You Hold a Malaysian Visa <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="malaysian_visa" id="visa1" value="Yes" 
                              
                              >
                              <span class="check-radio"></span> Yes </label>
                            <label class="radio-inline">
                              <input type="radio" name="malaysian_visa" id="visa2" value="No" 
                              
                                >
                              <span class="check-radio"></span> No </label>                              
                        </div>                         
                    </div>

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do You Hold a Malaysian Visa <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="malaysian_visa" id="visa1" value="Yes" 
                              <?php if(isset($visaDetails->malaysian_visa) && $visaDetails->malaysian_visa =='Yes')
                              {
                                 echo "checked=checked";
                              }
                              ;?>>
                              <span class="check-radio"></span> Yes
                             <span class='error-text'>*</span> </label>
                            <label class="radio-inline">
                              <input type="radio" name="malaysian_visa" id="visa2" value="No" 
                              <?php if(isset($visaDetails->malaysian_visa) && $visaDetails->malaysian_visa=='No') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                             <span class='error-text'>*</span> </label>                              
                        </div>                         
                    </div> -->



                    <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <p></p>
                        <label class="radio-inline">
                          <input type="radio" name="malaysian_visa" id="visa1" value="Yes" ><span class="check-radio"></span> Yes
                         <span class='error-text'>*</span> </label>
                        <label class="radio-inline">
                          <input type="radio" name="malaysian_visa" id="visa2" value="No"><span class="check-radio"></span> No
                         <span class='error-text'>*</span> </label>                              
                    </div>                         
                    </div> -->

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Visa Number <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="visa_number" name="visa_number"
                              >
                        </div>
                    </div>
                    
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Visa Status <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="visa_status" name="visa_status"
                              value="Active Visa" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Visa Expiry Date <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control datepicker" id="visa_expiry_date" name="visa_expiry_date" autocomplete="off" 
                              >
                        </div>
                    </div>                   


                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg"  name="btn_submit" value="5">Save</button>
                </div>
            </div>


            <?php
            if(!empty($visaDetails))
            {
            ?>

            <div class="form-container">
            <h4 class="form-group-title">Visa Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Is Malaysian Visa</th>
                        <th>Visa Number</th>
                        <th>Expire Date</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($visaDetails))
                      {
                        $i=1;
                        foreach ($visaDetails as $record) {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->malaysian_visa  ?>     
                            </td>
                            <td><?php echo $record->visa_number ?></td>
                            <td><?php echo date('d-m-Y',strtotime($record->visa_expiry_date)); ?></td>
                            <td class="text-center">
                                <a onclick="deleteVisaDetails(<?php echo $record->id; ?>)">Delete</a>    
                            </td>
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>

            <?php
            }
            ?>



             
         </div> <!-- END col-12 -->  


        </div>

        <div role="tabpanel" class="tab-pane" id="other">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Other Documents</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" name="doc_name" id="doc_name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>File <span class='error-text'>*</span> </label>
                            <input type="file" class="form-control" id="doc_file" name="doc_file" >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="remarks" name="remarks" >
                        </div>
                    </div>
                </div>

            </div>
            
            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="6">Save</button>
                </div>
            </div>

    <div class="form-container">
            <h4 class="form-group-title">Other Document Details</h4>

        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Name</th>
                <th>File</th>
                <th>Remarks</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($otherDocuments))
              {
                $i=1;
                foreach ($otherDocuments as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->doc_name ?></td>
                    <td><?php echo $record->doc_file ?></td>
                    <td><?php echo $record->remarks ?></td>
                    <td class="text-center"><?php echo anchor('admission/student/delete_document?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                  </tr>
              <?php
                $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
             
         </div> <!-- END col-12 -->  
        </div>

        <div role="tabpanel" class="tab-pane" id="course">
            <div class="col-12 mt-4">
                <br>

    <div class="form-container">
            <h4 class="form-group-title">Course Registration Details</h4>

        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Intake</th>
                <th>Programme</th>
                <th>Student</th>
                <th>Course</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($courseRegistrationList))
              {
                $i=1;
                foreach ($courseRegistrationList as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->intake ?></td>
                    <td><?php echo $record->program ?></td>
                    <td><?php echo $record->full_name ?></td>
                    <td><?php echo $record->course ?></td>
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>
    </div>
             
         </div> <!-- END col-12 -->  
        </div>






        <div role="tabpanel" class="tab-pane" id="tab_emergency_contact_details">
            <div class="col-12 mt-4">
                <br>

            <div class="form-container">
            <h4 class="form-group-title">Emergency Contact Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Relationship <span class='error-text'>*</span> </label>
                            <select name="relationship" id="relationship" class="form-control">
                            <option value="">Select</option>
                            <option value="Father">Father</option>
                            <option value="Mother">Mother</option>
                            <option value="Brother">Brother</option>
                            <option value="Sister">Sister</option>
                            <option value="Friend">Friend</option>
                            <option value="Relative">Relative</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="relative_name" name="relative_name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address <span class='error-text'>*</span> </label>
                            <input type="text" class="form-control" id="relative_address" name="relative_address" autocomplete="off">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="relative_mobile" name="relative_mobile">
                        </div>
                    </div>
               

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Home Phone Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="relative_home_phone" name="relative_home_phone">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Office Phone Number </label>
                            <input type="number" class="form-control" id="relative_office_phone" name="relative_office_phone">
                        </div>
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="7">Save</button>
                </div>
            </div>



    <div class="form-container">
            <h4 class="form-group-title">Emergency Contact Details</h4>

        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Relationship</th>
                <th>Name</th>
                <th>Address</th>
                <th>Contact Details</th>
                <!-- <th class="text-center">Action</th> -->
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($emergencyContactDetails))
              {
                $i=1;
                foreach ($emergencyContactDetails as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo $record->relationship ?></td>
                    <td><?php echo $record->relative_name ?></td>
                    <td><?php echo $record->relative_address ?></td>
                    <td> Mobile : <?php echo $record->relative_mobile ?>
                        <br>
                        Home : <?php echo $record->relative_home_phone ?>
                        <br>
                        Office : <?php echo $record->relative_office_phone ?>
                    </td>
                    <!-- <td class="text-center"><?php echo anchor('admission/student/delete_english?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td> -->
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>

    </div>
             
         </div> <!-- END col-12 -->  
        </div>


      </div>
    </div>

   </div> <!-- END row-->
   

            <!-- <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div> -->
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">

    function deleteVisaDetails(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/records/student/deleteVisaDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    window.location.reload();
               }
            });
    }
    
    $('select').select2();

    $( function()
    {
        $( ".datepicker" ).datepicker(
        {
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
        });
  });

  $(document).ready(function() {
        $("#form_profile").validate({
            rules: {
                qualification_level: {
                    required: true
                },
                degree_awarded: {
                    required: true
                },
                specialization: {
                    required: true
                },
                class_degree: {
                    required: true
                },
                result: {
                    required: true
                },
                year: {
                    required: true
                },
                medium: {
                    required: true
                },
                college_country: {
                    required: true
                },
                college_name: {
                    required: true
                },
                test: {
                    required: true
                },
                date: {
                    required: true
                },
                score: {
                    required: true
                },
                company_name: {
                    required: true
                },
                company_address: {
                    required: true
                },
                telephone: {
                    required: true
                },
                fax_num: {
                    required: true
                },
                designation: {
                    required: true
                },
                position: {
                    required: true
                },
                service_year: {
                    required: true
                },
                industry: {
                    required: true
                },
                job_desc: {
                    required: true
                },
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                id_type: {
                    required: true
                },
                passport_number: {
                    required: true
                },
                passport_expiry_date: {
                    required: true
                },
                phone: {
                    required: true
                },
                gender: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                martial_status: {
                    required: true
                },
                religion: {
                    required: true
                },
                nationality: {
                    required: true
                },
                id_race: {
                    required: true
                },
                email_id: {
                    required: true
                },
                nric: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                mail_address2: {
                    required: true
                },
                mailing_country: {
                    required: true
                },
                mailing_state: {
                    required: true
                },
                mailing_city: {
                    required: true
                },
                mailing_zipcode: {
                    required: true
                },
                permanent_address1: {
                    required: true
                },
                permanent_address2: {
                    required: true
                },
                permanent_country: {
                    required: true
                },
                permanent_state: {
                    required: true
                },
                permanent_city: {
                    required: true
                },
                permanent_zipcode: {
                    required: true
                },
                malaysian_visa: {
                    required: true
                },
                visa_expiry_date: {
                    required: true
                },
                visa_status: {
                    required: true
                },
                doc_name: {
                    required: true
                },
                remarks: {
                    required: true
                },
                visa_number: {
                    required: true
                },
                relationship: {
                    required: true
                },
                relative_name: {
                    required: true
                },
                relative_address: {
                    required: true
                },
                relative_mobile: {
                    required: true
                }
            },
            messages: {
                qualification_level: {
                    required: "<p class='error-text'>Qualification Level Required</p>",
                },
                degree_awarded: {
                    required: "<p class='error-text'>Degree Awarded Required</p>",
                },
                specialization: {
                    required: "<p class='error-text'>Specialization Required</p>",
                },
                class_degree: {
                    required: "<p class='error-text'>Degree Class Required</p>",
                },
                result: {
                    required: "<p class='error-text'>Result Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Requred</p>",
                },
                medium: {
                    required: "<p class='error-text'>Select Medium Of Study</p>",
                },
                college_country: {
                    required: "<p class='error-text'>Country Required</p>",
                },
                college_name: {
                    required: "<p class='error-text'>College Name Required</p>",
                },
                test: {
                    required: "<p class='error-text'>Test Name Required</p>",
                },
                date: {
                    required: "<p class='error-text'>Select Date</p>",
                },
                score: {
                    required: "<p class='error-text'>Score Required</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                company_address: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                telephone: {
                    required: "<p class='error-text'>Telephone No. Required</p>",
                },
                fax_num: {
                    required: "<p class='error-text'>Fax No. Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Of Rhe Job Required</p>",
                },
                service_year: {
                    required: "<p class='error-text'>No. Of Service Required</p>",
                },
                industry: {
                    required: "<p class='error-text'>Industry Required</p>",
                },
                job_desc: {
                    required: "<p class='error-text'>Job Description Required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_type: {
                    required: "<p class='error-text'>ID Type Required</p>",
                },
                passport_number: {
                    required: "<p class='error-text'>Passport NUmber Required</p>",
                },
                passport_expiry_date: {
                    required: "<p class='error-text'>Select Passport Expire Date</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Ntionality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Rece</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Main Id Required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Mailing Address 1 Required</p>",
                },
                mail_address2: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Mailing City Required</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Mailing Zipcode Required</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_address2: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Permanent City Required</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Permanent Zipcode Required</p>",
                },
                malaysian_visa: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                visa_expiry_date: {
                    required: "<p class='error-text'>Select Visa Expire Date</p>",
                },
                visa_status: {
                    required: "<p class='error-text'>Visa Status Required</p>",
                },
                doc_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remars Required</p>"
                },
                visa_number: {
                    required: "<p class='error-text'>Visa Number Required</p>"
                },
                relationship: {
                    required: "<p class='error-text'>Select Relationship</p>",
                },
                relative_name: {
                    required: "<p class='error-text'>Relaticve Name Required</p>",
                },
                relative_address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                relative_mobile: {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
  </script>









