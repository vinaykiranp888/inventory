<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Student Record</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>



       <div class="form-container">
          <h4 class="form-group-title">Student Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Student Name :</dt>
                              <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                          </dl>
                          <dl>
                              <dt>Student NRIC :</dt>
                              <dd><?php echo $getStudentData->nric ?></dd>
                          </dl>
                          <dl>
                              <dt>Student Email :</dt>
                              <dd><?php echo $getStudentData->email_id; ?></dd>
                          </dl>
                          <dl>
                              <dt>Mailing Address :</dt>
                              <dd></dd>
                          </dl>
                          <dl>
                              <dt></dt>
                              <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                          </dl>
                          <dl>
                              <dt></dt>
                              <dd><?php echo $getStudentData->mail_address2; ?></dd>
                          </dl>
                          <dl>
                              <dt>Mailing City :</dt>
                              <dd><?php echo $getStudentData->mailing_city; ?></dd>
                          </dl>
                          <dl>
                              <dt>Mailing Zipcode :</dt>
                              <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                          </dl>
                          <dl>
                              <dt>Company / Organisation :</dt>
                              <dd><?php

                              if($getStudentData->id_university != 1 && $getStudentData->id_university != 0)
                              {
                                  echo $getStudentData->partner_university_code , " - " . $getStudentData->partner_university_name;
                              }
                              elseif($getStudentData->id_company != '0')
                              {
                                  echo ucwords($getStudentData->company_name);
                              }
                              else
                              {
                                  echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                              }
                              
                              ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Nationality :</dt>
                              <dd><?php echo $getStudentData->nationality ?></dd>
                          </dl>                    
                          <dl>
                              <dt>Permanent Address :</dt>
                              <dd></dd>
                          </dl>
                          <dl>
                              <dt></dt>
                              <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                          </dl>
                          <dl>
                              <dt></dt>
                              <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                          </dl>
                          <dl>
                              <dt>Permanent City :</dt>
                              <dd><?php echo $getStudentData->permanent_city; ?></dd>
                          </dl>
                          <dl>
                              <dt>Permanent Zipcode :</dt>
                              <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                          </dl>
                          <dl>
                              <dt>Passport Number :</dt>
                              <dd><?php echo $getStudentData->passport_number; ?></dd>
                          </dl>
                          <dl>
                              <dt>Academic Advisor :</dt>
                              <dd><?php echo $getStudentData->advisor_icno . " - " . $getStudentData->advisor_name ; ?></dd>
                          </dl>
                      </div>
  
                  </div>
              </div>


       </div>


         



        <div class="form-container">
            <h4 class="form-group-title">Student Records Details</h4>
            <div class="m-auto text-center">
               <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>

            <div class="clearfix">
               <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                  <li role="presentation" class="active" ><a href="#tab_4" class="nav-link border rounded text-center"
                     aria-controls="tab_4" aria-selected="true"
                     role="tab" data-toggle="tab">Student Status</a>
                  </li>
                 <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center"
                     aria-controls="tab_2" role="tab" data-toggle="tab">Barring / Release</a>
                  </li>
                  <li role="presentation"><a href="#tab_3" class="nav-link border rounded text-center"
                     aria-controls="tab_3" role="tab" data-toggle="tab">Program Registration</a>
                  </li>
               </ul>


              <div class="tab-content offers-tab-content">


                  <div role="tabpanel" class="tab-pane" id="tab_1">
                     <div class="mt-4">
                        
                        <br>

                        <div class="form-container">
                           <h4 class="form-group-title">Personal Details</h4>
                           <div class='data-list'>
                              <div class='row'>
                                 <div class='col-sm-6'>
                                    <dl>
                                       <dt>Salutation :</dt>
                                       <dd><?php echo ucwords($studentDetails->salutation);?></dd>
                                    </dl>
                                    <dl>
                                       <dt>First Name :</dt>
                                       <dd><?php echo $studentDetails->first_name ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>ID Type :</dt>
                                       <dd><?php echo $studentDetails->id_type ?></dd>
                                    </dl>
                                    <!-- <dl>
                                       <dt>Passport Expiry Date :</dt>
                                       <dd><?php if($studentDetails->passport_expiry_date){ echo date('d-m-Y', strtotime($studentDetails->passport_expiry_date)); } ?></dd>
                                    </dl> -->
                                    <dl>
                                       <dt>Date Of Birth :</dt>
                                       <dd><?php if($studentDetails->date_of_birth){ echo date('d-m-Y', strtotime($studentDetails->date_of_birth)); } ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Maritual Status :</dt>
                                       <dd><?php echo $studentDetails->id_type ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Nationality :</dt>
                                       <dd><?php echo $studentDetails->nationality ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Race :</dt>
                                       <dd><?php echo $studentDetails->race ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Email ID :</dt>
                                       <dd><?php echo $studentDetails->email_id ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Phone Number :</dt>
                                       <dd><?php echo $studentDetails->phone ?></dd>
                                    </dl>
                                    <dl>
                                       <dt style="color: black"><br><b>Permanent Mailing Address :</b></dt>
                                       <dd></dd>
                                    </dl>
                                    <dl>
                                       <dt>Address :</dt>
                                       <dd><?php echo $studentDetails->permanent_address1 ?></dd>
                                    </dl>
                                    <dl>
                                       <dt></dt>
                                       <dd><?php echo $studentDetails->permanent_address2 ?></dd>
                                    </dl>
                                    <dl>
                                       <dt style="color: black"><br><br><br><b>Correspondance Mailing Address :</b></dt>
                                       <dd></dd>
                                    </dl>
                                    <dl>
                                       <dt>Address :</dt>
                                       <dd><?php echo $studentDetails->mail_address2 ?></dd>
                                    </dl>
                                    <dl>
                                       <dt></dt>
                                       <dd><?php echo $studentDetails->mail_address2 ?></dd>
                                    </dl>
                                 </div>
                                 <div class='col-sm-6'>
                                    <dl>
                                       <dt>Program Name :</dt>
                                       <dd><?php echo ucwords($studentDetails->programme_code . " - " . $studentDetails->programme_name);?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Last Name :</dt>
                                       <dd><?php echo $studentDetails->last_name; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>ID No. :</dt>
                                       <dd><?php echo $studentDetails->passport; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Gender :</dt>
                                       <dd><?php echo $studentDetails->gender; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Age :</dt>
                                       <dd><?php if($studentDetails->date_of_birth){
                                          echo date('Y') - date('Y', strtotime($studentDetails->date_of_birth)); } ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Religion :</dt>
                                       <dd><?php echo $studentDetails->religion; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Contact Email ID :</dt>
                                       <dd><?php echo $studentDetails->contact_email ?></dd>
                                    </dl>
                                    <dl>
                                       <dt> <br><br></dt>
                                       <dd> </dd>
                                    </dl>
                                    <dl>
                                       <dt>Postal/ Zipcode :</dt>
                                       <dd><?php echo $studentDetails->permanent_zipcode ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Country :</dt>
                                       <dd><?php echo $studentDetails->permanent_country ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>State :</dt>
                                       <dd><?php echo $studentDetails->permanent_state ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>City :</dt>
                                       <dd><?php echo $studentDetails->permanent_city ?></dd>
                                    </dl>
                                    <dl>
                                       <dt> <br><br><br></dt>
                                       <dd> </dd>
                                    </dl>
                                    <dl>
                                       <dt>Postal/ Zipcode :</dt>
                                       <dd><?php echo $studentDetails->mailing_zipcode ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Country :</dt>
                                       <dd><?php echo $studentDetails->mailing_country ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>State :</dt>
                                       <dd><?php echo $studentDetails->mailing_state ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>City :</dt>
                                       <dd><?php echo $studentDetails->mailing_city ?></dd>
                                    </dl>
                                 </div>
                              </div>
                           </div>
                        </div>


                        

                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_2">
                     <div class="mt-4">

                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Type</th>
                                    <th>ID Type</th>
                                    <th>Reason</th>
                                    <th>Date</th>
                                    <th>User</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($barrReleaseByStudentId)) {
                                        $i=1;
                                        foreach ($barrReleaseByStudentId as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->type ?></td>
                                    <td><?php echo $record->barring_code . " - " . $record->barring_name ?></td>
                                    <td><?php echo $record->reason ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_3">
                     <div class="mt-4">



                      <div class="form-container">
                        <h4 class="form-group-title">Program Registration Details</h4>



                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Programme Code</th>
                                    <th>Programme Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Registered On</th>
                                    <th>Invoice</th>
                                    <th>Payment Status</th>
                                    <th>Marks Obtained</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($studentHasProgramme)) {
                                        $i=1;
                                        foreach ($studentHasProgramme as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->programme_code; ?></td>
                                    <td><?php echo $record->programme_name; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->start_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->end_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->invoice_number ?></td>
                                    <td><?php
                                    if($record->status == '0')
                                    {
                                      echo 'Not Paid';
                                    }
                                    else
                                    {
                                      echo 'Paid';
                                    }
                                    ?></td>
                                    <td><?php if($record->id_marks_entry == 0 && $record->id_marks_adjustment == 0)
                                    {
                                      echo ' - ';
                                    }else
                                    {
                                      echo $record->marks;
                                    } 
                                       ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                          

                      </div>





                        
                        <!-- <?php
                           if(!empty($courseRegisteredLandscapeFBySemester))
                           {
                               ?> <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                 <?php
                                    if (!empty($courseRegisteredLandscapeFBySemester))
                                    {
                                        foreach ($courseRegisteredLandscapeFBySemester as $record)
                                        {
                                    ?>
                                 <td>
                                    <h3><?php echo $record->code . " - " . $record->name; ?></h3>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                
                                 <div class="custom-table" id="printReceipt">
                                    <table class="table" id="list-table">
                                       <thead>
                                          <tr>
                                             <th>Sl. No</th>
                                             <th>Course Code</th>
                                             <th>Course name</th>
                                             <th>Credit Hours</th>
                                             <th>Status</th>                                        
                                             <th>Withdraw</th>                                        
                                             <th>Grade</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             if (!empty($record->course)) {
                                                 $i=1;
                                                 foreach ($record->course as $record_course) {
                                             ?>
                                          <tr>
                                             <td><?php echo $i ?></td>
                                             <td><?php echo $record_course->course_code ?></td>
                                             <td><?php echo $record_course->course_name ?></td>
                                             <td><?php echo $record_course->credit_hours ?></td>

                                             <td>
                                                <?php if($record_course->is_result_announced > 0)
                                                   {
                                                       echo $record_course->total_result;
                                                   }
                                                   elseif($record_course->is_exam_registered > 0)
                                                   {
                                                       echo 'Exam Registered';
                                                   }
                                                   else{
                                                       echo "Exam Not Registered";
                                                   } ?>
                                             </td>
                                             <td><?php
                                             if($record_course->is_bulk_withdraw > 0)
                                                   {
                                                       echo 'Withdraw';
                                                   }
                                              ?></td>
                                             <td><?php echo $record_course->grade ?></td>

                                          </tr>
                                          <?php
                                             $i++;
                                                 }
                                             }
                                             ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <?php
                                    }
                                    }
                                    ?>
                                 </td>
                              </tr>
                              </tbody>
                           </table>
                        </div><?php
                         }
                       ?> -->


                     </div>

                  </div>


                  <div role="tabpanel" class="tab-pane active" id="tab_4">
                     <div class="mt-4">
                        <div class="form-container">
                           <h4 class="form-group-title">Student Status</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Date</th>
                                       <th>Reason</th>
                                       <th>By</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    <tr>
                                       <td><?php echo '1'; ?></td>
                                       <td><?php 
                                       if($studentStatus->id_applicant > 0)
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                       else
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                         ?>     
                                       </td>
                                       <td> </td>
                                       <td><?php
                                       if($studentStatus->added_by_partner_university > 0)
                                       {
                                        echo "Partner University";
                                       }
                                       else
                                       {
                                          echo 'Administrator';
                                       } ?></td>
                                       <td>Active</td>
                                    </tr>
                                    <?php
                                    if(!empty($applyChangeStatusListByStudentId))
                                    {
                                      $j=1;
                                        foreach ($applyChangeStatusListByStudentId as $record)
                                        {
                                    ?>
                                   <tr>
                                      <td><?php echo $j+1 ?></td>
                                      <td><?php echo date('d-m-Y H:i:s',strtotime($record->created_dt_tm)); ?></td>
                                      <td><?php echo $record->change_status ?></td>
                                      <td><?php echo $record->created_by ?></td>
                                      <td>
                                      <?php 
                                         if($record->status == 0)
                                         {
                                             echo "Pending"; 
                                         }if($record->status == 1)
                                         {
                                             echo "Approved"; 
                                         }if($record->status == 2)
                                         {
                                             echo "Rejected"; 
                                         }?>
                                         
                                      </td>
                                   </tr>
                                   <?php
                                      $j++;
                                      }
                                    }
                                    ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  
                  </div>





                  <div role="tabpanel" class="tab-pane" id="tab_5">
                     <div class="mt-4">
                        <div class="custom-table" id="printInvoice">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Advisor Name</th>
                                    <th>Date</th>
                                    <th>Updated By</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($advisorTaggingDetails)) {
                                        $i=1;
                                        foreach ($advisorTaggingDetails as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->ic_no . " - " . $record->advisor_name  ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  
                  </div>

        
              </div>

            </div>

        </div>


      <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
      </footer>

  </div> 
</div>
<script type="text/javascript">  
   
    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               note:
               {
                   required: true
               }         
           },
           messages:
           {
               note:
               {
                   required: "<p class='error-text'>Note Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
   
   
</script>