<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplyChangeProgramme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('apply_change_programme_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('apply_change_programme.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->apply_change_programme_model->programmeList();
            $data['studentList'] = $this->apply_change_programme_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            
            $data['searchParameters'] = $formData;
            $data['applyChangeProgrammeList'] = $this->apply_change_programme_model->applyChangeProgrammeListSearch($formData);
            // echo "<Pre>"; print_r($data['studentList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Apply Change Program List';
            $this->loadViews("apply_change_programme/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('apply_change_programme.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_new_programme = $this->security->xss_clean($this->input->post('id_new_programme'));
                $id_new_intake = $this->security->xss_clean($this->input->post('id_new_intake'));
                $id_new_semester = $this->security->xss_clean($this->input->post('id_new_semester'));
                $id_new_program_scheme = $this->security->xss_clean($this->input->post('id_new_program_scheme'));
                $id_new_program_has_scheme = $this->security->xss_clean($this->input->post('id_new_program_has_scheme'));
                $fee = $this->security->xss_clean($this->input->post('fee'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_new_programme' => $id_new_programme,
                    'id_new_intake' => $id_new_intake,
                    'id_new_program_scheme' => $id_new_program_scheme, // Its a Learning Mode After A Flow Change
                    'id_new_program_has_scheme' => $id_new_program_has_scheme, // Its a Programme Scheme After A Flow Change
                    'id_new_semester' => $id_new_semester,
                    'fee' => $fee,
                    'reason' => $reason,
                    'status' => '0',
                    'created_by' => $id_user
                );
            // echo "<Pre>";print_r($data);exit;
                $result = $this->apply_change_programme_model->addNewApplyChangeProgramme($data);

                $check_apply_status = $this->apply_change_programme_model->getFeeStructureActivityType('CHANGE PROGRAM','Application Level',$id_new_programme);
                if($check_apply_status)
                {
                    $data['add'] = 1;
                    $this->apply_change_programme_model->generateMainInvoice($data,$result);
                }

                redirect('/records/applyChangeProgramme/list');
            }
            // $data['programmeList'] = $this->apply_change_programme_model->programmeList();
            $data['studentList'] = $this->apply_change_programme_model->studentList();
            $data['programmeList'] = $this->apply_change_programme_model->programmeListByStatus('1');
            $data['intakeList'] = $this->apply_change_programme_model->intakeListByStatus('1');
            $data['semesterList'] = $this->apply_change_programme_model->semesterListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add New Apply Change Program';
            $this->loadViews("apply_change_programme/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('apply_change_programme.view') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/applyChangeProgramme/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_new_programme = $this->security->xss_clean($this->input->post('id_new_programme'));
                $id_new_intake = $this->security->xss_clean($this->input->post('id_new_intake'));
                $id_new_semester = $this->security->xss_clean($this->input->post('id_new_semester'));
                $id_new_program_scheme = $this->security->xss_clean($this->input->post('id_new_program_scheme'));
                $id_new_program_has_scheme = $this->security->xss_clean($this->input->post('id_new_program_has_scheme'));
                $fee = $this->security->xss_clean($this->input->post('fee'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_new_programme' => $id_new_programme,
                    'id_new_intake' => $id_new_intake,
                    'id_new_semester' => $id_new_semester,
                    'id_new_program_scheme' => $id_new_program_scheme,
                    'id_new_program_has_scheme' => $id_new_program_has_scheme, // Its a Programme Scheme After A Flow Change
                    'fee' => $fee,
                    'reason' => $reason,
                    'status' => $status,
                    'created_by' => $id_user
                );
                // echo "<Pre>";print_r($data);exit();
                $result = $this->apply_change_programme_model->editApplyChangeProgramme($data, $id);
                redirect('/records/applyChangeProgramme/list');
            }

            $data['programmeList'] = $this->apply_change_programme_model->programmeList();
            $data['studentList'] = $this->apply_change_programme_model->studentList();
            $data['applyChangeProgramme'] = $this->apply_change_programme_model->getApplyChangeProgramme($id);
            $data['intakeList'] = $this->apply_change_programme_model->intakeListByStatus('1');
            $data['semesterList'] = $this->apply_change_programme_model->semesterListByStatus('1');

            $id_student = $data['applyChangeProgramme']->id_student;

            $data['studentDetails'] = $this->apply_change_programme_model->getStudentByStudentId($id_student);

            $nationality = $data['studentDetails']->nationality;
            $currency = '';
            
            // $currency = $data['applyChangeProgramme']->id_currency;

            // if($nationality == 'Malaysian')
            // {
            //     $currency = 'MYR';
            // }elseif($nationality == 'Other')
            // {
            //     $currency = 'USD';
            // }



            $tempData['id_programme'] = $data['applyChangeProgramme']->id_new_programme;
            $tempData['id_intake'] = $data['applyChangeProgramme']->id_new_intake;
            $tempData['id_program_scheme'] = $data['applyChangeProgramme']->id_new_program_scheme;
            $tempData['currency'] = $currency;

            $data['feeStructure'] = $this->apply_change_programme_model->getFeeByProgrammeNIntake($tempData);
            $data['programScheme'] = $this->apply_change_programme_model->getProgramSchemeByProgramId($tempData['id_programme']);

            // echo "<Pre>";print_r($data['feeStructure']);exit;
            $this->global['pageTitle'] = 'Inventory Management : View Apply Change Program';
            $this->loadViews("apply_change_programme/edit", $this->global, $data, NULL);
        }
    }



    function approval_list()
    {
        if ($this->checkAccess('apply_change_programme.approval_list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {

           if($this->input->post())
            {
             $resultprint = $this->input->post();
             // echo "<Pre>"; print_r($resultprint['button']);exit;

             switch ($resultprint['button'])
             {
                 case 'Approve':
                    for($i=0;$i<count($resultprint['approval']);$i++)
                    {

                         $id = $resultprint['approval'][$i];
                         $data = array(
                            'status' => 1,
                        );
                        $result = $this->apply_change_programme_model->editApplyProgrammeList($data, $id);

                        if($result)
                        {
                            $change_program = $this->apply_change_programme_model->getApplyChangeProgramme($id);

                            $id_program = $change_program->id_new_programme;

                            $check_apply_status = $this->apply_change_programme_model->getFeeStructureActivityType('CHANGE PROGRAM','Approval Level',$id_program);

                            // echo "<Pre>"; print_r($id_program);exit;
                            if($check_apply_status)
                            {
                                $data['add'] = 0;
                                $data['id_student'] = $change_program->id_student;
                                $this->apply_change_programme_model->generateMainInvoice($data,$id);
                            }
                        }

                        // if($result)
                        // {
                            // $this->apply_change_programme_model->generateMainInvoiceByChangeProgram($id);
                        // }


                    }
                        redirect('/records/applyChangeProgramme/approval_list');
                     break;


                     case 'search':

                    $data['programmeList'] = $this->apply_change_programme_model->programmeList();
                    $data['studentList'] = $this->apply_change_programme_model->studentList();

                    $formData['name'] = $this->security->xss_clean($this->input->post('name'));
                    $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
                    $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
                    
                    $data['searchParameters'] = $formData;
                    $data['applyChangeProgrammeApprovalList'] = $this->apply_change_programme_model->applyChangeProgrammeListForApprovalSearch($formData);
                     
                     break;
                 
                 default:
                     break;
             }
                
            }
            $data['programmeList'] = $this->apply_change_programme_model->programmeList();
            $data['studentList'] = $this->apply_change_programme_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            
            $data['searchParameters'] = $formData;
            $data['applyChangeProgrammeApprovalList'] = $this->apply_change_programme_model->applyChangeProgrammeListForApprovalSearch($formData);
            // echo "<Pre>"; print_r($data['applyChangeStatusApprovalList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Apply Change Program Approval';
            $this->loadViews("apply_change_programme/approval_list", $this->global, $data, NULL);
        }
    }




    function view($id)
    {

        if ($this->checkAccess('apply_change_programme.approve') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        }
        else
        {

            if ($id == null)
            {
                redirect('/records/applyChangeProgramme/list');
            }
            if($this->input->post())
            {

                // echo "<Pre>"; print_r($this->input->post());exit;


                $status = $this->security->xss_clean($this->input->post('status'));
                $reject_reason = $this->security->xss_clean($this->input->post('reason'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));

                $data = array(
                    'status' => $status,
                    'reject_reason' => $reject_reason,
                );

                $result = $this->apply_change_programme_model->editApplyProgrammeList($data, $id);


                if($status == 1)
                {
                    if($result)
                    {

                        // $student_data = $this->apply_change_programme_model->getStudent($id_student);

                        $change_program = $this->apply_change_programme_model->getApplyChangeProgramme($id);

                        $id_program_scheme = $change_program->id_new_program_scheme;
                        $id_program_has_scheme = $change_program->id_new_program_has_scheme;
                        $id_intake = $change_program->id_new_intake;
                        $id_program = $change_program->id_new_programme;
                        
        

                        // echo "<Pre>"; print_r($change_program);exit;
                        


                        $student_data = array(
                            'id_intake' => $id_intake,
                            'id_program' => $id_program,
                            'id_program_scheme' => $id_program_scheme,
                            'id_program_has_scheme' => $id_program_has_scheme
                        );

                        $program = $this->apply_change_programme_model->getProgrammeById($id_program);

                        if($program)
                        {
                            $program_mode = $program->mode;

                            if($program_mode == 1)
                            {
                                $program_landscape = $this->apply_change_programme_model->getProgramLandscapeByLearningMode($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);

                                if($program_landscape)
                                {
                                    $student_data['id_program_landscape'] = $program_landscape->id;
                                }    
                            }
                            elseif($program_mode == 0)
                            {
                                $program_landscape = $this->apply_change_programme_model->getProgramLandscapeByMultipleLearningMode($id_intake, $id_program, $id_program_scheme,$id_program_has_scheme);

                                if($program_landscape)
                                {
                                    $student_data['id_program_landscape'] = $program_landscape->id_program_landscape;
                                }
                            }
                        }


                        $updated_student = $this->apply_change_programme_model->editStudent($student_data, $id_student);



                        // echo "<Pre>"; print_r($student_data);exit;




                        $check_apply_status = $this->apply_change_programme_model->getFeeStructureActivityType('CHANGE PROGRAM','Approval Level',$id_program);

                        // echo "<Pre>"; print_r($id_program);exit;
                        if($check_apply_status)
                        {
                            $data['add'] = 0;
                            $data['id_student'] = $change_program->id_student;
                            $this->apply_change_programme_model->generateMainInvoice($data,$id);
                        }
                    }

                }


                redirect('/records/applyChangeProgramme/approval_list');
            }

            $data['programmeList'] = $this->apply_change_programme_model->programmeList();
            $data['studentList'] = $this->apply_change_programme_model->studentList();
            $data['applyChangeProgramme'] = $this->apply_change_programme_model->getApplyChangeProgramme($id);
            $data['intakeList'] = $this->apply_change_programme_model->intakeListByStatus('1');
            $data['semesterList'] = $this->apply_change_programme_model->semesterListByStatus('1');



            $id_student = $data['applyChangeProgramme']->id_student;

            $data['studentDetails'] = $this->apply_change_programme_model->getStudentByStudentId($id_student);
            $nationality = $data['studentDetails']->nationality;

            if($nationality == '1')
            {
                $currency = 'MYR';
            }elseif($nationality != '')
            {
                $currency = 'USD';
            }
            
            // echo "<Pre>"; print_r($data['applyChangeProgramme']);exit;

            $tempData['id_programme'] = $data['applyChangeProgramme']->id_new_programme;
            $tempData['id_intake'] = $data['applyChangeProgramme']->id_new_intake;
            $tempData['id_program_scheme'] = $data['applyChangeProgramme']->id_new_program_scheme;
            $tempData['currency'] = $currency;

            $data['feeStructure'] = $this->apply_change_programme_model->getFeeByProgrammeNIntake($tempData);
            $data['programScheme'] = $this->apply_change_programme_model->getProgramSchemeByProgramId($tempData['id_programme']);

            $this->global['pageTitle'] = 'Inventory Management : View Apply Change Program';
            $this->loadViews("apply_change_programme/view", $this->global, $data, NULL);
        }
    }





    function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->apply_change_programme_model->getStudentByProgrammeId($id);
            $programme_data = $this->apply_change_programme_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $nric = $results[$i]->nric;
            $table.="<option value=".$id.">".$nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            

            $table .= "
            <div type='hidden' style='display: none;'>
                    <tr>
                        <td><input type='hidden' id='programme_name' name='programme_name' value='$programme_name' />$programme_name</td>  
                        <td><input type='hidden' id='programme_code' name='programme_code' value='$programme_code' />$programme_code</td>  
                        <td><input type='hidden' id='total_cr_hrs' name='total_cr_hrs' value='$total_cr_hrs' />$total_cr_hrs</td>  
                        <td><input type='hidden' id='graduate_studies' name='graduate_studies' value='$graduate_studies' />$graduate_studies</td>
                    </tr>
            </div>";

            // $d['table'] = $table;
            // $d['view'] = $view;

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->apply_change_programme_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;

            $nationality = $student_data->nationality;

            if($nationality == '1')
            {
                $currency = 'MYR';
            }elseif($nationality != '')
            {
                $currency = 'USD';
            }


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' />
                                    <input type='hidden' name='currency' id='currency' value='$currency' />
                                    $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";

                $table1  ="






            <h4 style='text-align: center;'><b>Student Details</b></h4>
            <table align='center' style='border: 1px solid;border-radius: 12px; width: 90%;'>
                <tr>
                    <td colspan='4'></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name :</th>
                    <td style='text-align: left;'>$student_name</td>
                    <th style='text-align: center;'>Intake :</th>
                    <td style='text-align: left;'> <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email :</th>
                    <td style='text-align: left;'>$email</td>
                    <th style='text-align: center;'>Programme :</th>
                    <td style='text-align: left;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC :</th>
                    <td style='text-align: left;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function getProgramSchemeByProgramId($id_program)
    {
         $intake_data = $this->apply_change_programme_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_new_program_scheme' id='id_new_program_scheme' class='form-control' onchange='getFeeByProgrammeNIntake()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }


    function getFeeByProgrammeNIntake()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $id_programme = $tempData['id_programme'];
        // $id_intake = $tempData['id_intake'];
        // $id_program_scheme = $tempData['id_program_scheme'];
        // echo "<Pre>";print_r($tempData);exit();
        
        $data = $this->apply_change_programme_model->getFeeByProgrammeNIntake($tempData);



         $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Currency</th>
                    <th>Amount</th>
                </tr>";

                    $total_amount = 0;
                    $i=1;
                    foreach ($data as $value)
                    {
                        $id = $value->id;
                        $fee_setup = $value->fee_setup;
                        $fee_code = $value->fee_code;
                        $currency = $value->currency;
                        $amount = $value->amount;
                        $fee = $fee_code . " - " . $fee_code;
                        
                    
                    $table .= "
                        <tr>
                            <td>$i</td>
                            <td>$fee</td>
                            <td>$currency</td>
                            <td>$amount</td>
                        </tr>";
                        $total_amount = $total_amount + $value->amount;
                        $i++;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='amount' value='$total_amount' />$total_amount</td>
                        </tr>";

        $table.= "</table>";

        // $table="
        // <input type='number' hidden='hidden' class='form-control' id='fee' name='fee' readonly='readonly' value='$total_amount'>

        // ";


        
        echo $table;
            // exit;      
    }

    function getProgrammeByEducationLevelIdByStudentId($id_student)
    {
        // echo "<Pre>"; print_r($id_education_level);exit;
        $student_data = $this->apply_change_programme_model->getStudentByStudentId($id_student);
        if($student_data)
        {
            $id_education_level = $student_data->id_degree_type;

            $intake_data = $this->apply_change_programme_model->getProgrammeByEducationLevelId($id_education_level);

             $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_new_programme' id='id_new_programme' class='form-control' onchange='getIntakeByProgramme(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
        }
    }



    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->apply_change_programme_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_new_intake' id='id_new_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getSchemeByProgramId($id_program)
    {
            // echo "<Pre>"; print_r($id_program);exit;
        // It's Actual Scheme What We Required
         $intake_data = $this->apply_change_programme_model->getSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_new_program_has_scheme' id='id_new_program_has_scheme' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $code = $intake_data[$i]->code;
            $name = $intake_data[$i]->description;

            $table.="<option value=".$id.">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function checkFeeStructure()
    {
        $id_session = $this->session->session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>"; print_r($tempData);exit;

        $result = $this->apply_change_programme_model->checkFeeStructure($tempData);
        
        echo $result;exit;
    
    }
}
