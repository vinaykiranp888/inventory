<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ChangeStatus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('change_status_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('change_status.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['changeStatusList'] = $this->change_status_model->changeStatusListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Change Status List';
            $this->loadViews("change_status/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('change_status.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->change_status_model->addNewChangeStatus($data);
                redirect('/records/changeStatus/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Change Status';
            $this->loadViews("change_status/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('change_status.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/records/changeStatus/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->change_status_model->editChangeStatus($data,$id);
                redirect('/records/changeStatus/list');
            }
            $data['changeStatus'] = $this->change_status_model->getChangeStatus($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Change Status';
            $this->loadViews("change_status/edit", $this->global, $data, NULL);
        }
    }
}
