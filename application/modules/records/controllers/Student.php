<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Student extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->student_model->intakeList();
            $data['programList'] = $this->student_model->programList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['last_name'] = $this->security->xss_clean($this->input->post('last_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            // $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
 
            $data['applicantList'] = $this->student_model->applicantList($formData);
            $data['searchParam'] = $formData;



            $this->global['pageTitle'] = 'Inventory Management : Student';
            // echo "<Pre>";print_r($data['applicantList']);exit;
            $this->loadViews("student/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('student.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            if ($id == null)
            {
                redirect('/records/student/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

                $btn_submit = $formData['btn_submit'];
                $id_student = $id;


                switch ($btn_submit)
                {
                    case '1':

                    $qualification_level = $this->security->xss_clean($this->input->post('qualification_level'));
                    $degree_awarded = $this->security->xss_clean($this->input->post('degree_awarded'));
                    $specialization = $this->security->xss_clean($this->input->post('specialization'));
                    $class_degree = $this->security->xss_clean($this->input->post('class_degree'));
                    $result = $this->security->xss_clean($this->input->post('result'));
                    $year = $this->security->xss_clean($this->input->post('year'));
                    $medium = $this->security->xss_clean($this->input->post('medium'));
                    $college_country = $this->security->xss_clean($this->input->post('college_country'));
                    $college_name = $this->security->xss_clean($this->input->post('college_name'));
                    $certificate = $this->security->xss_clean($this->input->post('certificate'));
                    $transcript = $this->security->xss_clean($this->input->post('transcript'));


                    $data = array(
                        'id_student' => $id_student,
                        'qualification_level' => $qualification_level,
                        'degree_awarded' => $degree_awarded,
                        'specialization' => $specialization,
                        'class_degree' => $class_degree,
                        'result' => $result,
                        'year' => $year,
                        'medium' => $medium,
                        'college_country' => $college_country,
                        'college_name' => $college_name,
                        'certificate' => $certificate,
                        'transcript' => $transcript
                    );
                    if ($qualification_level != "") {
                        $result = $this->student_model->addExamDetails($data);
                    }
                            
                    break;


                    case '2':
 
                    $test = $this->security->xss_clean($this->input->post('test'));
                    $date = $this->security->xss_clean($this->input->post('date'));
                    $score = $this->security->xss_clean($this->input->post('score'));
                    $file = $this->security->xss_clean($this->input->post('file'));

                    $data = array(
                        'id_student' => $id_student,
                        'test' => $test,
                        'date' => date("Y-m-d", strtotime($date)),
                        'score' => $score,
                        'file' => $file
                    );
                    if ($test != "") {
                        $result = $this->student_model->addProficiencyDetails($data);
                    }

                    break;


                    case '3':


                    $company_name = $this->security->xss_clean($this->input->post('company_name'));
                    $company_address = $this->security->xss_clean($this->input->post('company_address'));
                    $telephone = $this->security->xss_clean($this->input->post('telephone'));
                    $fax_num = $this->security->xss_clean($this->input->post('fax_num'));
                    $designation = $this->security->xss_clean($this->input->post('designation'));
                    $position = $this->security->xss_clean($this->input->post('position'));
                    $service_year = $this->security->xss_clean($this->input->post('service_year'));
                    $industry = $this->security->xss_clean($this->input->post('industry'));
                    $job_desc = $this->security->xss_clean($this->input->post('job_desc'));
                    $employment_letter = $this->security->xss_clean($this->input->post('employment_letter'));

                    $data = array(
                        'id_student' => $id_student,
                        'company_name' => $company_name,
                        'company_address' => $company_address,
                        'telephone' => $telephone,
                        'fax_num' => $fax_num,
                        'designation' => $designation,
                        'position' => $position,
                        'service_year' => $service_year,
                        'industry' => $industry,
                        'job_desc' => $job_desc,
                        'employment_letter' => $employment_letter
                    );
                    if ($company_name != "") {
                        $result = $this->student_model->addEmploymentDetails($data);
                    }



                    break;



                    case '4':


                    $salutation = $this->security->xss_clean($this->input->post('salutation'));
                    $first_name = $this->security->xss_clean($this->input->post('first_name'));
                    $last_name = $this->security->xss_clean($this->input->post('last_name'));
                    $id_type = $this->security->xss_clean($this->input->post('id_type'));
                    $id_number = $this->security->xss_clean($this->input->post('id_number'));
                    $passport_expiry_date = $this->security->xss_clean($this->input->post('passport_expiry_date'));
                    $gender = $this->security->xss_clean($this->input->post('gender'));
                    $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
                    $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
                    $religion = $this->security->xss_clean($this->input->post('religion'));
                    $nationality = $this->security->xss_clean($this->input->post('nationality'));
                    $nationality_type = $this->security->xss_clean($this->input->post('nationality_type'));
                    $race = $this->security->xss_clean($this->input->post('id_race'));
                    $email_id = $this->security->xss_clean($this->input->post('email_id'));
                    $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
                    $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
                    $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
                    $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
                    $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
                    $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
                    $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
                    $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
                    $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
                    $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
                    $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
                    $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
                    $passport_number = $this->security->xss_clean($this->input->post('passport_number'));
                    $phone = $this->security->xss_clean($this->input->post('phone'));
                    $nric = $this->security->xss_clean($this->input->post('nric'));


                    $salutationInfo = $this->student_model->getSalutation($salutation);


                    $data = array(
                        'id_student' => $id_student,
                        'full_name' => $salutationInfo->name.". ".$first_name." ".$last_name,
                        'salutation' => $salutation,
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'id_type' => $id_type,
                        'passport_number' => $passport_number,
                        'passport_expiry_date' => $passport_expiry_date,
                        'gender' => $gender,
                        'date_of_birth' => date('Y-m-d',strtotime($date_of_birth)),
                        'martial_status' => $martial_status,
                        'religion' => $religion,
                        'nationality' => $nationality,
                        'phone' => $phone,
                        'nric' => $nric,
                        'nationality_type' => $nationality_type,
                        'id_race' => $race,
                        // 'email_id' => $email_id,
                        'mail_address1' => $mail_address1,
                        'mail_address2' => $mail_address2,
                        'permanent_address1' => $permanent_address1,
                        'permanent_address2' => $permanent_address2,
                        'mailing_zipcode' => $mailing_zipcode,
                        'permanent_zipcode' => $permanent_zipcode,
                        'mailing_country' => $mailing_country,
                        'permanent_country' => $permanent_country,
                        'mailing_state' => $mailing_state,
                        'permanent_state' => $permanent_state,
                        'mailing_city' => $mailing_city,
                        'permanent_city' => $permanent_city
                    );

                    // $checkDuplicate = $this->student_model->checkDuplicateStudent($data,$id_student);
                    // if($checkDuplicate)
                    // {
                    //     echo "Entered Profile E-Mail / Phone / NRIC Already Exist";exit();
                    // }

                    $updated_student = $this->student_model->updateStudentData($data);

                    if ($first_name != "") {
                        $result = $this->student_model->editProfileDetails($data, $id_student);
                    }


                    break;
                    


                    case '5':



                    $malaysian_visa = $this->security->xss_clean($this->input->post('malaysian_visa'));
                    $visa_expiry_date = $this->security->xss_clean($this->input->post('visa_expiry_date'));
                    $visa_status = $this->security->xss_clean($this->input->post('visa_status'));
                    $visa_number = $this->security->xss_clean($this->input->post('visa_number'));


                    $data = array(
                        'id_student' => $id_student,
                        'malaysian_visa' => $malaysian_visa,
                        'visa_number' => $visa_number,
                        'visa_expiry_date' => date('Y-m-d',strtotime($visa_expiry_date)),
                        'visa_status' => $visa_status
                    );

                    // if ($malaysian_visa != "") {
                    //     $result = $this->student_model->addVisaDetails($data);
                    // }

                    $result = $this->student_model->addVisaDetails($data);

                    break;




                    case '6':



                    $doc_name = $this->security->xss_clean($this->input->post('doc_name'));
                    $doc_file = $this->security->xss_clean($this->input->post('doc_file'));
                    $remarks = $this->security->xss_clean($this->input->post('remarks'));

                    $data = array(
                        'id_student' => $id_student,
                        'doc_name' => $doc_name,
                        'doc_file' => $doc_file,
                        'remarks' => $remarks
                    );
                    if ($doc_name != "" && $remarks != "")
                    {
                        $result = $this->student_model->addOtherDocuments($data);
                    }


                        break;


                    case '7':


                    $relationship = $this->security->xss_clean($this->input->post('relationship'));
                    $relative_name = $this->security->xss_clean($this->input->post('relative_name'));
                    $relative_address = $this->security->xss_clean($this->input->post('relative_address'));
                    $relative_mobile = $this->security->xss_clean($this->input->post('relative_mobile'));
                    $relative_home_phone = $this->security->xss_clean($this->input->post('relative_home_phone'));
                    $relative_office_phone = $this->security->xss_clean($this->input->post('relative_office_phone'));




                    $emergency_contact_data = array(
                        'id_student' => $id_student,
                        'relationship' => $relationship,
                        'relative_name' => $relative_name,
                        'relative_address' => $relative_address,
                        'relative_mobile' => $relative_mobile,
                        'relative_home_phone' => $relative_home_phone,
                        'relative_office_phone' => $relative_office_phone
                    );



                    if($relationship != '')
                    {

                        $result = $this->student_model->addEmergencyContactDetails($emergency_contact_data);
                    }

                    break;
                    


                    default:
                   break;
                }
                redirect($_SERVER['HTTP_REFERER']);
            }

            $data['countryList'] = $this->student_model->countryListByStatus('1');
            $data['stateList'] = $this->student_model->stateListByStatus('1');

            // $data['student'] = $this->student_model->getStudentDetails($id);
            $data['getStudentData'] = $this->student_model->getStudentByStudentId($id);

            $data['examDetails'] = $this->student_model->getExamDetails($id);
            $data['proficiencyDetails'] = $this->student_model->getProficiencyDetails($id);
            $data['employmentDetails'] = $this->student_model->getEmploymentDetails($id);
            $data['profileDetails'] = $this->student_model->getProfileDetails($id);
            $data['visaDetails'] = $this->student_model->getVisaDetails($id);
            $data['otherDocuments'] = $this->student_model->getOtherDocuments($id);
            $data['courseRegistrationList'] = $this->student_model->courseRegistrationList($id);
            $data['emergencyContactDetails'] = $this->student_model->emergencyContactDetails($id);
            $data['raceList'] = $this->student_model->raceListByStatus('1');
            $data['religionList'] = $this->student_model->religionListByStatus('1');
            $data['salutationList'] = $this->student_model->salutationListByStatus('1');
            $data['organisationDetails'] = $this->student_model->getOrganisation();


            // echo "<Pre>";print_r($data['getStudentData']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Edit Student';
            $this->loadViews("student/edit", $this->global, $data, NULL);
        }
    }

    function delete_exam()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteExamDetails($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_english()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteProficiencyDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_employment()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteEmploymentDetails($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function delete_document()
    {
        $id = $this->input->get('id');

       $this->student_model->deleteOtherDocument($id);

       redirect($_SERVER['HTTP_REFERER']);  
    }

    function deleteVisaDetails($id)
    {
        $this->student_model->deleteVisaDetails($id);
        echo "succes";exit;
       // redirect($_SERVER['HTTP_REFERER']);  
    }
}
