<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class VisaDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('visa_details_model');
        $this->load->model('change_status_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('visa_details.student_list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->visa_details_model->intakeListByStatus('1');
            // $data['semesterList'] = $this->visa_details_model->semesterListByStatus('1');
            $data['programList'] = $this->visa_details_model->programListByStatus('1');

            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_program_scheme'] = $this->security->xss_clean($this->input->post('id_program_scheme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            
            $data['searchParam'] = $formData;
            $data['applicantList'] = $this->visa_details_model->studentListSearch($formData);
            // echo "<Pre>"; print_r($data['barringList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Visa Details List';
            $this->loadViews("visa_details/list", $this->global, $data, NULL);
        }
    }

    function add($id_student)
    {
        if ($this->checkAccess('visa_details.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_student == null)
            {
                redirect('/records/visaDetails/list');
            }


            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {

                $resultprint = $this->input->post();

                    // echo "<Pre>"; print_r($_FILES);exit();
                if($resultprint)
                {


                 switch ($resultprint['btn_add'])
                 {

                    case '1':
                        

                        $formData = $this->input->post();

                        if($_FILES['image_visa_1'])
                        {
                            $image_visa_1_name = $_FILES['image_visa_1']['name'];
                            $image_visa_1_size = $_FILES['image_visa_1']['size'];
                            $image_visa_1_tmp =$_FILES['image_visa_1']['tmp_name'];

                            $image_visa_1_ext=explode('.',$image_visa_1_name);
                            $image_visa_1_ext=end($image_visa_1_ext);
                            $image_visa_1_ext=strtolower($image_visa_1_ext);

                            $this->fileFormatNSizeValidation($image_visa_1_ext,$image_visa_1_size,'Visa Front Image File');

                            $image_visa_1 = $this->uploadFile($image_visa_1_name,$image_visa_1_tmp,'Visa Front Image');

                        }






                        if($_FILES['image_visa_2'])
                        {

                        $image_visa_2_name = $_FILES['image_visa_2']['name'];
                        $image_visa_2_size = $_FILES['image_visa_2']['size'];
                        $image_visa_2_tmp =$_FILES['image_visa_2']['tmp_name'];


                        $image_visa_2_ext=explode('.',$image_visa_2_name);
                        $image_visa_2_ext=end($image_visa_2_ext);
                        $image_visa_2_ext=strtolower($image_visa_2_ext);

                        $this->fileFormatNSizeValidation($image_visa_2_ext,$image_visa_2_size,'Visa Back Image');

                        $image_visa_2 = $this->uploadFile($image_visa_2_name,$image_visa_2_tmp,'Visa Back Image');
                        }



                        if($_FILES['special_pass_image_1'])
                        {

                        $special_pass_name = $_FILES['special_pass_image_1']['name'];
                        $special_pass_size = $_FILES['special_pass_image_1']['size'];
                        $special_pass_tmp =$_FILES['special_pass_image_1']['tmp_name'];


                        $special_pass_ext=explode('.',$special_pass_name);
                        $special_pass_ext=end($special_pass_ext);
                        $special_pass_ext=strtolower($special_pass_ext);

                        $this->fileFormatNSizeValidation($special_pass_ext,$special_pass_size,'Special Pass Image');

                        $special_pass_image = $this->uploadFile($special_pass_name,$special_pass_tmp,'Special Pass Image');

                        }


                        
                        
                        // echo "<Pre>"; print_r($certificate_tmp);exit();



                        $visa_passport_no = $this->security->xss_clean($this->input->post('visa_passport_no'));
                        $visa_type = $this->security->xss_clean($this->input->post('visa_type'));
                        $special_pass_type = $this->security->xss_clean($this->input->post('special_pass_type'));
                        $special_pass_remarks = $this->security->xss_clean($this->input->post('special_pass_remarks'));
                        $visa_number = $this->security->xss_clean($this->input->post('visa_number'));
                        $date_issue = $this->security->xss_clean($this->input->post('date_issue'));
                        $expiry_date = $this->security->xss_clean($this->input->post('expiry_date'));
                        // $image_visa_1 = $this->security->xss_clean($this->input->post('image_visa_1'));
                        // $image_visa_2 = $this->security->xss_clean($this->input->post('image_visa_2'));
                        // $special_pass_image_1 = $this->security->xss_clean($this->input->post('special_pass_image_1'));
                        $special_pass_remarks = $this->security->xss_clean($this->input->post('special_pass_remarks'));
                        $special_pass_date_issue = $this->security->xss_clean($this->input->post('special_pass_date_issue'));
                        $special_pass_expiry_date = $this->security->xss_clean($this->input->post('special_pass_expiry_date'));
                        $insurance_coverage = $this->security->xss_clean($this->input->post('insurance_coverage'));
                        $reminder_type = $this->security->xss_clean($this->input->post('reminder_type'));
                        $reminder_months_expiry = $this->security->xss_clean($this->input->post('reminder_months_expiry'));
                        $reminder_template = $this->security->xss_clean($this->input->post('reminder_template'));
                        $reminder_remarks = $this->security->xss_clean($this->input->post('reminder_remarks'));
                        $special_pass_number = $this->security->xss_clean($this->input->post('special_pass_number'));



                    
                        $data = array(
                            'id_student' => $id_student,
                            'passport_no' => $visa_passport_no,
                            'visa_type' => $visa_type,
                            'special_pass_type' => $special_pass_type,
                            'special_pass_remarks' => $special_pass_remarks,
                            'visa_number' => $visa_number,
                            'date_issue' => $date_issue,
                            'expiry_date' => date('Y-m-d', strtotime($expiry_date)),
                            'special_pass_remarks' => $special_pass_remarks,
                            'special_pass_date_issue' => date('Y-m-d', strtotime($special_pass_date_issue)),
                            'special_pass_expiry_date' => date('Y-m-d', strtotime($special_pass_expiry_date)),
                            'insurance_coverage' => $insurance_coverage,
                            'special_pass_number' => $special_pass_number,
                            'reminder_type' => $reminder_type,
                            'reminder_months_expiry' => $reminder_months_expiry,
                            'reminder_template' => $reminder_template,
                            'reminder_remarks' => $reminder_remarks,
                            'created_by' => $id_user
                        );

                        if($image_visa_1 != '')
                        {
                            $data['image_visa_1'] = $image_visa_1;
                        }

                        if($image_visa_2 != '')
                        {
                            $data['image_visa_2'] = $image_visa_2;
                        }

                        if($special_pass_image != '')
                        {
                            $data['special_pass_image'] = $special_pass_image;
                        }

            
                        // echo "<Pre>"; print_r($data);exit;
                        
                        $result = $this->visa_details_model->addVisaDetails($data);

                        redirect($_SERVER['HTTP_REFERER']);


                    break;



                    case '2':
                        

                        $formData = $this->input->post();

                         if($_FILES['insurance_cover_letter'])
                        {

                        $insurance_cover_letter_name = $_FILES['insurance_cover_letter']['name'];
                        $passport_cover_letter_size = $_FILES['insurance_cover_letter']['size'];
                        $insurance_cover_letter_tmp =$_FILES['insurance_cover_letter']['tmp_name'];

                        $insurance_cover_letter_ext=explode('.',$insurance_cover_letter_name);
                        $insurance_cover_letter_ext=end($insurance_cover_letter_ext);
                        $insurance_cover_letter_ext=strtolower($insurance_cover_letter_ext);

                        $this->fileFormatNSizeValidation($insurance_cover_letter_ext,$passport_cover_letter_size,'Passport Cover Letter');

                        $insurance_cover_letter = $this->uploadFile($insurance_cover_letter_name,$insurance_cover_letter_tmp,'Insurance Cover Letter');
                        
                        }
                        
                        // echo "<Pre>"; print_r($certificate_tmp);exit();



                        $insurance_id_visa = $this->security->xss_clean($this->input->post('insurance_id_visa'));
                        $insurance_insurance_type = $this->security->xss_clean($this->input->post('insurance_insurance_type'));
                        $insurance_reference_no = $this->security->xss_clean($this->input->post('insurance_reference_no'));
                        $insurance_date_issue = $this->security->xss_clean($this->input->post('insurance_date_issue'));
                        $insurance_expiry_date = $this->security->xss_clean($this->input->post('insurance_expiry_date'));
                        $insurance_reminder_type = $this->security->xss_clean($this->input->post('insurance_reminder_type'));
                        $insurance_reminder_months_expiry = $this->security->xss_clean($this->input->post('insurance_reminder_months_expiry'));
                        $insurance_reminder_template = $this->security->xss_clean($this->input->post('insurance_reminder_template'));
                        $insurance_reminder_remarks = $this->security->xss_clean($this->input->post('insurance_reminder_remarks'));


                    
                        $data = array(
                            'id_student' => $id_student,
                            'insurance_id_visa' => $insurance_id_visa,
                            'insurance_insurance_type' => $insurance_insurance_type,
                            'insurance_reference_no' => $insurance_reference_no,
                            'insurance_date_issue' => date('Y-m-d', strtotime($insurance_date_issue)),
                            'insurance_expiry_date' => date('Y-m-d', strtotime($insurance_expiry_date)),
                            'insurance_reminder_type' => $insurance_reminder_type,
                            'insurance_reminder_months_expiry' => $insurance_reminder_months_expiry,
                            'insurance_reminder_template' => $insurance_reminder_template,
                            'insurance_reminder_remarks' => $insurance_reminder_remarks,
                            'created_by' => $id_user
                        );

                        if($insurance_cover_letter != '')
                        {
                            $data['insurance_cover_letter'] = $insurance_cover_letter;
                        }

                        // echo "<Pre>"; print_r($data);exit;
                        
                        $result = $this->visa_details_model->addInsuranceDetails($data);

                        redirect($_SERVER['HTTP_REFERER']);


                    break;



                    case '3':
                        

                        $formData = $this->input->post();

                         if($_FILES['passport_cover_letter'])
                        {


                        $passport_cover_letter_name = $_FILES['passport_cover_letter']['name'];
                        $passport_cover_letter_size = $_FILES['passport_cover_letter']['size'];
                        $passport_cover_letter_tmp =$_FILES['passport_cover_letter']['tmp_name'];

                        $passport_cover_letter_ext=explode('.',$passport_cover_letter_name);
                        $passport_cover_letter_ext=end($passport_cover_letter_ext);
                        $passport_cover_letter_ext=strtolower($passport_cover_letter_ext);

                        $this->fileFormatNSizeValidation($passport_cover_letter_ext,$passport_cover_letter_size,'Passport Cover Letter');

                        $passport_cover_letter = $this->uploadFile($passport_cover_letter_name,$passport_cover_letter_tmp,'Passport Cover Letter');
                        
                        
                        }
                        // echo "<Pre>"; print_r($certificate_tmp);exit();



                        $passport_no = $this->security->xss_clean($this->input->post('passport_no'));
                        $passport_name = $this->security->xss_clean($this->input->post('passport_name'));
                        $passport_country_of_issue = $this->security->xss_clean($this->input->post('passport_country_of_issue'));
                        $passport_date_issue = $this->security->xss_clean($this->input->post('passport_date_issue'));
                        $passport_expiry_date = $this->security->xss_clean($this->input->post('passport_expiry_date'));
                        $passport_reminder_type = $this->security->xss_clean($this->input->post('passport_reminder_type'));
                        $passport_number = $this->security->xss_clean($this->input->post('passport_number'));
                        $passport_reminder_months_expiry = $this->security->xss_clean($this->input->post('passport_reminder_months_expiry'));
                        $passport_reminder_template = $this->security->xss_clean($this->input->post('passport_reminder_template'));
                        $passport_reminder_remarks = $this->security->xss_clean($this->input->post('passport_reminder_remarks'));


                    
                        $data = array(
                            'id_student' => $id_student,
                            'passport_number' => $passport_no,
                            'passport_name' => $passport_name,
                            'passport_country_of_issue' => $passport_country_of_issue,
                            'passport_date_issue' => date('Y-m-d', strtotime($passport_date_issue)),
                            'passport_expiry_date' => date('Y-m-d', strtotime($passport_expiry_date)),
                            'passport_reminder_type' => $passport_reminder_type,
                            'passport_number' => $passport_number,
                            'passport_reminder_months_expiry' => $passport_reminder_months_expiry,
                            'passport_reminder_template' => $passport_reminder_template,
                            'passport_reminder_remarks' => $passport_reminder_remarks,
                            'created_by' => $id_user
                        );

                        if($passport_cover_letter != '')
                        {
                            $data['passport_cover_letter'] = $passport_cover_letter;
                        }

                        // echo "<Pre>"; print_r($data);exit;
                        
                        $result = $this->visa_details_model->addPassportDetails($data);

                        redirect($_SERVER['HTTP_REFERER']);


                    break;

                   }

                }

            }


            $data['studentDetails'] = $this->visa_details_model->getStudentByStudentId($id_student);
            $data['visaDetails'] = $this->visa_details_model->getVisaDetails($id_student);
            $data['countryList'] = $this->visa_details_model->countryListByStatus('1');

            $data['visaDetailsList'] = $this->visa_details_model->getStudentVisaDetails($id_student);
            $data['insuranceDetails'] = $this->visa_details_model->getStudentInsuranceDetails($id_student);
            $data['passportDetails'] = $this->visa_details_model->getStudentPassportDetails($id_student);


            // echo "<Pre>"; print_r($data);exit;

            // $data['passportList'] = $this->visa_details_model->getStudentByStudentIdForPassport($id_student);

            $this->global['pageTitle'] = 'Inventory Management : Add New Apply Change Status';
            $this->loadViews("visa_details/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('visa_details.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/applyChangeStatus/add');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_change_status = $this->security->xss_clean($this->input->post('id_change_status'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_change_status' => $id_change_status,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                $result = $this->visa_details_model->editVisaDetails($data, $id);
                redirect('/records/applyChangeStatus/list');
            }

            $data['changeStatusList'] = $this->visa_details_model->changeStatusList();
            $data['semesterList'] = $this->visa_details_model->semesterList();
            $data['studentList'] = $this->visa_details_model->studentList();
            $data['applyChangeStatus'] = $this->visa_details_model->getVisaDetails($id);
            $id_student = $data['applyChangeStatus']->id_student;
            $data['studentDetails'] = $this->visa_details_model->getStudentByStudentId($id_student);
            // echo "<Pre>";print_r($data['studentDetails']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Apply Change Status';
            $this->loadViews("visa_details/edit", $this->global, $data, NULL);
        }
    }

    function view($id)
    {

        if ($this->checkAccess('visa_details.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/applyChangeStatus/add');
            }
            if($this->input->post())
            {

            }

            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->visa_details_model->semesterList();
            $data['studentList'] = $this->visa_details_model->studentList();
            $data['applyChangeStatus'] = $this->visa_details_model->getVisaDetails($id);
            $id_student = $data['applyChangeStatus']->id_student;
            $data['studentDetails'] = $this->visa_details_model->getStudentByStudentId($id_student);
            // echo "<Pre>";print_r($data['barring']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Apply Change Status';
            $this->loadViews("visa_details/view", $this->global, $data, NULL);
        }
    }

    function approval_list()
    {
        if ($this->checkAccess('visa_details.approval') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {

           if($this->input->post())
            {
             $resultprint = $this->input->post();
             // echo "<Pre>"; print_r($resultprint);exit;

             switch ($resultprint['button'])
             {
                 case 'Approve':
                     for($i=0;$i<count($resultprint['approval']);$i++)
                    {

                         $id = $resultprint['approval'][$i];
                         $data = array(
                            'status' => 1,
                        );
                        $result = $this->visa_details_model->editVisaDetails($data, $id);


                    }
                        redirect('/records/applyChangeStatus/approval_list');
                     break;


                case 'search':

                    $data['changeStatusList'] = $this->change_status_model->changeStatusList();
                    $data['semesterList'] = $this->visa_details_model->semesterList();
                    $data['studentList'] = $this->visa_details_model->studentList();

                    $formData['name'] = $this->security->xss_clean($this->input->post('name'));
                    $formData['id_change_status'] = $this->security->xss_clean($this->input->post('id_change_status'));
                    $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
                    $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
                    
                    $data['searchParameters'] = $formData;
                    $data['applyChangeStatusApprovalList'] = $this->visa_details_model->applyChangeStatusListForApprovalSearch($formData);
                     
                     break;
                 
                 default:
                     break;
             }
            }
            $data['changeStatusList'] = $this->change_status_model->changeStatusList();
            $data['semesterList'] = $this->visa_details_model->semesterList();
            $data['studentList'] = $this->visa_details_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_change_status'] = $this->security->xss_clean($this->input->post('id_change_status'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            
            $data['searchParameters'] = $formData;
            $data['applyChangeStatusApprovalList'] = $this->visa_details_model->applyChangeStatusListForApprovalSearch($formData);
            // echo "<Pre>"; print_r($data['applyChangeStatusApprovalList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : VisaDetails List';
            $this->loadViews("visa_details/approval_list", $this->global, $data, NULL);


        }

    }

    function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->visa_details_model->getStudentByProgrammeId($id);
            $programme_data = $this->visa_details_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;

            $table="


            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $nric = $results[$i]->nric;
            $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            // $d['table'] = $table;
            // $d['view'] = $view;

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->visa_details_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;

            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                 $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";


            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Programme</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function deleteVisaDetails($id)
    {
        $deleted = $this->visa_details_model->deleteVisaDetails($id);
        echo "success";exit();
    }


    function deleteInsuranceDetails($id)
    {
        $deleted = $this->visa_details_model->deleteInsuranceDetails($id);
        echo "success";exit();
    }


    function deletePassportDetails($id)
    {
        $deleted = $this->visa_details_model->deletePassportDetails($id);
        echo "success";exit();
    }
}
