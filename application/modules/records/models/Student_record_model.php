<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_record_model extends CI_Model
{
    function mainInvoiceList()
    {
        $this->db->select('mi.*, s.full_name as student, s.email_id, s.nric');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function applicantList($data)
    {
        if ($data['id_programme'] == '')
        {
            $this->db->select('a.*, c.name as company_name, c.registration_number as company_registration_number');
            $this->db->from('student as a');
            $this->db->join('company as c', 'a.id_company = c.id','left');
             if ($data['name'] != '')
            {
                $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%')";
                $this->db->where($likeCriteria);
            }
            if ($data['nric'] != '')
            {
                $likeCriteria = "(a.nric  LIKE '%" . $data['nric'] . "%')";
                $this->db->where($likeCriteria);
            }
            if ($data['email_id'] != '')
            {
                $likeCriteria = "(a.email_id  LIKE '%" . $data['email_id'] . "%')";
                $this->db->where($likeCriteria);
            }
            if ($data['id_programme'] != '')
            {
                $this->db->where('a.id_program', $data['id_programme']);
            }
            if ($data['status'] != '')
            {
                $this->db->where('mi.status', $data['status']);
            }
            $this->db->where('a.applicant_status !=', 'Graduated');
            $this->db->order_by("id", "DESC");
             $query = $this->db->get();
             $result = $query->result();
        }
        elseif ($data['id_programme'] != '')
        {
            $this->db->select('DISTINCT(a.id) as id, s.*, c.name as company_name, c.registration_number as company_registration_number');
            $this->db->from('student_has_programme as a');
            $this->db->join('student as s', 'a.id_student = s.id');
            $this->db->join('company as c', 's.id_company = c.id','left');
             if ($data['name'] != '')
            {
                $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
                $this->db->where($likeCriteria);
            }
            if ($data['nric'] != '')
            {
                $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
                $this->db->where($likeCriteria);
            }
            if ($data['email_id'] != '')
            {
                $likeCriteria = "(s.email_id  LIKE '%" . $data['email_id'] . "%')";
                $this->db->where($likeCriteria);
            }
            if ($data['id_programme'] != '')
            {
                $this->db->where('a.id_programme', $data['id_programme']);
            }
            $this->db->where('s.applicant_status !=', 'Graduated');
             $query = $this->db->get();
             $result = $query->result();
        }
         // print_r($result);exit();     
         return $result;
    }

    // function getMainInvoiceListByStatus($data)
    // {
    //     $this->db->select('DISTINCT(s.id) as id, s.*, p.name as programme_name, p.code as programme_code, i.name as intake_name');
    //     $this->db->from('main_invoice as mi');
    //     $this->db->join('student as s', 'mi.id_student = s.id');
    //     $this->db->join('programme as p', 's.id_program = p.id');
    //     $this->db->join('intake as i', 's.id_intake = i.id');
    //     if ($data['name'] != '')
    //     {
    //         $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
    //         $this->db->where($likeCriteria);
    //     }
    //     if ($data['nric'] != '')
    //     {
    //         $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
    //         $this->db->where($likeCriteria);
    //     }
    //     // if ($data['invoice_number'] != '')
    //     // {
    //     //     $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
    //     //     $this->db->where($likeCriteria);
    //     // }
    //     if ($data['id_programme'] != '')
    //     {
    //         $this->db->where('s.id_program', $data['id_programme']);
    //     }
    //     if ($data['id_intake'] != '')
    //     {
    //         $this->db->where('s.id_intake', $data['id_intake']);
    //     }
    //     if ($data['status'] != '')
    //     {
    //         $this->db->where('mi.status', $data['status']);
    //     }
        
    //     // $this->db->order_by("mi.id", "ASC");
    //     // $this->db->join('country as c', 'sp.id_country = c.id');
    //      $query = $this->db->get();
    //      $result = $query->result();  
    //      return $result;
    // }

    function editMainInvoiceList($array)
    {
        $status = ['status'=>'1'];
      $this->db->where_in('id', $array);
      $this->db->update('main_invoice', $status);
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, s.full_name as student');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'fs.id_fee_item = fstp.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentHasProgrammeByIdStudent($id_student)
    {
        $this->db->select('shp.*, p.code as programme_code, p.name as programme_name, mi.invoice_number, pmi.invoice_number as performa_invoice_number');
        $this->db->from('student_has_programme as shp');
        $this->db->join('programme as p', 'shp.id_programme = p.id');        
        $this->db->join('main_invoice as mi', 'shp.id_invoice = mi.id','left');
        $this->db->join('performa_main_invoice as pmi', 'shp.id_performa_invoice = pmi.id','left');
        $this->db->where('shp.id_student', $id_student);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function studentDetails($id)
    {
        $this->db->select('s.*, i.name as intakeName, p.name as programName');
        $this->db->from('student as s');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->where('s.id', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result); exit();
         return $result;
    }

    function addNewTempMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempMainInvoiceDetails($id_session)
    {
        $this->db->select('tmid.*, fs.name as fee_setup');
        $this->db->from('temp_main_invoice_details as tmid');
        $this->db->join('fee_setup as fs', 'tmid.id_fee_item = fs.id');        
        $this->db->where('tmid.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

     function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_main_invoice_details');
    }

    

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_main_invoice_details', $data);
        return TRUE;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    
    function intakeListByStatus($status)
    {
        $this->db->select('r.*');
        $this->db->from('intake as r');
        $this->db->where('r.status', $status);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getStudentNote($id_student)
    {
        // $id_session = $this->session->my_session_id;
        $user_id = $this->session->userId; 

        $this->db->select('r.*, s.name as user');
        $this->db->from('student_note as r');
        $this->db->join('users as s', 'r.created_by = s.id');
        if($user_id != 1)
        {
            $this->db->where('r.created_by', $user_id);
        }
        $this->db->where('r.id_student', $id_student);
        $this->db->order_by('r.id', 'DESC');
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function deleteStudentNote($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('student_note');
    }

    function addStudentNote($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_note', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no as advisor_icno, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name, q.name as qualification_name, q.short_name as qualification_code, pt.code as program_structure_code, pt.name as program_structure_name, n.name as nationality, com.name as company_name, com.registration_number as company_registration_number');
        $this->db->from('student as s');
        $this->db->join('program_type as pt', 's.id_program_structure_type = pt.id','left');
        $this->db->join('programme as p', 's.id_program = p.id','left'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id','left'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id','left'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id','left');
        $this->db->join('state as ps', 's.permanent_state = ps.id','left'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id','left'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id','left'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left');
        $this->db->join('education_level as q', 's.id_degree_type = q.id','left');
        $this->db->join('nationality as n', 's.nationality = n.id','left');
        $this->db->join('company as com', 's.id_company = com.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->where('status', 1);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getProgramme($id)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInvoiceByStudentId($id_student,$id_applicant)
    {

        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getReceiptByStudentId($id_student,$id_applicant)
    {
        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*');
        $this->db->from('receipt as mi');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    // function getInvoiceByStudentId($id_student)
    // {
    //     $this->db->select('mi.*, s.full_name as student, s.email_id, s.nric');
    //     $this->db->from('main_invoice as mi');
    //     $this->db->join('student as s', 'mi.id_student = s.id');
    //     $this->db->where('mi.id_student', $id_student);
    //     $query = $this->db->get();
    //      $result = $query->result();

    //     return$result;
    // }

    // function getReceiptByStudentId($id_student)
    // {
    //     $this->db->select('r.*, s.full_name as student, s.email_id, s.nric');
    //     $this->db->from('receipt as r');
    //     $this->db->join('student as s', 'r.id_student = s.id');
    //     $this->db->where('r.id_student', $id_student);
    //     $query = $this->db->get();
    //      $result = $query->result();

    //     return$result;
    // }

    function barrReleaseByStudentId($id)
    {
        $barrings = $this->getBarringByStudentId($id);
        $releases = $this->getReleaseByStudentId($id);

        $details = array();

        foreach ($barrings as $barring)
        {

            $barring->type = 'Barring';
            array_push($details, $barring);
        }

        foreach ($releases as $release)
        {

            $release->type = 'Release';
            array_push($details, $release);
        }

        return $details;

    }



    function getBarringByStudentId($id_student)
    {

        $this->db->select('mi.*, s.code as barring_code, s.name as barring_name, usr.name as created_by');
        $this->db->from('barring as mi');
        $this->db->join('barring_type as s', 'mi.id_barring_type = s.id');
        $this->db->join('users as usr', 'mi.created_by = usr.id');
        $this->db->where('mi.id_student', $id_student);
        $this->db->order_by('mi.created_dt_tm', 'DESC');
        $query = $this->db->get();
         $result = $query->result();

        return$result;

    }

    function getReleaseByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as barring_code, s.name as barring_name, usr.name as created_by');
        $this->db->from('release as mi');
        $this->db->join('barring_type as s', 'mi.id_barring_type = s.id');
        $this->db->join('users as usr', 'mi.created_by = usr.id');
        $this->db->where('mi.id_student', $id_student);
        $this->db->order_by('mi.created_dt_tm', 'DESC');
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }




    function getCourseRegisteredByStudentId($id_program,$id_intake,$id_student)
    {
        $this->db->select('mi.*, s.code as course_code, s.name as course_name, s.credit_hours');
        $this->db->from('add_course_to_program_landscape as mi');
        $this->db->join('course as s', 'mi.id_course = s.id');
        $this->db->where('mi.id_program', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        // $this->db->join('course_registration as cr', 'mi.id = cr.id','left');
        // $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
         $results = $query->result();

         $details = array();
         foreach ($results as $value)
         {
                $value->grade = 0;
                $value->is_registered = 0;
                $value->is_exam_registered = 0;
                $value->is_result_announced = 0;
                $value->is_bulk_withdraw = 0;
                $value->total_result = 0;

                $course_registered = $this->getCourseRegistered($value->id,$id_student);

               if($course_registered)
               {
                   $value->is_exam_registered = $course_registered->is_exam_registered;
                   $value->is_result_announced = $course_registered->is_result_announced;
                   $value->is_bulk_withdraw = $course_registered->is_bulk_withdraw;
                   $value->total_result = $course_registered->total_result;
                   $value->grade = $course_registered->grade;
                   $value->is_registered = $course_registered->id;
               }
           array_push($details, $value);


         }

        return$details;
    }


    function getCourseRegistered($id,$id_student)
    {
        $this->db->select('mi.*');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id_course_registered_landscape', $id);
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }


    function getWithdrawByCourseNStudent($id_course_registered_landscape, $id_student)
    {
        $this->db->select('mi.*, s.code as course_code, s.name as course_name, s.credit_hours');
        $this->db->from('course_registration as mi');
        $this->db->join('course as s', 'mi.id_course = s.id');
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        if($result)
        {
            return 1;
        }
    }

    function getCourseWithdrawByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as course_code, s.name as course_name');
        $this->db->from('bulk_withdraw as mi');
        $this->db->join('course as s', 'mi.id_course = s.id');
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getCreditNoteByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as credit_code, s.name as credit_name, inv.invoice_number');
        $this->db->from('credit_note as mi');
        $this->db->join('credit_note_type as s', 'mi.id_type = s.id');
        $this->db->join('main_invoice as inv', 'mi.id_invoice = inv.id');
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getDebitNoteByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as debit_code, s.name as debit_name, inv.invoice_number');
        $this->db->from('debit_note as mi');
        $this->db->join('credit_note_type as s', 'mi.id_type = s.id');
        $this->db->join('main_invoice as inv', 'mi.id_invoice = inv.id');
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getCreditTransferByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as course_code, s.name as course_name');
        $this->db->from('credit_transfer_details as mi');
        $this->db->join('course as s', 'mi.id_course = s.id','left');
        $this->db->join('credit_transfer as ct', 'mi.id_credit_transfer = ct.id');
        $this->db->where('ct.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }


    function addOtherDocuments($data)
    {
        $this->db->trans_start();
        $this->db->insert('other_documents', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamDetails($id)
    {
        $this->db->select('*');
        $this->db->from('examination_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProficiencyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('english_proficiency_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getEmploymentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('employment_status');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProfileDetails($id)
    {
        $this->db->select('*');
        $this->db->from('profile_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getVisaDetails($id)
    {
        $this->db->select('*');
        $this->db->from('visa_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getOtherDocuments($id)
    {
        $this->db->select('*');
        $this->db->from('other_documents');
        $this->db->where('id_student', $id);
        // $this->db->limit('0,1');
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function getHostelRoomAllotmentDetails($id)
    {
        $this->db->select('ra.*, hr.name as hostel_name, hr.code as hostel_code, hr1.code as room_code, hr1.name as room_name, hr2.code as unit_code, hr2.name as unit_name, hr3.code as apartment_code, hr3.name as apartment_name');
        $this->db->from('room_allotment as ra');
        $this->db->join('hostel_registration as hr', 'ra.id_hostel = hr.id');
        $this->db->join('hostel_room as hr1', 'ra.id_room = hr1.id');
        $this->db->join('hostel_room as hr2', 'hr1.id_parent = hr2.id');
        $this->db->join('hostel_room as hr3', 'hr2.id_parent = hr3.id');
        $this->db->where('hr1.level', 3);
        $this->db->where('hr2.level', 2);
        $this->db->where('hr3.level', 1);
        $this->db->where('ra.id_student', $id);
        // $this->db->limit('0,1');
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

     function emergencyContactDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student_emergency_contact_details');
        $this->db->where('id_student =', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function advisorTaggingDetails($id)
    {
        $this->db->select('adt.*, adv.ic_no, adv.name as advisor_name, usr.name as created_by');
        $this->db->from('advisor_tagging as adt');
        $this->db->join('staff as adv', 'adt.id_advisor = adv.id');
        $this->db->join('users as usr', 'adt.created_by = usr.id');
        $this->db->where('id_student', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function phdDurationTaggingDetails($id)
    {
        $this->db->select('adt.*, usr.name as created_by');
        $this->db->from('student_duration_history as adt');
        // $this->db->join('staff as adv', 'adt.id_advisor = adv.id');
        $this->db->join('users as usr', 'adt.created_by = usr.id');
        $this->db->where('adt.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function studentSemesterResult($id_Student)
    {
         $this->db->select('*');
        $this->db->from('student_semester_result as ssr');
        $this->db->where('id_student', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }



    function studentSemesterResultRMarksEntry($id)
    {
        $this->db->select('ssr.*, sem.name as semester_name, sem.code as semester_code, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code, gra.name as grade');
        $this->db->from('student_semester_result as ssr');
        $this->db->join('intake as i', 'ssr.id_intake = i.id','left');
        $this->db->join('programme as p', 'ssr.id_program = p.id','left');
        $this->db->join('semester as sem', 'ssr.id_semester = sem.id','left');
        $this->db->join('grade as gra', 'ssr.grade = gra.id','left');
        $this->db->where('ssr.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();  

         // foreach ($result as $data)
         // {
         //    $total_marks = $this->getTotalMarksByMasterId($data->id);
         //     # code...
         // }
         return $result;
    }

    // function getTotalMarksByMasterId($id)
    // {
    //     $this->db->select_sum('amount');
    //     $result = $this->db->get('student_semester_course_details')->row();  
    //     return $result->amount;
    //      $this->db->select('*');
    //     $this->db->from('student_semester_course_details as ssr');
    //     $this->db->where('id_student', $status);
    //     $this->db->order_by("name", "ASC");
    //     $query = $this->db->get();
    //     return $query->result();
    // }

    function getCourseRegisteredLandscape($id_program,$id_intake,$id_student)
    {
            // echo "<Pre>";print_r($id_program);exit;
        $this->db->select('mi.*');
        $this->db->from('add_course_to_program_landscape as mi');
        $this->db->where('mi.id_program', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $query = $this->db->get();
        $results = $query->result();
        
        $details = array();

        foreach ($results as $value)
        {
           $value->is_exam_registered = 0;
            $value->is_registered = 0;
           $value->is_result_announced = 0;
           $value->is_bulk_withdraw = 0;
           $value->total_result = 0;
           $value->grade = 0;

           $course = $this->getCourseDetails($value->id_course);
           $course_registered = $this->getCourseRegisteredDetails($value->id,$id_student);
           $value->course_name = $course->name;
           $value->course_code = $course->code;
           $value->credit_hours = $course->credit_hours;

           if($course_registered)
           {
               $value->is_exam_registered = $course_registered->is_exam_registered;
               $value->is_result_announced = $course_registered->is_result_announced;
               $value->is_bulk_withdraw = $course_registered->is_bulk_withdraw;
               $value->total_result = $course_registered->total_result;
               $value->grade = $course_registered->grade;
               $value->is_registered = $course_registered->id;
           }
           array_push($details, $value);
        }

        return $details;
    }

    function getCourseRegisteredLandscapeByLandscapeId($id,$id_student)
    {
            // echo "<Pre>";print_r($id_program);exit;
        $this->db->select('mi.*');
        $this->db->from('add_course_to_program_landscape as mi');
        $this->db->where('mi.id_program_landscape', $id);
        $query = $this->db->get();
        $results = $query->result();
        
        $details = array();

        foreach ($results as $value)
        {
           $value->is_exam_registered = 0;
            $value->is_registered = 0;
           $value->is_result_announced = 0;
           $value->is_bulk_withdraw = 0;
           $value->total_result = 0;
           $value->grade = 0;

           $course = $this->getCourseDetails($value->id_course);
           $course_registered = $this->getCourseRegisteredDetails($value->id,$id_student);
           $value->course_name = $course->name;
           $value->course_code = $course->code;
           $value->credit_hours = $course->credit_hours;

           if($course_registered)
           {
               $value->is_exam_registered = $course_registered->is_exam_registered;
               $value->is_result_announced = $course_registered->is_result_announced;
               $value->is_bulk_withdraw = $course_registered->is_bulk_withdraw;
               $value->total_result = $course_registered->total_result;
               $value->grade = $course_registered->grade;
               $value->is_registered = $course_registered->id;
           }
           array_push($details, $value);
        }

        return $details;
    }

    function getCourseDetails($id)
    {
        $this->db->select('mi.*');
        $this->db->from('course as mi');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }


    function getCourseRegisteredDetails($id,$id_student)
    {
        $this->db->select('mi.*');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id_course_registered_landscape', $id);
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getLandscapeCount($id_program,$id_intake)
    {
        $courseTypeCompulsary = 'Compulsory';
        $courseTypeNotCompulsary = 'Not Compulsory';
        $courseTypeMajor = 'Major';
        $courseTypeMinor = 'Minor';

        $data['compulsary'] = $this->getCompulsary($courseTypeCompulsary, $id_program, $id_intake);
        $data['notCompulsary'] = $this->getCompulsary($courseTypeNotCompulsary, $id_program, $id_intake);
        $data['manjor'] = $this->getCompulsary($courseTypeMajor, $id_program, $id_intake);
        $data['minor'] = $this->getCompulsary($courseTypeMinor, $id_program, $id_intake);

        return $data;
    }

    function getCompulsary($type,$id_program,$id_intake)
    {
        $this->db->select('mi.*');
        $this->db->from('add_course_to_program_landscape as mi');
        $this->db->where('mi.course_type', $type);
        $this->db->where('mi.id_program', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $query = $this->db->get();
        $results = $query->result();

            // echo "<Pre>";print_r($results);exit;
        $total_credit_hours = 0;
        foreach ($results as $result)
        {

            $course = $this->getCourseDetails($result->id_course);
            $total_credit_hours = $total_credit_hours + $course->credit_hours;
            
        }
            // echo "<Pre>";print_r($total_credit_hours);exit;

        return $total_credit_hours;
    }


    function courseCompletedDetails($id_program,$id_intake,$id_student)
    {
        $this->db->select('mi.*');
        $this->db->from('course_registration as mi');
        $this->db->join('exam_registration as er', 'mi.id = er.id_course_registered');
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
        $results = $query->result();
        
        $details = array();

        foreach ($results as $value)
        {
           $value->is_exam_registered = 0;
           $value->is_result_announced = 0;
           $value->is_bulk_withdraw = 0;
           $value->total_result = 0;

           $course = $this->getCourseDetails($value->id_course);
           $course_registered = $this->getCourseRegisteredDetails($value->id,$id_student);
           $value->course_name = $course->name;
           $value->course_code = $course->code;
           $value->credit_hours = $course->credit_hours;

           if($course_registered)
           {
               $value->is_exam_registered = $course_registered->is_exam_registered;
               $value->is_result_announced = $course_registered->is_result_announced;
               $value->is_bulk_withdraw = $course_registered->is_bulk_withdraw;
               $value->total_result = $course_registered->total_result;
           }

           array_push($details, $value);
        }

        return $details;
    }

    function getLandscapeRegisteredCount($id_program,$id_intake,$id_student)
    {
        $courseTypeCompulsary = 'Compulsory';
        $courseTypeNotCompulsary = 'Not Compulsory';
        $courseTypeMajor = 'Major';
        $courseTypeMinor = 'Minor';

        $data['compulsary'] = $this->getCourseDetailsForCompulsaryData($courseTypeCompulsary, $id_program, $id_intake,$id_student);
        $data['notCompulsary'] = $this->getCourseDetailsForCompulsaryData($courseTypeNotCompulsary, $id_program, $id_intake,$id_student);
        $data['manjor'] = $this->getCourseDetailsForCompulsaryData($courseTypeMajor, $id_program, $id_intake,$id_student);
        $data['minor'] = $this->getCourseDetailsForCompulsaryData($courseTypeMinor, $id_program, $id_intake,$id_student);

        return $data;
    }


    function getCourseDetailsForCompulsaryData($type,$id_program,$id_intake)
    {
        $this->db->select('mi.*');
        $this->db->from('course_registration as mi');
        $this->db->join('add_course_to_program_landscape as sem', 'mi.id_course_registered_landscape = sem.id','left');
        $this->db->where('sem.course_type', $type);
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $query = $this->db->get();
        $results = $query->result();

            // echo "<Pre>";print_r($results);exit;
        $total_credit_hours = 0;
        foreach ($results as $result)
        {

            $course = $this->getCourseDetails($result->id_course);
            $total_credit_hours = $total_credit_hours + $course->credit_hours;
            
        }
            // echo "<Pre>";print_r($total_credit_hours);exit;

        return $total_credit_hours;
    }

    function getProgrammeLandscape($id_program,$id_intake,$id_program_scheme,$id_program_has_scheme)
    {
        $this->db->select('mi.*');
        $this->db->from('programme_landscape as mi');
        $this->db->where('mi.program_scheme', $id_program_has_scheme);
        $this->db->where('mi.learning_mode', $id_program_scheme);
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $query = $this->db->get();
        $result = $query->row();
        return $result; 
    }

    function getSemester($id_semester)
    {
        $this->db->select('mi.*');
        $this->db->from('semester as mi');
        $this->db->where('mi.id', $id_semester);
        $query = $this->db->get();
        $result = $query->row();

        return $result; 
    }

    function getCourse($id)
    {
        $this->db->select('mi.*');
        $this->db->from('course as mi');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result; 
    }


    function getProgramLandscapeDetails($id_program,$id_intake,$id_student,$id_semester)
    {
        $this->db->select('DISTINCT(mi.id_course) as id_course, mi.id');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $this->db->where('mi.id_semester', $id_semester);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();

        foreach ($results as $value)
        {
            $value->is_exam_registered = 0;
           $value->is_result_announced = 0;
           $value->is_bulk_withdraw = 0;
           $value->total_result = 0;
           $value->grade = '';


            $course = $this->getCourse($value->id_course);
            $course_registered = $this->getCourseRegisteredDetailsForLandscape($value->id,$id_student);

            if($course_registered)
           {
               $value->is_exam_registered = $course_registered->is_exam_registered;
               $value->is_result_announced = $course_registered->is_result_announced;
               $value->is_bulk_withdraw = $course_registered->is_bulk_withdraw;
               $value->total_result = $course_registered->total_result;
               $value->grade = $course_registered->grade;
           }

           if($course)
           {
               $value->course_code = $course->code;
               $value->course_name = $course->name;
               $value->credit_hours = $course->credit_hours;
           }

           // echo "<Pre>";print_r($value);exit;


            array_push($details, $value);
        }

        return $details;
    }


    function getCourseRegisteredLandscapeBySemesterForDisplay($id_program,$id_intake,$id_program_landscape,$id_student)
    {
            // echo "<Pre>";print_r($results);exit;
        $this->db->select('DISTINCT(mi.id_semester) as id_semester');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $query = $this->db->get();
        $results = $query->result();

           // echo "<Pre>";print_r($results);exit;
        $details = array();
        foreach ($results as $result)
        {

            $semester = $this->getSemester($result->id_semester);
            $program_landscape = $this->getProgramLandscapeDetails($id_program,$id_intake,$id_student,$result->id_semester);

            $data = $semester;
            $data->course = $program_landscape;

            array_push($details, $data);
        }
            // echo "<Pre>";print_r($details);exit;

        return $details;
    }

    function getCourseRegisteredDetailsForLandscape($id,$id_student)
    {
        $this->db->select('mi.*');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id', $id);
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function studentStatus($id)
    {
        $this->db->select('mi.*');
        $this->db->from('student as mi');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function applyChangeStatusListByStudentId($id_student)
    {
        $this->db->select('b.*, stu.first_name as student, sem.name as semester, bt.name as change_status, usr.name as created_by');
        $this->db->from('apply_change_status as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_semester = sem.id');
        $this->db->join('change_status as bt', 'b.id_change_status = bt.id');
        $this->db->join('users as usr', 'b.created_by = usr.id','left');
        $this->db->where('b.id_student', $id_student);
        $this->db->order_by("b.id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function courseRegisteredList($id)
    {
        $this->db->select('mi.*, sem.code as semester_code, sem.name as semester_name');
        $this->db->from('course_register as mi');
        $this->db->join('semester as sem', 'mi.id_semester = sem.id');
        $this->db->where('mi.id_student', $id);
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getDeliverablesDurationsByIdStudent($id_student)
    {
        $this->db->select('DISTINCT(rds.id_chapter) as id_chapter');
        $this->db->from('research_deliverables_student as rds');
        $this->db->join('research_chapter as rc','rds.id_chapter = rc.id');
        $this->db->join('research_phd_duration as rpd','rds.id_phd_duration = rpd.id');
        $this->db->join('student as s','rds.id_student = s.id');
        $this->db->where('rds.id_student', $id_student);
        $query = $this->db->get();
        $results = $query->result();

        // echo "<Pre>";print_r($results);exit;

        $details = array();
        foreach ($results as $result)
        {
            $id_chapter = $result->id_chapter;
            $chapter = $this->getChapter($id_chapter);
            if($chapter)
            {
                $data_one = new stdClass;

                $data_one->chapter = $chapter->name;

                $topics = $this->getTopicsByIdChapterNIdStudent($id_chapter,$id_student);

                // echo "<Pre>";print_r($topics);exit;

                if(!empty($topics))
                {
                    $data_one->topics = $topics;
                }
                else
                {
                    $data_one->topics = array();
                }

                array_push($details,$data_one);
            }
        }

        // echo "<Pre>";print_r($details);exit;
        return $details;
    }

    function getChapter($id_chapter)
    {
        $this->db->select('fc.*');
        $this->db->from('research_chapter as fc');
        $this->db->where('fc.id', $id_chapter);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function getTopicsByIdChapterNIdStudent($id_chapter,$id_student)
    {
        $this->db->select('rc.topic');
        $this->db->from('research_deliverables_student as rds');
        $this->db->join('research_deliverables as rc','rds.id_topic = rc.id');
        $this->db->join('research_phd_duration as rpd','rds.id_phd_duration = rpd.id');
        // $this->db->join('research_phd_duration as rpd','rds.id_phd_duration = rpd.id');
        $this->db->join('student as s','rds.id_student = s.id');
        $this->db->where('rds.id_chapter', $id_chapter);
        $this->db->where('rds.id_student', $id_student);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function getDeliverablesStartingByIdStudent($id_student)
    {
        $this->db->select('fc.*');
        $this->db->from('student_deliverable_history as fc');
        $this->db->where('fc.id_student', $id_student);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
}