<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Apply_change_learning_mode_model extends CI_Model
{
    
    function applyChangeSchemeList()
    {
        $this->db->select('b.*, stu.full_name as student, pro.name as programme');
        $this->db->from('apply_change_scheme as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('programme as pro', 'b.id_programme = pro.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function applyChangeSchemeListSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, stu.email_id, pro.name as programme, stu.nric, pro.code as programme_code, inta.name as intake_name, sem.name as semester_name, sem.code as semester_code, pps.mode_of_program as previous_mode_of_program, pps.mode_of_study as previous_mode_of_study, pps1.mode_of_program as new_mode_of_program, pps1.mode_of_study as new_mode_of_study');
        $this->db->from('apply_change_scheme as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_new_semester = sem.id');
        $this->db->join('programme as pro', 'b.id_programme = pro.id');
        $this->db->join('intake as inta', 'b.id_intake = inta.id');
        $this->db->join('programme_has_scheme as pps', 'b.id_new_program_scheme = pps.id');
        $this->db->join('programme_has_scheme as pps1', 'b.id_program_scheme = pps1.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $likeCriteria = "(b.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getApplyChangeScheme($id)
    {
        $this->db->select('*');
        $this->db->from('apply_change_scheme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }
    

    function addNewApplyChangeScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('apply_change_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editApplyChangeScheme($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('apply_change_scheme', $data);

        return TRUE;
    }

    function deleteApplyChangeScheme($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, full_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function applyChangeSchemeListForApprovalSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, stu.email_id, pro.name as programme, stu.nric, pro.code as programme_code, inta.name as intake_name, sem.name as semester_name, sem.code as semester_code, pps.mode_of_program as previous_mode_of_program, pps.mode_of_study as previous_mode_of_study, pps1.mode_of_program as new_mode_of_program, pps1.mode_of_study as new_mode_of_study');
        $this->db->from('apply_change_scheme as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('semester as sem', 'b.id_new_semester = sem.id');
        $this->db->join('programme as pro', 'b.id_programme = pro.id');
        $this->db->join('intake as inta', 'b.id_intake = inta.id');
        $this->db->join('programme_has_scheme as pps', 'b.id_new_program_scheme = pps.id');
        $this->db->join('programme_has_scheme as pps1', 'b.id_program_scheme = pps1.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $likeCriteria = "(b.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_student']))
        {
            $likeCriteria = "(b.id_student  LIKE '%" . $formData['id_student'] . "%')";
            $this->db->where($likeCriteria);
        }
        $where_pending = "(b.status  = '0')";
        $this->db->where($where_pending);
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    // function editApplyProgrammeListList($array)
    // {
    //   $status = ['status'=>'1'];
    //   $this->db->where_in('id', $array);
    //   $this->db->update('apply_change_scheme', $status);
    //   // $this->db->set('status', $status);
    // }


    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        // echo "<Pre>";print_r($result);exit();
        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeByProgrammeNIntake($data)
    {
        $id_programme = $data['id_programme'];
        $id_intake = $data['id_intake'];
        $id_program_scheme = $data['id_program_scheme'];
        $currency = $data['currency'];
        // echo "<Pre>";  print_r($data);exit;

        $this->db->select('fs.*, fi.name as fee_setup, fi.code as fee_code, p.name as programme_name, i.name as intake_name');
        $this->db->from('fee_structure as fs');
        $this->db->join('fee_setup as fc', 'fs.id_fee_item = fc.id');   
        $this->db->join('programme as p', 'fs.id_programme = p.id'); 
        $this->db->join('intake as i', 'fs.id_intake = i.id'); 
        $this->db->join('fee_setup as fi', 'fs.id_fee_item = fi.id'); 
        $this->db->where('fs.id_programme', $id_programme);
        $this->db->where('fs.id_intake', $id_intake);
        // $this->db->where('fs.id_learning_mode', $id_program_scheme);
        // $this->db->where('fs.currency', $currency);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByData($data)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_program', $data['id_programme']);
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.id_program_scheme', $data['id_program_scheme']);
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function editApplyProgrammeList($data, $id)
    {

        $this->db->where('id', $id);
        $this->db->update('apply_change_scheme', $data);
        
        $data_row = $this->getApplyChangeScheme($id);


        $id_student = $data_row->id_student;
        
        $student_data = $this->getStudent($id_student);

        $id_program = $data_row->id_programme;
        $id_intake = $data_row->id_intake;
        $id_program_scheme = $data_row->id_new_program_scheme;



        $update = $this->editStudentData($id_program_scheme,$id_intake,$id_student);

        // Hided Due TO New Invoice Generation Flow From Fee Structure By Activity
        // $this->generateNewInvoiceForStudentToNewScheme($id);   
    
        return TRUE;
    }

    function generateNewInvoiceForStudentToNewScheme($id_program_scheme)
    {
        $user_id = $this->session->userId;

        $data_row = $this->getApplyChangeScheme($id);

        // echo "<Pre>";print_r($data_row);exit();

        $id_student = $data_row->id_student;
        
        $student_data = $this->getStudent($id_student);

        $nationality = $student_data->nationality;


        $id_program = $data_row->id_programme;
        $id_intake = $data_row->id_intake;
        $id_program_scheme = $data_row->id_new_program_scheme;


        if($nationality == 'Malaysian')
        {
            $currency = 'MYR';
            $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'MYR');
        }
        elseif($nationality == 'Other')
        {
            $currency = 'USD';
            $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'USD');
        }

        $invoice_number = $this->generateMainInvoiceNumber();


        $invoice['invoice_number'] = $invoice_number;
        $invoice['type'] = 'Student';
        $invoice['remarks'] = 'Student Apply For Migration / Change Of Scheme';
        $invoice['id_application'] = '0';
        $invoice['id_program'] = $id_program;
        $invoice['id_intake'] = $id_intake;
        $invoice['id_student'] = $id_student;
        $invoice['currency'] = $currency;
        $invoice['total_amount'] = '0';
        $invoice['balance_amount'] = '0';
        $invoice['paid_amount'] = '0';
        $invoice['status'] = '1';
        $invoice['created_by'] = $user_id;

        // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

        if($update)
        {

            $inserted_id = $this->addNewMainInvoice($invoice);
            $total_amount = 0;
            foreach ($fee_structure_data as $fee_structure)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure->id_fee_item,
                        'amount' => $fee_structure->amount,
                        'status' => '1',
                        'created_by' => $user_id
                    );
                $total_amount = $total_amount + $fee_structure->amount;
                $this->addNewMainInvoiceDetails($data);

            }

            // $total_amount = number_format($total_amount, 2, '.', ',');
            // echo "<Pre>";print_r($total_amount);exit;

            $invoice_update['total_amount'] = $total_amount;
            $invoice_update['invoice_total'] = $total_amount;
            $invoice_update['balance_amount'] = $total_amount;
            $invoice_update['paid_amount'] = '0';
            // $invoice_update['inserted_id'] = $inserted_id;
            // echo "<Pre>";print_r($invoice_update);exit;
            $this->editMainInvoice($invoice_update,$inserted_id);
        }
        return TRUE;
    }

    function getFeeStructure($id_programme,$id_intake,$id_program_scheme,$currency)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');   
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.currency', $currency);
        $query = $this->db->get();
        $fee_structure = $query->result();
        // $detail_data = $this->getFeeStructureDetails($fee_structure->id);
        return $fee_structure;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function editStudentData($id_program_scheme,$id_intake,$id)
    {
        $user_id = $this->session->userId;

        $program_scheme = $this->getProgramScheme($id_program_scheme);

        $data = array(
                    'id_program_scheme' => $id_program_scheme,
                    'program_scheme' => $program_scheme->mode_of_program . " - " . $program_scheme->mode_of_study,
                    'id_intake' => $id_intake,
                    'updated_by' => $user_id
                );

        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function getProgramScheme($id)
    {
        
        $this->db->select('s.*');
        $this->db->from('programme_has_scheme as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoice($data,$id_apply_change_status)
    {
        $user_id = $this->session->userId;


        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);

        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;

        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('CHANGE LEARNING MODE','Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('CHANGE LEARNING MODE','Approval Level',$id_program);
        }

        if($fee_structure_data)
        {

            $currency = $fee_structure_data->id_currency;
            $invoice_amount = $fee_structure_data->amount_local;

            // if($nationality == '1')
            // {
            //     $currency = $fee_structure_data->id_currency;
            //     $invoice_amount = $fee_structure_data->amount_local;
            // }
            // elseif($nationality != '')
            // {
            //     $currency = $fee_structure_data->id_currency;
            //     $invoice_amount = $fee_structure_data->amount_international;
            // }



            $invoice_number = $this->generateMainInvoiceNumber();


            $invoice['invoice_number'] = $invoice_number;
            $invoice['type'] = 'Student';
            $invoice['remarks'] = 'Student Apply Change Learning Mode';
            $invoice['id_application'] = '0';
            $invoice['id_program'] = $id_program;
            $invoice['id_intake'] = $id_intake;
            $invoice['id_student'] = $id_student;
            $invoice['id_student'] = $id_student;
            $invoice['currency'] = $currency;
            $invoice['total_amount'] = $invoice_amount;
            $invoice['invoice_total'] = $invoice_amount;
            $invoice['balance_amount'] = $invoice_amount;
            $invoice['paid_amount'] = '0';
            $invoice['status'] = '1';
            $invoice['created_by'] = $user_id;

            // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

            
            // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
            
            $inserted_id = $this->addNewMainInvoice($invoice);

            if($inserted_id)
            {
                $data = array(
                        'id_main_invoice' => $inserted_id,
                        'id_fee_item' => $fee_structure_data->id_fee_setup,
                        'amount' => $invoice_amount,
                        'status' => 1,
                        'quantity' => 1,
                        'price' => $invoice_amount,
                        'id_reference' => $id_apply_change_status,
                        'description' => 'APPLY CHANGE LEARNING MODE',
                        'created_by' => $user_id
                    );

                $this->addNewMainInvoiceDetails($data);
            }
        }
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }
}
