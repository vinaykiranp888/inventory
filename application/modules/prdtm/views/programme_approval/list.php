<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Product</h3>
      <a href="add" class="btn btn-primary">+ Add Product</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Product</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category Type</label>
                      <div class="col-sm-8">
                        <select name="id_category" id="id_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryList))
                            {
                                foreach ($categoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_category'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>



                <div class="row">

                  <!-- <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category Type Setup</label>
                      <div class="col-sm-8">
                        <select name="id_category_type" id="id_category_type" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryTypeList))
                            {
                                foreach ($categoryTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_category_type'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div> -->

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Product Type</label>
                      <div class="col-sm-8">
                        <select name="id_programme_type" id="id_programme_type" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($productTypeSetupList))
                            {
                                foreach ($productTypeSetupList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme_type'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>





              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Name</th>
            <th>Target Audience</th>
            <th>Student Learning Hours</th>           
            <th>Total Credit Hours</th>
            <th>Max. Duration</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($programmeList)) {
            $i=1;
            foreach ($programmeList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->target_audience ?></td>
                <td><?php echo $record->student_learning_hours ?></td>
                <td><?php echo $record->total_cr_hrs ?></td>               
                <td><?php echo $record->max_duration . " - " . $record->duration_type ?></td>
                <td>
                  <?php if($record->status == 1)
                  {
                    echo 'Active';
                  }elseif ($record->status == 0)
                  {
                    echo 'In-Active';
                  } ?>  
                </td>
                <!-- <td><?php echo  $record->scheme_code . " - " . $record->scheme ?></td> -->
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                <!--    -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

</script>