<?php
$programme_approval_model = new Programme_approval_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_approval_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>

<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/prdtm/programmeApproval/view/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programmeApproval/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            
          <!--   <li><a href="/prdtm/programmeApproval/skill/<?php echo $id_programme;?>">Skills</a></li>
          <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programmeApproval/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programmeApproval/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programmeApproval/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programmeApproval/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/prdtm/programmeApproval/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programmeApproval/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programmeApproval/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programmeApproval/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programmeApproval/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programmeApproval/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programmeApproval/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->
          
          <li class="active"><a href="/prdtm/programmeApproval/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/prdtm/programmeApproval/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          <li><a href="/prdtm/programmeApproval/approval/<?php echo $id_programme;?>">Approval</a></li>
        
        </ul>
      

    <div class="form-container">
            <h4 class="form-group-title">Fee Structure Main Details</h4> 

        <div class="row">
              

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Fee Structure Code <span class='error-text'>*</span></label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->code; ?>" readonly="readonly">
                  </div>
              </div>


              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Fee Structure Name <span class='error-text'>*</span></label>
                      <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->name; ?>" readonly="readonly">
                  </div>
              </div>    

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Name Optional Language</label>
                      <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $getProgrammeLandscapeLocal->name_optional_language; ?>" readonly="readonly">
                  </div>
              </div>     


        </div>

        

        <div class="row">

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Programme <span class='error-text'>*</span></label>
                      <input type="text" class="form-control" id="year" name="year" value="<?php echo $getProgrammeLandscapeLocal->program_code . " - " . $getProgrammeLandscapeLocal->program; ?>" readonly="readonly">
                  </div>
              </div>


              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Currency <span class='error-text'>*</span></label>
                      <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                          <option value="">Select</option>
                          <?php
                          if (!empty($currencyList))
                          {
                              foreach ($currencyList as $record)
                              {?>
                                  <option value="<?php echo $record->id;?>"
                                    <?php
                                    if($record->id == $getProgrammeLandscapeLocal->id_currency)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                  ><?php echo $record->code . " - " . $record->name;?>
                                  </option>
                          <?php
                              }
                          }
                          ?>
                      </select>
                  </div>
              </div>

              
        </div>

      </div>



      <br>


      <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                          <div class="form-container">
                              <h4 class="form-group-title">Fee Structure List</h4>        

                              <div class="custom-table">
                                <table class="table" id="list-table">
                                  <thead>
                                    <tr>
                                      <th>Sl. No</th>
                                      <th>Fee Item</th>
                                      <th>Is Registration Fee</th>
                                      <th>Tax Applicable</th>
                                      <th>Total Amount ( <?php echo $feeStructureLocalList[0]->currency_code ?> )</th>
                                      <!-- <th>Action</th> -->
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                    if (!empty($feeStructureLocalList))
                                    {
                                      $i = 1;
                                      $total_amount = 0;
                                      foreach ($feeStructureLocalList as $record)
                                      {
                                        $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                                        $tax_amount = number_format($tax_amount, 2, '.', ',');
                                    ?>
                                        <tr>
                                          <td><?php echo $i ?></td>
                                          <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?>                                
                                          </td>
                                          <td>
                                              <?php
                                              if($record->is_registration_fee == '1')
                                              {
                                                echo 'Yes';
                                              }else
                                              {
                                                echo 'No';
                                              }
                                              ?>
                                          </td>
                                          <td>
                                              <?php
                                              if($record->gst_tax == '1')
                                              {
                                                echo 'Yes';
                                              }else
                                              {
                                                echo 'No';
                                              }
                                              ?>
                                          </td>
                                          <td><?php echo $record->amount ?></td>
                                          <!-- <td>
                                            <a href='/prdtm/programme/fee/<?php echo $id_programme;?>/<?php echo $record->id;?>'>Edit</a> | 
                                            <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Delete">Delete</a>
                                          </td> -->
                                        </tr>
                                    <?php
                                    $total_amount = $total_amount + $record->amount;
                                    $i++;
                                      }
                                       $total_amount = number_format($total_amount, 2, '.', ',');
                                      ?>

                                      <tr >
                                          <td bgcolor="" colspan="3"></td>
                                          <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                          <td bgcolor="">
                                            <input type="hidden" id="local_amount" name="local_amount" value="<?php echo $total_amount; ?>">

                                            <b><?php echo $total_amount . " ( " . $feeStructureLocalList[0]->currency_code . " ) ";  ?></b></td>
                                        </tr>
                                      <?php
                                    }
                                    ?>
                                  </tbody>
                                </table>
                              </div>

                          </div>


                        


                        </div> 
                    </div>








                </div>

            </div>
      </div>   

      

   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">
      $('select').select2();


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">


    $('select').select2();

    updateMasterAmount();

    function updateMasterAmount()
    {
        var tempPR = {};
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";

        tempPR['id'] = id_program_landscape;
        tempPR['amount'] = $("#local_amount").val();
        tempPR['international_amount'] = $("#international_amount").val();
        
        // alert(tempPR['international_amount']);

            $.ajax(
            {
               url: '/finance/feeStructure/updateFeeStructureMasterAmount',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // window.location.reload();
               }
            });
      }

  

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id_frequency_mode").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }



    function showInstallments(is_installment)
    {
      // alert(is_installment);
      if(is_installment == 0)
      {
        $('#view_amount').show();
        $('#view_fee_item').show();
      }else
      {

        $('#view_amount').hide();
        $('#view_fee_item').hide();
      }
    }



    function saveData()
    {

      if($('#form_one').valid())
      {

        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_training_center'] = <?php echo '1'; ?>;
        tempPR['id_fee_item'] = $("#one_id_fee_item").val();
        tempPR['id_fee_structure_trigger'] = $("#one_id_fee_structure_trigger").val();
        tempPR['currency'] = 'MYR';
        tempPR['amount'] = $("#one_amount").val();
        tempPR['id_frequency_mode'] = $("#one_id_frequency_mode").val();

            $.ajax(
            {
               url: '/finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                window.location.reload();
               }
            });

      }
        
    }


    function saveDataUSD()
    {

      if($('#form_two').valid())
        {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_training_center'] = <?php echo '1'; ?>;
        tempPR['id_fee_item'] = $("#two_id_fee_item").val();
        tempPR['id_fee_structure_trigger'] = $("#two_id_fee_structure_trigger").val();
        tempPR['currency'] = 'USD';
        tempPR['amount'] = $("#two_amount").val();
        tempPR['id_frequency_mode'] = $("#two_id_frequency_mode").val();
            $.ajax(
            {
               url: '/finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                window.location.reload();
               }
            });

          }
        
    }



    function saveDataPartnerUSD()
    {

      if($('#form_three').valid())
        {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_fee_item'] = $("#three_id_fee_item").val();
        tempPR['currency'] = $("#three_currency").val();
        tempPR['amount'] = $("#three_amount").val();
        tempPR['id_frequency_mode'] = $("#three_id_frequency_mode").val();
        tempPR['id_training_center'] = $("#three_id_training_center").val();
        tempPR['is_installment'] = $("#three_is_installment").val();
        tempPR['installments'] = $("#three_installments").val();



        // alert(tempPR['three_id_fee_item']);
            $.ajax(
            {
               url: '/finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == 0)
                {
                  alert('Duplicate Training Center Data Not Allowed');
                }
                else
                {
                  window.location.reload();
                }
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });

          }
    }

    function viewFeeByTrainingCenter(id_fee_structure)
    {
      // alert(id_fee_structure);
      var tempPR = {};

        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        // tempPR['id_training_center'] = id_training_center;
        tempPR['id_fee_structure'] = id_fee_structure;
        tempPR['id_program_landscape'] = id_program_landscape;

        // alert(tempPR['three_id_fee_item']);
            $.ajax(
            {
               url: '/finance/feeStructure/getTrainingCenterFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // if(result == 0)
                // {
                //   alert('Duplicate Training Center Data Not Allowed');
                // }
                // alert(result);

                $("#view_model").html(result);

                var installment_amount_selected = $("#installment_amount_selected").val();
                var installment_nos = $("#installment_nos").val();
                var id_fee_structure = $("#id_fee_structure").val();     
                var id_training_center = $("#id_data_training_center").val();   
                var id_program_landscape = $("#id_data_program_landscape").val();
                var data_currency = $("#data_currency").val();







                $("#triggering_amount").val(installment_amount_selected);
                $("#triggering_installment_nos").val(installment_nos);
                $("#trigger_id_fee_structure").val(id_fee_structure);
                $("#trigger_id_training_center").val(id_training_center);
                $("#trigger_id_program_landscape").val(id_program_landscape);
                $("#triggering_data_currency").val(data_currency);

                $('#myModal').modal('show');
                // window.location.reload();
               }
            });
    }


    function saveInstallmentData()
    {

      if($('#form_four').valid())
        {
          var tempPR = {};

          tempPR['id_fee_item'] = $("#trigger_id_fee_item").val();
          tempPR['id_fee_structure_trigger'] = $("#id_fee_structure_trigger").val();
          tempPR['id_fee_structure'] = $("#trigger_id_fee_structure").val();
          tempPR['amount'] = $("#triggering_amount").val();
          tempPR['id_training_center'] = $("#trigger_id_training_center").val();
          tempPR['id_semester'] = $("#trigger_id_semester").val();
          tempPR['id_program_landscape'] = $("#trigger_id_program_landscape").val();

            $.ajax(
            {
               url: '/finance/feeStructure/saveInstallmentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
          }
    }




    function tempDelete(id)
    {
      // alert(id);
         $.ajax(
            {
               url: '/finance/feeStructure/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

    function deleteDataByTrainingCenter(id_training_center)
    {
        $.ajax(
            {
               url: '/finance/feeStructure/deleteTrainingCenterByTrainingCenterId/'+id_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

    function deleteSemesterDataByIdFeeStructureTrainingCenter(id_fee_training_center)
    {
      $.ajax(
            {
               url: '/finance/feeStructure/deleteSemesterDataByIdFeeStructureTrainingCenter/'+id_fee_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

 //    function tempDelete(id){

 //     $.get("/finance/feeStructure/tempDelete/"+id, function(data, status){
   
 //        // $("#selectsubcategory").html(data);
 //    });
 // }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/feeStructure/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function()
    {
        $("#form_one").validate({
            rules: {
                one_id_fee_item: {
                    required: true
                },
                one_amount: {
                    required: true
                },
                one_id_frequency_mode: {
                    required: true
                },
                one_id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                one_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                one_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                one_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                one_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_two").validate({
            rules: {
                two_id_fee_item: {
                    required: true
                },
                two_amount: {
                    required: true
                },
                two_id_frequency_mode: {
                    required: true
                },
                two_id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                two_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                two_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                two_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                two_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
    {

        $("#form_three").validate({
            rules: {
                three_id_fee_item: {
                    required: true
                },
                three_amount: {
                    required: true
                },
                three_id_frequency_mode: {
                    required: true
                },
                three_id_training_center: {
                    required: true
                },
                three_is_installment: {
                    required: true
                },
                three_currency: {
                    required: true
                },
                three_installments: {
                  required: true
                },
                id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                three_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                three_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                three_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                three_id_training_center: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                three_is_installment: {
                    required: "<p class='error-text'>Select Installment Status</p>",
                },
                three_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                three_installments: {
                    required: "<p class='error-text'>Installments Required</p>",
                },
                id_fee_structure_trigger: {
                    required: "<p class='error-text'>Installments Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });








    $(document).ready(function()
    {
        $("#form_four").validate({
            rules: {
                trigger_id_semester: {
                    required: true
                },
                trigger_id_fee_item: {
                  required: true
                }
            },
            messages: {
                trigger_id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                trigger_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    


</script>