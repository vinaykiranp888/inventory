<?php
$programme_approval_model = new Programme_approval_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_approval_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">
            <li><a href="/prdtm/programmeApproval/view/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programmeApproval/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            
          <!--   <li><a href="/prdtm/programmeApproval/skill/<?php echo $id_programme;?>">Skills</a></li>
          
          <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programmeApproval/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programmeApproval/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programmeApproval/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programmeApproval/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/prdtm/programmeApproval/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programmeApproval/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li class="active"><a href="/prdtm/programmeApproval/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programmeApproval/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programmeApproval/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programmeApproval/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programmeApproval/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->
          <li><a href="/prdtm/programmeApproval/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/prdtm/programmeApproval/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          <li><a href="/prdtm/programmeApproval/approval/<?php echo $id_programme;?>">Approval</a></li>

        </ul>

         <div role="tabpanel" class="tab-pane" id="program_accerdation">

            <div class="mt-4">


              <?php

              if(!empty($programmeAwardList))
              {
                  ?>

                  <div class="form-container">
                          <h4 class="form-group-title">Award Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                  <th>Sl. No</th>
                                   <th>Remarks</th>
                                   <th>Award Level</th>
                                   <th>Condition</th>
                                   <!-- <th style="text-align: center;">Action</th> -->
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php
                               $total = 0;
                                for($i=0;$i<count($programmeAwardList);$i++)
                               { ?>
                                  <tr>
                                  <td><?php echo $i+1;?></td>
                                  <td><?php echo $programmeAwardList[$i]->name;?></td>
                                  <td><?php echo $programmeAwardList[$i]->award_name ;?></td>
                                  <td><?php
                                  $count = 1;
                                  foreach ($programmeAwardList[$i]->condition_data as $condition)
                                  {
                                    if($count > 1)
                                    {
                                      echo '<br>';
                                    }

                                    echo $count . ". " .  $condition->condition_name;
                                    $count ++;
                                  }
                                  ?></td>
                                  <!-- <td style="text-align: center;">
                                   <a onclick="deleteAwardData(<?php echo $programmeAwardList[$i]->id; ?>)">Delete</a>
                                  </td> -->

                                  </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }
               ?>







          </div>
      
      </div>




         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveAcceredationData()
    {
        if($('#form_main').valid())
        {
          $('#form_main').submit();
        // var tempPR = {};
        // tempPR['name'] = $("#award_name").val();
        // tempPR['id_award'] = $("#id_award").val();
        // tempPR['id_program_condition'] = $("#id_program_condition").val();
        // tempPR['id_program'] = <?php echo $id_programme; ?>;
        // tempPR['id'] = <?php echo $id_programme_award; ?>;

        //     $.ajax(
        //     {
        //        url: '/prdtm/programmeApproval/saveAwardData',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //           window.location.reload();
        //        }
        //     });
        }
    }



    function deleteAwardData(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {

        $.ajax(
            {
               url: '/prdtm/programmeApproval/deleteAwardData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
      }
    }



    $(document).ready(function()
     {
        $("#form_main").validate({
            rules: {
                award_name: {
                    required: true
                },
                id_award: {
                    required: true
                },
                id_program_condition: {
                    required: true
                }
            },
            messages: {
                award_name: {
                    required: "<p class='error-text'>Award Name Required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                id_program_condition: {
                    required: "<p class='error-text'>Select Programme Condition</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>