<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">
            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

          
          <!--
            <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Skills</a></li>

           <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li class="active"><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programme/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->

        </ul>

         <div role="tabpanel" class="tab-pane" id="program_accerdation">
                        <div class="mt-4">


          <form id="form_main" action="" method="post">
              <div class="form-container">
                  <h4 class="form-group-title"> Award Details</h4>

                  <div class="row">

                     
                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Award Remarks <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="award_name" name="award_name" autocomplete="off" value="<?php echo $programmeAward->name; ?>">
                          </div>
                      </div>

                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Award Level <span class='error-text'>*</span></label>
                            <select name="id_award" id="id_award" class="form-control" onchange="getConditionsByAwardId()">
                                <option value="">Select</option>
                                <?php
                                if (!empty($awardList))
                                {
                                    foreach ($awardList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        <?php
                                        if($programmeAward->id_award == $record->id)
                                        {
                                          echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                      </div>


                      <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Condition To Meet <span class='error-text'>*</span></label>
                            <select name="id_program_condition" id="id_program_condition" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programmeConditionList))
                                {
                                    foreach ($programmeConditionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                          <?php
                                        if($programmeAward->id_program_condition == $record->id)
                                        {
                                          echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                      </div> -->




                  </div>


              </div>

              <div class="form-container" id="view_conditions" style="display: none;">
                  <h4 class="form-group-title"> Condition Details</h4>

                  <div id="view_conditions_list" class="row">
                  </div>

              </div>


              <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="button" class="btn btn-primary btn-lg" onclick="saveAcceredationData()">Save</button>
                    <?php
                    if($id_programme_award != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../award/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                      <!-- <a href="../list" class="btn btn-link">Back</a> -->
                  </div>
              </div>

          </form>




              <?php

              if(!empty($programmeAwardList))
              {
                  ?>
                  <br>

                  <div class="form-container">
                          <h4 class="form-group-title">Award Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                  <th>Sl. No</th>
                                   <th>Remarks</th>
                                   <th>Award Level</th>
                                   <th>Conditions</th>
                                   <th style="text-align: center;">Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php
                               $total = 0;
                                for($i=0;$i<count($programmeAwardList);$i++)
                               { ?>
                                  <tr>
                                  <td><?php echo $i+1;?></td>
                                  <td><?php echo $programmeAwardList[$i]->name;?></td>
                                  <td><?php echo $programmeAwardList[$i]->award_name ;?></td>
                                  <td><?php
                                  $count = 1;
                                  foreach ($programmeAwardList[$i]->condition_data as $condition)
                                  {
                                    if($count > 1)
                                    {
                                      echo '<br>';
                                    }

                                    echo $count . ". " .  $condition->condition_name;
                                    $count ++;
                                  }
                                  ?></td>
                                  <td style="text-align: center;">
                                    <a href='/prdtm/programme/award/<?php echo $id_programme;?>/<?php echo $programmeAwardList[$i]->id;?>'>Edit</a> |
                                    <a onclick="deleteAwardData(<?php echo $programmeAwardList[$i]->id; ?>)">Delete</a>
                                  </td>

                                  </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }
               ?>







          </div>
      
      </div>




         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveAcceredationData()
    {
        if($('#form_main').valid())
        {
          $('#form_main').submit();
        // var tempPR = {};
        // tempPR['name'] = $("#award_name").val();
        // tempPR['id_award'] = $("#id_award").val();
        // tempPR['id_program_condition'] = $("#id_program_condition").val();
        // tempPR['id_program'] = <?php echo $id_programme; ?>;
        // tempPR['id'] = <?php echo $id_programme_award; ?>;

        //     $.ajax(
        //     {
        //        url: '/prdtm/programme/saveAwardData',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //           window.location.reload();
        //        }
        //     });
        }
    }


    function getConditionsByAwardId()
    {
      var id_award = $("#id_award").val();
      // alert(id_award);
        if(id_award >0)
        {
            $.ajax(
            {
               url: '/prdtm/programme/getConditionsByAwardId/'+id_award,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_conditions").show();
                  $("#view_conditions_list").html(result);
               }
            });
        }
    }




    function deleteAwardData(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {

        $.ajax(
            {
               url: '/prdtm/programme/deleteAwardData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
      }
    }



    $(document).ready(function()
     {
        $("#form_main").validate({
            rules: {
                award_name: {
                    required: true
                },
                id_award: {
                    required: true
                },
                id_program_condition: {
                    required: true
                }
            },
            messages: {
                award_name: {
                    required: "<p class='error-text'>Award Remarks Required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                id_program_condition: {
                    required: "<p class='error-text'>Select Programme Condition</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>