<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>


            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>
            
            
          <!--   <li class="active"><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Skills</a></li>

          <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            <li><a href="/prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programme/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Skills & Competencies Details</h4>

             
                <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Skills <span class='error-text'>*</span></label>
                       <select name="id_skill" id="id_skill" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($skillList))
                             {
                                 foreach ($skillList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                              <?php
                              if($record->id == $programSkill->id_skill)
                              {
                                echo 'selected';
                              }
                              ?>
                              ><?php echo $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Competencies <span class='error-text'>*</span></label>
                       <select name="id_competency" id="id_competency" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($competencyList))
                             {
                                 foreach ($competencyList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                              <?php
                              if($record->id == $programSkill->id_competency)
                              {
                                echo 'selected';
                              }
                              ?>
                              ><?php echo $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                  </div>

                </div>




                 <div class="button-block clearfix">
                  <div class="form-group">

                      <button type="submit" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                      <?php
                    if($id_programme_skill != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../skill/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                   </div>
                 </div>

         </div>
     
      </form>


      <?php

        if(!empty($programHasSkills))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Skill & Competencies Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Skill</th>
                             <th>Competency</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($programHasSkills);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $programHasSkills[$i]->skill_name;?></td>
                            <td><?php echo $programHasSkills[$i]->competency_name;?></td>
                            <td class="text-center">
                                <a href='/prdtm/programme/skill/<?php echo $id_programme;?>/<?php echo $programHasSkills[$i]->id;?>'>Edit</a> | 
                                <a onclick="deleteProgramHasSkills(<?php echo $programHasSkills[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

    function deleteProgramHasSkills(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
         $.ajax(
            {
               url: '/prdtm/programme/deleteProgramHasSkills/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
       }
    }


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">



CKEDITOR.replace('overview',{
  width: "100%",
  height: "300px"

}); 

</script>