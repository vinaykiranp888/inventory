<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">

            <li><a href="/prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            
            <li><a href="/prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
            <li><a href="/prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
            <li><a href="/prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>
            
            
          <!--   <li><a href="/prdtm/programme/skill/<?php echo $id_programme;?>">Skills</a></li>

            <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li class="active"><a href="/prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            <li><a href="/prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programme/discount/<?php echo $id_programme;?>">Discounts</a></li>

          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->



        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">How To Pass</h4>

             <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                     <textarea type="text" class="form-control" id="description" name="description"><?php echo $howToPass->description; ?></textarea>
                     <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $howToPass->id; ?>">
                  </div>
               </div>
                </div>
                <div class="row">
               
               <div class="col-sm-2 pt-10">
                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-lg" value="Objective" name="save">Save</button>
                 </div>
               </div>
            </div>
         </div>
     
      </form>

   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">
      $('select').select2();


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

CKEDITOR.replace('description',{
  width: "100%",
  height: "300px"

}); 

</script>