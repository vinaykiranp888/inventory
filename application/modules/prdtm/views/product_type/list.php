<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Product Type</h3>
      <a href="add" class="btn btn-primary">+ Add Product Type</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Title</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Title</th>
            <th>Status</th>
            <th>Created By</th>
            <th>Created On</th>
            <th>Last Updated By</th>
            <th>Updated On</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($productTypeList))
          {
          $i=1;
            foreach ($productTypeList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo ucfirst($record->name) ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td><?php echo $record->creater_name ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
                <td><?php echo $record->updater_name ?></td>
                <td><?php if($record->updated_dt_tm){ echo date("d-m-Y", strtotime($record->updated_dt_tm)); } ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>  | 
                  <a href="<?php echo 'productTabs/' . $record->id; ?>" title="Add Tabs">Tabs</a>
                  <!-- <a href="<?php echo 'tabs/' . $record->id; ?>" title="Add Tabs">Tabs</a> -->
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>