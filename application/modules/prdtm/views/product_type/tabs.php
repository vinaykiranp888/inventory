<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Product Type Tabs</h3>
            <?php
            if($id_product_tab == NULL)
            {
            ?>
                <a href="../list" class="btn btn-link"> < Back</a>
            <?php
            }
            ?>
        </div>

 

            <div class="form-container">
                <h4 class="form-group-title">Product Type Details</h4>

                <!-- <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Parent Category <span class='error-text'>*</span></label>
                            <select name="id_parent_product" id="id_parent_product" class="form-control" onchange="showchildCategory(this.value)" disabled>
                                <option value="99999" <?php if($productType->id_parent_product=='99999') 
                                { echo "selected=selected"; } ?> >Top</option>

                                <?php for($i=0;$i<count($getParentProduct);$i++) { ?>
                                <option value="<?php echo $getParentProduct[$i]->id;?>"

                                    <?php if($productType->id_parent_product==$getParentProduct[$i]->id) 
                                { echo "selected=selected"; } ?>

                                ><?php echo $getParentProduct[$i]->name;?></option>
                            <?php } ?> 
                               
                            </select>
                         </div>
                    </div>
               
                    <div class="col-sm-4" id='showChildDiv' style="display: none;">
                        <div class="form-group">
                            <label>Child Category <span class='error-text'>*</span></label>
                                <div id="optionDiv"></div>

                         </div>
                    </div>
                </div> -->


                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Product Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $productType->name;?>" readonly>
                        </div>
                    </div>
              
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Product Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $productType->code;?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description </label>
                            <input type="text" class="form-control" id="description" name="description" value="<?php echo $productType->description;?>" readonly>
                        </div>
                    </div>

                </div>



                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                    </div>

                </div>

            </div>


        <form id="form_main" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Product Type Has Tabs Details</h4>


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Tab <span class='error-text'>*</span></label>
                            <select name="id_tab" id="id_tab" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($tabList))
                                {
                                    foreach ($tabList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        <?php
                                        if($productTab->tab == $record->name)
                                        {
                                          echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->title;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Tab Order <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="order_no" name="order_no" value="<?php echo $order_no; ?>" 
                              <?php
                              if($id_product_tab == NULL)
                              {
                                echo 'readonly';
                              }?>>
                          </div>
                      </div>



                </div>

            </div>




            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <?php
                    if($id_product_tab != NULL)
                    {
                    ?>
                        <a href="../../tabs/<?php echo $id_product_type; ?>" class="btn btn-link">Cancel</a>
                    <?php
                    }
                    ?>
                </div>
            </div>


        </form>



             <?php

              if(!empty($productTabsList))
              {
                  ?>
                  <br>

                  <div class="form-container">
                          <h4 class="form-group-title">Product Type Tab Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                  <th>Sl. No</th>
                                   <th>Tabs</th>
                                   <th>Tab Order</th>
                                   <th style="text-align: center;">Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php
                               $total = 0;
                                for($i=0;$i<count($productTabsList);$i++)
                               { ?>
                                  <tr>
                                  <td><?php echo $i+1;?></td>
                                  <td><?php echo $productTabsList[$i]->name;?></td>
                                  <td><?php echo $productTabsList[$i]->order_no;?></td>
                                  <!-- <td><?php echo $programmeAwardList[$i]->program_condition_name ;?></td> -->
                                  <td style="text-align: center;">
                                  <a href='/prdtm/productType/tabs/<?php echo $id_product_type;?>/<?php echo $productTabsList[$i]->id;?>'>Edit</a> |
                                   <a onclick="deleteProductHasTabs(<?php echo $productTabsList[$i]->id; ?>)">Delete</a>
                                  </td>

                                  </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }

              ?>






        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

</div>
<script>
    
    $('select').select2();

    function deleteProductHasTabs(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
        $.ajax(
            {
               url: '/prdtm/productType/deleteProductHasTabs/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
      }
    }

    $(document).ready(function()
    {
        // showchildCategory(<?php echo $productType->id_parent_product;?>);

        $("#form_main").validate({
            rules: {
                id_tab: {
                    required: true
                }
            },
            messages: {
                id_tab: {
                    required: "<p class='error-text'>Select Tab</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  

</script>