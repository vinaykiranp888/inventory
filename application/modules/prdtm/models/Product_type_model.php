<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_type_model extends CI_Model
{
    function productTypeListSearch($data)
    {
        $this->db->select('c.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('product_type as c');
        $this->db->join('users as cre','c.created_by = cre.id','left');
        $this->db->join('users as upd','c.updated_by = upd.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function productTypeList()
    {
        $this->db->select('*');
        $this->db->from('product_type');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getProductTypeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getParentProduct()
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where("id_parent_product='99999'");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getChildForParent($id)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where("id_parent_product",$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function addNewProductType($data)
    {
        $this->db->trans_start();
        $this->db->insert('product_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProductTypeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('product_type', $data);
        return TRUE;
    }

    function getProductTabsByProductId($id_product)
    {
        $this->db->select('*');
        $this->db->from('product_type_has_tabs');
        $this->db->where('id_product', $id_product);
        $this->db->order_by('order_no', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function getProductTab($id)
    {
        $this->db->select('*');
        $this->db->from('product_type_has_tabs');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getTabList()
    {
        $this->db->select('*');
        $this->db->from('product_tabs');
        $this->db->order_by('title', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function getAllFieldsById($id) {
        $this->db->select('pt.*');
        $this->db->from('product_tabs_has_fields as pt');
        $this->db->where('id_product_tabs', $id);
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function getValueNumber($id,$id_product_tab_has_fields) {
        $this->db->select('pt.*');
        $this->db->from('product_type_has_fields as pt');
        $this->db->where('id_product', $id);
        $this->db->where('id_product_tab_has_fields', $id_product_tab_has_fields);
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function getProductTabListByProductTypeId($id_product)
    {
        $this->db->select('pt.*');
        $this->db->from('product_tabs as pt');
        $query = $this->db->get();
        $results = $query->result();

        return $results;

    }


     function assignedProductTabsByProductId($id_product)
    {
        $this->db->select('pt.*');
        $this->db->from('product_type_has_tabs as pt');
        $this->db->where('id_product', $id_product);
        
        $query = $this->db->get();
        $results = $query->result();

        return $results;

    }

    function getProductOrderTab($data)
    {
        $this->db->select('*');
        $this->db->from('product_type_has_tabs');
        $this->db->where('tab', $data['tab']);
        $this->db->where('id_product', $data['id_product']);
        $query = $this->db->get();
        return $query->row();
    }

    function getTab($id)
    {
        $this->db->select('*');
        $this->db->from('product_tabs');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addProductTypeFields($data) {
        $this->db->trans_start();
        $this->db->insert('product_type_has_fields', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addproductTabs($data) {
        $this->db->trans_start();
        $this->db->insert('product_type_has_tabs', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    function addProductTypeTab($data)
    {
        $this->db->trans_start();
        $this->db->insert('product_type_has_tabs', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProductTypeTab($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('product_type_has_tabs', $data);
        return TRUE;
    }


    function deleteallFields($id) {
        $this->db->where('id_product', $id);
        $this->db->delete('product_type_has_fields');
        return $this->db->affected_rows();
    }

    function deletealltabs($id){
        $this->db->where('id_product', $id);
        $this->db->delete('product_type_has_tabs');
        return $this->db->affected_rows();       
    }
    
    function deleteProductHasTabs($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('product_type_has_tabs');
        return $this->db->affected_rows();
    }
}

