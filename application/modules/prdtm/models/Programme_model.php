<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_model extends CI_Model
{
    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
    
    function statusListByType($type)
    {
        $this->db->select('d.*');
        $this->db->from('status_table as d');
        $this->db->where('status', 1);
        $this->db->where('type', $type);
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
    
    function partnerUniversityListByStatus($status)
    {
        $this->db->select('d.*');
        $this->db->from('partner_university as d');
        $this->db->where('status', $status);
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function partnerUniversityList()
    {
        $this->db->select('d.*');
        $this->db->from('partner_university as d');
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function getExaminationComponent()
    {
        $this->db->select('d.*');
        $this->db->from('examination_components as d');
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function fetFacultySearch()
    {
        $this->db->select('d.*');
        $this->db->from('staff as d');
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function addsyllabus($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateProgrammeSyllabus($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_syllabus', $data);
        return TRUE;
    }

    function addProgrammetoStaff($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    } 

    function addProgramOverview($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_overview', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function assesmentListByStatus($status)
    {
        $this->db->select('d.*');
        $this->db->from('assessment as d');
        $this->db->where('d.status', $status);
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function addProgrammeAssessment($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_assessment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeAssessment($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_assessment', $data);
        return TRUE;
    }

    function getProgrammeAssesment($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_assessment');
        $this->db->where("id",$id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function addProgrammeAssessmentmain($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_assessment_main', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function addtopic($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_topic', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function getNames($ids)
    {
        if($ids)
        {
            $this->db->select('*');
            $this->db->from('programme_has_syllabus');
            $this->db->where("id in ($ids)");
            $query = $this->db->get();
            $result = $query->result();  
            return $result;
        }
    }

    function getAssessmentDetails($id)
    {
        $this->db->select('p.*, e.name as component');
        $this->db->from('programme_has_assessment as p');
        $this->db->join('examination_components as e', 'p.id_examination_components = e.id','left');
        
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAssessmentDetailsByProgrammeId($id_programme)
    {
        $this->db->select('p.*, a.name as assessment_name');
        $this->db->from('programme_has_assessment as p');
        $this->db->join('assessment as a', 'p.id_assesment = a.id','left');
        $this->db->where('p.id_programme', $id_programme);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAssessmentDetailsMain($id) {
        $this->db->select('*');
        $this->db->from('programme_has_assessment_main');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramOverview($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_overview');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeSyllabus($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSyllabus($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where('id', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getprogrammeTopic($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_topic');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function gettopicById($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_topic');
        $this->db->where('id', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function educationLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    
    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function categoryTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function productTypeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function awardListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programmeConditionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme_condition');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function schemeList()
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programmeListSearch($data)
    {
        $this->db->select('p.*, sl.name as competency_level_name, pt.name as product_type, st.name as status_name, pu.code as partner_university_code, pu.name as partner_university_name, fsm.id as id_fee_structure_master, cs.code as currency_code, cs.name as currency_name, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('programme as p');
        $this->db->join('study_level as sl', 'p.id_study_level = sl.id');
        $this->db->join('product_type as pt', 'p.id_programme_type = pt.id');
        $this->db->join('status_table as st', 'p.status = st.id','left');
        $this->db->join('fee_structure_master as fsm', 'p.id = fsm.id_programme','left');
        $this->db->join('partner_university as pu', 'p.id_partner_university = pu.id','left');
        $this->db->join('currency_setup as cs', 'fsm.id_currency = cs.id','left');
        $this->db->join('users as cre','p.created_by = cre.id','left');
        $this->db->join('users as upd','p.updated_by = upd.id','left');
        if ($data['name'] != '')
        {
            $likeCriteria = "(p.name  LIKE '%" . $data['name'] . "%' or p.name_optional_language  LIKE '%" . $data['name'] . "%' or p.code  LIKE '%" . $data['name'] . "%' or p.foundation  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('p.id_category', $data['id_category']);
        }
        if ($data['id_category_setup'] != '')
        {
            $this->db->where('p.id_category_setup', $data['id_category_setup']);
        }
        if ($data['id_programme_type'] != '')
        {
            $this->db->where('p.id_programme_type', $data['id_programme_type']);
        }
        if($data['id_partner_university'] != '')
        {
            $this->db->where('p.id_partner_university', $data['id_partner_university']);
        }
        $this->db->where('p.status !=', 0);
        $this->db->where('p.send_for_approval', 2);
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         $detais = array();
         foreach ($results as $value)
         {
            $total_fee = 0;
            $id_fee_structure_master = $value->id_fee_structure_master;

            if($id_fee_structure_master > 0)
            {
                $get_data['id_fee_structure'] = $id_fee_structure_master;
                $get_data['id_training_center'] = 1;
                $get_data['currency'] = 'MYR';
                $fee_add = 0;
                $fees = $this->getFeeStructureByData($get_data);

                foreach ($fees as $fee)
                {
                    $amount = $fee->amount;
                    $fee_add = $fee_add + $amount;
                }

                $total_fee = $fee_add;
            }

            $value->fee_amount = $total_fee;

            array_push($detais, $value);
         }

         return $detais;
    }

    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id');
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left');
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure']);
        $this->db->where('fst.currency', $data['currency']);
        // $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getProgrammeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    
    function addNewProgrammeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addProgrammeStatusChange($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_status_change', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme', $data);
        return TRUE;
    }

    function updatetopic($data,$idtopic)
    {
        $this->db->where('id', $idtopic);
        $this->db->update('programme_has_topic', $data);
        return TRUE;
    }

    function addNewTempProgrammeHasDean($data)
    {

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasDean($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewProgrammeHasCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addTempCourseDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_programme_has_dean', $data);
        return TRUE;
    }

    function updateTempCourseDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_programme_has_course', $data);
        return TRUE;
    }

    function getTempProgrammeHasDean($id_session)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation');
        $this->db->from('temp_programme_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempProgrammeHasCourse($id_session)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('temp_programme_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasDean($id_programme)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation,st.ic_no');
        $this->db->from('programme_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');     
        $this->db->where('tphd.id_programme', $id_programme);
        $this->db->order_by("st.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasCourse($id)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('programme_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_programme', $id);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_programme_has_dean');
    }

    function deleteassessmentmain($id) {
        $this->db->where('id_programme', $id);
       $this->db->delete('programme_has_assessment_main');

    }

    function deleteprogramoverview($id) {
        $this->db->where('id_programme', $id);
       $this->db->delete('programme_has_overview');

    }

    function deleteTempProgHasDeanDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_programme_has_dean');
    }

    function deleteTempCourseData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_programme_has_course');
    }

    function deleteProgrammeHasDean($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('programme_has_dean');
    }

    function deleteProgrammeHasCourse($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('programme_has_course');
    }

    function addTempProgramHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_programme_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempProgrammeHasScheme($id_session)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('temp_programme_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');     
        $this->db->where('tphd.id_session', $id_session);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempProgramHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_programme_has_scheme');
        return TRUE;
    }

    function deleteTempProgramHasSchemeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_programme_has_scheme');
        return TRUE;
    }

    function addNewProgrammeHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgrammeHasScheme($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');
        $this->db->where('tphd.id_program', $id);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programmeObjectiveList($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_programme', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeCourseOverviewList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_course_overview as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeDeliveryModeList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_delivery_mode as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeAssesmentList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_assesment as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeSyllabusList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_syllabus as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getDeliveryMode(){
        $this->db->select('tphd.*');
        $this->db->from('delivery_mode as tphd');
        $query = $this->db->get();
        return $query->result();
    }

    function getStudyLevel()
    {
        $this->db->select('tphd.*');
        $this->db->from('study_level as tphd');
        $query = $this->db->get();
        return $query->result();
    }


    function programmeObjective($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_programme', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeCourseOverview($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_course_overview as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeDeliveryMode($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_delivery_mode as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeAssesment($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_assesment as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeDiscount($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('programme_discount as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeSyllabus($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_syllabus as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function deleteProgrammeHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_scheme');
        return TRUE;
    }

    function programmeAwardList($id_programme)
    {
        $this->db->select('tphd.*, aw.code as award_code, aw.name as award_name');
        $this->db->from('programme_has_award as tphd');
        $this->db->join('award as aw', 'tphd.id_award = aw.id');     
        // $this->db->join('programme_condition as pc', 'tphd.id_program_condition = pc.id');     
        $this->db->where('tphd.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        // echo "<Pre>";print_r($results);exit;

        foreach ($results as $value)
        {
            $id_award = $value->id_award;

            $condition_data = array();

            $conditions = $this->getAwardConditionList($id_award);
            // echo "<Pre>";print_r($conditions);exit;
            
            if($conditions)
            {
                $condition_data = $conditions;
            }

            $value->condition_data = $condition_data;

            array_push($details, $value);
        }
        return $details;
    }

    function programmeAward($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('programme_has_award as tphd');
        // $this->db->join('award as aw', 'tphd.id_award = aw.id');     
        // $this->db->join('programme_condition as pc', 'tphd.id_program_condition = pc.id');     
        $this->db->where('tphd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function programmeLearningModeList($id_program)
    {
        $this->db->select('tphd.*, sc.name as program_name, sc.code as program_code');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->join('program_type as sc', 'tphd.id_program_type = sc.id');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeSchemeList($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('program_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');
        $this->db->where('tphd.id_program', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeMajoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_major_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeMinoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_minor_details as tphd');  
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeConcurrentList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_concurrent_program as tphd');   
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeAcceredationList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_acceredation_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeAcceredation($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_acceredation_details as tphd');
        $this->db->where('tphd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewProgrammeHasMajor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_major_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasMinor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_minor_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasConcurrent($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_concurrent_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasAcceredation($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_acceredation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeHasAcceredation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_has_acceredation_details', $data);
        return TRUE;
    }

    function addNewProgramSchemeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveProgramObjectiveData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_objective', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveCourseOverviewData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_course_overview', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveDeliveryModeData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_delivery_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveAssesmentData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_assesment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveSyllabusData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveDiscountData($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_discount', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateDiscountData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_discount', $data);
        return $this->db->affected_rows();
    }

    function deleteDiscountdata($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_discount');
        return TRUE;
    }

    function deleteProgramObjectiveDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_objective');
        return TRUE;
    }

    function deleteCourseOverviewDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_course_overview');
        return TRUE;
    }


    function deleteDeliveryModeDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_delivery_mode');
        return TRUE;
    }


    function deleteAssesmentDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_assesment');
        return TRUE;
    }


    function deleteSyllabusDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_syllabus');
        return TRUE;
    }


    function updateAllFacilitatorToInactive($idprogramme)
    {
        $data['status'] = 0;
        $this->db->where('id_programme', $idprogramme);
        $this->db->update('programme_has_dean', $data);
        return $this->db->affected_rows();
    }

     function updateFacilitatorToActive($id)
    {
        $data['status'] = 1;
        $this->db->where('id', $id);
        $this->db->update('programme_has_dean', $data);
        return $this->db->affected_rows();
    }


    function updateProgramObjectiveData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_objective', $data);
        return $this->db->affected_rows();
    }

    function updateCourseOverviewData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_course_overview', $data);
        return $this->db->affected_rows();
    }

    function updateDeliveryModeData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_delivery_mode', $data);
        return $this->db->affected_rows();
    }

    function updateAssesmentData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_assesment', $data);
        return $this->db->affected_rows();
    }

    function updateSyllabusData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_syllabus', $data);
        return $this->db->affected_rows();
    }


    function deleteAcceredationDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_acceredation_details');
        return TRUE;
    }

    function saveAwardData($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_award', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateAwardData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_award', $data);
        return $this->db->affected_rows();
    }

    function deleteAwardData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_award');
        return TRUE;
    }

    function deleteAssesmentData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_assessment');
        return TRUE;
    }

    function deleteTopicData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_topic');
        return TRUE;
    }

    function deleteSyllabusData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_syllabus');
        return TRUE;
    }

    function addProgramStructure($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgramStructure($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_structure');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteProgramStructure($id)
    {
        $this->db->where('id_programme', $id);
        $this->db->delete('programme_has_structure');
        return TRUE;
    }

    function addProgramAim($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_aim', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getProgramAim($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_aim');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteProgramAim($id)
    {
        $this->db->where('id_programme', $id);
        $this->db->delete('programme_has_aim');
        return TRUE;
    }

    function addProgramModules($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_ref_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateProgramModules($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_ref_program', $data);
        return $this->db->affected_rows();
    }

    function getProgramHasModules($id)
    {
        $this->db->select('phrp.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('programme_has_ref_program as phrp');
        $this->db->join('programme as p', 'phrp.id_child_programme = p.id');
        $this->db->where('phrp.id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramModule($id)
    {
        $this->db->select('phrp.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('programme_has_ref_program as phrp');
        $this->db->join('programme as p', 'phrp.id_child_programme = p.id');
        $this->db->where('phrp.id', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function deleteProgramHasModules($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_ref_program');
        return TRUE;
    }

    function getProgrammeListByIdCategory($id_category)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id_category', $id_category);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getSkillListByStatus($status)
    {
        $this->db->select('tphd.*');
        $this->db->from('skill as tphd');
        $this->db->where('tphd.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getCompetencyListByStatus($status)
    {
        $this->db->select('tphd.*');
        $this->db->from('competency as tphd');
        $this->db->where('tphd.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function addProgrammeHasSkills($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_skills', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateProgrammeHasSkills($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_skills', $data);
        return $this->db->affected_rows();
    }


    function getProgramHasSkills($id_programme)
    {
        $this->db->select('tphd.*, sk.name as skill_name, c.name as competency_name');
        $this->db->from('programme_has_skills as tphd');
        $this->db->join('skill as sk', 'tphd.id_skill = sk.id');
        $this->db->join('competency as c', 'tphd.id_competency = c.id','left');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgramSkill($id)
    {
        $this->db->select('tphd.*, sk.name as skill_name');
        $this->db->from('programme_has_skills as tphd');
        $this->db->join('skill as sk', 'tphd.id_skill = sk.id');
        $this->db->where('tphd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteProgramHasSkills($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_skills');
        return TRUE;
    }    


    function getResourcesListByStatus($status)
    {
        $this->db->select('tphd.*');
        $this->db->from('resources as tphd');
        $this->db->where('tphd.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function addProgrammeHasResources($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_resources', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateProgrammeHasResources($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_has_resources', $data);
        return $this->db->affected_rows();
    }

    function getProgramHasResources($id_programme)
    {
        $this->db->select('phr.*, r.name as resource_name');
        $this->db->from('programme_has_resources as phr');
        $this->db->join('resources as r', 'phr.id_resource = r.id');
        $this->db->where('phr.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteProgramHasResources($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_resources');
        return TRUE;
    }

    function getProgrammeDuplication($data)
    {
        $this->db->select('st.*');
        $this->db->from('programme as st');        
        if($data['id_programme'] != '')
        {
            $this->db->where('st.id !=', $data['id_programme']);
        }
        if($data['code'] != '')
        {
            $this->db->where('st.code', $data['code']);
        }
        if($data['name'] != '')
        {
            $this->db->where('st.name', $data['name']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getProductTabsByProductId($id_product)
    {
        $this->db->select('DISTINCT(ptt.tab) as tab, ptt.name as title, ptt.order_no');
        $this->db->from('product_type_has_tabs as ptt');
        $this->db->where('ptt.id_product', $id_product);
        $this->db->where('ptt.tab !=', 'edit');
        $this->db->order_by('ptt.order_no', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function addNewFeeStructureMaster($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_master', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProductFieldsByProductType($id)
    {
        $this->db->select('ptf.*, pthf.id as if_field, pthf.field_name');
        $this->db->from('product_type_has_fields as ptf');
        $this->db->join('product_tabs_has_fields as pthf', 'pthf.id = ptf.id_product_tab_has_fields');
        $this->db->where('ptf.id_product', $id);
        $this->db->where('ptf.value_no', 1);
        $this->db->or_where('ptf.value_no', 2);
        $query = $this->db->get();
        return $query->result();
    }

    function getAwardConditionList($id_award)
    {
        $this->db->select('ac.*, c.name as condition_name');
        $this->db->from('award_condition as ac');
        $this->db->join('programme_condition as c', 'ac.id_condition = c.id');
        $this->db->where('ac.status', 1);
        $this->db->where('ac.id_award', $id_award);
        $query = $this->db->get();
        return $query->result();        
    }

    function getProgrammeStatusChangeByIdProgramme($id_programme)
    {
        $this->db->select('psc.*, st.name as status_name, u.name as created_by');
        $this->db->from('programme_status_change as psc');
        $this->db->join('status_table as st', 'psc.status = st.id');
        $this->db->join('users as u', 'psc.created_by = u.id');
        $this->db->where('psc.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHowToPassByProgrammeId($id_programme)
    {
        $this->db->select('psc.*');
        $this->db->from('programme_how_to_pass as psc');
        $this->db->where('psc.id_programme', $id_programme);
        $this->db->order_by('psc.id', 'DESC');
        $query = $this->db->get();
        return $query->row();
    }

    function editProgrammeHowToPass($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_how_to_pass', $data);
        return $this->db->affected_rows();
    }

    function addProgrammeHowToPass($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_how_to_pass', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getFeeStructureMasterByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where("id_programme",$id_programme);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('tphd.*');
        $this->db->from('currency_setup as tphd');
        $this->db->where('tphd.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getFeeStructureMaster($id)
    {
        $this->db->select('a.*, p.name as program, p.code as program_code, cs.code as currency_code, cs.name as currency_name');
        $this->db->from('fee_structure_master as a');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('currency_setup as cs', 'a.id_currency = cs.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function feeSetupListByStatus($status)
    {
        $this->db->select('fs.*');
        $this->db->from('fee_setup as fs');
        $this->db->where('fs.status', $status);
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeStructureByIdProgrammeNIdIntake($id_program_landscape,$currency)
    {
       $this->db->select('p.*, fs.code as fee_structure_code, fs.name as fee_structure, fs.gst_tax,cs.code as currency_code');
        $this->db->from('fee_structure as p, currency_setup as cs');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $this->db->where('p.id_training_center', 1);
        $this->db->where('cs.id', 1);
        $this->db->where('p.currency', $currency);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFeeStructureDetail($id)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewFeeStructure($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateFeeStructureDetail($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_structure', $data);
        return TRUE;
    }

    function deleteFeeStructure($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure');
        return TRUE;
    }


    function editMarkDistribution($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('marks_distribution', $data);
        return TRUE;
    }

    function addMarkDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('marks_distribution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getMarkDistributionByProgramme($id_programme)
    {
        $this->db->select('md.*, ec.name as exam_component');
        $this->db->from('marks_distribution as md');
        $this->db->join('exam_components as ec', 'md.id_exam_component = ec.id');
        $this->db->where('md.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getMarkDistribution($id)
    {
        $this->db->select('*');
        $this->db->from('marks_distribution');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteMarksDistribution($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('marks_distribution');
        return TRUE;
    }

    function examComponentsListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_components');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
}