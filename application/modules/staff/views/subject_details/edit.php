<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Subject</h3>
        </div>
        <form id="form_subject" action="" method="post">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $subjectDetails->name;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code</label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $subjectDetails->code;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status</p>
                            <label class="radio-inline">
                              <input type="radio" name="active_radio" id="inlineRadio11" value="active" 
                              <?php if($subjectDetails->status=='1') {
                                 echo "checked=checked";
                              };?>>

                              <span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="inactive_radio" id="inlineRadio21" value="inactive"
                               <?php if($subjectDetails->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_subject").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                }
            },
            messages:
            {
                name:
                {
                    required: "Subject Name Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>