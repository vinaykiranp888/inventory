<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Campaign</h3>
        </div>


    <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
        <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                aria-controls="tab_one" aria-selected="true"
                role="tab" data-toggle="tab">Campaign Details</a>
        </li>

        <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                aria-controls="tab_two" role="tab" data-toggle="tab">Student Tagging Detaiils</a>
        </li>

    </ul>

    
    <div class="tab-content offers-tab-content">

        <div role="tabpanel" class="tab-pane active" id="tab_one">
            <div class="col-12 mt-4">


                <form id="form_category" action="" method="post">

                    <div class="form-container">
                            <h4 class="form-group-title">Campaign Details</h4>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $campaign->name; ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Start Date <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y',strtotime($campaign->start_date)); ?>" autocomplete="off" readonly>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>End Date <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y',strtotime($campaign->end_date)); ?>" autocomplete="off" readonly>
                                </div>
                            </div>

                        </div>


                        <div class="row">


                            <div class="col-sm-12">
                                <div class="form-group shadow-textarea">
                                  <label for="message">Description <span class='error-text'>*</span></label>
                                  <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $campaign->description; ?></textarea>
                                </div>
                            </div>


                        </div>



                    </div>

                    <div class="button-block clearfix">
                        <div class="bttn-group">
                            <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                            <a href="../list" class="btn btn-link">Back</a>
                        </div>
                    </div>

                
                </form>





            </div>

        </div>



        <div role="tabpanel" class="tab-pane" id="tab_two">
            <div class="col-12 mt-4">




                <form id="form_student" action="" method="post">

                    <div class="form-container">
                            <h4 class="form-group-title">Student Search</h4>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Programme </label>
                                    <select name="id_program" id="id_program" class="form-control" >
                                        <option value="">-- All --</option>
                                        <?php
                                        if (!empty($programmeList))
                                        {
                                            foreach ($programmeList as $record)
                                            {?>
                                         <option value="<?php echo $record->id;  ?>">
                                            <?php echo $record->code . " - " . $record->name;?>
                                         </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>  

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Student Name</label>
                                    <input type="text" class="form-control" id="full_name" name="full_name">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Student Email</label>
                                    <input type="text" class="form-control" id="email_id" name="email_id">
                                </div>
                            </div>        


                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
                            </div>




                        </div>


                    </div>

                    <div class="form-container" style="display: none;" id="view_student_display">
                        <h4 class="form-group-title">Supervisor Tagging For Student</h4>


                        <div  id='view_student'>
                        </div>

                    </div>




                    <div class="button-block clearfix">
                        <div class="bttn-group">
                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                            <a href="../list" class="btn btn-link">Back</a>
                        </div>
                    </div>

                </form>






                <div class="form-container">
                    <h4 class="form-group-title">Student Campaign Tagging</h4>


                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Student NRIC</th>
                            <th>Student Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Programme</th>
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($campaignStudentTagging))
                          {
                            $i=1;
                            foreach ($campaignStudentTagging as $record)
                            {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->nric ?></td>
                                <td><?php echo $record->student_name ?></td>
                                <td><?php echo $record->email_id ?></td>
                                <td><?php echo $record->phone ?></td>
                                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                                <td class="text-center">
                                    <a onclick="deleteCampaignStudentTagging(<?php echo $record->id; ?>)">Delete</a>
                                </td>
                              </tr>
                          <?php
                            $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>


                </div>







            </div>

        </div>



    </div>






        


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "800px",
  height: "200px"

}); 

</script>

<script>

    $(function()
    {
        $(".datepicker").datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
    });


    $(document).ready(function() {
        $("#form_category").validate({
            rules: {
                name: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Category Name required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>End Date</p>",
                },
                description: {
                    required: "<p class='error-text'>Descripition Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function searchStudents()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['full_name'] = $("#full_name").val();
        tempPR['email_id'] = $("#email_id").val();
        tempPR['id_campaign'] = <?php echo $campaign->id ?>;
            $.ajax(
            {
               url: '/mrktngm/campaign/searchStudents',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_display").show();
                $("#view_student").html(result);
                // $('#myModal').modal('hide');
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }


    function deleteCampaignStudentTagging(id)
    {
         $.ajax(
            {
               url: '/mrktngm/campaign/deleteCampaignStudentTagging/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    // $("#view_student").html(result);
                    window.location.reload();
               }
            });
    }



</script>

<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>