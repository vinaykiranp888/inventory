<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Campaign Attendance Details</h3>
            </div>


    <form id="form_main" action="" method="post">

        <div class="form-container">

            <h4 class="form-group-title">Campaign Attendance Details</h4>


            


        
                   
                <h3 class="header-title">
                    Welcome to Campaign Attendance <span> <b><?php echo $campaign->name . " From " . date('d-m-Y',strtotime($campaign->start_date)) . " To " . date('d-m-Y',strtotime($campaign->end_date)) ?></b></span>
                </h3>



            <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($student->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $student->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $student->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $student->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $student->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $student->programme_code . " - " . $student->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $student->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $student->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $student->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>


            </div>


                <div class="row">


                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                          <label for="message">Description <span class='error-text'>*</span></label>
                          <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $campaign->description; ?></textarea>
                        </div>
                    </div>


                </div>




                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                        <button type="button" class="btn btn-primary btn-lg" onclick="recordVote()">Raise Your Hand</button>
                        <!-- <a href="list" class="btn btn-link">Back</a> -->
                  </div>

                </div> 

            </div> 

    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "800px",
  height: "200px"

}); 

</script>

<script>

    function recordVote()
    {
        var tempPR = {};
        tempPR['id_campaign_tagging'] = <?php echo $campaignPooling->id ?>;
        tempPR['id_campaign'] = <?php echo $campaign->id ?>;
        tempPR['random_number'] = "<?php echo $campaignPooling->random_number ?>";
            $.ajax(
            {
               url: '/mrktngm/campaign/recordVote',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                    alert('Attendance Recorded')
                    window.location.reload();
               }
            });
    }


   $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>