<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Category Modules</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>

        <div class="page-container">

          <div>
            <h4 class="form-title">Category details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                 <!--    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo $courseDetails->code; ?>">
                        </div>
                      </div>
                    </div> -->


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $category->name; ?>" readonly>
                        </div>
                      </div>
                    </div>

             

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if($category->status == 1){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if($category->status == 0){
                                echo "checked=checked";
                            } ?> >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       

            </div>                                
        </div>

      <br>



      <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Module details</h4>
          </div>

            <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Module</label>
                        <div class="col-sm-8">
                          <select name="id_course" id="id_course" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($courseList))
                          {
                              foreach ($courseList as $record)
                              {?>
                           <option value="<?php echo $record->id;  ?>">
                              <?php echo $record->code . " - " . $record->name;?>
                           </option>
                          <?php
                              }
                          }
                          ?>
                        </select>
                        </div>
                      </div>
                    </div>   

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>

      </form>


      <br>


      <?php

        if(!empty($getCategoryHasModule))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Category Has Module Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Module</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($getCategoryHasModule);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $getCategoryHasModule[$i]->course_code . " - " . $getCategoryHasModule[$i]->course_name; ?></td>
                            <td class="text-center">
                            <a onclick="deleteCategoryHasModule(<?php echo $getCategoryHasModule[$i]->id; ?>)">Delete</a>
                            </td>
                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>









</main>

<script>


    $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }

    function deleteCategoryHasModule(id)
    {
      $.ajax(
      {
         url: '/cm/category/deleteCategoryHasModule/'+id,
         type: 'GET',
         error: function()
         {
          alert('Something is wrong');
         },
         success: function(result)
         {
          window.location.reload();
         }
      });
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_course: {
                    required: true
                }
            },
            messages: {
                id_course: {
                    required: "<p class='error-text'>Select Module</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>