<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Campaign extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('campaign_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('campaign.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['campaignList'] = $this->campaign_model->campaignListSearch($formData);

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Campaign Type';
            $this->loadViews("campaign/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('campaign.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'status' => $status
                );
            
                $result = $this->campaign_model->addNewCampaign($data);
                redirect('/mrktngm/campaign/list');
            }

            $this->global['pageTitle'] = 'Inventory Management : Add Campaign Type';
            $this->loadViews("campaign/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('campaign.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/mrktngm/campaign/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'status' => $status
                );

                $result = $this->campaign_model->editCampaign($data,$id);
                redirect('/mrktngm/campaign/list');
            }
            
            $data['campaign'] = $this->campaign_model->getCampaign($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Campaign Type';
            $this->loadViews("campaign/edit", $this->global, $data, NULL);
        }
    }



    function addStudent($id = NULL)
    {
        if ($this->checkAccess('campaign.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/mrktngm/campaign/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $id_students = $this->security->xss_clean($this->input->post('id_student'));
                $id_campaign = $id;

                foreach ($id_students as $id_student)
                {
                    // $student = $this->campaign_model->getStudent($id_student);

                    $uniqueId = rand(0000000000,9999999999);
                    
                    $data = array(
                        'id_student' => $id_student,
                        'id_campaign' => $id_campaign,
                        'random_number' => md5($uniqueId),
                        'status' => 1
                    );

                    $result = $this->campaign_model->addStudentCampaignTagging($data);
                }

                redirect('/mrktngm/campaign/addStudent/'.$id);
            }
            
            $data['campaign'] = $this->campaign_model->getCampaign($id);
            $data['programmeList'] = $this->campaign_model->programmeListByStatus('1');
            $data['campaignStudentTagging'] = $this->campaign_model->getCampaignStudentTagging($id);

            // echo "<Pre>";print_r($data['campaignStudentTagging']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Student To Campaign';
            $this->loadViews("campaign/add_student", $this->global, $data, NULL);
        }
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo "<Pre>";print_r($tempData);exit();

        $student_data = $this->campaign_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         // $table = "

         // <script type='text/javascript'>
         //     $('select').select2();
         // </script>

         // <h4>Supervisor Tagging For Students</h4>


         // <div class='row'>
         //    <div class='col-sm-4'>
         //        <div class='form-group'>
         //        <label>Select Supervisor </label>
         //        <select name='id_advisor_for_tagging' id='id_advisor_for_tagging' class='form-control'>";
         //    $table.="<option value=''>Select</option>";

         //    for($i=0;$i<count($staffList);$i++)
         //    {

         //    // $id = $results[$i]->id_procurement_category;
         //    $id = $staffList[$i]->id;
         //    $type = $staffList[$i]->type;
         //    if($type != '' && $type == 0)
         //    {
         //        $type = 'External';

         //    }elseif($type == 1)
         //    {
         //        $type = 'Internal';
         //    }
         //    $full_name = $staffList[$i]->full_name;
         //    $table.="<option value=".$id.">".$type . " - " . $full_name .
         //            "</option>";

         //    }
         //    $table .="
         //        </select>
         //        </div>
         //      </div>

         //    </div>
         //    ";


         $table = "
         <br>
         <h4> Select Students For Campaign Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Program</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $qualification_code = $student_data[$i]->qualification_code;
                $qualification_name = $student_data[$i]->qualification_name;

                // if($type != '' && $type == 0)
                // {
                //     $type = 'External';

                // }elseif($type == 1)
                // {
                //     $type = 'Internal';
                // }

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>                           
                    <td>$program_code - $program_name</td>
                    <td>$email_id</td>                      
                    <td>$phone</td>                
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }

    function deleteCampaignStudentTagging($id)
    {
        $id_deleted = $this->campaign_model->deleteCampaignStudentTagging($id);

        echo $id_deleted;exit;
    }



    function campaignPooling($uniq_id)
    {
        // echo "<Pre>";print_r($uniq_id);exit();
        
        if ($uniq_id == '')
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $campaign_pooling = $this->campaign_model->getCampaignPoolingDataByRandomNumber($uniq_id);

            if(!$campaign_pooling)
            {
                redirect('/mrktngm/campaign/pageNotFound');
            }
            $pooling = $campaign_pooling->pooling;
            // echo "<Pre>";print_r($campaign_pooling);exit();

            if($pooling != 0)
            {
                redirect('/mrktngm/campaign/votingRecorded/'.$uniq_id);
            }

            $id_campaign = $campaign_pooling->id_campaign;
            $id_campaign_tagging = $campaign_pooling->id;
            $id_student = $campaign_pooling->id_student;

            // echo "<Pre>";print_r($campaign_pooling);exit();


            if($this->input->post())
            {
            
                echo "<Pre>";print_r($this->input->post());exit();

                $id_participant = $this->security->xss_clean($this->input->post('id_participant'));

                $voting_data = array(
                    "machine_ip"=>$_SERVER['REMOTE_ADDR'],
                    "browser_info"=>getBrowserAgent(),
                    "agent_string"=>$this->agent->agent_string(),
                    "platform"=>$this->agent->platform(),
                    "id_participant"=>$id_participant,
                    "is_voted"=>$id_participant,
                    "voting_date_time"=>date('Y-m-d H:i:s')

                );
                
                // echo "<Pre>";print_r($voting_data);exit();

                $result = $this->campaign_model->updateVotingPool($voting_data,$id_pooling);
                redirect('/mrktngm/campaign/votingRecorded/'.$uniq_id);
            }

            $data['campaign'] = $this->campaign_model->getCampaign($id_campaign);
            $data['student'] = $this->campaign_model->getStudent($id_student);
            $data['campaignPooling'] = $campaign_pooling;

            // echo "<Pre>";print_r($data['campaign']);exit();

            $this->global['pageTitle'] = 'Campaign Pooling : Add Communication Group';
            $this->loadViews("campaign_pooling/pooling", $this->global, $data, NULL);
        }
    }

    function pageNotFound()
    {
        $data['message'] = 'No User String Available ';

        $this->global['pageTitle'] = 'Election Management : User Not Autherized';
        $this->loadViews("campaign_pooling/page_not_found", $this->global, $data, NULL);
    }

    function votingRecorded($uniq_id)
    {
        $campaign_pooling = $this->campaign_model->getCampaignPoolingDataByRandomNumber($uniq_id);
        $pooling = $campaign_pooling->pooling;
        if($pooling == 0)
        {
            redirect('/mrktngm/campaign/campaignPooling/'.$uniq_id);
        }

        $id_campaign = $campaign_pooling->id_campaign;
        $id_campaign_pooling = $campaign_pooling->id;
        $id_student = $campaign_pooling->id_student;

        
        $data['campaign'] = $this->campaign_model->getCampaign($id_campaign);
        $data['student'] = $this->campaign_model->getStudent($id_student);
        $data['campaignPooling'] = $campaign_pooling;

        // echo "<Pre>";print_r($data['campaignPooling']);exit();


        $data['message'] = ' Thank You For Voting <br> Your Vote Recorded ';

        $this->global['pageTitle'] = 'Inventory Management : Voting Already Recorded';
        $this->loadViews("campaign_pooling/pooling_thankyou", $this->global, $data, NULL);
    }


    function recordVote()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $student_data = $this->campaign_model->recordVote($tempData);
        echo "<Pre>";print_r($student_data);exit();
    }
}