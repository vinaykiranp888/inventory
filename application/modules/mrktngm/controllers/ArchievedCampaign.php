<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ArchievedCampaign extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('archieved_campaign_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('archieved_campaign.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['archievedCampaignList'] = $this->archieved_campaign_model->archievedCampaignListSearch($formData);

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Archieved Campaign Type';
            $this->loadViews("archieved_campaign/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('archieved_campaign.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'status' => $status
                );
            
                $result = $this->archieved_campaign_model->addNewArchievedCampaign($data);
                redirect('/mrktngm/archievedCampaign/list');
            }

            $this->global['pageTitle'] = 'Inventory Management : Add Archieved Campaign Type';
            $this->loadViews("archieved_campaign/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('archieved_campaign.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/mrktngm/archievedCampaign/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'status' => $status
                );

                $result = $this->archieved_campaign_model->editArchievedCampaign($data,$id);
                redirect('/mrktngm/archieved_campaign/list');
            }
            
            $data['archievedCampaign'] = $this->archieved_campaign_model->getArchievedCampaign($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Archieved Campaign Type';
            $this->loadViews("archieved_campaign/edit", $this->global, $data, NULL);
        }
    }
}
