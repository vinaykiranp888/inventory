<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CategoryType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('category_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            
            $data['categoryTypeList'] = $this->category_type_model->categoryTypeList();

            // print_r($subjectDetails);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Category Type';
            $this->loadViews("category_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('category_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $category_name = $this->security->xss_clean($this->input->post('category_name'));

            
                $data = array(
                    'category_name' => $category_name

                );
            
                $result = $this->category_type_model->addNewCategoryType($data);
                redirect('/mrktngm/categoryType/list');
            }

            $this->global['pageTitle'] = 'Inventory Management : Add Category Type';
            $this->loadViews("category_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('category_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/mrktngm/categoryType/list');
            }
            if($this->input->post())
            {
                $category_name = $this->security->xss_clean($this->input->post('category_name'));

                $data = array(
                    'category_name' => $category_name
                );
                $result = $this->category_type_model->editCategoryTypeDetails($data,$id);
                redirect('/mrktngm/categoryType/list');
            }
            $data['categoryTypeDetails'] = $this->category_type_model->getCategoryTypeDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Category Type';
            $this->loadViews("category_type/edit", $this->global, $data, NULL);
        }
    }
}
