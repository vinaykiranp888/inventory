<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Archieved_campaign_model extends CI_Model
{
    function archievedCampaignListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('campaign');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit(10, 0);
        $this->db->where('end_date <', date('d-m-Y'));
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getArchievedCampaign($id)
    {
        $this->db->select('*');
        $this->db->from('campaign');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewArchievedCampaign($data)
    {
        $this->db->trans_start();
        $this->db->insert('campaign', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editArchievedCampaign($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('campaign', $data);
        return TRUE;
    }
    
    function deleteArchievedCampaign($id, $date)
    {
        $this->db->where('id', $countryId);
        $this->db->update('campaign', $countryInfo);
        return $this->db->affected_rows();
    }
}