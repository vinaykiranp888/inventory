<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Campaign_model extends CI_Model
{
    function campaignListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('campaign');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->limit(10, 0);
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    function getCampaign($id)
    {
        $this->db->select('*');
        $this->db->from('campaign');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewCampaign($data)
    {
        $this->db->trans_start();
        $this->db->insert('campaign', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCampaign($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('campaign', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($id, $date)
    {
        $this->db->where('id', $countryId);
        $this->db->update('academic_year', $countryInfo);
        return $this->db->affected_rows();
    }

    function studentSearch($data)
    {
        $this->db->select('s.*, p.code as program_code, p.name as program_name, qs.short_name as qualification_code, qs.name as qualification_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id','left');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%' or s.nric  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['id_program'] != '')
        {
            $this->db->where('s.id_program', $data['id_program']);
        }
        if ($data['id_campaign'] != '')
        {
            $likeCriteria = " s.id NOT IN (SELECT id_student FROM campaign_student_tagging as cst where cst.id_campaign = " . $data['id_campaign'] . ")";
            $this->db->where($likeCriteria);                
        }

        $this->db->order_by('s.full_name', 'ASC');
        $query = $this->db->get();
        $result = $query->result(); 
        // echo "<pre>";print_r($query);die;

        return $result;
    }


    function getCampaignStudentTagging($id)
    {
        $this->db->select('cst.*, s.full_name as student_name, s.nric, s.email_id, s.phone, p.code as programme_code, p.name as programme_name');
        $this->db->from('campaign_student_tagging as cst');
        $this->db->join('student as s', 'cst.id_student = s.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->where('id_campaign', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function addStudentCampaignTagging($data)
    {
        $this->db->trans_start();
        $this->db->insert('campaign_student_tagging', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteCampaignStudentTagging($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('campaign_student_tagging');
        return $this->db->affected_rows();
    }

    function getStudent($id)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCampaignPoolingDataByRandomNumber($uniq_id)
    {
        $this->db->select('*');
        $this->db->from('campaign_student_tagging');
        $this->db->where('random_number', $uniq_id);
        // $this->db->where('pooling', 0);
        $query = $this->db->get();
        return $query->row();
    }

    function recordVote($data)
    {
        $id_campaign_tagging = $data['id_campaign_tagging'];
        unset($data['id_campaign_tagging']);
        $data['pooling'] = 1;
        $data['pooling_date_time'] = date('Y-m-d H:i:s');

        $this->db->where('id', $id_campaign_tagging);
        $this->db->update('campaign_student_tagging', $data);
        return $this->db->affected_rows();
    }
}