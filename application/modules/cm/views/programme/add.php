<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Program</h3>
            </div>


    <form id="form_programme" action="" method="post">
        
    <div class="form-container">
        <h4 class="form-group-title">Program Details</h4>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language</label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                    </div>
                </div>

                
            </div>

            <div class="row">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Scheme <span class='error-text'>*</span></label>
                        <select name="id_scheme" id="id_scheme" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($schemeList))
                            {
                                foreach ($schemeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code ." - ".$record->description; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Award <span class='error-text'>*</span></label>
                        <select name="id_award" id="id_award" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($awardList))
                            {
                                foreach ($awardList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Education Level <span class='error-text'>*</span></label>
                        <select name="id_education_level" id="id_education_level" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($educationLevelList))
                            {
                                foreach ($educationLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Cr. Hours <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_cr_hrs" name="total_cr_hrs">
                    </div>
                </div>

                


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Internal / External <span class='error-text'>*</span></label>
                        <select name="internal_external" id="internal_external" class="form-control" onchange="showPartner(this.value)">
                            <option value="Internal">Internal</option>
                            <option value="External">External</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                    <div class="form-group">
                        <label>Partner Name <span class='error-text'>*</span></label>
                        <select name="id_partner_university" id="id_partner_university" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($partnerList))
                            {
                                foreach ($partnerList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeDetails->id_partner_university)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>


                

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Is APEL A <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_apel" id="is_apel" value="0" checked="checked"><span class="check-radio"></span> DIRECT ENTRY
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_apel" id="is_apel" value="1"><span class="check-radio"></span> APEL A
                        </label>                              
                    </div>                         
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                                <p>English Language Test required for Non Malaysian students <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="is_english_required" id="is_english_required" value="1" checked="checked"><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="is_english_required" id="is_english_required" value="0"><span class="check-radio"></span> No
                        </label>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>


            </div>

            <!-- <div class="row">

               

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div> -->

        </div>

        <br>

        




            
    

    <!-- <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div> -->


    </form>



    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>

    <br>








     <div class="form-container">
            <h4 class="form-group-title"> Dean of Programme Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Dean Of Program</a>
                    </li>                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_programme_head" action="" method="post">


                            <!-- <div class="form-container">
                                <h4 class="form-group-title">Head Of Department Details</h4> -->
                                <br>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Staff <span class='error-text'>*</span></label>
                                            <select name="id_staff" id="id_staff" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($staffList))
                                                {
                                                    foreach ($staffList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->salutation ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="forintake_has_programmem-group">
                                            <label>Effective Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="effective_start_date" name="effective_start_date" autocomplete="off">
                                        </div>
                                    </div>
                                  
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            <!-- </div> -->


                        </form>



                        </div> 
                    </div>


                </div>

            </div>
        </div> 
           

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $('select').select2();

    function showPartner()
    {
        var value = $("#internal_external").val();
        if(value=='Internal')
        {
             $("#partnerdropdown").hide();
        }
        else if(value=='External')
        {
             $("#partnerdropdown").show();
        }
    }

    function getStateByCountry(id)
    {
         $.get("/cm/staff/getStateByCountry/"+id, function(data, status)
         {
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }

    function saveData()
    {
        if($('#form_programme_head').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['effective_start_date'] = $("#effective_start_date").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/cm/programme/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function saveSchemeData()
    {
        if($('#form_scheme').valid())
        {

        var tempPR = {};
        tempPR['id_scheme'] = $("#id_scheme").val();
            $.ajax(
            {
               url: '/cm/programme/tempSchemeAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_scheme").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function deleteTempData(id)
    {
        $.ajax(
        {
           url: '/cm/programme/tempDelete/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
                $("#view").html(result);
           }
        });
    }

    function deleteTempProgramHasScheme(id)
    {
        // alert(id);
        $.ajax(
        {
           url: '/cm/programme/deleteTempProgramHasScheme/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
                $("#view_scheme").html(result);
           }
        });
    }
</script>
<script>

    function validateDetailsData()
    {
        if($('#form_programme').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam == '')
            {
                alert("Add Head Of Department to the Program");
            }
            else
            {
                $('#form_programme').submit();
            }
        }    
    }


    $(document).ready(function() {
        $("#form_programme_head").validate({
            rules: {
                id_staff: {
                    required: true
                },
                effective_start_date: {
                    required: true
                }
            },
            messages: {
                id_staff: {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                effective_start_date: {
                    required: "<p class='error-text'>Effective Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_scheme").validate({
            rules: {
                id_scheme: {
                    required: true
                }
            },
            messages: {
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    




    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                graduate_studies: {
                    required: true
                },
                foundation: {
                    required: true
                },
                total_cr_hrs: {
                    required: true
                },
                id_award: {
                    required: true
                },
                status: {
                    required: true
                },
                id_scheme: {
                    required: true
                },
                id_education_level: {
                    required: true
                },
                id_partner_university: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                graduate_studies: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                foundation: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                total_cr_hrs: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                },
                id_scheme: {
                    required: "<p class='error-text'>Select Scheme</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner University</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>