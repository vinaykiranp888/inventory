<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Semester Has Activity</h3>
        </div>
        <form id="form_semester_has_activity" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Semester Has Activity Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity <span class='error-text'>*</span></label>
                        <select name="id_activity" id="id_activity" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityDetailsList))
                            {
                                foreach ($activityDetailsList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date () <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>
                
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date () <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" autocomplete="off" id="end_date" name="end_date">
                    </div>
                </div>                
            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
     $(document).ready(function()
    {
        $("#form_semester_has_activity").validate(
        {
            rules:
            {
                id_semester:
                {
                    required: true
                },
                id_activity:
                {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                }
            },
            messages:
            {
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester Required</p>",
                },
                id_activity:
                {
                    required: "<p class='error-text'>Select Activity Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Start Date Required</p>",
                },
                end_date:
                {
                    required: "<p class='error-text'>End Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });
</script>