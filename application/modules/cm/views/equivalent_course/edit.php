<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
        <!-- <a href="../list" class="btn btn-link-light">Back</a> -->
            <h3>Course Equivalent </h3>
        </div>
       


            <h4 class='sub-title'>Course Details</h4>
            <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Name :</dt>
                                <dd><?php echo $courseData->name;?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Course Code :</dt>
                                <dd><?php echo $courseData->code;?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

                <br>

        <!-- <h3>Add Equivalent Course </h3> -->
    <form id="form_equivalent_course" action="" method="post">

    <div class="form-container">
        <h4 class="form-group-title">Add Equivalent Course Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Course *</label>
                        <select name="id_course" id="id_course" class="form-control" onchange="getHours()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Hours <span class='error-text'>*</span></label>
                        <div id="view">
                        </div>
                    </div>
                </div>


            </div>
        </div>


    <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-light btn-lg">Add</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
    </div>




    <div class="form-container">
        <h4 class="form-group-title">Equivalent Course Details</h4>


                   
        <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Course Code</th>
                <th>Course Name</th>
                <th>Credit Hours</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($equivalentCourseDetailsList))
              {
                foreach ($equivalentCourseDetailsList as $record) {
                    // echo "<Pre>"; print_r($equivalentCourseDetailsList);exit;
              ?>
                  <tr>
                    <td><?php echo $record->course_name; ?></td>
                    <td><?php echo $record->course_code; ?></td>
                    <td><?php echo $record->credit_hours; ?></td>
                    <td class="text-center"><?php echo anchor('setup/equivalentCourse/delete_equivalent_course_details?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                  </tr>
              <?php
                }
              }
              ?>
            </tbody>
          </table>
        </div>
        
    </div>

    

    </form>

        



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<style type="text/css">
    .error{
        color: red;
    }
</style>
<script type="text/javascript">
    function getHours() {
         var id_course = $("#id_course").val();
            $.ajax(
            {
               url: '/cm/equivalentCourse/viewHour',
                type: 'POST',
               data:
               {
                'id_course': id_course
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        
    }
</script>
<script>
    $(document).ready(function()
    {
        $("#form_equivalent_course").validate(
        {
            rules:
            {
                id_course:
                {
                    required: true
                }
            },
            messages:
            {
                id_course:
                {
                    required: "Select Equivalent Course",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script>
    function show(){
        var value = $("#requisite_type").val();
        if (value =="Pass with Grade"){
            $("#min_pass").show();
        }
        else{
            $("#min_pass").hide();
        }

        if (value =="Total Credit Hours"){
            $("#min_total").show();
        }
        else{
            $("#min_total").hide();
        }
    }
</script>
<script type="text/javascript">
    $('select').select2();
</script>