<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course Equivalent</h3>
        </div>
        <form id="form_equivalent_course" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Equivalent Course Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Equivalent Course <span class='error-text'>*</span></label>
                        <select name="id_equivalent_course" id="id_equivalent_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                              
            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_equivalent_course").validate(
        {
            rules:
            {
                id_course:
                {
                    required: true
                },
                id_equuivalent_course:
                {
                    required: true
                }
            },
            messages:
            {
                id_course:
                {
                    required: "Select Course",
                },
                id_equuivalent_course:
                {
                    required: "Select Equivalent Course",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>