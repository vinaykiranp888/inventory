<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Landscape</h3>
        </div>


        <br>


        <div class="topnav">
          <a href="<?php echo '../../edit/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>">Landscape Info</a> | 
          <a href="<?php echo '../../editProgramRequirementTab/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>" style="background: #aaff00" >Program Requirement</a> | 
          <a href="<?php echo '../../editCourseTab/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>">Course</a> 
          <?php

            if($programme->mode == 0)
            {

            ?>
            |
          <a href="<?php echo '../../addLearningMode/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>">Learning Mode
          </a>

          <?php

            }
            
            ?>
        </div>

        <br>



        <div class="form-container">
            <h4 class="form-group-title">Programme Landscape Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeLandscapeDetails->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Type  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="program_landscape_type" name="program_landscape_type" value="<?php echo $programmeLandscapeDetails->program_landscape_type; ?>" readonly>
                    </div>
                </div>

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Scheme <span class='error-text'>*</span></label>

                         <select name="program_scheme" id="program_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeSchemeList))
                            {
                                foreach ($programmeSchemeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    <?php if($programmeLandscapeDetails->program_scheme==$record->id){ echo "selected"; } ?>

                                    ><?php echo $record->description;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>

                       
                    </div>
                </div>

            </div>

        </div>




        <form id="form_details" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Program Requirement Info</h4>

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Course Type <span class='error-text'>*</span></label>
                        <select name="id_landscape_course_type" id="id_landscape_course_type" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($landscapeCourseTypeList))
                            {
                                foreach ($landscapeCourseTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->name . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Requirement Type <span class='error-text'>*</span></label>
                        <select name="requirement_type" id="requirement_type" class="form-control">
                            <option value="">Select</option>
                            <option value="Credit Hours">Credit Hours</option>
                            <option value="Papers">Papers</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                        <div class="form-group">
                            <label>Minimum Credit Hr. <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="minimum_credit" name="minimum_credit">
                        </div>
                </div>

                <div class="col-sm-3">
                        <div class="form-group">
                            <p>Compulsary <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="compulsary" id="compulsary" value="1" checked="checked"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="compulsary" id="compulsary" value="0"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>



            </div>


            <div class="row">
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    <a href="<?php echo '../../programmeLandscapeList/' . $programmeLandscapeDetails->id_programme; ?>" class="btn btn-link">Back</a>
                </div>
            </div>


            <?php
            if(!empty($programLandscapeHasRequirementList))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Program Requirement Details</h4>


                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Course Type</th>
                                 <th>Requirement Type</th>
                                 <th class="text-center">Compulsary</th>
                                 <th class="text-center">Min Credit Hrs</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($programLandscapeHasRequirementList);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $programLandscapeHasRequirementList[$i]->course_type_code . " - " . $programLandscapeHasRequirementList[$i]->course_type_name;?></td>
                                <td><?php echo $programLandscapeHasRequirementList[$i]->requirement_type;?></td>
                                <td class="text-center"><?php if( $programLandscapeHasRequirementList[$i]->compulsary == '1')
                                {
                                  echo "Yes";
                                }
                                else
                                {
                                  echo "No";
                                } 
                                ?></td>
                                <td class="text-center"><?php echo $programLandscapeHasRequirementList[$i]->minimum_credit;?></td>

                                <td class="text-center">

                                <a onclick="deleteSemesterInfo(<?php echo $programLandscapeHasRequirementList[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                              $total = $total + $programLandscapeHasRequirementList[$i]->minimum_credit;
                          } 
                          ?>
                          <tr>
                                <td colspan="4" align="right">Total : </td>
                                <td class="text-center"><?php echo $total;?></td>
                            </tr>
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>

        </div>
    </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function saveData()
    {
        if($('#form_details').valid())
        {

        var tempPR = {};
        tempPR['id_landscape_course_type'] = $("#id_landscape_course_type").val();
        tempPR['requirement_type'] = $("#requirement_type").val();
        tempPR['minimum_credit'] = $("#minimum_credit").val();
        tempPR['compulsary'] = $("#compulsary").val();
        tempPR['id_program_landscape'] = <?php echo $id_program_landscape; ?>;
            $.ajax(
            {
               url: '/setup/programmeLandscape/programInfoAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
        // alert(result);
                    location.reload();
                // $("#view").html(result);
               }
            });
        }
    }


    function deleteSemesterInfo(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/programmeLandscape/deleteProgramInfo/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                id_landscape_course_type: {
                    required: true
                },
                requirement_type: {
                    required: true
                },
                minimum_credit: {
                    required: true
                },
                compulsary: {
                    required: true
                }
            },
            messages: {
                id_landscape_course_type: {
                    required: "<p class='error-text'>Select Course Type</p>",
                },
                requirement_type: {
                    required: "<p class='error-text'>Select Requirement Type</p>",
                },
                minimum_credit: {
                    required: "<p class='error-text'>Minimum Credit Hr. Required</p>",
                },
                compulsary: {
                    required: "<p class='error-text'>Select Compulsary Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
<script type="text/javascript">
    $('select').select2();
</script>