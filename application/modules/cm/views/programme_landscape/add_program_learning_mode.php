<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Programme Learning Mode</h3>
        </div>

        <br>


        <div class="topnav">

          <a href="<?php echo '/cm/programmeLandscape/edit/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>">Landscape Info</a> |
          <a href="<?php echo '/cm/programmeLandscape/programObjective/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme.'/' . $programmeLandscapeDetails->id_intake; ?>">Program Objective</a> |
          <a href="<?php echo '/cm/programmeLandscape/programLearningMode/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>"  style="background: #aaff00" >Programme Learning Mode</a> | 
          <a href="<?php echo '/cm/programmeLandscape/subjectRegistration/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme; ?>">Subject Registration</a> 

          
          <!-- <?php

            if($programme->mode == 0)
            {

            ?>
             |
          <a href="<?php echo '../../../addLearningMode/' . $programmeLandscapeDetails->id .'/' . $programmeLandscapeDetails->id_programme .'/' . $programmeLandscapeDetails->id_intake; ?>" style="background: #aaff00">Learning Mode
          </a>

          <?php

            }

            ?> -->

        </div>

        <br>



        <div class="form-container">
            <h4 class="form-group-title">Programme Landscape Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Name  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeLandscapeDetails->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program  <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $programmeLandscapeDetails->id_programme)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" disabled="disabled" class="form-control">

                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $id_intake)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->year . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Landscape Type  <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="program_landscape_type" name="program_landscape_type" value="<?php echo $programmeLandscapeDetails->program_landscape_type; ?>" readonly>
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program Scheme <span class='error-text'>*</span></label>
                       
                         <select name="program_scheme" id="program_scheme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeSchemeList))
                            {
                                foreach ($programmeSchemeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    <?php if($programmeLandscapeDetails->program_scheme==$record->id){ echo "selected"; } ?>

                                    ><?php echo $record->description;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

            </div>

        </div>



          <form id="form_scheme" action="" method="post">


              <br>

              <div class="form-container">
                  <h4 class="form-group-title"> Programme Learning Mode Details</h4>

                  <div class="row">

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Code <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="code" name="code">
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Name <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="name" name="name">
                          </div>
                      </div>



                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Mode Of Program <span class='error-text'>*</span></label>
                              <select name="mode_of_program" id="mode_of_program" class="form-control" style="width: 398px">
                                  <option value="">Select</option>
                                  <option value="Online">Online</option>
                                  <option value="Face to Face">Face to Face</option>
                                  <option value="Blended">Blended</option>
                              </select>
                          </div>
                      </div>



                  </div>

                  <div class="row">



                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Mode Of Study <span class='error-text'>*</span></label>
                              <select name="mode_of_study" id="mode_of_study" class="form-control" style="width: 398px">
                                  <option value="">Select</option>
                                  <option value="Part Time">Part Time</option>
                                  <option value="Full Time">Full Time</option>
                              </select>
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Program Structure Type <span class='error-text'>*</span></label>
                              <select name="id_program_type" id="id_program_type" class="form-control" style="width: 398px">
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($programTypeList))
                                  {
                                      foreach ($programTypeList as $record)
                                      {?>
                                          <option value="<?php echo $record->id;?>"
                                          ><?php echo $record->code ." - ".$record->name; ?>
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>

                  

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Total Semester <span class='error-text'>*</span></label>
                              <input type="number" class="form-control" id="total_semester" name="total_semester">
                          </div>
                      </div>


                  </div>


                  <div class="row">


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Total Min. Cr. Hours <span class='error-text'>*</span></label>
                              <input type="number" class="form-control" id="min_cr_hrs" name="min_cr_hrs">
                          </div>
                      </div>



                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Total Max. Cr. Hours <span class='error-text'>*</span></label>
                              <input type="number" class="form-control" id="max_cr_hrs" name="max_cr_hrs">
                          </div>
                      </div>


                  </div>

              </div>


              <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary btn-lg">Save</button>
                      <a href="<?php echo '/cm/programmeLandscape/programmeLandscapeList/' . $id_programme; ?>" class="btn btn-link">Back</a>
                  </div>
              </div>



          </form>



          <?php
            if(!empty($getProgrammeLandscapeLearningMode))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Programme Learning Mode Details</h4>


                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Code</th>
                                 <th>Name</th>
                                 <th>Mode Of Program</th>
                                 <th>Mode Of Study</th>
                                 <th>Total Semester</th>
                                 <th>Program Structure Type</th>
                                 <th>Min. Cr Hours</th>
                                 <th>Max. Cr Hours</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($getProgrammeLandscapeLearningMode);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->code;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->name;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->mode_of_program;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->mode_of_study;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->total_semester;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->program_type_code . " - " . $getProgrammeLandscapeLearningMode[$i]->program_type_name;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->min_cr_hrs;?></td>
                                <td><?php echo $getProgrammeLandscapeLearningMode[$i]->max_cr_hrs;?></td>
                                 </tr>
                              <?php 
                              // $total = $total + $getProgrammeLandscapeLearningMode[$i]->minimum_credit;
                          } 
                          ?>
                            
                            </tbody>
                        </table>
                      </div>

                    </div>

            <?php
            
            }
             ?>







        



   </div>


    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>
</div>



<script type="text/javascript">
    $('select').select2();
    
    function showProgramDetails()
    {
        var type = $("#course_type").val();
        $("#view_minor").hide();
        $("#view_major").hide();
        // if(type == 'Major')
        // {
        //     $("#view_minor").hide();
        //     $("#view_major").show();
        // }
        // else if(type == 'Minor')
        // {
        //     $("#view_minor").show();
        //     $("#view_major").hide();
        // }else
        // {
        //     $("#view_minor").hide();
        //     $("#view_major").hide();
        // }

    }

    $(document).ready(function() {
        $("#form_scheme").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                mode_of_program: {
                    required: true
                },
                mode_of_study: {
                    required: true
                },
                id_program_type: {
                    required: true
                },
                total_semester: {
                    required: true
                },
                min_cr_hrs: {
                    required: true
                },
                max_cr_hrs: {
                    required: true
                }
            },
            messages:
            {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Reuired</p>",
                },
                mode_of_program: {
                    required: "<p class='error-text'>Select Mode Of Program</p>",
                },
                mode_of_study: {
                    required: "<p class='error-text'>Select Mode Of Study</p>",
                },
                id_program_type: {
                    required: "<p class='error-text'>Select Structure Type</p>",
                },
                total_semester: {
                    required: "<p class='error-text'>Total Semester Required</p>",
                },
                min_cr_hrs: {
                    required: "<p class='error-text'>Min. Cr Hours Required</p>",
                },
                max_cr_hrs: {
                    required: "<p class='error-text'>Max. Cr Hours Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
