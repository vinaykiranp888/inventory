<?php
$roleModel = new Role_Model();

 $urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];

$roleList  = $roleModel->getSideMenuListByModule($urlmodule);
// echo "<Pre>";print_r($roleList);exit();
?>


            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li ><a href="/livechat/php/app.php?admin" target="_blank">Chat</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </div>
                   
                    </div>

                    <div class="sidebar-nav">   
                      <?php 
                      for($i=0;$i<count($roleList);$i++)
                      { 
                        $parent_order = $roleList[$i]->parent_order;

                        $data['module'] = $urlmodule;
                        $data['parent_order'] = $parent_order;

                         
                        $urlmenuListDetails  = $roleModel->getMenuListByParentOrder($data);

                        // echo "<Pre>";print_r($urlmenuListDetails);exit();

                        $collapse = 'collapsed';
                        $ulclass = 'collapse';
                        for($a=0;$a<count($urlmenuListDetails);$a++)
                        {                              
                          if($urlcontroller==$urlmenuListDetails[$a]->controller)
                          {
                              $collapse = "";
                              $ulclass = "";
                          }
                        }
                        ?>                         
                        <h4><a role="button" data-toggle="collapse" class="<?php echo $collapse;?>" href="#asdf<?php echo $parent_order;?>"><?php echo $roleList[$i]->parent_name;?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="<?php echo $ulclass;?>" id="asdf<?php echo $parent_order;?>">

                        <?php

                        $data['module'] = $urlmodule;
                        $data['parent_order'] = $parent_order;

                        $menuListDetails  = $roleModel->getMenuListByParentOrder($data);
                        

                          for($l=0;$l<count($menuListDetails);$l++)
                          {
                            $controller = $menuListDetails[$l]->controller;
                            $action = $menuListDetails[$l]->action; ?>
                            
                            <li <?php
                             if($controller == $urlcontroller)
                             {
                              echo "class='active'";
                             }
                              ?>>
                              <a href="<?php echo '/' . $urlmodule . '/' . $controller . '/' . $action;?> ">
                                <?php echo $menuListDetails[$l]->menu_name;?>      
                              </a>
                            </li>

                            <?php 
                              }
                            ?>
                        </ul>
                    
                        <?php 
                          }
                        ?>

                    </div>


                    <!-- <div class="sidebar-nav">                            
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#generalSetup">Course Setup <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="generalSetup">
                           <li><a href="/cm/courseType/list">Course Type</a></li>
                        <li><a href="/cm/course/list">Course Main</a></li>
                        <li><a href="/cm/courseMaster/list">Course Requisite</a></li>
                        <li><a href="/cm/equivalentCourse/list">Course Equivalent</a></li>
                        </ul>
                        <h4><a role="button" data-toggle="collapse" class="collapsed" href="#demographicSetup">Program Setup<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
                        <ul class="collapse" id="demographicSetup">
                            <li><a href="/cm/programType/list">Program Type</a></li>
                        <li><a href="/cm/programme/list">Program Main</a></li>
                        <li><a href="/cm/individualEntryRequirement/list">Program Entry Requirement</a></li>
                        <li><a href="/cm/programmeLandscape/list">Program Landscape</a></li>
                        </ul>
                        


                        
                        <h4>University Setup</h4>
                        <ul>
                            <li><a href="/setup/partnerCategory/list">Partner Category</a></li>
                            <li><a href="/setup/partnerUniversity/list">Partner University</a></li>

                        </ul>
                        <h4>Course Setup</h4>
                        <ul>
                            <li><a href="/setup/courseDescription/list">Course Major / Minor</a></li>
                            <li><a href="/setup/courseType/list">Course Type</a></li>
                            <li><a href="/setup/course/list">Course Main</a></li>
                            <li><a href="/setup/courseMaster/list">Course Requisite</a></li>
                            <li><a href="/setup/equivalentCourse/list">Course Equivalent</a></li>
                        </ul>

                        

                        <h4>Program Setup</h4>
                        <ul>
                            <li><a href="/setup/landscapeCourseType/list">Landscape Course Type</a></li>
                            <li><a href="/setup/programType/list">Program Type</a></li>
                            <li><a href="/setup/programme/list">Program Main</a></li>
                            <li class="active"><a href="/setup/programmeLandscape/list">Program Landscape</a></li>
                            <li><a href="/setup/individualEntryRequirement/list">Program Entry Requirement</a></li>
                        </ul>
                        
                        <h4>Registration</h4>
                        <ul>
                            <li><a href="/setup/courseWithdraw/list">Course Withdraw</a></li>
                            <li><a href="/setup/lateRegistration/list">Late Registration</a></li>
                        </ul>

                        <h4>Report</h4>
                        <ul>
                            <li><a href="/setup/user/chart1">Students by Intake</a></li>
                            <li><a href="/setup/user/chart2">Applicant V/S Lead V/S Student</a></li>
                            <li><a href="/setup/user/chart3">Course Wise data</a></li>
                        </ul> -->
                   
                </div>
            </div>
            