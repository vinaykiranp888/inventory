<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
</head>
<body>
    <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand main-logo" href="/">Asia e University</a>
            </div>

            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li ><a href="/livechat/php/app.php?admin" target="popup" onclick="window.open('/livechat/php/app.php?admin','Chat','width=600,height=400')">Open page in new window</a></li > -->
                    <li><a href="/setup/welcome">Setup</a></li>
                    <li class="active"><a href="/cm/welcome">Curriculum Management</a></li>
                    <li><a href="/pm/welcome">Partner Management</a></li>
                    <li><a href="/af/welcome">Academic Facilitator</a></li>
                    <li><a href="/admission/welcome">Admission</a></li>
                    <li><a href="/registration/welcome">Registration</a></li>
                    <li><a href="/research/welcome">IPS</a></li>
                   
                    <!-- <li><a href="/allumini/welcome">Alumni</a></li> -->

                    <!-- <li><a href="/timetable/timetable/list">Time Table</a></li> -->
                    <!-- <li><a href="/scholarship/scheme/list">Scholarship</a></li> -->
                    <!-- <li><a href="/teaching_evaluation/welcome">Teaching Evaluation</a></li> -->
                    <!-- <li><a href="/setup/user/logout">logout</a></li> -->

                    <li>
                        <button class="dropdown-toggle more-link" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More
                        </button>

                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                    <li><a href="/finance/welcome">Finance</a></li>                                                        
                            <li><a href="/records/welcome">Records</a></li>
                            <li><a href="/examination/welcome">Assessment</a></li>
                            <li><a href="/graduation/welcome">Graduation</a></li>
                            <li><a href="/communication/welcome">Communication</a></li>
                            <li><a href="/sponser/welcome">Sponsor</a></li>
                            <li><a href="/internship/welcome">Internship</a></li>
                            <li><a href="/hostel/welcome">Hostel</a></li>
                            <li><a href="/scholarship/scheme/list">Scholarship</a>
                            <li><a href="/allumini/welcome">Alumni</a></li>
                            <li><a href="/facility/welcome">Facility</a></li>
                            <li><a href="/teaching_evaluation/welcome">Teaching Evaluation</a></li>
                            <li><a href="/setup/user/logout">Logout</a></li>
                        </ul>
                    </li>
                </ul>
                
            </nav>
        </div>
    </header>
</body>

     
