<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Individual Program Requirement</h3>
            </div>


        <div class="form-container">
        <h4 class="form-group-title">Program Details</h4>

            <div class="data-list">
                <div class="row">

                    <div class="col-sm-6">
                        <dl>
                            <dt>Program Name :</dt>
                            <dd><?php echo $programDetails->name;?></dd>
                        </dl>
                    </div>        
                    
                    <div class="col-sm-6">
                        <dl>
                            <dt>Program Code :</dt>
                            <dd><?php echo $programDetails->code;?></dd>
                        </dl>
                    </div>

                </div>
            </div>

        </div>





    <form id="form_programme" action="" method="post">
        
    <!-- <div class="form-container">
        <h4 class="form-group-title">Age Requirement Details</h4>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Age Requirement</p>
                        <label class="radio-inline">
                            <input type="radio" id="age" name="age" value="0" onclick="hideAge()" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="age" name="age" value="1" onclick="showAge()"><span class="check-radio"></span> Yes
                        </label>
                        
                    </div>
                </div>

                
                <div class="col-sm-4" id="view_age_min" style="display: none;">
                    <div class="form-group">
                        <label>Min. (Years)</label>
                        <input type="number" class="form-control" id="min_age" name="min_age" min="14" max="99">
                    </div>
                </div>

                <div class="col-sm-4" id="view_age_max" style="display: none;">
                    <div class="form-group">
                        <label>Max. (Years)</label>
                        <input type="number" class="form-control" id="max_age" name="max_age" min="14" max="99">
                    </div>
                </div>

                
            </div>

        </div> -->

        <!-- <div class="form-container">
        <h4 class="form-group-title">Education Requirement Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Education</p>
                        <label class="radio-inline">
                            <input type="radio" id="education" name="education" value="0" onclick="hideEducation()" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="education" name="education" value="1" onclick="showEducation()"><span class="check-radio"></span> Yes
                        </label>
                        
                    </div>
                </div>


                <div class="col-sm-4" id="view_education_qualification" style="display: none;">
                    <div class="form-group">
                        <label>Select Qualification </label>
                        <select name="id_education_qualification" id="id_education_qualification" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($qualificationList))
                            {
                                foreach ($qualificationList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo  $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



        </div> -->


       <!--  <div class="form-container">
        <h4 class="form-group-title">Work Experience Requirement</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Work Experience</p>
                        <label class="radio-inline">
                            <input type="radio" id="work_experience" name="work_experience" value="0" onclick="hideWorkExperience()" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="work_experience" name="work_experience" value="1" onclick="showWorkExperience()"><span class="check-radio"></span> Yes
                        </label>
                        
                    </div>
                </div>



                <div class="col-sm-4" id="view_work_specialisation" style="display: none;">
                    <div class="form-group">
                        <label>Select Specialisation </label>
                        <select name="id_work_specialisation" id="id_work_specialisation" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($workSpecialisationList))
                            {
                                foreach ($workSpecialisationList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id="view_work_experience" style="display: none;">
                    <div class="form-group">
                        <label>Min. (Years)</label>
                        <input type="number" class="form-control" id="min_work_experience" name="min_work_experience">
                    </div>
                </div>

            </div>

            <div class="row">
            </div>

        </div> -->


        <div class="form-container">
        <h4 class="form-group-title">Requirement Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Other Requirement</p>
                        <label class="radio-inline">
                            <input type="radio" id="other" name="other" value="0" onclick="hideOther()" checked="checked"><span class="check-radio"></span> No
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="other" name="other" value="1" onclick="showOther()"><span class="check-radio"></span> Yes
                        </label>
                        
                    </div>
                </div>


                <div class="col-sm-10" id="view_other_description" style="display: none;">
                    <div class="form-group">
                        <label>Description </label>
                        <input type="text" class="form-control" id="other_description" name="other_description" autocomplete="off">
                    </div>
                </div>

            </div>

        </div>
    


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


        </form>




        <?php

            if(!empty($programEntryRequirementList))
            {
                ?>
                <br>

                <div class="form-container">
                        <h4 class="form-group-title">Program Requirement Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Requirement</th>
                                 <th class="text-center" valign="middle">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($programEntryRequirementList);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <!-- <td><?php echo $programEntryRequirementList[$i]->code . " - " . $programEntryRequirementList[$i]->name;?></td> -->
                                <!-- <td><?php echo $programEntryRequirementList[$i]->age;?></td>
                                <td><?php echo $programEntryRequirementList[$i]->education;?></td> -->
                                <td><?php

                                $and = '';

                                if($programEntryRequirementList[$i]->age == 1)
                                {
                                    echo "Age Min. " . $programEntryRequirementList[$i]->min_age . " Year, Max. " . $programEntryRequirementList[$i]->max_age . " Year" ;
                                    $and = ", AND <br>";
                                }
                                if($programEntryRequirementList[$i]->education == 1)
                                {
                                    echo $and . "Education Requirement is : " 
                                    // . $programEntryRequirementList[$i]->qualification_code  .  " - "
                                     . $programEntryRequirementList[$i]->qualification_name ;
                                    $and = " ,  AND <br>";

                                }
                                if($programEntryRequirementList[$i]->work_experience == 1)
                                {
                                    echo $and . "Work Experience is : " . $programEntryRequirementList[$i]->work_code . " - " . $programEntryRequirementList[$i]->work_name . ", With Min. Experience Of " . $programEntryRequirementList[$i]->min_work_experience . " Years";
                                    $and = " ,  AND <br>";
                                    
                                }
                                if($programEntryRequirementList[$i]->other == 1)
                                {
                                    echo $and . $programEntryRequirementList[$i]->other_description. " .";
                                }
                                 ?></td>
                                <td class="text-center" valign="middle">
                                <a onclick="deleteRequirement(<?php echo $programEntryRequirementList[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>




            <?php
            
            }
             ?>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    function showAge(){
            $("#view_age_min").show();
            $("#view_age_max").show();
    }

    function hideAge(){
            $("#view_age_min").hide();
            $("#view_age_max").hide();
    }



    function showEducation(){
            $("#view_education_qualification").show();
    }

    function hideEducation(){
            $("#view_education_qualification").hide();
    }




    function showWorkExperience(){
            $("#view_work_specialisation").show();
            $("#view_work_experience").show();
    }

    function hideWorkExperience(){
            $("#view_work_specialisation").hide();
            $("#view_work_experience").hide();
    }





    function showOther(){
            $("#view_other_description").show();
    }

    function hideOther(){
            $("#view_other_description").hide();
    }

    

    function deleteRequirement(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/individualEntryRequirement/deleteRequirement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }

    


    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                graduate_studies: {
                    required: true
                },
                foundation: {
                    required: true
                },
                total_cr_hrs: {
                    required: true
                },
                id_award: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Program Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Program Code Required</p>",
                },
                graduate_studies: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                foundation: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                total_cr_hrs: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                },
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>