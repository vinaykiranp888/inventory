<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Permission</h3>
        </div>
        <form id="form_permission" method="post" action="" method="post" id="editPermission">

         <div class="form-container">
            <h4 class="form-group-title">Permission Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $permissionInfo->code; ?>">
                        <input type="hidden" value="<?php echo $permissionInfo->id; ?>" name="permissionId" id="permissionId ?>">    
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $permissionInfo->description; ?>">
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($permissionInfo->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($permissionInfo->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div> -->
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Module <span class='error-text'>*</span></label>
                        <select name="module" id="module" class="form-control" onchange="getSubMenuByModule(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($menuList))
                            {
                                foreach ($menuList as $record)
                                {?>
                                    <option value="<?php echo $record->name;?>"
                                    <?php
                                    if($permissionInfo->module == $record->name)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Submenu <span class='error-text'>*</span></label>
                     <span id="view_sub_menu">
                        <select class="form-control" id='id_menu' name='id_menu'>
                            <option value=''></option>
                          </select>
                     </span>
                  </div>
               </div>

            </div>



        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $('select').select2();

    function getSubMenuByModule(menu)
    {
        if(menu != '')
        {
            var tempPR = {};
            tempPR['menu'] = menu;

            $.ajax(
            {
               url: '/setup/permission/getSubMenuByModule',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_sub_menu").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    $(document).ready(function()
    {
        var menu = "<?php echo $permissionInfo->module;?>";

        // alert(menu);
       if(menu != '')
        {
            var tempPR = {};
            tempPR['menu'] = menu;

            $.ajax(
            {
               url: '/setup/permission/getSubMenuByModule',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                var idstateselected = "<?php echo $permissionInfo->id_menu;?>";

                $("#view_sub_menu").html(result);
                $("#id_menu").find('option[value="'+idstateselected+'"]').attr('selected',true);
                $('select').select2();
                // $('#myModal').modal('hide');
               }
            });
        }



        $("#form_permission").validate({
            rules: {
                description: {
                    required: true
                },
                code: {
                    required: true
                },
                status: {
                    required: true
                },
                id_menu: {
                    required: true
                },
                module: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Description required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_menu: {
                    required: "<p class='error-text'>Select Sub Menu</p>",
                },
                module: {
                    required: "<p class='error-text'>Select Module</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
