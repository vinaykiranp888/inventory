<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Documents_program_model extends CI_Model
{
    function programListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function documentsListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('documents as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function documentsProgramListSearch($formData)
    {
        $this->db->select('a.*, d.name as document_name, d.code as document_code');
        $this->db->from('documents_program as a');
        $this->db->join('documents as d','a.id_document = d.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.description  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_document']))
        {
            $this->db->where('a.id_document', $formData['id_document']);
        }
        $this->db->order_by("a.description", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDocumentsProgram($id)
    {
        $this->db->select('*');
        $this->db->from('documents_program');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDocumentsProgram($data)
    {
        $this->db->trans_start();
        $this->db->insert('documents_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDocumentsProgram($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('documents_program', $data);
        return TRUE;
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_documents_program_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempData($id) {
        $this->db->where('id', $id);
        $this->db->delete('temp_documents_program_details');
        return TRUE;
    }


    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_documents_program_details');
        return TRUE;
    }


    function getTempDetailsBySession($sessionid)
    {
        $this->db->select('tpe.*, p.code as program_code, p.name as program_name');
        $this->db->from('temp_documents_program_details as tpe');
        $this->db->join('programme as p','tpe.id_program = p.id');
        $this->db->where('tpe.id_session', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempDetailsBySessionId($sessionid)
    {
        $this->db->select('tpe.*');
        $this->db->from('temp_documents_program_details as tpe');
        $this->db->join('programme as p','tpe.id_program = p.id');
        $this->db->where('tpe.id_session', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewDocumentsProgramDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('documents_program_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getNewDocumentsProgramDetailsByMasterId($id)
    {
        $this->db->select('tpe.*, p.code as program_code, p.name as program_name');
        $this->db->from('documents_program_details as tpe');
        $this->db->join('programme as p','tpe.id_program = p.id');
        $this->db->where('tpe.id_documents_program', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteDocumentsProgram($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('documents_program_details');
        return TRUE;
    }

    function moveTempToDetails($id_documents_program)
    {
        $id_session = $this->session->my_session_id;

        $tempDetails = $this->getTempDetailsBySessionId($id_session);
        foreach ($tempDetails as $tempDetail)
        {
            unset($tempDetail->id);
            unset($tempDetail->id_session);
            $tempDetail->id_documents_program = $id_documents_program;

            $detail_add = $this->addNewDocumentsProgramDetails($tempDetail);
        }
        $temp_deleted = $this->deleteTempDataBySession($id_session);
    }
}

