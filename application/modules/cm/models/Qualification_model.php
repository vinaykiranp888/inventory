<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Qualification_model extends CI_Model
{
    function qualificationList()
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function qualificationListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getQualification($id)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewQualification($data)
    {
        $this->db->trans_start();
        $this->db->insert('qualification_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editQualification($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('qualification_setup', $data);
        return TRUE;
    }
}

