<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Individual_entry_requirement_model extends CI_Model
{
    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programListSearch($search)
    {
        $this->db->select('sp.*, sd.code as award_code, sd.name as award_name');
        $this->db->from('programme as sp');
        $this->db->join('award as sd','sp.id_award = sd.id');
        if (!empty($search))
        {
            $likeCriteria = "(sp.name  LIKE '%" . $search . "%' or sp.name_optional_language  LIKE '%" . $search . "%' or sp.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('sp.status', '1');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programEntryRequirementList($id_program)
    {
        $this->db->select('ier.*, qs.code as qualification_code, qs.name as qualification_name, ws.code as work_code, ws.name as work_name');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->join('education_level as qs','ier.id_education_qualification = qs.id','left');
        $this->db->join('scholarship_work_specialisation as ws','ier.id_work_specialisation = ws.id','left');
        $this->db->where('ier.id_program', $id_program);
                $this->db->where('ier.entry_type', 'ENTRY');

        $query = $this->db->get();
        return $query->result();
    }


     function appelEntryRequirementList($id_program)
    {
        $this->db->select('ier.*');
        $this->db->from('scholarship_individual_entry_requirement as ier');
        $this->db->where('ier.id_program', $id_program);
        $this->db->where('ier.entry_type', 'APPEL');
        $query = $this->db->get();
        return $query->result();
    }

    function getProgram($id_program)
    {
        $this->db->select('ier.*');
        $this->db->from('programme as ier');
        $this->db->where('ier.id', $id_program);
        $query = $this->db->get();
        return $query->row();
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function workSpecialisationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_work_specialisation');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addIndividualEntryRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_individual_entry_requirement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


     function addIndividualAppelRequirement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_individual_appel_requirement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editIndividualEntryRequirement($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_individual_entry_requirement', $data);
        return TRUE;
    }

    function deleteRequirement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_individual_entry_requirement');
        return TRUE;
    }
















    
    

    function addNewProgrammeDocumentRequirements($id_scholarship_program)
    {
        $data['id_scholarship_program'] = $id_scholarship_program;
        $this->db->trans_start();
        $this->db->insert('is_scholarship_applicant_personel_details', $data);
        $this->db->insert('is_scholarship_applicant_examination_details', $data);
        $this->db->insert('is_scholar_applicant_family_details', $data);
        $this->db->insert('is_scholar_applicant_employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
        
    }
}