<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AcademicYear extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('academic_year_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('academic_year.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $name;
            $data['academicYearList'] = $this->academic_year_model->academicYearList($name);
            $this->global['pageTitle'] = 'Inventory Management : Academic Year';
            //print_r($subjectDetails);exit;
            $this->loadViews("academic_year/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('academic_year.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $user_id
                );
            
                $result = $this->academic_year_model->addNewAcademicYear($data);
                redirect('/setup/academicYear/list');
            }
            $this->load->model('academic_year_model');
            $this->global['pageTitle'] = 'Inventory Management : Add Academic Year';
            $this->loadViews("academic_year/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('academic_year.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/setup/academicYear/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $user_id
                );
                $result = $this->academic_year_model->editAcademicYearDetails($data,$id);
                redirect('/setup/academicYear/list');
            }
            $data['academicYearDetails'] = $this->academic_year_model->getAcademicYearDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Academic Year';
            $this->loadViews("academic_year/edit", $this->global, $data, NULL);
        }
    }
}
