<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerCategory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_category_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('partner_category.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['partnerCategoryList'] = $this->partner_category_model->partnerCategoryListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Program Type List';
            $this->loadViews("partner_category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('partner_category.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->partner_category_model->addNewProgramType($data);
                redirect('/setup/partnerCategory/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Program Type';
            $this->loadViews("partner_category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('partner_category.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/partnerCategory/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->partner_category_model->editProgramType($data,$id);
                redirect('/setup/partnerCategory/list');
            }
            $data['partnerCategory'] = $this->partner_category_model->getProgramType($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit ProgramType';
            $this->loadViews("partner_category/edit", $this->global, $data, NULL);
        }
    }
}
