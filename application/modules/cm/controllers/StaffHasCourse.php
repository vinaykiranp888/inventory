<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StaffHasCourse extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_has_course_model');
        $this->load->model('staff_model');
        $this->load->model('course_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('staff_has_course.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['staffList'] = $this->staff_model->staffList();
            $data['courseList'] = $this->course_model->courseList();

            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));

            $data['searchParameters'] = $formData; 

            $data['staffHasCourseList'] = $this->staff_has_course_model->staffHasCourseListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Staff Has Course List';
            $this->loadViews("staff_has_course/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('staff_has_course.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $status = $this->security->xss_clean($this->input->post('status'));
                     
                $data = array(
                    'id_staff' => $id_staff,
                    'id_course' => $id_course,
                    'status' => $status
                );   

                $result = $this->staff_has_course_model->addNewStaffHasCourse($data);
                redirect('/setup/staffHasCourse/list');
            }
            $data['staffList'] = $this->staff_model->staffList();
            $data['courseList'] = $this->course_model->courseList();
            //echo "<Pre>";print_r($data);exit();    
            $this->global['pageTitle'] = 'Inventory Management : Add Staff Has Course';
            $this->loadViews("staff_has_course/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('staff_has_course.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/staffHasCourse/list');
            }
            if($this->input->post())
            {
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $status = $this->security->xss_clean($this->input->post('status'));
                     
                $data = array(
                    'id_staff' => $id_staff,
                    'id_course' => $id_course,
                    'status' => $status
                );

                $result = $this->staff_has_course_model->editStaffHasCourse($data,$id);
                redirect('/setup/staffHasCourse/list');
            }
            $data['staffList'] = $this->staff_model->staffList();
            $data['courseList'] = $this->course_model->courseList();
            $data['staffHasCourse'] = $this->staff_has_course_model->getStaffHasCourse($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Staff Has Course';
            $this->loadViews("staff_has_course/edit", $this->global, $data, NULL);
        }
    }
}