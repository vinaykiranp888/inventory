<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Award extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('award_model');
        $this->load->model('programme_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('award.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->programme_model->programmeList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));

            $data['searchParameters'] = $formData;
            $data['awardList'] = $this->award_model->awardListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Award List';
            $this->loadViews("award/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('award.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                // $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $level = $this->security->xss_clean($this->input->post('level'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'level' => $level,
                    'description' => $description,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->award_model->addNewAward($data);
                redirect('/setup/award/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $this->global['pageTitle'] = 'Inventory Management : Add Award';
            $this->loadViews("award/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('award.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/award/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                // $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $level = $this->security->xss_clean($this->input->post('level'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'level' => $level,
                    'description' => $description,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->award_model->editAward($data,$id);
                redirect('/setup/award/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['awardDetails'] = $this->award_model->getAward($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Award';
            $this->loadViews("award/edit", $this->global, $data, NULL);
        }
    }
}
