<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Barring extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('barring_model');
        $this->load->model('semester_model');
        $this->load->model('barring_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('barring.list') == 1)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->barring_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_barring_type'] = $this->security->xss_clean($this->input->post('id_barring_type'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            
            $data['searchParameters'] = $formData;
            $data['barringList'] = $this->barring_model->barringListSearch($formData);
            // echo "<Pre>"; print_r($data['barringList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Barring List';
            $this->loadViews("barring/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('barring.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_barring_type = $this->security->xss_clean($this->input->post('id_barring_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $barring_date = $this->security->xss_clean($this->input->post('barring_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_barring_type' => $id_barring_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'barring_date' => $barring_date,
                    'status' => $status,
                    'created_by' => $id_user
                );
                $result = $this->barring_model->addNewBarring($data);
                redirect('/setup/barring/list');
            }
            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->barring_model->studentList();

            $this->global['pageTitle'] = 'Inventory Management : Add New Barring';
            $this->loadViews("barring/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('barring.edit') == 1)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/setup/barring/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_barring_type = $this->security->xss_clean($this->input->post('id_barring_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $barring_date = $this->security->xss_clean($this->input->post('barring_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_barring_type' => $id_barring_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'barring_date' => $barring_date,
                    'status' => $status,
                    'created_by' => $id_user
                );
                $result = $this->barring_model->editBarring($data, $id);
                redirect('/setup/barring/list');
            }

            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->barring_model->studentList();
            $data['barring'] = $this->barring_model->getBarring($id);
            // echo "<Pre>";print_r($data['barring']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Barring';
            $this->loadViews("barring/edit", $this->global, $data, NULL);
        }
    }
}
