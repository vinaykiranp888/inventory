<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Scheme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('scheme_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('scheme.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['schemeList'] = $this->scheme_model->schemeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Scheme List';
            $this->loadViews("scheme/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('scheme.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $description = $this->security->xss_clean($this->input->post('description'));
                $description_in_optional_language = $this->security->xss_clean($this->input->post('description_in_optional_language'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'description_in_optional_language' => $description_in_optional_language,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->scheme_model->addNewScheme($data);
                redirect('/setup/scheme/edit/' . $result);
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Scheme';
            $this->loadViews("scheme/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('scheme.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/scheme/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $description = $this->security->xss_clean($this->input->post('description'));
                $description_in_optional_language = $this->security->xss_clean($this->input->post('description_in_optional_language'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'description_in_optional_language' => $description_in_optional_language,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user
                );
                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->scheme_model->editScheme($data,$id);
                redirect('/setup/scheme/list');
            }
            $data['scheme'] = $this->scheme_model->getScheme($id);
            $data['schemeHasNationalityList'] = $this->scheme_model->schemeHasNationality($id);
            $data['nationalityList'] = $this->scheme_model->nationalityListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Edit Scheme';
            $this->loadViews("scheme/edit", $this->global, $data, NULL);
        }
    }

    function addSchemeHasNationality()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->scheme_model->addSchemeHasNationality($tempData);

        echo "success";exit;
    }

    function deleteSchemeHasNationality($id)
    {
        $inserted_id = $this->scheme_model->deleteSchemeHasNationality($id);
        echo "Success";exit; 
    }
}
