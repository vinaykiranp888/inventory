<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ClassDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('class_details_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('class_details.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['schoolDetails'] = $this->class_details_model->classList();
            $this->global['pageTitle'] = 'Inventory Management : Class List';
            $this->loadViews("class_details/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('class_details.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
            
                $schoolInfo = array(
                    'name' => $name
                );
                $result = $this->class_details_model->addNewClassDetails($schoolInfo);
                redirect('/setup/classDetails/list');
            }
            //print_r($data['stateList']);exit;
            $this->load->model('class_details_model');
            $this->global['pageTitle'] = 'Inventory Management : Add Class';
            $this->loadViews("class_details/add", $this->global, NULL, NULL);
        }
    }


    function edit($classId = NULL)
    {
        if ($this->checkAccess('class_details.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($classId == null)
            {
                redirect('/setup/subjectDetails/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $schoolInfo = array(
                    'name' => $name
                );
                $result = $this->class_details_model->editClassDetails($schoolInfo,$classId);
                redirect('/setup/classDetails/list');
            }
            $data['classDetails'] = $this->class_details_model->getClassDetails($classId);
            $this->global['pageTitle'] = 'Inventory Management : Edit Class';
            $this->loadViews("class_details/edit", $this->global, $data, NULL);
        }
    }

    function delete()
    {
        if ($this->checkAccess('class_details.delete') == 1)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->class_details_model->deleteCountry($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }
}
