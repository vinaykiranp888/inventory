<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SemesterHasActivity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('semester_has_activity_model');
        $this->load->model('semester_model');
        $this->load->model('activity_details_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('semester_has_activity.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['activityList'] = $this->activity_details_model->activityDetailsList();

            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_activity'] = $this->security->xss_clean($this->input->post('id_activity'));
 
            $data['searchParameters'] = $formData; 
            // print_r($formData);exit;

            $data['semesterDetails'] = $this->semester_has_activity_model->semesterHasActivityListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Semester List';
            $this->loadViews("semester_has_activity/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('semester_has_activity.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_activity = $this->security->xss_clean($this->input->post('id_activity'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                // $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_semester' => $id_semester,
                    'id_activity' => $id_activity,
                    'start_date' => date("Y-m-d", strtotime($start_date)),
                    'end_date' => date("Y-m-d", strtotime($end_date))
                );
                $result = $this->semester_has_activity_model->addNewSemesterHasActivity($data);
                redirect('/setup/semesterHasActivity/list');
            }
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['activityDetailsList'] = $this->activity_details_model->activityDetailsList();
            $this->global['pageTitle'] = 'Inventory Management : Add Semester Has Activity';
            $this->loadViews("semester_has_activity/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('semester.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/semesterHasActivity/list');
            }
            if($this->input->post())
            {
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_activity = $this->security->xss_clean($this->input->post('id_activity'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                // $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_semester' => $id_semester,
                    'id_activity' => $id_activity,
                    'start_date' => date("Y-m-d", strtotime($start_date)),
                    'end_date' => date("Y-m-d", strtotime($end_date))

                );
                $result = $this->semester_has_activity_model->editSemesterHasActivity($data,$id);
                redirect('/setup/semesterHasActivity/list');
            }

            $data['semesterList'] = $this->semester_model->semesterList();
            $data['activityDetailsList'] = $this->activity_details_model->activityDetailsList();
            $data['semesterHasActivityDetails'] = $this->semester_has_activity_model->getSemesterHasActivity($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Semester Has Activity';
            // echo "<Pre>";print_r($data['semesterHasActivityDetails']);exit;
            $this->loadViews("semester_has_activity/edit", $this->global, $data, NULL);
        }
    }
}
