<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approve Purchase Order</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Convocation</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Name</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Vendor</label>
                    <div class="col-sm-8">
                      <select name="id_vendor" id="id_vendor" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($vendorList)) {
                          foreach ($vendorList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_vendor']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Financial Year</label>
                    <div class="col-sm-8">
                      <select name="id_financial_year" id="id_financial_year" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($financialYearList)) {
                          foreach ($financialYearList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_financial_year']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->year;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Department</label>
                    <div class="col-sm-8">
                      <select name="id_department" id="id_department" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($departmentList)) {
                          foreach ($departmentList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_department']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>
              
              <div class="app-btn-group">
                <button type="button" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>PR Number</th>
            <th>PO Number</th>
            <th>Description</th>
            <th>Vendor</th>
            <th>Financial Year</th>
            <th>Amount</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
            <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll"> Check All</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($poList)) {
              $i=0;
            foreach ($poList as $record)
            {
          ?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $record->pr_number ?></td>
                <td><?php echo $record->po_number ?></td>
                <td><?php echo $record->description ?></td>
                <td><?php echo $record->vendor_code . " - " .$record->vendor_name ?></td>
                <td><?php echo $record->financial_year ?></td>
                <td><?php echo $record->amount ?></td>
                <!-- <td><?php echo $record->status ?></td> -->

                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>

                <?php if($record->status=="1")
                {
                  echo "<td class='text-center'>Approved</td>";
                }
                else
                {
                ?>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                  <!--  -->
                </td>
                <td class="text-center">
                  <input type="checkbox" name="checkvalue[]" class="check" value="<?php echo $record->id ?>">
                </td>
                <?php
                }
                ?>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
          <div class="app-btn-group">
            <button type="submit" class="btn btn-primary">Approve</button>
          </div>
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $(function () {
      $("#checkAll").click(function () {
          if ($("#checkAll").is(':checked')) {
              $(".check").prop("checked", true);
          } else {
              $(".check").prop("checked", false);
          }
      });
  });

  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }
</script>