<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Manufacturer</h3>
        </div>
        <form id="form_main" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Manufacturer Details</h4>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Manufacturer Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vname" name="vname">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vemail" name="vemail" >
                        </div>
                    </div>
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vmobile" name="vmobile" >
                        </div>
                    </div>

                
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number </label>
                            <input type="text" class="form-control" id="vphone" name="vphone">
                        </div>
                    </div>
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>GST IN </label>
                            <input type="text" class="form-control" id="gst_in" name="gst_in" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>CIN </label>
                            <input type="text" class="form-control" id="cin" name="cin"  >
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Line 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_one" name="vaddress_one" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Line 2 / Landmark</label>
                            <input type="text" class="form-control" id="vaddress_two" name="vaddress_two"  >
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="city" name="city">
                        </div>
                    </div>            
                    
                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="vcountry" id="vcountry" class="form-control" onchange="getStateByCountry(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id="view_state">
                                <select class="form-control" id='vstate' name='vstate'>
                                        <option value=''></option>
                                </select>
                            </span>
                        </div>
                    </div> 




                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode  <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="vzipcode" name="vzipcode" >
                        </div>
                    </div>


                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                    </div>

                    

                </div>

            </div>
            
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

    $('select').select2();


    function getStateByCountry(id)
    {
        if(id != '')
        {
            $.get("/procurement/manufacturer/getStateByCountry/"+id,
            function(data, status)
            {
                $("#view_state").html(data);
            });
        }
    }

    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                vname: {
                    required: true
                },
                vemail: {
                    required: true
                },
                vmobile: {
                    required: true
                },
                vaddress_one: {
                    required: true
                },
                city: {
                    required: true
                },
                vcountry: {
                    required: true
                },
                vstate: {
                    required: true
                },
                vzipcode: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                vname: {
                    required: "<p class='error-text'>Manufacturer Name Required</p>",
                },
                vemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                vmobile: {
                    required: "<p class='error-text'>Mobilee No. Required</p>",
                },
                vaddress_one: {
                    required: "<p class='error-text'>Address 1 Required</p>",
                },
                city: {
                    required: "<p class='error-text'>City Reuired</p>",
                },
                vcountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                vstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
                vzipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>