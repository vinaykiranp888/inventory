<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Part Number</h3>
        </div>
        <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Part Number Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <select name="id_procurement_category" id="id_procurement_category" class="form-control" onchange="getSubcategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                  
            
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Procurement Sub-Category <span class='error-text'>*</span></label>
                        <span id="view_subcategory">
                            <select class="form-control" id='id_procurement_sub_category' name='id_procurement_sub_category'>
                                    <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>    

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Part Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="part_number" name="part_number">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Value <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="value" name="value">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GST Tax <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="gst_tax" name="gst_tax" max="100">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Manufacturer <span class='error-text'>*</span></label>
                        <select name="manufacturer" id="manufacturer" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($manufacturerList))
                            {
                                foreach ($manufacturerList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>SPQ <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="spq" name="spq">
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>MOQ <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="moq" name="moq">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Alternative Part Number </label>
                        <input type="text" class="form-control" id="alternative_part_number" name="alternative_part_number">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="amount" class="form-control" id="price" name="price">
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Package <span class='error-text'>*</span></label>
                        <select name="id_package" id="id_package" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($packageList))
                            {
                                foreach ($packageList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thrushold <span class='error-text'>*</span></label>
                        <input type="amount" class="form-control" id="thrushold" name="thrushold">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Lead Time <span class='error-text'>*</span></label>
                        <input type="amount" class="form-control" id="lead_time" name="lead_time">
                    </div>
                </div>




            </div>

            <div class="row">


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <textarea type="amount" class="form-control" id="description" name="description"></textarea>
                    </div>
                </div>

            </div>

        </div>
        
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

    $('select').select2();

    CKEDITOR.replace('description',{
      width: "100%",
      height: "300px"

    });



    function getSubcategory(id)
    {
        if(id != '')
        {
            $.get("/procurement/procurementItem/getSubcategory/"+id,
            function(data, status)
            {
                $("#view_subcategory").html(data);
            });
        }
    }

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                 name: {
                    required: true
                },
                 id_procurement_category: {
                    required: true
                },
                 id_procurement_sub_category: {
                    required: true
                },
                 status: {
                    required: true
                },
                part_number: {
                    required: true
                },
                 value: {
                    required: true
                },
                 gst_tax: {
                    required: true
                },
                 manufacturer: {
                    required: true
                },
                 spq: {
                    required: true
                },
                 moq: {
                    required: true
                },
                 price: {
                    required: true
                },
                 id_package: {
                    required: true
                },
                 thrushold: {
                    required: true
                },
                 lead_time: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                id_procurement_category: {
                    required: "<p class='error-text'>Select Category Code</p>",
                },
                id_procurement_sub_category: {
                    required: "<p class='error-text'>Select Sub-Category Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                },
                part_number: {
                    required: "<p class='error-text'>Part Number Required</p>",
                },
                value: {
                    required: "<p class='error-text'>Value Required</p>",
                },
                gst_tax: {
                    required: "<p class='error-text'>GST Tax Required</p>",
                },
                manufacturer: {
                    required: "<p class='error-text'>Manufacturer Required</p>",
                },
                spq: {
                    required: "<p class='error-text'>SPQ required</p>",
                },
                moq: {
                    required: "<p class='error-text'>MOQ Required</p>",
                },
                price: {
                    required: "<p class='error-text'>Price Required</p>",
                },
                id_package: {
                    required: "<p class='error-text'>Select Package</p>",
                },
                thrushold: {
                    required: "<p class='error-text'>thrushold Required</p>",
                },
                lead_time: {
                    required: "<p class='error-text'>Lead time Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>