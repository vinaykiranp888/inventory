<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        


        <ul class="page-nav-links">
            <li class="active"><a href="../edit/<?php echo $id_item;?>">Edit Part Number</a></li>                
            <li><a href="../vendors/<?php echo $id_item;?>">Vendor List</a></li>
        </ul>


        <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Part Number Details</h4>


            <div class="row">
               
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $procurementItem->code; ?>">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $procurementItem->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <select name="id_procurement_category" id="id_procurement_category" class="form-control" onchange="getSubcategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $procurementItem->id_procurement_category)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code." - ".$record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub-Category <span class='error-text'>*</span></label>
                        <span id="view_subcategory">
                            <select name="id_procurement_sub_category" id="id_procurement_sub_category" class="form-control">
                                <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Part Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="part_number" name="part_number" value="<?php echo $procurementItem->part_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Value <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="value" name="value" value="<?php echo $procurementItem->value; ?>">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GST Tax <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="gst_tax" name="gst_tax" value="<?php echo $procurementItem->gst_tax; ?>" max='100'>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Manufacturer <span class='error-text'>*</span></label>
                        <select name="manufacturer" id="manufacturer" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($manufacturerList))
                            {
                                foreach ($manufacturerList as $record)
                                {?>
                             <option value="<?php echo $record->id; ?>"
                                <?php 
                                if($record->id == $procurementItem->manufacturer)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>SPQ <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="spq" name="spq" value="<?php echo $procurementItem->spq; ?>">
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>MOQ <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="moq" name="moq" value="<?php echo $procurementItem->moq; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Alternative Part Number </label>
                        <input type="text" class="form-control" id="alternative_part_number" name="alternative_part_number" value="<?php echo $procurementItem->alternative_part_number; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="amount" class="form-control" id="price" name="price" value="<?php echo $procurementItem->price; ?>">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Package <span class='error-text'>*</span></label>
                        <select name="id_package" id="id_package" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($packageList))
                            {
                                foreach ($packageList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $procurementItem->id_package)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Thrushold <span class='error-text'>*</span></label>
                        <input type="amount" class="form-control" id="thrushold" name="thrushold"value="<?php echo $procurementItem->thrushold; ?>">
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Lead Time <span class='error-text'>*</span></label>
                        <input type="amount" class="form-control" id="lead_time" name="lead_time"value="<?php echo $procurementItem->lead_time; ?>">
                    </div>
                </div>




            </div>

            <div class="row">


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($procurementItem->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($procurementItem->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>


            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <textarea type="amount" class="form-control" id="description" name="description"> <?php echo $procurementItem->description; ?></textarea>
                    </div>
                </div>

            </div>


        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>

    $('select').select2();


    CKEDITOR.replace('description',{
      width: "100%",
      height: "300px"

    }); 


    function getSubcategory(id)
    {
        if(id != '')
        {
            $.get("/procurement/procurementItem/getSubcategory/"+id,
            function(data, status)
            {
                $("#view_subcategory").html(data);
            });
        }
    }


    $(document).ready(function()
    {

        var id_procurement_category = <?php echo $procurementItem->id_procurement_category; ?>

        if(id_procurement_category > 0)
        {
            $.get("/procurement/procurementItem/getSubcategory/"+id_procurement_category,
            function(data, status)
            {
                var id_procurement_sub_category = "<?php echo $procurementItem->id_procurement_sub_category ?>";
                // alert(id_state);
                $("#view_subcategory").html(data);
                $("#id_procurement_sub_category").find('option[value="'+id_procurement_sub_category+'"]').attr('selected',true);
                $('select').select2();

            });
        }


        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                 name: {
                    required: true
                },
                 id_procurement_category: {
                    required: true
                },
                 id_procurement_sub_category: {
                    required: true
                },
                 status: {
                    required: true
                },
                part_number: {
                    required: true
                },
                 value: {
                    required: true
                },
                 gst_tax: {
                    required: true
                },
                 manufacturer: {
                    required: true
                },
                 spq: {
                    required: true
                },
                 moq: {
                    required: true
                },
                 price: {
                    required: true
                },
                 id_package: {
                    required: true
                },
                 thrushold: {
                    required: true
                },
                 lead_time: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                id_procurement_category: {
                    required: "<p class='error-text'>Select Category Code</p>",
                },
                id_procurement_sub_category: {
                    required: "<p class='error-text'>Select Sub-Category Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                },
                part_number: {
                    required: "<p class='error-text'>Part Number Required</p>",
                },
                value: {
                    required: "<p class='error-text'>Value Required</p>",
                },
                gst_tax: {
                    required: "<p class='error-text'>GST Tax Required</p>",
                },
                manufacturer: {
                    required: "<p class='error-text'>Manufacturer Required</p>",
                },
                spq: {
                    required: "<p class='error-text'>SPQ required</p>",
                },
                moq: {
                    required: "<p class='error-text'>MOQ Required</p>",
                },
                price: {
                    required: "<p class='error-text'>Price Required</p>",
                },
                id_package: {
                    required: "<p class='error-text'>Select Package</p>",
                },
                thrushold: {
                    required: "<p class='error-text'>thrushold Required</p>",
                },
                lead_time: {
                    required: "<p class='error-text'>Lead time Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    
</script>