<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="assemblyReturnst">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Assembly Return Entry</h3>
            </div>
            

        <div class="form-container">
            <h4 class="form-group-title">Assembly Return Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assembly Return Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number" value="<?php echo $assemblyReturn->reference_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Distribution Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="distribution_number" name="distribution_number" value="<?php echo $assemblyReturn->distribution_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Return Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $assemblyReturn->description;?>" readonly="readonly">
                    </div>
                </div>


            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assembly Team <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="id_vendor" name="id_vendor" value="<?php echo $assemblyReturn->assembly_code . ' - ' . $assemblyReturn->assembly_name; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $assemblyReturn->total_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Return Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="created_dt_tm" name="created_dt_tm" value="<?php echo date('d-m-Y',strtotime($assemblyReturn->created_dt_tm));?>" readonly="readonly">
                    </div>
                </div>


            </div>           

            <!-- <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($assemblyReturn->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($assemblyReturn->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($assemblyReturn->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            <?php
            if($assemblyReturn->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $assemblyReturn->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div> -->

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <hr>

        <h3>Return Details</h3>

        <div class="form-container">
            <h4 class="form-group-title">PO Details</h4>


            <div class="custom-table">
                <table class="table">
                    <thead>
                         <tr>
                             <th>Sl. No</th>
                             <th>Category</th>
                             <th>Sub Category</th>
                             <th>Product</th>
                             <th>Total Quantity</th>
                             <th>Balance Quantity</th>
                             <th>Returned Quantity</th>
                             <th>Price</th>
                             <th>Total Price</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php 
                          $total = 0;
                         for($i=0;$i<count($assemblyReturnDetails);$i++)
                            { 
                            // echo "<Pre>";print_r($assemblyReturnDetails[$i]);exit();

                                ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->category_code . " - " . $assemblyReturnDetails[$i]->category_name;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->sub_category_code . " - " . $assemblyReturnDetails[$i]->sub_category_name;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->item_code . " - " . $assemblyReturnDetails[$i]->item_name;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->total_quantity;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->balance_quantity;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->quantity;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->price;?></td>
                            <td><?php echo $assemblyReturnDetails[$i]->total_price;?></td>

                             </tr>
                          <?php 
                          $total = $total + $assemblyReturnDetails[$i]->total_price;
                        }
                        $total = number_format($total, 2, '.', ',');
                        ?>

                        <tr>
                            <td bgcolor="" colspan="6"></td>
                            <td bgcolor=""><b> Total : </b></td>
                            <td bgcolor=""><b><?php echo $total; ?></b></td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>