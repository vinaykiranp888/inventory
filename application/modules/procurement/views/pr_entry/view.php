<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View PR Entry</h3>
            </div>


        <div class="form-container">
            <h4 class="form-group-title">PR Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Entry Type <span class='error-text'>*</span></label>
                        <select disabled="disabled" name='type_of_pr' id='type_of_pr' class='form-control'>
                            <option value=''>Select</option>
                            <option value='PO' <?php if($prMaster->type_of_pr=='PO'){ echo "selected=selected";} ?> >PO</option>
                            <option value='Direct Purchase' <?php if($prMaster->type_of_pr=='Non-PO'){ echo "selected=selected";} ?> >Non-PO</option>
                            <option value='Warrant' <?php if($prMaster->type_of_pr=='Warrant'){ echo "selected=selected";} ?> >Warrant</option>
                            <option value='Tender' <?php if($prMaster->type_of_pr=='Tender'){ echo "selected=selected";} ?> >Tender</option>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_vendor" id="id_vendor" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($vendorList))
                            {
                                foreach ($vendorList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php if($record->id==$prMaster->id_vendor) { echo "selected=selected";} ?>>
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select disabled="disabled" name='type' id='type' class='form-control'>
                            <option value=''>Select</option>
                            <option value='Procurement' <?php if($prMaster->type=='Procurement'){ echo "selected=selected";} ?> >Procurement</option>
                            <option value='Asset' <?php if($prMaster->type=='Asset'){ echo "selected=selected";} ?> >Asset</option>
                        </select>
                    </div>
                </div>               


                
            </div>

            <div class="row">


               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Financial Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_financial_year" id="id_financial_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($financialYearList))
                            {
                                foreach ($financialYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$prMaster->id_financial_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Budget Year <span class='error-text'>*</span></label>
                        <select disabled="disabled" name="id_budget_year" id="id_budget_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($budgetYearList))
                            {
                                foreach ($budgetYearList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                    <?php if($record->id==$prMaster->id_budget_year)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

               
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Department Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $prMaster->department_code . " - " .$prMaster->department;?>" readonly="readonly">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_number" name="pr_number" value="<?php echo $prMaster->pr_number; ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PR Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="pr_entry_date" name="pr_entry_date" value="<?php echo date('d-m-Y', strtotime($prMaster->pr_entry_date));?>" readonly="readonly">
                    </div>
                </div>
                
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $prMaster->amount; ?>" readonly="readonly">
                    </div>
               </div>

            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $prMaster->description;?>" readonly="readonly">
                    </div>
                </div>
                
               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($prMaster->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($prMaster->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($prMaster->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

                <?php
            if($prMaster->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $prMaster->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>


                
            </div>

            <?php
            if($prMaster->status == '0')
            {
             ?>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p> Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                            <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                        </label>
                    </div>
                </div>

            </div>

          <?php
            }
            ?>

            <div class="row">

                <div class="col-sm-4" id="view_reject" style="display: none">
                    <div class="form-group">
                        <label>Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control">
                    </div>
                </div>
            </div>

        </div>

        </form>






          <h3>PR Details</h3>


          <div class="form-container">
            <h4 class="form-group-title">PR Details</h4>

              <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>DEBIT GL CODE</th>
                         <th>CREDIT GL CODE</th>
                         <th>Category</th>
                         <th>Sub Category</th>
                         <th>Item</th>
                         <th>Tax</th>
                         <th>Qty</th>
                         <th>Price</th>
                         <th>Tax Amount</th>
                         <th>Final Total</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($prDetails);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $prDetails[$i]->dt_fund . " - " . $prDetails[$i]->dt_department . " - " . $prDetails[$i]->dt_activity . " - " . $prDetails[$i]->dt_account;?></td>
                        <td><?php echo $prDetails[$i]->cr_fund . " - " . $prDetails[$i]->cr_department . " - " . $prDetails[$i]->cr_activity . " - " . $prDetails[$i]->cr_account;?></td>
                        <td><?php echo $prDetails[$i]->category_code . " - " . $prDetails[$i]->category_name;?></td>
                        <td><?php echo $prDetails[$i]->sub_category_code . " - " . $prDetails[$i]->sub_category_name;?></td>
                        <td><?php echo $prDetails[$i]->item_code . " - " . $prDetails[$i]->item_name;?></td>
                        <td><?php echo $prDetails[$i]->tax_code . " - " . $prDetails[$i]->tax_name;?></td>
                        <td><?php echo $prDetails[$i]->quantity;?></td>
                        <td><?php echo $prDetails[$i]->price;?></td>
                        <td><?php echo $prDetails[$i]->tax_price;?></td>
                        <td><?php echo $prDetails[$i]->total_final;?></td>

                         </tr>
                      <?php 
                      $total = $total + $prDetails[$i]->total_final;
                  } 
                  $total = number_format($total, 2, '.', ',');
                  ?>
                    <tr>
                        <td bgcolor="" colspan="9"></td>
                        <td bgcolor=""><b> Total : </b></td>
                        <td bgcolor=""><b><?php echo $total; ?></b></td>
                    </tr>

                    </tbody>
                </table>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">

         <?php
        if($prMaster->status == '0')
        {
         ?>
                <button type="submit" class="btn btn-primary btn-lg">Save</button>

         <?php
        }
         ?>
                <a href="../approvalList" class="btn btn-link">Back</a>
            </div>
        </div>

        
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>

  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField()
    {
        $("#view_reject").show();
    }

    function hideRejectField()
    {
        $("#view_reject").hide();
    }


  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>