<?php $this->load->helper("form"); ?>

<form id="form_nonpo_entry" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Tender Quotation</h3>
            </div>

    <div class="form-container">
        <h4 class="form-group-title">Tender Quotation</h4>

            <div class="row">
                <div class="col-sm-4">
                        <div class="form-group">
                            <label>PR Number <span class='error-text'>*</span></label>
                            <select name='type_of_pr' id='type_of_pr' class='form-control' onChange="getDetails(this.value)">
                                <option value=''>Select</option>

                                <?php for($i=0;$i<count($prEntryList);$i++)
                                {
                                    ?>
                                    <option value="<?php echo $prEntryList[$i]->id;?>">
                                    <?php echo $prEntryList[$i]->pr_number . " - " . $prEntryList[$i]->description;?>  
                                    </option>
                                    <?php
                                } ?> 
                            </select>
                        </div>
                </div>
            </div>


        <div id="view">
        </div>

    </div>


    <div class="page-title clearfix">
    </div>
    <div class="form-container">
    <h4 class="form-group-title">Tender Comitee Details</h4>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Select Staff <span class='error-text'>*</span></label>
                    <select name="id_staff" id="id_staff" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($staffList))
                        {
                            foreach ($staffList as $record)
                            {?>
                                <option value="<?php echo $record->id;?>"
                                ><?php echo $record->ic_no . " - " . $record->salutation . ". " . $record->name; ?>
                                </option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

             <div class="col-sm-4">
                <div class="form-group">
                    <label>Description <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="comitee_description" name="comitee_description">
                </div>
            </div>
          
            <div class="col-sm-4">
                <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveComiteeData()">Add</button>
            </div>
        </div>

        <br>
        <br>
        
        <div class="row">
            <div id="view_comitee"></div>
        </div>
    </div>


    <div class="page-title clearfix">
    </div>
    
    <div class="form-container">
    <h4 class="form-group-title">Tender Remarks Details</h4>

        <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Remarks <span class='error-text'>*</span></label>
                    <input type="text" class="form-control" id="remarks" name="remarks">
                </div>
            </div>
          
            <div class="col-sm-4">
                <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveRemarksData()">Add</button>
            </div>
        </div>

        <br>
        <br>

        <div class="row">
            <div id="view_remarks"></div>
        </div>
    </div>
        
    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>

        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>

<script>

    function validateDetailsData()
    {
        if($('#form_nonpo_entry').valid())
        {
            console.log($("#view_comitee").html());
            var addedProgam = $("#view_comitee").html();
            var addedRemarks = $("#view_remarks").html();
            if(addedProgam=='')
            {
                alert("Add Comitee Details");
            }
            else if(addedRemarks == '')
            {
                alert("Add Remarks Details");
            }else
            {
                // $('#type').prop('disabled', false);
                $('#form_nonpo_entry').submit();
            }
        }    
    }

    $(document).ready(function() {
        $("#form_nonpo_entry").validate({
            rules: {
                type_of_pr: {
                    required: true
                },
                 pr_number: {
                    required: true
                }
                ,
                 description: {
                    required: true
                },
                 pr_entry_date: {
                    required: true
                },
                 start_date: {
                    required: true
                },
                 end_date: {
                    required: true
                },
                 opening_date: {
                    required: true
                }
            },
            messages: {
                type_of_pr: {
                    required: "<p class='error-text'>Select Type Of PR</p>",
                },
                pr_number: {
                    required: "<p class='error-text'>Select PR Number</p>",
                },
                description: {
                    required: "<p class='error-text'>Tender Description Required</p>",
                },
                pr_entry_date: {
                    required: "<p class='error-text'>Select Vendor</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select Close Date Year</p>",
                },
                opening_date: {
                    required: "<p class='error-text'>Select Opening Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


  
    function getDetails(id) {       
            $.ajax(
            {
               url: '/procurement/tenderQuotation/getData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }

    function saveComiteeData()
    {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['comitee_description'] = $("#comitee_description").val();
            $.ajax(
            {
               url: '/procurement/tenderQuotation/tempaddComitee',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_comitee").html(result);
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }

    function saveRemarksData()
    {


        var tempPR = {};
        tempPR['remarks'] = $("#remarks").val();
            $.ajax(
            {
               url: '/procurement/tenderQuotation/tempaddRemarks',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_remarks").html(result);
                // var ta = $("#total_detail").val();
                // alert(ta);
                // $("#amount").val(ta);
               }
            });
    }

    function deleteTempCommitee(id) {
         $.ajax(
            {
               url: '/procurement/tenderQuotation/deleteTempCommitee/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_comitee").html(result);
               }
            });
    }

    function deleteTempRemarks(id) {
         $.ajax(
            {
               url: '/procurement/tenderQuotation/deleteTempRemarks/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_remarks").html(result);
               }
            });
    }


</script>
<script type="text/javascript">
    $('select').select2();
</script>