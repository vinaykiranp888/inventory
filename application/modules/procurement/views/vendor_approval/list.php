<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approve Vendors</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Convocation</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Vendor</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Email</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="email" value="<?php echo $searchParam['email']; ?>">
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-4 control-label">NRIC</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                      </div>
                    </div>

                  </div>
                </div>

              </div>
              <div class="app-btn-group">
                <button type="button" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. NO</th>
            <th>Vendor Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>NRIC</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($vendorList)) {
              $i=0;
            foreach ($vendorList as $record) {
          ?>
              <tr>
                <td><?php echo $i+1;?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->email ?></td>
                <td><?php echo $record->phone ?></td>
                <td><?php echo $record->nric ?></td>
                <td>
                <?php 
                if($record->vendor_status=="Approved")
                {
                  echo "Approved";
                }
                else
                {
                  ?>
                 <a href="<?php echo 'view/' . $record->id; ?>" title="Approve">Approve</a>
                <?php
                }
                ?>
              </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
          <div class="app-btn-group">
            <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
          </div>
    </div>
  </form>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  $(function () {
      $("#checkAll").click(function () {
          if ($("#checkAll").is(':checked')) {
              $(".check").prop("checked", true);
          } else {
              $(".check").prop("checked", false);
          }
      });
  });
    
  function clearSearchForm()
  {
    window.location.reload();
  }
</script>