<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Assembly Distribution </h3>
      <a href="add" class="btn btn-primary">+ Add Assembly Distribution</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Distribution Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Assembly Team</label>
                    <div class="col-sm-8">
                      <select name="id_vendor" id="id_vendor" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($assemblyTeamList)) {
                          foreach ($assemblyTeamList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_assembly']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button href="list" class="btn btn-link">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>PO Number</th>
            <th>Description</th>
            <th>Assembly Team</th>
            <th>Total Amount</th>
            <th>Created Date</th>
            <th>Status</th>
            <th style="text-align:center; ">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($assemblyDistributionList))
          {
            $i=1;
            foreach ($assemblyDistributionList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo $record->description ?></td>
                <td><?php echo $record->assembly_code . " - " . $record->assembly_name ?></td>
                <td><?php echo $record->total_amount ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                <td><?php if( $record->status_name == '')
                {
                  echo "Pending";
                }
                else
                {
                  echo $record->status_name;
                }
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
</script>