<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Assembly Distribution Entry</h3>
        </div>

    <form id="form_po_entry" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Assembly Distribution Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="po_date" name="po_date" value="<?php echo date('d-m-Y'); ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Assembly Team <span class='error-text'>*</span></label>
                        <select name='id_assembly' id='id_assembly' class='form-control'>
                            <option value=''>Select</option>

                            <?php foreach($assemblyTeamList as $record)
                            {
                                ?>
                                <option value="<?php echo $record->id;?>">
                                <?php echo $record->code . " - " . $record->name;?>  
                                </option>
                                <?php
                            } ?> 
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="" readonly>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description </label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>


                <!-- <div class="col-sm-12">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <textarea type="amount" class="form-control" id="description" name="description"></textarea>
                    </div>
                </div> -->

            </div>


        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
    </form>

    <br>

    <form id="form_details" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Assembly Distribution Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <select name="id_category" id="id_category" class="form-control" onchange="getSubcategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub-Category <span class='error-text'>*</span></label>
                        <span id="view_subcategory">
                            <select class="form-control" id='id_sub_category' name='id_sub_category'>
                                    <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>    

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Product <span class='error-text'>*</span></label>
                        <span id="view_item">
                            <select class="form-control" id='id_item' name='id_item'>
                                    <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>    

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Available Stock <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="available_stock" name="available_stock" readonly min="1">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quantity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="quantity" name="quantity" onblur="calculateTotalPrice()">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="price" name="price" onblur="calculateTotalPrice()">
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_price" name="total_price" readonly>
                    </div>
                </div>


            </div>


        </div>
    
    </form>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button onclick="saveTempAssemblyDistributionDetails()" type="button" class="btn btn-primary btn-lg">Save</button>
            </div>
        </div>


    <div class="form-container" id="view_temp_details_show" style="display: none;">
        <h4 class="form-group-title">AssemblyDistribution Details</h4>

            <div class="row" id="view_temp_details">
            </div>
    </div>


        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>


    </div>
</div>


<!-- <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> -->
<script>

    $('select').select2();

    // CKEDITOR.replace('description',{
    //   width: "100%",
    //   height: "100px"

    // });

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
    });

    function getSubcategory(id)
    {
        if(id != '')
        {
            $.get("/procurement/assemblyDistribution/getSubcategory/"+id,
            function(data, status)
            {
                $("#view_subcategory").html(data);
            });
        }
    }

    function procurementItemListBySubCategory(id)
    {
        // alert(id);
        if(id != '')
        {
            $.get("/procurement/assemblyDistribution/procurementItemListBySubCategory/"+id,
            function(data, status)
            {
                $("#view_item").html(data);
            });
        }
    }

    function getAvailableStocksByProductId()
    {
        var id_item = $("#id_item").val();
        // alert(id_item);
        if(id_item != '')
        {
            $.get("/procurement/assemblyDistribution/getAvailableStocksByProductId/"+id_item,
            function(data, status)
            {
                $("#available_stock").val(data);
            });

            $.get("/procurement/assemblyDistribution/getItemDuplicationInTempDetails/"+id_item,
            function(data, status)
            {
                if(data == '0')
                {
                    alert('Duplicate Products Are Not Allowed' );
                    $("#id_item").val('');
                    $("#available_stock").val('');
                }
            });
        }
    }

    function calculateTotalPrice()
    {
        var available_stock = $("#available_stock").val();
        var quantity = $("#quantity").val();
        var price = $("#price").val();
        var total_price = 0;

        if(available_stock < quantity)
        {
            alert('Quantity Must Be Lesser tha  Available Stock');
            $("#quantity").val('');
        }
        else
        if(quantity > 0 && price > 0)
        {
            total_price = quantity * price;
            $("#total_price").val(total_price);
        }

    }


    function saveTempAssemblyDistributionDetails(id)
    {
        if($('#form_details').valid())
        {
            var tempPR = {};
            tempPR['id_category'] = $("#id_category").val();
            tempPR['id_sub_category'] = $("#id_sub_category").val();
            tempPR['id_item'] = $("#id_item").val();
            tempPR['quantity'] = $("#quantity").val();
            tempPR['balance_quantity'] = $("#quantity").val();
            tempPR['price'] = $("#price").val();
            tempPR['total_price'] = $("#total_price").val();

                $.ajax(
                {
                   url: '/procurement/assemblyDistribution/saveTempAssemblyDistributionDetails',
                   type: 'POST',
                   data:
                   {
                    data: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    if(result == '')
                    {
                        alert('No Debit Budget GL Found For Entered Data')
                    }
                    else
                    {
                        $("#view_temp_details").html(result);
                        $("#view_temp_details_show").show();
                        var tota_detail = $("#total_detail").val();
                        $("#total_amount").val(tota_detail);

                        document.getElementById("form_details").reset();

                    }
                   }
                });
        }
    }

    function deleteTempPoDetails(id)
    {
        var cnf= confirm('Do you really want to delete?');
        if(cnf==true)
        {
            if(id != '')
            {
                $.get("/procurement/assemblyDistribution/deleteTempPoDetails/"+id,
                function(data, status)
                {
                    $("#view_temp_details").html(data);
                    $("#view_temp_details_show").show();
                    var tota_detail = $("#total_detail").val();
                    $("#total_amount").val(tota_detail);
                });
            }
        }
    }


   $(document).ready(function() {
        $("#form_po_entry").validate({
            rules: {
                po_date: {
                    required: true
                },
                 id_assembly: {
                    required: true
                },
                total_amount: {
                    required: true
                }
            },
            messages: {
                po_date: {
                    required: "<p class='error-text'>Select AssemblyDistribution Date</p>",
                },
                id_assembly: {
                    required: "<p class='error-text'>Select Assembly Team</p>",
                },
                total_amount: {
                    required: "<p class='error-text'>Add Detais For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


   $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                id_category: {
                    required: true
                },
                id_sub_category: {
                    required: true
                },
                id_item: {
                    required: true
                },
                quantity: {
                    required: true
                },
                price: {
                    required: true
                },
                available_stock: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_sub_category: {
                    required: "<p class='error-text'>Select Sub Category</p>",
                },
                id_item: {
                    required: "<p class='error-text'>Select Product</p>",
                },
                price: {
                    required: "<p class='error-text'>Price Required</p>",
                },
                quantity: {
                    required: "<p class='error-text'>Quantity Required</p>",
                },
                available_stock: {
                    required: "<p class='error-text'>Select A Product That Has Stock</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  
</script>