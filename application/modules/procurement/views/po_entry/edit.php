<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Purchase Order Entry</h3>
            </div>
            

        <div class="form-container">
            <h4 class="form-group-title">PO Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="po_number" name="po_number" value="<?php echo $po->po_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $po->description;?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="created_dt_tm" name="created_dt_tm" value="<?php echo date('d-m-Y H:i:s',strtotime($po->created_dt_tm));?>" readonly="readonly">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="id_vendor" name="id_vendor" value="<?php echo $po->vendor_code . ' - ' . $po->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount Before Tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount_before_tax" name="total_amount_before_tax" value="<?php echo $po->total_amount_before_tax;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Tax Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_tax_amount" name="total_tax_amount" value="<?php echo $po->total_tax_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $po->total_amount;?>" readonly="readonly">
                    </div>
                </div>


            </div>

            <!-- <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($po->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($po->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($po->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            <?php
            if($po->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $po->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div> -->

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        <div class="page-title clearfix">
                <a href="<?php echo '/procurement/poEntry/generatePO/'.$po->id ?>" target="_blank" class="btn btn-link btn-back">
                    Download PO >>></a>
        </div>

        
        <hr>

        <h3>Purchase Order Details</h3>


        <div class="form-container">
            <h4 class="form-group-title">PO Details</h4>


            <div class="custom-table">
                <table class="table">
                    <thead>
                         <tr>
                             <th>Sl. No</th>
                             <th>Category</th>
                             <th>Sub Category</th>
                             <th>Product</th>
                             <th>Quantity</th>
                             <th>Received Quantity</th>
                             <th>Balance Quantity</th>
                             <th>GST Tax</th>
                             <th>Price</th>
                             <th>Tax Amount / Item</th>
                             <th>Total Amount Before Tax</th>
                             <th>Total Tax Amount</th>
                             <th>Total Price</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php 
                          $total_price = 0;
                          $detail_total_tax_amount = 0;
                          $grand_total_tax_amount = 0;
                          $grand_total_price_before_gst = 0;
                          $total = 0;

                         for($i=0;$i<count($poDetails);$i++)
                            { 
                            // echo "<Pre>";print_r($poDetails[$i]);exit();

                                ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $poDetails[$i]->category_code . " - " . $poDetails[$i]->category_name;?></td>
                            <td><?php echo $poDetails[$i]->sub_category_code . " - " . $poDetails[$i]->sub_category_name;?></td>
                            <td><?php echo $poDetails[$i]->item_code . " - " . $poDetails[$i]->item_name;?></td>
                            <td><?php echo $poDetails[$i]->quantity;?></td>
                            <td><?php echo $poDetails[$i]->received_quantity;?></td>
                            <td><?php echo $poDetails[$i]->balance_quantity;?></td>
                            <td><?php echo $poDetails[$i]->gst_tax;?></td>
                            <td style='text-align: right'><?php echo $poDetails[$i]->price;?></td>
                            <td style='text-align: right'><?php echo $poDetails[$i]->tax_amount;?></td>
                            <td style='text-align: right'><?php echo $poDetails[$i]->total_price_before_tax;?></td>
                            <td style='text-align: right'><?php echo $poDetails[$i]->total_tax_amount;?></td>
                            <td style='text-align: right'><?php echo $poDetails[$i]->total_price;?></td>

                             </tr>
                          <?php 

                          $total_price = $total_price + $poDetails[$i]->price;
                          $detail_total_tax_amount = $detail_total_tax_amount + $poDetails[$i]->tax_amount;
                          $grand_total_tax_amount = $grand_total_tax_amount + $poDetails[$i]->total_tax_amount;
                          $grand_total_price_before_gst = $grand_total_price_before_gst + $poDetails[$i]->total_price_before_tax;
                          $total = $total + $poDetails[$i]->total_price;

                        }

                        $total_price = number_format($total_price, 2, '.', ',');
                        $detail_total_tax_amount = number_format($detail_total_tax_amount, 2, '.', ',');
                        $grand_total_tax_amount = number_format($grand_total_tax_amount, 2, '.', ',');
                        $grand_total_price_before_gst = number_format($grand_total_price_before_gst, 2, '.', ',');
                        $total = number_format($total, 2, '.', ',');
                        ?>

                        <tr>
                            <td colspan="7"></td>
                            <td style='text-align: right'><b> Total </b></td>
                            <td style='text-align: right'><b><?php echo $total_price; ?></b></td>
                            <td style='text-align: right'><b><?php echo $detail_total_tax_amount; ?></b></td>
                            <td style='text-align: right'><b><?php echo $grand_total_tax_amount; ?></b></td>
                            <td style='text-align: right'><b><?php echo $grand_total_price_before_gst; ?></b></td>
                            <td style='text-align: right'><b><?php echo $total; ?></b></td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>