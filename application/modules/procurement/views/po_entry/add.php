<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Purchase Order Entry</h3>
        </div>

    <form id="form_po_entry" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">PO Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="po_date" name="po_date" value="<?php echo date('d-m-Y'); ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <select name='id_vendor' id='id_vendor' class='form-control'>
                            <option value=''>Select</option>

                            <?php for($i=0;$i<count($vendorList);$i++)
                            {
                                ?>
                                <option value="<?php echo $vendorList[$i]->id;?>">
                                <?php echo $vendorList[$i]->code . " - " . $vendorList[$i]->name;?>  
                                </option>
                                <?php
                            } ?> 
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="po_total_tax_amount" name="po_total_tax_amount" value="" readonly>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount Before Tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="po_total_amount_before_tax" name="po_total_amount_before_tax" value="" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description </label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>


                <!-- <div class="col-sm-12">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <textarea type="amount" class="form-control" id="description" name="description"></textarea>
                    </div>
                </div> -->

            </div>


        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
    </form>

    <br>

    <form id="form_details" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">PO Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <select name="id_category" id="id_category" class="form-control" onchange="getSubcategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Sub-Category <span class='error-text'>*</span></label>
                        <span id="view_subcategory">
                            <select class="form-control" id='id_sub_category' name='id_sub_category'>
                                    <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>    

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Product <span class='error-text'>*</span></label>
                        <span id="view_item">
                            <select class="form-control" id='id_item' name='id_item'>
                                    <option value=''></option>
                            </select>
                        </span>
                    </div>
                </div>    

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GST tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="gst_tax" name="gst_tax" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Available Stock <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="available_stock" name="available_stock" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quantity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="quantity" name="quantity" onblur="calculateTotalPrice()">
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Price <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="price" name="price" onblur="calculateTotalPrice()">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax Amount / Item <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax_amount" name="tax_amount" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount Before Tax <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_price_before_tax" name="total_price_before_tax" readonly>
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Tax Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_tax_amount" name="total_tax_amount" readonly>
                    </div>
                </div>
                

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_price" name="total_price" readonly>
                    </div>
                </div>


            </div>


        </div>
    </form>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button onclick="saveTempPODetails()" type="button" name="detail_save" value="detail" class="btn btn-primary btn-lg">Save</button>
            </div>
        </div>


    <div class="form-container" id="view_temp_details_show" style="display: none;">
        <h4 class="form-group-title">PO Details</h4>

            <div class="row" id="view_temp_details">
            </div>
    </div>

    <div id="available_stock_view">
    </div>
    <div id="calculation_view">
    </div>


        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>


    </div>
</div>


<!-- <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> -->
<script>

    $('select').select2();

    // CKEDITOR.replace('description',{
    //   width: "100%",
    //   height: "100px"

    // });

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
    });

    function getSubcategory(id)
    {
        if(id != '')
        {
            $.get("/procurement/poEntry/getSubcategory/"+id,
            function(data, status)
            {
                $("#view_subcategory").html(data);
            });
        }
    }

    function procurementItemListBySubCategory(id)
    {
        // alert(id);
        if(id != '')
        {
            $.get("/procurement/poEntry/procurementItemListBySubCategory/"+id,
            function(data, status)
            {
                $("#view_item").html(data);
            });
        }
    }


    function getAvailableStocksByProductId()
    {
        var id_item = $("#id_item").val();

        if(id_item != '')
        {
            $.get("/procurement/poEntry/getAvailableStocksByProductId/"+id_item,
            function(data, status)
            {
                // alert(data);
                $("#available_stock_view").html(data);
                var product_quantity = $("#product_quantity").val();
                var product_gst_tax = $("#product_gst_tax").val();
                $("#gst_tax").val(product_gst_tax);
                $("#available_stock").val(product_quantity);

            });

            $.get("/procurement/poEntry/getItemDuplicationInTempDetails/"+id_item,
            function(data, status)
            {
                if(data == '0')
                {
                    alert('Duplicate Products Are Not Allowed' );
                    $("#id_item").val('');
                    $("#available_stock").val('');
                }
                // else
                // {
                    // var detail_save = $("#detail_save").val();
                    // alert(detail_save);
                    // saveTempPODetails();
                // }
            });
        }
    }

    function calculateTotalPrice()
    {
        var quantity = $("#quantity").val();
        var price = $("#price").val();
        var gst_tax = $("#gst_tax").val();
        var total_price = 0;
        var tax_amount = 0;
        var total_tax_amount = 0;
        var total_price_before_tax = 0;

        
        if(quantity > 0 && price > 0)
        {
            var tempPR = {};
            tempPR['quantity'] = $("#quantity").val();
            tempPR['price'] = $("#price").val();
            tempPR['gst_tax'] = $("#gst_tax").val();

            $.ajax(
            {
               url: '/procurement/poEntry/calculateTotalPrice',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                if(result == '')
                {
                    $("#total_price").val(total_price);
                    $("#tax_amount").val(tax_amount);
                }
                else
                {
                    // alert(result);
                    $("#calculation_view").html(result);
                    
                    var total_price = $("#product_total_price").val();
                    var tax_amount = $("#product_tax_amount").val();
                    var total_tax_amount = $("#product_total_tax_amount").val();
                    var total_price_before_tax = $("#product_total_price_before_tax").val();

                    $("#total_price").val(total_price);
                    $("#tax_amount").val(tax_amount);
                    $("#total_tax_amount").val(total_tax_amount);
                    $("#total_price_before_tax").val(total_price_before_tax);
                }
               }
            });
        }

    }


    function saveTempPODetails(id)
    {
        if($('#form_details').valid())
        {
            var tempPR = {};
            tempPR['id_category'] = $("#id_category").val();
            tempPR['id_sub_category'] = $("#id_sub_category").val();
            tempPR['id_item'] = $("#id_item").val();
            tempPR['gst_tax'] = $("#gst_tax").val();
            tempPR['quantity'] = $("#quantity").val();
            tempPR['balance_quantity'] = $("#quantity").val();
            tempPR['price'] = $("#price").val();
            tempPR['tax_amount'] = $("#tax_amount").val();
            tempPR['total_tax_amount'] = $("#total_tax_amount").val();
            tempPR['total_price_before_tax'] = $("#total_price_before_tax").val();
            tempPR['total_price'] = $("#total_price").val();

                $.ajax(
                {
                   url: '/procurement/poEntry/saveTempPODetails',
                   type: 'POST',
                   data:
                   {
                    data: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    if(result == '')
                    {
                        alert('No Debit Budget GL Found For Entered Data')
                    }
                    else
                    {
                        $("#view_temp_details").html(result);
                        $("#view_temp_details_show").show();

                        var total_tax_amount = $("#grand_total_tax_amount").val();
                        var total_befor_gst = $("#grand_total_befor_gst").val();
                        var total_detail = $("#total_detail").val();
                        
                        $("#po_total_tax_amount").val(total_tax_amount);
                        $("#po_total_amount_before_tax").val(total_befor_gst);
                        $("#total_amount").val(total_detail);

                        document.getElementById("form_details").reset();
                    }
                   }
                });
        }
    }

    function deleteTempPoDetails(id)
    {
        var cnf= confirm('Do you really want to delete?');
        if(cnf==true)
        {
            if(id != '')
            {
                $.get("/procurement/poEntry/deleteTempPoDetails/"+id,
                function(data, status)
                {
                    $("#view_temp_details").html(data);
                    $("#view_temp_details_show").show();
                    var total_detail = $("#total_detail").val();
                    $("#total_amount").val(total_detail);
                });
            }
        }
    }


   $(document).ready(function() {
        $("#form_po_entry").validate({
            rules: {
                po_date: {
                    required: true
                },
                 id_vendor: {
                    required: true
                },
                total_amount: {
                    required: true
                }
            },
            messages: {
                po_date: {
                    required: "<p class='error-text'>Select PO Date</p>",
                },
                id_vendor: {
                    required: "<p class='error-text'>Select Vendor</p>",
                },
                total_amount: {
                    required: "<p class='error-text'>Add Detais For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


   $(document).ready(function() {
        $("#form_details").validate({
            rules: {
                id_category: {
                    required: true
                },
                id_sub_category: {
                    required: true
                },
                id_item: {
                    required: true
                },
                quantity: {
                    required: true
                },
                price: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_sub_category: {
                    required: "<p class='error-text'>Select Sub Category</p>",
                },
                id_item: {
                    required: "<p class='error-text'>Select Product</p>",
                },
                price: {
                    required: "<p class='error-text'>Price Required</p>",
                },
                quantity: {
                    required: "<p class='error-text'>Quantity Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  
</script>