<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="grnst">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View GRN Entry</h3>
            </div>
            

        <div class="form-container">
            <h4 class="form-group-title">GRN Entry</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grn_number" name="grn_number" value="<?php echo $grn->grn_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>PO Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grn_number" name="grn_number" value="<?php echo $grn->po_number;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $grn->description;?>" readonly="readonly">
                    </div>
                </div>


            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Vendor <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="id_vendor" name="id_vendor" value="<?php echo $grn->vendor_code . ' - ' . $grn->vendor_name; ?>" readonly="readonly">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $grn->total_amount;?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GRN Entry Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="created_dt_tm" name="created_dt_tm" value="<?php echo date('d-m-Y',strtotime($grn->created_dt_tm));?>" readonly="readonly">
                    </div>
                </div>


            </div>           

            <!-- <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($grn->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($grn->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($grn->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            <?php
            if($grn->status == '2')
            {
             ?>


                <div class="col-sm-4" id="view_reject">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $grn->reason; ?>" readonly>
                    </div>
                </div>

            <?php
            }
            ?>

            </div> -->

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>
        
        <hr>

        <h3>GRN Details</h3>

        <div class="form-container">
            <h4 class="form-group-title">PO Details</h4>


            <div class="custom-table">
                <table class="table">
                    <thead>
                         <tr>
                             <th>Sl. No</th>
                             <th>Category</th>
                             <th>Sub Category</th>
                             <th>Product</th>
                             <th>Total Quantity</th>
                             <!-- <th>Balance Quantity</th> -->
                             <th>Received Quantity</th>
                             <th>Price</th>
                             <th>Total Price</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php 
                          $total = 0;
                         for($i=0;$i<count($grnDetails);$i++)
                            { 
                            // echo "<Pre>";print_r($grnDetails[$i]);exit();

                                ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $grnDetails[$i]->category_code . " - " . $grnDetails[$i]->category_name;?></td>
                            <td><?php echo $grnDetails[$i]->sub_category_code . " - " . $grnDetails[$i]->sub_category_name;?></td>
                            <td><?php echo $grnDetails[$i]->item_code . " - " . $grnDetails[$i]->item_name;?></td>
                            <td><?php echo $grnDetails[$i]->total_quantity;?></td>
                            <!-- <td><?php echo $grnDetails[$i]->balance_quantity;?></td> -->
                            <td><?php echo $grnDetails[$i]->quantity;?></td>
                            <td><?php echo $grnDetails[$i]->price;?></td>
                            <td><?php echo $grnDetails[$i]->total_price;?></td>

                             </tr>
                          <?php 
                          $total = $total + $grnDetails[$i]->total_price;
                        }
                        $total = number_format($total, 2, '.', ',');
                        ?>

                        <tr>
                            <td bgcolor="" colspan="6"></td>
                            <td bgcolor=""><b> Total : </b></td>
                            <td bgcolor=""><b><?php echo $total; ?></b></td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>