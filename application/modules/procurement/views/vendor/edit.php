<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Vendor</h3>
        </div>
        <form id="form_main" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Vendor Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendor Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $vendorRegistration->code; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Vendor Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vname" name="vname" value="<?php echo $vendorRegistration->name; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="vemail" name="vemail"  value="<?php echo $vendorRegistration->email; ?>">
                        </div>
                    </div>

                </div>

                <div class="row">
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mobile Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="vmobile" name="vmobile" value="<?php echo $vendorRegistration->mobile; ?>">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number </label>
                            <input type="number" class="form-control" id="vphone" name="vphone" value="<?php echo $vendorRegistration->phone; ?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>GST IN <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="gst_in" name="gst_in" value="<?= $vendorRegistration->gst_in ?>">
                        </div>
                    </div>



                </div>

                <div class="row">
                    


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>CIN <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="cin" name="cin" value="<?= $vendorRegistration->cin ?>">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Line 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="vaddress_one" name="vaddress_one" value="<?php echo $vendorRegistration->address_one; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address Line 2 / Landmark</label>
                            <input type="text" class="form-control" id="vaddress_two" name="vaddress_two" value="<?php echo $vendorRegistration->address_two; ?>">
                        </div>
                    </div>



                </div>

                <div class="row">
                    


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="city" name="city" value="<?php echo $vendorRegistration->city; ?>">
                        </div>
                    </div>         
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="vcountry" id="vcountry" class="form-control" onchange="getStateByCountry(this.value)" style="width: 360px">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($vendorRegistration->country == $record->id)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id="view_state">
                                <select class="form-control" id='vstate' name='vstate'>
                                        <option value=''></option>
                                </select>
                            </span>
                        </div>
                    </div>  

                
                </div>

                <div class="row">
                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode / Pincode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="vzipcode" name="vzipcode" value="<?php echo $vendorRegistration->zipcode; ?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($vendorRegistration->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($vendorRegistration->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>



                </div>

            </div>
            
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

    $('select').select2();


    function getStateByCountry(id)
    {
        if(id != '')
        {
            $.get("/procurement/vendor/getStateByCountry/"+id,
            function(data, status)
            {
                $("#view_state").html(data);
            });
        }
    }

    

    $(document).ready(function()
    {
        var country = <?php echo $vendorRegistration->country; ?>

        if(country > 0)
        {
            $.get("/procurement/vendor/getStateByCountry/"+country,
            function(data, status)
            {
                var state = <?php echo $vendorRegistration->state; ?>;
                // alert(id_state);
                $("#view_state").html(data);
                $("#vstate").find('option[value="'+state+'"]').attr('selected',true);
                $('select').select2();
            });
        }

        $("#form_main").validate({
            rules: {
                vname: {
                    required: true
                },
                 vemail: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 vnric: {
                    required: true
                },
                 vmobile: {
                    required: true
                },
                 vaddress_one: {
                    required: true
                },
                 vcountry: {
                    required: true
                },
                 vzipcode: {
                    required: true
                },
                 city: {
                    required: true
                },
                 vstate: {
                    required: true
                },
                 cname: {
                    required: true
                },
                 cphone: {
                    required: true
                },
                 caddress_one: {
                    required: true
                },
                 ccountry: {
                    required: true
                },
                 cemail: {
                    required: true
                },
                 tax_no: {
                    required: true
                },
                 caddress_two: {
                    required: true
                },
                 czipcode: {
                    required: true
                },
                 cstate: {
                    required: true
                },
                 gst_in: {
                    required: true
                },
                 cin: {
                    required: true
                }
            },
            messages: {
                vname: {
                    required: "<p class='error-text'>Vendor Name Required</p>",
                },
                vemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                vnric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                vmobile: {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                vaddress_one: {
                    required: "<p class='error-text'>Vendor Address One Required</p>",
                },
                vcountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                vzipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                vstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
                cname: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                cphone: {
                    required: "<p class='error-text'>Company Phone Required</p>",
                },
                caddress_one: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                ccountry: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                cemail: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                tax_no: {
                    required: "<p class='error-text'>Tax Number Required</p>",
                },
                caddress_two: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                czipcode: {
                    required: "<p class='error-text'>Company Zipcode Required</p>",
                },
                cstate: {
                    required: "<p class='error-text'>Select State</p>",
                },
                gst_in: {
                    required: "<p class='error-text'>GST IN Required</p>",
                },
                cin: {
                    required: "<p class='error-text'>CIN Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>