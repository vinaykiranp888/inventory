<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Product</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Product</a> -->
    </div>

     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Code / Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParameters['name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Sub-Category Code</label>
                      <div class="col-sm-8">
                        <select name="id_procurement_sub_category" id="id_procurement_sub_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementSubCategoryList))
                            {
                                foreach ($procurementSubCategoryList as $record)
                                {?>
                                   <option value="<?php echo $record->id; ?>"
                                        <?php 
                                        if($record->id == $searchParameters['id_procurement_sub_category'])
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category Code</label>
                      <div class="col-sm-8">
                        <select name="id_procurement_category" id="id_procurement_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                                   <option value="<?php echo $record->id; ?>"
                                        <?php 
                                        if($record->id == $searchParameters['id_procurement_category'])
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button href="list" class="btn btn-link">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Name</th>
            <th>Category</th>
            <th>Sub-Category</th>
            <th>Stock Available</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($procurementItemList))
          {
            $i=1;
            foreach ($procurementItemList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->pr_category_code . " - " . $record->pr_category_name; ?></td>
                <td><?php echo $record->pr_sub_category_code . " - " . $record->pr_sub_category_name; ?></td>
                <td><?php echo $record->quantity ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View Summary">Summary</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>