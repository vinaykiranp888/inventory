<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Product Details</h3>
            <a href="../list" class="btn btn-link"> < Back</a>
        </div>


        <div class="form-container">
          <h4 class="form-group-title">Product Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Product Code : </dt>
                              <dd><?php echo $product->code ?></dd>
                          </dl>
                          <dl>
                              <dt>Manufacturer : </dt>
                              <dd><?php echo $product->manufacturer_code . " - " . $product->manufacturer_name ?></dd>
                          </dl>
                          <dl>
                              <dt>Category : </dt>
                              <dd><?php echo $product->pr_category_code . " - " . $product->pr_category_name ?></dd>
                          </dl>
                          <dl>
                              <dt>Part No. : </dt>
                              <dd><?php echo $product->part_number; ?></dd>
                          </dl>
                          <dl>
                              <dt>SPQ : </dt>
                              <dd><?php echo $product->spq; ?></dd>
                          </dl>
                          <dl>
                              <dt>Available Stock. : </dt>
                              <dd><?php echo $product->quantity; ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Product Name : </dt>
                              <dd><?php echo ucwords($product->name); ?></dd>
                          </dl>                          
                          <dl>
                              <dt>Package : </dt>
                              <dd><?php echo $product->package; ?></dd>
                          </dl>
                          <dl>
                              <dt>Sub-category : </dt>
                              <dd><?php echo $product->pr_sub_category_code . " - " . $product->pr_sub_category_name ?></dd>
                          </dl>
                          <dl>
                              <dt>Alternative Part No. : </dt>
                              <dd><?php echo $product->alternative_part_number; ?></dd>
                          </dl>
                          <dl>
                              <dt>MOQ : </dt>
                              <dd><?php echo $product->moq; ?></dd>
                          </dl>
                      </div>
  
                  </div>
              </div>


        </div>

        <br>

        <div class="page-title clearfix">
            <h3>Product Summary</h3>
        </div>


       <div class="custom-table">
          <table class="table" id="list-table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Date Time</th>
                <th>Reference</th>
                <th>Description</th>
                <th>Initial Quantity</th>
                <th>Cre / Deb</th>
                <th>Quantity</th>
                <th>Updated By</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($productQuantity))
              {
                $i=1;
                foreach ($productQuantity as $record) {
              ?>
                  <tr>
                    <td><?php echo $i ?></td>
                    <td><?php echo date('d-m-Y H:i:s', strtotime($record->created_dt_tm)) ?></td>
                    <td><?php 
                    if( $record->grn_quantity > 0)
                    {
                      echo $record->grn_number;
                    }
                    elseif($record->assembly_quantity > 0)
                    {
                      echo $record->assembly_number;
                    }
                    elseif($record->assembly_return_quantity > 0)
                    {
                      echo $record->return_number;
                    }
                    ?></td>
                    <td ><?php echo $record->description ?></td>
                    <td ><?php echo $record->previous_quantity ?></td>
                    <td><?php 
                    if( $record->grn_quantity > 0)
                    {
                      echo $record->grn_quantity;
                    }
                    elseif($record->assembly_quantity > 0)
                    {
                      echo $record->assembly_quantity;
                    }
                    elseif($record->assembly_return_quantity > 0)
                    {
                      echo $record->assembly_return_quantity;
                    }
                    ?></td>
                    <td ><?php echo $record->quantity ?></td>
                    <td><?php echo $record->creater_name ?></td>
                  </tr>
              <?php
              $i++;
                }
              }
              ?>
            </tbody>
          </table>
        </div>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>

    $('select').select2();


    CKEDITOR.replace('description',{
      width: "100%",
      height: "300px"

    }); 


    function getSubcategory(id)
    {
        if(id != '')
        {
            $.get("/procurement/procurementItem/getSubcategory/"+id,
            function(data, status)
            {
                $("#view_subcategory").html(data);
            });
        }
    }


    $(document).ready(function()
    {

        var id_procurement_category = <?php echo $procurementItem->id_procurement_category; ?>

        if(id_procurement_category > 0)
        {
            $.get("/procurement/procurementItem/getSubcategory/"+id_procurement_category,
            function(data, status)
            {
                var id_procurement_sub_category = "<?php echo $procurementItem->id_procurement_sub_category ?>";
                // alert(id_state);
                $("#view_subcategory").html(data);
                $("#id_procurement_sub_category").find('option[value="'+id_procurement_sub_category+'"]').attr('selected',true);
                $('select').select2();

            });
        }


        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                 name: {
                    required: true
                },
                 id_procurement_category: {
                    required: true
                },
                 id_procurement_sub_category: {
                    required: true
                },
                 status: {
                    required: true
                },
                part_number: {
                    required: true
                },
                 value: {
                    required: true
                },
                 package: {
                    required: true
                },
                 manufacturer: {
                    required: true
                },
                 spq: {
                    required: true
                },
                 moq: {
                    required: true
                },
                 price: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                id_procurement_category: {
                    required: "<p class='error-text'>Select Category Code</p>",
                },
                id_procurement_sub_category: {
                    required: "<p class='error-text'>Select Sub-Category Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                },
                part_number: {
                    required: "<p class='error-text'>Part Number Required</p>",
                },
                value: {
                    required: "<p class='error-text'>Value Required</p>",
                },
                package: {
                    required: "<p class='error-text'>Package Required</p>",
                },
                manufacturer: {
                    required: "<p class='error-text'>Manufacturer Required</p>",
                },
                spq: {
                    required: "<p class='error-text'>SPQ required</p>",
                },
                moq: {
                    required: "<p class='error-text'>MOQ Required</p>",
                },
                price: {
                    required: "<p class='error-text'>Proce Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    
</script>