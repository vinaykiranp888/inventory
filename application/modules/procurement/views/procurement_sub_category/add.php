<?php $this->load->helper("form"); ?>
<p class='error-text'></p>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sub Category</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Sub-Category Details</h4>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category <span class='error-text'>*</span></label>
                        <select name="id_procurement_category" id="id_procurement_category" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($procurementCategoryList))
                            {
                                foreach ($procurementCategoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                  
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>

        </div>

            
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                code: {
                    required: true
                },
                 name: {
                    required: true
                },
                 id_procurement_category: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Sub Category Code required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                id_procurement_category: {
                    required: "<p class='error-text'>Select Category Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>