<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssemblyDistribution extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assembly_distribution_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('assembly_distribution.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_assembly'] = $this->security->xss_clean($this->input->post('id_assembly'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['assemblyDistributionList'] = $this->assembly_distribution_model->getAssemblyDistributionListSearch($formData);

            $data['assemblyTeamList'] = $this->assembly_distribution_model->assemblyTeamListByStatus('1');

            // echo '<Pre>';print_r($data['assemblyDistributionList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("assembly_distribution/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('assembly_distribution.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {

                // echo "<Pre>";print_r($_POST);exit;

                $id_assembly = $this->security->xss_clean($this->input->post('id_assembly'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $description = $this->security->xss_clean($this->input->post('description'));

                $generated_number = $this->assembly_distribution_model->generateAssemblyDistributionNumber();
                
                $total_amount = str_replace(",", "", $total_amount);

                $data = array(
                    'id_assembly' => $id_assembly,
                    'reference_number' =>$generated_number,
                    'total_amount' => $total_amount,
                    'balance_amount' => $total_amount,
                    'description' => $description,
                    'created_by' => $id_user,
                    'status'=>3
                );

                // echo "<Pre>";print_r($data);exit;
               
                $inserted_id = $this->assembly_distribution_model->addAssemblyDistribution($data);

                if($inserted_id)
                {
                    $this->assembly_distribution_model->moveTempToAssemblyDistributionDetails($inserted_id);
                    // $update_data = array('is_po' => $inserted_id);
                    // $updated_pr = $this->assembly_distribution_model->updatePR($update_data,$id);
                }
                
                redirect('/procurement/assemblyDistribution/list');
            }
            else
            {
                $this->assembly_distribution_model->deleteTempPoDetailsBySessionId($id_session);
            }

            $data['procurementCategoryList'] = $this->assembly_distribution_model->procurementCategoryListByStatus('1');
            $data['assemblyTeamList'] = $this->assembly_distribution_model->assemblyTeamListByStatus('1');

            // echo "<Pre>";print_r($data['procurementCategoryList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("assembly_distribution/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('assembly_distribution.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/assemblyDistribution/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            
            $data['assemblyDistribution'] = $this->assembly_distribution_model->getAssemblyDistribution($id);
            $data['assemblyDistributionDetails'] = $this->assembly_distribution_model->getAssemblyDistributionDetails($id);
            $data['procurementCategoryList'] = $this->assembly_distribution_model->procurementCategoryListByStatus('1');
            $data['assemblyTeamList'] = $this->assembly_distribution_model->assemblyTeamListByStatus('1');

            // echo "<Pre>";print_r($data['po']);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Assembly Distribution';
            $this->loadViews("assembly_distribution/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('assembly_distribution.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/assemblyDistribution/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->assembly_distribution_model->editAssemblyDistributionList($data,$id);

                redirect('/procurement/assemblyDistribution/approvallist');

                // $check_duplication = $this->budget_amount_model->editBudgetAmount($data,$id);

                // print_r($this->input->post());exit;
            }
            
            $data['poMaster'] = $this->assembly_distribution_model->getMasterPoDetails($id);
            $data['poDetails'] = $this->assembly_distribution_model->getPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("assembly_distribution/view", $this->global, $data, NULL);
        }
    }

    function approvallist()
    {
        if ($this->checkAccess('assembly_distribution.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $resultprint = $this->input->post();

            if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         
                         $result = $this->assembly_distribution_model->editAssemblyDistributionList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':
                     
                     break;
                 
                default:
                     break;
             }
                
            }


            
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));


            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['poList'] = $this->assembly_distribution_model->getAssemblyDistributionListSearch($formData);

            $data['financialYearList'] = $this->assembly_distribution_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->assembly_distribution_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->assembly_distribution_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->assembly_distribution_model->getDepartmentCodeList();
            

            // echo "<Pre>";print_r($formData);exit();
            
                // $array = $this->security->xss_clean($this->input->post('checkvalue'));
                // if (!empty($array))
                // {

                //     $result = $this->assembly_distribution_model->editAssemblyDistributionList($array);
                //     redirect($_SERVER['HTTP_REFERER']);
                // }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("assembly_distribution/approval_list", $this->global, $data, NULL);

        }
    }

    function getSubcategory($id)
    {
        $results = $this->assembly_distribution_model->procurementSubCategoryListByProcurementId($id);
                // echo "<Pre>";print_r($results);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_sub_category' id='id_sub_category' class='form-control' onchange='procurementItemListBySubCategory(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $sub_category_code = $results[$i]->code;
            $sub_categoryname = $results[$i]->name;

            $table.='<option value="'. $id.'">'.$sub_category_code . ' - '. $sub_categoryname .'</option>';

            }

            $table.="</select>";
            echo $table;
            exit;
    }

    function procurementItemListBySubCategory($id)
    {
        $results = $this->assembly_distribution_model->procurementItemListBySubCategory($id);
                // echo "<Pre>";print_r($results);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_item' id='id_item' class='form-control' onchange='getAvailableStocksByProductId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $sub_category_code = $results[$i]->code;
            $sub_categoryname = $results[$i]->name;

            $table.='<option value="'. $id.'">'.$sub_category_code . ' - '. $sub_categoryname .'</option>';

            }

            $table.="</select>";
            echo $table;
            exit;
    }

    function saveTempAssemblyDistributionDetails()
    {
        $id_session = $this->session->my_session_id;
        $data = $this->security->xss_clean($this->input->post('data'));
        
        $data['id_session'] = $id_session;
        $inserted_id = $this->assembly_distribution_model->saveTempAssemblyDistributionDetails($data);
        // }
        $data = $this->displayTempData();
        
        echo $data;        
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->assembly_distribution_model->getTempAssemblyDistributionDetailsByIdSessionForView($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $total_detail = 0;
        if(!empty($details))
        {
            
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Total Price</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $category_name = $details[$i]->category_name;
                        $category_code = $details[$i]->category_code;
                        $sub_category_name = $details[$i]->sub_category_name;
                        $sub_category_code = $details[$i]->sub_category_code;
                        $item_name = $details[$i]->item_name;
                        $item_code = $details[$i]->item_code;
                        $quantity = $details[$i]->quantity;
                        $price = $details[$i]->price;
                        $total_price = $details[$i]->total_price;

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$category_code - $category_name</td>
                            <td>$sub_category_code - $sub_category_name</td>
                            <td>$item_code - $item_name</td>
                            <td>$quantity</td>
                            <td>$price</td>
                            <td>$total_price</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempPoDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_detail = $total_detail + $total_price;
                    }

                    $total_detail = number_format($total_detail, 2, '.', ',');

                    $table .= "

                    <tr>
                            <td bgcolor='' colspan='5'></td>
                            <td bgcolor=''><b> Total : </b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail = number_format($total_detail, 2, '.', ',');
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function deleteTempPoDetails($id)
    {
        $inserted_id = $this->assembly_distribution_model->deleteTempPoDetails($id);
        $data = $this->displayTempData();
        echo $data; 
    }

    function getAvailableStocksByProductId($id)
    {
        $item = $this->assembly_distribution_model->getProductByProductId($id);
        if($item)
        {
            echo $item->quantity;exit;
        }
        else
        {
            echo 0;exit;
        }
    }

    function getItemDuplicationInTempDetails($id_item)
    {
        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;
        $data['id_item'] = $id_item;
        $data['id_session'] = $id_session;

        $result = $this->assembly_distribution_model->getItemDuplicationInTempDetails($data);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }
}