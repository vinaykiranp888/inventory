<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Unit extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('unit_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('unit.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['unitList'] = $this->unit_model->unitListSearch($name);
            // echo "<Pre>"; print_r($data['unitList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : List Unit Category';
            $this->loadViews("unit/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('unit.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user

                );
            
                $result = $this->unit_model->addNewUnit($data);
                redirect('/procurement/unit/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add Unit Category';
            $this->loadViews("unit/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('unit.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if ($id == null)
            {
                redirect('/procurement/unit/list');
            }
            if($this->input->post())
            {

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );
                
                $result = $this->unit_model->editUnit($data,$id);
                redirect('/procurement/unit/list');
            }
            $data['unit'] = $this->unit_model->getUnitDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Unit Category';
            $this->loadViews("unit/edit", $this->global, $data, NULL);
        }
    }   
}