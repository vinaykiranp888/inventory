<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TenderQuotation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tender_quotation_model');
        $this->load->model('Pr_model');

        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('tender_quotation.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['tenderQuotationList'] = $this->tender_quotation_model->getTenderQuotationListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Pr_model->budgetYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            


            $this->global['pageTitle'] = 'Inventory Management : List Tender Quotation';
            //print_r($subjectDetails);exit;
            $this->loadViews("tender_quotation/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('tender_quotation.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                
                $generated_number = $this->tender_quotation_model->generateQuotationNumber();

                


                $formData = $this->input->post();

                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $opening_date = $this->security->xss_clean($this->input->post('opening_date'));
                // $id_pr_detail = $this->security->xss_clean($this->input->post('id_pr_detail'));
                // $visit_location = $this->security->xss_clean($this->input->post('visit_location'));
                // $visit_date_time = $this->security->xss_clean($this->input->post('visit_date_time'));
                $id = $this->security->xss_clean($this->input->post('type_of_pr'));

                $prMaster = $this->Pr_model->getMasterPrDetails($id);
                $prDetails = $this->Pr_model->getPRDetailsByMasterID($id);

                $description = $this->security->xss_clean($this->input->post('description'));

                


                $data = array(
                    'id_pr' => $id,
                    'quotation_number' =>$generated_number,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'opening_date' => date('Y-m-d',strtotime($opening_date)),
                    'type' => $prMaster->type,
                    'id_financial_year' => $prMaster->id_financial_year,
                    'id_budget_year' => $prMaster->id_budget_year,
                    'department_code' => $prMaster->department_code,
                    'id_department' => $prMaster->id_department,
                    'id_vendor' => $prMaster->id_vendor,
                    'amount' => $prMaster->amount,
                    'description' => $description,
                    'created_by' => $user_id,
                    'status'=>'1'
                );
               
                $inserted_id = $this->tender_quotation_model->addTenderQuotation($data);

                if($inserted_id)
                {
                    $update_data = array('is_po' => $inserted_id);
                    $updated_pr = $this->Pr_model->updatePR($update_data,$id);

                    $data_moved = $this->tender_quotation_model->moveSessionTempData($id_session,$inserted_id);
                }


                for($i=0;$i<count($formData['id_pr_detail']);$i++)
                 {

                    $id_prd = $formData['id_pr_detail'][$i];
                    $visit_location = $formData['visit_location'][$i];
                    $visit_date_time = $formData['visit_date_time'][$i];
                    $period = $formData['period'][$i];
                    $period_type = $formData['period_type'][$i];




                    $prDetail = $this->Pr_model->getPRDetailsByID($id_prd);

                    // echo "<Pre>";print_r($formData['visit_date_time'][$i]);exit;

                        $dt_fund = $prDetail->dt_fund;
                        $dt_department = $prDetail->dt_department;
                        $dt_activity = $prDetail->dt_activity;
                        $dt_account = $prDetail->dt_account;
                        $cr_fund = $prDetail->cr_fund;
                        $cr_department = $prDetail->cr_department;
                        $cr_activity = $prDetail->cr_activity;
                        $cr_account = $prDetail->cr_account;
                        $type = $prDetail->type;
                        $id_category = $prDetail->id_category;
                        $id_sub_category = $prDetail->id_sub_category;
                        $id_item = $prDetail->id_item;
                        $quantity = $prDetail->quantity;
                        $price = $prDetail->price;
                        $id_tax = $prDetail->id_tax;
                        $tax_price = $prDetail->tax_price;
                        $total_final = $prDetail->total_final;


                    $detailsData = array(
                        'id_tender_quotation'=>$inserted_id,
                        'dt_fund'=>$dt_fund,
                        'dt_department'=>$dt_department,
                        'dt_activity'=>$dt_activity,
                        'dt_account'=>$dt_account,
                        'cr_fund'=>$cr_fund,
                        'cr_department'=>$cr_department,
                        'cr_activity'=>$cr_activity,
                        'cr_account'=>$cr_account,
                        'type'=>$type,
                        'id_category'=>$id_category,
                        'id_sub_category'=>$id_sub_category,
                        'id_item'=>$id_item,
                        'quantity'=>$quantity,
                        'price'=>$price,
                        'id_tax'=>$id_tax,
                        'period'=>$period,
                        'period_type'=>$period_type,
                        'visit_location'=>$visit_location,
                        'visit_date_time'=>date('Y-m-d',strtotime($visit_date_time)),
                        'tax_price'=>$tax_price,
                        'total_final'=>$total_final

                    );
                //      // echo "<Pre>";print_r($detailsData);exit;
                    $result = $this->tender_quotation_model->addNewTenderQuotationDetail($detailsData);
                   
                }
                redirect('/procurement/tenderQuotation/list');
              }
              else
              {
                    $deleted = $this->tender_quotation_model->deleteTempComiteeBySession($id_session);
                    $deleted = $this->tender_quotation_model->deleteTempRemarksBySession($id_session);
              }
              $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['prEntryList'] = $this->Pr_model->getPRListForProcess('Tender');
            $data['staffList'] = $this->tender_quotation_model->getStaffList();
            $this->global['pageTitle'] = 'Inventory Management : Add Tender Quotation';
            //print_r($subjectDetails);exit;
            $this->loadViews("tender_quotation/add", $this->global, $data, NULL);

            }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('tender_quotation.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/pr_entry/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }

            $data['tenderQuotationMaster'] = $this->tender_quotation_model->getTenderQuotaion($id);
            $data['tenderQuotationDetails'] = $this->tender_quotation_model->getTenderQuotationDetailsByMasterId($id);
            $data['tenderComiteeDetails'] = $this->tender_quotation_model->getTenderComiteeDetailsByMasterId($id);
            $data['tenderRemarksDetails'] = $this->tender_quotation_model->getTenderRemarksDetailsByMasterId($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : View Tender Quotation';
            $this->loadViews("tender_quotation/edit", $this->global, $data, NULL);
        }
    }

    

    function getData($id) {
        // echo "<Pre>";print_r($id);exit;

        // $financialYearList = $this->Pr_model->financialYearListByStatus('1');
        $vendorList = $this->Pr_model->vendorListByStatus('Approved');

        $prMaster = $this->Pr_model->getMasterPrDetails($id);
        $prDetails = $this->Pr_model->getPrDetails($id);


        $array = $this->Pr_model->financialYearListByStatus('1');

        // $prDetails = $data['prDetails'];
        $financialYearList = json_decode( json_encode($array), true);

        // $vendorList = json_decode( json_encode($vendorList), true);
        // $financialYearList = json_decode( json_encode($array), true);

// echo "<Pre>";print_r($prMaster);exit;




        $table = "

            <script type='text/javascript'>
                $('select').select2();
            </script



                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$prMaster->pr_number'>
                        </div>
                    </div>

                   <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Type <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$prMaster->type'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='$prMaster->pr_entry_date'>
                        </div>
                    </div>

                    
                </div>

                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Financial Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->financial_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Budget Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->budget_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Department <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$prMaster->department_code - $prMaster->department' readonly='readonly'>
                        </div>
                    </div>

                </div>

                <div class='row'>

                  <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Vendor <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$prMaster->vendor_code - $prMaster->vendor' readonly='readonly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Description <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='pr_description'  value='$prMaster->description'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Total Amount <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$prMaster->amount' readonly='readonly'>
                        </div>
                    </div>

                 </div>

                  <div class='row'>


                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Tender Start Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' name='start_date' autocomplete='off'>
                        </div>
                    </div>
               

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Tender Close Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' name='end_date' autocomplete='off'>
                        </div>
                    </div>


                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Tender Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' name='description' id='description'>
                        </div>
                    </div>



                </div>

                <script>
                  $( function() {
                    $( '.datepicker' ).datepicker({
                        changeYear: true,
                        changeMonth: true,
                    });
                  } );
                </script>


                ";

                
                    // <div class='col-sm-4'>
                    //     <div class='form-group'>
                    //         <label>Tender Opening Date <span class='error-text'>*</span></label>
                    //         <input type='text' class='form-control datepicker' name='opening_date' autocomplete='off'>
                    //     </div>
                    // </div>


                $table1="

            

            <select name='id_vendor' id='id_vendor' class='form-control'>";
            $table1.="<option value=''>Select</option>";

            for($i=0;$i<count($vendorList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $vendorList[$i]->id;
            $code = $vendorList[$i]->code;
            $name = $vendorList[$i]->name;
            $table1.="<option value=".$id.">";

                    if($id == $prMaster->id_vendor)
                    {
                      // echo "selected=selected";
                    }
                    // echo $code . " - " . $name; 

                    $table1.="</option>";

            }
            $table1.="
            </select>
                 </div>
              </div>
            </div>
            ";






        $table.="


        <script>
          $( function() {
            $( '.datepicker' ).datepicker({
                changeYear: true,
                changeMonth: true,
            });
          } );
        </script>



        <h3>&nbsp;&nbsp;&nbsp;PR Details</h3>
             

        <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Site Visit Location</th>
             <th>Site Visit Date -Time</th>
             <th>Completion Period Type</th>
             <th>Completion Period</th>
             <th>Debit GL Code</th>
             <th>Credit GL Code</th>
             <th>Category</th>
             <th>Sub Category</th>
             <th>Item</th>
             <th>Tax</th>
             <th>Qty</th>
             <th>Price</th>
             <th>Tax Amount</th>
             <th>Final Total</th>
             
         </tr>
        </thead>
        <tbody>";
            $total = 0;
          for($i=0;$i<count($prDetails);$i++)
          {
          $j=$i+1; 
            $table.="<tr>
            <td>".$j."<input type='hidden' name='id_pr_detail[]' class='form-control' value='".$prDetails[$i]->id."'></td>
            <td class='text-center'>
                <input type='text' name='visit_location[]' class='form-control' style='width:198px'>
            </td>
            <td>
                <input type='text' class='form-control datepicker' name='visit_date_time[]' autocomplete='off' style='width:198px'>
            </td>
            <td>
                <select name='period_type[]' id='period_type[]' class='form-control' style='width:198px'>
                    <option value=''>Select</option>
                    <option value='Months'>Months</option>
                    <option value='Years'>Years</option>
                </select>
            </td>
            <td class='text-center'>
                <input type='number' name='period[]' class='form-control' style='width:198px'>
            </td>
            <td>".$prDetails[$i]->dt_fund. " - " . $prDetails[$i]->dt_department. " - " . $prDetails[$i]->dt_activity . " - "  . $prDetails[$i]->dt_account . "</td>
            <td>".$prDetails[$i]->cr_fund. " - " . $prDetails[$i]->cr_department. " - " . $prDetails[$i]->cr_activity . " - "  . $prDetails[$i]->cr_account . "</td>
            <td>".$prDetails[$i]->category_code . " - " .$prDetails[$i]->category_name."</td>
            <td>".$prDetails[$i]->sub_category_code . " - " .$prDetails[$i]->sub_category_name."</td>
            <td>".$prDetails[$i]->item_code . " - " .$prDetails[$i]->item_name."</td>
            <td>".$prDetails[$i]->tax_code . " - " .$prDetails[$i]->tax_name."</td>
            <td>".$prDetails[$i]->quantity."</td>
            <td>".$prDetails[$i]->price."</td>
            <td>".$prDetails[$i]->tax_price."</td>
            <td>".$prDetails[$i]->total_final."</td>
            

             </tr>";

             $total = $total + $prDetails[$i]->total_final;
         }
         $total = number_format($total, 2, '.', ',');

        $table.="
        <tr>
            <td bgcolor='' colspan='13'></td>
            <td bgcolor=''><b> Total : </b></td>
            <td bgcolor=''><b>".$total."</b></td>
        </tr>

        </tbody>
    </table>
    </div>";
       echo $table;

    }

    function tempaddComitee()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit();
        
        $inserted_id = $this->tender_quotation_model->addTempComitee($tempData);

        $data = $this->displayTempComiteeData();        
        echo $data;        
    }

    function deleteTempCommitee($id)
    {
        $inserted_id = $this->tender_quotation_model->deleteTempComitee($id);
        $data = $this->displayTempComiteeData();
        echo $data; 
    } 

    function displayTempComiteeData()
    {
        $id_session = $this->session->my_session_id;
        $comitee_details = $this->tender_quotation_model->getTempComiteeBySession($id_session); 
        if(!empty($comitee_details))
        {
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Staff</th>
                    <th>Description</th>
                    <th>Action</th>
                    </tr>";
                    for($i=0;$i<count($comitee_details);$i++)
                    {
                        $id = $comitee_details[$i]->id;
                        $salutation = $comitee_details[$i]->salutation;
                        $staff_name = $comitee_details[$i]->staff_name;
                        $ic_no = $comitee_details[$i]->ic_no;
                        $description = $comitee_details[$i]->comitee_description;

                        $j=$i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $salutation . $staff_name</td>
                            <td>$description</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempCommitee($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick=''>Delete</a>
                    }

        $table.= "</table>";
        }else
        {
            $table="";
        }
        return $table;
    }

    function tempaddRemarks()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        
        $inserted_id = $this->tender_quotation_model->addTempRemarks($tempData);

        $data = $this->displayTempRemarksData();        
        echo $data;        
    }

    function deleteTempRemarks($id)
    {
        $inserted_id = $this->tender_quotation_model->deleteTempRemarks($id);
        $data = $this->displayTempRemarksData();
        echo $data; 
    } 

    function displayTempRemarksData()
    {
        $id_session = $this->session->my_session_id;
        $remark_details = $this->tender_quotation_model->getTempRemarksBySession($id_session);

        if(!empty($remark_details))
        {
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Remarks</th>
                    <th>Action</th>
                    </tr>";
                    $total_detail = 0;
                    for($i=0;$i<count($remark_details);$i++)
                    {
                        $id = $remark_details[$i]->id;
                        $remarks = $remark_details[$i]->remarks;

                        $j=$i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$remarks</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempRemarks($id)'>Delete</a>
                            <td>
                        </tr>";
                    }

        $table.= "</table>";
                                // <span onclick='deleteTempRemarks($id)'>Delete</a>

        }else
        {
            $table="";
        }

        return $table;
    }


}
