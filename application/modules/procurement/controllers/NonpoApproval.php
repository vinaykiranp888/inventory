<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class NonpoApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Nonpo_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('pr_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['nonPoList'] = $this->Nonpo_model->getNonPOListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array))
                {

                    $result = $this->Nonpo_model->editPOList($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("nonpo_approval/list", $this->global, $data, NULL);

        }
    }
    
    
}
