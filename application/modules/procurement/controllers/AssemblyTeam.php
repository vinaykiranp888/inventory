<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssemblyTeam extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assembly_team_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('assembly_team.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['mobile'] = $this->security->xss_clean($this->input->post('mobile'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['assemblyTeamList'] = $this->assembly_team_model->assemblyTeamListSearch($formData);

            // print_r($formData);exit;
            $this->global['pageTitle'] = 'Inventory Management :List AssemblyTeam';
            $this->loadViews("assembly_team/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('assembly_team.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if($this->input->post())
            {
                
                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $generated_number = $this->assembly_team_model->generateAssemblyTeamNumber();

                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));


                $data = array(
                    'name' => $vname,
                    'code' => $generated_number,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'city' => $city,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'created_by' => $id_user
                );

                $inserted_id = $this->assembly_team_model->addNewAssemblyTeam($data);
                redirect('/procurement/assemblyTeam/list');
            }

            $data['countryList'] = $this->assembly_team_model->countryListByStatus('1');
            // $data['bankList'] = $this->assembly_team_model->bankListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add AssemblyTeam';
            $this->loadViews("assembly_team/add", $this->global, $data, NULL);

        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('assembly_team.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if ($id == null)
            {
                redirect('/procurement/assemblyTeam/list');
            }
            if($this->input->post())
            {
                $vname = $this->security->xss_clean($this->input->post('vname'));
                $vemail = $this->security->xss_clean($this->input->post('vemail'));
                $vphone = $this->security->xss_clean($this->input->post('vphone'));
                $vnric = $this->security->xss_clean($this->input->post('vnric'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $vmobile = $this->security->xss_clean($this->input->post('vmobile'));
                $vaddress_one = $this->security->xss_clean($this->input->post('vaddress_one'));
                $vaddress_two = $this->security->xss_clean($this->input->post('vaddress_two'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $vcountry = $this->security->xss_clean($this->input->post('vcountry'));
                $vstate = $this->security->xss_clean($this->input->post('vstate'));
                $vzipcode = $this->security->xss_clean($this->input->post('vzipcode'));
                $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));


                $data = array(
                    'name' => $vname,
                    'email' => $vemail,
                    'phone' => $vphone,
                    'nric' => $vnric,
                    'gender' => $gender,
                    'mobile' => $vmobile,
                    'address_one' => $vaddress_one,
                    'address_two' => $vaddress_two,
                    'city' => $city,
                    'country' => $vcountry,
                    'state' => $vstate,
                    'zipcode' => $vzipcode,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                // echo "<Pre>"; print_r($data);exit;

                
                $inserted_id = $this->assembly_team_model->editAssemblyTeam($data,$id);
                redirect('/procurement/assemblyTeam/list');

            }

            $data['assemblyTeam'] = $this->assembly_team_model->getAssemblyTeam($id);
            $data['countryList'] = $this->assembly_team_model->countryListByStatus('1');

            // echo "<Pre>"; print_r($data['assembly_teamRegistration']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View AssemblyTeam';
            $this->loadViews("assembly_team/edit", $this->global, $data, NULL);
        }
    }

    function blockList()
    {
        if ($this->checkAccess('assembly_team.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['assembly_teamList'] = $this->assembly_team_model->assembly_teamListSearchForBlocList($formData);

            $this->global['pageTitle'] = 'Inventory Management :Approval List AssemblyTeam';
            $this->loadViews("assembly_team/block_list", $this->global, $data, NULL);
        }
    }

    function getStateByCountry($id_country)
    {
        $results = $this->assembly_team_model->getStateByCountry($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
     ";

        $table.="
        <select name='vstate' id='vstate' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}
?>