<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PrApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pr_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('pr_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $status = 0;
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['prList'] = $this->Pr_model->prList($status);

            $this->loadViews("pr_approval/list", $this->global, $data, NULL);
        }
    }
    
    
}
