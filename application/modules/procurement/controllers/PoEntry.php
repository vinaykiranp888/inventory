<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PoEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('po_model');

        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('po_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['poList'] = $this->po_model->getPOListSearch($formData);

            $data['vendorList'] = $this->po_model->vendorListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            // print_r($subjectDetails);exit;
            $this->loadViews("po_entry/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('po_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {

                // echo "<Pre>";print_r($_POST);exit;

                $id_vendor = $this->security->xss_clean($this->input->post('id_vendor'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $total_tax_amount = $this->security->xss_clean($this->input->post('po_total_tax_amount'));
                $total_amount_before_tax = $this->security->xss_clean($this->input->post('po_total_amount_before_tax'));

                $generated_number = $this->po_model->generatePONumber();
                
                $total_amount = str_replace(",", "", $total_amount);
                $total_tax_amount = str_replace(",", "", $total_tax_amount);
                $total_amount_before_tax = str_replace(",", "", $total_amount_before_tax);

                $data = array(
                    'id_vendor' => $id_vendor,
                    'po_number' =>$generated_number,
                    'total_tax_amount' => $total_tax_amount,
                    'total_amount_before_tax' => $total_amount_before_tax,
                    'total_amount' => $total_amount,
                    'balance_amount' => $total_amount,
                    'description' => $description,
                    'created_by' => $id_user,
                    'status'=>3
                );

                // echo "<Pre>";print_r($data);exit;
               
                $inserted_id = $this->po_model->addPO($data);

                if($inserted_id)
                {
                    $this->po_model->moveTempToPoDetails($inserted_id);
                    // $update_data = array('is_po' => $inserted_id);
                    // $updated_pr = $this->po_model->updatePR($update_data,$id);
                }
                
                redirect('/procurement/poEntry/list');
            }
            else
            {
                $this->po_model->deleteTempPoDetailsBySessionId($id_session);
            }

            $data['procurementCategoryList'] = $this->po_model->procurementCategoryListByStatus('1');
            $data['vendorList'] = $this->po_model->vendorListByStatus('1');

            // echo "<Pre>";print_r($data['procurementCategoryList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("po_entry/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('po_entry.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/poEntry/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            

            $data['po'] = $this->po_model->getPo($id);
            $data['poDetails'] = $this->po_model->getPoDetails($id);
            $data['procurementCategoryList'] = $this->po_model->procurementCategoryListByStatus('1');
            $data['vendorList'] = $this->po_model->vendorListByStatus('1');

            // echo "<Pre>";print_r($data['po']);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("po_entry/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('po_entry.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/poEntry/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->po_model->editPOList($data,$id);

                redirect('/procurement/poEntry/approvallist');

                // $check_duplication = $this->budget_amount_model->editBudgetAmount($data,$id);

                // print_r($this->input->post());exit;
            }
            //  $data['fundCodeList'] = $this->po_model->getFundCodeList();
            // $data['departmentCodeList'] = $this->po_model->getDepartmentCodeList();
            // $data['accountCodeList'] = $this->po_model->getAccountCodeList();
            // $data['activityCodeList'] = $this->po_model->getActivityCodeList();
            // $data['prCategoryList'] = $this->po_model->prCategoryList();
            // $data['prSubCategoryList'] = $this->po_model->prSubCategoryList();
            // $data['prItemList'] = $this->po_model->prItemList();
            // $data['departmentList'] = $this->po_model->departmentList();
            // $data['vendorList'] = $this->po_model->vendorListByStatus('Approved');
            
            $data['poMaster'] = $this->po_model->getMasterPoDetails($id);
            $data['poDetails'] = $this->po_model->getPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("po_entry/view", $this->global, $data, NULL);
        }
    }

    function approvallist()
    {
        if ($this->checkAccess('po_entry.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $resultprint = $this->input->post();

           if($resultprint)
            {
                switch ($resultprint['button'])
                {
                    case 'approve':

                        for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                            $id = $resultprint['checkvalue'][$i];
                            $result = $this->po_model->editPOList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                    break;

                    case 'search':
                    break;
                     
                    default:
                    break;
                }
            }
            
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));


            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['poList'] = $this->po_model->getPOListSearch($formData);

            $data['financialYearList'] = $this->po_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->po_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->po_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->po_model->getDepartmentCodeList();
            

            // echo "<Pre>";print_r($formData);exit();
            
                // $array = $this->security->xss_clean($this->input->post('checkvalue'));
                // if (!empty($array))
                // {

                //     $result = $this->po_model->editPOList($array);
                //     redirect($_SERVER['HTTP_REFERER']);
                // }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("po_entry/approval_list", $this->global, $data, NULL);

        }
    }

    function getSubcategory($id)
    {
        $results = $this->po_model->procurementSubCategoryListByProcurementId($id);
                // echo "<Pre>";print_r($results);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_sub_category' id='id_sub_category' class='form-control' onchange='procurementItemListBySubCategory(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $sub_category_code = $results[$i]->code;
            $sub_categoryname = $results[$i]->name;

            $table.='<option value="'. $id.'">'.$sub_category_code . ' - '. $sub_categoryname .'</option>';

            }

            $table.="</select>";
            echo $table;
            exit;
    }

    function procurementItemListBySubCategory($id)
    {
        $results = $this->po_model->procurementItemListBySubCategory($id);
                // echo "<Pre>";print_r($results);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_item' id='id_item' class='form-control' onchange='getAvailableStocksByProductId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $sub_category_code = $results[$i]->code;
            $sub_categoryname = $results[$i]->name;

            $table.='<option value="'. $id.'">'.$sub_category_code . ' - '. $sub_categoryname .'</option>';

            }

            $table.="</select>";
            echo $table;
            exit;
    }

    function saveTempPODetails()
    {
        $id_session = $this->session->my_session_id;
        $data = $this->security->xss_clean($this->input->post('data'));
        
        $data['id_session'] = $id_session;
        $inserted_id = $this->po_model->saveTempPODetails($data);
        // }
        $data = $this->displayTempData();
        
        echo $data;        
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->po_model->getTempPODetailsByIdSessionForView($id_session); 
        // echo "<Pre>";print_r($details);exit;

        $detail_tax_amount = 0;
        $grand_total_tax_amount = 0;
        $grand_total_befor_gst = 0;
        $total_detail = 0;

        if(!empty($details))
        {
            
            $table = "
            <div class='custom-table'>
            <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>GST Tax</th>
                    <th>Tax Amount / Item</th>
                    <th>Total Tax Amount</th>
                    <th>Total Amount Before Tax</th>
                    <th>Total Price</th>
                    <th>Action</th>
                    </tr>
                </thead>";

                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $category_name = $details[$i]->category_name;
                        $category_code = $details[$i]->category_code;
                        $sub_category_name = $details[$i]->sub_category_name;
                        $sub_category_code = $details[$i]->sub_category_code;
                        $item_name = $details[$i]->item_name;
                        $item_code = $details[$i]->item_code;
                        $quantity = $details[$i]->quantity;
                        $gst_tax = $details[$i]->gst_tax;
                        $tax_amount = $details[$i]->tax_amount;
                        $total_tax_amount = $details[$i]->total_tax_amount;
                        $quantity = $details[$i]->quantity;
                        $price = $details[$i]->price;
                        $total_price_before_tax = $details[$i]->total_price_before_tax;
                        $total_price = $details[$i]->total_price;

                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$category_code - $category_name</td>
                            <td>$sub_category_code - $sub_category_name</td>
                            <td>$item_code - $item_name</td>
                            <td>$quantity</td>
                            <td>$price</td>
                            <td>$gst_tax</td>
                            <td style='text-align: right' >$tax_amount</td>
                            <td style='text-align: right' >$total_tax_amount</td>
                            <td style='text-align: right' >$total_price_before_tax</td>
                            <td style='text-align: right' >$total_price</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempPoDetails($id)'>Delete</a>
                            <td>
                        </tr>";

                        $detail_tax_amount = $detail_tax_amount + $tax_amount;
                        $grand_total_tax_amount = $grand_total_tax_amount + $total_tax_amount;
                        $grand_total_befor_gst = $grand_total_befor_gst + $total_price_before_tax;
                        $total_detail = $total_detail + $total_price;
                    }

                    $detail_tax_amount = number_format($detail_tax_amount, 2, '.', ',');
                    $grand_total_tax_amount = number_format($grand_total_tax_amount, 2, '.', ',');
                    $grand_total_befor_gst = number_format($grand_total_befor_gst, 2, '.', ',');
                    $total_detail = number_format($total_detail, 2, '.', ',');

                    $table .= "
                    <tr>
                            <td colspan='6'></td>
                            <td><b> Total: </b></td>
                            <td style='text-align: right'><input type='hidden' name='detail_tax_amount' id='detail_tax_amount' value='$detail_tax_amount' /><b>$detail_tax_amount</b></td>
                            <td style='text-align: right'><input type='hidden' name='grand_total_tax_amount' id='grand_total_tax_amount' value='$grand_total_tax_amount' /><b>$grand_total_tax_amount</b></td>
                            <td style='text-align: right'><input type='hidden' name='grand_total_befor_gst' id='grand_total_befor_gst' value='$grand_total_befor_gst' /><b>$grand_total_befor_gst</b></td>
                            <td style='text-align: right'><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td></td>
                        </tr>
                    </tbody>";
            $table.= "</table>
            </div>";
        }
        else
        {
            $total_detail = number_format($total_detail, 2, '.', ',');
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function deleteTempPoDetails($id)
    {
        $inserted_id = $this->po_model->deleteTempPoDetails($id);
        $data = $this->displayTempData();
        echo $data; 
    }

    function getAvailableStocksByProductId($id)
    {
        $item = $this->po_model->getProductByProductId($id);
        
        $product_quantity = 0;
        $product_gst_tax = 0;

        if($item)
        {
            $product_quantity = $item->quantity;
            $product_gst_tax = $item->gst_tax;

            $table = "
            <input type='hidden' name='product_quantity' id='product_quantity' value='$product_quantity' />
            <input type='hidden' name='product_gst_tax' id='product_gst_tax' value='$product_gst_tax' />
            ";
            echo $table;exit;
        }
        else
        {
            $table = "
            <input type='hidden' name='product_quantity' id='product_quantity' value='$product_quantity' />
            <input type='hidden' name='product_gst_tax' id='product_gst_tax' value='$product_gst_tax' />
            ";
            echo $table;exit;
        }
    }

    function getItemDuplicationInTempDetails($id_item)
    {
        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;
        $data['id_item'] = $id_item;
        $data['id_session'] = $id_session;

        $result = $this->po_model->getItemDuplicationInTempDetails($data);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function calculateTotalPrice()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $quantity = $data['quantity'];
        $price = $data['price'];
        $gst_tax = $data['gst_tax'];

        $one_percent = $price * 0.01;
        $product_tax_amount = $gst_tax * $one_percent;
        $product_total_tax_amount = $product_tax_amount * $quantity;
        $product_total_price_before_tax = $quantity * $price;
        $product_total_price = $product_total_tax_amount + $product_total_price_before_tax;


        $table = "
            <input type='hidden' name='product_total_price' id='product_total_price' value='$product_total_price' />
            <input type='hidden' name='product_tax_amount' id='product_tax_amount' value='$product_tax_amount' />
            <input type='hidden' name='product_total_tax_amount' id='product_total_tax_amount' value='$product_total_tax_amount' />
            <input type='hidden' name='product_total_price_before_tax' id='product_total_price_before_tax' value='$product_total_price_before_tax' />
            ";
        echo $table;exit;
    }


    function generatePO($id_po)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();
        $mpdf=new \Mpdf\Mpdf(); 


        $currentDate = date('d-m-Y');
        $currentTime = date('h:i:s a');
        $currentDateTime = date('d_m_Y_His');

        $organisationDetails = $this->po_model->getOrganisation();


        // echo "<Pre>";print_r($organisationDetails);exit;

        

        // $signature = $_SERVER['DOCUMENT_ROOT']."/assets/img/speed_logo.svg";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }

        $po = $this->po_model->getPo($id_po);

        // echo "<Pre>";print_r($po);exit;


        $organisation_name = $organisationDetails->name;
        $organisation_short_name = $organisationDetails->short_name;
        $organisation_local_name = $organisationDetails->name_in_malay;
        $organisation_url = $organisationDetails->url;
        $organisation_state_name = $organisationDetails->state_name;
        $organisation_email = $organisationDetails->email;
        $organisation_address1 = $organisationDetails->address1;
        $organisation_address2 = $organisationDetails->address2;
        $organisation_city = $organisationDetails->city;
        $organisation_zipcode = $organisationDetails->zipcode;
        $organisation_gst_in = $organisationDetails->gst_in;
        $organisation_cin = $organisationDetails->cin;
        $organisation_contact_number = $organisationDetails->contact_number;


        $po_number = $po->po_number;
        $description = $po->description;
        $paid_amount = $po->paid_amount;
        $status = $po->status;
        $total_amount_before_tax = $po->total_amount_before_tax;
        $total_tax_amount = $po->total_tax_amount;
        $total_amount = $po->total_amount;
        $balance_amount = $po->balance_amount;
        $date_time = $po->created_dt_tm;


        $vendor_code = $po->vendor_code;
        $vendor_name = $po->vendor_name;
        $vendor_address_one = $po->address_one;
        $vendor_address_two = $po->address_two;
        $vendor_state = $po->vendor_state;
        $vendor_city = $po->city;
        $vendor_zipcode = $po->vendor_zipcode;
        $vendor_gst_in = $po->gst_in;
        $vendor_phone = $po->phone;
        $vendor_email = $po->email;

        if($date_time)
        {
            $date_time = date('d-m-Y H:i:s', strtotime($date_time));
        }



        $file_data = "
        <table align='center' width='100%'>
            <tr>
            <td style='text-align: left' width='20%'><img src='$signature' width='80px' /></td>
            <td style='text-align: left' width='80%'></td>
            </tr>
        </table>";


        $file_data.="
        <table align='center' width='100%'>
            <tr>
                <td style='text-align: left' width='60%' ><b>$organisation_name</b></td>
                <td style='text-align: left' width='40%' ><b>PURCHASE ORDER</b></td>
            </tr>
            <td style='text-align: left' width='60%' >$organisation_address1
                <br>$organisation_address2
                <br>$organisation_city, $organisation_state_name, $organisation_zipcode
                <br>PH : $organisation_contact_number, Email : $organisation_email
            </td>
            <td valign='top' style='text-align: left' width='40%' >$organisation_gst_in</td>
            </tr>
            <tr>
                <td style='text-align: left' width='60%' ></td>
                <td style='text-align: left' width='40%' >Date : $date_time</td>
            </tr>
            <tr>
                <td style='text-align: left' width='60%' ></td>
                <td style='text-align: left' width='40%' >PO NO: $po_number</td>
            </tr>
        </table>";


        $file_data.="
        <table align='center' width='100%'>
            <tr>
                <td style='text-align: left' width='50%' >
                    <table align='center' width='100%' style='border:1px solid black;'>
                    <tr>
                        <td><b>To :</b></td>
                    </tr>
                    <tr>
                        <td style='text-align: left' width='100%' >$vendor_name ( $vendor_code )
                            <br>$vendor_address_one
                            <br>$vendor_address_two
                            <br>$vendor_city, $vendor_state, $vendor_gst_in
                            <br>PH : $vendor_phone, Email : $vendor_email
                        </td>
                    </tr>
                    </table>

                </td>
                <td style='text-align: left' width='50%'>
                    <table align='center' width='100%' style='border:1px solid black;'>
                    <tr>
                        <td><b>Ship To : </b></td>
                    </tr>
                    <tr>
                        <td style='text-align: left' width='50%' >$organisation_name
                            <br>$organisation_address1
                            <br>$organisation_address2
                            <br>$organisation_city, $organisation_state_name, $organisation_zipcode
                            <br>PH : $organisation_contact_number, Email : $organisation_email
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
            </table>";


        $file_data.="
        <br>
        <table align='center' style='border-collapse: collapse;' width='100%' border='1px'>
            <tr>
                <th style='text-align: left' width='5%' >Sl. No</th>
                <th style='text-align: left' width='50%' >Description Of Goods</th>
                <th style='text-align: center' width='15%' >Quantity</th>
                <th style='text-align: center' width='15%' >Unit Price</th>
                <th style='text-align: center' width='15%' >Amount</th>
            </tr>
        ";

        $poDetailsList = $this->po_model->getPoDetails($id_po);

        // echo "<Pre>";print_r($poDetailsList);exit;

        $i=1;
        foreach ($poDetailsList as $value)
        {

            $quantity = $value->quantity;
            $balance_quantity = $value->balance_quantity;
            $received_quantity = $value->received_quantity;
            $price = $value->price;
            $gst_tax = $value->gst_tax;
            $tax_amount = $value->tax_amount;
            $total_tax_amount = $value->total_tax_amount;
            $total_price_before_tax = $value->total_price_before_tax;
            $price = $value->price;
            $total_price = $value->total_price;
            $category_name = $value->category_name;
            $category_code = $value->category_code;
            $sub_category_name = $value->sub_category_name;
            $sub_category_code = $value->sub_category_code;
            $item_name = $value->item_name;
            $item_code = $value->item_code;
            $package_name = $value->package_name;
            $package_code = $value->package_code;
            $part_number = $value->part_number;

            // $total_price = number_format($total_price, 2, '.', ',');
            // $price = number_format($price, 2, '.', ',');

    
            $file_data = $file_data ."
               <tr>
                   <td style='text-align: left' width='5%'>$i </td>
                   <td style='text-align: left' width='50%'>$item_name, $part_number, $package_name, ($gst_tax %)</td>
                   <td style='text-align: center' width='15%'>$quantity</td>
                   <td style='text-align: center' width='15%'>$price</td>
                   <td style='text-align: right' width='15%'>$total_price</td>
               </tr>";

            $i++;
        }


            $file_data = $file_data ."
           <tr>
               <td style='text-align: right' width='85%' colspan='4'> Total Amount : </td>
               <td style='text-align: right' width='15%' >$total_amount_before_tax</td>
           </tr>
           <tr>
               <td style='text-align: right' width='85%' colspan='4'> GST Tax Amount: </td>
               <td style='text-align: right' width='15%' >$total_tax_amount</td>
           </tr>
           <tr>
               <td style='text-align: right' width='85%' colspan='4'> Grand Total : </td>
               <td style='text-align: right' width='15%' >$total_amount</td>
           </tr>";

        $amount_c = $total_amount;
        $amount_word = $this->getAmountWordings($amount_c);
        $amount_word = ucwords($amount_word);


        $file_data = $file_data ."

            <tr>
               <td colspan='5%' width='100%' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>INR : $amount_word</b></td>
            </tr>
        </table>";



        $file_data = $file_data ."
        <br/><br/>
        <h4><ul>Terms and Conditions: </ul></h4>

        <table align='center' width='100%'>
            <tr>
                <td style='text-align: left' width='30%' valign='top'>1. Taxes</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>as per GST rates.</td>
            </tr>

            <tr>
                <td style='text-align: left' width='30%' valign='top'>2. Payment</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'><b>100% Before Dispatch.</b></td>
            </tr>

            <tr>
                <td style='text-align: left' width='30%' valign='top'>3. Delivery Schedule</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'><b>within 14 weeks from the po date.</b></td>
            </tr>

            <tr>
                <td style='text-align: left' width='30%' valign='top'>4. Other Terms and Conditions</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>Terms and Conditions of Purchase (On Next Page)</td>
            </tr>

            <tr>
                <td style='text-align: left' width='30%' valign='top'>5. Send all Correspondence to</td>
                <td style='text-align: center' width='5%' valign='top'>:</td>
                <td style='text-align: left' width='65%'>$organisation_email
                    <br>$organisation_email
                    <br>$organisation_email
                </td>
            </tr>
        </table>";
        print_r($file_data);exit;

        $mpdf->WriteHTML($file_data);
        $mpdf->Output($type . '_PO_'.$po_number.'_'.$currentDateTime.'.pdf', 'D');
        exit;
    }
}