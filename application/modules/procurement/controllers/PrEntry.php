<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PrEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pr_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('pr_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('1');

            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['type_of_pr'] = $this->security->xss_clean($this->input->post('type_of_pr'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['prEnteryList'] = $this->Pr_model->getPRListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : List Purchase Requisition';
            // echo "<Pre>";print_r($data['prEnteryList']);exit;
            $this->loadViews("pr_entry/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('pr_entry.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                
                $journal_number = $this->Pr_model->generatePRNumber();

                // $journal_number = $this->security->xss_clean($this->input->post('journal_number'));
                $type_of_pr = $this->security->xss_clean($this->input->post('type_of_pr'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_budget_year = $this->security->xss_clean($this->input->post('id_budget_year'));
                $id_financial_year = $this->security->xss_clean($this->input->post('id_financial_year'));
                $department_code = $this->security->xss_clean($this->input->post('department_code'));
                $pr_entry_date = $this->security->xss_clean($this->input->post('pr_entry_date'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_department = $this->security->xss_clean($this->input->post('id_department'));
                $id_vendor = $this->security->xss_clean($this->input->post('id_vendor'));
                $amount = $this->security->xss_clean($this->input->post('amount'));

                     // echo "<Pre>";print_r($id_department);exit;
                $data = array(
                    'pr_number' =>$journal_number,
                    'type_of_pr' => $type_of_pr,
                    'type' => $type,
                    'pr_entry_date' => date("Y-m-d", strtotime($pr_entry_date)),
                    'id_budget_year' => $id_budget_year,
                    'id_financial_year' => $id_financial_year,
                    'department_code' => $department_code,
                    // 'id_department' => $id_department,
                    'id_vendor' => $id_vendor,
                    'amount' => $amount,
                    'description' => $description,
                    'created_by' => $user_id,
                    'status'=>'0'
                );
                     // echo "<Pre>";print_r($data);exit;
             $inserted_id = $this->Pr_model->addPR($data);


                $temp_details = $this->Pr_model->getTempDetailsFromPR($id_session);
                     // echo "<Pre>";print_r($temp_details);exit;

                 for($i=0;$i<count($temp_details);$i++)
                 {

                        $dt_fund = $temp_details[$i]->dt_fund;
                        $dt_department = $temp_details[$i]->dt_department;
                        $dt_activity = $temp_details[$i]->dt_activity;
                        $dt_account = $temp_details[$i]->dt_account;
                        $cr_fund = $temp_details[$i]->cr_fund;
                        $cr_department = $temp_details[$i]->cr_department;
                        $cr_activity = $temp_details[$i]->cr_activity;
                        $cr_account = $temp_details[$i]->cr_account;
                        $type = $temp_details[$i]->type;
                        $id_category = $temp_details[$i]->id_category;
                        $id_sub_category = $temp_details[$i]->id_sub_category;
                        $id_item = $temp_details[$i]->id_item;
                        $quantity = $temp_details[$i]->quantity;
                        $price = $temp_details[$i]->price;
                        $id_tax = $temp_details[$i]->id_tax;
                        $tax_price = $temp_details[$i]->tax_price;
                        $total_final = $temp_details[$i]->total_final;
                        $id_budget_allocation = $temp_details[$i]->id_budget_allocation;
                    

                     $detailsData = array(
                        'id_pr_entry'=>$inserted_id,
                        'dt_fund'=>$dt_fund,
                        'dt_department'=>$dt_department,
                        'dt_activity'=>$dt_activity,
                        'dt_account'=>$dt_account,
                        'cr_fund'=>$cr_fund,
                        'cr_department'=>$cr_department,
                        'cr_activity'=>$cr_activity,
                        'cr_account'=>$cr_account,
                        'type'=>$type,
                        'id_category'=>$id_category,
                        'id_sub_category'=>$id_sub_category,
                        'id_item'=>$id_item,
                        'id_budget_allocation'=>$id_budget_allocation,
                        'quantity'=>$quantity,
                        'price'=>$price,
                        'id_tax'=>$id_tax,
                        'tax_price'=>$tax_price,
                        'total_final'=>$total_final

                    );
                     
                    $added_detail = $this->Pr_model->addNewPRDetails($detailsData);
                    if($added_detail)
                    {
                        // echo "<Pre>";print_r($added_detail);exit();
                        $added_detail = $this->Pr_model->updateBudgetAllocationAmount($detailsData);
                    }
                }

                $this->Pr_model->deleteFullSessionData($id_session);
                redirect('/procurement/prEntry/list');


            }
            else
            {
        // print_r($id_session);exit();
                $this->Pr_model->deleteFullSessionData($id_session);
            }

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Pr_model->budgetYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['fundCodeList'] = $this->Pr_model->getFundCodeList();
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->Pr_model->getAccountCodeList();
            $data['activityCodeList'] = $this->Pr_model->getActivityCodeList();
            $data['prCategoryList'] = $this->Pr_model->prCategoryList();
            $data['prSubCategoryList'] = $this->Pr_model->prSubCategoryList();
            $data['prItemList'] = $this->Pr_model->prItemList();
            $data['taxList'] = $this->Pr_model->getTaxList();
            // echo "<Pre>";print_r($data['taxList']);exit;



            $this->global['pageTitle'] = 'Inventory Management : Add Procurement Category';
            $this->loadViews("pr_entry/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        $id_session = $this->session->my_session_id;
        // print_r($id_session);exit();
        if ($this->checkAccess('pr_entry.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/pr_entry/list');
            }
            if($this->input->post())
            {
                // $code = $this->security->xss_clean($this->input->post('code'));
                // $description = $this->security->xss_clean($this->input->post('description'));
                // $status = $this->security->xss_clean($this->input->post('status'));           
                // $data = array(
                //     'code' => $code,
                //     'description' => $description,
                //     'status' => $status
                // );                
                // $result = $this->Pr_model->editProcurementCategory($data,$id);
                // redirect('/procurement/procurementCategory/list');
            }
            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Pr_model->budgetYearListByStatus('1');
            $data['fundCodeList'] = $this->Pr_model->getFundCodeList();
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->Pr_model->getAccountCodeList();
            $data['activityCodeList'] = $this->Pr_model->getActivityCodeList();
            $data['prCategoryList'] = $this->Pr_model->prCategoryList();
            $data['prSubCategoryList'] = $this->Pr_model->prSubCategoryList();
            $data['prItemList'] = $this->Pr_model->prItemList();
            $data['departmentList'] = $this->Pr_model->departmentList();
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            
            $data['prMaster'] = $this->Pr_model->getMasterPrDetails($id);
            $data['prDetails'] = $this->Pr_model->getPrDetails($id);
            
            // echo "<Pre>";print_r($data['prMaster']);exit();
            // echo "<Pre>";print_r($data['prDetails']);exit();
            // $this->loadViews("pr_entry/edit", $this->global, $data, NULL);
            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("pr_entry/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('pr_entry.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/pr_entry/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                
                 $result = $this->Pr_model->editPRList($data,$id);
                 if($status == '2')
                 {
                    $detailsDatas = $this->Pr_model->getPrDetails($id);
                    foreach ($detailsDatas as $detailsData)
                    {
            // echo "<Pre>";print_r($detailsData);exit();
                        $details_data['id_budget_allocation'] = $detailsData->id_budget_allocation;
                        $details_data['total_final'] = $detailsData->total_final;
                        $updated_budget_amount = $this->Pr_model->updateBudgetAllocationAmountOnReject($details_data);

                    }
                 }
                redirect('/procurement/PrEntry/approvalList');

            }
            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Pr_model->budgetYearListByStatus('1');
            $data['fundCodeList'] = $this->Pr_model->getFundCodeList();
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();
            $data['accountCodeList'] = $this->Pr_model->getAccountCodeList();
            $data['activityCodeList'] = $this->Pr_model->getActivityCodeList();
            $data['prCategoryList'] = $this->Pr_model->prCategoryList();
            $data['prSubCategoryList'] = $this->Pr_model->prSubCategoryList();
            $data['prItemList'] = $this->Pr_model->prItemList();
            $data['departmentList'] = $this->Pr_model->departmentList();
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            
            $data['prMaster'] = $this->Pr_model->getMasterPrDetails($id);
            $data['prDetails'] = $this->Pr_model->getPrDetails($id);
            
            // echo "<Pre>";print_r($data['prMaster']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            // $this->loadViews("pr_entry/edit", $this->global, $data, NULL);
            $this->loadViews("pr_entry/view", $this->global, $data, NULL);
        }
    }


    function approvalList()
    {
        if ($this->checkAccess('pr_entry_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         
                         $result = $this->Pr_model->editPRList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':


                     
                     break;
                 
                default:
                     break;
             }
                
            }



            
            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Pr_model->budgetYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();

            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['type_of_pr'] = $this->security->xss_clean($this->input->post('type_of_pr'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['prList'] = $this->Pr_model->getPRListSearch($formData);
            // echo "<Pre>";print_r($data['prList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Approve PR Entry';
            $this->loadViews("pr_entry/approval_list", $this->global, $data, NULL);
        }
    }

    function tempedit($id){
        $data = $this->Pr_model->getTempDetails($id);
        echo json_encode($data);

    }

    function tempDelete($id,$type)
    {
        // echo "<Pre>";print_r($type);exit;
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->Pr_model->deleteTempData($id);
        $data = $this->displaytempdata($type);
        echo $data; 
    } 

    function displaytempdata($type)
    {
        $id_session = $this->session->my_session_id;
        
        $details = $this->Pr_model->getTempPRDetails($id_session,$type); 
        if(!empty($details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Debit GL Code</th>
                    <th>Credit GL Code</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>Item</th>
                    <th>Tax</th>
                    <th>Quantity</th>
                    <th>Tax Price</th>
                    <th>Amount</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $type = $details[$i]->type;
                        $dt_account = $details[$i]->dt_account;
                        $dt_activity = $details[$i]->dt_activity;
                        $dt_department = $details[$i]->dt_department;
                        $dt_fund = $details[$i]->dt_fund;
                        $cr_account = $details[$i]->cr_account;
                        $cr_activity = $details[$i]->cr_activity;
                        $cr_department = $details[$i]->cr_department;
                        $cr_fund = $details[$i]->cr_fund;
                        $category_name = $details[$i]->category_name;
                        $sub_category_name = $details[$i]->sub_category_name;
                        $item_name = $details[$i]->item_name;
                        $tax_code = $details[$i]->tax_code;
                        $tax_name = $details[$i]->tax_name;
                        $quantity = $details[$i]->quantity;
                        $price = $details[$i]->price;
                        $tax_price = $details[$i]->tax_price;
                        $total_final = $details[$i]->total_final;
                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$dt_fund - $dt_department - $dt_activity - $dt_account</td>
                            <td>$cr_fund - $cr_department - $cr_activity - $cr_account</td>
                            <td>$category_name</td>
                            <td>$sub_category_name</td>
                            <td>$item_name</td>
                            <td>$tax_code - $tax_name</td>
                            <td>$quantity</td>
                            <td>$tax_price</td>
                            <td>$price</td>
                            <td>$total_final</td>
                            
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                        $total_detail = $total_detail + $total_final;
                    }

                     $table .= "

                    <tr>
                            <td bgcolor='' colspan='9'></td>
                            <td bgcolor=''><b> Total : </b></td>
                            <td bgcolor=''><input type='hidden' name='total_detail' id='total_detail' value='$total_detail' /><b>$total_detail</b></td>
                            <td bgcolor=''></td>
                        </tr>
                    </tbody>";

                    // <td>
                    //             <span onclick='getTempData($id)'>Edit</a>
                    //         <td>
        $table.= "</table>
        </div>";
        }
        else
        {
            $total_detail='';
            $table = "
            <input type='hidden' name='total_detail' id='total_detail' value='$total_detail' />
            ";
        }
        return $table;
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['sessionid'] = $id_session;
        // if($tempData['id'] && $tempData['id']>0)
        // {
        //     $id =  $tempData['id'];
        //     unset($tempData['id']);
        //     $inserted_id = $this->Pr_model->updateTempDetails($tempData,$id);
        // }
        // else
        // {
        // echo "<Pre>";print_r($tempData);exit();
            // unset($tempData['id']);
            $inserted_id = $this->Pr_model->addTempDetails($tempData);
        // }
        $data = $this->displaytempdata($tempData['type']);
        
        echo $data;        
    }

    function getCategory($type)
    {
        // echo "<Pre>";print_r($type);exit();
        if($type == 'Asset')
        {
            $results = $this->Pr_model->assetCategoryByStatus('1');

        }elseif ($type == 'Procurement')
        {
            $results = $this->Pr_model->procurementCategoryByStatus('1');            
        }


            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_category' id='id_category' style='width: 196px;' class='form-control' onchange='getSubCategoryByCategoryId()'>";

            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $code = $results[$i]->code;
            $description = $results[$i]->description;
            $table.="<option value=".$id.">".$code. " - " . $description . "</option>";

            }
            $table.="</select>";

            echo $table;

    }

    function getSubCategoryByCategoryId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_category = $tempData['id_category'];
        $type = $tempData['type'];
        // echo "<Pre>";print_r($tempData);exit();
        if($type == 'Asset')
        {
            $results = $this->Pr_model->assetSubCategoryByCategory($id_category);

        }elseif ($type == 'Procurement')
        {
            $results = $this->Pr_model->procurementSubCategoryByCategory($id_category);            
        }


            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_sub_category' id='id_sub_category' style='width: 196px;' class='form-control' onchange='getItemBySubCategoryId()'>";

            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $code = $results[$i]->code;
            $description = $results[$i]->description;
            $table.="<option value=".$id.">".$code. " - " . $description . "</option>";

            }
            $table.="</select>";

            echo $table;

    }

    function getItemBySubCategoryId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_sub_category = $tempData['id_sub_category'];
        $id_category = $tempData['id_category'];
        $type = $tempData['type'];
        // echo "<Pre>";print_r($tempData);exit();
        if($type == 'Asset')
        {
            $results = $this->Pr_model->assetItemBySubCategory($id_category,$id_sub_category);

        }elseif ($type == 'Procurement')
        {
            $results = $this->Pr_model->procurementItemBySubCategory($id_category,$id_sub_category);            
        }


            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_item' id='id_item' class='form-control' style='width: 196px;'>";

            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $code = $results[$i]->code;
            $description = $results[$i]->description;
            $table.="<option value=".$id.">".$code. " - " . $description . "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getTaxCalculated()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_tax = $tempData['id_tax'];
        $quantity = $tempData['quantity'];
        $price = $tempData['price'];
        if ($tempData['id_tax'] != '' && $tempData['quantity'] != '' && $tempData['price'] != '')
        {
            $tax_row = $this->Pr_model->getTaxRow($id_tax);

            $tax_percentage = $tax_row->percentage;
            $one_percent = $price *0.01;
            $tax_amount = $one_percent * $tax_percentage;
            $total_amount = $price + $tax_amount;
            $total_amount = $total_amount * $quantity;
            $tax_amount = $tax_amount * $quantity;
            // echo "<Pre>";print_r($total_amount);exit();

            $table = "

            <input type='hidden' name='tax_amount' id='tax_amount' value='$tax_amount' />$tax_amount
            <input type='hidden' name='tax_per' id='tax_per' value='$tax_percentage' />$tax_percentage
            <input type='hidden' name='total_amount' id='total_amount' value='$total_amount' />$total_amount
            ";

            echo $table;

        }
    }

























    function getDebitBudget()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->Pr_model->getDebitBudget($data);
        // echo "<Pre>";print_r($budget_data);exit;

        if($budget_data == '')
        {
            echo "";exit;
        }

        $id_budget_amount = $budget_data->id;
        
        // echo "<Pre>";print_r($id_budget_amount);exit;

        $results = $this->Pr_model->getBudgetAllocationByBudgetAmount($id_budget_amount);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
        ";

        $table.="
        <select name='dt_fund' id='dt_fund' class='form-control' style='width: 196px' onchange='getActivityDebitCodes()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $fund_code = $results[$i]->fund_code;
        $fund_data = $this->Pr_model->getFundNameByCode($fund_code);
        // echo "<Pre>"; print_r($fund_data);exit;
        $fund_name = $fund_data->fund_name;

        $table.="<option value=".$fund_code.">".$fund_code . " - " . $fund_name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }


    function getActivityDebitCodes()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->Pr_model->getDebitBudget($data);

        if($budget_data == '')
        {
            echo "";exit;
        }

        $id_budget_amount = $budget_data->id;
        $data['id_budget_amount'] = $id_budget_amount;
        
        // echo "<Pre>";print_r($data);exit;

        $results = $this->Pr_model->getBudgetAllocationByBudgetAmounNFund($data);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
        ";

        $table.="
        <select name='dt_activity' id='dt_activity' class='form-control' style='width: 196px' onchange='getAccountDebitCodes()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $activity_code = $results[$i]->activity_code;
        $data = $this->Pr_model->getActivityNameByCode($activity_code);
        // echo "<Pre>"; print_r($data);exit;
        $activity_name = $data->activity_name;

        $table.="<option value=".$activity_code.">".$activity_code . " - " . $activity_name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }

    function getAccountDebitCodes()
    {
        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->Pr_model->getDebitBudget($data);

        if($budget_data == '')
        {
            echo "";exit;
        }

        $id_budget_amount = $budget_data->id;
        $data['id_budget_amount'] = $id_budget_amount;
        
        // echo "<Pre>";print_r($data);exit;

        $results = $this->Pr_model->getBudgetAllocationByBudgetAmounNFundNActivity($data);

        // echo "<Pre>"; print_r($results);exit;
        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>
        ";

        $table.="
        <select name='dt_account' id='dt_account' class='form-control' style='width: 196px' onchange='getBudgetAllocationByCodes()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $account_code = $results[$i]->account_code;
        $data = $this->Pr_model->getAccountNameByCode($account_code);
        $account_name = $data->account_name;

        $table.="<option value=".$account_code.">".$account_code . " - " . $account_name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;

    }

    function getBudgetAllocationByCodes()
    {

        $data = $this->security->xss_clean($this->input->post('data'));

        $budget_data = $this->Pr_model->getDebitBudget($data);

        if($budget_data == '')
        {
            $balance_amount = 0;            

            $table = "
                          
          <input type='number' class='form-control' name='balance_amount' id='balance_amount' value='$balance_amount' readonly>
                         ";

        echo $table;exit();


        }


        if($budget_data != '')
        {
            $id_budget_amount = $budget_data->id;
            $amount = $budget_data->amount;

            $data['dt_id_budget_amount'] = $id_budget_amount;

             $budget_allocation = $this->Pr_model->getBudgetAllocationByCodes($data);

             // echo "<Pre>"; print_r($budget_allocation);exit;

             $id = $budget_allocation->id;
             $allocated_amount = $budget_allocation->allocated_amount;
             $balance_amount = $budget_allocation->balance_amount;
             $used_amount = $budget_allocation->used_amount;
             $increment_amount = $budget_allocation->increment_amount;
             $decrement_amount = $budget_allocation->decrement_amount;
        }
        else
        {
            $balance_amount = 0;
        }

         $table = "
                          
          <input type='number' class='form-control' name='balance_amount' id='balance_amount' value='$balance_amount' readonly>


          <input type='hidden' class='form-control' name='id_budget_allocation' id='id_budget_allocation' value='$id' readonly>
                         ";

       echo $table;exit();

    }

    function checkBudgetAmount()
    {
        $check_budget_amount = $this->Pr_model->checkBudgetAmountExceeds();

        $table = "    
          <input type='number' class='form-control' name='check_budget' id='check_budget' value='$check_budget_amount' readonly>
                         ";
        echo $table;exit();

    }
}
