<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PoApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Po_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('pr_entry.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['poList'] = $this->Po_model->getPOListSearch($formData);

            // echo "<Pre>";print_r($formData);exit();
            
                $array = $this->security->xss_clean($this->input->post('checkvalue'));
                if (!empty($array))
                {

                    $result = $this->Po_model->editPOList($array);
                    redirect($_SERVER['HTTP_REFERER']);
                }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("po_approval/list", $this->global, $data, NULL);

        }
    }
    
    
}
