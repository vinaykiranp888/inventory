<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TenderShortlist extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tender_submission_model');
        $this->load->model('tender_quotation_model');
        $this->load->model('Pr_model');

        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('tender_shortlist.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['id_budget_year'] = $this->security->xss_clean($this->input->post('id_budget_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';

            $formData['submitted'] = '1';
            $formData['shortlisted'] = '1';
            $formData['awarded'] = '0';
 
            $data['searchParam'] = $formData;

            $data['tenderQuotationList'] = $this->tender_submission_model->getTenderSubmissionListSearch($formData);

            $data['financialYearList'] = $this->Pr_model->financialYearListByStatus('1');
            $data['budgetYearList'] = $this->Pr_model->budgetYearListByStatus('1');
            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->Pr_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->Pr_model->getDepartmentCodeList();


            $this->global['pageTitle'] = 'Inventory Management : List Tender Shortlist';
            //print_r($subjectDetails);exit;
            $this->loadViews("tender_shortlist/list", $this->global, $data, NULL);
        }
    }

     function add()
    {

        if ($this->checkAccess('tender_shortlist.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

            	$id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

            	$array = $this->security->xss_clean($this->input->post('checkvalue'));
            	$id_tender_quotation = $this->security->xss_clean($this->input->post('id_tender_quotation'));

                if (!empty($array))
                {

                    for ($i=0;$i<count($array);$i++)
                    {
         // echo "<Pre>"; print_r($array[$i]);exit;
                        $data = array(
                            'is_shortlisted' => 1,
                            'is_awarded' => 0

                        );
                    	$updated = $this->tender_submission_model->updateTenderSubmissionVendor($data,$array[$i]);
                    	if($i==0)
                    	{
		                    if($updated)
			                {
			                    $update_data = array('is_shortlisted' => 1);
			                    $updated_quotation = $this->tender_submission_model->updateTenderQuotation($update_data,$id_tender_quotation);
			                }
		            	}
	            	}
                }
       	    	redirect('/procurement/tenderShortlist/list');
            }

            $data['vendorList'] = $this->Pr_model->vendorListByStatus('Approved');
            $data['tenderQuotationList'] = $this->tender_submission_model->tenderQuotationListForShortlist();
            $this->global['pageTitle'] = 'Inventory Management : Add Tender Shortlist';
            // echo "<Pre>"; print_r($data['tenderQuotationList']);exit;
            $this->loadViews("tender_shortlist/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('tender_shortlist.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/procurement/tenderQuotation/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                $generated_number = $this->tender_submission_model->generateQuotationNumber();

                

                $description = $this->security->xss_clean($this->input->post('description'));
                $id_tender_quotation = $this->security->xss_clean($this->input->post('id_tender_quotation'));
                $id_vendor = $this->security->xss_clean($this->input->post('id_vendor'));

                $check_duplication =  $this->tender_submission_model->checkDuplicateSubmission($id_vendor,$id_tender_quotation);

                if($check_duplication)
                {
                    echo "<Pre>";print_r('Duplicate Entry Not Allowed');exit();
                }


                $data = array(
                    'description' => $description,
                    'id_tender_quotation' =>$id_tender_quotation,
                    'id_vendor' => $id_vendor,
                    'is_submitted' => 1,
                    'is_shortlisted' => 0,
                    'is_awarded' => 0,
                    'created_by' => $user_id,
                    'status'=>'0'
                );

                $inserted_id = $this->tender_submission_model->addTenderSubmissionVendor($data);
                redirect('/procurement/tenderSubmission/list');
            }
            $data['tenderQuotationMaster'] = $this->tender_quotation_model->getTenderQuotaion($id);
            $data['tenderQuotationDetails'] = $this->tender_quotation_model->getTenderQuotationDetailsByMasterId($id);
            $data['tenderComiteeDetails'] = $this->tender_quotation_model->getTenderComiteeDetailsByMasterId($id);
            $data['tenderRemarksDetails'] = $this->tender_quotation_model->getTenderRemarksDetailsByMasterId($id);
            $data['tenderSubmissionForTendor'] = $this->tender_submission_model->getTenderSubmissionForTendorShortlisted($id);
            // echo "<Pre>";print_r($data);exit();
            $this->global['pageTitle'] = 'Inventory Management : View Tender Shortlist';
            $this->loadViews("tender_shortlist/edit", $this->global, $data, NULL);
        }
    }

    

    function getData($id) {
        // echo "<Pre>";print_r($id);exit;
        $tenderQuotation = $this->tender_quotation_model->getTenderQuotaion($id);
        $tenderQuotationDetails = $this->tender_quotation_model->getTenderQuotationDetailsByMasterId($id);
        $tenderComiteeDetails = $this->tender_quotation_model->getTenderComiteeDetailsByMasterId($id);
        $tenderRemarksDetails = $this->tender_quotation_model->getTenderRemarksDetailsByMasterId($id);
        $tenderSubmissionForTendor = $this->tender_submission_model->getTenderSubmissionForTendor($id);

// echo "<Pre>";print_r($tenderQuotation);exit;

       $vendorList = $this->Pr_model->vendorListByStatus('Approved');
        $array = $this->Pr_model->financialYearListByStatus('1');
        $financialYearList = json_decode( json_encode($array), true);

        // $prDetails = $data['prDetails'];

        // $vendorList = json_decode( json_encode($vendorList), true);
        // $financialYearList = json_decode( json_encode($array), true);


        $table = "

            <script type='text/javascript'>
                $('select').select2();
            </script



                 <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$tenderQuotation->pr_number'>
                        </div>
                    </div>

                   <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Type <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$tenderQuotation->type'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PR Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='" . date('d-m-Y',strtotime($tenderQuotation->pr_entry_date)) . "'>
                        </div>
                    </div>

                    
                </div>

                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Financial Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$tenderQuotation->financial_year' readonly='readonly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Budget Year <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$tenderQuotation->budget_year' readonly='readonly'>
                        </div>
                    </div>

                     <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Department <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$tenderQuotation->department_code - $tenderQuotation->department_name' readonly='readonly'>
                        </div>
                    </div>


                    
                </div>

                <div class='row'>

                
                  <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Tender Description <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='pr_description'  value='$tenderQuotation->description'>
                        </div>
                    </div>
                    

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Tender Start Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' name='start_date' autocomplete='off' value='" . date('d-m-Y',strtotime($tenderQuotation->start_date)) . "' readonly='readonly'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Tender Close Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' name='end_date' autocomplete='off' value='" . date('d-m-Y',strtotime($tenderQuotation->end_date)) . "' readonly='readonly'>
                        </div>
                    </div>

                </div>

                <div class='row'>


                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type='text' class='form-control'  value='$tenderQuotation->amount' readonly='readonly'>
                        </div>
                    </div>

                </div>


                <script>
                  $( function() {
                    $( '.datepicker' ).datepicker({
                        changeYear: true,
                        changeMonth: true,
                    });
                  } );
                </script>


                ";


                // <div class='row'>

                //     <div class='col-sm-4'>
                //         <div class='form-group'>
                //             <label>Tender Opening Date <span class='error-text'>*</span></label>
                //             <input type='text' class='form-control datepicker' name='opening_date' autocomplete='off' value='" . date('d-m-Y',strtotime($tenderQuotation->opening_date)) . "' readonly='readonly'>
                //         </div>
                //     </div>

                // </div>


                $table1="

            

            <select name='id_vendor' id='id_vendor' class='form-control'>";
            $table1.="<option value=''>Select</option>";

            for($i=0;$i<count($vendorList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $vendorList[$i]->id;
            $code = $vendorList[$i]->code;
            $name = $vendorList[$i]->name;
            $table1.="<option value=".$id.">";

                    if($id == $tenderQuotation->id_vendor)
                    {
                      // echo "selected=selected";
                    }
                    // echo $code . " - " . $name; 

                    $table1.="</option>";

            }
            $table1.="
            </select>
                 </div>
              </div>
            </div>
            ";






        $table.="


        <script>
          $( function() {
            $( '.datepicker' ).datepicker({
                changeYear: true,
                changeMonth: true,
            });
          } );
        </script>




        <hr>

        <br>
            <h3>   Tender Specification Details </h3>
        

        <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Site Visit Location</th>
             <th>Site Visit Date -Time</th>
             <th>Period</th>
             <th>Period Type</th>
             <th>Debit GL Code</th>
             <th>Credit GL Code</th>
             <th>Category</th>
             <th>Sub Category</th>
             <th>Item</th>
             <th>Tax</th>
             <th>Qty</th>
             <th>Price</th>
             <th>Tax Amount</th>
             <th>Final Total</th>
         </tr>
        </thead>
        <tbody>";
            $total = 0;
          for($i=0;$i<count($tenderQuotationDetails);$i++)
          {
          $j=$i+1; 
            $table.="<tr>
            <td>".$j."<input type='hidden' name='id_pr_detail[]' class='form-control' value='".$tenderQuotationDetails[$i]->id."'></td>
            <td>".$tenderQuotationDetails[$i]->visit_location."</td>
            <td>".date('d-m-Y', strtotime($tenderQuotationDetails[$i]->visit_date_time))."</td>
            <td>".$tenderQuotationDetails[$i]->period."</td>
            <td>".$tenderQuotationDetails[$i]->period_type."</td>
            <td>".$tenderQuotationDetails[$i]->dt_fund. " - " . $tenderQuotationDetails[$i]->dt_department. " - " . $tenderQuotationDetails[$i]->dt_activity . " - "  . $tenderQuotationDetails[$i]->dt_account . "</td>
            <td>".$tenderQuotationDetails[$i]->cr_fund. " - " . $tenderQuotationDetails[$i]->cr_department. " - " . $tenderQuotationDetails[$i]->cr_activity . " - "  . $tenderQuotationDetails[$i]->cr_account . "</td>
            <td>".$tenderQuotationDetails[$i]->category_code . " - " .$tenderQuotationDetails[$i]->category_name."</td>
            <td>".$tenderQuotationDetails[$i]->sub_category_code . " - " .$tenderQuotationDetails[$i]->sub_category_name."</td>
            <td>".$tenderQuotationDetails[$i]->item_code . " - " .$tenderQuotationDetails[$i]->item_name."</td>
            <td>".$tenderQuotationDetails[$i]->tax_code . " - " .$tenderQuotationDetails[$i]->tax_name."</td>
            <td>".$tenderQuotationDetails[$i]->quantity."</td>
            <td>".$tenderQuotationDetails[$i]->price."</td>
            <td>".$tenderQuotationDetails[$i]->tax_price."</td>
            <td>".$tenderQuotationDetails[$i]->total_final."</td>
             </tr>";

             $total = $total + $tenderQuotationDetails[$i]->total_final;
         }
         $total = number_format($total, 2, '.', ',');

        $table.="
        <tr>
            <td bgcolor='' colspan='13'></td>
            <td bgcolor=''><b> Total : </b></td>
            <td bgcolor=''><b>".$total."</b></td>
        </tr>

        </tbody>
    </table>
    </div>

    <hr>

        <br>
            <h3>   Tender Comitee Details </h3>
        

         <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Staff Name</th>
             <th>Description</th>
         </tr>
        </thead>
        <tbody> 
        ";

        for($i=0;$i<count($tenderComiteeDetails);$i++)
        {
          $j=$i+1; 
            $table.="<tr>
            <td>".$j."</td>
            <td>".$tenderComiteeDetails[$i]->ic_no . " - " .$tenderComiteeDetails[$i]->salutation. ". " .$tenderComiteeDetails[$i]->staff_name ."</td>
            <td>".$tenderComiteeDetails[$i]->comitee_description."</td>
             </tr>";
         }

        $table.="

        </tbody>
    </table>
    </div> 

    <hr>

        <br>
            <h3>   Tender Remarks </h3>
       
         <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Remarks</th>
         </tr>
        </thead>
        <tbody> 
        ";

        for($i=0;$i<count($tenderRemarksDetails);$i++)
        {
          $j=$i+1; 
            $table.="<tr>
            <td>".$j."</td>
            <td>".$tenderRemarksDetails[$i]->remarks."</td>
             </tr>";

         }

        $table.="

        </tbody>
    </table>
    </div>

        <br>
            <h3>   Vendor Submitted For Tender </h3>
        

         <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Vendor Name</th>
             <th>Description</th>
             <th style='text-align: center;'>
             	<input type='checkbox' id='checkAll' name='checkAll'>
             	Check All
             </th>
         </tr>
        </thead>
        <tbody> 
    ";



        for($i=0;$i<count($tenderSubmissionForTendor);$i++)
        {
          $j=$i+1; 
            $table.="
        <tr>
            <td>".$j."</td>
            <td>".$tenderSubmissionForTendor[$i]->vendor_code . " - " .$tenderSubmissionForTendor[$i]->vendor_name."</td>
            <td>".$tenderSubmissionForTendor[$i]->description."</td>
             <td class='text-center'>
              <input type='checkbox' name='checkvalue[]' class='check' value='".$tenderSubmissionForTendor[$i]->id."'>
            </td>
        </tr>";
         }

        $table.="

        </tbody>
    </table>
    </div> ";

       echo $table;
    }
}
