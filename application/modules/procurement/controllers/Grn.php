<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Grn extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grn_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('grn.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = '';
            $data['searchParam'] = $formData;

            $data['grnList'] = $this->grn_model->getGRNListSearch($formData);

            $data['vendorList'] = $this->grn_model->vendorListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            //print_r($subjectDetails);exit;
            $this->loadViews("grn/list", $this->global, $data, NULL);
        }
    }

    function add()
    {

        if ($this->checkAccess('grn.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {

                // echo "<Pre>";print_r($_POST);exit;

                $id_po = $this->security->xss_clean($this->input->post('id_po'));
                $id_vendor = $this->security->xss_clean($this->input->post('id_vendor'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $received_quantity = $this->security->xss_clean($this->input->post('received_quantity'));
                $id_po_details = $this->security->xss_clean($this->input->post('id_po_detail'));

                $generated_number = $this->grn_model->generateGRNNumber();


                $data = array(
                    'id_vendor' => $id_vendor,
                    'id_po' => $id_po,
                    'grn_number' =>$generated_number,
                    'description' => $description,
                    'created_by' => $id_user,
                    'status'=>7
                );

                $inserted_id = $this->grn_model->addGrn($data);

                $total_amount = 0;
                if($inserted_id)
                {
                    // echo "<Pre>";print_r($data);exit;
                    for($i=0;$i<count($received_quantity);$i++)
                    {
                        $id_po_detail = $id_po_details[$i];
                        $quantity = $received_quantity[$i];
                        if($quantity > 0)
                        {
                            $po_detail = $this->grn_model->getPoDetailById($id_po_detail);

                            if($po_detail)
                            {
                                $id_item = $po_detail->id_item;

                                $item = $this->grn_model->getProcurementItem($id_item);
                                $product_quantity = 0;

                                if($item)
                                {
                                    $product_quantity = $item->quantity;
                                }

                                $po_balance_quantity = $po_detail->balance_quantity;
                                $po_quantity = $po_detail->balance_quantity;
                                $po_received_quantity = $po_detail->received_quantity;
                                $po_price = $po_detail->price;
                                $total_price = $po_price * $quantity;



                                $detail_data = array(
                                    'id_grn' => $inserted_id,
                                    'id_po_detail' =>$id_po_detail,
                                    'id_category' => $po_detail->id_category,
                                    'id_sub_category' => $po_detail->id_sub_category,
                                    'id_item' => $po_detail->id_item,
                                    'total_quantity' => $po_quantity,
                                    'quantity' => $quantity,
                                    'balance_quantity' => $po_balance_quantity - $quantity,
                                    'price' => $po_price,
                                    'total_price' => $total_price,
                                    'created_by' => $id_user,
                                    'status'=>1
                                );
                                

                                $id_grn_detail = $this->grn_model->addGrnDetail($detail_data);

                                if($id_grn_detail)
                                {
                                    $product_quantity_after_add = $product_quantity + $quantity;

                                    $add_item_quantity_data = array(
                                        'id_description' => 1,
                                        'id_item' => $id_item,
                                        'id_grn_detail' => $id_grn_detail,
                                        'id_grn' => $inserted_id,
                                        'previous_quantity' => $product_quantity,
                                        'grn_quantity' =>$quantity,
                                        'assembly_quantity' => 0,
                                        'quantity' => $product_quantity_after_add,
                                        'status' => 1,
                                        'created_by' => $id_user
                                        );
                                    
                                    $id_product_quantity = $this->grn_model->addProductQuantity($add_item_quantity_data);

                                    if($id_product_quantity)
                                    {
                                        $item_update_data = array(
                                            'quantity' => $product_quantity_after_add
                                        );
                                    
                                        $id_grn_detail = $this->grn_model->updateItem($item_update_data,$id_item);
                                    }


                                    $po_balance_quantity = $po_balance_quantity - $quantity;
                                    $po_received_quantity = $po_received_quantity + $quantity;

                                    $po_update_data = array(
                                        'id_grn_detail' => $id_grn_detail,
                                        'received_quantity' =>$po_received_quantity,
                                        'balance_quantity' => $po_balance_quantity,
                                        'updated_by' => $id_user,
                                        'updated_dt_tm' => date('Y-m-d H:i:s')
                                        );
                                    
                                    $id_grn_detail = $this->grn_model->updatePODetail($po_update_data,$id_po_detail);
                                }
                                $total_amount = $total_amount + $total_price;
                            }
                        }
                    }
                }

                $update_data['total_amount'] = $total_amount;
                $update_data['balance_amount'] = $total_amount;

                $id_grn_detail = $this->grn_model->updateGrn($update_data,$inserted_id);
                redirect('/procurement/grn/list');
            }

            $data['poPendingList'] = $this->grn_model->poPendingList();

            // echo "<Pre>";print_r($data['poPendingList']);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Category';
            $this->loadViews("grn/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('grn.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/grn/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            

            $data['grn'] = $this->grn_model->getGRN($id);
            $data['grnDetails'] = $this->grn_model->getGRNDetails($id);
            // $data['po'] = $this->grn_model->getPo($id);
            // $data['poDetails'] = $this->grn_model->getPoDetails($id);
            // $data['procurementCategoryList'] = $this->grn_model->procurementCategoryListByStatus('1');
            // $data['vendorList'] = $this->grn_model->vendorListByStatus('1');

            // echo "<Pre>";print_r($data['po']);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("grn/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('grn.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/poEntry/list');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );
                 $result = $this->grn_model->editPOList($data,$id);

                redirect('/procurement/poEntry/approvallist');

                // $check_duplication = $this->budget_amount_model->editBudgetAmount($data,$id);

                // print_r($this->input->post());exit;
            }
            //  $data['fundCodeList'] = $this->grn_model->getFundCodeList();
            // $data['departmentCodeList'] = $this->grn_model->getDepartmentCodeList();
            // $data['accountCodeList'] = $this->grn_model->getAccountCodeList();
            // $data['activityCodeList'] = $this->grn_model->getActivityCodeList();
            // $data['prCategoryList'] = $this->grn_model->prCategoryList();
            // $data['prSubCategoryList'] = $this->grn_model->prSubCategoryList();
            // $data['prItemList'] = $this->grn_model->prItemList();
            // $data['departmentList'] = $this->grn_model->departmentList();
            // $data['vendorList'] = $this->grn_model->vendorListByStatus('Approved');
            
            $data['poMaster'] = $this->grn_model->getMasterPoDetails($id);
            $data['poDetails'] = $this->grn_model->getPoDetails($id);

            // echo "<Pre>";print_r($data);exit();
            

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("grn/view", $this->global, $data, NULL);
        }
    }

    function approvallist()
    {
        if ($this->checkAccess('grn.approval') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $resultprint = $this->input->post();

           if($resultprint)
            {
             switch ($resultprint['button'])
             {
                case 'approve':

                     for($i=0;$i<count($resultprint['checkvalue']);$i++)
                        {

                         $id = $resultprint['checkvalue'][$i];
                         
                         $result = $this->grn_model->editPOList($id);
                        }
                        redirect($_SERVER['HTTP_REFERER']);
                     break;


                case 'search':
                     
                     break;
                 
                default:
                     break;
             }
                
            }


            
            $formData['id_vendor'] = $this->security->xss_clean($this->input->post('id_vendor'));
            $formData['id_financial_year'] = $this->security->xss_clean($this->input->post('id_financial_year'));
            $formData['department_code'] = $this->security->xss_clean($this->input->post('department_code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));


            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['poList'] = $this->grn_model->getPOListSearch($formData);

            $data['financialYearList'] = $this->grn_model->financialYearListByStatus('1');
            $data['vendorList'] = $this->grn_model->vendorListByStatus('Approved');
            $data['departmentList'] = $this->grn_model->departmentListByStatus('1');
            $data['departmentCodeList'] = $this->grn_model->getDepartmentCodeList();
            

            // echo "<Pre>";print_r($formData);exit();
            
                // $array = $this->security->xss_clean($this->input->post('checkvalue'));
                // if (!empty($array))
                // {

                //     $result = $this->grn_model->editPOList($array);
                //     redirect($_SERVER['HTTP_REFERER']);
                // }


            $this->global['pageTitle'] = 'Inventory Management : Approve Vendor';
            $this->loadViews("grn/approval_list", $this->global, $data, NULL);

        }
    }

    function getData($id)
    {
        // echo "<Pre>";print_r($id);exit;

        // $vendorList = $this->grn_model->vendorListByStatus('1');

        $po = $this->grn_model->getPo($id);


        // $array = $this->grn_model->financialYearListByStatus('1');

        // $financialYearList = json_decode( json_encode($array), true);

        // echo "<Pre>";print_r($po);exit;




        // <div class='container-fluid page-wrapper'>
        $table = "

            <script type='text/javascript'>
                $('select').select2();
            </script>




                <div class='row'>
                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Number <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$po->po_number'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' readonly='readonly' value='$po->description'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>PO Entry Date <span class='error-text'>*</span></label>
                            <input type='text' class='form-control datepicker' readonly='readonly' value='$po->created_dt_tm'>
                        </div>
                    </div>

                    
                </div>

                <div class='row'>

                  <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Vendor <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' value='$po->vendor_code - $po->vendor_name' readonly='readonly'>
                            <input type='hidden' class='form-control' id='id_vendor' name='id_vendor' value='$po->id_vendor'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>Total Amount <span class='error-text'>*</span></label>
                            <input type='text' readonly='readonly' class='form-control' name='total_amount'  value='$po->total_amount'>
                        </div>
                    </div>

                    <div class='col-sm-4'>
                        <div class='form-group'>
                            <label>GRN Description <span class='error-text'>*</span></label>
                            <input type='text' class='form-control' id='description' name='description'>
                        </div>
                    </div>

                 </div>


                ";
                // </div>

        $poDetails = $this->grn_model->getPoDetails($id);


        $table.="

        <h3> &nbsp;&nbsp;&nbsp;PO Details </h3>

        <div class='custom-table'>

        <table class='table' width='100%'>
        <thead>
         <tr>
             <th>Sl. No</th>
             <th>Catedory</th>
             <th>Sub Catedory</th>
             <th>Product</th>
             <th>Quantity</th>
             <th>Balance Quantity</th>
             <th>Received Quantity</th>
             <th>Price</th>
             <th>Total Price</th>
             <th>Quantity</th>
         </tr>
        </thead>
        <tbody>";
            $total_detail = 0;
          for($i=0;$i<count($poDetails);$i++)
          {
          $j=$i+1; 

            $id = $poDetails[$i]->id;
            $category_name = $poDetails[$i]->category_name;
            $category_code = $poDetails[$i]->category_code;
            $sub_category_name = $poDetails[$i]->sub_category_name;
            $sub_category_code = $poDetails[$i]->sub_category_code;
            $item_name = $poDetails[$i]->item_name;
            $item_code = $poDetails[$i]->item_code;
            $quantity = $poDetails[$i]->quantity;
            $price = $poDetails[$i]->price;
            $total_price = $poDetails[$i]->total_price;
            $balance_quantity = $poDetails[$i]->balance_quantity;
            $received_quantity = $poDetails[$i]->received_quantity;

            $j=$i+1;
            $table .= "
            <tbody>
            <tr>
                <td>$j</td>
                <td>$category_code - $category_name</td>
                <td>$sub_category_code - $sub_category_name</td>
                <td>$item_code - $item_name</td>
                <td>$quantity</td>
                <td>$balance_quantity</td>
                <td>$received_quantity</td>
                <td>$price</td>
                <td>$total_price</td>
                <td>
                    <input type='number' id='received_quantity[]' name='received_quantity[]'";
                    if($balance_quantity == 0)
                    {
                        $table .= " value='$balance_quantity' readonly";
                    }
                    $table .= ">
                    <input type='hidden' id='id_po_detail[]' name='id_po_detail[]' value='$id'>
                </td>
            </tr>";
            $total_detail = $total_detail + $total_price;
         }
         $total_detail = number_format($total_detail, 2, '.', ',');

        $table.="
        <tr>
            <td bgcolor='' colspan='5'></td>
            <td bgcolor=''><b> Total : </b></td>
            <td bgcolor=''><b>".$total_detail."</b></td>
            <td bgcolor=''><b></b></td>
        </tr>

        </tbody>
        </table>
        </div>";
       echo $table;

    }
}