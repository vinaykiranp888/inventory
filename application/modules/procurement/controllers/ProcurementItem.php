<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProcurementItem extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('procurement_sub_category_model');
            $this->load->model('procurement_category_model');
            $this->load->model('procurement_item_model');
            $this->isLoggedIn();
            error_reporting(0);
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function list()
    {

        if ($this->checkAccess('procurement_sub_category.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r("dasda");exit;
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['procurementSubCategoryList'] = $this->procurement_sub_category_model->procurementSubCategoryList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_procurement_category'] = $this->security->xss_clean($this->input->post('id_procurement_category'));
            $formData['id_procurement_sub_category'] = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));

            $data['searchParameters'] = $formData; 

            $data['procurementItemList'] = $this->procurement_item_model->procurementItemListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : List Procurement Item';
            $this->loadViews("procurement_item/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('procurement_item.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_procurement_category = $this->security->xss_clean($this->input->post('id_procurement_category'));
                $id_procurement_sub_category = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));
                $id_package = $this->security->xss_clean($this->input->post('id_package'));
                $gst_tax = $this->security->xss_clean($this->input->post('gst_tax'));
                $part_number = $this->security->xss_clean($this->input->post('part_number'));
                $value = $this->security->xss_clean($this->input->post('value'));
                $package = $this->security->xss_clean($this->input->post('package'));
                $manufacturer = $this->security->xss_clean($this->input->post('manufacturer'));
                $spq = $this->security->xss_clean($this->input->post('spq'));
                $moq = $this->security->xss_clean($this->input->post('moq'));
                $alternative_part_number = $this->security->xss_clean($this->input->post('alternative_part_number'));
                $price = $this->security->xss_clean($this->input->post('price'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $thrushold = $this->security->xss_clean($this->input->post('thrushold'));
                $lead_time = $this->security->xss_clean($this->input->post('lead_time'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'part_number' => $part_number,
                    'value' => $value,
                    'package' => $package,
                    'manufacturer' => $manufacturer,
                    'spq' => $spq,
                    'moq' => $moq,
                    'price' => $price,
                    'alternative_part_number' => $alternative_part_number,
                    'description' => $description,
                    'id_procurement_category' => $id_procurement_category,
                    'id_procurement_sub_category' => $id_procurement_sub_category,
                    'id_package' => $id_package,
                    'thrushold' => $thrushold,
                    'lead_time' => $lead_time,
                    'gst_tax' => $gst_tax,
                    'status' => $status,
                    'created_by' => $id_user
                );
            // echo "<Pre>"; print_r($data);exit;
            
                $result = $this->procurement_item_model->addNewProcurementItem($data);
                redirect('/procurement/procurementItem/list');
            }
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['manufacturerList'] = $this->procurement_item_model->manufacturerListBystatus('1');
            $data['packageList'] = $this->procurement_item_model->packageListBystatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Procurement Sub Category';
            $this->loadViews("procurement_item/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_item.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if ($id == null)
            {
                redirect('/procurement/procurementItem/list');
            }
            if($this->input->post())
            {

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_procurement_category = $this->security->xss_clean($this->input->post('id_procurement_category'));
                $id_procurement_sub_category = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));
                $id_package = $this->security->xss_clean($this->input->post('id_package'));
                $gst_tax = $this->security->xss_clean($this->input->post('gst_tax'));
                $part_number = $this->security->xss_clean($this->input->post('part_number'));
                $value = $this->security->xss_clean($this->input->post('value'));
                $package = $this->security->xss_clean($this->input->post('package'));
                $manufacturer = $this->security->xss_clean($this->input->post('manufacturer'));
                $spq = $this->security->xss_clean($this->input->post('spq'));
                $moq = $this->security->xss_clean($this->input->post('moq'));
                $alternative_part_number = $this->security->xss_clean($this->input->post('alternative_part_number'));
                $price = $this->security->xss_clean($this->input->post('price'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $thrushold = $this->security->xss_clean($this->input->post('thrushold'));
                $lead_time = $this->security->xss_clean($this->input->post('lead_time'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'part_number' => $part_number,
                    'value' => $value,
                    'package' => $package,
                    'manufacturer' => $manufacturer,
                    'spq' => $spq,
                    'moq' => $moq,
                    'price' => $price,
                    'alternative_part_number' => $alternative_part_number,
                    'description' => $description,
                    'id_procurement_category' => $id_procurement_category,
                    'id_procurement_sub_category' => $id_procurement_sub_category,
                    'id_package' => $id_package,
                    'thrushold' => $thrushold,
                    'lead_time' => $lead_time,
                    'gst_tax' => $gst_tax,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );
                // print_r($results);exit;
                $result = $this->procurement_item_model->editProcurementItem($data,$id);
                redirect('/procurement/procurementItem/list');
            }
            $data['id_item'] = $id;
            $data['procurementItem'] = $this->procurement_item_model->getProcurementItem($id);
            $data['procurementCategoryList'] = $this->procurement_category_model->procurementCategoryList();
            $data['manufacturerList'] = $this->procurement_item_model->manufacturerListBystatus('1');
            $data['packageList'] = $this->procurement_item_model->packageListBystatus('1');

            // $data['procurementSubCategoryList'] = $this->procurement_sub_category_model->procurementSubCategoryList();

            // echo "<Pre>";print_r($data['procurementItem']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Sub Category';
            $this->loadViews("procurement_item/edit", $this->global, $data, NULL);
        }
    }


    function vendors($id = NULL,$id_item_vendor = NULL)
    {
        if ($this->checkAccess('procurement_item.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;
            
            if ($id == null)
            {
                redirect('/procurement/procurementItem/list');
            }
            if($this->input->post())
            {

                // echo '<Pre>';print_r($_POST);exit;

                $id_vendor = $this->security->xss_clean($this->input->post('id_vendor'));
                $price = $this->security->xss_clean($this->input->post('price'));

                $data = array(
                    'id_item' => $id,
                    'id_vendor' => $id_vendor,
                    'price' => $price,
                    'status' => 1
                );

                if($id_item_vendor > 0)
                {
                    $data['updated_by'] = $id_user;
                    $data['updated_dt_tm'] = date('Y-m-d H:i:s');
                    $result = $this->procurement_item_model->editProcurementItemHasVendor($data,$id);
                }
                else
                {
                    $data['created_by'] = $id_user;
                    $result = $this->procurement_item_model->addProcurementItemHasVendor($data);
                }
                // print_r($results);exit;
                redirect('/procurement/procurementItem/vendors/'.$id);
            }

            $data['id_item'] = $id;
            $data['id_item_vendor'] = $id_item_vendor;
            $data['procurementItemVendorList'] = $this->procurement_item_model->getProcurementItemVendorList($id);
            $data['procurementItemVendor'] = $this->procurement_item_model->getProcurementItemVendor($id_item_vendor);
            $data['vendorList'] = $this->procurement_item_model->getVendorListByStatus('1');
            // echo "<Pre>";print_r($data['procurementItemVendorList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Item Vendors';
            $this->loadViews("procurement_item/vendors", $this->global, $data, NULL);
        }
    }


    function getSubcategory($id)
    {
        $results = $this->procurement_sub_category_model->procurementSubCategoryListByProcurementId($id);
                // echo "<Pre>";print_r($results);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_procurement_sub_category' id='id_procurement_sub_category' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $sub_category_code = $results[$i]->code;
            $sub_categoryname = $results[$i]->name;

            $table.='<option value="'. $id.'">'.$sub_category_code . ' - '. $sub_categoryname .'</option>';

            }

            $table.="</select>";
            echo $table;
            exit;
    }

    function deleteProcurementItemHasVendor($id)
    {
        $results = $this->procurement_item_model->deleteProcurementItemHasVendor($id);
        echo 1;exit;
    }

    function getProductItemVendorDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $result = $this->procurement_item_model->getProductItemVendorDuplication($tempData);
        // print_r($result);exit;

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }
}