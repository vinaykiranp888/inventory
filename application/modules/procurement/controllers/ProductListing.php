<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProductListing extends BaseController
{
    public function __construct()
    {
        try
        {
            parent::__construct();
            $this->load->model('product_listing_model');
            $this->isLoggedIn();
        }
        catch(Exception $e)
        {
            echo "<Pre>";print_r("Exception Generating On Model Loading In Controller : \n\n\n".$e);exit;
        }
    }

    function list()
    {

        if ($this->checkAccess('procurement_item.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_procurement_category'] = $this->security->xss_clean($this->input->post('id_procurement_category'));
            $formData['id_procurement_sub_category'] = $this->security->xss_clean($this->input->post('id_procurement_sub_category'));
            $data['searchParameters'] = $formData; 

            $data['procurementItemList'] = $this->product_listing_model->procurementItemListSearch($formData);

         
            $data['procurementCategoryList'] = $this->product_listing_model->procurementCategoryList();
            $data['procurementSubCategoryList'] = $this->product_listing_model->procurementSubCategoryList();

            $this->global['pageTitle'] = 'Inventory Management : List Procurement Item';
            $this->loadViews("product_listing/list", $this->global, $data, NULL);

        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('procurement_item.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/procurement/ProductListing/list');
            }
            if($this->input->post())
            {
                print_r($this->input->post());exit;
            }
            

            $data['product'] = $this->product_listing_model->getProcurementItem($id);
            $data['productQuantity'] = $this->product_listing_model->getProductQuantityListByIdItem($id);            
            
            // echo '<Pre>';print_r($data['product']);exit();     

            $this->global['pageTitle'] = 'Inventory Management : Edit Procurement Category';
            $this->loadViews("product_listing/edit", $this->global, $data, NULL);
        }
    }
}