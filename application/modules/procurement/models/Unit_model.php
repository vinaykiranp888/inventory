<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Unit_model extends CI_Model
{
    function unitList()
    {
        $this->db->select('*');
        $this->db->from('unit');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function unitListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('unit');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getUnitDetails($id)
    {
        $this->db->select('*');
        $this->db->from('unit');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewUnit($data)
    {
        $this->db->trans_start();
        $this->db->insert('unit', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editUnit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('unit', $data);
        return TRUE;
    }
}