<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Procurement_item_model extends CI_Model
{
    function manufacturerListBystatus($status)
    {
        $this->db->select('m.*');
        $this->db->from('manufacturer as m');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getVendorListByStatus($status)
    {
        $this->db->select('m.*');
        $this->db->from('vendor_details as m');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function packageListBystatus($status)
    {
        $this->db->select('m.*');
        $this->db->from('packages as m');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function procurementItemList()
    {
        $this->db->select('pi.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code');
        $this->db->from('procurement_item as pi');
        $this->db->join('procurement_category as pc', 'pi.id_procurement_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'pi.id_procurement_sub_category = psc.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementItemListSearch($formData)
    {
        $this->db->select('pi.*, pc.name as pr_category_name, pc.code as pr_category_code, psc.name as pr_sub_category_name, psc.code as pr_sub_category_code,');
        $this->db->from('procurement_item as pi');
        $this->db->join('procurement_category as pc', 'pi.id_procurement_category = pc.id');
        $this->db->join('procurement_sub_category as psc', 'pi.id_procurement_sub_category = psc.id');
        if($formData['id_procurement_category']) {
            $likeCriteria = "(pi.id_procurement_category  LIKE '%" . $formData['id_procurement_category'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($formData['id_procurement_sub_category']) {
            $likeCriteria = "(pi.id_procurement_sub_category  LIKE '%" . $formData['id_procurement_sub_category'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($formData['name']) {
            $likeCriteria = "(pi.name  LIKE '%" . $formData['name'] . "%' or pi.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($formData);exit();     
         return $result;
    }

    function getProcurementItem($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_item');
        $this->db->where('id', $id);
        $query = $this->db->get();
         // print_r($query->row());exit();     
        return $query->row();
    }
    
    function addNewProcurementItem($data)
    {
        $this->db->trans_start();
        $this->db->insert('procurement_item', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProcurementItem($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_item', $data);
        return TRUE;
    }

    function getProcurementItemVendorList($id_item)
    {
        $this->db->select('pihs.*, v.name as vendor_name, v.code as vendor_code');
        $this->db->from('procurement_item_has_vendors as pihs');
        $this->db->join('vendor_details as v', 'pihs.id_vendor = v.id');
        $this->db->where('pihs.id_item', $id_item);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function addProcurementItemHasVendor($data)
    {
        $this->db->trans_start();
        $this->db->insert('procurement_item_has_vendors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProcurementItemVendor($id)
    {
        $this->db->select('pihs.*');
        $this->db->from('procurement_item_has_vendors as pihs');
        $this->db->where('pihs.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         //print_r($result);exit();     
         return $result;
    }

    function deleteProcurementItemHasVendor($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('procurement_item_has_vendors');
        return TRUE;
    }

    function editProcurementItemHasVendor($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_item_has_vendors', $data);
        return TRUE;
    }

    function getProductItemVendorDuplication($data)
    {
        $this->db->select('st.*');
        $this->db->from('procurement_item_has_vendors as st');        
        if($data['id'] != '')
        {
            $this->db->where('st.id !=', $data['id']);
        }
        if($data['id_vendor'] != '')
        {
            $this->db->where('st.id_vendor', $data['id_vendor']);
        }
        if($data['id_item'] != '')
        {
            $this->db->where('st.id_item', $data['id_item']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}

