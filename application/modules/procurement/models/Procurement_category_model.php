<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Procurement_category_model extends CI_Model
{
    function procurementCategoryList()
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementCategoryListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        if (!empty($search))
        {
            $likeCriteria = "(code  LIKE '%" . $search . "%' or description  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getProcurementCategoryDetails($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProcurementCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('procurement_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProcurementCategory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_category', $data);
        return TRUE;
    }
}

