<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturer_model extends CI_Model
{
   function manufacturerListSearch($data)
    {
        $this->db->select('vd.*');
        $this->db->from('manufacturer as vd');
        if ($data['name']!='')
        {
            $likeCriteria = "(vd.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email']!='')
        {
            $likeCriteria = "(vd.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['mobile']!='')
        {
            $likeCriteria = "(vd.mobile  LIKE '%" . $data['mobile'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->order_by("vd.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function generateManufacturerNumber()
    {
        $year = date('y');
        $Year = date('Y');

        $this->db->select('*');
        $this->db->from('manufacturer');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

        // $data=$result->po_number;
        // $value=substr($data, 3,6);
        $count=$result + 1;

       $generated_number = "M" .(sprintf("%'05d", $count)). "-" . $Year;
       return $generated_number;
    }

    function addNewManufacturer($data)
    {
        $this->db->trans_start();
        $this->db->insert('manufacturer', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getManufacturer($id)
    {
        $this->db->select('*');
        $this->db->from('manufacturer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getTempBank($id_session)
    {
        $this->db->select('bd.*, c.name as country, s.name as state, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('temp_bank_details as bd');
        $this->db->join('country as c', 'bd.country = c.id');
        $this->db->join('state as s', 'bd.state = s.id');
        $this->db->join('bank_registration as bnk', 'bd.id_bank = bnk.id');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempBankBySessionId($id_session)
    {
        $this->db->select('bd.*');
        $this->db->from('temp_bank_details as bd');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewTempBank($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_bank_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempBilling($id_session)
    {
        $this->db->select('*');
        $this->db->from('temp_billing_details ');        
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewTempBilling($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_billing_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

    function deleteTempBank($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_bank_details');
         return TRUE;
    }

    function deleteTempBilling($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_billing_details');
        return TRUE;
    }

    function addNewBankDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('bank_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

    function addNewBillingDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('personal_billing_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

    

    function addNewCompany($companyData)
    {
        $this->db->trans_start();
        $this->db->insert('company_details', $companyData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }    
    
    function editManufacturer($data,$id)
    {
        $this->db->where_in('id', $id);
        $this->db->update('manufacturer', $data);
        return TRUE;
    }

    function editManufacturerList($array)
    {
      $status = ['manufacturer_status'=>'Approved'];

        $this->db->where_in('id', $array);
      $this->db->update('manufacturer', $status);
      // $this->db->set('status', $status);
    }

    function updateManufacturer($data,$id)
    {
        $this->db->where_in('id', $id);
        $this->db->update('manufacturer', $data);
        return TRUE;
    }

    function blockManufacturerList($array)
    {
        $status = ['manufacturer_status'=>'Blocked'];

        $this->db->where_in('id', $array);
        $this->db->update('manufacturer', $status);
        // $this->db->set('status', $status);
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function bankListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function deleteTempBankBySessionId($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_bank_details');
       return TRUE;
    }
}