<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Nonpo_model extends CI_Model
{
    
    function getPRApproval() {
        $this->db->select('*');
        $this->db->from('pr_entry');;
        $query = $this->db->get();
        return $query->result();
    }

    function generateNONPONumber()
    {

        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('nonpo_entry');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $generate_number = $number = "NONPO" .(sprintf("%'06d", $count)). "/" . $Year;
           return $generate_number;        
    }

    function addNonPO($data) {
        $this->db->trans_start();
        $this->db->insert('nonpo_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewPODetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('nonpo_detail', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

     
    function poPendingList($id) {
        $this->db->select('*');
        $this->db->from('nonpo_entry');
        $this->db->where('status', $id);
        $query = $this->db->get();
        return $query->result();
    }

     function editNONPO($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('nonpo_entry', $data);
      return TRUE;
    }






    function getNonPOListSearch($data)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, pr.pr_number, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('nonpo_entry as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        $this->db->join('pr_entry as pr','po.id_pr = pr.id');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.description  LIKE '%" . $data['name'] . "%' or po.reason  LIKE '%" . $data['name'] . "%' or po.nonpo_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('po.department_code', $data['department_code']);
        }
        if ($data['id_vendor'] !='')
        {
            $this->db->where('po.id_vendor', $data['id_vendor']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('po.id_financial_year', $data['id_financial_year']);
        }
        if ($data['status'] !='')
        {
         // echo "<Pre>";print_r($data);exit();     

            $this->db->where('po.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getMasterNonPoDetails($id)
    {
        $this->db->select('po.*, pr.pr_entry_date, pr.description as pr_description, pr.pr_number, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, pr.pr_number, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('nonpo_entry as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('pr_entry as pr','po.id_pr = pr.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        $this->db->where('po.id', $id);
         $query = $this->db->get();
         $result = $query->row();   
         return $result;
    }

    function getNonPoDetails($id)
    {

         $this->db->select('pe.*');
        $this->db->from('nonpo_entry as pe');
        $this->db->where('pe.id', $id);
        $query = $this->db->get();
        $data = $query->row();

        $type = $data->type;



         $this->db->select('tpe.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code,  pi.description as item_name, pi.code as item_code, tx.code as tax_code, tx.name as tax_name');
        // $this->db->from('pr_detail');
        $this->db->from('nonpo_detail as tpe');
        $this->db->join('tax as tx', 'tpe.id_tax = tx.id');
        if($type == 'Procurement')
        {

            $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id');

        }
        elseif($type == 'Asset')
        {
             $this->db->join('asset_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('asset_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('asset_item as pi', 'tpe.id_item = pi.id');

        }
        $this->db->where('tpe.id_po_entry', $id);
        // $this->db->where('id_pr', $id);
        $query = $this->db->get();
        return $query->result();
    }

}
