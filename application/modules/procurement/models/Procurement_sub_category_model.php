<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Procurement_sub_category_model extends CI_Model
{
    function procurementSubCategoryList()
    {
        $this->db->select('psc.*, pc.code as pr_category_code, pc.name as pr_category_name');
        $this->db->from('procurement_sub_category as psc');
        $this->db->join('procurement_category as pc', 'psc.id_procurement_category = pc.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementSubCategoryListByProcurementId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_sub_category as psc');
        $this->db->where('psc.id_procurement_category='.$id);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementSubCategoryListSearch($formData)
    {
        $this->db->select('psc.*, pc.code as pr_category_code, pc.name as pr_category_name');
        $this->db->from('procurement_sub_category as psc');
        $this->db->join('procurement_category as pc', 'psc.id_procurement_category = pc.id');
        if($formData['pr_category_code']) {
            $likeCriteria = "(psc.id_procurement_category  LIKE '%" . $formData['pr_category_code'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['name']) {
            $likeCriteria = "(psc.description  LIKE '%" . $formData['name'] . "%' or psc.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();   
         // print_r($formData);exit();     
         return $result;
    }

    function getProcurementSubCategory($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_sub_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProcurementSubCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('procurement_sub_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProcurementSubCategory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_sub_category', $data);
        return TRUE;
    }
}

