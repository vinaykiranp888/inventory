<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model extends CI_Model
{
   function vendorListSearch($data)
    {
        $this->db->select('vd.*');
        $this->db->from('vendor_details as vd');
        if ($data['name']!='')
        {
            $likeCriteria = "(vd.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email']!='')
        {
            $likeCriteria = "(vd.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['mobile']!='')
        {
            $likeCriteria = "(vd.mobile  LIKE '%" . $data['mobile'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->order_by("vd.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function vendorListSearchForBlocList($data)
    {
        $this->db->select('vd.*');
        $this->db->from('vendor_details as vd');
        if ($data['name']!='')
        {
            $likeCriteria = "(vd.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email']!='')
        {
            $likeCriteria = "(vd.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric']!='')
        {
            $likeCriteria = "(vd.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('vd.vendor_status', 'Approved');
        $this->db->or_where('vd.vendor_status', 'Blocked');

        $query = $this->db->get();
        $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getVendor($id)
    {
        $this->db->select('*');
        $this->db->from('vendor_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getVendorCompanyByVendorId($id)
    {
        $this->db->select('*');
        $this->db->from('company_details');
        $this->db->where('id_vendor', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getVendorBankListByVendorId($id)
    {
        $this->db->select('bd.*, c.name as country, s.name as state, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('bank_details as bd');
        $this->db->join('country as c', 'bd.country = c.id');
        $this->db->join('state as s', 'bd.state = s.id');
        $this->db->join('bank_registration as bnk', 'bd.id_bank = bnk.id');
        $this->db->where('id_vendor', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getVendorBillingListByVendorId($id)
    {
        $this->db->select('*');
        $this->db->from('personal_billing_details');
        $this->db->where('id_vendor', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function deleteTempData($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_bank_details');
    }

    function getTempBank($id_session)
    {
        $this->db->select('bd.*, c.name as country, s.name as state, bnk.name as bank_name, bnk.code as bank_code');
        $this->db->from('temp_bank_details as bd');
        $this->db->join('country as c', 'bd.country = c.id');
        $this->db->join('state as s', 'bd.state = s.id');
        $this->db->join('bank_registration as bnk', 'bd.id_bank = bnk.id');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }


    function getTempBankBySessionId($id_session)
    {
        $this->db->select('bd.*');
        $this->db->from('temp_bank_details as bd');
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }


    function addNewTempBank($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_bank_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempBilling($id_session)
    {
        $this->db->select('*');
        $this->db->from('temp_billing_details ');        
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }


    function addNewTempBilling($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_billing_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

    function deleteTempBank($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_bank_details');
         return TRUE;
    }

    function deleteTempBilling($id)
    {
         $this->db->where('id', $id);
       $this->db->delete('temp_billing_details');
         return TRUE;
    }

    function addNewBankDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('bank_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

    function addNewBillingDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('personal_billing_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

    function addNewVendor($data)
    {
        $this->db->trans_start();
        $this->db->insert('vendor_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewCompany($companyData)
    {
        $this->db->trans_start();
        $this->db->insert('company_details', $companyData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }    
    
    function editVendor($data,$id)
    {
        $this->db->where_in('id', $id);
        $this->db->update('vendor_details', $data);
        return TRUE;
    }

    function editVendorList($array)
    {
      $status = ['vendor_status'=>'Approved'];

        $this->db->where_in('id', $array);
      $this->db->update('vendor_details', $status);
      // $this->db->set('status', $status);
    }

    function updateVendor($data,$id)
    {
        $this->db->where_in('id', $id);
        $this->db->update('vendor_details', $data);
        return TRUE;
    }

    function blockVendorList($array){
      $status = ['vendor_status'=>'Blocked'];

        $this->db->where_in('id', $array);
      $this->db->update('vendor_details', $status);
      // $this->db->set('status', $status);
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function bankListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function generateVendorNumber()
    {
        $year = date('y');
        $Year = date('Y');

        $this->db->select('*');
        $this->db->from('vendor_details');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

        // $data=$result->po_number;
        // $value=substr($data, 3,6);
        $count=$result + 1;

       $generated_number = "V" .(sprintf("%'05d", $count)). "-" . $Year;
       return $generated_number;
    }

    function moveTempBankToDetails($id_vendor)
    {
        $id_session = $this->session->my_session_id;
        $bank_details = $this->getTempBankBySessionId($id_session);
        // echo "<Pre>";print_r($bank_details);exit();
        foreach ($bank_details as $bank_detail)
        {
            unset($bank_detail->id_session);
            unset($bank_detail->id);
            $bank_detail->id_vendor = $id_vendor;
            $inserted_bank_id = $this->addNewBankDetails($bank_detail);
        }
        $this->deleteTempBankBySessionId($id_session);
    }

    function moveTempBillToDetails($id_vendor)
    {
        $id_session = $this->session->my_session_id;
        $bill_details = $this->getTempBilling($id_session);
        foreach ($bill_details as $bill_detail)
        {
            unset($bill_detail->id_session);
            unset($bill_detail->id);
            $bill_detail->id_vendor = $id_vendor;
            $inserted_bank_id = $this->addNewBillingDetails($bill_detail);
        }
        $this->deleteTempBillBySessionId($id_session);
    }

     function deleteTempBankBySessionId($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_bank_details');
    }

    function deleteTempBillBySessionId($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_billing_details');
    }
}