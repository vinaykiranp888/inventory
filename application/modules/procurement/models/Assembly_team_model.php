<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assembly_team_model extends CI_Model
{
   function assemblyTeamListSearch($data)
    {
        $this->db->select('vd.*');
        $this->db->from('assembly_team as vd');
        if ($data['name']!='')
        {
            $likeCriteria = "(vd.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email']!='')
        {
            $likeCriteria = "(vd.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['mobile']!='')
        {
            $likeCriteria = "(vd.mobile  LIKE '%" . $data['mobile'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("vd.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function assemblyTeamListSearchForBlocList($data)
    {
        $this->db->select('vd.*');
        $this->db->from('assembly_team as vd');
        if ($data['name']!='')
        {
            $likeCriteria = "(vd.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email']!='')
        {
            $likeCriteria = "(vd.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric']!='')
        {
            $likeCriteria = "(vd.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('vd.assembly_team_status', 'Approved');
        $this->db->or_where('vd.assembly_team_status', 'Blocked');

        $query = $this->db->get();
        $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function generateAssemblyTeamNumber()
    {
        $year = date('y');
        $Year = date('Y');

        $this->db->select('*');
        $this->db->from('assembly_team');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

        // $data=$result->po_number;
        // $value=substr($data, 3,6);
        $count=$result + 1;

       $generated_number = "A" .(sprintf("%'05d", $count)). "-" . $Year;
       return $generated_number;
    }

    function addNewAssemblyTeam($data)
    {
        $this->db->trans_start();
        $this->db->insert('assembly_team', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function editAssemblyTeam($data,$id)
    {
        $this->db->where_in('id', $id);
        $this->db->update('assembly_team', $data);
        return TRUE;
    }

    function getAssemblyTeam($id)
    {
        $this->db->select('*');
        $this->db->from('assembly_team');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getStateByCountry($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempBillBySessionId($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_billing_details');
    }
}
?>