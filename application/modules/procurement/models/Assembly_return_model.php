<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assembly_return_model extends CI_Model
{
    function getAssemblyReturnListSearch($data)
    {
        $this->db->select('ar.*, po.reference_number as distribution_number, v.name as assembly_name, v.code as assembly_code, st.name as status_name');
        $this->db->from('assembly_return as ar');
        $this->db->join('assembly_team as v','ar.id_assembly = v.id');
        $this->db->join('assembly_distribution as po','ar.id_assembly_distribution = po.id');
        $this->db->join('status_table as st','ar.status = st.id','left');
        if ($data['name']!='')
        {
            $likeCriteria = "(ar.description  LIKE '%" . $data['name'] . "%' or ar.reference_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_assembly'] !='')
        {
            $this->db->where('ar.id_assembly', $data['id_assembly']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('ar.status', $data['status']);
        }
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }

    function assemblyTeamListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('assembly_team');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function assemblyPendingList()
    {
        $this->db->select('DISTINCT(po.id) as id, po.*');
        $this->db->from('assembly_distribution as po');
        $this->db->join('assembly_distribution_details as pod','po.id = pod.id_assembly_distribution');
        $this->db->where('pod.balance_quantity >',0);
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }

    function getAssemblyDistribution($id)
    {
        $this->db->select('po.*, v.code as assembly_code, v.name as assembly_name');
        $this->db->from('assembly_distribution as po');;
        $this->db->join('assembly_team as v','po.id_assembly = v.id');
        $this->db->where('po.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAssemblyDistributionDetails($id_assembly_distribution)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('assembly_distribution_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_assembly_distribution', $id_assembly_distribution);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function addAssemblyReturn($data)
    {
        $this->db->trans_start();
        $this->db->insert('assembly_return', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addAssemblyReturnDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('assembly_return_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getAssemblyDistributionDetailById($id)
    {
        $this->db->select('*');
        $this->db->from('assembly_distribution_details');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function updateAssemblyDistributionDetail($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('assembly_distribution_details', $data);
        return TRUE;
    }

    function generateAssemblyReturnNumber()
    {
        $year = date('y');
        $Year = date('Y');
    
        $this->db->select('j.*');
        $this->db->from('assembly_return as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->row();
        $value = 0;
        if($result)
        {
            $data=$result->reference_number;
            $value=substr($data, 2,6);
        }
        $count=$value + 1;
        // echo "<Pre>";print_r($count);exit;

        $generated_number = "AR" .(sprintf("%'06d", $count)). "/" . $Year;
           
        return $generated_number;        
    }

    function updateAssemblyReturn($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('assembly_return', $data);
        return TRUE;        
    }

    function getAssemblyReturn($id)
    {
        $this->db->select('ar.*, po.reference_number as distribution_number, v.code as assembly_code, v.name as assembly_name');
        $this->db->from('assembly_return as ar');;
        $this->db->join('assembly_team as v','ar.id_assembly = v.id');
        $this->db->join('assembly_distribution as po','ar.id_assembly_distribution = po.id');
        $this->db->where('ar.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAssemblyReturnDetails($id_po)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('assembly_return_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_assembly_return', $id_po);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getProcurementItem($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_item');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function addProductQuantity($data)
    {
        $this->db->trans_start();
        $this->db->insert('product_quantity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateItem($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_item', $data);
        return TRUE;         
    }


    function procurementSubCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }



    function procurementSubCategoryListByProcurementId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_sub_category as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_category',$id);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementItemListBySubCategory($id_sub_category)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_sub_category',$id_sub_category);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    // function saveTempPODetails($data)
    // {
    //     $this->db->trans_start();
    //     $this->db->insert('temp_assembly_distribution_details', $data);
    //     $insert_id = $this->db->insert_id();
    //     $this->db->trans_complete();

    //     return $insert_id;
    // }

    // function getTempPODetailsByIdSessionForView($id_session)
    // {
    //     $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
    //     $this->db->from('temp_assembly_distribution_details as tpod');
    //     $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
    //     $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
    //     $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
    //     $this->db->where('tpod.id_session', $id_session);
    //      $query = $this->db->get();
    //      $result = $query->result();   
    //      return $result;
    // }

    // function getTempPODetailsByIdSession($id_session)
    // {
    //     $this->db->select('tpod.*');
    //     $this->db->from('temp_assembly_distribution_details as tpod');
    //     $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
    //     $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
    //     $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
    //     $this->db->where('tpod.id_session', $id_session);
    //      $query = $this->db->get();
    //      $result = $query->result();   
    //      return $result;
    // }

    // function deleteTempPoDetails($id)
    // {
    //    $this->db->where('id', $id);
    //    $this->db->delete('temp_assembly_distribution_details');
    //    return TRUE;
    // }

    // function deleteTempPoDetailsBySessionId($id_session)
    // {
    //    $this->db->where('id_session', $id_session);
    //    $this->db->delete('temp_assembly_distribution_details');
    //    return TRUE;
    // }

    // function moveTempToPoDetails($id_po)
    // {
    //     $id_session = $this->session->my_session_id;
    //     $temp_details = $this->getTempPODetailsByIdSession($id_session);

    //     foreach ($temp_details as $detail)
    //     {
    //         unset($detail->id);
    //         unset($detail->id_session);
    //         $detail->id_po = $id_po;

    //         $id_po_detail = $this->addPODetails($detail);
    //     }

    //     if($id_po_detail)
    //     {
    //         $id_po_detail = $this->deleteTempPoDetailsBySessionId($id_session);
    //     }

    //     return TRUE;
    // }
}