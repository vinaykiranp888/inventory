<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Assembly_distribution_model extends CI_Model
{   
    function procurementCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function procurementSubCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_sub_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function assemblyTeamListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('assembly_team');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getAssemblyDistributionListSearch($data)
    {
        $this->db->select('po.*, st.name as status_name, a.code as assembly_code, a.name as assembly_name');
        $this->db->from('assembly_distribution as po');
        $this->db->join('assembly_team as a','po.id_assembly = a.id');
        $this->db->join('status_table as st','po.status = st.id','left');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.description  LIKE '%" . $data['name'] . "%' or po.po_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_assembly'] !='')
        {
            $this->db->where('po.id_assembly', $data['id_assembly']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('po.status', $data['status']);
        }
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }

    function procurementSubCategoryListByProcurementId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_sub_category as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_category',$id);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementItemListBySubCategory($id_sub_category)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_sub_category',$id_sub_category);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function saveTempAssemblyDistributionDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_assembly_distribution_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempAssemblyDistributionDetailsByIdSessionForView($id_session)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('temp_assembly_distribution_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getTempAssemblyDistributionDetailsByIdSession($id_session)
    {
        $this->db->select('tpod.*');
        $this->db->from('temp_assembly_distribution_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function deleteTempPoDetails($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_assembly_distribution_details');
       return TRUE;
    }

    function deleteTempPoDetailsBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_assembly_distribution_details');
       return TRUE;
    }

    function generateAssemblyDistributionNumber()
    {
        $year = date('y');
        $Year = date('Y');
    
        $this->db->select('j.*');
        $this->db->from('assembly_distribution as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->row();
        $value = 0;
        if($result)
        {
            $data=$result->reference_number;
            $value=substr($data, 2,6);
            // $count= $result + 1;
        }
         // print_r($value);exit(); 
 
        $count=$value + 1;
        $generated_number = "AD" .(sprintf("%'06d", $count)). "/" . $Year;
           
        return $generated_number;        
    }

    function moveTempToAssemblyDistributionDetails($id_assembly_distribution)
    {
        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->getTempAssemblyDistributionDetailsByIdSession($id_session);

        foreach ($temp_details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);
            $detail->id_assembly_distribution = $id_assembly_distribution;

            $id_assembly_distribution_detail = $this->addAssemblyDistributionDetails($detail);

            if($id_assembly_distribution_detail)
            {

                $id_item = $detail->id_item;
                $quantity = $detail->quantity;

                $item = $this->getProductByProductId($id_item);
                if($item)
                {
                    $available_quantity = $item->quantity;
                    $updated_quantity = $available_quantity - $quantity;

                    $add_item_quantity_data = array(
                        'id_description' => 2,
                        'id_item' => $id_item,
                        'id_assembly' => $id_assembly_distribution,
                        'id_assembly_detail' => $id_assembly_distribution_detail,
                        'previous_quantity' => $available_quantity,
                        'grn_quantity' => 0,
                        'assembly_quantity' => $quantity,
                        'quantity' => $updated_quantity,
                        'status' => 1,
                        'created_by' => $id_user
                        );
                    
                    $id_product_quantity = $this->addProductQuantity($add_item_quantity_data);

                    if($id_product_quantity)
                    {
                        $update_item_data = array(
                            'quantity' => $updated_quantity
                        );

                        $updated_item = $this->updateProductItem($update_item_data,$id_item);

                    }
                }

            }

        }
        
        $id_assembly_distribution_delete = $this->deleteTempPoDetailsBySessionId($id_session);

        return TRUE;
    }

    function addAssemblyDistributionDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('assembly_distribution_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addAssemblyDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('assembly_distribution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getAssemblyDistribution($id)
    {
        $this->db->select('po.*, v.code as assembly_code, v.name as assembly_name');
        $this->db->from('assembly_distribution as po');;
        $this->db->join('assembly_team as v','po.id_assembly = v.id');
        $this->db->where('po.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getAssemblyDistributionDetails($id_assembly_distribution)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('assembly_distribution_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_assembly_distribution', $id_assembly_distribution);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getProductByProductId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
        $this->db->where('psc.id',$id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function addProductQuantity($data)
    {
        $this->db->trans_start();
        $this->db->insert('product_quantity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateProductItem($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_item', $data);
        return TRUE;
    }

    function getItemDuplicationInTempDetails($data)
    {
        $this->db->select('st.*');
        $this->db->from('temp_assembly_distribution_details as st');
        if($data['id_session'] != '')
        {
            $this->db->where('st.id_session', $data['id_session']);
        }
        if($data['id_item'] != '')
        {
            $this->db->where('st.id_item', $data['id_item']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}