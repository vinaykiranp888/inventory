<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Po_model extends CI_Model
{   
    function procurementCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function procurementSubCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_sub_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function vendorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('vendor_details');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getOrganisation()
    {
        $this->db->select('org.*, s.name as state_name');
        $this->db->from('organisation as org');
        $this->db->join('state as s','org.id_state = s.id');
        $query = $this->db->get();
        $sd = $query->row();
        return $sd;
    }

    function getPOListSearch($data)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, st.name as status_name');
        $this->db->from('purchase_order as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('status_table as st','po.status = st.id','left');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.description  LIKE '%" . $data['name'] . "%' or po.po_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_vendor'] !='')
        {
            $this->db->where('po.id_vendor', $data['id_vendor']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('po.status', $data['status']);
        }
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }

    function procurementSubCategoryListByProcurementId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_sub_category as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_category',$id);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementItemListBySubCategory($id_sub_category)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_sub_category',$id_sub_category);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function saveTempPODetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_purchase_order_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempPODetailsByIdSessionForView($id_session)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('temp_purchase_order_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getTempPODetailsByIdSession($id_session)
    {
        $this->db->select('tpod.*');
        $this->db->from('temp_purchase_order_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function deleteTempPoDetails($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_purchase_order_details');
       return TRUE;
    }

    function deleteTempPoDetailsBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_purchase_order_details');
       return TRUE;
    }

    function generatePONumber()
    {
        $year = date('y');
        $Year = date('Y');
    
        $this->db->select('j.*');
        $this->db->from('purchase_order as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->row();
        $value = 0;
        if($result)
        {
            $data=$result->po_number;
            $value=substr($data, 2,6);
            // $count= $result + 1;
        }
        
 
        $count=$value + 1;
        $generated_number = "PO" .(sprintf("%'06d", $count)). "/" . $Year;
           
        return $generated_number;        
    }

    function moveTempToPoDetails($id_po)
    {
        $id_session = $this->session->my_session_id;
        $temp_details = $this->getTempPODetailsByIdSession($id_session);

        foreach ($temp_details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);
            $detail->id_po = $id_po;

            $id_po_detail = $this->addPODetails($detail);
        }

        // if($id_po_detail)
        // {
            $id_po_detail = $this->deleteTempPoDetailsBySessionId($id_session);
        // }

        return TRUE;
    }

    function addPODetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('purchase_order_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addPO($data)
    {
        $this->db->trans_start();
        $this->db->insert('purchase_order', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getPo($id)
    {
        $this->db->select('po.*, v.code as vendor_code, v.name as vendor_name, v.address_one, v.address_two, v.city, sv.name as vendor_state, v.zipcode as vendor_zipcode, v.gst_in, v.phone, v.email');
        $this->db->from('purchase_order as po');;
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('state as sv','v.state = sv.id');
        $this->db->where('po.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getPoDetails($id_po)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code, pi.part_number, pak.name as package_name, pak.code as package_code');
        $this->db->from('purchase_order_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->join('packages as pak','pi.id_package = pak.id');
        $this->db->where('tpod.id_po', $id_po);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getProductByProductId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
        $this->db->where('psc.id',$id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getItemDuplicationInTempDetails($data)
    {
        $this->db->select('st.*');
        $this->db->from('temp_purchase_order_details as st');
        if($data['id_session'] != '')
        {
            $this->db->where('st.id_session', $data['id_session']);
        }
        if($data['id_item'] != '')
        {
            $this->db->where('st.id_item', $data['id_item']);
        }
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
}