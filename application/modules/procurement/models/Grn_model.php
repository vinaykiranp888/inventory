<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grn_model extends CI_Model
{   

    function getGRNListSearch($data)
    {
        $this->db->select('grn.*, po.po_number, v.name as vendor_name, v.code as vendor_code, st.name as status_name');
        $this->db->from('grn as grn');
        $this->db->join('vendor_details as v','grn.id_vendor = v.id');
        $this->db->join('purchase_order as po','grn.id_po = po.id');
        $this->db->join('status_table as st','grn.status = st.id','left');
        if ($data['name']!='')
        {
            $likeCriteria = "(grn.description  LIKE '%" . $data['name'] . "%' or grn.grn_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_vendor'] !='')
        {
            $this->db->where('grn.id_vendor', $data['id_vendor']);
        }
        if ($data['status'] !='')
        {
            $this->db->where('grn.status', $data['status']);
        }
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }

    function vendorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('vendor_details');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function poPendingList()
    {
        $this->db->select('DISTINCT(po.id) as id, po.*');
        $this->db->from('purchase_order as po');
        $this->db->join('purchase_order_details as pod','po.id = pod.id_po');
        $this->db->where('pod.balance_quantity >',0);
        $query = $this->db->get();
        $result = $query->result();
        
        return $result;
    }

    function getPo($id)
    {
        $this->db->select('po.*, v.code as vendor_code, v.name as vendor_name');
        $this->db->from('purchase_order as po');;
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->where('po.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getPoDetails($id_po)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('purchase_order_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_po', $id_po);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function addGrn($data)
    {
        $this->db->trans_start();
        $this->db->insert('grn', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addGrnDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('grn_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getPoDetailById($id)
    {
        $this->db->select('*');
        $this->db->from('purchase_order_details');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function updatePODetail($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('purchase_order_details', $data);
        return TRUE;
    }

    function generateGRNNumber()
    {
        $year = date('y');
        $Year = date('Y');
    
        $this->db->select('j.*');
        $this->db->from('grn as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->row();
        $value = 0;
        if($result)
        {
            $data=$result->grn_number;
            $value=substr($data, 2,6);
        }
        $count=$value + 1;
        // echo "<Pre>";print_r($count);exit;

        $generated_number = "GR" .(sprintf("%'06d", $count)). "/" . $Year;
           
        return $generated_number;        
    }

    function updateGrn($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('grn', $data);
        return TRUE;        
    }

    function getGRN($id)
    {
        $this->db->select('grn.*, po.po_number, v.code as vendor_code, v.name as vendor_name');
        $this->db->from('grn as grn');;
        $this->db->join('vendor_details as v','grn.id_vendor = v.id');
        $this->db->join('purchase_order as po','grn.id_po = po.id');
        $this->db->where('grn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGrnDetails($id_po)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('grn_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_grn', $id_po);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getProcurementItem($id)
    {
        $this->db->select('*');
        $this->db->from('procurement_item');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function addProductQuantity($data)
    {
        $this->db->trans_start();
        $this->db->insert('product_quantity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateItem($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('procurement_item', $data);
        return TRUE;         
    }


    function procurementSubCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }



    function procurementSubCategoryListByProcurementId($id)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_sub_category as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_category',$id);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function procurementItemListBySubCategory($id_sub_category)
    {
        $this->db->select('psc.*');
        $this->db->from('procurement_item as psc');
        $this->db->where('psc.status',1);
        $this->db->where('psc.id_procurement_sub_category',$id_sub_category);
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function saveTempPODetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_purchase_order_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempPODetailsByIdSessionForView($id_session)
    {
        $this->db->select('tpod.*, pc.name as category_name, pc.code as category_code, psc.name as sub_category_name, psc.code as sub_category_code, pi.name as item_name, pi.code as item_code');
        $this->db->from('temp_purchase_order_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getTempPODetailsByIdSession($id_session)
    {
        $this->db->select('tpod.*');
        $this->db->from('temp_purchase_order_details as tpod');
        $this->db->join('procurement_category as pc','tpod.id_category = pc.id');
        $this->db->join('procurement_sub_category as psc','tpod.id_sub_category = psc.id');
        $this->db->join('procurement_item as pi','tpod.id_item = pi.id');
        $this->db->where('tpod.id_session', $id_session);
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function deleteTempPoDetails($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_purchase_order_details');
       return TRUE;
    }

    function deleteTempPoDetailsBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_purchase_order_details');
       return TRUE;
    }

    function moveTempToPoDetails($id_po)
    {
        $id_session = $this->session->my_session_id;
        $temp_details = $this->getTempPODetailsByIdSession($id_session);

        foreach ($temp_details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);
            $detail->id_po = $id_po;

            $id_po_detail = $this->addPODetails($detail);
        }

        if($id_po_detail)
        {
            $id_po_detail = $this->deleteTempPoDetailsBySessionId($id_session);
        }

        return TRUE;
    }
}