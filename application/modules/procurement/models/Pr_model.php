<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pr_model extends CI_Model
{
    
    function prList($id) {
        $this->db->select('*');
        $this->db->from('pr_entry');
        $this->db->where('status', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getList()
    {
        $this->db->select('pe.*');
        $this->db->from('pr_entry as pe');
        $query = $this->db->get();
        return $query->result();

    }

    function getPRListForProcess($process)
    {
        $this->db->select('pe.*');
        $this->db->from('pr_entry as pe');
        $this->db->where('status', '1');
        $this->db->where('is_po', '0');
        $this->db->where('type_of_pr', $process);
        $query = $this->db->get();
        return $query->result();
    }

    function getPRList() {
        $this->db->select('pe.*, dep.name as department, fy.name as financial_year, vd.name as vendor');
        $this->db->from('pr_entry as pe');
        $this->db->join('department as dep', 'pe.id_department = dep.id');
        $this->db->join('financial_year as fy', 'pe.id_financial_year = fy.id');
        $this->db->join('vendor_details as vd', 'pe.id_vendor = vd.id');
        $query = $this->db->get();
        return $query->result();
    }

    function getPRListByStatus($status) {
        $this->db->select('pe.*, dep.code as department_code, dep.name as department, fy.name as financial_year, vd.name as vendor');
        $this->db->from('pr_entry as pe');
        $this->db->join('department as dep', 'pe.id_department = dep.id');
        $this->db->join('financial_year as fy', 'pe.id_financial_year = fy.id');
        $this->db->join('vendor_details as vd', 'pe.id_vendor = vd.id');
        $this->db->where('pe.status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getMasterPrDetails($id)
    {
        // echo "<Pre>";print_r($id);exit;
         $this->db->select('pe.*, dep.code as department_code, dep.name as department, fy.name as financial_year, bty.name as budget_year, vd.name as vendor, vd.code as vendor_code');
        $this->db->from('pr_entry as pe');
        $this->db->join('department_code as dep', 'pe.department_code = dep.code');
        $this->db->join('financial_year as fy', 'pe.id_financial_year = fy.id');
        $this->db->join('budget_year as bty', 'pe.id_budget_year = bty.id');
        $this->db->join('vendor_details as vd', 'pe.id_vendor = vd.id');
        $this->db->where('pe.id', $id);
        $query = $this->db->get();
        
        return $query->row();
    }

     function getPrDetails($id)
     {
         $this->db->select('pe.*');
        $this->db->from('pr_entry as pe');
        $this->db->where('pe.id', $id);
        $query = $this->db->get();
        $data = $query->row();

        $type = $data->type;

        // echo "<Pre>";print_r($type);exit();





         $this->db->select('tpe.*, pc.description as category_name, pc.code as category_code, psc.description as sub_category_name, psc.code as sub_category_code,  pi.description as item_name, pi.code as item_code, tx.code as tax_code, tx.name as tax_name');
        // $this->db->from('pr_detail');
        $this->db->from('pr_entry_details as tpe');
        $this->db->join('tax as tx', 'tpe.id_tax = tx.id');

        if($type == 'Procurement')
        {
            // echo "<Pre>";print_r('P');exit();
            $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id');
        }elseif($type == 'Asset')
        {   
            // echo "<Pre>";print_r('A');exit();

            $this->db->join('asset_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('asset_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('asset_item as pi', 'tpe.id_item = pi.id');

        }

        $this->db->where('id_pr_entry', $id);
        // $this->db->where('id_pr', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function generatePRNumber()
    {

        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('pr_entry');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;

           $generated_number = "PRE" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function addPR($data) {
        $this->db->trans_start();
        $this->db->insert('pr_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewPRDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('pr_entry_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteFullSessionData($sessionid) {
        $this->db->where('sessionid', $sessionid);
        $this->db->delete('temp_pr_entry');
        return TRUE;
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_pr_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_pr_entry', $data);
        return TRUE;
    }

    function getTempDetails($id){
        $this->db->select('*');
        $this->db->from('temp_pr_entry');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getTempDetailsFromPR($sessionid)
    {
        $this->db->select('*');
        $this->db->from('temp_pr_entry');
        $this->db->where('sessionid', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempPRDetails($sessionid,$type)
    {
            // echo "<Pre>";print_r($type);exit();

        $this->db->select('tpe.*, pc.description as category_name, psc.description as sub_category_name,  pi.description as item_name, tx.code as tax_code, tx.name as tax_name');
        $this->db->from('temp_pr_entry as tpe');

        if($type == 'Procurement')
        {
            // echo "<Pre>";print_r('P');exit();
            $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id');
        }elseif($type == 'Asset')
        {   
            // echo "<Pre>";print_r('A');exit();

            $this->db->join('asset_category as pc', 'tpe.id_category = pc.id');
            $this->db->join('asset_sub_category as psc', 'tpe.id_sub_category = psc.id');
            $this->db->join('asset_item as pi', 'tpe.id_item = pi.id');

        }

        // $this->db->join('procurement_category as pc', 'tpe.id_category = pc.id','left');
        // $this->db->join('procurement_sub_category as psc', 'tpe.id_sub_category = psc.id','left');
        // $this->db->join('procurement_item as pi', 'tpe.id_item = pi.id','left');
        $this->db->join('tax as tx', 'tpe.id_tax = tx.id');
        $this->db->where('tpe.sessionid', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function getPRDetailsByMasterID($id_pr)
    {
        $this->db->select('tpe.*');
        $this->db->from('pr_entry_details as tpe');
        $this->db->where('tpe.id_pr_entry', $id_pr);
        $query = $this->db->get();
        return $query->result();
    }

    function getPRDetailsByID($id)
    {
        $this->db->select('tpe.*');
        $this->db->from('pr_entry_details as tpe');
        $this->db->where('tpe.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteTempData($id) {
        $this->db->where('id', $id);
        $this->db->delete('temp_pr_entry');
        return TRUE;
    }

    function getAccountCodeList()
    {
        $this->db->select('*');
        $this->db->from('account_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getActivityCodeList()
    {
        $this->db->select('*');
        $this->db->from('activity_code');
        $this->db->where('level', '3');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getDepartmentCodeList()
    {
        $this->db->select('*');
        $this->db->from('department_code');
         $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFundCodeList()
    {
        $this->db->select('*');
        $this->db->from('fund_code');
        $this->db->where('status', '1');
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function departmentListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
         $this->db->where('status', $status);
        $this->db->order_by("code", "ASC");
        $query = $this->db->get();
        return $query->result();

    }

    function financialYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('financial_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function budgetYearListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('budget_year');
         $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function prCategoryList()
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function prSubCategoryList()
    {
        $this->db->select('*');
        $this->db->from('procurement_sub_category');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function prItemList()
    {
        $this->db->select('*');
        $this->db->from('procurement_item');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function editPRList($data,$id)
    {
      $this->db->where_in('id', $id);
      $this->db->update('pr_entry', $data);
      return TRUE;
  }

    function departmentList()
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();

    }

    function vendorListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('vendor_details');
        // $this->db->where('status', '1');
        $this->db->where('vendor_status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getPRListSearch($data)
    {
        $this->db->select('po.*, v.name as vendor_name, v.code as vendor_code, fy.year as financial_year, fy.name as financial_name, d.name as department_name, d.code as department_code, bty.name as budget_year');
        $this->db->from('pr_entry as po');
        $this->db->join('vendor_details as v','po.id_vendor = v.id');
        $this->db->join('financial_year as fy','po.id_financial_year = fy.id');
        $this->db->join('budget_year as bty','po.id_budget_year = bty.id');
        $this->db->join('department_code as d','po.department_code = d.code');
        if ($data['name']!='')
        {
            $likeCriteria = "(po.description  LIKE '%" . $data['name'] . "%' or po.reason  LIKE '%" . $data['name'] . "%' or po.pr_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type_of_pr']!='')
        {
            $this->db->where('po.type_of_pr', $data['type_of_pr']);
        }
        if ($data['department_code'] !='')
        {
            $this->db->where('po.department_code', $data['department_code']);
        }
        if ($data['id_vendor'] !='')
        {
            $this->db->where('po.id_vendor', $data['id_vendor']);
        }
        if ($data['id_financial_year']!='')
        {
            $this->db->where('po.id_financial_year', $data['id_financial_year']);
        }
        if ($data['id_budget_year']!='')
        {
            $this->db->where('po.id_budget_year', $data['id_budget_year']);
        }
        if ($data['status'] !='')
        {
         // echo "<Pre>";print_r($data);exit();     

            $this->db->where('po.status', $data['status']);
        }
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }
    
    function assetCategoryByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('asset_category');
         $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    
    function procurementCategoryByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('procurement_category');
         $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function assetSubCategoryByCategory($id_cat)
    {
        $this->db->select('*');
        $this->db->from('asset_sub_category');
         $this->db->where('id_asset_category', $id_cat);
        $query = $this->db->get();
        return $query->result();
    }

    function procurementSubCategoryByCategory($id_cat)
    {
        $this->db->select('*');
        $this->db->from('procurement_sub_category');
         $this->db->where('id_procurement_category', $id_cat);
        $query = $this->db->get();
        return $query->result();
    }

    function assetItemBySubCategory($id_cat,$id_sub_cat)
    {
        $this->db->select('*');
        $this->db->from('asset_item');
         $this->db->where('id_asset_category', $id_cat);
         $this->db->where('id_asset_sub_category', $id_sub_cat);
        $query = $this->db->get();
        return $query->result();
    }

    function procurementItemBySubCategory($id_cat,$id_sub_cat)
    {
        $this->db->select('*');
        $this->db->from('procurement_item');
         $this->db->where('id_procurement_category', $id_cat);
         $this->db->where('id_procurement_sub_category', $id_sub_cat);
        $query = $this->db->get();
        return $query->result();
    }

    function getTaxList()
    {
        $this->db->select('*');
        $this->db->from('tax');
         $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getTaxRow($id)
    {
        $this->db->select('*');
        $this->db->from('tax');
         $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function updatePR($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('pr_entry', $data);
        return TRUE;
    }


     function getDebitBudget($data)
    {
        $this->db->select('ba.*, bty.name as budget_year, d.name as department_name, d.code as department');
        $this->db->from('budget_amount as ba');
        $this->db->join('budget_year as bty','ba.id_budget_year = bty.id');
        $this->db->join('department_code as d','ba.department_code = d.code');
        $this->db->where('ba.id_financial_year', $data['id_dt_financial_year']);
        $this->db->where('ba.department_code', $data['id_dt_department']);
        $this->db->where('ba.id_budget_year', $data['id_dt_budget_year']);
        $this->db->where('ba.status', '1');
        $query = $this->db->get();
        return $query->row();
    }

     function getBudgetAllocationByBudgetAmount($id_budget_amount)
    {
        $this->db->select('DISTINCT(ba.fund_code) as fund_code');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->where('ba.id_budget_amount', $id_budget_amount);
        $query = $this->db->get();
        return $query->result();
    }

     function getFundNameByCode($code)
    {
        $this->db->select('ba.name as fund_name');
        $this->db->from('fund_code as ba');
        $this->db->where('ba.code', $code);
        $query = $this->db->get();
        return $query->row();
    }


    function getBudgetAllocationByBudgetAmounNFund($data)
    {
        $this->db->select('DISTINCT(ba.activity_code) as activity_code');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->where('ba.id_budget_amount', $data['id_budget_amount']);
        $this->db->where('ba.fund_code', $data['dt_fund_code']);
        $query = $this->db->get();
        return $query->result();
    }

     function getActivityNameByCode($code)
    {
        $this->db->select('ba.name as activity_name');
        $this->db->from('activity_code as ba');
        $this->db->where('ba.code', $code);
        $query = $this->db->get();
        return $query->row();
    }


    function getBudgetAllocationByBudgetAmounNFundNActivity($data)
    {
        $this->db->select('DISTINCT(ba.account_code) as account_code');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->where('ba.id_budget_amount', $data['id_budget_amount']);
        $this->db->where('ba.fund_code', $data['dt_fund_code']);
        $this->db->where('ba.activity_code', $data['dt_activity_code']);
        $query = $this->db->get();
        return $query->result();
    }

     function getAccountNameByCode($code)
    {
        $this->db->select('ba.name as account_name');
        $this->db->from('account_code as ba');
        $this->db->where('ba.code', $code);
        $query = $this->db->get();
        return $query->row();
    }
    

    function getBudgetAllocationByCodes($data)
    {
         $this->db->select('ba.*, fc.name as fund_name, ac.name as account_name, actc.name as activity_name');
        $this->db->from('budget_allocation as ba');
        $this->db->join('fund_code as fc','ba.fund_code = fc.code');
        $this->db->join('account_code as ac','ba.account_code = ac.code');
        $this->db->join('activity_code as actc','ba.activity_code = actc.code');
        $this->db->where('ba.id_budget_amount', $data['dt_id_budget_amount']);
        $this->db->where('ba.fund_code', $data['dt_fund_code']);
        $this->db->where('ba.activity_code', $data['dt_activity_code']);
        $this->db->where('ba.account_code', $data['dt_account_code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkBudgetAmountExceeds()
    {
        $id_session = $this->session->my_session_id;

        $this->db->select('DISTINCT(tpd.id_budget_allocation) as id_budget_allocation');
        $this->db->from('temp_pr_entry as tpd');
        $this->db->where('tpd.sessionid', $id_session);
        $query = $this->db->get();
        $budget_allocation_datas = $query->result();
        
            // echo "<Pre>";print_r($id_budget_allocation);exit();
        // echo "<Pre>";print_r($budget_allocation_datas[1]);exit();

        foreach ($budget_allocation_datas as $budget_allocation_data)
        {
            $id_budget_allocation = $budget_allocation_data->id_budget_allocation;
            // echo "<Pre>";print_r($id_budget_allocation);exit();

            $budget_allocation_from_temp_datas = $this->getTempPRDetailByIdBudgetAllocation($id_budget_allocation);
            $budget_allocation_from_budget = $this->getBudgetAllocation($id_budget_allocation);
            $allocated_balance_amount = $budget_allocation_from_budget->balance_amount;
            // echo "<Pre>";print_r($allocated_balance_amount);exit();

            $total_temp_pr_detail = 0;
            foreach ($budget_allocation_from_temp_datas as $budget_allocation_from_temp_data)
            {
               $total_temp_pr_detail = $total_temp_pr_detail + $budget_allocation_from_temp_data->total_final;
            }

            if($total_temp_pr_detail > $allocated_balance_amount)
            {
                echo "<Pre>";print_r($id_budget_allocation . " - ".$total_temp_pr_detail . " - ".$allocated_balance_amount);exit();
                echo "0";exit();

            }
            else
            {

            }
        }
        echo "1";exit();

    }

    function getTempPRDetailByIdBudgetAllocation($id_budget_allocation)
    {
        $id_session = $this->session->my_session_id;
         $this->db->select('tpd.*');
        $this->db->from('temp_pr_entry as tpd');
        $this->db->where('tpd.id_budget_allocation', $id_budget_allocation);
        $this->db->where('tpd.sessionid', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getBudgetAllocation($id)
    {
        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function updateBudgetAllocationAmount($data)
    {
        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id', $data['id_budget_allocation']);
        $query = $this->db->get();
        $budget_data = $query->row();


        $budget_old_balance_amount = $budget_data->balance_amount;
        $budget_old_used_amount = $budget_data->used_amount;
        $id_budget_allocation = $budget_data->id;

        $budget_new_balance_amount = $budget_old_balance_amount - $data['total_final'];
        $budget_new_used_amount = $budget_old_used_amount + $data['total_final'];

        $updated_data['balance_amount'] = $budget_new_balance_amount;
        $updated_data['used_amount'] = $budget_new_used_amount;
        $updated = $this->updateBudgetAllocation($updated_data,$id_budget_allocation);
        // echo "<Pre>";print_r($updated);exit();

    }


    function updateBudgetAllocationAmountOnReject($data)
    {
        $this->db->select('ba.*');
        $this->db->from('budget_allocation as ba');
        $this->db->where('ba.id', $data['id_budget_allocation']);
        $query = $this->db->get();
        $budget_data = $query->row();


        $budget_old_balance_amount = $budget_data->balance_amount;
        $budget_old_used_amount = $budget_data->used_amount;
        $id_budget_allocation = $budget_data->id;

        $budget_new_balance_amount = $budget_old_balance_amount + $data['total_final'];
        $budget_new_used_amount = $budget_old_used_amount - $data['total_final'];

        $updated_data['balance_amount'] = $budget_new_balance_amount;
        $updated_data['used_amount'] = $budget_new_used_amount;
        // echo "<Pre>";print_r($updated_data);exit();
        $updated = $this->updateBudgetAllocation($updated_data,$id_budget_allocation);

    }

    function updateBudgetAllocation($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_allocation', $data);
        return TRUE;
    }
}
