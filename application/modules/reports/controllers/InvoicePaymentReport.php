<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvoicePaymentReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_payment_report_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('invoice_payment_report.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
            $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = $this->security->xss_clean($this->input->post('type'));
            
            $data['searchParam'] = $formData;

            // $data['invoice_payment_reportList'] = $this->invoice_payment_report_model->invoice_payment_reportList();

            $data['invoice_payment_reportList'] = $this->invoice_payment_report_model->invoiceCollectionReportListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : Corporate Company List';
            $this->loadViews("invoice_payment_report/approval_list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('invoice_payment_report.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {                

                // For file validation from 36 to 44 , file size validationss
                // echo "<Pre>";print_r($this->input->post());exit;


                // echo "<Pre>"; print_r($_FILES['image']);exit;
                // if($_FILES['image'])
                // {

                //     $certificate_name = $_FILES['image']['name'];
                //     $certificate_size = $_FILES['image']['size'];
                //     $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                //     // echo "<Pre>"; print_r($certificate_tmp);exit();

                //     $certificate_ext=explode('.',$certificate_name);
                //     $certificate_ext=end($certificate_ext);
                //     $certificate_ext=strtolower($certificate_ext);


                //     $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                //     $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // }



                $level = $this->security->xss_clean($this->input->post('level'));
                $id_parent_invoice_payment_report = $this->security->xss_clean($this->input->post('id_parent_invoice_payment_report'));
                $registration_number = $this->security->xss_clean($this->input->post('registration_number'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
                $website = $this->security->xss_clean($this->input->post('website'));
                $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $ceo = $this->security->xss_clean($this->input->post('ceo'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $country_code = $this->security->xss_clean($this->input->post('country_code'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $corporate_membership_status = $this->security->xss_clean($this->input->post('corporate_membership_status'));
                $staff_strength = $this->security->xss_clean($this->input->post('staff_strength'));
                $debtor_code = $this->security->xss_clean($this->input->post('debtor_code'));
                $credit_term = $this->security->xss_clean($this->input->post('credit_term'));
                $control_account = $this->security->xss_clean($this->input->post('control_account'));
                $credit_term_code = $this->security->xss_clean($this->input->post('credit_term_code'));
                $primary_contact_name = $this->security->xss_clean($this->input->post('primary_contact_name'));
                $primary_contact_designation = $this->security->xss_clean($this->input->post('primary_contact_designation'));
                $primary_contact_email = $this->security->xss_clean($this->input->post('primary_contact_email'));
                $primary_country_code = $this->security->xss_clean($this->input->post('primary_country_code'));
                $primary_phone = $this->security->xss_clean($this->input->post('primary_phone'));
                $second_contact_name = $this->security->xss_clean($this->input->post('second_contact_name'));
                $second_contact_designation = $this->security->xss_clean($this->input->post('second_contact_designation'));
                $second_contact_email = $this->security->xss_clean($this->input->post('second_contact_email'));
                $second_country_code = $this->security->xss_clean($this->input->post('second_country_code'));
                $second_phone = $this->security->xss_clean($this->input->post('second_phone'));
                $staff_credit = $this->security->xss_clean($this->input->post('staff_credit'));
                $status = $this->security->xss_clean($this->input->post('status'));



            
                $data = array(
                    'level' => $level,
                    'id_parent_invoice_payment_report' => $id_parent_invoice_payment_report,
                    'registration_number' => $registration_number,
                    'name' => $name,
                    'password' => md5($password),
                    'joined_date' => date('Y-m-d', strtotime($joined_date)),
                    'website' => $website,
                    'institution_type' => $institution_type,
                    'chairman' => $chairman,
                    'ceo' => $ceo,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'country_code' => $country_code,
                    'phone' => $phone,
                    'corporate_membership_status' => $corporate_membership_status,
                    'staff_strength' => $staff_strength,
                    'debtor_code' => $debtor_code,
                    'credit_term' => $credit_term,
                    'control_account' => $control_account,
                    'credit_term_code' => $credit_term_code,
                    'primary_contact_name' => $primary_contact_name,
                    'primary_contact_designation' => $primary_contact_designation,
                    'primary_contact_email' => $primary_contact_email,
                    'primary_country_code' => $primary_country_code,
                    'primary_phone' => $primary_phone,
                    'second_contact_name' => $second_contact_name,
                    'second_contact_designation' => $second_contact_designation,
                    'second_contact_email' => $second_contact_email,
                    'second_country_code' => $second_country_code,
                    'second_phone' => $second_phone,
                    'staff_credit' => $staff_credit,
                    'status' => $status
                );

                // echo "<Pre>"; print_r($image_file);exit;

                // if($image_file)
                // {
                //     $data['image'] = $image_file;
                // }

                $inserted_id = $this->invoice_payment_report_model->addNewProgrammeDetails($data);
                redirect('/corporate/invoice_payment_report/list');
            }
            // else
            // {
            //     $this->invoice_payment_report_model->deleteTempProgHasDeanDataBySession($id_session);
            // }

            $data['countryList'] = $this->invoice_payment_report_model->countryListByStatus('1');
            $data['parentCompanyList'] = $this->invoice_payment_report_model->parentCompanyList('Parent');
            $data['statusListByType'] = $this->invoice_payment_report_model->statusListByType('Company');

            // echo "<Pre>";print_r($data['partnerCategoryList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Programme';
            $this->loadViews("invoice_payment_report/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('invoice_payment_report.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/corporate/invoice_payment_reportApproval/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $level = $this->security->xss_clean($this->input->post('level'));
                $id_parent_invoice_payment_report = $this->security->xss_clean($this->input->post('id_parent_invoice_payment_report'));
                $registration_number = $this->security->xss_clean($this->input->post('registration_number'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
                $website = $this->security->xss_clean($this->input->post('website'));
                $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $ceo = $this->security->xss_clean($this->input->post('ceo'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $country_code = $this->security->xss_clean($this->input->post('country_code'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $corporate_membership_status = $this->security->xss_clean($this->input->post('corporate_membership_status'));
                $staff_strength = $this->security->xss_clean($this->input->post('staff_strength'));
                $debtor_code = $this->security->xss_clean($this->input->post('debtor_code'));
                $credit_term = $this->security->xss_clean($this->input->post('credit_term'));
                $control_account = $this->security->xss_clean($this->input->post('control_account'));
                $credit_term_code = $this->security->xss_clean($this->input->post('credit_term_code'));
                $primary_contact_name = $this->security->xss_clean($this->input->post('primary_contact_name'));
                $primary_contact_designation = $this->security->xss_clean($this->input->post('primary_contact_designation'));
                $primary_contact_email = $this->security->xss_clean($this->input->post('primary_contact_email'));
                $primary_country_code = $this->security->xss_clean($this->input->post('primary_country_code'));
                $primary_phone = $this->security->xss_clean($this->input->post('primary_phone'));
                $second_contact_name = $this->security->xss_clean($this->input->post('second_contact_name'));
                $second_contact_designation = $this->security->xss_clean($this->input->post('second_contact_designation'));
                $second_contact_email = $this->security->xss_clean($this->input->post('second_contact_email'));
                $second_country_code = $this->security->xss_clean($this->input->post('second_country_code'));
                $second_phone = $this->security->xss_clean($this->input->post('second_phone'));
                $staff_credit = $this->security->xss_clean($this->input->post('staff_credit'));
                $status = $this->security->xss_clean($this->input->post('status'));



            
                $data = array(
                    'level' => $level,
                    'id_parent_invoice_payment_report' => $id_parent_invoice_payment_report,
                    'registration_number' => $registration_number,
                    'name' => $name,
                    'joined_date' => date('Y-m-d', strtotime($joined_date)),
                    'website' => $website,
                    'institution_type' => $institution_type,
                    'chairman' => $chairman,
                    'ceo' => $ceo,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'country_code' => $country_code,
                    'phone' => $phone,
                    'corporate_membership_status' => $corporate_membership_status,
                    'staff_strength' => $staff_strength,
                    'debtor_code' => $debtor_code,
                    'credit_term' => $credit_term,
                    'control_account' => $control_account,
                    'credit_term_code' => $credit_term_code,
                    'primary_contact_name' => $primary_contact_name,
                    'primary_contact_designation' => $primary_contact_designation,
                    'primary_contact_email' => $primary_contact_email,
                    'primary_country_code' => $primary_country_code,
                    'primary_phone' => $primary_phone,
                    'second_contact_name' => $second_contact_name,
                    'second_contact_designation' => $second_contact_designation,
                    'second_contact_email' => $second_contact_email,
                    'second_country_code' => $second_country_code,
                    'second_phone' => $second_phone,
                    'staff_credit' => $staff_credit,
                    'status' => $status
                );

                // echo "<Pre>";print_r($data);exit;
                
                $result = $this->invoice_payment_report_model->editProgrammeDetails($data,$id);
                redirect('/corporate/invoice_payment_report/list');
            }

            $data['id_invoice_payment_report'] = $id;
            
            $data['countryList'] = $this->invoice_payment_report_model->countryListByStatus('1');
            $data['invoice_payment_reportUserRoleList'] = $this->invoice_payment_report_model->invoice_payment_reportUserRoleListByStatus('1');
            $data['parentCompanyList'] = $this->invoice_payment_report_model->parentCompanyList('Parent');
            $data['statusListByType'] = $this->invoice_payment_report_model->statusListByType('Company');

            $data['invoice_payment_reportDetails'] = $this->invoice_payment_report_model->getCompanyDetails($id);
            $data['invoice_payment_reportUsersList'] = $this->invoice_payment_report_model->getCompanyUsersList($id);
            
            // echo "<Pre>";print_r($data['statusListByType']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Company';
            $this->loadViews("invoice_payment_report/edit", $this->global, $data, NULL);
        }
    }
}