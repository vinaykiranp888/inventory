<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Credit_note_report_model extends CI_Model
{
    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function creditNoteReportListSearch($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('credit_note as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.created_dt_tm >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.created_dt_tm <=', $data['to_date']);
        }
        $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('r.*');
        $this->db->from('country as r');
        $this->db->where('r.status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCreditNote($id)
    {
        $this->db->select('cn.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('credit_note as cn');
        $this->db->join('programme as p', 'cn.id_program = p.id','left');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getInvoice($id)
    {
        $this->db->select('mi.*, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function getCompanyDetails($id)
    {
        $this->db->select('mi.*');
        $this->db->from('company as mi');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function getStudentData($id_student)
    {
        $this->db->select('*');
        $this->db->from('student');
         $this->db->where('id', $id_student);
         $query = $this->db->get();
         $result = $query->row(); 
         return $result;
    }

    function creditNoteTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('credit_note_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function downloadCreitNoteInvoiceReportCSVBtwnDates($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('credit_note as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.created_dt_tm >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.created_dt_tm <=', $data['to_date']);
        }
        $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}

