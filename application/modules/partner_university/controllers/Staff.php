<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Staff extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_model');
        $this->isPartnerUniversityLoggedIn();
    }

    function list()
    {
        $id_partner_university = $this->session->id_partner_university;

        $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_partner_university'] = $id_partner_university;

        $data['searchParam'] = $formData;
        $data['departmentList'] = $this->staff_model->getDepartmentByStatus('1');
        // echo "<Pre>";print_r($data['countryList']);exit;
        // $data['departmentList'] = $this->staff_model->getStateByStatus('1');
        $data['staffDetails'] = $this->staff_model->staffListSearch($formData);
        $this->global['pageTitle'] = 'Inventory Management : Staff List';
        $this->loadViews("staff/list", $this->global, $data, NULL);
    }

    
    function add()
    {
        $id_partner_university = $this->session->id_partner_university;
        $id_session = $this->session->my_partner_university_session_id;
        

        if($this->input->post())
        {
            
            // echo "<Pre>";print_r($this->input->post());exit;


            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $ic_no = $this->security->xss_clean($this->input->post('ic_no'));
            $staff_id = $this->security->xss_clean($this->input->post('staff_id'));
            $address = $this->security->xss_clean($this->input->post('address'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $address_two = $this->security->xss_clean($this->input->post('address_two'));
            $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
            $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
            $id_country = $this->security->xss_clean($this->input->post('id_country'));
            $id_state = $this->security->xss_clean($this->input->post('id_state'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $email = $this->security->xss_clean($this->input->post('email')); 
            $job_type = $this->security->xss_clean($this->input->post('job_type'));
            $id_department = $this->security->xss_clean($this->input->post('id_department'));
            $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
            $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
            $dob = $this->security->xss_clean($this->input->post('dob'));
            $academic_type = $this->security->xss_clean($this->input->post('academic_type'));
            $id_type = $this->security->xss_clean($this->input->post('id_type'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
            $status = $this->security->xss_clean($this->input->post('status'));

            $whatsapp_number = $this->security->xss_clean($this->input->post('whatsapp_number'));
            $linked_in = $this->security->xss_clean($this->input->post('linked_in'));
            $facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
            $twitter_id = $this->security->xss_clean($this->input->post('twitter_id'));
            $ig_id = $this->security->xss_clean($this->input->post('ig_id'));


            if($_FILES['image'])
            {

                $certificate_name = $_FILES['image']['name'];
                $certificate_size = $_FILES['image']['size'];
                $certificate_tmp =$_FILES['image']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


                 
            $salutationInfo = $this->staff_model->getSalutation($salutation);


            $data = array(
                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                'id_type' => $id_type,
                'nationality' => $nationality,
                'ic_no' => $ic_no,
                'staff_id' => $staff_id,
                'gender' => $gender,
                'mobile_number' => $mobile_number,
                'phone_number' => $phone_number,
                'id_country' => $id_country,
                'id_state' => $id_state,
                'zipcode' => $zipcode,
                'email' => $email,
                'address' => $address,
                'address_two' => $address_two,
                'job_type' => $job_type,
                'id_department' => $id_department,
                'id_faculty_program' => $id_faculty_program,
                'id_university' => $id_partner_university,
                'dob' => date('Y-m-d',strtotime($dob)),
                'joined_date' => date('Y-m-d',strtotime($joined_date)),
                'academic_type' => $academic_type,
                'id_education_level' => $id_education_level,
                'whatsapp_number' => $whatsapp_number,
                'linked_in' => $linked_in,
                'facebook_id' => $facebook_id,
                'twitter_id' => $twitter_id,
                'ig_id' => $ig_id,
                'status' => $status
            );

            $duplicate_row = $this->staff_model->checkStaffDuplication($data);

            if($duplicate_row)
            {
                echo "Duplicate Staff Data Not Allowed Check Mobile Number, IC No & Staff ID";exit();
            }

            if($image_file)
            {
                $data['image'] = $image_file;
            }

            $inserted_id = $this->staff_model->addNewStaff($data);

            $details = $this->staff_model->getTempStaff($id_session);
             for($i=0;$i<count($details);$i++)
             {
                $id_course = $details[$i]->id_course;

                 $detailsData = array(
                    'id_staff' => $inserted_id,
                    'id_course' => $id_course,
                );
                //print_r($details);exit;
                $result = $this->staff_model->addNewStaffCourse($detailsData);
             }

            $this->staff_model->deleteTempDataBySession($id_session);
            redirect('/partner_university/staff/list');
        }
        else
        {
            $this->staff_model->deleteTempDataBySession($id_session);
        }
        
        // $type = array();
        // $type_data['type'] = 'Permanent';
        // $type_data1['type'] = 'Contract';
        // array_push($type, $type_data);
        // array_push($type, $type_data1);
        // $data['staffType'] = $type;

        $data['countryList'] = $this->staff_model->getCountryByStatus('1');
        $data['departmentList'] = $this->staff_model->getDepartmentByStatus('1');
        $data['facultyProgramList'] = $this->staff_model->getFacultyProgramListByStatus('1');
        $data['salutationList'] = $this->staff_model->salutationListByStatus('1');
        $data['qualificationList'] = $this->staff_model->qualificationListByStatus('1');
        $data['nationalityList'] = $this->staff_model->nationalityListByStatus('1');
        $data['courseList'] = $this->staff_model->courseListByStatus('1');
        
        $this->global['pageTitle'] = 'Inventory Management : Add Staff';
        $this->loadViews("staff/add", $this->global, $data, NULL);
    }


    function edit($id)
    {
        if ($id == null)
        {
            redirect('/partner_university/staff/list');
        }


        $tab = 0;

        $resultprint = $this->input->post();

        // echo "<Pre>"; print_r($resultprint);exit();
        
        if($resultprint)
        {
        
        // echo "<Pre>"; print_r($resultprint);exit();

        $tab = $resultprint['btn_submit'];


        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;

        $formData = $this->input->post();


         switch ($resultprint['btn_submit'])
         {

            case '1':





            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $ic_no = $this->security->xss_clean($this->input->post('ic_no'));
            $staff_id = $this->security->xss_clean($this->input->post('staff_id'));
            $address = $this->security->xss_clean($this->input->post('address'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $address_two = $this->security->xss_clean($this->input->post('address_two'));
            $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
            $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
            $id_country = $this->security->xss_clean($this->input->post('id_country'));
            $id_state = $this->security->xss_clean($this->input->post('id_state'));
            $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
            $email = $this->security->xss_clean($this->input->post('email')); 
            $job_type = $this->security->xss_clean($this->input->post('job_type'));
            $id_department = $this->security->xss_clean($this->input->post('id_department'));
            $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
            $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
            $dob = $this->security->xss_clean($this->input->post('dob'));
            $academic_type = $this->security->xss_clean($this->input->post('academic_type'));
            $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $id_type = $this->security->xss_clean($this->input->post('id_type'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $status = $this->security->xss_clean($this->input->post('status'));
            $joined_date = $this->security->xss_clean($this->input->post('joined_date'));


            $whatsapp_number = $this->security->xss_clean($this->input->post('whatsapp_number'));
            $linked_in = $this->security->xss_clean($this->input->post('linked_in'));
            $facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
            $twitter_id = $this->security->xss_clean($this->input->post('twitter_id'));
            $ig_id = $this->security->xss_clean($this->input->post('ig_id'));
                 
            $salutationInfo = $this->staff_model->getSalutation($salutation);
            
            $data = array(
                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                'id_type' => $id_type,
                'nationality' => $nationality,
                'ic_no' => $ic_no,
                'staff_id' => $staff_id,
                'gender' => $gender,
                'mobile_number' => $mobile_number,
                'phone_number' => $phone_number,
                'id_country' => $id_country,
                'id_state' => $id_state,
                'zipcode' => $zipcode,
                'email' => $email,
                'address' => $address,
                'address_two' => $address_two,
                'job_type' => $job_type,
                'id_department' => $id_department,
                'id_faculty_program' => $id_faculty_program,
                'dob' =>  date('Y-m-d',strtotime($dob)),
                'joined_date' => date('Y-m-d',strtotime($joined_date)),
                'academic_type' => $academic_type,
                'id_education_level' => $id_education_level,
                'whatsapp_number' => $whatsapp_number,
                'linked_in' => $linked_in,
                'facebook_id' => $facebook_id,
                'twitter_id' => $twitter_id,
                'ig_id' => $ig_id,
                'status' => $status
            );              
            $result = $this->staff_model->editStaff($data,$id);
            redirect($_SERVER['HTTP_REFERER']);
            // redirect('/partner_university/staff/list');

            break;



            case '2':


            $formData = $this->input->post();



            $id_teaching_semester = $this->security->xss_clean($this->input->post('id_teaching_semester'));
            $id_teaching_programme = $this->security->xss_clean($this->input->post('id_teaching_programme'));
            $id_teaching_course = $this->security->xss_clean($this->input->post('id_teaching_course'));
            $id_teaching_mode_of_study = $this->security->xss_clean($this->input->post('id_teaching_mode_of_study'));
            $id_teaching_learning_center = $this->security->xss_clean($this->input->post('id_teaching_learning_center'));

            
            $data = array(
                'id_staff' => $id,
                'id_semester' => $id_teaching_semester,
                'id_programme' => $id_teaching_programme,
                'id_course' => $id_teaching_course,
                'id_mode_of_study' => $id_teaching_mode_of_study,
                'id_learning_center' => $id_teaching_learning_center,
                'status' => 1,
                'created_by' => $id_user,
            );              
            $result = $this->staff_model->addStaffTeachingDetails($data);
            redirect($_SERVER['HTTP_REFERER']);
            // redirect('/partner_university/staff/list');

            break;






            case '3':


            $formData = $this->input->post();



            $leave_name = $this->security->xss_clean($this->input->post('leave_name'));
            $leave_from_dt = $this->security->xss_clean($this->input->post('leave_from_dt'));
            $leave_to_dt = $this->security->xss_clean($this->input->post('leave_to_dt'));

            
            $data = array(
                'id_staff' => $id,
                'name' => $leave_name,
                'from_dt' => date('Y-m-d',strtotime($leave_from_dt)),
                'to_dt' => date('Y-m-d',strtotime($leave_to_dt)),
                'status' => 1,
                'created_by' => $id_user,
            );              

            $result = $this->staff_model->addStaffLeaveDetails($data);

            
            redirect($_SERVER['HTTP_REFERER']);
            // redirect('/partner_university/staff/list');

            break;





            case '5':


            $formData = $this->input->post();



            $id_change_status = $this->security->xss_clean($this->input->post('id_change_status'));
            $change_from_dt = $this->security->xss_clean($this->input->post('change_from_dt'));
            $change_to_dt = $this->security->xss_clean($this->input->post('change_to_dt'));
            $change_status_reason = $this->security->xss_clean($this->input->post('change_status_reason'));
            
            $data = array(
                'id_staff' => $id,
                'id_change_status' => $id_change_status,
                'reason' => $change_status_reason,
                'from_dt' => date('Y-m-d',strtotime($change_from_dt)),
                'to_dt' => date('Y-m-d',strtotime($change_to_dt)),
                'status' => 1,
                'created_by' => $id_user,
            );              
            $result = $this->staff_model->addStaffChangeStatus($data);


            redirect($_SERVER['HTTP_REFERER']);
            // redirect('/partner_university/staff/list');

            break;



            case '6':


            $formData = $this->input->post();



            $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
            $bank_account_name = $this->security->xss_clean($this->input->post('bank_account_name'));
            $bank_code = $this->security->xss_clean($this->input->post('bank_code'));
            $bank_address = $this->security->xss_clean($this->input->post('bank_address'));
            $bank_account_number = $this->security->xss_clean($this->input->post('bank_account_number'));
            
            $data = array(
                'id_staff' => $id,
                'id_bank' => $id_bank,
                'bank_account_name' => $bank_account_name,
                'bank_code' => $bank_code,
                'bank_account_number' => $bank_account_number,
                'bank_address' => $bank_address,
                // 'bank_status' => $bank_status,
                'status' => 1,
                'created_by' => $id_user,
            );              
        
            // echo "<Pre>"; print_r($data);exit();

            $result = $this->staff_model->addStaffBankDetails($data);


            redirect($_SERVER['HTTP_REFERER']);
            // redirect('/partner_university/staff/list');

            break;



            case '7':

                // echo "<Pre>"; print_r($formData);exit();

                // echo "<Pre>"; print_r($_FILES);exit();

                if($_FILES['certificate'])
                {  


                    $certificate_name = $_FILES['certificate']['name'];
                    $certificate_size = $_FILES['certificate']['size'];
                    $certificate_tmp =$_FILES['certificate']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate File');

                    $certificate = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate File');

                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $qualification_level = $this->security->xss_clean($this->input->post('qualification_level'));
                $qualification_name = $this->security->xss_clean($this->input->post('qualification_name'));
                $awarding_institute = $this->security->xss_clean($this->input->post('awarding_institute'));
                $country = $this->security->xss_clean($this->input->post('country'));
                $year_obtained = $this->security->xss_clean($this->input->post('year_obtained'));

                $qualification = array(

                        'id_staff' => $id,
                        'level' => $qualification_level,
                        'name' => $qualification_name,
                        'awarding_institute' => $awarding_institute,
                        'country' => $country,
                        'year' => $year_obtained,
                        'status' => 1
                    );


                if($certificate != '')
                {
                    $qualification['certificate'] = $certificate;
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->staff_model->addNewStaffEducatinQualification($qualification);

                
                redirect($_SERVER['HTTP_REFERER']);
                
                break;



            case '8':

                // echo "<Pre>"; print_r($formData);exit();

                // echo "<Pre>"; print_r($_FILES);exit();

                if($_FILES['employment_letter'])
                {  


                    $certificate_name = $_FILES['employment_letter']['name'];
                    $certificate_size = $_FILES['employment_letter']['size'];
                    $certificate_tmp =$_FILES['employment_letter']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Employment Letter File');

                    $employment_letter = $this->uploadFile($certificate_name,$certificate_tmp,'Employment Letter File');

                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $organisation_name = $this->security->xss_clean($this->input->post('organisation_name'));
                $designation = $this->security->xss_clean($this->input->post('designation'));
                $level = $this->security->xss_clean($this->input->post('level'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));

                $work_experience = array(

                        'id_staff' => $id,
                        'organisation_name' => $organisation_name,
                        'designation' => $designation,
                        'level' => $level,
                        'start_date' => date('Y-m-d',strtotime($start_date)),
                        'end_date' => date('Y-m-d',strtotime($end_date)),
                        'status' => 1
                    );


                if($employment_letter != '')
                {
                    $work_experience['employment_letter'] = $employment_letter;
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->staff_model->addNewStaffWorkExperience($work_experience);

                
                redirect($_SERVER['HTTP_REFERER']);
                
                break;




                case '9':

                // echo "<Pre>"; print_r($formData);exit();

                // echo "<Pre>"; print_r($_FILES);exit();

                $id_specialization = $this->security->xss_clean($this->input->post('id_specialization'));

                $specialization = array(

                        'id_staff' => $id,
                        'id_specialization' => $id_specialization,
                        'status' => 1
                    );

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->staff_model->addNewStaffSpecialization($specialization);

                
                redirect($_SERVER['HTTP_REFERER']);
                
                break;


                case '10':

                // echo "<Pre>"; print_r($formData);exit();

                // echo "<Pre>"; print_r($_FILES);exit();

                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));

                $staff_programme = array(

                        'id_staff' => $id,
                        'id_programme' => $id_programme,
                        'status' => 1
                    );


                $result = $this->staff_model->addNewStaffHasProgramme($staff_programme);
                // echo "<Pre>"; print_r($result);exit();

                
                redirect($_SERVER['HTTP_REFERER']);
                
                break;


            }

         }



        $data['tab'] = $tab;
        $data['id_staff'] = $id;
        $data['courseList'] = $this->staff_model->courseListByStatus('1');
        $data['countryList'] = $this->staff_model->getCountryByStatus('1');
        $data['stateList'] = $this->staff_model->getStateByStatus('1');
        $data['specializationList'] = $this->staff_model->getSpecializationListByStatus('1');
        $data['departmentList'] = $this->staff_model->departmentListByStatus('1');

        $data['staffDetails'] = $this->staff_model->getStaff($id);
        $data['getStaffCourse'] = $this->staff_model->getStaffCourse($id);
        $data['getStaffTeachingDetails'] = $this->staff_model->getStaffTeachingDetails($id);
        $data['getStaffChangeStatusDetails'] = $this->staff_model->getStaffChangeStatusDetails($id);
        $data['getStaffLeaveDetails'] = $this->staff_model->getStaffLeaveDetails($id);
        $data['getStaffBankDetails'] = $this->staff_model->getStaffBankDetails($id);
        $data['staffEducationQualificationDetails'] = $this->staff_model->getStaffEducationQualificationDetails($id);
        $data['staffWorkExperienceDetails'] = $this->staff_model->getStaffWorkExperienceDetails($id);
        $data['staffSpecializationDetails'] = $this->staff_model->getStaffSpecializationDetails($id);
        $data['staffHasProgramme'] = $this->staff_model->getStaffHasProgramme($id);



        // echo "<Pre>"; print_r($data['getStaffBankDetails']);exit();

        $data['nationalityList'] = $this->staff_model->nationalityListByStatus('1');
        $data['facultyProgramList'] = $this->staff_model->getFacultyProgramListByStatus('1');
        $data['qualificationList'] = $this->staff_model->qualificationListByStatus('1');
        $data['salutationList'] = $this->staff_model->salutationListByStatus('1');

        $data['semesterList'] = $this->staff_model->semesterListByStatus('1');
        $data['programmeList'] = $this->staff_model->programmeListByStatus('1');
        $data['courseList'] = $this->staff_model->courseListByStatus('1');
        $data['modeOfStudyList'] = $this->staff_model->modeOfStudyListByStatus('1');
        $data['learningCenterList'] = $this->staff_model->learningCenterListByStatus('1');
        $data['bankList'] = $this->staff_model->bankListByStatus('1');


        // $data['changeStatusList'] = $this->staff_model->changeStatusListByStatus('1');

        $this->global['pageTitle'] = 'Inventory Management : Edit Staff';
        $this->loadViews("staff/edit", $this->global, $data, NULL);
        // $this->loadViews("staff/edit_tab", $this->global, $data, NULL);
    }


    function changeStatusList()
    {
        $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
        $formData['name'] = $this->security->xss_clean($this->input->post('name'));

        $data['searchParam'] = $formData;
        $data['departmentList'] = $this->staff_model->getDepartmentByStatus('1');
        // $data['departmentList'] = $this->staff_model->getStateByStatus('1');
        $data['staffChangeStatusList'] = $this->staff_model->staffChangeStatusListSearch($formData);

        // echo "<Pre>";print_r($data['staffChangeStatusList']);exit;
        
        $this->global['pageTitle'] = 'Inventory Management : Staff List';
        $this->loadViews("staff/change_status_list", $this->global, $data, NULL);
    }


    function changeStatus()
    {
        
        $resultprint = $this->input->post();

        // echo "<Pre>"; print_r($resultprint);exit();
        
        if($resultprint)
        {
        

        // echo "<Pre>"; print_r($resultprint);exit();

        $id_user = $this->session->userId;
        $id_session = $this->session->my_session_id;



         switch ($resultprint['btn_submit'])
         {

            case '5':


            $formData = $this->input->post();



            $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
            $id_change_status = $this->security->xss_clean($this->input->post('id_change_status'));
            $change_from_dt = $this->security->xss_clean($this->input->post('change_from_dt'));
            $change_to_dt = $this->security->xss_clean($this->input->post('change_to_dt'));
            $change_status_reason = $this->security->xss_clean($this->input->post('change_status_reason'));
            
            $data = array(
                'id_staff' => $id_staff,
                'id_change_status' => $id_change_status,
                'reason' => $change_status_reason,
                'from_dt' => date('Y-m-d',strtotime($change_from_dt)),
                'to_dt' => date('Y-m-d',strtotime($change_to_dt)),
                'status' => 1,
                'created_by' => $id_user,
            );               

            $result = $this->staff_model->addStaffChangeStatus($data);

            if($result)
            {
                $staff_data['status'] = $id_change_status;
                $staff_data['updated_by'] = $id_user;

                $updated_student = $this->staff_model->editStaff($staff_data,$id_staff);
            }

            // redirect($_SERVER['HTTP_REFERER']);
            redirect('/partner_university/staff/changeStatusList');

            break;


            }

         }

        $data['changeStatusList'] = $this->staff_model->changeStatusListByStatus('1');
        $data['staffList'] = $this->staff_model->staffList();

        $this->global['pageTitle'] = 'Inventory Management : Edit Staff';
        $this->loadViews("staff/change_status", $this->global, $data, NULL);
        // $this->loadViews("staff/edit_tab", $this->global, $data, NULL);
    }

    function viewChangeStatus($id)
    {
        if ($id == null)
        {
            redirect('/partner_university/staff/changeStatusList');
        }

        $data['staffChangeStatus'] = $this->staff_model->getStaffChangeStatus($id);

        // $data['staffDetails'] = $this->staff_model->getStaff($data['staffChangeStatus']->id_staff);
        $data['changeStatusList'] = $this->staff_model->changeStatusListByStatus('1');
        $data['staffList'] = $this->staff_model->staffList();

        $this->global['pageTitle'] = 'Inventory Management : Edit Staff';
        $this->loadViews("staff/view_change_status", $this->global, $data, NULL);
        // $this->loadViews("staff/edit_tab", $this->global, $data, NULL);
    }


    function delete()
    {
        $countryId = $this->input->post('countryId');
        $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
        $result = $this->staff_model->deleteSemmester($countryId, $countryInfo);
        if ($result > 0)
        {
            echo (json_encode(array('status' => TRUE)));
        }
        else
        {
            echo (json_encode(array('status' => FALSE)));
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->staff_model->updateTempDetails($tempData,$id);
        }
        else
        {
            unset($tempData['id']);
            $inserted_id = $this->staff_model->addTempDetails($tempData);
// echo "<Pre>";  print_r($tempData);exit;
        }
        $data = $this->displaytempdata();
        
        echo $data;
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->staff_model->getTempStaff($id_session); 
        // echo "<Pre>";print_r($details);exit;
         if(!empty($temp_details))
        {
            $table = "<table  class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Course Name</th>
                        <th>Action</th>
                    </tr>";
                        for($i=0;$i<count($temp_details);$i++)
                        {
                        $id = $temp_details[$i]->id;
                        $fee_name = $temp_details[$i]->name;
                        $j = $i+1;
                            $table .= "
                            <tr>
                                <td>$j</td>
                                <td>$fee_name</td>                       
                                <td>
                                    <span onclick='deleteTempData($id)'>Delete</a>
                                <td>
                            </tr>";
                        }
            $table.= "</table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->staff_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    }

    function getStateByCountry($id_country)
    {
            $results = $this->staff_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="<select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_course'] =  $tempData['id_course'];
        $data['id_staff'] =  $tempData['id'];
        $inserted_id = $this->staff_model->addNewStaffCourse($data);
        
        echo $inserted_id;exit;
         // $temp_details = $this->staff_model->getStaffCourse($tempData['id']);

        // if(!empty($temp_details))
        // {

        //     $table = "<table  class='table' id='list-table'>
        //               <tr>
        //                 <th>Sl. No</th>
        //                 <th>Course Name</th>
        //                 <th>Action</th>
        //             </tr>";
        //                 for($i=0;$i<count($temp_details);$i++)
        //                 {
        //                 $id = $temp_details[$i]->id;
        //                 $coursename = $temp_details[$i]->coursename;
        //                 $j = $i+1;
        //                     $table .= "
        //                     <tr>
        //                         <td>$j</td>
        //                         <td>$coursename</td>                         
        //                         <td>
        //                             <span onclick='deleteCourseDetailData($id)'>Delete</a>
        //                         <td>
        //                     </tr>";
        //                 }
        //     $table.= "</table>";
        // }
        // else
        // {
        //     $table="";
        // }
        // echo $table;           
    }

     function getSchemeByProgramId($id_program)
    {
        // It's A Learning Mode After Flow Change
         $intake_data = $this->staff_model->getProgramSchemeByProgramId($id_program);
        
        // Multiple Programme Mode Ignored For Demo On 09-11-2020
        // $intake_data = $this->applicant_model->getProgramLandscapeSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
        
        $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_teaching_mode_of_study' id='id_teaching_mode_of_study' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function deleteCourseDetailData($id_details)
    {
        $inserted_id = $this->staff_model->deleteCourseData($id_details);
        echo "Success"; 
    }

    function deleteTeachingDetails($id)
    {
        $inserted_id = $this->staff_model->deleteTeachingDetails($id);
        echo "Success"; 
    }

    function deleteStaffChangeStatus($id)
    {
        $inserted_id = $this->staff_model->deleteStaffChangeStatus($id);
        echo "Success";
    }

    function deleteStaffLeaveDetails($id)
    {
        $inserted_id = $this->staff_model->deleteStaffLeaveDetails($id);
        echo "Success";
    }

    function deleteStaffBankDetails($id)
    {
        $inserted_id = $this->staff_model->deleteStaffBankDetails($id);
        echo "Success";

    }

    function deleteEducationQualificationDetails($id)
    {
        $inserted_id = $this->staff_model->deleteEducationQualificationDetails($id);
        echo "Success";
    }

    function deleteWorkExperienceDetails($id)
    {
        $inserted_id = $this->staff_model->deleteWorkExperienceDetails($id);
        echo "Success";
    }

    function deleteSpecializationDetails($id)
    {
        $inserted_id = $this->staff_model->deleteSpecializationDetails($id);
        echo "Success";
    }

    function deleteStaffHasProgrammeDetails($id)
    {
        $inserted_id = $this->staff_model->deleteStaffHasProgrammeDetails($id);
        echo "Success";
    }
}