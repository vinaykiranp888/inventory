<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Proposal extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('research_proposal_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_proposal.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_research_category'] = $this->security->xss_clean($this->input->post('id_research_category'));
            $formData['id_research_topic'] = $this->security->xss_clean($this->input->post('id_research_topic'));
            $formData['status'] = '';


            $data['searchParam'] = $formData;
            $data['researchProposalList'] = $this->research_proposal_model->researchProposalListSearch($formData);
            $data['researchCategoryList'] = $this->research_proposal_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_proposal_model->researchTopicListByStatus('1');


            // echo "<Pre>";print_r($data['researchProposalList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Proposal List';
            $this->loadViews("research_proposal/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_proposal.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            if($this->input->post())
            {

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_research_category = $this->security->xss_clean($this->input->post('id_research_category'));
                $id_research_topic = $this->security->xss_clean($this->input->post('id_research_topic'));
                $synopsis = $this->security->xss_clean($this->input->post('synopsis'));
                $meeting_date = $this->security->xss_clean($this->input->post('meeting_date'));
                $completion_date = $this->security->xss_clean($this->input->post('completion_date'));
                $semester_start_date = $this->security->xss_clean($this->input->post('semester_start_date'));
                $semester_end_date = $this->security->xss_clean($this->input->post('semester_end_date'));
            
                if($completion_date)
                {
                    $completion_date = date('Y-m-d', strtotime($completion_date));
                }

                if($meeting_date)
                {
                    $meeting_date = date('Y-m-d', strtotime($meeting_date));
                }

                if($semester_start_date)
                {
                    $semester_start_date = date('Y-m-d', strtotime($semester_start_date));
                }

                if($semester_end_date)
                {
                    $semester_end_date = date('Y-m-d', strtotime($semester_end_date));
                }
                $data = array(
                    'id_student' => $id_student,
                    'name' => $name,
                    'id_research_category' => $id_research_category,
                    'id_research_topic' => $id_research_topic,
                    'synopsis' => $synopsis,
                    'meeting_date' => $meeting_date,
                    'completion_date' => $completion_date,
                    'semester_start_date' => $semester_start_date,
                    'semester_end_date' => $semester_end_date,
                    'status' => 0,
                    'created_by' => $id_user
                );

                $inserted_id = $this->research_proposal_model->addNewResearchProposal($data);

                if($inserted_id)
                {
                    $moved = $this->research_proposal_model->moveTempToDetails($inserted_id);
                }

                redirect('/research/proposal/list');
            }
            else
            {
                $this->research_proposal_model->deleteTempResearchProposalHasSupervisorBySessionId($id_session);
                $this->research_proposal_model->deleteTempResearchProposalHasExaminerBySessionId($id_session);
            }
            $data['staffList'] = $this->research_proposal_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_proposal_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_proposal_model->researchTopicListByStatus('1');
            $data['programList'] = $this->research_proposal_model->programListByStatus('1');

            $data['supervisorRoleList'] = $this->research_proposal_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_proposal_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_proposal_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['examinerList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Proposal';
            $this->loadViews("research_proposal/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_proposal.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/proposal/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_research_category = $this->security->xss_clean($this->input->post('id_research_category'));
                $id_research_topic = $this->security->xss_clean($this->input->post('id_research_topic'));
                $synopsis = $this->security->xss_clean($this->input->post('synopsis'));
                $meeting_date = $this->security->xss_clean($this->input->post('meeting_date'));
                $completion_date = $this->security->xss_clean($this->input->post('completion_date'));
                $semester_start_date = $this->security->xss_clean($this->input->post('semester_start_date'));
                $semester_end_date = $this->security->xss_clean($this->input->post('semester_end_date'));
            
                if($completion_date)
                {
                    $completion_date = date('Y-m-d', strtotime($completion_date));
                }

                // echo "<Pre>";print_r($completion_date);exit();


                if($meeting_date)
                {
                    $meeting_date = date('Y-m-d', strtotime($meeting_date));
                }

                if($semester_start_date)
                {
                    $semester_start_date = date('Y-m-d', strtotime($semester_start_date));
                }

                if($semester_end_date)
                {
                    $semester_end_date = date('Y-m-d', strtotime($semester_end_date));
                }
                $data = array(
                    'name' => $name,
                    'id_research_category' => $id_research_category,
                    'id_research_topic' => $id_research_topic,
                    'synopsis' => $synopsis,
                    'meeting_date' => $meeting_date,
                    'completion_date' => $completion_date,
                    'semester_start_date' => $semester_start_date,
                    'semester_end_date' => $semester_end_date,
                    'status' => 0,
                    'updated_by' => $id_user
                );

                $result = $this->research_proposal_model->editResearchProposalDetails($data,$id);
                redirect('/research/proposal/list');
            }
            $data['researchProposal'] = $this->research_proposal_model->getResearchProposal($id);
            $data['researchProposalHasSupervisor'] = $this->research_proposal_model->getResearchProposalHasSupervisor($id);
            $data['researchProposalHasExaminer'] = $this->research_proposal_model->getResearchProposalHasExaminer($id);

            $data['studentDetails'] = $this->research_proposal_model->getStudentByStudentId($data['researchProposal']->id_student);



            $data['staffList'] = $this->research_proposal_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_proposal_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_proposal_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->research_proposal_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_proposal_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_proposal_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchProposalHasExaminer']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Proposal';
            $this->loadViews("research_proposal/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('research_proposal.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/proposal/list');
            }
            
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                redirect('/research/proposal/list');
            }

            $data['researchProposal'] = $this->research_proposal_model->getResearchProposal($id);
            $data['researchProposalHasSupervisor'] = $this->research_proposal_model->getResearchProposalHasSupervisor($id);
            $data['researchProposalHasExaminer'] = $this->research_proposal_model->getResearchProposalHasExaminer($id);

            $data['studentDetails'] = $this->research_proposal_model->getStudentByStudentId($data['researchProposal']->id_student);



            $data['staffList'] = $this->research_proposal_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_proposal_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_proposal_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->research_proposal_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_proposal_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_proposal_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchProposalHasExaminer']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Proposal';
            $this->loadViews("research_proposal/view", $this->global, $data, NULL);
        }
    }



    function approvalList()
    {
        if ($this->checkAccess('research_proposal.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_research_category'] = $this->security->xss_clean($this->input->post('id_research_category'));
            $formData['id_research_topic'] = $this->security->xss_clean($this->input->post('id_research_topic'));
            $formData['status'] = '0';



            $data['searchParam'] = $formData;
            $data['researchProposalList'] = $this->research_proposal_model->researchProposalListSearch($formData);
            $data['researchCategoryList'] = $this->research_proposal_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_proposal_model->researchTopicListByStatus('1');


            // echo "<Pre>";print_r($data['researchProposalList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Proposal Approval List';
            $this->loadViews("research_proposal/approval_list", $this->global, $data, NULL);
        }
    }


    function approve($id = NULL)
    {
        if ($this->checkAccess('research_proposal.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/proposal/approvalList');
            }
            
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->research_proposal_model->editResearchProposalDetails($data,$id);
                redirect('/research/proposal/approvalList');
            }

            $data['researchProposal'] = $this->research_proposal_model->getResearchProposal($id);
            $data['researchProposalHasSupervisor'] = $this->research_proposal_model->getResearchProposalHasSupervisor($id);
            $data['researchProposalHasExaminer'] = $this->research_proposal_model->getResearchProposalHasExaminer($id);

            $data['studentDetails'] = $this->research_proposal_model->getStudentByStudentId($data['researchProposal']->id_student);



            $data['staffList'] = $this->research_proposal_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->research_proposal_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->research_proposal_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->research_proposal_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_proposal_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_proposal_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchProposalHasExaminer']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Approve Proposal';
            $this->loadViews("research_proposal/approve", $this->global, $data, NULL);
        }
    }


    function getIntakeByProgramme($id_programme)
    {
        $intake_data = $this->research_proposal_model->getIntakeByProgrammeId($id_programme);

        // echo "<Pre>"; print_r($intake_data);exit;

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByData()'>
        <option value=''>Select</option>
        ";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $year = $intake_data[$i]->year;

        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id_programme = $tempData['id_programme'];
        // $id_program_scheme = $tempData['id_program_scheme'];
        // $id_intake = $tempData['id_intake'];

        $student_data = $this->research_proposal_model->getStudentByData($tempData);

        // echo "<Pre>";print_r($student_data);exit();

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>
        <option value=''>Select</option>";

        for($i=0;$i<count($student_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_data[$i]->id;
        $full_name = $student_data[$i]->full_name;
        $nric = $student_data[$i]->nric;

        $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->research_proposal_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $nationality = $student_data->nationality;

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';
            }elseif($nationality == 'Other')
            {
                $currency = 'USD';
            }


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' />
                                    <input type='hidden' name='currency' id='currency' value='$currency' />
                                    $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";

                
            echo $table;
            exit;
    }




    function tempAddResearchProposalHasSupervisor()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_proposal_model->tempAddResearchProposalHasSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempSupervisordata();
        
        echo $data;        
    }

    function displayTempSupervisordata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->research_proposal_model->getTempResearchProposalHasSupervisorBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Supervisor</th>
                    <th>Supervisor Role</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $supervisor_role = $temp_details[$i]->supervisor_role;
                    $status = $temp_details[$i]->status;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $staff_name</td>                         
                            <td>$supervisor_role</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchProposalHasSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProposalHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchProposalHasSupervisor($id)
    {
        $inserted_id = $this->research_proposal_model->deleteTempResearchProposalHasSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displayTempSupervisordata();
            echo $data;  
        }
    }



    function tempAddResearchProposalHasExaminer()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_proposal_model->tempAddResearchProposalHasExaminer($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempExaminerdata();
        
        echo $data;        
    }

    function displayTempExaminerdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->research_proposal_model->getTempResearchProposalHasExaminerBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Type</th>
                    <th>Examiner</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>";


                    // <th>Status</th>
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $type = $temp_details[$i]->type;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $full_name = $temp_details[$i]->full_name;
                    $status = $temp_details[$i]->status;
                    $examiner_role = $temp_details[$i]->examiner_role;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }


                    if($type == 1)
                    {
                        $type = 'Internal';
                        $staff = $ic_no . " - " . $staff_name;
                    }else
                    {
                        $type = 'External';
                        $staff = $full_name;
                    }

                    $j = $i+1;
                            // <td>$status</td>

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$type</td>
                            <td>$staff</td>                         
                            <td>$examiner_role</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchProposalHasExaminer($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempProposalHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchProposalHasExaminer($id)
    {
        $inserted_id = $this->research_proposal_model->deleteTempResearchProposalHasExaminer($id);
        if($inserted_id)
        {
            $data = $this->displayTempExaminerdata();
            echo $data;  
        }
    }




    function addResearchProposalHasSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_proposal_model->addNewResearchProposalHasSupervisors($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchProposalHasSupervisor($id)
    {
        $inserted_id = $this->research_proposal_model->deleteResearchProposalHasSupervisor($id);
        echo "Success"; 
    }





    function addResearchProposalHasExaminer()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_proposal_model->addNewResearchProposalHasExaminer($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchProposalHasExaminer($id)
    {
        $inserted_id = $this->research_proposal_model->deleteResearchProposalHasExaminer($id);
        echo "Success"; 
    }
}
