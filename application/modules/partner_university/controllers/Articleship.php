<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Articleship extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('research_articleship_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('research_articleship.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['status'] = '';



            $data['searchParam'] = $formData;
            $data['researchArticleshipList'] = $this->research_articleship_model->researchArticleshipListSearch($formData);
            $data['semesterList'] = $this->research_articleship_model->semesterListByStatus('1');


            // echo "<Pre>";print_r($data['researchArticleshipList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Articleship List';
            $this->loadViews("research_articleship/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('research_articleship.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            if($this->input->post())
            {

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $assistance = $this->security->xss_clean($this->input->post('assistance'));
                $company_name = $this->security->xss_clean($this->input->post('company_name'));
                $designation = $this->security->xss_clean($this->input->post('designation'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $is_employee = $this->security->xss_clean($this->input->post('is_employee'));
                $year_of_service = $this->security->xss_clean($this->input->post('year_of_service'));
            
        
                if($start_date)
                {
                    $start_date = date('Y-m-d', strtotime($start_date));
                }

                if($end_date)
                {
                    $end_date = date('Y-m-d', strtotime($end_date));
                }

                $data = array(
                    'id_student' => $id_student,
                    'id_semester' => $id_semester,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'assistance' => $assistance,
                    'company_name' => $company_name,
                    'designation' => $designation,
                    'contact_person' => $contact_person,
                    'address' => $address,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'phone_number' => $phone_number,
                    'fax' => $fax,
                    'is_employee' => $is_employee,
                    'year_of_service' => $year_of_service,
                    'status' => 0,
                    'created_by' => $id_user
                );

                $inserted_id = $this->research_articleship_model->addNewResearchArticleship($data);

                if($inserted_id)
                {
                    $moved = $this->research_articleship_model->moveTempToDetails($inserted_id);
                }

                redirect('/research/articleship/list');
            }
            else
            {
                $this->research_articleship_model->deleteTempResearchArticleshipHasSupervisorBySessionId($id_session);
                $this->research_articleship_model->deleteTempResearchArticleshipHasExaminerBySessionId($id_session);
            }
            
            $data['staffList'] = $this->research_articleship_model->staffListByStatus('1');
            $data['supervisorRoleList'] = $this->research_articleship_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_articleship_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_articleship_model->examinerRoleListByStatus('1');
            $data['semesterList'] = $this->research_articleship_model->semesterListByStatus('1');
            $data['countryList'] = $this->research_articleship_model->countryListByStatus('1');
            $data['programList'] = $this->research_articleship_model->programListByStatus('1');



            // echo "<Pre>";print_r($data['examinerList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Articleship';
            $this->loadViews("research_articleship/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('research_articleship.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/articleship/list');
            }
            if($this->input->post())
            {
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $assistance = $this->security->xss_clean($this->input->post('assistance'));
                $company_name = $this->security->xss_clean($this->input->post('company_name'));
                $designation = $this->security->xss_clean($this->input->post('designation'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $is_employee = $this->security->xss_clean($this->input->post('is_employee'));
                $year_of_service = $this->security->xss_clean($this->input->post('year_of_service'));
            
        
                if($start_date)
                {
                    $start_date = date('Y-m-d', strtotime($start_date));
                }

                if($end_date)
                {
                    $end_date = date('Y-m-d', strtotime($end_date));
                }

                $data = array(
                    'id_semester' => $id_semester,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'assistance' => $assistance,
                    'company_name' => $company_name,
                    'designation' => $designation,
                    'contact_person' => $contact_person,
                    'address' => $address,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'phone_number' => $phone_number,
                    'fax' => $fax,
                    'is_employee' => $is_employee,
                    'year_of_service' => $year_of_service,
                    'status' => 0,
                    'updated_by' => $id_user
                );

                $result = $this->research_articleship_model->editResearchArticleshipDetails($data,$id);
                redirect('/research/articleship/list');
            }
            $data['researchArticleship'] = $this->research_articleship_model->getResearchArticleship($id);
            $data['researchArticleshipHasSupervisor'] = $this->research_articleship_model->getResearchArticleshipHasSupervisor($id);
            $data['researchArticleshipHasExaminer'] = $this->research_articleship_model->getResearchArticleshipHasExaminer($id);

            $data['studentDetails'] = $this->research_articleship_model->getStudentByStudentId($data['researchArticleship']->id_student);



            $data['staffList'] = $this->research_articleship_model->staffListByStatus('1');
            $data['supervisorRoleList'] = $this->research_articleship_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_articleship_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_articleship_model->examinerRoleListByStatus('1');
            $data['semesterList'] = $this->research_articleship_model->semesterListByStatus('1');
            $data['countryList'] = $this->research_articleship_model->countryListByStatus('1');
            $data['programList'] = $this->research_articleship_model->programListByStatus('1');
            // echo "<Pre>";print_r($data['researchArticleshipHasExaminer']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Articleship';
            $this->loadViews("research_articleship/edit", $this->global, $data, NULL);
        }
    }



    function view($id = NULL)
    {
        if ($this->checkAccess('research_articleship.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/articleship/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                redirect('/research/articleship/list');
            }
            $data['researchArticleship'] = $this->research_articleship_model->getResearchArticleship($id);
            $data['researchArticleshipHasSupervisor'] = $this->research_articleship_model->getResearchArticleshipHasSupervisor($id);
            $data['researchArticleshipHasExaminer'] = $this->research_articleship_model->getResearchArticleshipHasExaminer($id);

            $data['studentDetails'] = $this->research_articleship_model->getStudentByStudentId($data['researchArticleship']->id_student);



            $data['staffList'] = $this->research_articleship_model->staffListByStatus('1');
            $data['supervisorRoleList'] = $this->research_articleship_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_articleship_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_articleship_model->examinerRoleListByStatus('1');
            $data['semesterList'] = $this->research_articleship_model->semesterListByStatus('1');
            $data['countryList'] = $this->research_articleship_model->countryListByStatus('1');
            $data['programList'] = $this->research_articleship_model->programListByStatus('1');
            // echo "<Pre>";print_r($data['researchArticleshipHasExaminer']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Articleship';
            $this->loadViews("research_articleship/view", $this->global, $data, NULL);
        }
    }


    function approvalList()
    {
        if ($this->checkAccess('research_articleship.approval_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['status'] = '0';



            $data['searchParam'] = $formData;
            $data['researchArticleshipList'] = $this->research_articleship_model->researchArticleshipListSearch($formData);
            $data['semesterList'] = $this->research_articleship_model->semesterListByStatus('1');


            // echo "<Pre>";print_r($data['researchArticleshipList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Articleship Approval List';
            $this->loadViews("research_articleship/approval_list", $this->global, $data, NULL);
        }
    }



    function approve($id = NULL)
    {
        if ($this->checkAccess('research_articleship.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/research/articleship/list');
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->research_articleship_model->editResearchArticleshipDetails($data,$id);
                redirect('/research/articleship/approvalList');
            }
            $data['researchArticleship'] = $this->research_articleship_model->getResearchArticleship($id);
            $data['researchArticleshipHasSupervisor'] = $this->research_articleship_model->getResearchArticleshipHasSupervisor($id);
            $data['researchArticleshipHasExaminer'] = $this->research_articleship_model->getResearchArticleshipHasExaminer($id);

            $data['studentDetails'] = $this->research_articleship_model->getStudentByStudentId($data['researchArticleship']->id_student);



            $data['staffList'] = $this->research_articleship_model->staffListByStatus('1');
            $data['supervisorRoleList'] = $this->research_articleship_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->research_articleship_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->research_articleship_model->examinerRoleListByStatus('1');
            $data['semesterList'] = $this->research_articleship_model->semesterListByStatus('1');
            $data['countryList'] = $this->research_articleship_model->countryListByStatus('1');
            $data['programList'] = $this->research_articleship_model->programListByStatus('1');
            // echo "<Pre>";print_r($data['researchArticleshipHasExaminer']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Approve Articleship';
            $this->loadViews("research_articleship/approve", $this->global, $data, NULL);
        }
    }


    function getIntakeByProgramme($id_programme)
    {
        $intake_data = $this->research_articleship_model->getIntakeByProgrammeId($id_programme);

        // echo "<Pre>"; print_r($intake_data);exit;

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByData()'>
        <option value=''>Select</option>
        ";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $year = $intake_data[$i]->year;

        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id_programme = $tempData['id_programme'];
        // $id_program_scheme = $tempData['id_program_scheme'];
        // $id_intake = $tempData['id_intake'];

        $student_data = $this->research_articleship_model->getStudentByData($tempData);

        // echo "<Pre>";print_r($student_data);exit();

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>
        <option value=''>Select</option>";

        for($i=0;$i<count($student_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_data[$i]->id;
        $full_name = $student_data[$i]->full_name;
        $nric = $student_data[$i]->nric;

        $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->research_articleship_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $nationality = $student_data->nationality;

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';
            }elseif($nationality == 'Other')
            {
                $currency = 'USD';
            }


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' />
                                    <input type='hidden' name='currency' id='currency' value='$currency' />
                                    $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";

                
            echo $table;
            exit;
    }

    function getStateByCountry($id_country)
    {
            $results = $this->research_articleship_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function tempAddResearchArticleshipHasSupervisor()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_articleship_model->tempAddResearchArticleshipHasSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempSupervisordata();
        
        echo $data;        
    }

    function displayTempSupervisordata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->research_articleship_model->getTempResearchArticleshipHasSupervisorBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Supervisor</th>
                    <th>Supervisor Role</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $supervisor_role = $temp_details[$i]->supervisor_role;
                    $status = $temp_details[$i]->status;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $staff_name</td>                         
                            <td>$supervisor_role</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchArticleshipHasSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempArticleshipHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchArticleshipHasSupervisor($id)
    {
        $inserted_id = $this->research_articleship_model->deleteTempResearchArticleshipHasSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displayTempSupervisordata();
            echo $data;  
        }
    }



    function tempAddResearchArticleshipHasExaminer()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->research_articleship_model->tempAddResearchArticleshipHasExaminer($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempExaminerdata();
        
        echo $data;        
    }

    function displayTempExaminerdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->research_articleship_model->getTempResearchArticleshipHasExaminerBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Type</th>
                    <th>Examiner</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>";


                    // <th>Status</th>
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $type = $temp_details[$i]->type;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $full_name = $temp_details[$i]->full_name;
                    $status = $temp_details[$i]->status;
                    $examiner_role = $temp_details[$i]->examiner_role;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }


                    if($type == 1)
                    {
                        $type = 'Internal';
                        $staff = $ic_no . " - " . $staff_name;
                    }else
                    {
                        $type = 'External';
                        $staff = $full_name;
                    }

                    $j = $i+1;
                            // <td>$status</td>

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$type</td>
                            <td>$staff</td>                         
                            <td>$examiner_role</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchArticleshipHasExaminer($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempArticleshipHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchArticleshipHasExaminer($id)
    {
        $inserted_id = $this->research_articleship_model->deleteTempResearchArticleshipHasExaminer($id);
        if($inserted_id)
        {
            $data = $this->displayTempExaminerdata();
            echo $data;  
        }
    }




    function addResearchArticleshipHasSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_articleship_model->addNewResearchArticleshipHasSupervisors($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchArticleshipHasSupervisor($id)
    {
        $inserted_id = $this->research_articleship_model->deleteResearchArticleshipHasSupervisor($id);
        echo "Success"; 
    }





    function addResearchArticleshipHasExaminer()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->research_articleship_model->addNewResearchArticleshipHasExaminer($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchArticleshipHasExaminer($id)
    {
        $inserted_id = $this->research_articleship_model->deleteResearchArticleshipHasExaminer($id);
        echo "Success"; 
    }
}
