<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Academic Facilitator </h3>
        </div>




            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Faculty Details</a>
                    </li>

                   <!--  <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Teaching Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Records</a>
                    </li>

                    <li role="presentation"><a href="#tab_four" class="nav-link border rounded text-center"
                            aria-controls="tab_four" role="tab" data-toggle="tab">Statement of account</a>
                    </li>

                    <li role="presentation"><a href="#tab_five" class="nav-link border rounded text-center"
                            aria-controls="tab_five" role="tab" data-toggle="tab">Change status menu</a>
                    </li> -->

                    <li role="presentation"><a href="#tab_six" class="nav-link border rounded text-center"
                            aria-controls="tab_six" role="tab" data-toggle="tab">Bank Account Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_seven" class="nav-link border rounded text-center"
                            aria-controls="tab_seven" role="tab" data-toggle="tab">Qualification Details</a>
                    </li>

                    <li role="presentation"><a href="#tab_eight" class="nav-link border rounded text-center"
                            aria-controls="tab_eight" role="tab" data-toggle="tab">Work Experience Details</a>
                    </li>


                    <li role="presentation"><a href="#tab_nine" class="nav-link border rounded text-center"
                            aria-controls="tab_nine" role="tab" data-toggle="tab">Specialization Details</a>
                    </li>


                    <!-- <li role="presentation"><a href="#tab_ten" class="nav-link border rounded text-center"
                            aria-controls="tab_ten" role="tab" data-toggle="tab">Programme Details</a>
                    </li> -->


                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="col-12 mt-4">




                    

                            <form id="form_staff" action="" method="post">

                            <div class="form-container">
                                <h4 class="form-group-title">Faculty Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Salutation <span class='error-text'>*</span></label>
                                             <select name="salutation" id="salutation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($salutationList)) {
                                                    foreach ($salutationList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php if($staffDetails->salutation==$record->id)
                                                            {
                                                                echo "selected=selected";
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>First Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $staffDetails->first_name;?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Last Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $staffDetails->last_name;?>">
                                        </div>
                                    </div>

                                      
                                    
                                </div>


                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <label>ID Type <span class='error-text'>*</span></label>
                                           <select name="id_type" id="id_type" class="form-control" required onchange="getlabel()">
                                            <option value="">Select</option>
                                              <option value="NRIC" <?php if($staffDetails->id_type=='NRIC') { echo "selected=selected";}?>>NRIC</option>
                                              <option value="PASSPORT" <?php if($staffDetails->id_type=='PASSPORT') { echo "selected=selected";}?>>PASSPORT</option>
                                          </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                         <div class="form-group">
                                            <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="<?php echo $staffDetails->ic_no ?>">
                                         </div>
                                    </div>



                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>IC No. <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="<?php echo $staffDetails->ic_no;?>">
                                        </div>
                                    </div> -->

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Gender <span class='error-text'>*</span></label>
                                             <select name="gender" id="gender" class="form-control">
                                                <option value="Male" <?php if($staffDetails->gender=='Male')
                                                {
                                                    echo "selected=selected";
                                                }
                                                ?>
                                                >Male</option>
                                                <option value="Female" <?php if($staffDetails->gender=='Female')
                                                {
                                                    echo "selected=selected";
                                                }?>
                                                >Female</option>
                                            </select>
                                        </div>
                                    </div>

                                
                                </div>

                                <div class="row"> 



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mobile  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $staffDetails->mobile_number;?>">
                                        </div>
                                    </div> 
                                   


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $staffDetails->email;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty ID <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="staff_id" name="staff_id" value="<?php echo $staffDetails->staff_id;?>">
                                        </div>
                                    </div> 


                                    

                                </div>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty Type <span class='error-text'>*</span></label>
                                             <select name="job_type" id="job_type" class="form-control">
                                                <option value="">Select</option>
                                                <option value="0" <?php if($staffDetails->job_type=='0') { echo "selected=selected";}?>>Part Time</option>
                                                <option value="1" <?php if($staffDetails->job_type=='1') { echo "selected=selected";}?>>Full TIme</option>
                                            </select>
                                        </div>
                                    </div>  


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>DOB <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="dob" name="dob" value="<?php echo date('d-m-Y',strtotime($staffDetails->dob));?>">
                                        </div>
                                    </div>
                                    
                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Academic Type <span class='error-text'>*</span></label>

                                             <select name="academic_type" id="academic_type" class="form-control">
                                                <option value="1" <?php if($staffDetails->academic_type=='1') { echo "selected=selected";}?>>Academic Faculty</option>
                                                <option value="0" <?php if($staffDetails->academic_type=='0') { echo "selected=selected";}?>>Non-Academic Faculty</option>
                                            </select>
                                        </div>
                                    </div>



                                </div>



                                <!-- <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Department <span class='error-text'>*</span></label>
                                            <select name="id_department" id="id_department" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($departmentList))
                                                {
                                                    foreach ($departmentList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_department)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->code . " - " . $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Faculty Program <span class='error-text'>*</span></label>
                                            <select name="id_faculty_program" id="id_faculty_program" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($facultyProgramList))
                                                {
                                                    foreach ($facultyProgramList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_faculty_program)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->code . " - " . $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Highest Education Level <span class='error-text'>*</span></label>
                                            <select name="id_education_level" id="id_education_level" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($qualificationList))
                                                {
                                                    foreach ($qualificationList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_education_level)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div> -->


                                <div class="row">



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Joined Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" autocomplete="off" value="<?php
                                            if($staffDetails->joined_date)
                                            {
                                                echo date('d-m-Y',strtotime($staffDetails->joined_date));
                                            }
                                            ?>">
                                        </div>
                                    </div> 




                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nationality <span class='error-text'>*</span></label>
                                            <select name="nationality" id="nationality" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($nationalityList)) {
                                                foreach ($nationalityList as $record) {
                                            ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->nationality)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>
                                                    >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                      <div class="form-group">
                                         <label>FILE 
                                         <span class='error-text'>*</span>
                                         <?php
                                            if ($staffDetails->image != '')
                                            {
                                            ?>
                                         <a href="<?php echo '/assets/images/' . $staffDetails->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $staffDetails->image; ?>)" title="<?php echo $staffDetails->image; ?>"> View </a>
                                         <?php
                                            }
                                            ?>
                                         </label>
                                         <input type="file" name="image" id="image">
                                      </div>
                                    </div>




                                </div>


                                <div class="row">


                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <p>Status <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="1" <?php if($staffDetails->status=='1') {
                                                     echo "checked=checked";
                                                  };?>><span class="check-radio"></span> Active
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="0" <?php if($staffDetails->status=='0') {
                                                     echo "checked=checked";
                                                  };?>>
                                                  <span class="check-radio"></span> In-Active
                                                </label>                              
                                            </div>                         
                                    </div>
                                    
                                </div>


                            </div>

                            <div class="form-container">
                                <h4 class="form-group-title">Contact Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $staffDetails->phone_number;?>">
                                        </div>
                                    </div>     

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $staffDetails->address;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $staffDetails->address_two;?>">
                                        </div>
                                    </div> 

                                    
                                </div>  

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Country <span class='error-text'>*</span></label>
                                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_country)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select State <span class='error-text'>*</span></label>
                                            <select name="id_state" id="id_state" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_state)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->
                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select State <span class='error-text'>*</span></label>
                                                <span id='view_state'>
                                                    <select name='id_state' id='id_state' class='form-control'>
                                                        <option value=''></option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $staffDetails->zipcode;?>">
                                            <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="1">
                                        </div>
                                    </div>

                                      
                                </div>


                                

                            </div>




                            <div class="form-container">
                             <h4 class="form-group-title">Other Details</h4>
                             <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Whatsapp Number <span class='error-text'>*</span></label>
                                      <input type="text" class="form-control" id="whatsapp_number" name="whatsapp_number" value="<?php echo $staffDetails->whatsapp_number; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>LinkedIn ID/Link: <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="linked_in" name="linked_in" value="<?php echo $staffDetails->linked_in; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Facebook ID/ Link <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="facebook_id" name="facebook_id" value="<?php echo $staffDetails->facebook_id; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Twitter ID/Link: <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="twitter_id" name="twitter_id" value="<?php echo $staffDetails->twitter_id; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Instagram ID/ Link <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="ig_id" name="ig_id" value="<?php echo $staffDetails->ig_id; ?>">
                                   </div>
                                </div>

                              </div>
                            
                           </div>













<!-- 

                            <div class="form-container">
                                <h4 class="form-group-title">Banking Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $staffDetails->phone_number;?>">
                                        </div>
                                    </div>     

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $staffDetails->address;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $staffDetails->address_two;?>">
                                        </div>
                                    </div> 

                                    
                                </div>  

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Country <span class='error-text'>*</span></label>
                                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_country)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select State <span class='error-text'>*</span></label>
                                                <span id='view_state'>
                                                    <select name='id_state' id='id_state' class='form-control'>
                                                        <option value=''></option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $staffDetails->zipcode;?>">
                                        </div>
                                    </div>

                                      
                                </div>


                                

                            </div> -->



                            <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                        <a href="../list" class="btn btn-link">Cancel</a>
                                    </div>
                            </div>


                            


                            <!-- <div class="form-container">
                                <h4 class="form-group-title">Assign Courses to Faculty</h4> 


                                <div class="row">
                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Course <span class='error-text'>*</span></label>
                                                <select name="id_course" id="id_course" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($courseList))
                                                    {
                                                        foreach ($courseList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;?>"
                                                            ><?php echo $record->code . " - " . $record->name;?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                      
                                        <div class="col-sm-4">
                                            <button type="button" class="btn-lg btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                        </div>
                                </div>




                                <?php

                                if(!empty($getStaffCourse))
                                {
                                    ?>

                                    <div class="form-container">
                                            <h4 class="form-group-title">Semester Info. Details</h4>

                                        

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th>Sl. No</th>
                                                     <th>Course</th>
                                                     <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     <?php
                                                 $total = 0;
                                                  for($i=0;$i<count($getStaffCourse);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td><?php echo $i+1;?></td>
                                                    <td><?php echo $getStaffCourse[$i]->coursename;?></td>
                                                    <td class="text-center">
                                                        <a onclick="deleteCourseDetailData(<?php echo $getStaffCourse[$i]->id; ?>)">Delete</a>
                                                    </td>

                                                     </tr>
                                                  <?php 
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                 ?>

                                
                            </div> -->



                            </form>

        
        



        

                        



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="mt-4">


                        <form id="form_teaching_details" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Teaching Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Semester <span class='error-text'>*</span></label>
                                            <select name="id_teaching_semester" id="id_teaching_semester" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($semesterList))
                                                {
                                                    foreach ($semesterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Programme <span class='error-text'>*</span></label>
                                            <select name="id_teaching_programme" id="id_teaching_programme" class="form-control" onchange="getProgramModeByProgram(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programmeList))
                                                {
                                                    foreach ($programmeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Course <span class='error-text'>*</span></label>
                                            <select name="id_teaching_course" id="id_teaching_course" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($courseList))
                                                {
                                                    foreach ($courseList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-sm-4">
                                      <div class="form-group">
                                         <label>Select Mode Of Study <span class='error-text'>*</span></label>
                                         <span id="view_program_mode">
                                              <select class="form-control" id='id_teaching_mode_of_study' name='id_teaching_mode_of_study'>
                                                <option value=''></option>
                                              </select>

                                         </span>
                                      </div>
                                   </div>




                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Mode Of Study <span class='error-text'>*</span></label>
                                            <select name="id_teaching_mode_of_study" id="id_teaching_mode_of_study" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($modeOfStudyList))
                                                {
                                                    foreach ($modeOfStudyList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->mode_of_program ." - ".$record->mode_of_study; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div> -->


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Learning centre <span class='error-text'>*</span></label>
                                            <select name="id_teaching_learning_center" id="id_teaching_learning_center" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($learningCenterList))
                                                {
                                                    foreach ($learningCenterList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="2">

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="button" onclick="saveTeachingDetails()" class="btn btn-primary btn-lg">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                        </form>



                            <?php

                            if(!empty($getStaffTeachingDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Teaching Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Semester</th>
                                                 <th>Programme Type</th>
                                                 <th>Course</th>
                                                 <th>Mode Of Study</th>
                                                 <th>Organisation</th>
                                                 <th>Created By</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getStaffTeachingDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->semester_code . " - " . $getStaffTeachingDetails[$i]->semester_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->program_code . " - " . $getStaffTeachingDetails[$i]->program_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->course_code . " - " . $getStaffTeachingDetails[$i]->course_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->mode_of_program . " - " . $getStaffTeachingDetails[$i]->mode_of_study;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->organisation_code . " - " . $getStaffTeachingDetails[$i]->organisation_name;?></td>
                                                <td><?php echo $getStaffTeachingDetails[$i]->user;?></td>
                                                <td>
                                                <a onclick="deleteTeachingDetails(<?php echo $getStaffTeachingDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    


                    </div>








                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="mt-4">



                            <form id="form_scheme" action="" method="post">


                                <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="3">
                            <!-- <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Leave Record Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Leave Type <span class='error-text'>*</span></label>
                                            <select name="leave_name" id="leave_name" class="form-control">
                                                <option value="">Select</option>
                                                <option value="Sabbatical">Sabbatical</option>
                                                <option value="Long MC">Long MC</option>
                                                <option value="Maternity">Maternity</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>From Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="leave_from_dt" name="leave_from_dt" >
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>To Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="leave_to_dt" name="leave_to_dt" >
                                        </div>
                                    </div>



                                </div>

                            </div>


                            

                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="3">Save</button>
                                </div>
                            </div> -->

                           <!--  <div class="row">
                                <div id="view_scheme"></div>
                            </div> -->


                        </form>



                            <?php

                            if(!empty($getStaffChangeStatusDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Staff Change Status Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Status</th>
                                                 <th>From Date</th>
                                                 <th>To Date</th>
                                                 <th>Reason</th>
                                                 <th>Created On</th>
                                                 <th>Created By</th>
                                                 <!-- <th>Action</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getStaffChangeStatusDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getStaffChangeStatusDetails[$i]->change_status;?></td>
                                                <td><?php if($getStaffChangeStatusDetails[$i]->from_dt)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffChangeStatusDetails[$i]->from_dt));
                                                } 
                                                ?></td>
                                                <td><?php if($getStaffChangeStatusDetails[$i]->to_dt)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffChangeStatusDetails[$i]->to_dt));
                                                } 
                                                ?></td>
                                                <td><?php echo $getStaffChangeStatusDetails[$i]->reason;?></td>
                                                <td><?php if($getStaffChangeStatusDetails[$i]->created_dt_tm)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffChangeStatusDetails[$i]->created_dt_tm));
                                                } 
                                                ?></td>
                                                <td><?php echo $getStaffChangeStatusDetails[$i]->user;?></td>
                                                <!-- <td>
                                                    <a onclick="deleteStaffChangeStatus(<?php echo $getStaffChangeStatusDetails[$i]->id; ?>)">Delete</a>
                                                </td> -->

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>






                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_four">
                        <div class="mt-4">


                        <form id="form_majoring" action="" method="post">


                            <br>

                                <div class="form-container">
                                    <h4 class="form-group-title"> Coming Soon</h4>
                                    <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="4">
                                </div>

                                <div class="button-block clearfix">
                                    <div class="bttn-group">
                                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                        <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                    </div>
                                </div>



                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_five">
                        <div class="mt-4">


                        <form id="form_minoring" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title">Change Status Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Status <span class='error-text'>*</span></label>
                                             <select name="id_change_status" id="id_change_status" class="form-control" onchange="showLeaveFields(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($changeStatusList)) {
                                                    foreach ($changeStatusList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>" >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>




                                    <div class="col-sm-4" id="view_change_from_date" style="display: none;">
                                        <div class="form-group">
                                            <label>From Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="change_from_dt" name="change_from_dt" >
                                        </div>
                                    </div>



                                    <div class="col-sm-4" id="view_change_to_date" style="display: none;">
                                        <div class="form-group">
                                            <label>To Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="change_to_dt" name="change_to_dt" >
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Reason <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="change_status_reason" name="change_status_reason" >
                                            <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="5">
                                        </div>
                                    </div>



                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                              


                            </form>



                            <?php

                            if(!empty($getStaffChangeStatusDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Staff Change Status Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Status</th>
                                                 <th>From Date</th>
                                                 <th>To Date</th>
                                                 <th>Reason</th>
                                                 <th>Created On</th>
                                                 <th>Created By</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getStaffChangeStatusDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getStaffChangeStatusDetails[$i]->change_status;?></td>
                                                <td><?php if($getStaffChangeStatusDetails[$i]->from_dt)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffChangeStatusDetails[$i]->from_dt));
                                                } 
                                                ?></td>
                                                <td><?php if($getStaffChangeStatusDetails[$i]->to_dt)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffChangeStatusDetails[$i]->to_dt));
                                                } 
                                                ?></td>
                                                <td><?php echo $getStaffChangeStatusDetails[$i]->reason;?></td>
                                                <td><?php if($getStaffChangeStatusDetails[$i]->created_dt_tm)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffChangeStatusDetails[$i]->created_dt_tm));
                                                } 
                                                ?></td>
                                                <td><?php echo $getStaffChangeStatusDetails[$i]->user;?></td>
                                                <td>
                                                    <a onclick="deleteStaffChangeStatus(<?php echo $getStaffChangeStatusDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>









                    <div role="tabpanel" class="tab-pane" id="tab_six">
                        <div class="mt-4">


                        <form id="form_bank_details" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title">Bank Account Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Bank Name <span class='error-text'>*</span></label>
                                             <select name="id_bank" id="id_bank" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($bankList)) {
                                                    foreach ($bankList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>" >
                                                            <?php echo $record->code . " - " . $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>




                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Account Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="bank_account_name" name="bank_account_name" >
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Account Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="bank_account_number" name="bank_account_number" >
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Bank Code <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="bank_code" name="bank_code" >
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Bank Address </label>
                                            <input type="text" class="form-control" id="bank_address" name="bank_address" >
                                            <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="6">
                                        </div>
                                    </div>



                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group">
                                        <button type="button" onclick="saveBankDetails()" class="btn btn-primary btn-lg">Save</button>
                                    <!-- <a href="../list" class="btn btn-link">Back</a> -->
                                </div>
                            </div>

                              


                            </form>



                            <?php

                            if(!empty($getStaffBankDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Staff Bank Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Bank</th>
                                                 <th>Account Name</th>
                                                 <th>Account Number</th>
                                                 <th>Bank Code</th>
                                                 <th>Address</th>
                                                 <th>Created On</th>
                                                 <th>Created By</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getStaffBankDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getStaffBankDetails[$i]->bank_registration_code . " - " . $getStaffBankDetails[$i]->bank_registration_name;?></td>
                                                <td><?php echo $getStaffBankDetails[$i]->bank_account_name;?></td>
                                                <td><?php echo $getStaffBankDetails[$i]->bank_account_number;?></td>
                                                <td><?php echo $getStaffBankDetails[$i]->bank_code;?></td>
                                                <td><?php echo $getStaffBankDetails[$i]->bank_address;?></td>
                                                <td><?php if($getStaffBankDetails[$i]->created_dt_tm)
                                                {
                                                    echo date('d-m-Y',strtotime($getStaffBankDetails[$i]->created_dt_tm));
                                                } 
                                                ?></td>
                                                <td><?php echo $getStaffBankDetails[$i]->user;?></td>
                                                <td>
                                                    <a onclick="deleteStaffBankDetails(<?php echo $getStaffBankDetails[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>













                    <div role="tabpanel" class="tab-pane" id="tab_seven">
                        <div class="mt-4">


                        <form id="form_qualification" action="" method="post" enctype="multipart/form-data">


                            <br>



                            <div class="form-container">
                                <h4 class="form-group-title">Education Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Qualification Level <span class='error-text'>*</span></label>
                                            <select name="qualification_level" id="qualification_level" class="form-control">
                                                <option value="">Select</option>
                                                <option value="diploma">diploma</option>
                                                <option value="degree">degree</option>
                                                <option value="master">master</option>
                                                <option value="PHD">PHD</option>
                                                <option value="professional">professional</option>
                                                <option value="certificate">certificate</option>
                                                <option value="certified">certified</option>
                                                <option value="chartered">chartered</option>
                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Qualification Name <span class='error-text'>*</span> </label>
                                            <input type="text" class="form-control" id="qualification_name" name="qualification_name">
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Awarding Institute <span class='error-text'>*</span> </label>
                                            <input type="text" class="form-control" id="awarding_institute" name="awarding_institute">
                                        </div>
                                    </div>


                                </div>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Country Of Awarding Institution <span class='error-text'>*</span></label>
                                            <select name="country" id="country" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_country)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Year Obtained <span class='error-text'>*</span> </label>
                                            <input type="number" class="form-control" id="year_obtained" name="year_obtained" >
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Upload Certificate <span class='error-text'>*</span> </label>
                                            <input type="file" class="form-control" id="certificate" name="certificate" >
                                            <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="7">
                                        </div>
                                    </div>

                                </div>

                            </div>


                            <div class="button-block clearfix">
                                <div class="bttn-group pull-right">
                                    <button type="button" onclick="saveEducationQualificationDetails()"  class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>


                        </form>



                            <div class="form-container">
                                    <h4 class="form-group-title">Education Qualification Details</h4>

                                <div class="custom-table">
                                  <table class="table" id="list-table">
                                    <thead>
                                      <tr>
                                        <th>Sl. No</th>
                                        <th>Qualification Level</th>
                                        <th>Qualification Name</th>
                                        <th>Awarded Institution</th>
                                        <th>Country Of Institution</th>
                                        <th>Year Obtained</th>
                                        <th>Certificate</th>
                                        <th class="text-center">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <?php
                                      if (!empty($staffEducationQualificationDetails))
                                      {
                                        $i=1;
                                        foreach ($staffEducationQualificationDetails as $record)
                                        {
                                      ?>
                                          <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->level ?></td>
                                            <td><?php echo $record->name ?></td>
                                            <td><?php echo $record->awarding_institute ?></td>
                                            <td><?php echo $record->country ?></td>
                                            <td><?php echo $record->year ?></td>
                                            <td class="text-center">

                                            <?php
                                            if($record->certificate)
                                            {
                                                ?>

                                                <a href="<?php echo '/assets/images/' . $record->certificate; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->certificate; ?>)" title="<?php echo $record->certificate; ?>">View</a>

                                            
                                                <?php
                                            }
                                            ?>
                                            </td>
                                            <td class="text-center">
                                                <a onclick="deleteEducationQualificationDetails(<?php echo $record->id; ?>)">Delete</a>
                                            </td>

                                          </tr>
                                      <?php
                                        $i++;
                                        }
                                      }
                                      ?>
                                    </tbody>
                                  </table>
                                </div>

                            </div>









                    







                        </div>
                
                    
                    </div>


















                    <div role="tabpanel" class="tab-pane" id="tab_eight">
                        <div class="mt-4">


                        <form id="form_experience" action="" method="post" enctype="multipart/form-data">


                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Work Experience Details</h4>

                                    <div class="row">

                                        <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="8">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Organisation Name <span class='error-text'>*</span> </label>
                                                <input type="text" class="form-control" id="organisation_name" name="organisation_name">
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Designation <span class='error-text'>*</span> </label>
                                                <input type="text" class="form-control" id="designation" name="designation" >
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Level <span class='error-text'>*</span> </label>
                                                <input type="text" class="form-control" id="level" name="level" >
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Start Date <span class='error-text'>*</span> </label>
                                                <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>End Date <span class='error-text'>*</span> </label>
                                                <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                                            </div>
                                        </div>


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Upload Employment Letter </label>
                                                <input type="file" class="form-control" id="employment_letter" name="employment_letter">
                                            </div>
                                        </div>

                                        
                                    </div>



                            </div>
                                    
                                


                            <div class="button-block clearfix">
                                <div class="bttn-group pull-right">
                                    <button type="button" onclick="saveWorkExperienceDetails()" class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>


                        

                        </form>



                        <div class="form-container">
                                <h4 class="form-group-title">Work Experience Details</h4>

                            <div class="custom-table">
                              <table class="table" id="list-table">
                                <thead>
                                  <tr>
                                    <th>Sl. No</th>
                                    <th>Organisation Name</th>
                                    <th>Designation</th>
                                    <th>Level</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Employment Letter</th>
                                    <th class="text-center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  if (!empty($staffWorkExperienceDetails))
                                  {
                                    $i=1;
                                    foreach ($staffWorkExperienceDetails as $record) {
                                  ?>
                                      <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->organisation_name ?></td>
                                        <td><?php echo $record->designation ?></td>
                                        <td><?php echo $record->level ?></td>
                                        <td><?php 
                                        if($record->start_date)
                                        {
                                         echo date('d-m-Y', strtotime($record->start_date));
                                        }
                                         ?></td>
                                        <td><?php
                                        if($record->end_date)
                                        {
                                         echo date('d-m-Y', strtotime($record->end_date));
                                        }
                                         ?></td>
                                        <td class="text-center">
                                            <?php 
                                            if($record->employment_letter)
                                            {
                                                ?>


                                            <a href="<?php echo '/assets/images/' . $record->employment_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->employment_letter; ?>)" title="<?php echo $record->employment_letter; ?>">View</a>
                                            <?php
                                            }
                                            else
                                            {
                                                echo 'No File';
                                            }
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <a onclick="deleteWorkExperienceDetails(<?php echo $record->id; ?>)">Delete</a>
                                        </td>
                                        </tr>
                                  <?php
                                  $i++;
                                    }
                                  }
                                  ?>
                                </tbody>
                              </table>
                            </div>

                        </div>


                           
            


                        </div>
                    
                    </div>
















                    <div role="tabpanel" class="tab-pane" id="tab_nine">
                        <div class="mt-4">


                        <form id="form_specialization" action="" method="post">


                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Specialization Details</h4>

                                    <div class="row">

                                        <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="9">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Specialization <span class='error-text'>*</span></label>
                                                 <select name="id_specialization" id="id_specialization" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($specializationList)) {
                                                        foreach ($specializationList as $record) {
                                                    ?>
                                                            <option value="<?php echo $record->id;  ?>" >
                                                                <?php echo $record->code . " - " . $record->name;  ?>        
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                    
                                <div class="button-block clearfix">
                                    <div class="bttn-group pull-right">
                                            <button type="button" onclick="saveSpecializationDetails()" class="btn btn-primary btn-lg">Save</button>
                                    </div>
                                </div>


                        <div class="form-container">
                                <h4 class="form-group-title">Specialization Details</h4>

                            <div class="custom-table">
                              <table class="table" id="list-table">
                                <thead>
                                  <tr>
                                    <th>Sl. No</th>
                                    <th>Specialization Name</th>
                                    <th>Specialization Code</th>
                                    <th class="text-center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  if (!empty($staffSpecializationDetails))
                                  {
                                    $i=1;
                                    foreach ($staffSpecializationDetails as $record) {
                                  ?>
                                      <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->specialization_code ?></td>
                                        <td><?php echo $record->specialization_name ?></td>
                                        <td class="text-center">
                                            <a onclick="deleteSpecializationDetails(<?php echo $record->id; ?>)">Delete</a>
                                        </td>
                                      </tr>
                                  <?php
                                  $i++;
                                    }
                                  }
                                  ?>
                                </tbody>
                              </table>
                            </div>

                        </div>


                           


                        </form>



            


                        </div>
                    
                    </div>







                    <div role="tabpanel" class="tab-pane" id="tab_ten">
                        <div class="mt-4">


                        <form id="form_programme" action="" method="post">


                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Staff Has Programme Details</h4>

                                    <div class="row">

                                        <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="10">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Programme <span class='error-text'>*</span></label>
                                                 <select name="id_programme" id="id_programme" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($programmeList)) {
                                                        foreach ($programmeList as $record) {
                                                    ?>
                                                            <option value="<?php echo $record->id;  ?>" >
                                                                <?php echo $record->code . " - " . $record->name;  ?>        
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                    
                                <div class="button-block clearfix">
                                    <div class="bttn-group pull-right">
                                            <button type="button" onclick="saveStaffHasProgrammeDetails()" class="btn btn-primary btn-lg">Save</button>
                                    </div>
                                </div>


                        <div class="form-container">
                                <h4 class="form-group-title">Staff Has Programme Details</h4>

                            <div class="custom-table">
                              <table class="table" id="list-table">
                                <thead>
                                  <tr>
                                    <th>Sl. No</th>
                                    <th>Programme Name</th>
                                    <th>Programme Code</th>
                                    <th class="text-center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  if (!empty($staffHasProgramme))
                                  {
                                    $i=1;
                                    foreach ($staffHasProgramme as $record)
                                    {
                                  ?>
                                      <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->programme_code ?></td>
                                        <td><?php echo $record->programme_name ?></td>
                                        <td class="text-center">
                                            <a onclick="deleteStaffHasProgrammeDetails(<?php echo $record->id; ?>)">Delete</a>
                                        </td>
                                      </tr>
                                  <?php
                                  $i++;
                                    }
                                  }
                                  ?>
                                </tbody>
                              </table>
                            </div>

                        </div>


                           


                        </form>



            


                        </div>
                    
                    </div>







                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script type="text/javascript">


    $('select').select2();


    function getlabel()
    {
      var labelnric = $("#id_type").val();
      //alert(labelnric);
      $("#labelspanid").html(labelnric);
    }



    $(function()
    {
        $(".datepicker").datepicker(
        {
            changeYear: true,
            changeMonth: true,
        });
    });


    function showLeaveFields(id_status)
    {
        if(id_status > 6)
        {
            $("#view_change_from_date").show();
            $("#view_change_to_date").show();
        }
        else
        {
            $("#view_change_from_date").hide();
            $("#view_change_to_date").hide();
        }

    }


    $( document ).ready(function()
    {
        $.get("/partner_university/staff/getStateByCountry/"+<?php echo $staffDetails->id_country;?>, function(data, status)
        {
            var idstateselected = "<?php echo $staffDetails->id_state;?>";

            $("#view_state").html(data);
            $("#id_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
            $('select').select2();
        });
    });



    function getStateByCountry(id)
    {
     $.get("/partner_university/staff/getStateByCountry/"+id, function(data, status)
     {
        $("#view_state").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
     });
    }



    function saveData()
    {
        var tempPR = {};
        tempPR['id_course'] = $("#id_course").val();
        tempPR['id'] = <?php echo $id_staff;?>;
            $.ajax(
            {
               url: '/partner_university/staff/directadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
                    // $("#view").html(result);
               }
            });
    }


    function getProgramModeByProgram(id_program)
    {
        // alert(id_program);
        $.get("/partner_university/staff/getSchemeByProgramId/"+id_program, function(data, status)
        {
            $("#view_program_mode").html(data);
        });
    }



    function deleteCourseDetailData(id) {
         $.ajax(
            {
               url: '/partner_university/staff/deleteCourseDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    window.location.reload();
               }
            });
    }


    function deleteTeachingDetails(id)
    {
        // alert(id);

        $.ajax(
            {
               url: '/partner_university/staff/deleteTeachingDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function deleteStaffChangeStatus(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteStaffChangeStatus/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }
    

    function deleteStaffLeaveDetails(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteStaffLeaveDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }

    function deleteStaffBankDetails(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteStaffBankDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function validateDetailsData()
    {
        if($('#form_staff').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Course To The Faculty.");
            }
            else
            {
                $('#form_staff').submit();
            }
        }    
    }

    function saveTeachingDetails()
    {
        if($('#form_teaching_details').valid())
        {
            $('#form_teaching_details').submit();    
        }
    }

    function saveBankDetails()
    {
        if($('#form_bank_details').valid())
        {
            $('#form_bank_details').submit();    
        }
    }

    function saveEducationQualificationDetails()
    {
        if($('#form_qualification').valid())
        {
            $('#form_qualification').submit();    
        }
    }


    function saveWorkExperienceDetails()
    {
        if($('#form_experience').valid())
        {
            $('#form_experience').submit();    
        }
    }

    function saveSpecializationDetails()
    {
        if($('#form_specialization').valid())
        {
            $('#form_specialization').submit();    
        }
    }

    function saveStaffHasProgrammeDetails()
    {
        if($('#form_programme').valid())
        {
            $('#form_programme').submit();    
        }
    }


    function checkDemo()
    {
        var tempPR = {};
        tempPR['id_student'] = '93';
        tempPR['id_programme'] = '12';
        tempPR['is_discount'] = 0;
        tempPR['type'] = 'Student';

            $.ajax(
            {
               url: '/profile/dashboard/generateInvoice',
                type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // window.location.reload();
                    // $("#view").html(result);
                    alert(result);
               }
            });
    }


    function deleteEducationQualificationDetails(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteEducationQualificationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteWorkExperienceDetails(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteWorkExperienceDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteSpecializationDetails(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteSpecializationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }


    function deleteStaffHasProgrammeDetails(id)
    {
        $.ajax(
            {
               url: '/partner_university/staff/deleteStaffHasProgrammeDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    alert('Deleted Sessessfully');
                    location.reload();
               }
            });
    }








    $(document).ready(function()
    {
        getlabel();

        $("#form_staff").validate(
        {
            rules:
            {
                salutation:
                {
                    required: true
                },
                first_name:
                {
                    required: true
                },
                last_name:
                {
                    required: true
                },
                ic_no:
                {
                    required: true
                },
                gender:
                {
                    required: true
                },
                mobile_number:
                {
                    required: true
                },
                phone_number:
                {
                    required: true
                },
                address:
                {
                    required: true
                },
                address_two:
                {
                    required: true
                },
                id_country:
                {
                    required: true
                },
                id_state:
                {
                    required: true
                },
                zipcode:
                {
                    required: true
                },
                job_type:
                {
                    required: true
                },
                email:
                {
                    required: true
                },
                staff_id:
                {
                    required: true
                },
                dob:
                {
                    required: true
                },
                academic_type:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_education_level:
                {
                    required: true
                },
                id_type:
                {
                    required: true
                },
                status:
                {
                    required: true
                },
                nationality:
                {
                    required: true
                },
                whatsapp_number : {
                    required: true
                },
                joined_date: {
                    required: true
                }
            },
            messages:
            {
                salutation:
                {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name:
                {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name:
                {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                ic_no:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                gender:
                {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mobile_number:
                {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                phone_number:
                {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                address:
                {
                    required: "<p class='error-text'>Faculty Address Required</p>",
                },
                address_two:
                {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                id_country:
                {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state:
                {
                    required: "<p class='error-text'>Select State</p>",
                },
                zipcode:
                {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                job_type:
                {
                    required: "<p class='error-text'>Select Job Type</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                staff_id:
                {
                    required: "<p class='error-text'>Enter Faculty ID</p>",
                },
                dob:
                {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                academic_type:
                {
                    required: "<p class='error-text'>Select Academic Type</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_education_level:
                {
                    required: "<p class='error-text'>Select Highest Education Level</p>",
                },
                id_type:
                {
                    required: "<p class='error-text'>Select ID Type</p>",
                },
                status:
                {
                    required: "<p class='error-text'>Select Status</p>",
                },
                nationality:
                {
                    required: "<p class='error-text'>Select Nationality</p>",
                },
                whatsapp_number:
                {
                    required: "<p class='error-text'>WhatsApp No. Required</p>",
                },
                joined_date:
                {
                    required: "<p class='error-text'>Select Joined Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    $(document).ready(function()
    {
        $("#form_teaching_details").validate(
        {
            rules:
            {
                id_teaching_semester:
                {
                    required: true
                },
                id_teaching_programme:
                {
                    required: true
                },
                id_teaching_course:
                {
                    required: true
                },
                id_teaching_mode_of_study:
                {
                    required: true
                },
                id_teaching_learning_center:
                {
                    required: true
                }
            },
            messages:
            {
                id_teaching_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_teaching_programme:
                {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_teaching_course:
                {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_teaching_mode_of_study:
                {
                    required: "<p class='error-text'>Select Mode Of Study</p>",
                },
                id_teaching_learning_center:
                {
                    required: "<p class='error-text'>Select Learning Center</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(document).ready(function()
    {
        $("#form_bank_details").validate(
        {
            rules:
            {
                id_bank:
                {
                    required: true
                },
                bank_account_name:
                {
                    required: true
                },
                bank_account_number:
                {
                    required: true
                },
                bank_code:
                {
                    required: true
                },
                bank_address:
                {
                    required: true
                }
            },
            messages:
            {
                id_bank:
                {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                bank_account_name:
                {
                    required: "<p class='error-text'>Account Holder Name Required</p>",
                },
                bank_account_number:
                {
                    required: "<p class='error-text'>Bank Account Number Required</p>",
                },
                bank_code:
                {
                    required: "<p class='error-text'>Bank Code Required</p>",
                },
                bank_address:
                {
                    required: "<p class='error-text'>Bank Address Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
    {
        $("#form_qualification").validate(
        {
            rules:
            {
                qualification_level:
                {
                    required: true
                },
                qualification_name:
                {
                    required: true
                },
                awarding_institute:
                {
                    required: true
                },
                country:
                {
                    required: true
                },
                year_obtained:
                {
                    required: true
                },
                certificate: {
                    required: true
                }
            },
            messages:
            {
                qualification_level:
                {
                    required: "<p class='error-text'>Select Level </p>",
                },
                qualification_name:
                {
                    required: "<p class='error-text'>Qualification Name Required</p>",
                },
                awarding_institute:
                {
                    required: "<p class='error-text'>Awarding Institute Required</p>",
                },
                country:
                {
                    required: "<p class='error-text'>Select The Country</p>",
                },
                year_obtained:
                {
                    required: "<p class='error-text'>Year Obtained Required</p>",
                },
                certificate:
                {
                    required: "<p class='error-text'>Select Certificate</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
    {
        $("#form_experience").validate(
        {
            rules:
            {
                organisation_name:
                {
                    required: true
                },
                designation:
                {
                    required: true
                },
                level:
                {
                    required: true
                },
                start_date:
                {
                    required: true
                }
            },
            messages:
            {
                organisation_name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                designation:
                {
                    required: "<p class='error-text'>Designatin Required</p>",
                },
                level:
                {
                    required: "<p class='error-text'>Level Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Select Start Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_specialization").validate(
        {
            rules:
            {
                id_specialization:
                {
                    required: true
                }
            },
            messages:
            {
                id_specialization:
                {
                    required: "<p class='error-text'>Select Specialization</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function()
    {
        $("#form_programme").validate(
        {
            rules:
            {
                id_programme:
                {
                    required: true
                }
            },
            messages:
            {
                id_programme:
                {
                    required: "<p class='error-text'>Select Programme</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>