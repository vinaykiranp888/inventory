<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Change Partner University Password</h3>
            </div>



    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Partner University Details
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">


            <div class="panel-body">




                <div class="form-container">
                  <h4 class="form-group-title">Partner Details</h4>
                      

                      <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Code <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="code" name="code" value="<?php echo $partnerUniversity->code; ?>" readonly>
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Name <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="name" name="name" value="<?php echo $partnerUniversity->name; ?>" readonly>
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Short Name <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="short_name" name="short_name" value="<?php echo $partnerUniversity->short_name; ?>" readonly>
                              </div>
                          </div>


                      </div>




                      <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Name In Other Language</label>
                                  <input type="text" class="form-control" id="name_in_malay" name="name_in_malay" value="<?php echo $partnerUniversity->name_in_malay; ?>" readonly>
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Url <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="url" name="url" value="<?php echo $partnerUniversity->url; ?>" readonly>
                              </div>
                          </div>

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Login ID <span class='error-text'>*</span></label>
                                  <input type="text" class="form-control" id="login_id" name="login_id" value="<?php echo $partnerUniversity->login_id; ?>" readonly>
                              </div>
                          </div>                


                      </div>


              </div>



              <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

                  <div class="row">

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Contact Number <span class='error-text'>*</span></label>
                              <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $partnerUniversity->contact_number; ?>" readonly>
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Contact Email <span class='error-text'>*</span></label>
                              <input type="email" class="form-control" id="email" name="email" value="<?php echo $partnerUniversity->email; ?>" readonly>
                          </div>
                      </div>


                      
                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Address 1 <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $partnerUniversity->address1 ?>" readonly>
                          </div>
                      </div>

                  </div>





                  <div class="row">


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Address 2</label>
                              <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $partnerUniversity->address2 ?>" readonly>
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>City <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="city" name="city" value="<?php echo $partnerUniversity->city ?>" readonly>
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Zipcode <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $partnerUniversity->zipcode ?>" readonly>
                          </div>
                      </div>
                  </div>


              </div>




                



              
            </div>
          </div>
        </form>
      </div>
    </div>


    <br>


    <form id="form_programme" action="" method="post">
        
    
      <div class="form-container">
         <h4 class="form-group-title">Change Password Here</h4>

         <div class="row">
         
            <div class="col-sm-4">
               <div class="form-group">
                  <label>Enter Current Password <span class='error-text'>*</span></label>
                  <input type="password" class="form-control" id="old_password" name="old_password" onblur="checkPartnerUniversityPassword()">
               </div>
            </div>           
            
         </div>
      
      </div>





      <div class="button-block clearfix">
          <div class="bttn-group">
              <button type="button" class="btn btn-primary btn-lg" name="btn" id="btn" disabled="true" onclick="openModalDialogue()">Continue</button>
          </div>
      </div>


   



      <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">


                  <div class="form-container">
                    <h4 class="form-group-title"> New Password Details</h4>

                    <div class="row">


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>New Password <span class='error-text'>*</span></label>
                                <input type="password" class="form-control" id="new_password" name="new_password" onblur="enableConfirmPassword()">
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Confirm Password <span class='error-text'>*</span></label>
                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" readonly>
                            </div>
                        </div>


                      </div>



                  </div>
              
              </form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="updateCompanyUserPassword()">Update Password</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div>



            </div>
            </div>

          </div>
      

      </div>


    </form>


        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>


    </div>
</div>

<script>

    $('select').select2();


    $(function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    function openModalDialogue()
    {
      $('#myModal').modal('show');
    }


    function checkPartnerUniversityPassword()
    {
      if($('#form_programme').valid())
      {

        var old_password = $("#old_password").val();


        if(old_password != '')
        {

            var tempPR = {};
            tempPR['id_partner_university'] = '<?php echo $id_partner_university; ?>;'
            tempPR['old_password'] = old_password;
            // alert(tempPR['id_program']);

                $.ajax(
                {
                   url: '/partner_university/partnerUniversity/checkPartnerUniversityPassword',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                      // alert(result);
                      if(result == '0')
                      {
                          document.getElementById("btn").disabled = true;                            
                          alert('Check The Password Entered, Password Is Incorrect');
                          // $("#id_intake").val('');
                      }
                      else if(result == '1')
                      {
                          document.getElementById("btn").disabled = false;
                      }
                   }
                });
        }

      }
    }

    function enableConfirmPassword()
    {
        var new_password = $("#new_password").val();
        // alert(new_password);

        if(new_password != '')
        {
          document.getElementById("confirm_password").removeAttribute('readonly');
        }
    }

    function updateCompanyUserPassword()
    {
        var new_password = $("#new_password").val();
        var confirm_password = $("#confirm_password").val();
        // alert(confirm_password);

        if(new_password == confirm_password)
        {
          // changeCompanyUserPassword();
          $('#form_programme').submit();
        }
        else
        {
          alert("Entered Paswword's Doesn't Match. Try Again");
          $("#new_password").val('');
          $("#confirm_password").val('');
          document.getElementById("confirm_password").readOnly = true;
        }
    }

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                old_password: {
                    required: true
                },
                new_password: {
                    required: true
                },
                confirm_password: {
                    required: true
                }
            },
            messages: {
                old_password: {
                    required: "<p class='error-text'>Old Password Required</p>",
                },
                new_password: {
                    required: "<p class='error-text'>New Password Required</p>",
                },
                confirm_password: {
                    required: "<p class='error-text'>Confirm Password Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>