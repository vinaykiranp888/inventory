<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Parter University Trainng Center</h3>
        </div>


        <br>


        <div class="topnav">
          <a href="<?php echo '../edit/' . $id; ?>" title="Partner University Info">Parter University Info</a> | 
          <a href="<?php echo '../addTrainingCenterInfo/' . $id; ?>" title="Trainnig Center Info" style="background: #aaff00">Traing Centers Info</a> | 
          <a href="<?php echo '../addAggrementInfo/' . $id; ?>" title="Aggrement Info">Aggrement Info</a>
        </div>

        <br>



            <form id="form_training" action="" method="post">


            <br>

            <div class="form-container">
                <h4 class="form-group-title"> Training Center Details</h4>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="training_code" name="training_code"  onchange="codeConcate()">
                            <input type="hidden" class="form-control" id="code" name="code" value="<?php echo $partnerUniversity->code; ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="training_name" name="training_name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Complete Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="training_complete_code" name="training_complete_code" readonly>
                        </div>
                    </div>

                </div>  


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="training_status" id="training_status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="training_status" id="training_status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>  
            </div>                                

            </div>


            <div class="form-container">
                <h4 class="form-group-title">Training Center Contact Details</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Contact Person <span class='error-text'>*</span></label>
                            <select name="id_training_contact_person" id="id_training_contact_person" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($staffList))
                                {
                                    foreach ($staffList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->ic_no . " - " . $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Contact Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="training_contact_number" name="training_contact_number" >
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Contact Email <span class='error-text'>*</span></label>
                            <input type="email" class="form-control" id="training_email" name="training_email">
                        </div>
                    </div>



                </div>


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fax Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="training_fax" name="training_fax">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="training_address1" name="training_address1">
                        </div>
                    </div>

                

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Address 2 </label>
                            <input type="text" class="form-control" id="training_address2" name="training_address2">
                        </div>
                    </div>

                    

                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Location <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="training_location" name="training_location">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Country <span class='error-text'>*</span></label>
                            <select name="id_training_country" id="id_training_country" class="form-control" onchange="getStateByCountry(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                
                
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>State <span class='error-text'>*</span></label>
                            <span id="view_state">
                             <select class="form-control" id='id_training_state' name='id_training_state'>
                                <option value=''></option>
                              </select>
                            </span>
                        </div>
                    </div>

                </div>


                <div class="row">                               

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="training_city" name="training_city">
                        </div>
                    </div>

                

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="training_zipcode" name="training_zipcode">
                        </div>
                    </div>
                </div>
            </div>




            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="saveTrainingCenterData()" name="btn_submit" value="2">Add</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>

           <!--  <div class="row">
                <div id="view_scheme"></div>
            </div> -->


            </form>



            <?php

            if(!empty($trainingCenterList))
            {
                ?>
                <br>

                <div class="form-container">
                        <h4 class="form-group-title">Program Training Center Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Center Name</th>
                                 <th>City</th>
                                 <th>Contact Number</th>
                                 <th>Contact Email</th>
                                 <th class="text-center">Status</th>
                                 <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($trainingCenterList);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $trainingCenterList[$i]->code . " - " . $trainingCenterList[$i]->name;?></td>
                                <td><?php echo $trainingCenterList[$i]->city;?></td>
                                <td><?php echo $trainingCenterList[$i]->contact_number;?></td>
                                <td><?php echo $trainingCenterList[$i]->email;?></td>
                                <td style="text-align: center;"><?php if( $record->status == '1')
                                  {
                                    echo "Active";
                                  }
                                  else
                                  {
                                    echo "In-Active";
                                  } 
                                  ?>      
                                </td>
                                <td class="text-center">
                                <a onclick="deleteTrainingCenter(<?php echo $trainingCenterList[$i]->id; ?>)">Delete</a>
                                </td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>




            <?php
            
            }
             ?>







    



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



<script type="text/javascript">

    $('select').select2();


    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });


    function codeConcate()
    {
        var d='_';
        document.getElementById('training_complete_code').value = document.getElementById('code').value + d+document.getElementById('training_code').value;
    }


    function getStateByCountry(id)
    {
        // alert(id);
        $.get("/pm/partnerUniversity/getStateByCountryForTraining/"+id, function(data, status){
            
            // alert(data);
            $("#view_state").html(data);
        });
    }
    

    function saveTrainingCenterData()
    {
        if($('#form_training').valid())
        {


        var tempPR = {};
        tempPR['code'] = $("#training_code").val();
        tempPR['name'] = $("#training_name").val();
        tempPR['complete_code'] = $("#training_complete_code").val();
        tempPR['status'] = $("#training_status").val();
        tempPR['contact_number'] = $("#training_contact_number").val();
        tempPR['email'] = $("#training_email").val();
        tempPR['fax'] = $("#training_fax").val();
        tempPR['address1'] = $("#training_address1").val();
        tempPR['address2'] = $("#training_address2").val();
        tempPR['location'] = $("#training_location").val();
        tempPR['id_country'] = $("#id_training_country").val();
        tempPR['id_state'] = $("#id_training_state").val();
        tempPR['city'] = $("#training_city").val();
        tempPR['zipcode'] = $("#training_zipcode").val();
        tempPR['id_contact_person'] = $("#id_training_contact_person").val();
        tempPR['status'] = 1;
        tempPR['id_organisation'] = <?php echo $partnerUniversity->id; ?>;

        //  This is Changed To Another Table from scholarship_partner_university_has_training_center -> organisation_has_training_center
        // tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;


            $.ajax(
            {
               url: '/pm/partnerUniversity/addTrainingCenter',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view_aggrement_data").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }


    function deleteTrainingCenter(id)
    {
         $.ajax(
            {
               url: '/pm/partnerUniversity/deleteTrainingCenter/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    // window.location.reload();
                    location.reload();
               }
            });
    }

    


    $(document).ready(function() {
        $("#form_training").validate({
            rules: {
                training_code: {
                    required: true
                },
                training_name: {
                    required: true
                },
                training_status: {
                    required: true
                },
                training_contact_number: {
                    required: true
                },
                training_email: {
                    required: true
                },
                training_fax: {
                    required: true
                },
                training_address1: {
                    required: true
                },
                id_training_country: {
                    required: true
                },
                training_city: {
                    required: true
                },
                training_zipcode: {
                    required: true
                },
                id_training_state: {
                    required: true
                },
                id_training_contact_person : {
                    required : true
                },
                training_location : {
                    required : true
                }
            },
            messages: {
                training_code: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                training_name: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                training_status: {
                    required: "<p class='error-text'>Training Status Required</p>",
                },
                training_contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                training_email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                training_fax: {
                    required: "<p class='error-text'>Fax Number Required</p>",
                },
                training_address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                id_training_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                training_city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                training_zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                id_training_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                id_training_contact_person: {
                    required: "<p class='error-text'>Select Contact Person</p>",
                },
                training_location: {
                    required: "<p class='error-text'>Training Location Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    
</script>
