<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>View Partner University </h3>
        </div>



            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Profile</a>
                    </li>

                    <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Training Center</a>
                    </li>

                    <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Program</a>
                    </li>

                    <li role="presentation"><a href="#tab_four" class="nav-link border rounded text-center"
                            aria-controls="tab_four" role="tab" data-toggle="tab">Aggrement</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="col-12 mt-4">




                <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Partner University Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $partnerUniversity->code; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $partnerUniversity->name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="short_name" name="short_name" value="<?php echo $partnerUniversity->short_name; ?>">
                    </div>
                </div>


                
            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language</label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay" value="<?php echo $partnerUniversity->name_in_malay; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Url <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $partnerUniversity->url; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Partner Category <span class='error-text'>*</span></label>
                        <select name="id_partner_category" id="id_partner_category" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($partnerCategoryList))
                            {
                                foreach ($partnerCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $partnerUniversity->id_partner_category)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>



            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Login ID <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="login_id" name="login_id" value="<?php echo $partnerUniversity->login_id; ?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password <span class='error-text'>*</span></label>
                        <input type="password" class="form-control" id="password" name="password" value="<?php echo $partnerUniversity->password; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Billing To <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="billing_to" id="billing_to" value="Student" <?php if($partnerUniversity->billing_to=='Student') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Student
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="billing_to" id="billing_to" value="Partner University" <?php if($partnerUniversity->billing_to=='Partner University') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> Partner University
                            </label>                              
                        </div>                         
                </div> 


            </div>

            <div class="row">




                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($partnerUniversity->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($partnerUniversity->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
            </div>

        </div>


        
                    <?php
                        if($partnerUniversity->partner_category == 'Franchise')
                        {

                         ?>

                         <!--    <td class="text-center">

                                    <a href="<?php echo '/assets/images/' . $record->transcript; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->transcript; ?>)" title="View">
                                    <span style='font-size:18px;'>&#128065;</span>
                                    </a>

                                </td> -->

                    <div class="form-container">
                    <h4 class="form-group-title">Partner University Details</h4>

                     <div class="row">


                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Start Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($partnerUniversity->start_date)); ?>">
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>End Date <span class='error-text'>*</span></label>
                                <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($partnerUniversity->end_date)); ?>">
                            </div>
                        </div>



                        <div class="col-sm-4">
                            <div class="forintake_has_programmem-group">
                                <label>Certificate <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="certificate" name="certificate">
                                <input type="hidden" class="form-control" id="cert" name="cert" value="<?php echo $partnerUniversity->certificate; ?>">
                                <br>
                                <a href="<?php echo '/assets/images/' . $partnerUniversity->certificate; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $partnerUniversity->certificate; ?>)" title="<?php echo $partnerUniversity->certificate; ?>">View</a>
                                    <!-- <span style='font-size:18px;'>&#128065;</span> -->


                                <!-- <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php echo date('d-m-Y', strtotime($partnerUniversity->end_date)); ?>"> -->
                            </div>
                        </div>




                        </div>

                    </div>


                         <?php

                            }
                            
                         ?>

        <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $partnerUniversity->contact_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $partnerUniversity->email; ?>">
                    </div>
                </div>


                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $partnerUniversity->address1 ?>">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $partnerUniversity->address2 ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($partnerUniversity->id_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($partnerUniversity->id_state==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $partnerUniversity->city ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $partnerUniversity->zipcode ?>">
                    </div>
                </div>
            </div>
        </div>


        </form>



        <?php

            if(!empty($comiteeList))
            {
                ?>

                <div class="form-container">
                        <h4 class="form-group-title">Comitee Details</h4>

                    

                      <div class="custom-table">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>Sl. No</th>
                                 <th>Role</th>
                                 <th>Name</th>
                                 <th>NRIC</th>
                                 <th>Effective Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                 <?php
                             $total = 0;
                              for($i=0;$i<count($comiteeList);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $comiteeList[$i]->role;?></td>
                                <td><?php echo $comiteeList[$i]->name;?></td>
                                <td><?php echo $comiteeList[$i]->nric;?></td>
                                <td><?php echo date('d-m-Y', strtotime($comiteeList[$i]->effective_date));?></td>

                                 </tr>
                              <?php 
                          } 
                          ?>
                            </tbody>
                        </table>
                      </div>

                    </div>




            <?php
            
            }
             ?>






                    
                                   



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="mt-4">


                            <?php

                            if(!empty($trainingCenterList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Program Training Center Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Center Name</th>
                                                 <th>City</th>
                                                 <th>Contact Number</th>
                                                 <th>Contact Email</th>
                                                 <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($trainingCenterList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $trainingCenterList[$i]->code . " - " . $trainingCenterList[$i]->name;?></td>
                                                <td><?php echo $trainingCenterList[$i]->city;?></td>
                                                <td><?php echo $trainingCenterList[$i]->contact_number;?></td>
                                                <td><?php echo $trainingCenterList[$i]->email;?></td>
                                                <td style="text-align: center;"><?php if( $record->status == '1')
                                                  {
                                                    echo "Active";
                                                  }
                                                  else
                                                  {
                                                    echo "In-Active";
                                                  } 
                                                  ?>      
                                                </td>
                                                </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="mt-4">


                        <form id="form_program" action="" method="post">


                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Details</h4>

                                    <!-- <h3>&emsp; Coming Soon... !</h3> -->




                                <?php 
                                if(!empty($partnerProgramDetails))
                                {
                                    ?>


                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Program <span class='error-text'>*</span></label>
                                                <select name="id_detail_program" id="id_detail_program" class="form-control" style="width: 408px">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($programList))
                                                    {
                                                        foreach ($programList as $record)
                                                        {?>
                                                    <option value="<?php echo $record->id;  ?>"  <?php if($partnerProgramDetails->id_program==$record->id){ echo "selected"; } ?>>
                                                        <?php echo $record->code . " - " . $record->name;?>
                                                    </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <div class="form-group">
                                                <label>Completion</label>
                                                <input type="number" class="form-control" id="detail_completion_criteria" name="detail_completion_criteria" value="<?php echo $partnerProgramDetails->completion_criteria; ?>">

                                                <input type="hidden" class="form-control" id="id_detail" name="id_detail" value="<?php echo $partnerProgramDetails->id; ?>">
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Criteria <span class='error-text'>*</span></label>
                                                <select name="detail_completion_criteria_type" id="detail_completion_criteria_type" class="form-control" style="width: 308px">
                                                    <option value="">Select</option>
                                                    <option value="Credit Hours" <?php if($partnerProgramDetails->completion_criteria_type=='Credit Hours'){ echo "selected"; } ?>>Credit Hours</option>
                                                    <option value="Total Module" <?php if($partnerProgramDetails->completion_criteria_type=='Total Module'){ echo "selected"; } ?>>Total Module</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Assesment Method <span class='error-text'>*</span></label>
                                                <select name="detail_assesment_method" id="detail_assesment_method" class="form-control" style="width: 408px">
                                                    <option value="">Select</option>
                                                    <option value="CGPA" <?php if($partnerProgramDetails->assesment_method=='CGPA'){ echo "selected"; } ?>>CGPA</option>
                                                    <option value="Grade" <?php if($partnerProgramDetails->assesment_method=='Grade'){ echo "selected"; } ?>>Grade</option>
                                                    <option value="Pass/Fail" <?php if($partnerProgramDetails->assesment_method=='Pass/Fail'){ echo "selected"; } ?>>Pass/Fail</option>
                                                    <option value="Attendence" <?php if($partnerProgramDetails->assesment_method=='Attendence'){ echo "selected"; } ?>>Attendence</option>
                                                </select>
                                            </div>
                                        </div>




                                    </div>



                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <p>Status <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="details_program_status" id="details_program_status" value="1" <?php if($partnerProgramDetails->status=='1') {
                                                 echo "checked=checked";
                                              };?>><span class="check-radio"></span> Active
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="details_program_status" id="details_program_status" value="0" <?php if($partnerProgramDetails->status=='0') {
                                                 echo "checked=checked";
                                              };?>>
                                              <span class="check-radio"></span> In-Active
                                            </label>                
                                        </div>
                                    </div>

                                </div>

                            <?php 
                                }
                            ?>

                            </div>


                            </form>



                            


                        <!-- Another Tab For Program Details STARTS HERE-->

           <?php

        if(!empty($partnerProgramDetails))
        {
        ?>




            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_program_one" class="nav-link border rounded text-center"
                            aria-controls="tab_program_one" aria-selected="true"
                            role="tab" data-toggle="tab">Study Mode</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_two" class="nav-link border rounded text-center"
                            aria-controls="tab_program_two" role="tab" data-toggle="tab">Internship</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_three" class="nav-link border rounded text-center"
                            aria-controls="tab_program_three" role="tab" data-toggle="tab">Apprenticeship</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_four" class="nav-link border rounded text-center"
                            aria-controls="tab_program_four" role="tab" data-toggle="tab">Syllabus</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">




                    <div role="tabpanel" class="tab-pane active" id="tab_program_one">
                        <div class="col-12 mt-4">





                        <?php

                            if(!empty($partnerProgramStudyModeDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Mode Of Study List</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Mode Of Study</th>
                                                 <th>Min. Duratin</th>
                                                 <th>Max. Duratin</th>
                                                 <th>Training Center</th>
                                                 <th>City</th>
                                                 <th>Location</th>
                                                 <th>Start Date</th>
                                                 <th>End Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($partnerProgramStudyModeDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $partnerProgramStudyModeDetails[$i]->mode_of_study;?></td>
                                                <td><?php echo $partnerProgramStudyModeDetails[$i]->min_duration . " - " . $partnerProgramStudyModeDetails[$i]->min_duration_type;?></td>
                                                <td><?php echo $partnerProgramStudyModeDetails[$i]->max_duration . " - " . $partnerProgramStudyModeDetails[$i]->max_duration_type;?></td>
                                                <td><?php echo $partnerProgramStudyModeDetails[$i]->training_code . " - " . $partnerProgramStudyModeDetails[$i]->training_name;?></td>
                                                <td><?php echo $partnerProgramStudyModeDetails[$i]->city;?></td>
                                                <td><?php echo $partnerProgramStudyModeDetails[$i]->location;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($partnerProgramStudyModeDetails[$i]->start_date));?></td>
                                                <td><?php echo date('d-m-Y', strtotime($partnerProgramStudyModeDetails[$i]->end_date));?></td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

                            <?php
                            
                            }
                             ?>

                                 
                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="tab_program_two">
                        <div class="mt-4">



                        <?php

                            if(!empty($partnerProgramInternshipDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Internship List</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Duration</th>
                                                 <th>Internship Center</th>
                                                 <th>City</th>
                                                 <th>Location</th>
                                                 <th>Start Date</th>
                                                 <th>End Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($partnerProgramInternshipDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $partnerProgramInternshipDetails[$i]->duration . " - " . $partnerProgramInternshipDetails[$i]->duration_type;?></td>
                                                <td><?php echo $partnerProgramInternshipDetails[$i]->internship_code . " - " . $partnerProgramInternshipDetails[$i]->internship_name;?></td>
                                                <td><?php echo $partnerProgramInternshipDetails[$i]->city;?></td>
                                                <td><?php echo $partnerProgramInternshipDetails[$i]->location;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($partnerProgramInternshipDetails[$i]->start_date));?></td>
                                                <td><?php echo date('d-m-Y', strtotime($partnerProgramInternshipDetails[$i]->end_date));?></td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_program_three">
                        <div class="mt-4">


                        <?php

                            if(!empty($partnerProgramApprenticeshipDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Apprenticeship List</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Duration</th>
                                                 <th>Internship Center</th>
                                                 <th>City</th>
                                                 <th>Location</th>
                                                 <th>Start Date</th>
                                                 <th>End Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($partnerProgramApprenticeshipDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $partnerProgramApprenticeshipDetails[$i]->duration . " - " . $partnerProgramApprenticeshipDetails[$i]->duration_type;?></td>
                                                <td><?php echo $partnerProgramApprenticeshipDetails[$i]->apprenticeship_code . " - " . $partnerProgramApprenticeshipDetails[$i]->apprenticeship_name;?></td>
                                                <td><?php echo $partnerProgramApprenticeshipDetails[$i]->city;?></td>
                                                <td><?php echo $partnerProgramApprenticeshipDetails[$i]->location;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($partnerProgramApprenticeshipDetails[$i]->start_date));?></td>
                                                <td><?php echo date('d-m-Y', strtotime($partnerProgramApprenticeshipDetails[$i]->end_date));?></td>

                                                </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_program_four">
                        <div class="mt-4">


                        <?php

                            if(!empty($partnerProgramSyllabusDetails))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Syllabus List</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Module Code</th>
                                                 <th>Module Name</th>
                                                 <th>Module Credit</th>
                                                 <th>Module Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($partnerProgramSyllabusDetails);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $partnerProgramSyllabusDetails[$i]->code;?></td>
                                                <td><?php echo $partnerProgramSyllabusDetails[$i]->name;?></td>
                                                <td><?php echo $partnerProgramSyllabusDetails[$i]->credit . " - " . $partnerProgramSyllabusDetails[$i]->credit_type;?></td>
                                                <td><?php echo $partnerProgramSyllabusDetails[$i]->module_code . " - " . $partnerProgramSyllabusDetails[$i]->module_name;?></td>
                                                
                                                </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>




                        </div>
                    
                    </div>





            </div>

        </div> 



        <?php

        }
        ?>





                            <!-- Another Tab For Program Details ENDS HERE-->




                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_four">
                        <div class="mt-4">


                            <?php

                            if(!empty($getPartnerUniversityAggrementList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">MOA Aggrement List</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th>MOA Start Date</th>
                                                 <th>MOA End Date</th>
                                                 <th>Reminder (Months)</th>
                                                 <th>Currency</th>
                                                 <th class="text-center">MOA File</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getPartnerUniversityAggrementList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $getPartnerUniversityAggrementList[$i]->name;?></td>               
                                                <td><?php echo date('d-m-Y', strtotime($getPartnerUniversityAggrementList[$i]->start_date));?></td>
                                                <td><?php echo date('d-m-Y', strtotime($getPartnerUniversityAggrementList[$i]->end_date));?></td>
                                                <td><?php echo $getPartnerUniversityAggrementList[$i]->reminder_months;?></td>               
                                                <td><?php echo $getPartnerUniversityAggrementList[$i]->currency_code . " - " . $getPartnerUniversityAggrementList[$i]->currency_name;?></td>               
                                                <td class="text-center">

                                                    <a href="<?php echo '/assets/images/' . $getPartnerUniversityAggrementList[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $getPartnerUniversityAggrementList[$i]->file; ?>)" title="<?php echo $getPartnerUniversityAggrementList[$i]->file; ?>">View</a>
                                                </td>
                                                </tr>
                                              <?php
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>





                        </div>
                    
                    </div>


                </div>


            </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script>

    function saveData()
    {
        if($('#form_comitee').valid())
        {

        var tempPR = {};
        tempPR['role'] = $("#role").val();
        tempPR['name'] = $("#com_name").val();
        tempPR['nric'] = $("#com_nric").val();
        tempPR['effective_date'] = $("#effective_date").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id;?>;
            $.ajax(
            {
               url: '/setup/partnerUniversity/addComitee',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function deleteOrganisationConitee(id)
    {
        $.ajax(
            {
               url: '/setup/partnerUniversity/deleteComitee/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    location.reload();
               }
            });
    }

    function codeConcate()
    {
        var d='_';
        document.getElementById('training_complete_code').value = document.getElementById('code').value + d+document.getElementById('training_code').value;
    }

    function getStateByCountry(id)
    {
        // alert(id);
        $.get("/setup/partnerUniversity/getStateByCountryForTraining/"+id, function(data, status){
            
            // alert(data);
            $("#view_state").html(data);
        });
    }


    function saveAggrementData()
    {
        if($('#form_aggrement').valid())
        {

            $('#form_aggrement').submit();
        }
    }

    function validateUniversityData()
    {
        if($('#form_award').valid())
        {

            $('#form_award').submit();
        }
    }

    function saveTrainingCenterData()
    {
        if($('#form_training').valid())
        {


        var tempPR = {};
        tempPR['code'] = $("#training_code").val();
        tempPR['name'] = $("#training_name").val();
        tempPR['complete_code'] = $("#training_complete_code").val();
        tempPR['status'] = $("#training_status").val();
        tempPR['contact_number'] = $("#training_contact_number").val();
        tempPR['email'] = $("#training_email").val();
        tempPR['fax'] = $("#training_fax").val();
        tempPR['address1'] = $("#training_address1").val();
        tempPR['address2'] = $("#training_address2").val();
        tempPR['location'] = $("#training_location").val();
        tempPR['id_country'] = $("#id_training_country").val();
        tempPR['id_state'] = $("#id_training_state").val();
        tempPR['city'] = $("#training_city").val();
        tempPR['zipcode'] = $("#training_zipcode").val();
        tempPR['id_contact_person'] = $("#id_training_contact_person").val();
        tempPR['status'] = 1;
        tempPR['id_organisation'] = <?php echo $partnerUniversity->id; ?>;

        //  This is Changed To Another Table from scholarship_partner_university_has_training_center -> organisation_has_training_center
        // tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;


            $.ajax(
            {
               url: '/setup/partnerUniversity/addTrainingCenter',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view_aggrement_data").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function saveProgramDetailsData()
    {
        if($('#form_program').valid())
        {


        var tempPR = {};
        tempPR['id_program'] = $("#id_detail_program").val();
        tempPR['completion_criteria'] = $("#detail_completion_criteria").val();
        tempPR['completion_criteria_type'] = $("#detail_completion_criteria_type").val();
        tempPR['assesment_method'] = $("#detail_assesment_method").val();
        tempPR['status'] = $("#details_program_status").val();
        tempPR['id'] = $("#id_detail").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;


            $.ajax(
            {
               url: '/setup/partnerUniversity/saveProgramDetailsData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view_aggrement_data").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function deleteMoaAggrement(id) {
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteMoaAggrement/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    // window.location.reload();
                    location.reload();
               }
            });
    }


    function deleteTrainingCenter(id) {
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteTrainingCenter/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    // window.location.reload();
                    location.reload();
               }
            });
    }


    function addProgramStudyModeData()
    {
        if($('#form_program_one').valid())
        {


        var tempPR = {};
        tempPR['mode_of_study'] = $("#program_detail_mode_of_study").val();
        tempPR['min_duration'] = $("#program_detail_mode_of_study_min_duration").val();
        tempPR['min_duration_type'] = $("#program_detail_mode_of_study_min_duration_type").val();
        tempPR['max_duration'] = $("#program_detail_mode_of_study_max_duration").val();
        tempPR['max_duration_type'] = $("#program_detail_mode_of_study_max_duration_type").val();
        tempPR['id_training_center'] = $("#program_detail_mode_of_study_id_training_center").val();
        tempPR['start_date'] = $("#program_detail_mode_of_study_start_date").val();
        tempPR['end_date'] = $("#program_detail_mode_of_study_end_date").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;
        tempPR['id_program_detail'] =  <?php if($partnerProgramDetails){ echo $partnerProgramDetails->id; }else{ echo 0;} ?>;

            $.ajax(
            {
               url: '/setup/partnerUniversity/addProgramStudyModeData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // location.reload();
                window.location.reload();

               }
            });
        }

    }
    function addProgramInternshipData()
    {
        if($('#form_program_two').valid())
        {


        var tempPR = {};
        tempPR['duration'] = $("#program_detail_internship_duration").val();
        tempPR['duration_type'] = $("#program_detail_internship_duration_type").val();
        tempPR['id_internship_center'] = $("#program_detail_internship_id_internship_center").val();
        tempPR['start_date'] = $("#program_detail_internship_start_date").val();
        tempPR['end_date'] = $("#program_detail_internship_end_date").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;
        tempPR['id_program_detail'] =  <?php if($partnerProgramDetails){ echo $partnerProgramDetails->id; }else{ echo 0;} ?>;

            $.ajax(
            {
               url: '/setup/partnerUniversity/addProgramInternshipData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // location.reload();
                window.location.reload();

               }
            });
        }

    }


    function addProgramApprenticeData()
    {
        if($('#form_program_three').valid())
        {


        var tempPR = {};
        tempPR['duration'] = $("#program_detail_apprenticeship_duration").val();
        tempPR['duration_type'] = $("#program_detail_apprenticeship_duration_type").val();
        tempPR['id_apprenticeship_center'] = $("#program_detail_apprenticeship_id_apprenticeship_center").val();
        tempPR['start_date'] = $("#program_detail_apprenticeship_start_date").val();
        tempPR['end_date'] = $("#program_detail_apprenticeship_end_date").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;
        tempPR['id_program_detail'] =  <?php if($partnerProgramDetails){ echo $partnerProgramDetails->id; }else{ echo 0;} ?>;

            $.ajax(
            {
               url: '/setup/partnerUniversity/addProgramApprenticeData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // location.reload();
                window.location.reload();

               }
            });
        }
    }


    function addProgramSyllabusData()
    {
        if($('#form_program_four').valid())
        {


        var tempPR = {};
        tempPR['code'] = $("#program_detail_syllabus_code").val();
        tempPR['name'] = $("#program_detail_syllabus_name").val();
        tempPR['credit'] = $("#program_detail_syllabus_credit").val();
        tempPR['credit_type'] = $("#program_detail_syllabus_credit_type").val();
        tempPR['id_module_type'] = $("#program_detail_syllabus_module_type").val();
        tempPR['id_partner_university'] = <?php echo $partnerUniversity->id; ?>;
        tempPR['id_program_detail'] = <?php if($partnerProgramDetails){ echo $partnerProgramDetails->id; }else{ echo 0;} ?>;

            $.ajax(
            {
               url: '/setup/partnerUniversity/addProgramSyllabusData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // location.reload();
                window.location.reload();

               }
            });
        }
    }


    function deleteProgramModeOfStudy(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteProgramModeOfStudy/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }


    function deleteProgramInternship(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteProgramInternship/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }


    function deleteProgramApprenticeship(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteProgramApprenticeship/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }


    function deleteProgramSyllabus(id) {
        // alert(id);
         $.ajax(
            {
               url: '/setup/partnerUniversity/deleteProgramSyllabus/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // window.location.reload();
                    // alert(result);
                    location.reload();
               }
            });
    }










    $(document).ready(function() {
        $("#form_program_one").validate({
            rules: {
                program_detail_mode_of_study: {
                    required: true
                },
                program_detail_mode_of_study_min_duration: {
                    required: true
                },
                program_detail_mode_of_study_min_duration_type: {
                    required: true
                },
                program_detail_mode_of_study_max_duration: {
                    required: true
                },
                program_detail_mode_of_study_max_duration_type: {
                    required: true
                },
                program_detail_mode_of_study_id_training_center: {
                    required: true
                },
                program_detail_mode_of_study_start_date: {
                    required: true
                },
                program_detail_mode_of_study_end_date: {
                    required: true
                }
            },
            messages: {
                program_detail_mode_of_study: {
                    required: "<p class='error-text'>Select Mode Of Study</p>",
                },
                program_detail_mode_of_study_min_duration: {
                    required: "<p class='error-text'>Min. Duration Required</p>",
                },
                program_detail_mode_of_study_min_duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                program_detail_mode_of_study_max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                },
                program_detail_mode_of_study_max_duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                program_detail_mode_of_study_id_training_center: {
                    required: "<p class='error-text'>Select Training Center</p>",
                },
                program_detail_mode_of_study_start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                program_detail_mode_of_study_end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_program_two").validate({
            rules: {
                program_detail_internship_duration: {
                    required: true
                },
                program_detail_internship_duration_type: {
                    required: true
                },
                program_detail_internship_id_internship_center: {
                    required: true
                },
                program_detail_internship_start_date: {
                    required: true
                },
                program_detail_internship_end_date: {
                    required: true
                }
            },
            messages: {
                program_detail_internship_duration: {
                    required: "<p class='error-text'>Duration Required</p>",
                },
                program_detail_internship_duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                program_detail_internship_id_internship_center: {
                    required: "<p class='error-text'>Select Internship Center</p>",
                },
                program_detail_internship_start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                program_detail_internship_end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });









    $(document).ready(function() {
        $("#form_program_three").validate({
            rules: {
                program_detail_apprenticeship_duration: {
                    required: true
                },
                program_detail_apprenticeship_duration_type: {
                    required: true
                },
                program_detail_apprenticeship_id_apprenticeship_center: {
                    required: true
                },
                program_detail_apprenticeship_start_date: {
                    required: true
                },
                program_detail_apprenticeship_end_date: {
                    required: true
                }
            },
            messages: {
                program_detail_apprenticeship_duration: {
                    required: "<p class='error-text'>Duration Required</p>",
                },
                program_detail_apprenticeship_duration_type: {
                    required: "<p class='error-text'>Duration Type Required</p>",
                },
                program_detail_apprenticeship_id_apprenticeship_center: {
                    required: "<p class='error-text'>Apprenticeship Center Required</p>",
                },
                program_detail_apprenticeship_start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                program_detail_apprenticeship_end_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_program_four").validate({
            rules: {
                program_detail_syllabus_code: {
                    required: true
                },
                program_detail_syllabus_name: {
                    required: true
                },
                program_detail_syllabus_credit: {
                    required: true
                },
                program_detail_syllabus_credit_type: {
                    required: true
                },
                program_detail_syllabus_module_type: {
                    required: true
                }
            },
            messages: {
                program_detail_syllabus_code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                program_detail_syllabus_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                program_detail_syllabus_credit: {
                    required: "<p class='error-text'>Credit Reuired</p>",
                },
                program_detail_syllabus_credit_type: {
                    required: "<p class='error-text'>Select Credit Type</p>",
                },
                program_detail_syllabus_module_type: {
                    required: "<p class='error-text'>Select Module Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_training").validate({
            rules: {
                training_code: {
                    required: true
                },
                training_name: {
                    required: true
                },
                training_status: {
                    required: true
                },
                training_contact_number: {
                    required: true
                },
                training_email: {
                    required: true
                },
                training_fax: {
                    required: true
                },
                training_address1: {
                    required: true
                },
                training_address2: {
                    required: true
                },
                id_training_country: {
                    required: true
                },
                training_city: {
                    required: true
                },
                training_zipcode: {
                    required: true
                },
                id_training_state: {
                    required: true
                },
                id_training_contact_person : {
                    required : true
                },
                training_location : {
                    required : true
                }
            },
            messages: {
                training_code: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                training_name: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                training_status: {
                    required: "<p class='error-text'>Training Status Required</p>",
                },
                training_contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                training_email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                training_fax: {
                    required: "<p class='error-text'>Fax Number Required</p>",
                },
                training_address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                training_address2: {
                    required: "<p class='error-text'>Address2 Required</p>",
                },
                id_training_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                training_city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                training_zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                id_training_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                id_training_contact_person: {
                    required: "<p class='error-text'>Select Contact Person</p>",
                },
                training_location: {
                    required: "<p class='error-text'>Training Location Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_aggrement").validate({
            rules: {
                moa_start_date: {
                    required: true
                },
                moa_end_date: {
                    required: true
                },
                moa_file: {
                    required: true
                },
                moa_name: {
                    required: true
                },
                moa_id_currency: {
                    required: true
                },
                moa_reminder_months: {
                    required: true
                }
            },
            messages: {
                moa_start_date: {
                    required: "<p class='error-text'>Select MOA Start Date</p>",
                },
                moa_end_date: {
                    required: "<p class='error-text'>Select MOA End Date</p>",
                },
                moa_file: {
                    required: "<p class='error-text'>Select MOA File</p>",
                },
                moa_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                moa_id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                moa_reminder_months: {
                    required: "<p class='error-text'>Select Reminer Months</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_program").validate({
            rules: {
                id_detail_program: {
                    required: true
                },
                detail_completion_criteria: {
                    required: true
                },
                detail_completion_criteria_type: {
                    required: true
                },
                detail_assesment_method: {
                    required: true
                },
                details_program_status: {
                    required: true
                }
            },
            messages: {
                id_detail_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                detail_completion_criteria: {
                    required: "<p class='error-text'>Completion Criteria Required</p>",
                },
                detail_completion_criteria_type: {
                    required: "<p class='error-text'>Select Criteria Type</p>",
                },
                detail_assesment_method: {
                    required: "<p class='error-text'>Select Assesment Method</p>",
                },
                details_program_status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





    

     $(document).ready(function() {
        $("#form_comitee").validate({
            rules: {
                role: {
                    required: true
                },
                com_name: {
                    required: true
                },
                com_nric: {
                    required: true
                },
                effective_date: {
                    required: true
                }
            },
            messages: {
                role: {
                    required: "<p class='error-text'>Select Role</p>",
                },
                com_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                com_nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                short_name: {
                    required: true
                },
                url: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address1: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                },
                id_partner_category: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                login_id: {
                    required: true
                },
                password : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                url: {
                    required: "<p class='error-text'>URL Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                id_partner_category: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                login_id: {
                    required: "<p class='error-text'>Login ID Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

    $( function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });
</script>