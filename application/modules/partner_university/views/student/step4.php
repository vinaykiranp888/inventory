    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">

            <div class="main-container clearfix">
                <div class="page-title clearfix">
                    <h3>Student Document Upload Details</h3>                    
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                         <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step1/'. $getApplicantDetails->id ?>" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step2/'. $getApplicantDetails->id ?>" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step3/'. $getApplicantDetails->id ?>" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step4/'. $getApplicantDetails->id ?>" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>

    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step6/'. $getApplicantDetails->id ?>" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">
                          
                           <div class="clearfix">

                            <div id='doc'>
                            </div>
                           



                           <?php

                            if(!empty($studentUploadedFiles))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Document Uploaded Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th style="text-align: center;">File</th>
                                                 <th style="text-align: center;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($studentUploadedFiles);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $studentUploadedFiles[$i]->document_name;?></td>
                                                <td class="text-center">

                                                    <a href="<?php echo '/assets/images/' . $studentUploadedFiles[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $studentUploadedFiles[$i]->file; ?>)" title="<?php echo $studentUploadedFiles[$i]->file; ?>">View</a>
                                                </td>
                                                <td class="text-center">
                                                    <a onclick="deleteStudentUploadedDocument(<?php echo $studentUploadedFiles[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

                            <?php
                            
                            }
                             ?>


                           </div>              
                        </div>    
                      </div>
    
                      <div class="wizard__footer">

                        <a href="../list" class="btn btn-primary">Back</a>

                        <a href="<?php '../step3/' . $getApplicantDetails->id ?>"  class="btn btn-link mr-3">Previous</a>

                        <button class="btn btn-primary next" type="submit">Save & Continue</button>
                      </div>
                    </div>
    
                  </div>
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
            </div>        
        </div>
    </div>      
    </form>
    
 


<script type="text/javascript">

  $(document).ready(function()
  {
        var idprogram = "<?php echo $getApplicantDetails->id_program;?>";

        if(idprogram != '')
        {
          getDocumentByProgramme(idprogram);
        }
        else
        {
          alert('Complete The Previous Registration Details To Complete The Application');
          // $("input.is_submitted").prop("disabled", true);
          // $("input.is_submitted").removeAttr('disabled');
        }
  });


   function getDocumentByProgramme(id)
    {
        if(id != '')
        {

            $.get("/partner_university/student/getDocumentByProgramme/"+id, function(data, status){
           
                if(data != '')
                {
                    $("#doc").html(data);
                    // $("#view_document").show();
                }else
                {
                    // $("#view_document").hide();
                    alert('No Records Defined To Upload');
                }

            });
        }
    }

    function deleteStudentUploadedDocument(id)
    {
      if(id != '')
        {

            $.get("/partner_university/student/deleteStudentUploadedDocument/"+id, function(data, status)
            {
              alert('Document File Deleted Successfully');
              window.location.reload();
            });
        }
      
    }
</script>