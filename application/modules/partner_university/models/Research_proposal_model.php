<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Research_proposal_model extends CI_Model
{
    function researchProposalList()
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->order_by("i.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function researchProposalListByStatus($status)
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->where("i.status", $status);
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function researchCategoryListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('research_topic_category');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function researchTopicListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('research_topic');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    function supervisorRoleListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('supervisor_role');
        $this->db->where("status", $status);
        $this->db->order_by("code", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    function examinerListByStatus($status)
    {
    	$this->db->select('re.*, s.ic_no, s.name as staff_name');
        $this->db->from('research_examiner as re');
        $this->db->join('staff as s', 're.id_staff = s.id','left');
        $this->db->where("re.status", $status);
        $this->db->order_by("re.id", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    

    function examinerRoleListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('examiner_role');
        $this->db->where("status", $status);
        $this->db->order_by("code", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }


    function researchProposalListSearch($data)
    {
        $this->db->select('i.*, p.name as research_category, rt.name as research_topic, s.nric, s.full_name as student_name');
        $this->db->from('research_proposal as i');
        $this->db->join('research_topic_category as p', 'i.id_research_category = p.id');
        $this->db->join('research_topic as rt', 'i.id_research_topic = rt.id');
        $this->db->join('student as s', 'i.id_student = s.id');
        if($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_research_category'] != '')
        {
            $this->db->where("i.id_research_category", $data['id_research_category']);
        }
        if($data['id_research_topic'] != '')
        {
            $this->db->where("i.id_research_topic", $data['id_research_topic']);
        }
        if($data['status'] != '')
        {
            $this->db->where("i.status", $data['status']);
        }
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getResearchProposal($id)
    {
        $this->db->select('i.*');
        $this->db->from('research_proposal as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewResearchProposal($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_proposal', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function tempAddResearchProposalHasSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_proposal_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchProposalHasSupervisorBySession($id_session)
    {
        $this->db->select('tihp.*, p.ic_no, p.name as staff_name, sr.code as supervisor_role');
        $this->db->from('temp_research_proposal_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->join('supervisor_role as sr', 'tihp.id_staff_role = sr.id');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchProposalHasSupervisor($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_proposal_has_supervisors');
       return TRUE;
    }

    function moveTempToDetails($id_proposal)
    {
    	$this->moveSupervisorTempToDetails($id_proposal);
    	$this->moveExaminerTempToDetails($id_proposal);
    }

    function moveSupervisorTempToDetails($id_proposal)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchProposalHasSupervisor($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_proposal = $id_proposal;
            $this->addNewResearchProposalHasSupervisors($result);
        }

        $deleted = $this->deleteTempResearchProposalHasSupervisorBySessionId($id_session);
        return $deleted;
    }

     function moveExaminerTempToDetails($id_proposal)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchProposalHasExaminer($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_proposal = $id_proposal;
            $this->addNewResearchProposalHasExaminer($result);
        }

        $deleted = $this->deleteTempResearchProposalHasExaminerBySessionId($id_session);
        return $deleted;
    }

    function getTempResearchProposalHasSupervisor($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_proposal_has_supervisors as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempResearchProposalHasExaminer($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_proposal_has_examiner as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewResearchProposalHasSupervisors($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_proposal_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewResearchProposalHasExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_proposal_has_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempResearchProposalHasSupervisorBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_proposal_has_supervisors');
       return TRUE;
    }

    function deleteTempResearchProposalHasExaminerBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_proposal_has_examiner');
       return TRUE;
    }

    function getResearchProposalHasSupervisor($id)
    {
    	$this->db->select('tihp.*, p.ic_no, p.name as staff_name, sr.code as supervisor_role');
        $this->db->from('research_proposal_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->join('supervisor_role as sr', 'tihp.id_staff_role = sr.id');
        $this->db->where('tihp.id_proposal', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getResearchProposalHasExaminer($id)
    {
    	$this->db->select('tihp.*, p.type, p.id_staff,s.name as staff_name, s.ic_no, p.full_name, sr.code as examiner_role');
        $this->db->from('research_proposal_has_examiner as tihp');
        $this->db->join('examiner_role as sr', 'tihp.id_examiner_role = sr.id');
        $this->db->join('research_examiner as p', 'tihp.id_examiner = p.id');
        $this->db->join('staff as s', 'p.id_staff = s.id','left');
        $this->db->where('tihp.id_proposal', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editResearchProposalDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_proposal', $data);
        return TRUE;
    }

     function deleteResearchProposalHasSupervisor($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_proposal_has_supervisors');
       return TRUE;
    }

     function deleteResearchProposalHasExaminer($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_proposal_has_examiner');
       return TRUE;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentByData($data)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_program', $data['id_programme']);
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function tempAddResearchProposalHasExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_proposal_has_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchProposalHasExaminerBySession($id_session)
    {
        $this->db->select('tihp.*, p.type, p.id_staff,s.name as staff_name, s.ic_no, p.full_name, sr.code as examiner_role');
        $this->db->from('temp_research_proposal_has_examiner as tihp');
        $this->db->join('examiner_role as sr', 'tihp.id_examiner_role = sr.id');
        $this->db->join('research_examiner as p', 'tihp.id_examiner = p.id');
        $this->db->join('staff as s', 'p.id_staff = s.id','left');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchProposalHasExaminer($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_proposal_has_examiner');
       return TRUE;
    }
}