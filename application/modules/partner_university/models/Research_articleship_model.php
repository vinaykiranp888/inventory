<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Research_articleship_model extends CI_Model
{
    function researchArticleshipList()
    {
        $this->db->select('i.*');
        $this->db->from('research_articleship as i');
        $this->db->order_by("i.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function researchArticleshipListByStatus($status)
    {
        $this->db->select('i.*');
        $this->db->from('research_articleship as i');
        $this->db->where("i.status", $status);
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function countryListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('country');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    function supervisorRoleListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('supervisor_role');
        $this->db->where("status", $status);
        $this->db->order_by("code", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    function examinerListByStatus($status)
    {
    	$this->db->select('re.*, s.ic_no, s.name as staff_name');
        $this->db->from('research_examiner as re');
        $this->db->join('staff as s', 're.id_staff = s.id','left');
        $this->db->where("re.status", $status);
        $this->db->order_by("re.id", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    

    function examinerRoleListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('examiner_role');
        $this->db->where("status", $status);
        $this->db->order_by("code", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }


    function researchArticleshipListSearch($data)
    {
        $this->db->select('i.*, p.name as semester_name, p.code as semester_code, s.nric, s.full_name as student_name');
        $this->db->from('research_articleship as i');
        $this->db->join('semester as p', 'i.id_semester = p.id');
        $this->db->join('student as s', 'i.id_student = s.id');
        if($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_semester'] != '')
        {
            $this->db->where("i.id_semester", $data['id_semester']);
        }
        if($data['status'] != '')
        {
            $this->db->where("i.status", $data['status']);
        }
        $this->db->order_by("i.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getResearchArticleship($id)
    {
        $this->db->select('i.*');
        $this->db->from('research_articleship as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewResearchArticleship($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_articleship', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function tempAddResearchArticleshipHasSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_articleship_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchArticleshipHasSupervisorBySession($id_session)
    {
        $this->db->select('tihp.*, p.ic_no, p.name as staff_name, sr.code as supervisor_role');
        $this->db->from('temp_research_articleship_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->join('supervisor_role as sr', 'tihp.id_staff_role = sr.id');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchArticleshipHasSupervisor($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_articleship_has_supervisors');
       return TRUE;
    }

    function moveTempToDetails($id_articleship)
    {
    	$this->moveSupervisorTempToDetails($id_articleship);
    	$this->moveExaminerTempToDetails($id_articleship);
    }

    function moveSupervisorTempToDetails($id_articleship)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchArticleshipHasSupervisor($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_articleship = $id_articleship;
            $this->addNewResearchArticleshipHasSupervisors($result);
        }

        $deleted = $this->deleteTempResearchArticleshipHasSupervisorBySessionId($id_session);
        return $deleted;
    }

     function moveExaminerTempToDetails($id_articleship)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchArticleshipHasExaminer($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_articleship = $id_articleship;
            $this->addNewResearchArticleshipHasExaminer($result);
        }

        $deleted = $this->deleteTempResearchArticleshipHasExaminerBySessionId($id_session);
        return $deleted;
    }

    function getTempResearchArticleshipHasSupervisor($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_articleship_has_supervisors as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempResearchArticleshipHasExaminer($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_articleship_has_examiner as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewResearchArticleshipHasSupervisors($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_articleship_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewResearchArticleshipHasExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_articleship_has_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempResearchArticleshipHasSupervisorBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_articleship_has_supervisors');
       return TRUE;
    }

    function deleteTempResearchArticleshipHasExaminerBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_articleship_has_examiner');
       return TRUE;
    }

    function getResearchArticleshipHasSupervisor($id)
    {
    	$this->db->select('tihp.*, p.ic_no, p.name as staff_name, sr.code as supervisor_role');
        $this->db->from('research_articleship_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->join('supervisor_role as sr', 'tihp.id_staff_role = sr.id');
        $this->db->where('tihp.id_articleship', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getResearchArticleshipHasExaminer($id)
    {
    	$this->db->select('tihp.*, p.type, p.id_staff,s.name as staff_name, s.ic_no, p.full_name, sr.code as examiner_role');
        $this->db->from('research_articleship_has_examiner as tihp');
        $this->db->join('examiner_role as sr', 'tihp.id_examiner_role = sr.id');
        $this->db->join('research_examiner as p', 'tihp.id_examiner = p.id');
        $this->db->join('staff as s', 'p.id_staff = s.id','left');
        $this->db->where('tihp.id_articleship', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editResearchArticleshipDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_articleship', $data);
        return TRUE;
    }

     function deleteResearchArticleshipHasSupervisor($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_articleship_has_supervisors');
       return TRUE;
    }

     function deleteResearchArticleshipHasExaminer($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_articleship_has_examiner');
       return TRUE;
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentByData($data)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_program', $data['id_programme']);
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function tempAddResearchArticleshipHasExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_articleship_has_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchArticleshipHasExaminerBySession($id_session)
    {
        $this->db->select('tihp.*, p.type, p.id_staff,s.name as staff_name, s.ic_no, p.full_name, sr.code as examiner_role');
        $this->db->from('temp_research_articleship_has_examiner as tihp');
        $this->db->join('examiner_role as sr', 'tihp.id_examiner_role = sr.id');
        $this->db->join('research_examiner as p', 'tihp.id_examiner = p.id');
        $this->db->join('staff as s', 'p.id_staff = s.id','left');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchArticleshipHasExaminer($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_articleship_has_examiner');
       return TRUE;
    }
}