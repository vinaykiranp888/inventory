<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_profile_model extends CI_Model
{
   function applicantList($applicantList)
    {
        $this->db->select('a.*, i.name as intake, p.code as program_code, p.name as program');
        $this->db->from('student as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        if($applicantList['first_name'])
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['email_id'])
        {
            $likeCriteria = "(a.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['nric'])
        {
            $likeCriteria = "(a.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($applicantList['id_program'])
        {
            $this->db->where('a.id_program', $applicantList['id_program']);
        }
        if($applicantList['id_intake'])
        {
            $this->db->where('a.id_intake', $applicantList['id_intake']);
        }
        if($applicantList['id_partner_university'])
        {
            $this->db->where('a.id_university', $applicantList['id_partner_university']);
        }
        if($applicantList['applicant_status'])
        {
            $this->db->where('a.applicant_status', $applicantList['applicant_status']);
        }
        $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseRegistrationList($id)
    {
        $this->db->select('a.id, i.name as intake, p.name as program, std.full_name, c.name as course');
        $this->db->from('course_registration as a');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->where('a.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function addExamDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('examination_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addProficiencyDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('english_proficiency_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addEmploymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProfileDetails($data, $id_student)
    {
        $this->db->where('id_student', $id_student);
        $this->db->update('profile_details', $data);
        return TRUE;
    }

    function updateStudentData($data)
    {
        $id = $data['id_student'];
        unset($data['id_student']);
        // unset($data['id_type']);
        // unset($data['passport_number']);
        // unset($data['passport_expiry_date']);
        unset($data['nationality_type']);
        // echo "<Pre>";print_r($data);exit();
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;

    }

    function addVisaDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('visa_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateVisaDetails($data)
    {
        $id_student = $data['id_student'];
        unset($data['id_student']);

        $this->db->where('id_student', $id_student);
        $this->db->update('visa_details', $data);
        return TRUE;
    }

    function addOtherDocuments($data)
    {
        $this->db->trans_start();
        $this->db->insert('other_documents', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamDetails($id)
    {
        $this->db->select('*');
        $this->db->from('examination_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProficiencyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('english_proficiency_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getEmploymentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('employment_status');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProfileDetails($id)
    {
        $this->db->select('*');
        $this->db->from('profile_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('country as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('state as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getVisaDetails($id)
    {
        $this->db->select('*');
        $this->db->from('visa_details');
        $this->db->where('id_student', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getOtherDocuments($id)
    {
        $this->db->select('*');
        $this->db->from('other_documents');
        $this->db->where('id_student', $id);
        // $this->db->limit('0,1');
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function nationalityList()
    {
        $this->db->select('*');
        $this->db->from('nationality');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function addNewStudent()
    {
        $query = $this->db->get('applicant_table');
        foreach ($query->result() as $row) {
              $this->db->insert('student',$row);
        }
    }

    function getStudentData($id)
    {
        $this->db->select('a.*, in.name as intake, p.name as program, qs.name as qualification_name, qs.code as qualification_code, st.ic_no, st.name as advisor, brch.code as branch_code, brch.name as branch_name');
        $this->db->from('student as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('organisation_has_training_center as brch', 'a.id_branch = brch.id'); 
        $this->db->join('programme as p', 'a.id_program = p.id');
        $this->db->join('qualification_setup as qs', 'a.id_degree_type = qs.id');
        $this->db->join('staff as st', 'a.id_advisor = st.id','left');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('applicant_table', $data);
        return TRUE;
    }
    
    function deleteExamDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('examination_details');
         return TRUE;
    }

    function deleteProficiencyDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('english_proficiency_details');
         return TRUE;
    }

    function deleteEmploymentDetails($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('employment_status');
         return TRUE;
    }

    function deleteOtherDocument($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('other_documents');
         return TRUE;
    }

    function checkDuplicateStudent($data,$id)
    {
        $this->db->select('id, email_id, full_name');
        $this->db->from('student');
        $this->db->where('email_id', $data['email_id']);
        $this->db->or_where('phone', $data['phone']);
        $this->db->or_where('nric', $data['nric']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getOrganisation()
    {
        $this->db->select('s.*');
        $this->db->from('organisation as s');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function deleteVisaDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('visa_details');
         return TRUE;
    }

    function addEmergencyContactDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_emergency_contact_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function emergencyContactDetails($id)
    {
        $this->db->select('*');
        $this->db->from('student_emergency_contact_details');
        $this->db->where('id_student =', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function raceListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('race_setup');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function religionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('religion_setup');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    
}