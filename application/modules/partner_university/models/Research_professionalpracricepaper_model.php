<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Research_professionalpracricepaper_model extends CI_Model
{
    function researchProfessionalpracricepaperList()
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->order_by("i.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function researchProfessionalpracricepaperListByStatus($status)
    {
        $this->db->select('i.*');
        $this->db->from('research_topic as i');
        $this->db->where("i.status", $status);
        $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function researchCategoryListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('research_topic_category');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function researchTopicListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('research_topic');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    function supervisorRoleListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('supervisor_role');
        $this->db->where("status", $status);
        $this->db->order_by("code", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    function examinerListByStatus($status)
    {
    	$this->db->select('re.*, s.ic_no, s.name as staff_name');
        $this->db->from('research_examiner as re');
        $this->db->join('staff as s', 're.id_staff = s.id','left');
        $this->db->where("re.status", $status);
        $this->db->order_by("re.id", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }

    

    function examinerRoleListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('examiner_role');
        $this->db->where("status", $status);
        $this->db->order_by("code", "ASC");
	    $query = $this->db->get();
	    $result = $query->result();   
	    //print_r($result);exit();     
	    return $result;
    }


    function researchProfessionalpracricepaperListSearch($data)
    {
        $this->db->select('i.*, s.nric, s.full_name as student_name');
        $this->db->from('research_professionalpracricepaper as i');
        $this->db->join('student as s', 'i.id_student = s.id');
        if($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['status'] != '')
        {
            // echo "<Pre>";print_r($data['status']);exit();

            $this->db->where("i.status", $data['status']);
        }
        $this->db->order_by("i.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getResearchProfessionalpracricepaper($id)
    {
        $this->db->select('i.*');
        $this->db->from('research_professionalpracricepaper as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewResearchProfessionalpracricepaper($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_professionalpracricepaper', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function tempAddResearchProfessionalpracricepaperHasSupervisor($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_professionalpracricepaper_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchProfessionalpracricepaperHasSupervisorBySession($id_session)
    {
        $this->db->select('tihp.*, p.ic_no, p.name as staff_name, sr.code as supervisor_role');
        $this->db->from('temp_research_professionalpracricepaper_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->join('supervisor_role as sr', 'tihp.id_staff_role = sr.id');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchProfessionalpracricepaperHasSupervisor($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_professionalpracricepaper_has_supervisors');
       return TRUE;
    }


    function tempAddResearchProfessionalpracricepaperHasEmployment($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_professionalpracricepaper_has_employment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchProfessionalpracricepaperHasEmploymentBySession($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_professionalpracricepaper_has_employment as tihp');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchProfessionalpracricepaperHasEmployment($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_professionalpracricepaper_has_employment');
       return TRUE;
    }


    function moveTempToDetails($id_professionalpracricepaper)
    {
    	$this->moveSupervisorTempToDetails($id_professionalpracricepaper);
        $this->moveExaminerTempToDetails($id_professionalpracricepaper);
    	$this->moveEmploymentTempToDetails($id_professionalpracricepaper);
    }

    function moveSupervisorTempToDetails($id_professionalpracricepaper)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchProfessionalpracricepaperHasSupervisor($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_professionalpracricepaper = $id_professionalpracricepaper;
            $this->addNewResearchProfessionalpracricepaperHasSupervisors($result);
        }

        $deleted = $this->deleteTempResearchProfessionalpracricepaperHasSupervisorBySessionId($id_session);
        return $deleted;
    }

    function moveExaminerTempToDetails($id_professionalpracricepaper)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchProfessionalpracricepaperHasExaminer($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_professionalpracricepaper = $id_professionalpracricepaper;
            $this->addNewResearchProfessionalpracricepaperHasExaminer($result);
        }

        $deleted = $this->deleteTempResearchProfessionalpracricepaperHasExaminerBySessionId($id_session);
        return $deleted;
    }



    function moveEmploymentTempToDetails($id_professionalpracricepaper)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempResearchProfessionalpracricepaperHasEmployment($id_session);

        foreach ($temp_details as $result)
        {
            unset($result->id);
            unset($result->id_session);
            $result->id_professionalpracricepaper = $id_professionalpracricepaper;
            $this->addNewResearchProfessionalpracricepaperHasEmployment($result);
        }

        $deleted = $this->deleteTempResearchProfessionalpracricepaperHasEmploymentBySessionId($id_session);
        return $deleted;
    }

    function getTempResearchProfessionalpracricepaperHasSupervisor($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_professionalpracricepaper_has_supervisors as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempResearchProfessionalpracricepaperHasExaminer($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_professionalpracricepaper_has_examiner as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempResearchProfessionalpracricepaperHasEmployment($id_session)
    {
        $this->db->select('tihp.*');
        $this->db->from('temp_research_professionalpracricepaper_has_employment as tihp');    
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewResearchProfessionalpracricepaperHasSupervisors($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_professionalpracricepaper_has_supervisors', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewResearchProfessionalpracricepaperHasExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_professionalpracricepaper_has_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addNewResearchProfessionalpracricepaperHasEmployment($data)
    {
        $this->db->trans_start();
        $this->db->insert('research_professionalpracricepaper_has_employment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempResearchProfessionalpracricepaperHasSupervisorBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_professionalpracricepaper_has_supervisors');
       return TRUE;
    }

    function deleteTempResearchProfessionalpracricepaperHasExaminerBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_professionalpracricepaper_has_examiner');
       return TRUE;
    }

    function deleteTempResearchProfessionalpracricepaperHasEmploymentBySessionId($id_session)
    {
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_research_professionalpracricepaper_has_employment');
       return TRUE;
    }

    function getResearchProfessionalpracricepaperHasSupervisor($id)
    {
    	$this->db->select('tihp.*, p.ic_no, p.name as staff_name, sr.code as supervisor_role');
        $this->db->from('research_professionalpracricepaper_has_supervisors as tihp');
        $this->db->join('staff as p', 'tihp.id_staff = p.id');
        $this->db->join('supervisor_role as sr', 'tihp.id_staff_role = sr.id');
        $this->db->where('tihp.id_professionalpracricepaper', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getResearchProfessionalpracricepaperHasExaminer($id)
    {
    	$this->db->select('tihp.*, p.type, p.id_staff,s.name as staff_name, s.ic_no, p.full_name, sr.code as examiner_role');
        $this->db->from('research_professionalpracricepaper_has_examiner as tihp');
        $this->db->join('examiner_role as sr', 'tihp.id_examiner_role = sr.id');
        $this->db->join('research_examiner as p', 'tihp.id_examiner = p.id');
        $this->db->join('staff as s', 'p.id_staff = s.id','left');
        $this->db->where('tihp.id_professionalpracricepaper', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getResearchProfessionalpracricepaperHasEmployment($id)
    {
        $this->db->select('tihp.*');
        $this->db->from('research_professionalpracricepaper_has_employment as tihp');
        $this->db->where('tihp.id_professionalpracricepaper', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editResearchProfessionalpracricepaperDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('research_professionalpracricepaper', $data);
        return TRUE;
    }

    function deleteResearchProfessionalpracricepaperHasSupervisor($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_professionalpracricepaper_has_supervisors');
       return TRUE;
    }

    function deleteResearchProfessionalpracricepaperHasExaminer($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_professionalpracricepaper_has_examiner');
       return TRUE;
    }


    function deleteResearchProfessionalpracricepaperHasEmployment($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('research_professionalpracricepaper_has_employment');
       return TRUE;
    }


    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentByData($data)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_program', $data['id_programme']);
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.id as id_intake, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function tempAddResearchProfessionalpracricepaperHasExaminer($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_research_professionalpracricepaper_has_examiner', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempResearchProfessionalpracricepaperHasExaminerBySession($id_session)
    {
        $this->db->select('tihp.*, p.type, p.id_staff,s.name as staff_name, s.ic_no, p.full_name, sr.code as examiner_role');
        $this->db->from('temp_research_professionalpracricepaper_has_examiner as tihp');
        $this->db->join('examiner_role as sr', 'tihp.id_examiner_role = sr.id');
        $this->db->join('research_examiner as p', 'tihp.id_examiner = p.id');
        $this->db->join('staff as s', 'p.id_staff = s.id','left');
        $this->db->where('tihp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempResearchProfessionalpracricepaperHasExaminer($id)
    {
       $this->db->where('id', $id);
       $this->db->delete('temp_research_professionalpracricepaper_has_examiner');
       return TRUE;
    }



}