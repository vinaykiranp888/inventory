<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AlumniDiscount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alumni_discount_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('alumni_discount.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));


            $data['alumniList'] = $this->alumni_discount_model->alumniList($name);

            $data['searchName'] = $name;
            $this->global['pageTitle'] = 'Inventory Management : Alumni Discount';
            //print_r($subjectDetails);exit;
            $this->loadViews("alumni_discount/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('alumni_discount.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'name' => $name,
                    'amount' => $amount,
                    'currency' => $currency,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
            
                $result = $this->alumni_discount_model->addNewAlumniDiscount($data);
                redirect('/finance/alumniDiscount/list');
            }

            $this->global['pageTitle'] = 'Inventory Management : Add Alumni Discount';
            $this->loadViews("alumni_discount/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('alumni_discount.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/alumniDiscount/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'amount' => $amount,
                    'currency' => $currency,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
                $result = $this->alumni_discount_model->editAlumniDiscountDetails($data,$id);
                redirect('/finance/alumniDiscount/list');
            }

            $data['alumniDiscountDetails'] = $this->alumni_discount_model->getAlumniDiscountDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Alumni Discount';
            $this->loadViews("alumni_discount/edit", $this->global, $data, NULL);
        }
    }
}

