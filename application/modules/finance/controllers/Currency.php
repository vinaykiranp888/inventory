<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Currency extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('currency_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('currency.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['currencyList'] = $this->currency_model->currencyListSearch($name);

            $this->global['pageTitle'] = 'Inventory Management : Currency List';
            $this->loadViews("currency/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('currency.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;   

            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $prefix = $this->security->xss_clean($this->input->post('prefix'));
                $suffix = $this->security->xss_clean($this->input->post('suffix'));
                $decimal_place = $this->security->xss_clean($this->input->post('decimal_place'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'prefix' => $prefix,
                    'suffix' => $suffix,
                    'decimal_place' => $decimal_place,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->currency_model->addNewCurrency($data);
                redirect('/finance/currency/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Currency';
            $this->loadViews("currency/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('currency.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId; 

            if ($id == null)
            {
                redirect('/finance/currency/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $prefix = $this->security->xss_clean($this->input->post('prefix'));
                $suffix = $this->security->xss_clean($this->input->post('suffix'));
                $decimal_place = $this->security->xss_clean($this->input->post('decimal_place'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'prefix' => $prefix,
                    'suffix' => $suffix,
                    'decimal_place' => $decimal_place,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->currency_model->editCurrency($data,$id);
                redirect('/finance/currency/list');
            }
            $data['currency'] = $this->currency_model->getCurrencySetup($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Currency';
            $this->loadViews("currency/edit", $this->global, $data, NULL);
        }
    }

    function makeDefault($id)
    {
        $data['default'] = 0;
        $data1['default'] = 1;
        $result = $this->currency_model->editCurrencyNot($data,$id);
        $result = $this->currency_model->editCurrency($data1,$id);

        echo "success";exit;

    }
}
