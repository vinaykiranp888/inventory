<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SponserMainInvoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sponser_main_invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('sponser_main_invoice.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_sponser'] = $this->security->xss_clean($this->input->post('id_sponser'));
            $formData['type'] = 'Sponsor';
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->sponser_main_invoice_model->getMainInvoiceListByStatus($formData);

            $data['sponserList'] = $this->sponser_main_invoice_model->sponserListByStatus('1');
            $data['studentList'] = $this->sponser_main_invoice_model->studentList();

// echo "<Pre>";print_r($data['mainInvoiceList']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Sponsor Invoice List';
            $this->loadViews("sponser_main_invoice/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('sponser_main_invoice.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;                    
            
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_program = $this->security->xss_clean($this->input->post('id_programme'));
                $id_sponser = $this->security->xss_clean($this->input->post('id_sponser'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $invoice_number = $this->sponser_main_invoice_model->generateMainInvoiceNumber();

            
                $data = array(
                    'invoice_number' => $invoice_number,
                    'total_amount' => $total_amount,
                    'invoice_total' => $total_amount,
                    'balance_amount' => $total_amount,
                    'paid_amount' => '0',
                    'id_student' => $id_student,
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'id_sponser' => $id_sponser,
                    'type' => $type,
                    'remarks' => $remarks,
                    'status' => '1',
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->sponser_main_invoice_model->addNewMainInvoice($data);

                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
                $temp_details = $this->sponser_main_invoice_model->getTempMainInvoiceDetails($id_session);
                 for($i=0;$i<count($temp_details);$i++)
                 {
                    $amount = $temp_details[$i]->amount;
                    $id_fee_item = $temp_details[$i]->id_fee_item;

                     $detailsData = array(
                        'id_main_invoice' => $inserted_id,
                        'amount' => $amount,
                        'id_fee_item' => $id_fee_item,
                        'status' => '1',
                        'created_by' => $user_id
                    );
                    //print_r($details);exit;
                    $result = $this->sponser_main_invoice_model->addNewMainInvoiceDetails($detailsData);
                 }

                $this->sponser_main_invoice_model->deleteTempDataBySession($id_session);
                redirect('/finance/sponserMainInvoice/list');
            }
            else
            {
                $this->sponser_main_invoice_model->deleteTempDataBySession($id_session);
            }
            $data['studentList'] = $this->sponser_main_invoice_model->studentList();
            $data['feeSetupList'] = $this->sponser_main_invoice_model->feeSetupList();
            $data['programmeList'] = $this->sponser_main_invoice_model->programmeListByStatus('1');
            $data['sponserList'] = $this->sponser_main_invoice_model->sponserListByStatus('1');
            $data['intakeList'] = $this->sponser_main_invoice_model->intakeList();

            $this->global['pageTitle'] = 'Inventory Management : Add Sponsor Invoice';
            $this->loadViews("sponser_main_invoice/add", $this->global, $data, NULL);
        }
    }


    function getData() {
        
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('sponser_main_invoice.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/sponserMainInvoice/list');
            }
            if($this->input->post())
            {
                redirect('/sponser/mainInvoice/list');
            }
            $data['mainInvoice'] = $this->sponser_main_invoice_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->sponser_main_invoice_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->sponser_main_invoice_model->getMainInvoiceDiscountDetails($id);
            $data['studentDetails'] = $this->sponser_main_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            // $data['sponser'] = $this->sponser_main_invoice_model->getSponser($data['mainInvoice']->id_sponser);
            // $data['sponser'] = $this->sponser_main_invoice_model->getStudent($data['mainInvoice']->id_student);
            $data['degreeTypeList'] = $this->sponser_main_invoice_model->qualificationList();
            
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Main Invoice';
            $this->loadViews("sponser_main_invoice/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('sponser_main_invoice.cancel') == 0)
        // if ($this->checkAccess('sponser_main_invoice.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/sponserMainInvoice/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $result = $this->sponser_main_invoice_model->editMainInvoice($data,$id);
            //      if($status == '2')
            //      {
            //         $detailsDatas = $this->Pr_model->getPrDetails($id);
            //         foreach ($detailsDatas as $detailsData)
            //         {
            // // echo "<Pre>";print_r($detailsData);exit();
            //             $details_data['id_budget_allocation'] = $detailsData->id_budget_allocation;
            //             $details_data['total_final'] = $detailsData->total_final;
            //             $updated_budget_amount = $this->Pr_model->updateBudgetAllocationAmountOnReject($details_data);

            //         }
            //      }

                redirect('/finance/sponserMainInvoice/approvalList');
            }
            $data['mainInvoice'] = $this->sponser_main_invoice_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->sponser_main_invoice_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->sponser_main_invoice_model->getMainInvoiceDiscountDetails($id);
            $data['studentDetails'] = $this->sponser_main_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("sponser_main_invoice/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('sponser_main_invoice.cancel_list') == 0)
        // if ($this->checkAccess('sponser_main_invoice_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_sponser'] = $this->security->xss_clean($this->input->post('id_sponser'));
            $formData['type'] = 'Sponsor';
            $formData['status'] = 1;
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->sponser_main_invoice_model->getMainInvoiceListByStatusForCancelation($formData);

            $data['sponserList'] = $this->sponser_main_invoice_model->sponserListByStatus('1');
            $data['studentList'] = $this->sponser_main_invoice_model->studentList();
            

            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("sponser_main_invoice/approval_list", $this->global, $data, NULL);
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        unset($tempData['id']);
        $inserted_id = $this->sponser_main_invoice_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->sponser_main_invoice_model->getTempMainInvoiceDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($temp_details))
        {

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_setup</td>
                            <td>$amount</td>                           
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='inv-total-amount' value='$total_amount' />$total_amount</td>                           
                            <td></td>
                        </tr>";

        $table.= "</table>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->sponser_main_invoice_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempadd1()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
        $amount = $this->security->xss_clean($this->input->post('amount'));

        // echo "<Pre>";  print_r($id_session . "=". $amount);exit;
        $data = array(
               'id_session' => $id_session,
               'id_fee_item' => $id_fee_item,
               'amount' => $amount
            );
        $inserted_id = $this->sponser_main_invoice_model->addNewTempMainInvoiceDetails($data);
        //echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'amount' => $amount,
                'id_fee_item' => $id_fee_item,
            );
        $temp_details = $this->sponser_main_invoice_model->getTempMainInvoiceDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $id = $temp_details[$i]->id;

                    $table .= "
                <tr>
                    <td>$fee_setup</td>
                    <td ><input type='hidden' id='inv-total-amount' value='$amount' />$amount</td>             

                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }


     function getStudentBySponser()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $id_sponser = $formData['id_sponser'];
        $table = $this->getStudentList($id_sponser);
        echo $table;
        exit;

            
    }

    function getStudentList($id_sponser)
    {
        $data = $this->sponser_main_invoice_model->getStudentBySponser($id_sponser);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantList($data)
    {
        $data = $this->sponser_main_invoice_model->getApplicantListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Applicant <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getApplicantByApplicantId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->sponser_main_invoice_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";


            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Program</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function getApplicantByApplicantId($id)
    {
         // print_r($id);exit;
            $student_data = $this->sponser_main_invoice_model->getApplicantByApplicantId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Applicant Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Applicant NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";
            echo $table;
            exit;
    }
}
