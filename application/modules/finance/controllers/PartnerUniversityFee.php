<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerUniversityFee extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_fee_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('partne_university_fee.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->partner_fee_model->intakeListByStatus('1');
            $data['programList'] = $this->partner_fee_model->programListByStatus('1');
            $data['partnerUniversityList'] = $this->partner_fee_model->partnerUniversityListByStatus('1');

            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['fee_type'] = $this->security->xss_clean($this->input->post('fee_type'));
 

            $data['searchParam'] = $formData;


            $data['partnerFeeList'] = $this->partner_fee_model->partnerFeeListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Partner University Fee List';
            $this->loadViews("partner_fee/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('partne_university_fee.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $fee_type = $this->security->xss_clean($this->input->post('fee_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $payment_day = $this->security->xss_clean($this->input->post('payment_day'));
                $id_agreement = $this->security->xss_clean($this->input->post('id_agreement'));

                $data = array(
                    'id_partner_university' => $id_partner_university,
                    'id_intake' => $id_intake,
                    'id_program' => $id_program,
                    'fee_type' => $fee_type,
                    'amount' => $amount,
                    'status' => $status,
                    'payment_day' => $payment_day,
                    'id_agreement' => $id_agreement,
                );


                $inserted_id = $this->partner_fee_model->addNewPartnerUniversityFee($data);
                redirect('/finance/partnerUniversityFee/list');
            }

            $data['programList'] = $this->partner_fee_model->programListByStatus('1');

            $data['agreementList'] = $this->partner_fee_model->getAgreement();


            $data['partnerUniversityList'] = $this->partner_fee_model->partnerUniversityListByStatus('1');
            
                // echo "<Pre>";print_r($data['partnerUniversityList']);exit;


            $this->global['pageTitle'] = 'Inventory Management : Partner University Add Fee';
            $this->loadViews("partner_fee/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('partne_university_fee.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/partnerUniversityFee/list');
            }
            if($this->input->post())
            {
	            $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $fee_type = $this->security->xss_clean($this->input->post('fee_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $payment_day = $this->security->xss_clean($this->input->post('payment_day'));
                $id_agreement = $this->security->xss_clean($this->input->post('id_agreement'));

                $data = array(
                    'id_partner_university' => $id_partner_university,
                    'id_intake' => $id_intake,
                    'id_program' => $id_program,
                    'fee_type' => $fee_type,
                    'amount' => $amount,
                    'status' => $status,
                    'payment_day' => $payment_day,
                    'id_agreement' => $id_agreement,
                );

                // echo "<Pre>";print_r($data);exit;
                $result = $this->partner_fee_model->editPartnerUniversityFee($data,$id);
                redirect('/finance/partnerUniversityFee/list');
            }
            // $data['studentList'] = $this->partner_fee_model->studentList();
            $data['partnerFee'] = $this->partner_fee_model->getPartnerUniversityFee($id);

            $data['programList'] = $this->partner_fee_model->programListByStatus('1');
            // $data['agreementList'] = $this->partner_fee_model->getAgreement();

            $data['partnerUniversityList'] = $this->partner_fee_model->partnerUniversityListByStatus('1');

                // echo "<Pre>";print_r($data['partnerUniversityList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Partner University Fee';
            $this->loadViews("partner_fee/edit", $this->global, $data, NULL);
        }
    }

    

    function getPartnerUniversityAggrementList($id_partner_university)
    {
        // $id_session = $this->session->my_session_id;
        // $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        $list_data = $this->partner_fee_model->getPartnerUniversityAggrementList($id_partner_university);

        // $student_list_data = $this->partner_fee_model->getIntakeListByProgramme($id_programme);
        // echo "<Pre>";print_r($end_date);exit;

        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_agreement' id='id_agreement' class='form-control'>";
        $table.="<option value=''>Select</option> 
                ";

        for($i=0;$i<count($list_data);$i++)
        {

        $id = $list_data[$i]->id;
        $name = $list_data[$i]->name;
        $start_date = $list_data[$i]->start_date;
        $end_date = $list_data[$i]->end_date;

        if($start_date != '')
        {
           $start_date = date('d-m-Y',strtotime($start_date));
        }


        if($end_date != '')
        {
           $end_date = date('d-m-Y',strtotime($end_date));
        }


        $table.="<option value=".$id.">" . $name . " - From". $start_date . " to " . $end_date .
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }


    function getIntakes($id_programme)
    {
        // $id_session = $this->session->my_session_id;
        // $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;

        $student_list_data = $this->partner_fee_model->getIntakeListByProgramme($id_programme);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control'>";
        $table.="<option value=''>Select</option> 
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">" . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

}
