<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerUniversityInvoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_invoice_model');
        $this->isLoggedIn();
    }

    function comingSoon()
    {

        $this->global['pageTitle'] = 'Inventory Management : Coming Soon';
        $this->loadViews("partner_university_invoice/coming_soon", $this->global, NULL, NULL);
    }

    

    function list()
    {
        if ($this->checkAccess('partner_university_invoice.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = 'Partner University';
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->partner_university_invoice_model->getMainInvoiceListByStatus($formData);

            $data['programmeList'] = $this->partner_university_invoice_model->programmeListByStatus('1');
            $data['partnerUniversityList'] = $this->partner_university_invoice_model->partnerUniversityListByStatus('1');

            // echo "<Pre>";print_r($data['mainInvoiceList']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Main Invoice List';
            $this->loadViews("partner_university_invoice/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('partner_university_invoice.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;                    
            
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_program = $this->security->xss_clean($this->input->post('id_programme'));
                // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $id_detail_student = $this->security->xss_clean($this->input->post('id_detail_student'));

                $student_quantity = count($id_detail_student);

                $invoice_number = $this->partner_university_invoice_model->generateMainInvoiceNumber();

                $total_after_multiply = $student_quantity * $total_amount;

                $data = array(
                    'invoice_number' => $invoice_number,
                    'total_amount' => $total_after_multiply,
                    'invoice_total' => $total_after_multiply,
                    'balance_amount' => $total_after_multiply,
                    'paid_amount' => 0,
                    'id_student' => $id_student,
                    'id_program' => $id_program,
                    'no_count' => $student_quantity,
                    'type' => $type,
                    'remarks' => $remarks,
                    'currency' => $currency,
                    'status' => '1',
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->partner_university_invoice_model->addNewMainInvoice($data);


                // $amount = $this->security->xss_clean($this->input->post('amount'));
                // $id_detail_student = $this->security->xss_clean($this->input->post('id_detail_student'));


                if($inserted_id)
                {

                    $temp_details = $this->partner_university_invoice_model->getTempMainInvoiceDetails($id_session);

                     for($i=0;$i<count($temp_details);$i++)
                     {
                        $amount = $temp_details[$i]->amount;
                        $id_fee_item = $temp_details[$i]->id_fee_item;

                         $detailsData = array(
                            'id_main_invoice' => $inserted_id,
                            'quantity' => $student_quantity,
                            'amount' => $student_quantity * $amount,
                            'price' => $amount,
                            'id_fee_item' => $id_fee_item,
                            'status' => '1',
                            'created_by' => $user_id
                        );
                        // print_r($detailsData);exit;
                        $result = $this->partner_university_invoice_model->addNewMainInvoiceDetails($detailsData);
                     }
                    
                    $this->partner_university_invoice_model->deleteTempDataBySession($id_session);


                    for($i=0;$i<count($formData['id_detail_student']);$i++)
                    {
                        $id_detail_student = $formData['id_detail_student'][$i];

                        if($id_detail_student > 0 && $inserted_id > 0)
                        {
                            $student_invoice_data = array(
                            'id_main_invoice' => $inserted_id,
                            'id_partner_university' => $id_student,
                            'id_student'=> $id_detail_student,
                            'amount'=> $total_amount,
                            'status'=> 1

                            );
                        // echo "<Pre>"; print_r($student_invoice_data);exit;
                        $added_student_data = $this->partner_university_invoice_model->addPartnerStudentInvoiceDetails($student_invoice_data);
                        }
                    }

                }

                redirect('/finance/partnerUniversityInvoice/list');
            }
            else
            {
                $this->partner_university_invoice_model->deleteTempDataBySession($id_session);
            }
            $data['studentList'] = $this->partner_university_invoice_model->studentList();
            $data['feeSetupList'] = $this->partner_university_invoice_model->feeSetupList();
            $data['programmeList'] = $this->partner_university_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->partner_university_invoice_model->intakeList();
            $data['partnerUniversityList'] = $this->partner_university_invoice_model->partnerUniversityListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Main Invoice';
            $this->loadViews("partner_university_invoice/add", $this->global, $data, NULL);
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkAccess('partner_university_invoice.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/partnerUniversityInvoice/list');
            }
            if($this->input->post())
            {
                redirect('/finance/partnerUniversityInvoice/list');
            }
            $data['mainInvoice'] = $this->partner_university_invoice_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->partner_university_invoice_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->partner_university_invoice_model->getMainInvoiceDiscountDetails($id);

            $data['mainInvoiceStudentDetails'] = $this->partner_university_invoice_model->getMainInvoiceStudentDetails($id,$data['mainInvoice']->applicant_partner_fee);


            if($data['mainInvoice']->type == 'Applicant')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoiceApplicantData($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Partner University')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoicePartnerUniversityData($data['mainInvoice']->id_student);
                // $data['studentDetails'] = $this->partner_university_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'Sponser')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoiceSponserData($data['mainInvoice']->id_sponser);
                $data['studentDetails'] = $this->partner_university_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            $data['degreeTypeList'] = $this->partner_university_invoice_model->qualificationList();
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoiceStudentDetails']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Partner University Main Invoice';
            $this->loadViews("partner_university_invoice/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('partner_university_invoice.view') == 0)
        // if ($this->checkAccess('partner_university_invoice.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/mainInvoice/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));

                // echo "<Pre>";print_r($status);exit();

                if($status)
                {

                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $result = $this->partner_university_invoice_model->editMainInvoice($data,$id);
                
                }
            //      if($status == '2')
            //      {
            //         $detailsDatas = $this->Pr_model->getPrDetails($id);
            //         foreach ($detailsDatas as $detailsData)
            //         {
            // // echo "<Pre>";print_r($detailsData);exit();
            //             $details_data['id_budget_allocation'] = $detailsData->id_budget_allocation;
            //             $details_data['total_final'] = $detailsData->total_final;
            //             $updated_budget_amount = $this->Pr_model->updateBudgetAllocationAmountOnReject($details_data);

            //         }
            //      }

                redirect('/finance/partnerUniversityInvoice/approvalList');
            }
            $data['mainInvoice'] = $this->partner_university_invoice_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->partner_university_invoice_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->partner_university_invoice_model->getMainInvoiceDiscountDetails($id);

            $data['mainInvoiceStudentDetails'] = $this->partner_university_invoice_model->getMainInvoiceStudentDetails($id);

            
            if($data['mainInvoice']->type == 'Applicant')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoiceApplicantData($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Partner University')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoicePartnerUniversityData($data['mainInvoice']->id_student);
                // $data['studentDetails'] = $this->partner_university_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Sponser')
            {
                $data['invoiceFor'] = $this->partner_university_invoice_model->getMainInvoiceSponserData($data['mainInvoice']->id_sponser);
                $data['studentDetails'] = $this->partner_university_invoice_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            $data['degreeTypeList'] = $this->partner_university_invoice_model->qualificationList();
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("partner_university_invoice/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('pr_entry_approval.list') == 0)
        // if ($this->checkAccess('partner_university_invoice_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

           //  $resultprint = $this->input->post();

           // if($resultprint)
           //  {
           //   switch ($resultprint['button'])
           //   {
           //      case 'approve':

           //           for($i=0;$i<count($resultprint['checkvalue']);$i++)
           //              {

           //               $id = $resultprint['checkvalue'][$i];
                         
           //               $result = $this->partner_university_invoice_model->editMainInvoiceList($id);
           //              }
           //              redirect($_SERVER['HTTP_REFERER']);
           //           break;


           //      case 'search':

           //           $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
           //          $formData['name'] = $this->security->xss_clean($this->input->post('name'));
           //          $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
           //          $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
           //          $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
           //          $formData['status'] = '0';
         
           //          $data['searchParam'] = $formData;
           //          $data['mainInvoiceList'] = $this->partner_university_invoice_model->getMainInvoiceListByStatus($formData);
                     
           //           break;
                 
           //      default:
           //           break;
           //   }
                
           //  }
            
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '1';
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->partner_university_invoice_model->getMainInvoiceListByStatusForCancellation($formData);

            $data['programmeList'] = $this->partner_university_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->partner_university_invoice_model->intakeListByStatus('1');
            $data['applicantList'] = $this->partner_university_invoice_model->applicantList();
            $data['studentList'] = $this->partner_university_invoice_model->studentList();
            $data['sponserList'] = $this->partner_university_invoice_model->sponserListByStatus('1');
            $data['partnerUniversityList'] = $this->partner_university_invoice_model->partnerUniversityListByStatus('1');
            
//             $array = $this->security->xss_clean($this->input->post('checkvalue'));
//             if (!empty($array))
//             {
// // echo "<Pre>"; print_r($array);exit;
                
//             }


            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("partner_university_invoice/approval_list", $this->global, $data, NULL);
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        unset($tempData['id']);
        $inserted_id = $this->partner_university_invoice_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->partner_university_invoice_model->getTempMainInvoiceDetails($id_session); 
        // echo "<Pre>";print_r($details);exit;
        if(!empty($temp_details))
        {

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_setup</td>
                            <td>$amount</td>                           
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='inv-total-amount' value='$total_amount' />$total_amount</td>                           
                            <td></td>
                        </tr>";

        $table.= "</table>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->partner_university_invoice_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempadd1()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
        $amount = $this->security->xss_clean($this->input->post('amount'));

        // echo "<Pre>";  print_r($id_session . "=". $amount);exit;
        $data = array(
               'id_session' => $id_session,
               'id_fee_item' => $id_fee_item,
               'amount' => $amount
            );
        $inserted_id = $this->partner_university_invoice_model->addNewTempMainInvoiceDetails($data);
        //echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'amount' => $amount,
                'id_fee_item' => $id_fee_item,
            );
        $temp_details = $this->partner_university_invoice_model->getTempMainInvoiceDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $id = $temp_details[$i]->id;

                    $table .= "
                <tr>
                    <td>$fee_setup</td>
                    <td ><input type='hidden' id='inv-total-amount' value='$amount' />$amount</td>             

                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }


     function getStudentByProgrammeId()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $type = $formData['type'];
            switch ($type)
            {
                case 'Applicant':

                    $table = $this->getApplicantList($formData);

                    break;

                case 'Student':

                    $table = $this->getStudentList($formData);
                    
                    break;


                default:
                    # code...
                    break;
            }

            echo $table;
            exit;

            // $results = $this->partner_university_invoice_model->getStudentByProgrammeId($id);
            // $programme_data = $this->partner_university_invoice_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($formData);exit;
            // $programme_name = $programme_data->name;
            // $programme_code = $programme_data->code;
            // $total_cr_hrs = $programme_data->total_cr_hrs;
            // $graduate_studies = $programme_data->graduate_studies;
            // $foundation = $programme_data->foundation;
           
            
            // $table="

            // <script type='text/javascript'>
            //      $('select').select2();
            // </script>

            // <div class='col-sm-4'>
            //         <div class='form-group'>
            //         <label>Select Staff <span class='error-text'>*</span></label>
            // <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            // $table.="<option value=''>Select</option>";

            // for($i=0;$i<count($results);$i++)
            // {

            // // $id = $results[$i]->id_procurement_category;
            // $id = $results[$i]->id;
            // $nric = $results[$i]->nric;
            // $full_name = $results[$i]->full_name;
            // $table.="<option value=".$id.">".$nric . " - " . $full_name.
            //         "</option>";

            // }
            // $table.="</select>
            // </div>
            // </div>";

            // $view  = "
            // <table border='1px' style='width: 100%'>
            //     <tr>
            //         <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Programme Name</th>
            //         <td style='text-align: center;'>$programme_name</td>
            //         <th style='text-align: center;'>Programme Code</th>
            //         <td style='text-align: center;'>$programme_code</td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Total Credit Hours</th>
            //         <td style='text-align: center;'>$total_cr_hrs</td>
            //         <th style='text-align: center;'>Graduate Studies</th>
            //         <td style='text-align: center;'>$graduate_studies</td>
            //     </tr>

            // </table>
            // <br>
            // <br>
            // ";

            
    }

    function getStudentList($data)
    {
        $data = $this->partner_university_invoice_model->getStudentListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantList($data)
    {
        $data = $this->partner_university_invoice_model->getApplicantListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Applicant <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getApplicantByApplicantId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->partner_university_invoice_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";


            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Program</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function getApplicantByApplicantId($id)
    {
         // print_r($id);exit;
            $student_data = $this->partner_university_invoice_model->getApplicantByApplicantId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Applicant Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Applicant NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";
            echo $table;
            exit;
    }



    function generateMainInvoice($id_partner_university_invoice)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();

                    // print_r($base_url);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            // $mpdf->SetHeader("<div style='text-align: left;'>Inventory Management
            //                    </div>");


            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');

        $organisationDetails = $this->partner_university_invoice_model->getOrganisation();


        // echo "<Pre>";print_r($organisationDetails);exit;
        

        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }




        $mainInvoice = $this->partner_university_invoice_model->getMainInvoice($id_partner_university_invoice);

        if($mainInvoice->type == 'Applicant')
        {
            $invoiceFor = $this->partner_university_invoice_model->getMainInvoiceApplicantData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'Partner University')
        {
            $invoiceFor = $this->partner_university_invoice_model->getMainInvoicePartnerData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'Student')
        {
            $invoiceFor = $this->partner_university_invoice_model->getMainInvoiceStudentData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'Sponser')
        {
            $invoiceFor = $this->partner_university_invoice_model->getMainInvoiceSponserData($mainInvoice->id_sponser);
            $studentDetails = $this->partner_university_invoice_model->getStudentByStudent($mainInvoice->id_student);
        }


        // echo "<Pre>";print_r($invoiceFor);exit;


        $type = $mainInvoice->type;
        $invoice_number = $mainInvoice->invoice_number;
        $date_time = $mainInvoice->date_time;
        $remarks = $mainInvoice->remarks;
        $currency = $mainInvoice->currency;
        $total_amount = $mainInvoice->total_amount;
        $invoice_total = $mainInvoice->invoice_total;
        $balance_amount = $mainInvoice->balance_amount;
        $paid_amount = $mainInvoice->paid_amount;
        $programme_name = $mainInvoice->programme_name;
        $programme_code = $mainInvoice->programme_code;
        $intake_name = $mainInvoice->intake_name;
        $intake_year = $mainInvoice->intake_year;
        if($date_time)
        {
            $date_time = date('d-m-Y', strtotime($date_time));
        }


        $invoice_generation_name = $invoiceFor->full_name;
        $invoice_generation_nric = $invoiceFor->nric;


            $file_data = "";


 $file_data.="<table align='center' width='100%'>
        <tr>
                  <td style='text-align: left' width='20%' ><b>INVOICE</b></td>
                  <td style='text-align: center' width='30%' ></td>

          <td style='text-align: right' width='40%' ><img src='$signature' width='120px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'></td>
        </tr>
    </table>";

        $invoice_type = $mainInvoice->type;
        

            $file_data = $file_data ."

            <table width='100%'>
            <tr>
             <td>$invoice_type Name : $invoice_generation_name </td>
             <td></td>
             <td>Invoice No: $invoice_number</td>
             <td></td>
             </tr>
              <tr>
             <td>$invoice_type Code / No:  </td>
             <td></td>
             <td>Invoice Date: $date_time</td>
             <td></td>
             </tr>
             </table>



             <table width='100%' border='1' style='margin-top:20px;border-collapse: collapse;'>
              <tr>
               <td><b>No</b></td>
               <td><b>DESCRIPTION</b></td>
               <td><b>UNIT PRICE ($currency)</b></td>
               <td><b>TOTAL ($currency)</b></td>
              </tr>
               <tr>
               <td></td>
               <td>Program Name : $programme_code - $programme_name</td>
               <td></td>
               <td></td>
               </tr>
               <tr>
               <td></td>
               <td>INTAKE : $intake_name</td>
               <td></td>
               <td></td>
               </tr>";



        if($remarks == 'Student Course Registration')
        {

        $mainInvoiceDetailsList = $this->partner_university_invoice_model->getMainInvoiceDetailsForCourseRegistrationShow($id_partner_university_invoice);

        $semesterDetails = $this->partner_university_invoice_model->getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id_partner_university_invoice);

            // echo "<Pre>";print_r($semesterDetails);exit;

            if($semesterDetails)
            {
                $semester_name = $semesterDetails->name;
                $semester_code = $semesterDetails->code;
                $start_date = $semesterDetails->start_date;


                if($start_date)
                {
                    $start_date = date('Y-m', strtotime($start_date));
                }


               $file_data = $file_data ."
               <tr>
               <td></td>
               <td>SEMESTER : $semester_name </td>
               <td></td>
               <td></td>
               </tr>
                <tr>
               <td style='padding-top:20px;'></td>
               <td></td>
               <td></td>
               <td></td>
               </tr>
               ";



            foreach ($mainInvoiceDetailsList as $value)
            {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;
            $frequency_mode = $value->frequency_mode;
            $amount_calculation_type = $value->amount_calculation_type;
            $id_reference = $value->id_reference;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));
                if($description == 'CREDIT HOUR MULTIPLICATION' && $id_reference > 0)
                {

            // echo "<Pre>";print_r($value);exit;
                    $course_code = $value->course_code;
                    $course_name = $value->course_name;

            
                $file_data = $file_data ."
               <tr>
               <td></td>
               <td>$course_code - $course_name</td>
               <td style='text-align:right;'>$amount</td>
               <td></td>
               </tr>";

                }else
                {


                $file_data = $file_data ."
               <tr>
               <td></td>
               <td>$fee_setup</td>
               <td style='text-align:right;'>$amount</td>
               <td></td>
               </tr>";

                }

            }

          }else
          {

                $mainInvoiceDetailsList = $this->partner_university_invoice_model->getMainInvoiceDetails($id_partner_university_invoice);


                foreach ($mainInvoiceDetailsList as $value)
                {

                    $description = $value->description;
                    $amount = $value->amount;
                    $fee_setup = $value->fee_setup;
                    $frequency_mode = $value->frequency_mode;
                    $amount_calculation_type = $value->amount_calculation_type;

                    $amount = number_format($amount, 2, '.', ',');

                        // $acqDate   = date("d/m/Y", strtotime($acqDate));

                    

                    $file_data = $file_data ."
                   <tr>
                   <td></td>
                   <td>$fee_setup</td>
                   <td>$frequency_mode</td>
                   <td style='text-align:right;'>$amount</td>
                   </tr>";


                  // $i++;

                }
            }
        }
        else
        {


        $mainInvoiceDetailsList = $this->partner_university_invoice_model->getMainInvoiceDetails($id_partner_university_invoice);


          foreach ($mainInvoiceDetailsList as $value)
          {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;
            $frequency_mode = $value->frequency_mode;
            $amount_calculation_type = $value->amount_calculation_type;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));

            
            $file_data = $file_data ."
               <tr>
               <td></td>
               <td>$fee_setup</td>
               <td>$frequency_mode</td>
               <td style='text-align:right;'>$amount</td>
               </tr>";
          // $i++;

          }
        }


        $amount_c = $invoice_total;

    $invoice_total = number_format($invoice_total, 2, '.', ',');
    $amount_word = $this->getAmountWordings($amount_c);

    $amount_word = ucwords($amount_word);


    $file_data = $file_data ."

        <tr>
           <td></td>
           <td style='text-align:center;'><b>TOTAL</b></td>
           <td></td>
           <td style='text-align:right;'><b>$invoice_total</b></td>
        </tr>


        <tr>
           <td colspan='3' style='text-align:center;'><b>GRAND TOTAL</b></td>
           <td style='text-align:right;'><b>$invoice_total</b></td>
        </tr>

        <tr>
           <td colspan='4' style='text-align:center;'><b>$currency : $amount_word</b></td>
        </tr>";



        $file_data = $file_data ."
               </table>";





        if($type == 'Partner University')
        {
            $student_data = $this->partner_university_invoice_model->getMainInvoiceStudentDetails($id_partner_university_invoice,$mainInvoice->applicant_partner_fee);

            // echo "<Pre>"; print_r($student_data);exit;

            if(!empty($student_data))
            {

             $file_data = $file_data ."
             
            <br>
            
            <h4 style='text-align: center;'>Students List Partner University Invoice</h4>
            
            <table width='100%' border='1' style='margin-top:20px;border-collapse: collapse;'>
                    <tr>
                        <th>Sl. No</th>
                        <th>Student Name</th>
                        <th>Student NRIC</th>
                        <th>Program</th>
                        <th>Intake</th>
                        <th>Qualification</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th style='text-align: center;'>Amount</th>
                    </tr>";

                $total_amount = 0;
                for($i=0;$i<count($student_data);$i++)
                {
                    
                    // echo "<Pre>"; print_r($student_data[$i]);exit;

                    $id = $student_data[$i]->id;
                    $full_name = $student_data[$i]->student_name;
                    $nric = $student_data[$i]->nric;
                    $email_id = $student_data[$i]->email_id;
                    $phone = $student_data[$i]->phone;
                    $program_code = $student_data[$i]->program_code;
                    $program_name = $student_data[$i]->program_name;
                    $intake_year = $student_data[$i]->intake_year;
                    $intake_name = $student_data[$i]->intake_name;
                    $qualification_name = $student_data[$i]->qualification_name;
                    $qualification_name = $student_data[$i]->qualification_name;
                    $amount = $student_data[$i]->amount;
                    $j = $i+1;
                    $file_data = $file_data ."
                    <tr>
                        <td>$j</td>
                        <td>$full_name</td>                        
                        <td>$nric</td>                           
                        <td>$program_code - $program_name</td>                           
                        <td>$intake_year - $intake_name</td>                           
                        <td>$qualification_name - $qualification_name</td>                           
                        <td>$email_id</td>
                        <td>$phone</td>
                        <td>$amount</td>
                    </tr>";

                    $total_amount = $total_amount + $amount;
                }

                $file_data = $file_data ."
                    <tr>
                        <td colspan='7'></td>
                        <td>Total Amount : </td>
                        <td>$total_amount</td>
                    </tr>
                    </table>";
            }
        }

        // echo "<Pre>"; print_r($file_data);exit;
            






        $bankDetails = $this->partner_university_invoice_model->getBankRegistration();


        if($bankDetails && $organisationDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;
            

            $organisation_name = $organisationDetails->name;




             $file_data = $file_data ."
        <p>1. All cheque should be crossed and make payable to : </p>

    <table align='center' width='100%'>
      <tr>
            <td style='text-align: left' width='30%'>PAYEE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$organisation_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>BANK</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ADDRESS</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ACCOUNT NO</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$account_no</td>
      </tr>
      <tr>
            <td style='text-align: left' width='30%'>SWIFT CODE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_code</td>
      </tr>

      
    </table>
    <p> 2. This is auto generated invoice. No signature is required : </p>
      ";


        }


      $file_data = $file_data ."
    <pagebreak>";

    
        // echo "<Pre>";print_r($file_data);exit;


            // $mpdf->SetFooter('<div>Inventory Management</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);

            $mpdf->Output($type . '_INVOICE_'.$invoice_number.'.pdf', 'D');
            exit;
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo "<Pre>";print_r($tempData);exit();
        $student_data = $this->partner_university_invoice_model->studentSearch($tempData);
        // echo "<Pre>";print_r($student_data);exit();

        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script>";




         $table .= "
         <br>
         <h4> Select Students For Partner University Invoice</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>                           
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                           
                    <td>$qualification_name - $qualification_name</td>                           
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_detail_student[]' name='id_detail_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }

    function viewStudentFeeBulkPartnerDetails($id_pu_invoice_student_details)
    {
        $data = $this->partner_university_invoice_model->viewStudentFeeBulkPartnerDetails($id_pu_invoice_student_details);
        
        // echo "<Pre>"; print_r($data);exit;

        if(!empty($data))
        {


         $table = "";


         $table .= "
         <br>
         <h4> Fee Structure (Billable)</h4>

         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Frequency Mode</th>
                    <th>Amount Calculation Type</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>";

                $total = 0;
            for($i=0;$i<count($data);$i++)
            {
                $fee_setup_name = $data[$i]->fee_setup_name;
                $fee_steup_code = $data[$i]->fee_steup_code;
                $frequency_mode = $data[$i]->frequency_mode;
                $amount_calculation_type = $data[$i]->amount_calculation_type;
                $quantity = $data[$i]->quantity;
                $amount = $data[$i]->amount;

                // if($currency_name != '')
                // {
                //     $currency = $currency_name;
                // }

                // if($is_installment == 0)
                // {
                //     $type = 'Installment';
                // }
                // else
                // {
                //     $type = 'Per Semester';
                // }
            

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$fee_steup_code - $fee_setup_name</td>                           
                    <td>$frequency_mode</td>                           
                    <td>$amount_calculation_type</td>                        
                    <td>$quantity</td>             
                    <td>$amount</td>               
                </tr>";

                $total = $total + $amount;

            }
                $table .= "
                <tr>
                    <td colspan='4'></td>                      
                    <td>Total : </td>               
                    <td>$total</td>               
                </tr>";


                        // <a onclick='viewFeeStructureDetails($id)' title='View Fee Details'>View</a>

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Fee Structure Data Found, Add Fee Structure</h4>";
        }
        echo $table;exit;
    }
}
