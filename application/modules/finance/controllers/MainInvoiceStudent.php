<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MainInvoiceStudent extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_invoice_student_model');
        $this->isLoggedIn();
    }

    function comingSoon()
    {
        $this->global['pageTitle'] = 'Inventory Management : Coming Soon';
        $this->loadViews("main_invoice_student/coming_soon", $this->global, NULL, NULL);
    }    

    function list()
    {
        if ($this->checkAccess('main_invoice_student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['type'] = 'Student';
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->main_invoice_student_model->getMainInvoiceListByStatus($formData);

            $data['programmeList'] = $this->main_invoice_student_model->programmeListByStatus('1');
            $data['studentList'] = $this->main_invoice_student_model->studentList();

            // echo "<Pre>";print_r($data['mainInvoiceList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Main Invoice List';
            $this->loadViews("main_invoice_student/list", $this->global, $data, NULL);
        }
    }



    function partnerUniversityInvoiceList()
    {
        if ($this->checkAccess('main_invoice_student.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            $formData['type'] = 'Partner University';
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->main_invoice_student_model->getMainInvoiceListByStatus($formData);

            $data['programmeList'] = $this->main_invoice_student_model->programmeListByStatus('1');
            $data['partnerUniversityList'] = $this->main_invoice_student_model->partnerUniversityListByStatus('1');

// echo "<Pre>";print_r($data['mainInvoiceList']);exit();
            $this->global['pageTitle'] = 'Inventory Management : Partner University Main Invoice List';
            $this->loadViews("main_invoice_student/partner_university_invoice_list", $this->global, $data, NULL);
        }
    }

    
    function add()
    {
        if ($this->checkAccess('main_invoice_student.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;                    
            
            if($this->input->post())
            {

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;


                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $fee_type = $this->security->xss_clean($this->input->post('fee_type'));
                $status = $this->security->xss_clean($this->input->post('status'));


                if($fee_type == 'Individual')
                {

                    $invoice_number = $this->main_invoice_student_model->generateMainInvoiceNumber();

                
                    $data = array(
                        'invoice_number' => $invoice_number,
                        'total_amount' => $total_amount,
                        'amount_before_gst' => $amount_before_gst,
                        'invoice_total' => $total_amount,
                        'balance_amount' => $total_amount,
                        'paid_amount' => '0',
                        'id_student' => $id_student,
                        'id_program' => $id_programme,
                        'id_intake' => $id_intake,
                        'type' => $type,
                        'remarks' => $remarks,
                        'fee_type' => $fee_type,
                        'currency' => $currency,
                        'status' => '1',
                        'created_by' => $user_id
                    );

                    // echo "<Pre>";print_r($data);exit;
                    $inserted_id = $this->main_invoice_student_model->addNewMainInvoice($data);

                    $amount = $this->security->xss_clean($this->input->post('amount'));
                    $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
                    $temp_details = $this->main_invoice_student_model->getTempMainInvoiceDetails($id_session);

                     for($i=0;$i<count($temp_details);$i++)
                     {
                        
                        $amount = $temp_details[$i]->amount;
                        $id_fee_item = $temp_details[$i]->id_fee_item;

                         $detailsData = array(
                            'id_main_invoice' => $inserted_id,
                            'quantity' => 1,
                            'price' => $amount,
                            'price_before_gst' => $amount,
                            'amount' => $amount,
                            'id_fee_item' => $id_fee_item,
                            'status' => '1',
                            'created_by' => $user_id
                        );
                        //print_r($details);exit;
                        $result = $this->main_invoice_student_model->addNewMainInvoiceDetails($detailsData);
                     }

                    $this->main_invoice_student_model->deleteTempDataBySession($id_session);

                }
                elseif($fee_type == 'Registration')
                {
                    // $fee_structure = $this->main_invoice_student_model->getFeeStructureMaster($id_programme);

                    // if($fee_structure)
                    // {
                    //     $student_data['id_fee_structure'] = $fee_structure->id;

                    //     $updated_student = $this->main_invoice_student_model->editStudent($student_data, $id_student);
                    // }

                    $id_invoice = '0';

                    if($id_student == '')
                    {
                        $student_count = 0;
                    }
                    else
                    {
                        $student_count = 1;
                    }


                    if($student_count > 0)
                    {
                        // echo "<Pre>";print_r($student_count);exit;

                        $id_fee_structure_master = 0;

                        $fee_structure_master = $this->main_invoice_student_model->getFeeStructureMaster($id_programme);

                        if($fee_structure_master)
                        {
                            $id_fee_structure_master = $fee_structure_master->id;
                        }
                        
                        $get_data['id_programme'] = $id_programme;
                        $get_data['id_fee_structure_master'] = $id_fee_structure_master;
                        $get_data['currency'] = 'MYR';

                        // echo "<Pre>";print_r($get_data);exit;

                        $detail_data = $this->main_invoice_student_model->getFeeStructureByData($get_data);

                        // echo "<Pre>";print_r($detail_data);exit;

                        if(!empty($detail_data))
                        {
                            $finance_configuration = $this->main_invoice_student_model->getFinanceConfiguration();
                            $gst_percentage = $finance_configuration->tax_sst;

                            $invoice_number = $this->main_invoice_student_model->generateMainInvoiceNumber();

                            $invoice['invoice_number'] = $invoice_number;
                            $invoice['type'] = $type;
                            $invoice['fee_type'] = $fee_type;
                            // $invoice['remarks'] = 'Course Registration Fee';
                            $invoice['id_application'] = '0';
                            $invoice['id_student'] = $id_student;
                            $invoice['id_program'] = $id_programme;
                            $invoice['currency'] = 1;
                            $invoice['total_amount'] = '0';
                            $invoice['balance_amount'] = '0';
                            $invoice['paid_amount'] = '0';
                            $invoice['amount_before_gst'] = '0';
                            $invoice['gst_amount'] = '0';
                            $invoice['gst_percentage'] = $gst_percentage;
                            $invoice['status'] = 1;
                            $invoice['is_migrate_applicant'] = 0;
                            $invoice['created_by'] = $user_id;

                            // echo "<Pre>";print_r($invoice);exit;
                            
                            $inserted_id = $this->main_invoice_student_model->addNewMainInvoice($invoice);

                            $id_invoice = $inserted_id;

                            // echo "<Pre>";print_r($inserted_id);exit;

                            $total_amount = 0;
                            $total_amount_before_gst = 0;
                            $total_gst_amount = 0;
                            $total_discount_amount = 0;
                            $sibling_discount_amount = 0;
                            $employee_discount_amount = 0;
                            $alumni_discount_amount = 0;

                            // echo "<Pre>";print_r($detail_data);exit;

                            foreach ($detail_data as $fee_structure)
                            {
                                $gst_tax = '0';
                                $one_percent = 0;
                                $details_gst_amount = 0;
                                $amount_before_gst = 0;

                                $is_installment = $fee_structure->is_installment;
                                $id_training_center = $fee_structure->id_training_center;
                                $id_fee_item = $fee_structure->id_fee_item;
                                $amount = $fee_structure->amount;


                                $fee_setup = $this->main_invoice_student_model->getFeeSetup($id_fee_item);
                                // echo "<Pre>";print_r($fee_setup);exit;

                                if($fee_setup)
                                {
                                    $gst_tax = $fee_setup->gst_tax;
                                }
                                
                                $amount_before_gst = $amount;

                                if($gst_tax == '1')
                                {
                                    if($gst_percentage > 0)
                                    {
                                        $one_percent = $amount * 0.01;
                                        $details_gst_amount = $one_percent * $gst_percentage;
                                    }

                                    // echo "<Pre>";print_r($details_gst_amount);exit;


                                    if($details_gst_amount > 0)
                                    {
                                        $amount = $amount + $details_gst_amount;
                                    }
                                }


                                    $data = array(
                                        'id_main_invoice' => $inserted_id,
                                        'id_fee_item' => $id_fee_item,
                                        'amount' => $amount * $student_count,
                                        'amount_before_gst' => $amount_before_gst * $student_count,
                                        'gst_amount' => $details_gst_amount * $student_count,
                                        'gst_percentage' => $gst_percentage,
                                        'price' => $amount,
                                        'price_before_gst' => $amount_before_gst,
                                        'quantity' => $student_count,
                                        'id_reference' => $fee_structure->id,
                                        'description' => 'Course Registration Fee',
                                        'status' => 1
                                    );

                                // echo "<Pre>";print_r($data);exit;

                                    $total_amount = $total_amount + ($amount * $student_count);
                                    $total_amount_before_gst = $total_amount_before_gst + ($amount_before_gst * $student_count);
                                    $total_gst_amount = $total_gst_amount + ($details_gst_amount * $student_count);

                                    $this->main_invoice_student_model->addNewMainInvoiceDetails($data);
                                // }
                                // echo "<Pre>";print_r($data);exit;
                            }

                            $total_invoice_amount = $total_amount;


                            $invoice_update['total_amount'] = $total_invoice_amount;
                            $invoice_update['balance_amount'] = $total_invoice_amount;
                            $invoice_update['invoice_total'] = $total_invoice_amount;
                            $invoice_update['amount_before_gst'] = $total_amount_before_gst;
                            $invoice_update['gst_amount'] = $total_gst_amount;
                            $invoice_update['total_discount'] = 0;
                            $invoice_update['paid_amount'] = '0';
                            $this->main_invoice_student_model->editMainInvoice($invoice_update,$inserted_id);


                            // if($inserted_id)
                            // {
                            //     // echo "<Pre>";print_r($inserted_id);exit;

                            //     $inserted_student_data = $this->main_invoice_student_model->addNewMainInvoiceHasStudents($id_student,$inserted_id,$total_invoice_amount);
                            // }
                        }
                    }




                    if($id_invoice != '0')
                    {
                        
                        $programme = $this->main_invoice_student_model->getProgramme($id_programme);

                        if($programme)
                        {
                            // echo "<Pre>"; print_r($programme);exit;
                            
                            $max_duration = $programme->max_duration;
                            $duration_type = $programme->duration_type;

                            $start_date = date('Y-m-d');

                            $data_student_has_programme = array(
                                'id_student' => $id_student,
                                'id_invoice' => $id_invoice,
                                'id_programme' => $id_programme,
                                'start_date' => $start_date,
                                'end_date' => date('Y-m-d', strtotime($start_date . "+" . $max_duration . " " . $duration_type) ),
                                'status' => 0
                            );
                            // echo "<Pre>"; print_r($data_student_has_programme);exit;

                            $id_student_has_programme = $this->main_invoice_student_model->addNewStudentHasProgramme($data_student_has_programme);

                        }
                    }
                }

                redirect('/finance/mainInvoiceStudent/list');
            }
            else
            {
                $this->main_invoice_student_model->deleteTempDataBySession($id_session);
            }
            $data['studentList'] = $this->main_invoice_student_model->studentList();
            $data['feeSetupList'] = $this->main_invoice_student_model->feeSetupList();
            $data['programmeList'] = $this->main_invoice_student_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_student_model->intakeList();
            $data['partnerUniversityList'] = $this->main_invoice_student_model->partnerUniversityListByStatus('1');
            $data['currencyList'] = $this->main_invoice_student_model->currencyListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Main Invoice';
            $this->loadViews("main_invoice_student/add", $this->global, $data, NULL);
        }
    }


    function getData() {
        
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('main_invoice_student.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/mainInvoiceStudent/list');
            }
            if($this->input->post())
            {
                redirect('/finance/mainInvoiceStudent/list');
            }
            $data['mainInvoice'] = $this->main_invoice_student_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->main_invoice_student_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_student_model->getMainInvoiceDiscountDetails($id);
            
            if($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'CORPORATE')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceCorporateData($data['mainInvoice']->id_student);
            }

            $data['degreeTypeList'] = $this->main_invoice_student_model->qualificationListByStatus('1');
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['invoiceFor']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Main Invoice';
            $this->loadViews("main_invoice_student/edit", $this->global, $data, NULL);
        }
    }

    function editPartnerInvoice($id = NULL)
    {
        if ($this->checkAccess('main_invoice_student.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/mainInvoiceStudent/list');
            }
            if($this->input->post())
            {
                redirect('/finance/mainInvoiceStudent/list');
            }
            $data['mainInvoice'] = $this->main_invoice_student_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->main_invoice_student_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_student_model->getMainInvoiceDiscountDetails($id);
            if($data['mainInvoice']->type == 'Applicant')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceApplicantData($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Partner University')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoicePartnerUniversityData($data['mainInvoice']->id_student);
                // $data['studentDetails'] = $this->main_invoice_student_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            elseif($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'Sponser')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceSponserData($data['mainInvoice']->id_sponser);
                $data['studentDetails'] = $this->main_invoice_student_model->getStudentByStudent($data['mainInvoice']->id_student);
            }
            $data['degreeTypeList'] = $this->main_invoice_student_model->qualificationListByStatus('1');
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Partner University Main Invoice';
            $this->loadViews("main_invoice_student/partner_invoice_view", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('main_invoice_student.cancel') == 0)
        // if ($this->checkAccess('main_invoice_student.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/mainInvoiceStudent/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));

                // echo "<Pre>";print_r($status);exit();

                if($status)
                {

                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $result = $this->main_invoice_student_model->editMainInvoice($data,$id);
                
                }
            //      if($status == '2')
            //      {
            //         $detailsDatas = $this->Pr_model->getPrDetails($id);
            //         foreach ($detailsDatas as $detailsData)
            //         {
            // // echo "<Pre>";print_r($detailsData);exit();
            //             $details_data['id_budget_allocation'] = $detailsData->id_budget_allocation;
            //             $details_data['total_final'] = $detailsData->total_final;
            //             $updated_budget_amount = $this->Pr_model->updateBudgetAllocationAmountOnReject($details_data);

            //         }
            //      }

                redirect('/finance/mainInvoiceStudent/approvalList');
            }
            $data['mainInvoice'] = $this->main_invoice_student_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->main_invoice_student_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->main_invoice_student_model->getMainInvoiceDiscountDetails($id);
            if($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'CORPORATE')
            {
                $data['invoiceFor'] = $this->main_invoice_student_model->getMainInvoiceCorporateData($data['mainInvoice']->id_student);
            }
            
            $data['degreeTypeList'] = $this->main_invoice_student_model->qualificationListByStatus('1');
            // echo "<Pre>";  print_r($data);exit;
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("main_invoice_student/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('main_invoice_student.cancel_list') == 0)
        // if ($this->checkAccess('main_invoice_student_approval.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

           //  $resultprint = $this->input->post();

           // if($resultprint)
           //  {
           //   switch ($resultprint['button'])
           //   {
           //      case 'approve':

           //           for($i=0;$i<count($resultprint['checkvalue']);$i++)
           //              {

           //               $id = $resultprint['checkvalue'][$i];
                         
           //               $result = $this->main_invoice_student_model->editMainInvoiceList($id);
           //              }
           //              redirect($_SERVER['HTTP_REFERER']);
           //           break;


           //      case 'search':

           //           $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
           //          $formData['name'] = $this->security->xss_clean($this->input->post('name'));
           //          $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
           //          $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
           //          $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
           //          $formData['status'] = '0';
         
           //          $data['searchParam'] = $formData;
           //          $data['mainInvoiceList'] = $this->main_invoice_student_model->getMainInvoiceListByStatus($formData);
                     
           //           break;
                 
           //      default:
           //           break;
           //   }
                
           //  }
            
            
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['invoice_number'] = $this->security->xss_clean($this->input->post('invoice_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '1';
 
            $data['searchParam'] = $formData;
            $data['mainInvoiceList'] = $this->main_invoice_student_model->getMainInvoiceListByStatusForCancellation($formData);

            $data['programmeList'] = $this->main_invoice_student_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_student_model->intakeListByStatus('1');
            $data['applicantList'] = $this->main_invoice_student_model->applicantList();
            $data['studentList'] = $this->main_invoice_student_model->studentList();
            $data['sponserList'] = $this->main_invoice_student_model->sponserListByStatus('1');
            $data['partnerUniversityList'] = $this->main_invoice_student_model->partnerUniversityListByStatus('1');
            
            // echo "<Pre>"; print_r($data['mainInvoiceList']);exit;
            
//             $array = $this->security->xss_clean($this->input->post('checkvalue'));
//             if (!empty($array))
//             {
// // echo "<Pre>"; print_r($array);exit;
                
//             }


            $this->global['pageTitle'] = 'Inventory Management : Approve Main Invoice';
            $this->loadViews("main_invoice_student/approval_list", $this->global, $data, NULL);
        }
    }


    function getStudentByProgram()
    {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $type = $formData['type'];
            switch ($type)
            {
                case 'CORPORATE':

                    $table = $this->getCorporateList($formData);

                    break;

                case 'Student':

                    $table = $this->getStudentList($formData);
                    
                    break;


                default:
                    # code...
                    break;
            }

            echo $table;
            exit;            
    }

    function getStudentList($data)
    {
        $data = $this->main_invoice_student_model->getStudentListByData();
                // echo "<Pre>";print_r($data);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_student' id='id_student' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $data[$i]->id;
            $nric = $data[$i]->nric;
            $full_name = $data[$i]->full_name;

            $table.="<option value=".$id.">".$nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function getCorporateList($data)
    {
        $data = $this->main_invoice_student_model->getCorporateListByData();
                // echo "<Pre>";print_r($data);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_student' id='id_student' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $data[$i]->id;
            $registration_number = $data[$i]->registration_number;
            $name = $data[$i]->name;

            $table.="<option value=".$id.">".$registration_number . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        unset($tempData['id']);
        $inserted_id = $this->main_invoice_student_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->main_invoice_student_model->getTempMainInvoiceDetails($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;
        if(!empty($temp_details))
        {

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_setup</td>
                            <td>$amount</td>                           
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $total_amount = number_format($total_amount, 2, '.', ',');


                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='inv-total-amount' value='$total_amount' />$total_amount</td>                           
                            <td></td>
                        </tr>";

        $table.= "</table>";
        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->main_invoice_student_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempadd1()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
        $amount = $this->security->xss_clean($this->input->post('amount'));

        // echo "<Pre>";  print_r($id_session . "=". $amount);exit;
        $data = array(
               'id_session' => $id_session,
               'id_fee_item' => $id_fee_item,
               'amount' => $amount
            );
        $inserted_id = $this->main_invoice_student_model->addNewTempMainInvoiceDetails($data);
        //echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'amount' => $amount,
                'id_fee_item' => $id_fee_item,
            );
        $temp_details = $this->main_invoice_student_model->getTempMainInvoiceDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $fee_setup = $temp_details[$i]->fee_setup;
                    $amount = $temp_details[$i]->amount;
                    $id = $temp_details[$i]->id;

                    $table .= "
                <tr>
                    <td>$fee_setup</td>
                    <td ><input type='hidden' id='inv-total-amount' value='$amount' />$amount</td>             

                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->main_invoice_student_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";


            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Program</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";
            echo $table;
            exit;
    }

    function getApplicantByApplicantId($id)
    {
         // print_r($id);exit;
            $student_data = $this->main_invoice_student_model->getApplicantByApplicantId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $phone = $student_data->phone;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Applicant Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Applicant NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Phone No.</dt>
                                <dd>$phone</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";
            echo $table;
            exit;
    }



    function generateMainInvoice($id_main_invoice)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();

                    // print_r($base_url);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            // $mpdf->SetHeader("<div style='text-align: left;'>Inventory Management
            //                    </div>");


            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');
            $currentDateTime = date('d_m_Y_His');

        $organisationDetails = $this->main_invoice_student_model->getOrganisation();


        // echo "<Pre>";print_r($organisationDetails);exit;

        

        // $signature = $_SERVER['DOCUMENT_ROOT']."/assets/img/speed_logo.svg";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }




        $mainInvoice = $this->main_invoice_student_model->getMainInvoice($id_main_invoice);


        if($mainInvoice->type == 'Student')
        {
            $invoiceFor = $this->main_invoice_student_model->getMainInvoiceStudentData($mainInvoice->id_student);
        }elseif($mainInvoice->type == 'CORPORATE')
        {
            $invoiceFor = $this->main_invoice_student_model->getMainInvoiceCorporateData($mainInvoice->id_student);
        }


        // echo "<Pre>";print_r($invoiceFor);exit;


        $type = $mainInvoice->type;
        $invoice_number = $mainInvoice->invoice_number;
        $date_time = $mainInvoice->date_time;
        $remarks = $mainInvoice->remarks;
        $currency = $mainInvoice->currency_name;
        $total_amount = $mainInvoice->total_amount;
        $invoice_total = $mainInvoice->invoice_total;
        $balance_amount = $mainInvoice->balance_amount;
        $paid_amount = $mainInvoice->paid_amount;
        $programme_name = $mainInvoice->programme_name;
        $programme_code = $mainInvoice->programme_code;
        $intake_name = $mainInvoice->intake_name;
        $intake_year = $mainInvoice->intake_year;
        if($date_time)
        {
            $date_time = date('d-m-Y', strtotime($date_time));
        }


        $invoice_generation_name = $invoiceFor->full_name;
        $invoice_generation_nric = $invoiceFor->nric;


            $file_data = "";


 $file_data.="<table align='center' width='100%'>
        <tr>
                  <td style='text-align: left;font-size:30px;'><b>INVOICE</b></td>
                  <td style='text-align: center' width='30%' ></td>

          <td style='text-align: right' width='40%' ><img src='$signature' width='180px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'> <br/><br/><br/></td>
        </tr>
    </table>";

            $invoice_type = $mainInvoice->type;


            $file_data = $file_data ."

            <table width='100%' style='font-size:16px;'>
            <tr>
             <td>$invoice_type Name : $invoice_generation_name </td>
             <td width='25%'></td>
             <td style='text-align:right;'>Invoice No  : $invoice_number</td>
             <td style='text-align:right;'></td>
             </tr>
              <tr>
             <td>IC No / Passport No: $invoice_generation_nric </td>
             <td></td>
             <td style='text-align:right;'>Invoice Date: $date_time</td>
             <td style='text-align:right;'></td>
             </tr>
             </table>




             <table width='100%' height='50%'  style='margin-top:30px;border-collapse: collapse;padding:10px 10px;height:75%;font-size:16px;' border='1'>
              <tr>
               <th style='text-align:center;line-height:30px;'><b>No</b></th>
               <th style='text-align:center;'><b>DESCRIPTION</b></th>
               <th style='text-align:center;'><b>UNIT PRICE (RM)</b></th>
               <th style='text-align:center;'><b>TOTAL (RM)</b></th>
              </tr>
               <tr>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;text-align:center;'>$programme_code - $programme_name,<br/>Being charge for,</td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               </tr>
              ";

 // 


        if($remarks == 'Student Course Registration')
        {

        $mainInvoiceDetailsList = $this->main_invoice_student_model->getMainInvoiceDetailsForCourseRegistrationShow($id_main_invoice);

        $semesterDetails = $this->main_invoice_student_model->getSemesterByMainInvoiceDetailsForCourseRegistrationShow($id_main_invoice);

            // echo "<Pre>";print_r($semesterDetails);exit;

            if($semesterDetails)
            {
                $semester_name = $semesterDetails->name;
                $semester_code = $semesterDetails->code;
                $start_date = $semesterDetails->start_date;


                if($start_date)
                {
                    $start_date = date('Y-m', strtotime($start_date));
                }


               // $file_data = $file_data ."
              
               //  <tr>
               // <td style='padding-top:20px;'></td>
               // <td></td>
               // <td></td>
               // <td></td>
               // </tr>
               // ";


            foreach ($mainInvoiceDetailsList as $value)
            {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;
            $id_reference = $value->id_reference;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));
                if($description == 'CREDIT HOUR MULTIPLICATION' && $id_reference > 0)
                {

            // echo "<Pre>";print_r($value);exit;
                    $course_code = $value->course_code;
                    $course_name = $value->course_name;

            
                $file_data = $file_data ."
               <tr>
               <td style='padding-top:20px;'> </td>
               <td style='padding-top:20px;' >$course_code - $course_name</td>
               <td style='padding-top:20px;' style='text-align:right;'>$amount</td>
               <td style='padding-top:20px;' ></td>
               </tr>";

                }else
                {


                $file_data = $file_data ."
               <tr>
               <td style='padding-top:20px;'></td>
               <td style='padding-top:20px;'>$fee_setup</td>
               <td style='padding-top:20px;' style='text-align:right;'>$amount</td>
               <td style='padding-top:20px;'></td>
               </tr>";

                }

            }

          }else
          {

            $mainInvoiceDetailsList = $this->main_invoice_student_model->getMainInvoiceDetails($id_main_invoice);


            foreach ($mainInvoiceDetailsList as $value)
            {

                $description = $value->description;
                $amount = $value->amount;
                $fee_setup = $value->fee_setup;

                $amount = number_format($amount, 2, '.', ',');

                    // $acqDate   = date("d/m/Y", strtotime($acqDate));

                

                $file_data = $file_data ."
               <tr>
               <td style='style='padding-top:20px;padding-bottom:20px;'></td>
               <td style='style='padding-top:20px;padding-bottom:20px;'>$fee_setup</td>
               <td style='style='padding-top:20px;text-align:right;padding-top:20px;'>$amount</td>
                              <td></td>

               </tr>";


              // $i++;

            }
            }
        }
        else
        {


        $mainInvoiceDetailsList = $this->main_invoice_student_model->getMainInvoiceDetails($id_main_invoice);

          $i=1;
          foreach ($mainInvoiceDetailsList as $value)
          {

            $description = $value->description;
            $amount = $value->amount;
            $fee_setup = $value->fee_setup;

            $amount = number_format($amount, 2, '.', ',');

                // $acqDate   = date("d/m/Y", strtotime($acqDate));

            
            $file_data = $file_data ."
               <tr>
               <td style='padding-top:20px;padding-bottom:10px;'>$i .</td>
               <td style='padding-top:20px;padding-bottom:10px;text-align:center;'>$fee_setup</td>
               <td style='padding-top:20px;padding-bottom:10px;' style='text-align:right;'>$amount</td>
               <td style='padding-top:20px;padding-bottom:10px;'></td>
               </tr>";
          $i++;

          }
        }


        $amount_c = $invoice_total;

    $invoice_total = number_format($invoice_total, 2, '.', ',');
    $amount_word = $this->getAmountWordings($amount_c);

    $amount_word = ucwords($amount_word);


    $file_data = $file_data ."



        <tr>
           <td colspan='3' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>GRAND TOTAL</b></td>
           <td style='text-align:right;padding-top:20px;padding-bottom:10px;'><b>$invoice_total</b></td>
        </tr>

        <tr>
           <td colspan='4' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>$currency : $amount_word</b></td>
        </tr>";

            $file_data = $file_data ."
               </table>";






        $bankDetails = $this->main_invoice_student_model->getBankRegistration();


        if($bankDetails && $organisationDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;
            

            $organisation_name = $organisationDetails->name;




        $file_data = $file_data ."<br/><br/>
        <p>1. All cheque should be crossed and make payable to:: </p>
    <table align='center' width='100%' style='font-size:16px;'>
      <tr>
            <td style='text-align: left' width='30%' valign='top'>PAYEE</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$organisation_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%' valign='top'>BANK</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$bank_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%' valign='top'>ADDRESS</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%' valign='top'>ACCOUNT NO</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$account_no</td>
      </tr>
      <tr>
            <td style='text-align: left' width='30%' valign='top'>SWIFT CODE</td>
            <td style='text-align: center' width='5%' valign='top'>:</td>
            <td style='text-align: left' width='65%'>$bank_code</td>
      </tr>

      
    </table>
    <p> 2. This is auto generated Receipt. No signature is required. </p>
      ";

        }
    
        // echo "<Pre>";print_r($file_data);exit;

        // $mpdf->SetFooter('<div>Inventory Management</div>');
        // echo $file_data;exit;
        // $stylesheet = file_get_contents('pdfdownload_18_19.css'); // external css
        // $mpdf->WriteHTML($stylesheet,1);

        $mpdf->WriteHTML($file_data);
        $mpdf->Output($type . '_INVOICE_'.$invoice_number.'_'.$currentDateTime.'.pdf', 'D');
        exit;

    }

    function getRegistrationFeeListByProgramme()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));

        $id_programme = $formData['id_programme'];


        $id_fee_structure_master = 0;
        $id_fee_master_currency = 1;

        $fee_structure_master = $this->main_invoice_student_model->getFeeStructureMaster($id_programme);
        
        // echo "<Pre>";print_r($fee_structure_master);exit;

        if($fee_structure_master)
        {
            $id_fee_structure_master = $fee_structure_master->id;
            $id_fee_master_currency = $fee_structure_master->id_currency;
        }
        
        $get_data['id_programme'] = $id_programme;
        $get_data['id_fee_structure_master'] = $id_fee_structure_master;
        $get_data['currency'] = 'MYR';

        // echo "<Pre>";print_r($get_data);exit;

        $detail_data = $this->main_invoice_student_model->getFeeStructureByData($get_data);

        // echo "<Pre>";print_r($detail_data);exit;

        if(!empty($detail_data))
        {

            $table = "

        <div class='form-container'>
            <h4 class='form-group-title'>Main Invoice Details</h4>

            <input type='hidden' id='id_fee_master_currency' name='id_fee_master_currency' value='$id_fee_master_currency' />


            <table  class='table' id='list-table'>
                <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                </tr>";
                    // <th>Delete</th>
                $total_amount = 0;
                $j = 1;
                for($i=0;$i<count($detail_data);$i++)
                {
                    $fee_structure = $detail_data[$i]->fee_structure;
                    $fee_structure_code = $detail_data[$i]->fee_structure_code;
                    $amount = $detail_data[$i]->amount;

                    $table .= "
                <tr>
                    <td >$j</td>                    
                    <td>$fee_structure_code - $fee_structure</td>
                    <td >$amount</td>                    
                </tr>";
                    // <td>
                    //     <span onclick='deleteid($id)'>Delete</a>
                    // <td>
                $total_amount = $total_amount + $amount;
                }

                $total_amount = number_format($total_amount, 2, '.', ',');
                
                $table .= "
                <tr>
                    <td></td>
                    <td>Total : </td>
                    <td ><input type='hidden' id='registration_fee_total_amount' name='registration_fee_total_amount' value='$total_amount' />$total_amount</td>                    
                </tr>";
                        
            $table .= "
            </table>
        </div>";
        

        echo $table;
        
        }
    }
}