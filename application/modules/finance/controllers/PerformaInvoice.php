<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PerformaInvoice extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('performa_invoice_model');
                $this->load->model('fee_setup_model');

      $this->load->database();
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('performa_invoice.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['performaInvoiceList'] = $this->performa_invoice_model->performaInvoiceList();
            $this->global['pageTitle'] = 'Inventory Management : Performa Invoice List';
            $this->loadViews("performa_invoice/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('performa_invoice.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                
                $type_of_invoice = $this->security->xss_clean($this->input->post('type_of_invoice'));
                $performa_number = $this->security->xss_clean($this->input->post('performa_number'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_application = $this->security->xss_clean($this->input->post('id_application'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));

            
                $data = array(
                    'type_of_invoice' => $type_of_invoice,
                    'performa_number' => $performa_number,
                    'total_amount' => $total_amount,
                    'id_student' => $id_student,
                    'id_application' => $id_application,
                    'remarks' => $remarks,
                    'date_time' => $date_time,
                    'status' => $status
                );
                //print_r($data);exit;
                $inserted_id = $this->performa_invoice_model->addNewPerformaInvoice($data);
                $details = $this->performa_invoice_model->getTempPerformaInvoice($id_session);
                 for($i=0;$i<count($details);$i++)
                 {
                    $feename = $details[$i]->name;
                    $amount = $details[$i]->amount;
                    $id_fee_item = $details[$i]->id_fee_item;

                     $detailsData = array(
                        'id_performa' => $inserted_id,
                        'amount' => $amount,
                        'id_fee_item' => $id_fee_item,
                    );
                    //print_r($details);exit;
                    $result = $this->performa_invoice_model->addNewPerformaInvoiceDetails($detailsData);
                 }

                $this->performa_invoice_model->deleteTempDataBySession($id_session);
                redirect('/finance/performaInvoice/list');
            }
            //print_r($data['stateList']);exit;
            $id_session = $this->session->my_session_id;
            $this->performa_invoice_model->deleteTempData($id_session);

            $data['studentList'] = $this->performa_invoice_model->studentList();
            $data['applicantList'] = $this->performa_invoice_model->applicantList();
            $data['feeSetupList'] = $this->fee_setup_model->feeSetupList();

            $this->global['pageTitle'] = 'Inventory Management : Add Performa Invoice';
            $this->loadViews("performa_invoice/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        // print_r($_POST);exit;
        if ($this->checkAccess('performa_invoice.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/performaInvoice/list');
            }
            if($this->input->post())
            {
                $type_of_invoice = $this->security->xss_clean($this->input->post('type_of_invoice'));
                $performa_number = $this->security->xss_clean($this->input->post('performa_number'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_application = $this->security->xss_clean($this->input->post('id_application'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'type_of_invoice' => $type_of_invoice,
                    'performa_number' => $performa_number,
                    'total_amount' => $total_amount,
                    'id_student' => $id_student,
                    'id_application' => $id_application,
                    'remarks' => $remarks,
                    'date_time' => $date_time,
                    'status' => $status
                );
                
                $result = $this->performa_invoice_model->editPerformaInvoice($data,$id);
                redirect('/finance/performaInvoice/list');
            }
            $data['studentList'] = $this->performa_invoice_model->studentList();
            $data['applicantList'] = $this->performa_invoice_model->applicantList();
            $data['performaInvoice'] = $this->performa_invoice_model->getPerformaInvoice($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Performa Invoice';
            $this->loadViews("performa_invoice/edit", $this->global, $data, NULL);
        }
    }

    // function ajaxRequest()
 //   {
 //       $data['data'] = $this->db->get("temp_performa_invoice_details")->result();
 //       $this->load->view("performa_invoice/add", $data);
 //   }


     function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->performa_invoice_model->updateTempDetails($tempData,$id);
        }
        else
        {
            unset($tempData['id']);
            $inserted_id = $this->performa_invoice_model->addTempDetails($tempData);
// echo "<Pre>";  print_r($tempData);exit;
        }
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->performa_invoice_model->getTempPerformaInvoice($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_name = $temp_details[$i]->name;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_name</td>
                            <td>$amount</td>                           
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</table>";
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->performa_invoice_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 


    function tempadd1()
    {
        $id_session = $this->session->my_session_id;
        $amount = $this->security->xss_clean($this->input->post('amount'));
        $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));

        $data = array(
               'id_session' => $id_session,
               'id_fee_item' => $id_fee_item,
               'amount' => $amount
            );
        $inserted_id = $this->performa_invoice_model->addNewTempPerformaInvoiceDetails($data);

        $details = array(
                'id' => $inserted_id,
                'amount' => $amount,
                'id_fee_item' => $id_fee_item,
            );
        $details = $this->performa_invoice_model->getTempPerformaInvoice($id_session);

        if(!empty($details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($details);$i++)
                {
                    $feename = $details[$i]->name;
                    $amount = $details[$i]->amount;
                    $id = $details[$i]->id;

                    $table .= "
                <tr>
                    <td>$feename</td>
                    <td>$amount</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }
}
