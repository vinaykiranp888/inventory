<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeStructure extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_setup_model');
        $this->load->model('fee_structure_model');
        $this->load->model('fee_category_model');
        $this->load->model('frequency_mode_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('fee_structure.programme_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $data['searchParam'] = $formData;


            $data['feeStructureMasterList'] = $this->fee_structure_model->feeStructureMasterListSearch($formData);

            $data['feeStructureItemList'] = $this->fee_structure_model->getAllFeeitemsFromDb();
            $data['categoryList'] = $this->fee_structure_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->fee_structure_model->productTypeSetupListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Fee Structure List';
            $this->loadViews("fee_structure/fee_structure_master_list", $this->global, $data, NULL);
        }
    }

    function deleteitem($id)
    {
        $this->fee_structure_model->deletefromfeeitem($id);
        return 1;
    }

    function addFeeStructureMaster()
    {
        if ($this->checkAccess('fee_structure.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;


                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_intake_to = $this->security->xss_clean($this->input->post('id_intake_to'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));
                $id_program_scheme = $this->security->xss_clean($this->input->post('id_program_has_scheme'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'description' => $description,
                    'id_education_level' => $id_education_level,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_intake_to' => $id_intake_to,
                    'id_learning_mode' => $id_learning_mode,
                    'id_program_scheme' => $id_program_scheme,
                    'id_currency' => $id_currency,
                    'id_partner_university' => 1,
                    'status' => $status
                );
                $inserted_id = $this->fee_structure_model->addNewFeeStructureMaster($data);
                redirect('/finance/feeStructure/list');
            }
            
            // $data['degreeTypeList'] = $this->fee_structure_model->qualificationListByStatus('1');
            
            $data['currencyList'] = $this->fee_structure_model->currencyListByStatus('1');
            $data['programmeList'] = $this->fee_structure_model->getProgrammeByEducationLevelId();


            $this->global['pageTitle'] = 'Inventory Management : Add Fee Structure';
            $this->loadViews("fee_structure/add_fee_structure_master", $this->global, $data, NULL);
        }
    }

    function editFeeStructureMaster($id)
    {
        if ($this->checkAccess('fee_structure.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                

                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'description' => $description
                );
                $inserted_id = $this->fee_structure_model->editFeeStructureMaster($data,$id);
                redirect('/finance/feeStructure/list');
            }
            
            $data['feeStructuremaster'] = $this->fee_structure_model->getFeeStructureMaster($id);

            // echo "<Pre>"; print_r($data['feeStructuremaster']);exit;

            // $id_intake = $data['feeStructuremaster']->id_intake;
            // $id_programme = $data['feeStructuremaster']->id_programme;
            
            // $data['intake'] = $this->fee_structure_model->getIntakeById($id_intake);


            $data['degreeTypeList'] = $this->fee_structure_model->qualificationListByStatus('1');
            $data['currencyList'] = $this->fee_structure_model->currencyListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Fee Structure';
            $this->loadViews("fee_structure/edit_fee_structure_master", $this->global, $data, NULL);
        }
    }

    function show_intake($id = NULL)
    {
        if ($this->checkAccess('fee_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

        	if ($id == null)
            {
                redirect('/finance/feeStructure/list');
            }

            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['programme_id'] = $id;

            $data['intakeList'] = $this->fee_structure_model->intakeListByProgrammeId($id,$name);
            $data['programme'] = $this->fee_structure_model->getProgrammeById($id);
            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management :Fee Structure';
            $this->loadViews("fee_structure/intake_list", $this->global, $data, NULL);
        }
    }


    function showProgramScheme($id_intake,$id_program)
    {
        if ($this->checkAccess('fee_structure.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id_intake == null)
            {
                redirect('/finance/feeStructure/list');
            }

            $data['programme'] = $this->fee_structure_model->getProgrammeById($id_program);
            $data['intake'] = $this->fee_structure_model->getIntakeById($id_intake);
            $data['programSchemeList'] = $this->fee_structure_model->programSchemeList($id_program);

            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management :Fee Structure';
            $this->loadViews("fee_structure/program_scheme_list", $this->global, $data, NULL);
        }
    }

    
    function add($id_intake,$id_programme,$id_program_scheme)
    {
        if ($this->checkAccess('fee_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
                if($this->input->post())
                {
                    redirect('/finance/feeStructure/show_intake/'.$id_programme);
                }           

                
                $data['intake'] = $this->fee_structure_model->getIntakeById($id_intake);
                $data['programme'] = $this->fee_structure_model->getProgrammeById($id_programme);
                $data['programScheme'] = $this->fee_structure_model->getProgrammeScheme($id_program_scheme);

                $data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeListByStatus('1');
                $data['feeSetupList'] = $this->fee_setup_model->feeSetupListByStatus('1');
                $data['feeStructureList'] = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_programme,$id_intake,$id_program_scheme);
                $data['id_program_scheme'] = $id_program_scheme;


                $this->global['pageTitle'] = 'Inventory Management : Add Fee Structure';
                $this->loadViews("fee_structure/add", $this->global, $data, NULL);
            // }
        }
    }

    function programmeLandscapeList($id_programme = NULL)
    {
        if ($this->checkAccess('fee_structure.programme_landscape_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));

            $data['searchParam'] = $formData; 

            $data['intakeList'] = $this->fee_structure_model->intakeListByStatus('1');
            $data['programme'] = $this->fee_structure_model->getProgramme($id_programme);
            $data['id_programme'] = $id_programme;

            $data['programmeLandscapeList'] = $this->fee_structure_model->programmeLandscapeByProgrammeId($id_programme);
            $this->global['pageTitle'] = 'Inventory Management : Program Landscape List For Fee Structure';
            $this->loadViews("fee_structure/programme_landscape_list", $this->global, $data, NULL);
        }
    }

    function addFeeStructure($id_program_landscape,$id_fee_details=NULL)
    {

        // echo "<Pre>";print_r($id_fee_details);exit();

        if ($this->checkAccess('fee_structure.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
                $data['getProgrammeLandscapeLocal'] = $this->fee_structure_model->getFeeStructureMaster($id_program_landscape);


                $id_intake = $data['getProgrammeLandscapeLocal']->id_intake;
                $id_programme = $data['getProgrammeLandscapeLocal']->id_programme;
            

                // echo "<Pre>";print_r($data['getProgrammeLandscapeLocal']);exit();

                if($this->input->post())
                {
                    // echo "<Pre>";print_r($this->input->post());exit();

                    $id_fee_item = $this->security->xss_clean($this->input->post('one_id_fee_item'));
                    $is_registration_fee = $this->security->xss_clean($this->input->post('is_registration_fee'));
                    $amount = $this->security->xss_clean($this->input->post('one_amount'));


                    $fee_detail_data = array(
                        'id_programme' => $id_programme, 
                        'id_program_landscape' => $id_program_landscape, 
                        'id_training_center' => 1, 
                        'is_registration_fee' => $is_registration_fee, 
                        'id_fee_item' => $id_fee_item, 
                        'currency' => 'MYR', 
                        'amount' => $amount, 
                        'status' => 1 
                        );

                    // echo "<Pre>";print_r($fee_detail_data);exit();

                    if($id_fee_details > 0)
                    {
                        $inserted_id = $this->fee_structure_model->updateFeeStructureDetail($fee_detail_data,$id_fee_details);
                    }
                    else
                    {
                        $inserted_id = $this->fee_structure_model->addNewFeeStructure($fee_detail_data);
                    }

                    redirect('/finance/feeStructure/addFeeStructure/'.$id_program_landscape);
                }        

                // $data['getProgrammeLandscapeInternational'] = $this->fee_structure_model->getProgrammeLandscapeLocal($id_program_landscape,'USD');
                // $data['getProgrammeLandscapePInternational'] = $this->fee_structure_model->getProgrammeLandscapeLocal($id_program_landscape,'PUSD');

                
                
                $data['intake'] = $this->fee_structure_model->getIntakeById($id_intake);
                $data['programme'] = $this->fee_structure_model->getProgrammeById($id_programme);
                $data['programScheme'] = $this->fee_structure_model->getProgrammeScheme($data['getProgrammeLandscapeLocal']->id_program_scheme);

                $data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeListByStatus('1');
                $data['semesterList'] = $this->fee_structure_model->semesterListByStatus('1');
                $data['feeSetupList'] = $this->fee_setup_model->feeSetupListByStatus('1');
                $data['partnerUniversityList'] = $this->fee_structure_model->partnerUniversityListByStatus('1');
                $data['currencyList'] = $this->fee_structure_model->currencyListByStatus('1');
                $data['getFeeStructureTriggerList'] = $this->fee_structure_model->getFeeStructureTriggerListByStatus('1');
                $data['financialConfiguration'] = $this->fee_structure_model->getFinanceConfiguration();
                $data['tax_percentage'] = $data['financialConfiguration']->tax_sst;


                $data['branchListByProgram'] = $this->fee_structure_model->branchListByProgram($id_programme);


                $data['feeStructureLocalList'] = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_program_landscape,'MYR');
                $data['feeStructureInternationalList'] = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_program_landscape,'USD');

                


                $data['feeStructurePInternationalList'] = $this->fee_structure_model->getFeeStructureByIdProgrammeForTrainingCenter($id_programme,$id_program_landscape,'PUSD');
                // $data['feeStructurePInternationalList'] = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_program_landscape,'PUSD');

                // echo "<Pre>";print_r($data['feeStructureLocalList']);exit();

                $data['feeStructureDetail'] = $this->fee_structure_model->getFeeStructureDetail($id_fee_details);
                $data['id_program_scheme'] = $data['getProgrammeLandscapeLocal']->id_program_scheme;
                $data['id_program_landscape'] = $id_program_landscape;
                $data['id_fee_details'] = $id_fee_details;
                $data['id_programme'] = $id_programme;


                $this->global['pageTitle'] = 'Inventory Management : Add Fee Structure';
                $this->loadViews("fee_structure/add_fee_structure", $this->global, $data, NULL);
            // }
        }
    }

    function copyFeeStructure($id_new_program_landscape,$id_programme)
    {
        if ($this->checkAccess('fee_structure.copy_fee_structure') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo $id_programme;exit();


            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();
                
                $id_program_landscape = $this->security->xss_clean($this->input->post('id_program_landscape'));

                $copied_to_landscape = $this->fee_structure_model->copyFeeStructureByProgramLandscape($id_program_landscape,$id_new_program_landscape);

                redirect('/finance/feeStructure/addFeeStructure/'.$id_new_program_landscape);
            }




            $data['getProgrammeLandscape'] = $this->fee_structure_model->getProgrammeLandscape($id_new_program_landscape);
            $id_intake = $data['getProgrammeLandscape']->id_intake;

            $data['intake'] = $this->fee_structure_model->getIntakeById($id_intake);
            $data['programme'] = $this->fee_structure_model->getProgrammeById($id_programme);

            $data['programmeLandscapeList'] = $this->fee_structure_model->programmeLandscapeByProgrammeId($id_programme);


            $this->global['pageTitle'] = 'Inventory Management : Copy Fee Structure';
            $this->loadViews("fee_structure/copy_fee_structure", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        // $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->fee_structure_model->addNewFeeStructure($tempData);
        echo $inserted_id;exit;
        // unset($tempData['id']);
        // $id_programme = $tempData['id_programme'];
        // $id_intake = $tempData['id_intake'];
        // $id_program_scheme = $tempData['id_program_scheme'];

        // echo "<Pre>";  print_r($tempData);exit;
        // $check_duplication = $this->fee_structure_model->checkDuplicationTrainingCenterFeeStructure($tempData);

        // echo "<Pre>";  print_r($check_duplication);exit;
        // if($check_duplication)
        // {
        //     echo 0;exit;
        // }else
        // {
        // }
        
        // echo "<Pre>";  print_r($tempData);exit;
         // redirect('/finance/feeStructure/add/'.$id_intake.'/'.$id_programme . '/'.$id_program_scheme);
        // header("Refresh:0; url=". $_SERVER['HTTP_REFERER']);
        // echo $_SERVER['HTTP_REFERER'];
        // redirect($_SERVER['HTTP_REFERER']);       
    }

    function displaytempdata($id_programme,$id_intake)
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_programme,$id_intake); 

        if(!empty($temp_details))
        {

        $table = "
        

        <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {


        // echo "<Pre>";print_r($temp_details);exit;
                    $id = $temp_details[$i]->id;
                    $fee_structure = $temp_details[$i]->fee_structure;
                    $frequency_mode = $temp_details[$i]->frequency_mode;
                    $frequency_code = $temp_details[$i]->frequency_code;
                    $frequency = $frequency_code . " - " . $frequency_mode;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j
                            
                            </td>
                            <td>$fee_structure</td>
                            <td>$frequency</td>
                            <td>$amount</td>                           
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Total : </td>
                            <td>$total_amount</td>                           
                            <td>
                            <td>
                        </tr>";
        $table.= "</table>";

        }
        else
        {
            $table.= "";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        // $inserted_id = $this->fee_structure_model->deleteTempData($id);
        $this->fee_structure_model->deleteFeeStructureById($id);
        // $data = $this->displaytempdata();
        // echo $inserted_id; 
    }

     function tempaddExisting()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";  print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->fee_structure_model->addTempDetails($tempData);
        $data = $this->displaytempdata();
        echo $data;        
    }

    function getTrainingCenterFeeStructure()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;


        $fee_structure_data = $this->fee_structure_model->getTrainingCenterFeeStructure($tempData); 
        $temp_details = $this->fee_structure_model->getTrainingCenterInstallmentDetails($tempData);
        $data = $this->displayTrainingUniversityData($temp_details,$fee_structure_data);
        echo $data; 
    }

    function displayTrainingUniversityData($temp_details,$fee_structure_data)
    {
        // echo "<Pre>";print_r($fee_structure_data);exit;

        $table = "";
         if(!empty($fee_structure_data))
        {
            $amount = $fee_structure_data->amount;
            $installments = $fee_structure_data->installments;
            $id_fee_structure = $fee_structure_data->id;
            $id_training_center = $fee_structure_data->id_training_center;
            $id_program_landscape = $fee_structure_data->id_program_landscape;
            $currency = $fee_structure_data->currency_code . " - " . $fee_structure_data->currency;

            $table .= "
        <input type='hidden' id='installment_amount_selected' name='installment_amount_selected' value='$amount'>
        <input type='hidden' id='installment_nos' name='installment_nos' value='$installments'>
        <input type='hidden' id='id_fee_structure' name='id_fee_structure' value='$id_fee_structure'>
        <input type='hidden' id='id_data_training_center' name='id_data_training_center' value='$id_training_center'>
        <input type='hidden' id='id_data_program_landscape' name='id_data_program_landscape' value='$id_program_landscape'>
        <input type='hidden' id='data_currency' name='data_currency' value='$currency'>
        ";
        }

        if(!empty($temp_details))
        {
            

        $table .= "
        <div class='form-container'>
         <h4 class='form-group-title'>Installment List</h4>

        <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Installment Trigger On</th>
                    <th>Fee Item</th>
                    <th>Trgger Point</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
        // echo "<Pre>";print_r($temp_details);exit;
                    $id = $temp_details[$i]->id;
                    $fee_name = $temp_details[$i]->fee_name;
                    $fee_code = $temp_details[$i]->fee_code;
                    $frequency_mode = $temp_details[$i]->frequency_mode;
                    $id_semester = $temp_details[$i]->id_semester;
                    $fee = $fee_code . " - " . $fee_name;
                    $trigger_name = $temp_details[$i]->trigger_name;
                    $amount = $temp_details[$i]->amount;

                    $j = $i+1;

                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$id_semester</td>
                            <td>$fee</td>
                            <td>$trigger_name</td>
                            <td>$frequency_mode</td>
                            <td>$amount</td>
                            <td>
                                <a onclick='deleteSemesterDataByIdFeeStructureTrainingCenter($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td colspan='4'></td>
                            <td>Total : </td>
                            <td>$total_amount</td>                           
                            <td>
                            <td>
                        </tr>";
        $table.= "</table>
        </div>
        ";


         }
        else
        {
            $table .= "";
        }
        return $table;
    }

    function deleteTrainingCenterByTrainingCenterId($id)
    {
        $this->fee_structure_model->deleteFeeStructure($id);
        $this->fee_structure_model->deleteFeeStructureInstallmentByIdFeeStructure($id);

    }


    function saveInstallmentData()
    {

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->fee_structure_model->saveInstallmentData($tempData);
        echo $inserted_id;exit;
    }



    function deleteSemesterDataByIdFeeStructureTrainingCenter($id_fee_training_center)
    {   
        $this->fee_structure_model->deleteSemesterDataByIdFeeStructureTrainingCenter($id_fee_training_center);
    }








    function getProgrammeByEducationLevelId($id_education_level)
    {
            // echo "<Pre>"; print_r($id_education_level);exit;
            $intake_data = $this->fee_structure_model->getProgrammeByEducationLevelId($id_education_level);

             $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_programme' id='id_programme' class='form-control' onchange='getIntakes()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }

    function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->fee_structure_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }


    function getIntakesTo()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->fee_structure_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake_to' id='id_intake_to' class='form-control'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getLearningModeByProgramId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_program = $tempData['id_program'];

        $intake_data = $this->fee_structure_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_learning_mode' id='id_learning_mode' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;


            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";



            // $table.="<option value='".$id . "'";
            // if($id_learning_mode == $id)
            // {
            //    $table.="selected";
            // } $table.= ">". $mode_of_program . " - " .  $mode_of_study .
            //         "</option>";

            }
            $table.="</select>";

            echo $table;  
    }


    function getSchemeByProgramId($id_program)
    {
       $intake_data = $this->fee_structure_model->getSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_program_has_scheme' id='id_program_has_scheme' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $code = $intake_data[$i]->code;
        $name = $intake_data[$i]->description;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }

    function updateFeeStructureMasterAmount()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        $inserted_id = $this->fee_structure_model->editFeeStructureMaster($tempData,$id);
        echo $inserted_id;
    }

}