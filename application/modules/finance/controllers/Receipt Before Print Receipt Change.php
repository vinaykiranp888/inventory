<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Receipt extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('receipt_model');
        $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('receipt.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['receiptList'] = $this->receipt_model->getReceiptListBySearch($formData);

            $data['programmeList'] = $this->main_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_model->intakeListByStatus('1');
            $data['applicantList'] = $this->main_invoice_model->applicantList();
            $data['studentList'] = $this->main_invoice_model->studentList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Receipt List';
            $this->loadViews("receipt/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        $user_id = $this->session->userId;
        if ($this->checkAccess('receipt.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            

            if($this->input->post())
            {

                $formData = $this->input->post();

                $receipt_amount = $this->security->xss_clean($this->input->post('receipt_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_program = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_sponser = $this->security->xss_clean($this->input->post('id_sponser'));
                $receipt_date = $this->security->xss_clean($this->input->post('receipt_date'));

                

                $receipt_number = $this->receipt_model->generateReceiptNumber();

                $data = array(
                    'receipt_date' =>date('Y-m-d',strtotime($receipt_date)),
                    'id_student' => $id_student,
                    'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'id_sponser' => $id_sponser,
                    'type' => $type,
                    'receipt_number' => $receipt_number,
                    'receipt_amount' => $receipt_amount,
                    'remarks' => $remarks,
                    'status' => '0'
                );
                // echo "<Pre>";print_r($data);exit;
                $inserted_id = $this->receipt_model->addNewReceipt($data);


                // $id_main_invoice = $this->security->xss_clean($this->input->post('id_main_invoice'));
                // $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
                // $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));

                $temp_details = $this->receipt_model->getTempReceiptDetails($id_session);

                              

                 for($i=0;$i<count($temp_details);$i++)
                 {
                    $id_payment_mode = $temp_details[$i]->id_payment_mode;
                    $payment_reference_number = $temp_details[$i]->payment_reference_number;
                    $payment_mode_amount = $temp_details[$i]->payment_mode_amount;

                     $detailsData = array(
                        'id_receipt' => $inserted_id,
                        'id_payment_type' => $id_payment_mode,
                        'paid_amount' => $payment_mode_amount,
                        'payment_reference_number' => $payment_reference_number,                        
                        'status' => '1',
                        'created_by' => $user_id
                    );
                    // print_r($details);exit;
                    $result = $this->receipt_model->addNewReceiptPaymentDetails($detailsData);
                 }



                 for($i=0;$i<count($formData['invoice_id']);$i++)
                 {
                    $invoice_id = $formData['invoice_id'][$i];
                    $payable_amount = $formData['payable_amount'][$i];

                    $updated_main_invoice = $this->receipt_model->updateMainInvoiceAmount($invoice_id,$payable_amount);

                    if ($updated_main_invoice)
                    {
                        // echo "<Pre>";print_r($updated_main_invoice);exit();
                        if($payable_amount>0)
                        {
                            $detailsData = array(
                            'id_receipt' => $inserted_id,
                            'id_main_invoice' => $invoice_id,
                            'invoice_amount' => '0',
                            'paid_amount' => $payable_amount,                        
                            'status' => '1',
                            'created_by' => $user_id
                            );
                            //print_r($details);exit;
                            $result = $this->receipt_model->addNewReceiptDetails($detailsData);
                        }
                    }
                 }
                // $this->receipt_model->deleteTempDataBySession($id_session);
                 
                $this->receipt_model->deleteTempAmountDataBySession($id_session);
                redirect('/finance/receipt/list');
            }
            else
            {
                $this->receipt_model->deleteTempAmountDataBySession($id_session);
            }
            $data['status'] = '1';
            $data['nric'] = '';
            $data['type'] = '';
            $data['invoice_number'] = '';
            $data['id_programme'] = '';
            $data['id_intake'] = '';
            $data['name'] = '';

            $data['receiptList'] = $this->main_invoice_model->getMainInvoiceListByStatus($data);
            $data['programmeList'] = $this->main_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_model->intakeList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Receipt';
            $this->loadViews("receipt/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('receipt.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/receipt/list');
            }
            if($this->input->post())
            {
                redirect('/finance/receipt/list');
            }
            // $data['studentList'] = $this->receipt_model->studentList();
            $data['receipt'] = $this->receipt_model->getReceipt($id);
            if($data['receipt']->type == 'Applicant')
            {
                $data['receiptFor'] = $this->main_invoice_model->getMainInvoiceApplicantData($data['receipt']->id_student);
            }elseif($data['receipt']->type == 'Student')
            {
                $data['receiptFor'] = $this->main_invoice_model->getMainInvoiceStudentData($data['receipt']->id_student);
            }elseif($data['receipt']->type == 'Sponsor')
            {
                $data['receiptFor'] = $this->main_invoice_model->getMainInvoiceSponserData($data['receipt']->id_sponser);
                $data['studentDetails'] = $this->main_invoice_model->getStudentByStudent($data['receipt']->id_student);
            }
            $data['invoiceDetails'] = $this->receipt_model->getReceiptInvoiceDetails($id);
            $data['paymentDetails'] = $this->receipt_model->getReceiptPaymentDetails($id);
            $data['degreeTypeList'] = $this->main_invoice_model->qualificationList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');
            // echo "<pre>";print_R($data['receiptFor']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Receipt';
            $this->loadViews("receipt/edit", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('receipt.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/receipt/approvalList');
            }
            if($this->input->post())
            {
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                $data = array(
                    'status' => $status,
                    'reason' => $reason
                );

                $result = $this->receipt_model->editReceipt($data,$id);

                redirect('/finance/receipt/approvalList');
            }

            $data['receipt'] = $this->receipt_model->getReceipt($id);
            if($data['receipt']->type == 'Applicant')
            {
                $data['receiptFor'] = $this->main_invoice_model->getMainInvoiceApplicantData($data['receipt']->id_student);
            }elseif($data['receipt']->type == 'Student')
            {
                $data['receiptFor'] = $this->main_invoice_model->getMainInvoiceStudentData($data['receipt']->id_student);
            }elseif($data['receipt']->type == 'Sponser')
            {
                $data['receiptFor'] = $this->main_invoice_model->getMainInvoiceSponserData($data['receipt']->id_sponser);
                $data['studentDetails'] = $this->main_invoice_model->getStudentByStudent($data['receipt']->id_student);
            }
            // echo "<pre>";print_R($data['receiptFor']);exit;
            $data['invoiceDetails'] = $this->receipt_model->getReceiptInvoiceDetails($id);
            $data['paymentDetails'] = $this->receipt_model->getReceiptPaymentDetails($id);
            $data['degreeTypeList'] = $this->main_invoice_model->qualificationList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');
            // echo "<Pre>"; print_r($data['receipt']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Receipt';
            $this->loadViews("receipt/view", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('pr_entry_approval.list') == 0)
        // if ($this->checkAccess('receipt.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

           //  $resultprint = $this->input->post();

           // if($resultprint)
           //  {
           //   switch ($resultprint['button'])
           //   {
           //      case 'approve':

           //           for($i=0;$i<count($resultprint['checkvalue']);$i++)
           //              {

           //               $id = $resultprint['checkvalue'][$i];
           //               $result = $this->receipt_model->editReceiptList($id);
           //              }
           //              redirect($_SERVER['HTTP_REFERER']);
           //           break;


           //      case 'search':

           //          $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
           //          $formData['name'] = $this->security->xss_clean($this->input->post('name'));
           //          $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
           //          $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
           //          $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
           //          $formData['status'] = '0';
         
           //          $data['searchParam'] = $formData;
           //          // echo "<Pre>";print_r("ss");exit();
           //          $data['receiptList'] = $this->receipt_model->getReceiptListBySearch($formData);
                     
           //           break;
                 
           //      default:
           //           break;
           //   }
                
           //  }
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['receipt_number'] = $this->security->xss_clean($this->input->post('receipt_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['receiptList'] = $this->receipt_model->getReceiptListBySearch($formData);

            $data['programmeList'] = $this->main_invoice_model->programmeListByStatus('1');
            $data['intakeList'] = $this->main_invoice_model->intakeListByStatus('1');
            $data['applicantList'] = $this->main_invoice_model->applicantList();
            $data['studentList'] = $this->main_invoice_model->studentList();
            $data['sponserList'] = $this->receipt_model->sponserListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Approve Receipt';
            $this->loadViews("receipt/approval_list", $this->global, $data, NULL);
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        unset($tempData['id']);
        $inserted_id = $this->receipt_model->addTempDetails($tempData);
        
        $data = $this->displaytempdata();
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->receipt_model->getTempReceiptDetails($id_session); 

        if(!empty($temp_details))
        {

         // echo "<Pre>";print_r($temp_details);exit;
        
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Payment Mode</th>
                    <th>Amount </th>
                    <th>Reference Number</th>
                    <th>Action</th>
                </tr>";
                $invoice_total_amount = 0;
                $invoice_paid_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $id_payment_mode = $temp_details[$i]->id_payment_mode;
                    $payment_mode_amount = $temp_details[$i]->payment_mode_amount;
                    $payment_reference_number = $temp_details[$i]->payment_reference_number;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$id_payment_mode</td>
                            <td>$payment_mode_amount</td>
                            <td>$payment_reference_number</td>                            
                            <td>
                                <a onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $invoice_total_amount = $invoice_total_amount + $payment_mode_amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td style='text-align: right'>Total : </td>
                            <td><input type='hidden' id='invoice_total_amount' value='$invoice_total_amount'/>$invoice_total_amount</td>
                                               
                            <td></td>
                        </tr>
                        </table>";

            }
            else
            {
                $table = '';
            }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->receipt_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function tempDetailsDataAdd()
    {
        //echo "<Pre>";  print_r("adaf");exit;
        $id_session = $this->session->my_session_id;
        $id_main_invoice = $this->security->xss_clean($this->input->post('id_main_invoice'));
        $invoice_amount = $this->security->xss_clean($this->input->post('invoice_amount'));
        $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));

        $data = array(
                'id_session' => $id_session,
                'id_main_invoice' => $id_main_invoice,
                'invoice_amount' => $invoice_amount,
                'paid_amount' => $paid_amount
            );
        $inserted_id = $this->receipt_model->addNewTempReceiptDetails($data);
        // echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'id_main_invoice' => $id_main_invoice,
                'invoice_amount' => $invoice_amount,
                'paid_amount' => $paid_amount
            );
        $temp_details = $this->receipt_model->getTempReceiptDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Main Invoice</th>
                    <th>Invoice Amount</th>
                    <th>Paid Amount</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $main_invoice = $temp_details[$i]->main_invoice;
                    $invoice_amount = $temp_details[$i]->invoice_amount;
                    $paid_amount = $temp_details[$i]->paid_amount;

                    $table .= "
                <tr>
                    <td>$main_invoice</td>
                    <td>$invoice_amount</td>
                    <td>$paid_amount</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

     function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->receipt_model->getStudentByProgrammeId($id);
            $programme_data = $this->receipt_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;


            

            $table="

            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $nric = $results[$i]->nric;
            $table.="<option value=".$id.">".$nric . " - " .$full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            // $d['table'] = $table;
            // $d['view'] = $view;

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->receipt_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";




            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Programme</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";

            $invoice_data = $this->receipt_model->getInvoicesByStudentId($id);
            // echo "<Pre>";print_r($invoice_data);exit;

            if(!$invoice_data)
            {
                $table .= "
                <br>
                <br>
                <div class='custom-table'>
                    <table align='center' class='table' id='list-table'>
                      <tr>
                        <h3 style='text-align: center;'>No Balance Invoices Available For This Student</h3>
                    </tr>
                    </table>
                </div>
                <br>
                <br>";
                // echo "<Pre>";print_r("No Data");exit;
            }
            else
            {
                $table .= "
                <h3>Receipt Details</h3>
                <div class='custom-table'>
                    <table class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Invoice Number</th>
                        <th>Currency</th>
                        <th>Total Amount</th>
                        <th>Balance Amount</th>
                        <th>Payable Amount</th>
                    </tr>";


                for($i=0;$i<count($invoice_data);$i++)
                    {
                        $id = $invoice_data[$i]->id;
                        $invoice_number = $invoice_data[$i]->invoice_number;
                        $total_amount = $invoice_data[$i]->total_amount;
                        $balance_amount = $invoice_data[$i]->balance_amount;
                        $paid_amount = $invoice_data[$i]->paid_amount;
                        $currency = $invoice_data[$i]->currency;
                        $j=$i+1;

                        $table .= "
                    <tr>
                        <td>$j
                        <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
                        <td>$invoice_number</td>
                        <td>$currency</td>
                        <td>$total_amount</td>
                        <td>$balance_amount</td>
                        <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
                        </div>
                        </td>
                    </tr>";
                    }
                            
                $table .= "
                </table>";
            }


            echo $table;
            exit;
    }


    function payableAmountAdd()
    {
        $id_session = $this->session->my_session_id;
        $tempAmount = $this->security->xss_clean($this->input->post('tempAmount'));
        $tempAmount['id_session'] = $id_session;



        $result = $this->receipt_model->insertAmountToTempData($tempAmount);
        $amount_data = $this->receipt_model->getAmountFromTempTableReceiptBySession($id_session);
        $total_amount = 0;


        foreach ($amount_data as $value)
        {
            $total_amount = $total_amount + $value->payable_amount;
        }

        print_r($total_amount);exit();

       //  $table = "
       //  <table border='1px' style='width: 100%'>
       //  <tr>
       //      <td>
       //          <input type='hidden' name='total_amount_payable' id='total_amount_payable' value='$total_amount'/>
       //          $total_amount
       //      </td>
       //  </tr>
       // </table>";
       // echo $total_amount;
       // exit;
    }


    function getStudentByProgram()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $type = $formData['type'];
            switch ($type)
            {
                case 'Applicant':

                    $table = $this->getApplicantList($formData);

                    break;

                case 'Student':

                    $table = $this->getStudentList($formData);
                    
                    break;


                default:
                    # code...
                    break;
            }

            echo $table;
            exit;            
    }

    function getStudentList($data)
    {
        $data = $this->main_invoice_model->getStudentListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantList($data)
    {
        $data = $this->main_invoice_model->getApplicantListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Select Applicant <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getApplicantByApplicantId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getApplicantByApplicantId($id)
    {
         // print_r($id);exit;
            $student_data = $this->receipt_model->getApplicantByApplicantId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;


            $table  = "

             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Applicant Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Applicant NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";




            // $table1  = "
            // <table border='1px' style='width: 100%'>
            //     <tr>
            //         <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Applicant Name</th>
            //         <td style='text-align: center;'>$student_name</td>
            //         <th style='text-align: center;'>Intake</th>
            //         <td style='text-align: center;'>$intake_name</td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Applicant Email</th>
            //         <td style='text-align: center;'>$email</td>
            //         <th style='text-align: center;'>Programme</th>
            //         <td style='text-align: center;'>$programme_name</td>
            //     </tr>
            //     <tr>
            //         <th style='text-align: center;'>Applicant NRIC</th>
            //         <td style='text-align: center;'>$nric</td>
            //         <th style='text-align: center;'></th>
            //         <td style='text-align: center;'></td>
            //     </tr>

            // </table>
            // <br>
            // <br>
            // ";

            $invoice_data = $this->receipt_model->getInvoicesByApplicantId($id);
            // echo "<Pre>";print_r($invoice_data);exit;

            if(!$invoice_data)
            {
                $table .= "
                <br>
                <br>
                <div class='custom-table'>
                    <table align='center' class='table' id='list-table'>
                      <tr>
                        <h3 style='text-align: center;'>No Balance Invoices Available For This Applicant</h3>
                    </tr>
                    </table>
                </div>
                <br>
                <br>";
                // echo "<Pre>";print_r("No Data");exit;
            }
            else
            {
                $table .= "
                <h3>Receipt Details</h3>
                <div class='custom-table'>
                    <table class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Invoice Number</th>
                        <th>Currency</th>
                        <th>Total Amount</th>
                        <th>Balance Amount</th>
                        <th>Payable Amount</th>
                    </tr>";


                for($i=0;$i<count($invoice_data);$i++)
                    {
                        $id = $invoice_data[$i]->id;
                        $invoice_number = $invoice_data[$i]->invoice_number;
                        $total_amount = $invoice_data[$i]->total_amount;
                        $balance_amount = $invoice_data[$i]->balance_amount;
                        $paid_amount = $invoice_data[$i]->paid_amount;
                        $currency = $invoice_data[$i]->currency;
                        $j=$i+1;
                        $table .= "
                    <tr>
                        <td>$j
                        <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
                        <td>$invoice_number</td>
                        <td>$currency</td>
                        <td>$total_amount</td>
                        <td>$balance_amount</td>
                        <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
                        </div>
                        </td>
                    </tr>";
                    }
                            
                $table .= "
                </table>";
            }


            echo $table;
            exit;
    }


    function getStudentBySponser()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $id_sponser = $formData['id_sponser'];
        $table = $this->getStudentBySponserId($id_sponser);
        echo $table;
        exit;   
    }

    function getStudentBySponserId($id_sponser)
    {
        $data = $this->receipt_model->getStudentBySponser($id_sponser);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentIdNSponser()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentByStudentIdNSponser()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));

            // echo "<Pre>"; print_r($formData);exit;

        $id = $formData['id_student'];
        $id_sponser = $formData['id_sponser'];
        $type = $formData['type'];

        
            $student_data = $this->receipt_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $programme_name = $student_data->programme_name;


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    $intake_name
                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";




            $table1  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Student Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Name</th>
                    <td style='text-align: center;'>$student_name</td>
                    <th style='text-align: center;'>Intake</th>
                    <td style='text-align: center;'>$intake_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student Email</th>
                    <td style='text-align: center;'>$email</td>
                    <th style='text-align: center;'>Programme</th>
                    <td style='text-align: center;'>$programme_name</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Student NRIC</th>
                    <td style='text-align: center;'>$nric</td>
                    <th style='text-align: center;'></th>
                    <td style='text-align: center;'></td>
                </tr>

            </table>
            <br>
            <br>
            ";

            $invoice_data = $this->receipt_model->getInvoicesByStudentIdNSponser($id);
            // echo "<Pre>";print_r($invoice_data);exit;

            if(!$invoice_data)
            {
                $table .= "
                <br>
                <br>
                <div class='custom-table'>
                    <table align='center' class='table' id='list-table'>
                      <tr>
                        <h3 style='text-align: center;'>No Balance Invoices Available For This Student</h3>
                    </tr>
                    </table>
                </div>
                <br>
                <br>";
                // echo "<Pre>";print_r("No Data");exit;
            }
            else
            {
                $table .= "
                <h3>Receipt Details</h3>
                <div class='custom-table'>
                    <table class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Invoice Number</th>
                        <th>Total Amount</th>
                        <th>Balance Amount</th>
                        <th>Payable Amount</th>
                    </tr>";


                for($i=0;$i<count($invoice_data);$i++)
                    {
                        $id = $invoice_data[$i]->id;
                        $invoice_number = $invoice_data[$i]->invoice_number;
                        $total_amount = $invoice_data[$i]->total_amount;
                        $balance_amount = $invoice_data[$i]->balance_amount;
                        $paid_amount = $invoice_data[$i]->paid_amount;
                        $j=$i+1;
                        $table .= "
                    <tr>
                        <td>$j
                        <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
                        <td>$invoice_number</td>
                        <td>$total_amount</td>
                        <td>$balance_amount</td>
                        <td style='text-align: center;'>
                        <div class='form-group'>
                            <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
                        </div>
                        </td>
                    </tr>";
                    }
                            
                $table .= "
                </table>";
            }


            echo $table;
            exit;
    }

    function generateReceipt($id_receipt)
    {

        $this->getMpdfLibrary();


           // echo "<pre>"; print_r($receiptFor);exit;

            // include("/home/camsedu/public_html/assets/mpdf/vendor/autoload.php");
            //  require_once __DIR__ . '/vendor/autoload.php';
            
            $mpdf=new \Mpdf\Mpdf(); 

            // $mpdf->SetHeader("<div style='text-align: left;'>Inventory Management
            //                    </div>");

        $organisationDetails = $this->receipt_model->getOrganisation();

        // echo "<Pre>";print_r($organisationDetails);exit;


        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }

            $currentDate = date('d-m-Y');
            $currentTime = date('h:i:s a');

        

         $receipt = $this->receipt_model->getReceipt($id_receipt);

        if($receipt->type == 'Applicant')
        {
            $receiptFor = $this->main_invoice_model->getMainInvoiceApplicantData($receipt->id_student);
        }elseif($receipt->type == 'Student')
        {
            $receiptFor = $this->main_invoice_model->getMainInvoiceStudentData($receipt->id_student);
        }elseif($receipt->type == 'Sponser')
        {
            $receiptFor = $this->main_invoice_model->getMainInvoiceSponserData($receipt->id_sponser);
            $studentDetails = $this->main_invoice_model->getStudentByStudent($receipt->id_student);
        }
        // echo "<pre>";print_R($data['receiptFor']);exit;
        

        // echo "<Pre>";print_r($receiptFor);exit;


        $type = $receipt->type;
        $receipt_number = $receipt->receipt_number;
        $receipt_date = $receipt->receipt_date;
        $remarks = $receipt->remarks;
        $receipt_amount = $receipt->receipt_amount;
        $programme_name = $receipt->programme_name;
        $programme_code = $receipt->programme_code;
        $intake_name = $receipt->intake_name;
        $intake_year = $receipt->intake_year;


        if($receipt_date)
        {
            $receipt_date = date('d-m-Y', strtotime($receipt_date));
        }


        $receipt_generation_name = $invoiceFor->full_name;
        $receipt_generation_nric = $invoiceFor->nric;



            $file_data = "";


            $file_data = $file_data ."
    <table align='center' width='100%'>
        <tr>
          <td style='text-align: left' width='20%' ><b>RECEIPT</b></td>
          <td style='text-align: center' width='30%' ></td>
          <td style='text-align: right' width='40%' ><img src='$signature' width='120px' /></td>
          
        </tr>
       
        
        <tr>
          <td style='text-align: center' width='100%'  colspan='3'></td>
        </tr>
    </table>


    <table width='100%'>
        <tr>
             <td>Student Name : $receipt_generation_name </td>
             <td></td>
             <td>Receipt No: $receipt_number</td>
             <td></td>
        </tr>
        <tr>
             <td>Matric No:  </td>
             <td></td>
             <td>Invoice Date: $receipt_date</td>
             <td></td>
        </tr>
    </table>

  <br>

<table align='center' width='100%' border='1' style='border-collapse: collapse;'>
  <tr>
    <th style='text-align:center;' width='15%'>Invoice Number</th>
    <th style='text-align:center;' width='25%'>Remarks</th>
    <th style='text-align:center;' width='12%'>Currency</th>
    <th style='text-align:center;' width='12%'>Invoice Total</th>
    <th style='text-align:center;' width='12%'>Paid Amount</th>
    <th style='text-align:center;' width='12%'>Invoice Payable</th>
    <th style='text-align:center;' width='12%'>Balance</th>
  </tr>";
  $i = 0;


  
    $invoiceDetails = $this->receipt_model->getReceiptInvoiceDetails($id_receipt);
    
    // echo "<pre>"; print_r($invoiceDetails);exit;
    

        $total_receipt_detail = 0;
      foreach ($invoiceDetails as $value)
      {

        $remarks = $value->remarks;
        $invoice_number = $value->invoice_number;
        $invoice_total = $value->invoice_total;
        $total_amount = $value->total_amount;
        $paid_amount = $value->paid_amount;
        $balance_amount = $value->balance_amount;
        $total_discount = $value->total_discount;
        $balance_amount = $value->balance_amount;
        $currency = $value->currency;

        // $amount = number_format($amount, 2, '.', ',');

            // $acqDate   = date("d/m/Y", strtotime($acqDate));

        

       $file_data = $file_data ."
      <tr>
        <td style='text-align:left;' width='15%'><font size='3'>$invoice_number</font></td>
        <td style='text-align:left;' width='25%'><font size='3'>$remarks</font></td>
        <td style='text-align:left;' width='12%'><font size='3'>$currency</font></td>
        <td style='text-align:left;' width='12%'><font size='3'>$invoice_total</font></td>
        <td style='text-align:left;' width='12%'><font size='3'>$paid_amount</font></td>
        <td style='text-align:left;' width='12%'><font size='3'>$total_amount</font></td>
        <td style='text-align:left;' width='12%'><font size='3'>$balance_amount</font></td>
      </tr>";

      $total_receipt_detail = $total_receipt_detail + $paid_amount;
      $i++;

      }

    $amount_word = $this->getAmountWordings($receipt_amount);

    $amount_word = ucwords($amount_word);

    $receipt_amount = number_format($receipt_amount, 2, '.', ',');


  
   $file_data = $file_data ."
  <tr>
    <td style='text-align:left;' width='15%'><font size='3'></font></td>
    <td style='text-align:left;' width='25%'><font size='3'></font></td>
    <td style='text-align:left;' width='12%'><font size='3'></font></td>
    <td style='text-align:left;' width='12%'><font size='3'><b>Total</b></font></td>
    <td style='text-align:left;' width='12%'><font size='3'>$receipt_amount</font></td>
    <td style='text-align:left;' width='12%'><font size='3'></font></td>
    <td style='text-align:left;' width='12%'><font size='3'></font></td>
  </tr>
  <tr>
    <td style='text-align:center;' width='100%' colspan='7'><font size='3'><b>$currency : $amount_word</b></font></td>
  </tr>
  </table>";

  $paymentDetails = $this->receipt_model->getReceiptPaymentDetails($id_receipt);



  $file_data = $file_data ."
  <br>
  <h4> Payment Details</h4>
  <table align='center' width='100%' border='1' style='border-collapse: collapse;'>
    <tr>
        <th style='text-align:center;' width='30%'>Payment Type</th>
        <th style='text-align:center;' width='40%'>Reference Number</th>
        <th style='text-align:center;' width='30%'>Amount</th>
  </tr>
  ";



   $total_receipt_detail = 0;
      foreach ($paymentDetails as $payment)
      {

        $id_payment_type = $payment->id_payment_type;
        $payment_reference_number = $payment->payment_reference_number;
        $paid_amount = $payment->paid_amount;

        

       $file_data = $file_data ."
      <tr>
        <td style='text-align:left;' width='30%'><font size='3'>$id_payment_type</font></td>
        <td style='text-align:left;' width='40%'><font size='3'>$payment_reference_number</font></td>
        <td style='text-align:center;' width='30%'><font size='3'>$paid_amount</font></td>
      </tr>";

      }


      $file_data = $file_data ."
</table>";
    
    // echo "<pre>"; print_r($paymentDetails);exit;


  $bankDetails = $this->receipt_model->getBankRegistration();

        if($bankDetails)
        {
            $bank_name = $bankDetails->name;
            $bank_code = $bankDetails->code;
            $account_no = $bankDetails->account_no;
            $state = $bankDetails->state;
            $country = $bankDetails->country;
            $address = $bankDetails->address;
            $city = $bankDetails->city;
            $zipcode = $bankDetails->zipcode;

             $file_data = $file_data ."
        <p>1. All Receipts are Checked and Paid to : </p>
    <table align='center' width='100%'>
      <tr>
            <td style='text-align: left' width='30%'>PAYEE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>AGILE DIGITAL TECHNOLOGIES</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>BANK</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_name</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ADDRESS</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
      </tr>

      <tr>
            <td style='text-align: left' width='30%'>ACCOUNT NO</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$account_no</td>
      </tr>
      <tr>
            <td style='text-align: left' width='30%'>SWIFT CODE</td>
            <td style='text-align: center' width='5%'>:</td>
            <td style='text-align: left' width='65%'>$bank_code</td>
      </tr>

      
    </table>
    <p> 2. This is auto generated Receipt. No signature is required : </p>


    <p> 2. Issued by:<br>Finance & Accounts Department : </p>




    
      ";


        }

  


    // echo "<pre>"; print_r($file_data);exit;

    
  $file_data .="
<pagebreak>";



            // $mpdf->SetFooter('<div>Inventory Management</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);

            $mpdf->Output($type . '_RECEIPT_'.$receipt_number.'.pdf', 'D');
            exit;
    }
}
