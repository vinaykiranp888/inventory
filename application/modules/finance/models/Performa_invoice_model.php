<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Performa_invoice_model extends CI_Model
{
    function performaInvoiceList()
    {
        $this->db->select('pi.*, s.full_name as student');
        $this->db->from('performa_invoice as pi');
        $this->db->join('student as s', 'pi.id_student = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPerformaInvoice($id)
    {
        $this->db->select('pi.*, s.full_name as student');
        $this->db->from('performa_invoice as pi');
        $this->db->join('student as s', 'pi.id_student = s.id');
        $this->db->where('pi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPerformaInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('performa_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

     function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_performa_invoice_details');
    }

    function addNewPerformaInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('performa_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempPerformaInvoice($id_session)
    {
        $this->db->select('*');
        $this->db->from('temp_performa_invoice_details as tempinvoice');
        $this->db->join('fee_setup as s', 'tempinvoice.id_fee_item = s.id');        
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }


    function addNewTempPerformaInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_performa_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
         // print_r($insert_id);exit();     
        return $insert_id;
    }

     function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_performa_invoice_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_performa_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_performa_invoice_details', $data);
        return TRUE;
    }

    function editPerformaInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('performa_invoice', $data);
        return TRUE;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function applicantList()
    {
        $this->db->select('*');
        $this->db->from('applicant');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}

