<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Statement_of_account_model extends CI_Model
{
    function mainInvoiceList()
    {
        $this->db->select('mi.*, s.full_name as student, s.email_id, s.nric');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getMainInvoiceListByStatus($data)
    {
        $this->db->select('DISTINCT(s.id) as id, s.*, p.name as programme_name, p.code as programme_code, i.name as intake_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['invoice_number'] != '')
        // {
        //     $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if ($data['id_programme'] != '')
        {
            $this->db->where('s.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('s.id_intake', $data['id_intake']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        
        // $this->db->order_by("mi.id", "ASC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editMainInvoiceList($array)
    {
        $status = ['status'=>'1'];
      $this->db->where_in('id', $array);
      $this->db->update('main_invoice', $status);
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, s.full_name as student');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('fee_structure as fs', 'mid.id_fee_item = fs.id');        
        $this->db->join('fee_setup as fstp', 'fs.id_fee_item = fstp.id');        
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function studentDetails($id)
    {
        $this->db->select('s.*, i.name as intakeName, p.name as programName');
        $this->db->from('student as s');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->where('s.id', $id);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result); exit();
         return $result;
    }

    function addNewTempMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getTempMainInvoiceDetails($id_session)
    {
        $this->db->select('tmid.*, fs.name as fee_setup');
        $this->db->from('temp_main_invoice_details as tmid');
        $this->db->join('fee_setup as fs', 'tmid.id_fee_item = fs.id');        
        $this->db->where('tmid.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

     function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_main_invoice_details');
    }

     function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_main_invoice_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_main_invoice_details', $data);
        return TRUE;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, adv.ic_no, adv.name as advisor_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('staff as adv', 's.id_advisor = adv.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    // function getInvoiceByStudentId($id_student)
    // {
    //     $this->db->select('mi.*, s.full_name as student, s.email_id, s.nric');
    //     $this->db->from('main_invoice as mi');
    //     $this->db->join('student as s', 'mi.id_student = s.id');
    //     $this->db->where('mi.id_student', $id_student);
    //     $query = $this->db->get();
    //      $result = $query->result();

    //     return$result;
    // }

    // function getReceiptByStudentId($id_student)
    // {
    //     $this->db->select('r.*, s.full_name as student, s.email_id, s.nric');
    //     $this->db->from('receipt as r');
    //     $this->db->join('student as s', 'r.id_student = s.id');
    //     $this->db->where('r.id_student', $id_student);
    //     $query = $this->db->get();
    //      $result = $query->result();

    //     return$result;
    // }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


    function getInvoiceByStudentId($id_student,$id_applicant)
    {

        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getReceiptByStudentId($id_student,$id_applicant)
    {
        $student = "Student";
        $applicant = "Applicant";

        $this->db->select('mi.*, cs.name as currency');
        $this->db->from('receipt as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $likeCriteria = "(id_student  = '" . $id_student . "' and type  ='" . $student . "') or (id_student  ='" . $id_applicant . "' and type  ='" . $applicant . "')";
        $this->db->where($likeCriteria);


        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getStatementOfAccountByStudentId($id_student,$id_applicant)
    {

        $invoices = $this->getInvoiceByStudentId($id_student,$id_applicant);
        $details = array();
        foreach ($invoices as $invoice)
        {
            // echo "<Pre>";print_r($invoice);exit();

            $invoice->transaction_type = 'Invoice';
            $invoice->reference_number = $invoice->invoice_number;
            $invoice->invoice_total = $invoice->invoice_total;
            $invoice->total_payable = $invoice->total_amount;
            $invoice->paid_amount = $invoice->paid_amount;
            $invoice->balance_amount = $invoice->balance_amount;
            
            // echo "<Pre>";print_r($invoice);exit();
            array_push($details, $invoice);
        }
        $receipts = $this->getReceiptByStudentId($id_student,$id_applicant);
        foreach ($receipts as $receipt)
        {
            $receipt->transaction_type = 'Payment';
            $receipt->reference_number = $receipt->receipt_number;
            $receipt->invoice_total = $receipt->receipt_amount;
            $receipt->total_payable = $receipt->receipt_amount;
            $receipt->paid_amount = $receipt->receipt_amount;
            $receipt->balance_amount = 0;
            array_push($details, $receipt);
        }

        return $details;
    }

    function getCreditNoteByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as credit_code, s.name as credit_name, inv.invoice_number');
        $this->db->from('credit_note as mi');
        $this->db->join('credit_note_type as s', 'mi.id_type = s.id');
        $this->db->join('main_invoice as inv', 'mi.id_invoice = inv.id');
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }

    function getDebitNoteByStudentId($id_student)
    {
        $this->db->select('mi.*, s.code as debit_code, s.name as debit_name, inv.invoice_number');
        $this->db->from('debit_note as mi');
        $this->db->join('credit_note_type as s', 'mi.id_type = s.id');
        $this->db->join('main_invoice as inv', 'mi.id_invoice = inv.id');
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
         $result = $query->result();

        return$result;
    }


}

