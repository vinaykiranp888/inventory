<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Debit_note_model extends CI_Model
{

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function sponserListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("year", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function debitNoteTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('credit_note_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function applicantList()
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('is_submitted', 1);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function studentList()
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }



    function debitNoteListSearch($data)
    {

        $this->db->select('mi.*, p.code as program_code, p.name as program_name');
        $this->db->from('debit_note as mi');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        if ($data['reference_number'] != '')
        {
            $likeCriteria = "(mi.reference_number  LIKE '%" . $data['reference_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('mi.id_intake', $data['id_intake']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        $this->db->order_by("mi.id", "ASC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getInvoicesByData($data)
    {
        $balance_amount = "0";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $this->db->where('mi.id_student', $data['id_student']);
        $this->db->where('mi.type', $data['type']);
        // $this->db->where('mi.paid_amount >', $balance_amount);
        // $likeCriteria = "(mi.paid_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $data['id_student'] . "' and mi.type  ='" . $data['type'] . "' and mi.id_program  ='" . $data['id_program'] . "')";
        // $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();
    }

    function getInvoicesByStudentId($id_student)
    {
        $balance_amount = "0";
        $type = "Student";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $this->db->where('mi.type !=', 'Sponsor');
        $likeCriteria = "(mi.balance_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $id_student . "' and mi.type  ='" . $type . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();

    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getDebitNote($id)
    {
        $this->db->select('cn.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('debit_note as cn');
        $this->db->join('programme as p', 'cn.id_program = p.id','left');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDebitNote($data)
    {
        $this->db->trans_start();
        $this->db->insert('debit_note', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewDebitNoteDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('debit_note_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editDebitNote($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('debit_note', $data);
        return TRUE;
    }

    function addNewTempDebitNoteDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_debit_note_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
// echo "<Pre>";  print_r($db);exit;
        return $insert_id;
    }

    function getTempDebitNoteDetails($id_session)
    {
        $this->db->select('tcnd.*, mi.invoice_number as main_invoice');
        $this->db->from('temp_debit_note_details as tcnd');
        $this->db->join('main_invoice as mi', 'tcnd.id_main_invoice = mi.id');        
        $this->db->where('tcnd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_debit_note_details');
    }

     function deleteTempData($id)
    { 
       $this->db->where('id', $id);
       $this->db->delete('temp_debit_note_details');
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_debit_note_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_debit_note_details', $data);
        return TRUE;
    }

     function getApplicantListByData($data)
    {
        $this->db->select('*');
        $this->db->from('applicant');
        $this->db->where('id_program', $data['id_program']);
        $this->db->where('id_intake', $data['id_intake']);
        $this->db->where('applicant_status', 'Approved');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStudentListByData($data)
    {
        $this->db->select('DISTINCT(mi.id_student) as id_student');
        $this->db->from('student as s');
        $this->db->join('main_invoice as mi', 's.id = mi.id_student');        
        $this->db->where('mi.id_program', $data['id_program']);
        $this->db->where('mi.type', 'Student');
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_student = $result->id_student;
            $student = $this->getStudent($id_student);
            if($student)
            {
                array_push($details, $student);
            }
        }

        return $details;
    }

    function getStudent($id)
    {
        $this->db->select('cn.*');
        $this->db->from('student as cn');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCorporateListByData($data)
    {
        $this->db->select('DISTINCT(mi.id_student) as id_student');
        $this->db->from('company as s');
        $this->db->join('main_invoice as mi', 's.id = mi.id_student');        
        $this->db->where('mi.id_program', $data['id_program']);
        $this->db->where('mi.type', 'CORPORATE');
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_student = $result->id_student;
            $student = $this->getCompany($id_student);
            if($student)
            {
                array_push($details, $student);
            }
        }

        return $details;
    }

    function getCompany($id)
    {
        $this->db->select('cn.*');
        $this->db->from('company as cn');
        $this->db->where('cn.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getApplicantByApplicantId($id_applicant)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_applicant);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getInvoicesByApplicantId($id_student)
    {
        $balance_amount = "0";
        $type = "Applicant";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $likeCriteria = "(mi.balance_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $id_student . "' and mi.type  ='" . $type . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();

    }

    function getInvoice($id)
    {
        $this->db->select('mi.*, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function getCompanyDetails($id)
    {
        $this->db->select('mi.*');
        $this->db->from('company as mi');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();

        return $query->row();
    }

    function getStudentBySponser($id_sponser)
    {
        $this->db->select('DISTINCT(id_student) as id_student');
        $this->db->from('sponser_has_students');
        $this->db->where('end_date >', date('Y-m-d'));
        $this->db->where('id_sponser', $id_sponser);
         $query = $this->db->get();
         $results = $query->result();
         // print_r($results);exit();

         $details = array();
         foreach ($results as $key => $result)
         {
            $student_data = $this->getStudentData($result->id_student);

            array_push($details, $student_data);
         }     
         return $details;
    }

    function getStudentData($id_student)
    {
        $this->db->select('*');
        $this->db->from('student');
         $this->db->where('id', $id_student);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getInvoicesByStudentIdNSponser($id_student)
    {
        $balance_amount = "0";
        $type = "Sponsor";
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');        
        // $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.status', '1');
        $this->db->where('mi.type', 'Sponsor');
        $likeCriteria = "(mi.balance_amount  > '" . $balance_amount . "' and mi.id_student  ='" . $id_student . "' and mi.type  ='" . $type . "')";
        $this->db->where($likeCriteria);
        $query = $this->db->get();
        return $query->result();

    }

    function generateDebitNote()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('debit_note as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "DB" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }
    
    function getBankRegistration()
    {
        $this->db->select('fc.*, c.name as country, s.name as state');
        $this->db->from('bank_registration as fc');
        $this->db->join('country as c', 'fc.id_country = c.id');
        $this->db->join('state as s', 'fc.id_state = s.id');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
}

