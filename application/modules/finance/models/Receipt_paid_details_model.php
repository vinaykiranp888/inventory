<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Receipt_paid_details_model extends CI_Model
{
    function receiptPaidDetailsList()
    {
        $this->db->select('rpd.*, r.receipt_number, pt.name payment_type');
        $this->db->from('receipt_paid_details as rpd');
        $this->db->join('receipt as r', 'rpd.id_receipt = r.id');
        $this->db->join('payment_type as pt', 'rpd.id_payment_type = pt.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getReceiptPaidDetails($id)
    {
        $this->db->select('rpd.*, r.receipt_number, pt.name payment_type');
        $this->db->from('receipt_paid_details as rpd');
        $this->db->join('receipt as r', 'rpd.id_receipt = r.id');
        $this->db->join('payment_type as pt', 'rpd.id_payment_type = pt.id');
        $this->db->where('rpd.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewReceiptPaidDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editReceiptPaidDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('receipt_paid_details', $data);
        return TRUE;
    }
}

