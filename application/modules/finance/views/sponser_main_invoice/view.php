<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Cancel Sponsor Invoice</h3>
            <a href="../approvalList" class="btn btn-link btn-back">‹ Back</a>
        </div>
        <form id="form_performa_invoice" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Sponsor Invoice Details</h4>             
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="<?php echo $mainInvoice->invoice_number;?>" readonly="readonly">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Type</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->type;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks</label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->remarks;?>" readonly="readonly">
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Total Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="invoice_total" name="invoice_total" value="<?php echo number_format($mainInvoice->invoice_total, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Discount Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_discount" name="total_discount" value="<?php echo number_format($mainInvoice->total_discount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Payable Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->total_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>
                
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Paid Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->paid_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Balance Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->balance_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                            if($mainInvoice->status == '0')
                            {
                                echo 'Pending';
                            }
                            elseif($mainInvoice->status == '1')
                            {
                                echo 'Approved';
                            }
                            elseif($mainInvoice->status == '2')
                            {
                                echo 'Rejected';
                            }?>" readonly="readonly">
                        </div>
                    </div>
                </div>



                <div class="row">

                <?php
                if($mainInvoice->status == '2')
                {
                 ?>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Cancel Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $mainInvoice->reason; ?>" readonly>
                        </div>
                    </div>

                <?php
                }
                ?>

                </div>
                
            </div>


            <div class="form-container">
                <h4 class="form-group-title"><?php echo $mainInvoice->type; ?> Details For Main Invoice</h4>              

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php echo $mainInvoice->type; ?> Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_name" name="student_name" value="<?php echo $mainInvoice->sponser_name;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php echo $mainInvoice->type; ?> Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_nric" name="student_nric" value="<?php echo $mainInvoice->sponser_code;?>" readonly="readonly">
                        </div>
                    </div>
                </div>
            </div>



              <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl> 
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Program Scheme :</dt>
                                <dd><?php echo $studentDetails->program_scheme; ?></dd>
                            </dl>
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


        <form id="form_performa_invoice" action="" method="post">

      <?php
            if($mainInvoice->status == '1')
            {
             ?>

            <div class="form-container">
                <h4 class="form-group-title">Cancellation Invoice</h4>


                 <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p> Cancellation <span class='error-text'>*</span></p>
                            <!-- <label class="radio-inline">
                                <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                            </label> -->
                            <label class="radio-inline">
                                <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Cancel
                            </label>
                        </div>
                    </div>

                     <div class="col-sm-4" id="view_reject" style="display: none">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control">
                        </div>
                    </div>

                </div>

            </div>

          <?php
            }
            ?>



        <div class="button-block clearfix">
                <div class="bttn-group">

             <?php
            if($mainInvoice->status == '1')
            {
             ?>
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

                    <a href="../approvalList" class="btn btn-link">Back</a>
             <?php
            }
             ?>
                </div>
        </div>


        </form>




            <br>

        
            <div class="form-container">
                <h4 class="form-group-title">Sponsor Invoice Details</h4>  

                <div class="custom-table">
                    <table class="table" id="list-table">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Fee Item</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $total_amount = 0;
                        if (!empty($mainInvoiceDetailsList)) {
                            $i = 1;
                            foreach ($mainInvoiceDetailsList as $record) {
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->fee_setup ?></td>
                                <td><?php echo $record->amount ?></td>
                            </tr>
                        <?php
                        $total_amount = $total_amount + $record->amount;
                        $i++;
                            }
                        }
                        $total_amount = number_format($total_amount, 2, '.', ',');
                        ?>
                        <tr>
                                <td bgcolor=""></td>
                                <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                <td bgcolor=""><b><?php echo $total_amount; ?></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Sponsor Invoice Discount Details</h4> 

                <div class="custom-table">
                    <table class="table" id="list-table">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Discount</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $discount_total_amount = 0;
                        if (!empty($mainInvoiceDiscountDetailsList)) {
                            $i = 1;
                            foreach ($mainInvoiceDiscountDetailsList as $record) {
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->name ?></td>
                                <td><?php echo $record->amount ?></td>
                            </tr>
                        <?php
                        $discount_total_amount = $discount_total_amount + $record->amount;
                        $i++;
                            }
                        }
                        $discount_total_amount = number_format($discount_total_amount, 2, '.', ',');
                        ?>
                        <tr>
                                <td bgcolor=""></td>
                                <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                <td bgcolor=""><b><?php echo $discount_total_amount; ?></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
   $(document).ready(function() {
        $("#form_performa_invoice").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>
