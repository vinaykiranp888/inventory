<?php
$roleModel = new Role_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];

$roleList  = $roleModel->getSideMenuListByModule($urlmodule);
// echo "<Pre>";print_r($roleList);exit();
?>

    <div class="sidebar">
        <!-- <div class="user-profile clearfix">
            <a onclick="showModelPopUp()" class="user-profile-link">
              <span><img src="<?php echo BASE_PATH; ?>assets/images/<?php echo $user_image; ?>"></span> <?php echo $name; ?>
            </a>
            <div class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                </button>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                    <li><a href="/finance/user/profile">Edit Profile</a></li>
                    <li><a href="/setup/user/logout">Logout</a></li>
                </ul>
            </div>
        </div> -->






        <div class="sidebar-nav">   
          <?php 
          for($i=0;$i<count($roleList);$i++)
          { 
            $parent_order = $roleList[$i]->parent_order;

            $data['module'] = $urlmodule;
            $data['parent_order'] = $parent_order;

             
            $urlmenuListDetails  = $roleModel->getMenuListByParentOrder($data);

            // echo "<Pre>";print_r($urlmenuListDetails);exit();

            $collapse = 'collapsed';
            $ulclass = 'collapse';
            for($a=0;$a<count($urlmenuListDetails);$a++)
            {                              
              if($urlcontroller==$urlmenuListDetails[$a]->controller)
              {
                  $collapse = "";
                  $ulclass = "";
              }
            }
            ?>                         
            <h4><a role="button" data-toggle="collapse" class="<?php echo $collapse;?>" href="#asdf<?php echo $parent_order;?>"><?php echo $roleList[$i]->parent_name;?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
            <ul class="<?php echo $ulclass;?>" id="asdf<?php echo $parent_order;?>">
                
            <?php

            $data['module'] = $urlmodule;
            $data['parent_order'] = $parent_order;

            $menuListDetails  = $roleModel->getMenuListByParentOrder($data);
            

              for($l=0;$l<count($menuListDetails);$l++)
              {
                $controller = $menuListDetails[$l]->controller;
                $action = $menuListDetails[$l]->action; ?>
                
                <li <?php
                     if($controller == $urlcontroller)
                     {
                      echo "class='active'";
                     }
                      ?>>
                    <a href="<?php echo '/' . $urlmodule . '/' . $controller . '/' . $action;?> "><?php echo $menuListDetails[$l]->menu_name;?>
                    </a>
                </li>

                <?php 
                  }
                ?>
            </ul>
        
            <?php 
              }
            ?>

        </div>





        <!-- <div class="sidebar-nav">
            <h4>Finance Setup</h4>
            <ul>
                <li><a href="/finance/currency/list">Currency Setup</a></li>
                <li><a href="/finance/currencyRateSetup/list">Currency Rate Setup</a></li>
                <li><a href="/finance/creditNoteType/list">Credit Note Type</a></li>
                <li><a href="/finance/bankRegistration/list">Bank Registration</a></li>
                <li><a href="/finance/accountCode/list">Account Code</a></li>
                <li><a href="/finance/frequencyMode/list">Frequency Mode</a></li>
                <li><a href="/finance/amountCalculationType/list">Amount Calculation Type</a></li>
                <li><a href="/finance/feeCategory/list">Fee Category</a></li>
                <li><a href="/finance/paymentType/list">Payment Type</a></li>
            </ul>
            <h4>Fee Structure</h4>
            <ul>
                <li><a href="/finance/feeSetup/list">Fee Setup</a></li>
                <li><a href="/finance/feeStructure/list">Fee Structure</a></li>
                <li><a href="/finance/feeStructureActivity/list">Fee Structure Activity</a></li>
                <li><a href="/finance/partnerUniversityFee/list">Partner University Fee</a></li>
                <li><a href="/finance/feeStructureData/list">Fee Structure</a></li>
            </ul>
            <h4>Discount Setup</h4>
            <ul>
                <li><a href="/finance/welcome/comingsoon">Early Bird</a></li>
                <li><a href="/finance/sibblingDiscount/list">Sibbling Discount</a></li>
                <li><a href="/finance/employeeDiscount/list">Employee Discount</a></li>
                <li><a href="/finance/alumniDiscount/list">Alumni Discount</a></li>
            </ul>

            <h4>Invoice</h4>
            <ul>
                <li><a href="/finance/performaInvoice/list">Performa Invoice</a></li>
                <li><a href="/finance/mainInvoice/list">Invoice Generation</a></li>
                <li><a href="/finance/mainInvoice/approvalList">Invoice Cancellation</a></li>
                <li><a href="/finance/creditNote/list">Credit Note</a></li>
            </ul>

            <h4>Receivables</h4>
            <ul>
                <li><a href="/finance/receipt/list">Issue Receipt</a></li>
                <li><a href="/finance/receipt/approvalList">Receipt Approval</a></li>
                <li><a href="/finance/creditNote/list">Credit Note</a></li>
                
            </ul>

            <h4>Credit Note</h4>
            <ul>
                <li><a href="/finance/creditNote/list">Credit Note Entry</a></li>
                <li><a href="/finance/welcome/comingsoon">Credit Note Approval</a></li>
                <li><a href="/finance/creditNote/list">Credit Note</a></li>
                
            </ul>

            <h4>Debit Note</h4>
            <ul>
                <li><a href="/finance/debitNote/list">Debit Note Entry</a></li>
                <li><a href="/finance/welcome/comingsoon">Debit Note Approval</a></li>
                <li><a href="/finance/creditNote/list">Credit Note</a></li>
                
            </ul>

            <h4>Refund</h4>
            <ul>
                <li><a href="/finance/StatementOfAccount/list">Statement Of Accounts</a></li>
            </ul>

            <h4>Refund</h4>
            <ul>
                <li><a href="/finance/debitNote/list">Debit Note Entry</a></li>
                <li><a href="/finance/welcome/comingsoon">Debit Note Approval</a></li>
            </ul>
                                
            <h4>Partner University Invoice</h4>
            <ul>
                <li><a href="/finance/partnerUniversityInvoice/list">Invoice Generation</a></li>
            </ul>
            
            <h4>Sponsor Invoice</h4>
            <ul>
                <li><a href="/finance/sponserMainInvoice/list">Sponsor Invoice Generation</a></li>
                <li><a href="/finance/sponserMainInvoice/approvalList">Sponsor Invoice Cancellation</a></li>
            </ul>
            <h4>Advance Payment</h4>
            <ul>
                <li><a href="/finance/mainInvoice/comingSoon">Advance Payment</a></li>
                
            </ul>
            <h4>Refund Note</h4>
            <ul>
                <li><a href="/finance/mainInvoice/comingSoon">Refund Entry</a></li>
                
            </ul>
        </div> -->
    </div>


    <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>

              <div class="modal-body">

                <br>

                <form action="/setup/organisation/uploadUserImage" id="form_upload_image" action="" method="post" enctype="multipart/form-data">


                  <div class="form-container">
                    <h4 class="form-group-title"> Upload Image</h4>

                      <div class="container">
                      <!-- Page Heading -->
                          <div class="row">
                              <div class="col-6 offset-md-3">
                                

                                    <input type="hidden" name="id" id="id" value="<?php echo $id_user; ?>">                                    
                                    
                           
                                    <div class="form-group">
                                      <label for="exampleInputFile">File input</label>
                                      <input type="file" class="dropify" name="image" id="image" data-height="300" required>
                                       
                                    </div>
                       
                                  
                                   
                              </div>
                          </div>
                               
                      </div>


                  <button type="submit" class="btn btn-primary">Upload</button>

                  </div>
              
              </form>

            </div>
            </div>

          </div>
      

    </div>

 
<script>
  function showModelPopUp()
  {
    $('#myModal').modal('show');
  }
 
</script>