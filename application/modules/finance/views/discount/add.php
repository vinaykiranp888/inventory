<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Discount</h3>
        </div>
        <form id="form_fee_category" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Discount Details</h4> 

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Discount Type <span class='error-text'>*</span></label>
                            <select name="id_discount_type" id="id_discount_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($discountTypeList))
                                {
                                    foreach ($discountTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date">
                        </div>
                    </div>

                   
                </div>


                <div class="row">


                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <select name="id_intake" id="id_intake" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->year . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="id_currency" id="id_currency" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount Type <span class='error-text'>*</span></label>
                            <select name="amount_type" id="amount_type" class="form-control" onchange="percentageAmount(this.value)">
                                <option value="">Select</option>
                                <option value="1">Amount</option>
                                <option value="2">Percentage</option>
                            </select>
                        </div>
                    </div>



                </div>


                <div class="row">



                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div> -->



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label id="view_amount" style="display: none;">Amount <span class='error-text'>*</span></label>
                            <label id="view_percentage" style="display: none;">Percentage <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount" max="100">
                        </div>
                    </div>




                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    function percentageAmount(value)
    {
        if(value == '1')
        {
                $("#view_amount").show();
                $("#view_percentage").hide();
        }
        else
        if(value == '2')
        {
                $("#view_amount").hide();
                $("#view_percentage").show();
        }
    }


    $(document).ready(function() {
        $("#form_fee_category").validate({
            rules: {
                id_discount_type: {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                amount_type: {
                    required: true
                },
                id_currency: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_discount_type: {
                    required: "<p class='error-text'>Select Discount Type</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                amount_type: {
                    required: "<p class='error-text'>Select Amount Type</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                amount: {
                    required: "<p class='error-text'>Value Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
