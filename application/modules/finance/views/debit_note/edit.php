<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_debitNote" action="" method="post">
        <div class="page-title clearfix">
            <h3>View Debit Note</h3>
            <a href="../list" class="btn btn-link btn-back">‹ Back</a>
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Debit Note Details</h4> 

  
          <div class="row">
              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Reference Number</label>
                      <input type="text" class="form-control" id="debitNote_number" name="debitNote_number" readonly="readonly" value="<?php echo $debitNote->reference_number;?>" >
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Amount <span class='error-text'>*</span></label>
                      <input type="Amount" class="form-control" id="amount" name="amount" readonly="readonly" value="<?php echo number_format($debitNote->amount, 2, '.', ',');?>" >
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Ratio</label>
                      <input type="text" readonly="readonly" class="form-control" id="ratio" name="ratio" value="<?php echo $debitNote->ratio;?>">
                  </div>
              </div>
          </div>

          <div class="row">



              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Date </label>
                      <input type="text" readonly="readonly" class="form-control" id="remarks" name="remarks" value="<?php echo date('d-m-Y', strtotime($debitNote->created_dt_tm));?>">
                  </div>
              </div>

              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Status </label>
                      <input type="text" class="form-control" id="status" name="status" value="<?php 
                      if($debitNote->status == '0')
                        {
                            echo 'Pending';
                        }
                        elseif($debitNote->status == '1')
                        {
                            echo 'Approved';
                        }
                        elseif($debitNote->status == '2')
                        {
                            echo 'Rejected';
                        }?>" readonly="readonly">
                  </div>
              </div>

              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Id Type <span class='error-text'>*</span></label>
                        <select name="id_type" id="id_type" class="form-control" disabled="true">
                            <option value="">Select</option>
                            <?php
                            if (!empty($debitNoteTypeList))
                            {
                                foreach ($debitNoteTypeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $debitNote->id_type)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
              </div>
            
          </div>


          <div class="row">

            <div class="col-sm-4">
                <div class="form-group">
                    <label>Type</label>
                    <input type="text" readonly="readonly" class="form-control" id="type" name="type" value="<?php echo $debitNote->type;?>">
                </div>
            </div>


            <div class="col-sm-4">
                <div class="form-group">
                    <label>Description</label>
                    <input type="text" readonly="readonly" class="form-control" id="remarks" name="remarks" value="<?php echo $debitNote->description;?>">
                </div>
            </div>


            

          </div>


        </div>


        <div class="page-title clearfix">
            <a href="<?php echo '/finance/debitNote/generateDebitNote/'.$debitNote->id ?>" target="_blank" class="btn btn-link btn-back">
                Download Debit Note >>></a>
        </div>















          <?php
           
            if($debitNote->type == 'CORPORATE')
            {
             
             ?>


            <div class="form-container">
                <h4 class="form-group-title">Company Details</h4> 

      
                <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Company Name</label>
                          <input type="text" readonly="readonly" class="form-control" id="company_name" name="company_name" value="<?php echo $companyDetails->name;?>">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Company Registration Number</label>
                          <input type="text" readonly="readonly" class="form-control" id="registration_number" name="registration_number" value="<?php echo $companyDetails->registration_number;?>">
                      </div>
                  </div>


                </div>


            </div>


            <?php
           
            }
            elseif($debitNote->type == 'Student')
            {
             
             ?>



                
                <?php
                
                if($studentDetails)
                {
                
                 ?>


                   <div class="form-container">
                  <h4 class="form-group-title">Student Details</h4>
                  <div class='data-list'>
                      <div class='row'> 
                          <div class='col-sm-6'>
                              <dl>
                                  <dt>Student Name :</dt>
                                  <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                              </dl>
                              <dl>
                                  <dt>Student NRIC :</dt>
                                  <dd><?php echo date('d-m-Y', strtotime($studentDetails->nric));?></dd>
                              </dl>
                                                
                          </div>        
                          
                          <div class='col-sm-6'>                           
                              
                              <dl>
                                  <dt>Student Email :</dt>
                                  <dd><?php echo ucwords($studentDetails->email_id);?></dd>
                              </dl>
                              <dl>
                                  <dt>Program Scheme :</dt>
                                  <dd>
                                    <?php echo $studentDetails->program_scheme;?>

                                  </dd>
                              </dl>    
                          </div>
                      </div>
                  </div>
              </div>


              <br>



                   <?php
               }
               
            }
            ?>


              








        <?php
                if($invoiceDetails)
                {
                 ?>




                 <div class="form-container">
                <h4 class="form-group-title">Invoice Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Invoice Number :</dt>
                                <dd><?php echo ucwords($invoiceDetails->invoice_number);?></dd>
                            </dl>
                            <dl>
                                <dt>Invoice Date :</dt>
                                <dd><?php echo date('d-m-Y', strtotime($invoiceDetails->date_time));?></dd>
                            </dl>
                            <dl>
                                <dt>Invoice Type :</dt>
                                <dd><?php echo ucwords($invoiceDetails->type);?></dd>
                            </dl>  
                            <dl>
                                <dt>Currency :</dt>
                                <dd><?php
                                if($invoiceDetails->currency_name != '')
                                {
                                  echo ucwords($invoiceDetails->currency_name);
                                }
                                else
                                {
                                  echo ucwords($invoiceDetails->currency_name);
                                } 
                                ?></dd>
                            </dl>                       
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Invoice Total :</dt>
                                <dd>
                                    <?php echo $invoiceDetails->invoice_total;?>
                                </dd>
                            </dl>
                            <dl>
                                <dt>Payable Amount :</dt>
                                <dd>
                                  <?php echo $invoiceDetails->total_amount;?>
                                </dd>
                            </dl>
                            <dl>
                                <dt>Balance Amount</dt>
                                <dd>
                                  <?php echo $invoiceDetails->balance_amount;?>

                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>


            <br>



                 <?php
             }
             ?>




      

           </form>

    

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>
<script>
    $('select').select2();
</script>