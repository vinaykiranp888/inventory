<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Cancel Wrong Invoice</h3>
            <a href="../approvalList" class="btn btn-link btn-back">‹ Back</a>
        </div>


        
       

            <div class="form-container">
                <h4 class="form-group-title">Invoice Details</h4>             
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="invoice_number" name="invoice_number" value="<?php echo $mainInvoice->invoice_number;?>" readonly="readonly">
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Type <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->type;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks </label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php echo $mainInvoice->remarks;?>" readonly="readonly">
                        </div>
                    </div>
                    
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Invoice Total Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="invoice_total" name="invoice_total" value="<?php echo number_format($mainInvoice->invoice_total, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Discount Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_discount" name="total_discount" value="<?php echo number_format($mainInvoice->total_discount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>Total Payable Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->total_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>
                
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Paid Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->paid_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Balance Amount <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo number_format($mainInvoice->balance_amount, 2, '.', ',');?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <input type="amount" class="form-control" id="total_amount" name="total_amount" value="<?php echo $mainInvoice->currency_name; ?>" readonly="readonly">
                        </div>
                    </div>



                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Time <span class='error-text'>*</span></label>
                            <input type="text" id="date_time" name="date_time" class="form-control" value="<?php echo date('d-m-Y', strtotime($mainInvoice->created_dt_tm)); ?>" readonly>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks" value="<?php 
                            if($mainInvoice->status == '0')
                            {
                                echo 'Pending';
                            }
                            elseif($mainInvoice->status == '1')
                            {
                                echo 'Approved';
                            }
                            elseif($mainInvoice->status == '2')
                            {
                                echo 'Cancelled';
                            }?>" readonly="readonly">
                        </div>
                    </div>
                </div>


                <div class="row">

                    <?php
                    if($mainInvoice->status == '2')
                    {
                     ?>


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Cancel Reason <span class='error-text'>*</span></label>
                                <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $mainInvoice->reason; ?>" readonly>
                            </div>
                        </div>

                    <?php
                    }
                    ?>

                </div>
            </div>





            <div class="form-container">
                <h4 class="form-group-title"><?php echo $mainInvoice->type; ?> Details For Main Invoice</h4>              

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php echo $mainInvoice->type; ?> Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_name" name="student_name" value="<?php echo $invoiceFor->full_name;?>" readonly="readonly">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php
                              if($mainInvoice->type == 'CORPORATE')
                              {
                                echo 'Registration Number';
                              }else
                              {
                                echo 'NRIC';
                              } ?> <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="student_nric" name="student_nric" value="<?php echo $invoiceFor->nric;?>" readonly="readonly">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="programme" name="programme" readonly="readonly" value="<?php echo $mainInvoice->programme_code . ' - ' . $mainInvoice->programme_name;?>" >
                        </div>
                    </div>


                    

                </div>



        </div>
            

        <form id="form_main" action="" method="post">

             <?php
            if($mainInvoice->status == '1')
            {
             ?>


        


             <div class="form-container">
                <h4 class="form-group-title">Approval Details For Main Invoice</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p> Cancellation <span class='error-text'>*</span></p>
                            <!-- <label class="radio-inline">
                                <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                            </label> -->
                            <label class="radio-inline">
                                <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Cancel
                            </label>
                        </div>
                    </div>

                     <div class="col-sm-4" id="view_reject" style="display: none">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control">
                        </div>
                    </div>

                </div>


            </div>



          <?php
            }
            ?>

        


        <div class="button-block clearfix">
                <div class="bttn-group">

             <?php
            if($mainInvoice->status == '1')
            {
             ?>
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

                    <a href="../approvalList" class="btn btn-link">Back</a>
             <?php
            }
             ?>
                

                </div>

        </div>



        </form>








            

        
             <?php
            if($mainInvoice->fee_type == 'Registration')
            {
                ?>

                    <div class="form-container">
                        <h4 class="form-group-title">Main Invoice Details</h4>  

                        <div class="custom-table">
                            <table class="table" id="list-table">
                                <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Fee Item</th>
                                    <th>Amount</th>
                                    <th>Tax ( % )</th>
                                    <th>Total Amount</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $total_amount = 0;
                                if (!empty($mainInvoiceDetailsList)) {
                                    $i = 1;
                                    foreach ($mainInvoiceDetailsList as $record) {
                                ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->fee_setup ?></td>
                                        <td><?php echo $record->price_before_gst ?></td>
                                        <td><?php echo $record->gst_amount . " ( " . $record->gst_percentage . " )" ?></td>
                                        <td><?php echo $record->price ?></td>
                                        <td><?php echo $record->quantity ?></td>
                                        <td><?php echo $record->amount ?></td>
                                    </tr>
                                <?php
                                $total_amount = $total_amount + $record->amount;
                                $i++;
                                    }
                                }
                                $total_amount = number_format($total_amount, 2, '.', ',');
                                ?>
                                <tr>
                                    <td bgcolor="" colspan="5"></td>
                                    <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                    <td bgcolor=""><b><?php echo $total_amount; ?></b></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                <?php
            }else
            {
                ?>

                        <div class="form-container">
                            <h4 class="form-group-title">Main Invoice Details</h4>  

                            <div class="custom-table">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Fee Item</th>
                                        <!-- <th>Frequency Mode</th>
                                        <th>Currency</th>
                                        <th>Amount Calculation Mode</th>
                                        <th>Description</th> -->
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $total_amount = 0;
                                    if (!empty($mainInvoiceDetailsList)) {
                                        $i = 1;
                                        foreach ($mainInvoiceDetailsList as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->fee_setup ?></td>
                                            <!-- <td><?php echo $record->frequency_mode ?></td>
                                            <td><?php echo $mainInvoice->currency ?></td>
                                            <td><?php echo $record->amount_calculation_type ?></td>
                                            <td><?php echo $record->description ?></td> -->
                                            <td><?php echo $record->amount ?></td>
                                        </tr>
                                    <?php
                                    $total_amount = $total_amount + $record->amount;
                                    $i++;
                                        }
                                    }
                                    $total_amount = number_format($total_amount, 2, '.', ',');
                                    ?>
                                    <tr>
                                            <td bgcolor=""></td>
                                            <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                            <td bgcolor=""><b><?php echo $total_amount; ?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

            <?php
            }

            ?>



        <?php
        if(!empty($mainInvoiceDiscountDetailsList))
        {
            ?>




            <div class="form-container">
                <h4 class="form-group-title">Main Invoice Discount Details</h4> 

                <div class="custom-table">
                    <table class="table" id="list-table">
                        <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Discount</th>
                            <th>Currency</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            $discount_total_amount = 0;
                        if (!empty($mainInvoiceDiscountDetailsList)) {
                            $i = 1;
                            foreach ($mainInvoiceDiscountDetailsList as $record) {
                        ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->name ?></td>
                                <td><?php echo $mainInvoice->currency ?></td>
                                <td><?php echo $record->amount ?></td>
                            </tr>
                        <?php
                        $discount_total_amount = $discount_total_amount + $record->amount;
                        $i++;
                            }
                        }
                        $discount_total_amount = number_format($discount_total_amount, 2, '.', ',');
                        ?>
                        <tr>
                                <td bgcolor=""></td>
                                <td bgcolor="" style="text-align:  right;"><b> Total :  </b></td>
                                <td bgcolor=""><b><?php echo $discount_total_amount; ?></b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        <?php
        
        }
        ?>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>
