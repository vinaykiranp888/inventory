<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Fee Structure Main</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Fee Structure Main</h4>

            <div class="row">


            		<div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name In Other Language </label>
                            <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                        </div>
                    </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>



                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Programme <span class='error-text'>*</span></label>
                     <select name='id_programme' id='id_programme' class="form-control" >
                        <option value="">Select</option>
                        <?php
                           if (!empty($programmeList))
                           {
                             foreach ($programmeList as $record)
                             {
                                    ?>
                        <option value="<?php echo $record->id;  ?>">
                           <?php echo $record->code . " - " . $record->name;  ?>
                        </option>
                        <?php
                            }
                          }
                           ?>
                     </select>
                  </div>
                </div>


                
               
            </div>

            <!-- <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>From Intake <span class='error-text'>*</span></label>
                        <span id="view_intake">
                          <select class="form-control" id='id_intake' name='id_intake'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>



            	<div class="col-sm-4">
                    <div class="form-group">
                        <label>To Intake </label>
                        <span id="view_intake_to">
                          <select class="form-control" id='id_intake_to' name='id_intake_to'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>
                
               
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Learning Mode <span class='error-text'>*</span></label>
                        <span id="view_learning_mode">
                          <select class="form-control" id='id_learning_mode' name='id_learning_mode'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>


            </div> -->

            <div class="row">


            

                <!-- <div class="col-sm-4">
                  <div class="form-group">
                     <label>Program Scheme Mode <span class='error-text'>*</span></label>
                     <span id="view_program_scheme">
                          <select class="form-control" id='id_program_has_scheme' name='id_program_has_scheme'>
                            <option value=''></option>
                          </select>

                     </span>
                  </div>
                </div> -->




                <div class="col-sm-4">
                  <div class="form-group">
                     <label>Currency <span class='error-text'>*</span></label>
                     <select name="id_currency" id="id_currency" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($currencyList))
                           {
                             foreach ($currencyList as $record)
                             {
                                    ?>
                        <option value="<?php echo $record->id;  ?>">
                           <?php echo $record->code . " - " . $record->name;  ?>
                        </option>
                        <?php
                            }
                          }
                           ?>
                     </select>
                  </div>
                </div>



            		<div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>               
              
            </div>


        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script type="text/javascript">

    function getProgrammeByEducationLevelId(id_education_level)
    {
      // alert(id_education_level);
      if(id_education_level != '')
        {

            $.get("/finance/feeStructure/getProgrammeByEducationLevelId/"+id_education_level, function(data, status){
           
                $("#view_programme").html(data);
                $("#view_programme").show();
            });
        }

    }

    function getIntakes()
    {

     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/finance/feeStructure/getIntakes',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake").html(result);
               }
            });


     var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/finance/feeStructure/getIntakesTo',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_intake_to").html(result);
               }
            });


      var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
            $.ajax(
            {
               url: '/finance/feeStructure/getLearningModeByProgramId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_learning_mode").html(result);
                $("#view_learning_mode").show();
                // $("#dummy_learning_mode").hide();


                // if (tempPR['id_programme'] != '' && tempPR['id_intake'] == '')
                // {
                    
                // }
                // var ita = $("#invoice_total_amount").val();
                // $("#receipt_amount").val(ita);
               }
            });


            var id_program = $("#id_programme").val();

            $.get("/finance/feeStructure/getSchemeByProgramId/"+id_program, function(data, status){
                $("#view_program_scheme").html(data);
                $("#view_program_scheme").show();
            });
    }
    
    

    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                  required: true
                },
                code: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_education_level: {
                  required: true
                },
                id_programme: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 id_course_registered_landscape: {
                    required: true
                },
                 id_learning_mode: {
                    required: true
                },
                 id_program_has_scheme: {
                    required: true
                },
                id_currency: {
                  required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Intake Required</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Semester Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Student Required</p>",
                },
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>