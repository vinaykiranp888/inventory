<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Add Fee Structure </h3>
            <a href="<?php echo '../list' ?>" class="btn btn-link"> < Back</a>
        </div>



          <div class="form-container">
                <h4 class="form-group-title">Fee Structure Main Details</h4> 

            <div class="row">
                  

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Code</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->code; ?>" readonly="readonly">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $getProgrammeLandscapeLocal->name; ?>" readonly="readonly">
                      </div>
                  </div>    

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Name Optional Language</label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $getProgrammeLandscapeLocal->name_optional_language; ?>" readonly="readonly">
                      </div>
                  </div>     

            </div>



            <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Program</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $getProgrammeLandscapeLocal->program_code . " - " . $getProgrammeLandscapeLocal->program; ?>" readonly="readonly">
                      </div>
                  </div>




                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Currency <span class='error-text'>*</span></label>
                          <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                              <option value="">Select</option>
                              <?php
                              if (!empty($currencyList))
                              {
                                  foreach ($currencyList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php
                                        if($record->id == $getProgrammeLandscapeLocal->id_currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name;?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>

            </div>



          </div>





        <br>






        <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <!-- <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Local Students</a>
                    </li>   -->  
                    
                    <!-- <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">International Students</a>
                    </li>   -->
                    
                    <!-- <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Partner University</a>
                    </li> -->
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                      <div class="col-12 mt-4">



                        <form id="form_one" action="" method="post">

                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure Details</h4> 

                              <div class="row">

                                    <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_item" id="one_id_fee_item" class="form-control" required>
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                            <?php
                                                            if($feeStructureDetail->id_fee_item == $record->id)
                                                            {
                                                              echo 'selected';
                                                            }
                                                            ?>
                                                          ><?php echo $record->code . " - " . $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>


                                      <!-- <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Trigger On <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_structure_trigger" id="one_id_fee_structure_trigger" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($getFeeStructureTriggerList))
                                                  {
                                                      foreach ($getFeeStructureTriggerList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div> -->


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Amount <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="one_amount" name="one_amount"  value="<?php echo $feeStructureDetail->amount; ?>" required>
                                          </div>
                                      </div>


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <p>Registration Fee <span class='error-text'>*</span></p>
                                              <label class="radio-inline">
                                              <input type="radio" name="is_registration_fee" id="is_registration_fee" value="1" <?php if($feeStructureDetail->is_registration_fee=='1') {
                                                  echo "checked=checked";
                                              };?>><span class="check-radio"></span> Yes
                                              </label>
                                              <label class="radio-inline">
                                              <input type="radio" name="is_registration_fee" id="is_registration_fee" value="0" <?php if($feeStructureDetail->is_registration_fee=='0') {
                                                  echo "checked=checked";
                                              };?>>
                                              <span class="check-radio"></span> No
                                              </label>
                                          </div>
                                      </div>



                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Currency <span class='error-text'>*</span></label>
                                              <input type="text" class="form-control" id="one_currency" name="one_currency" value="<?php echo $getProgrammeLandscapeLocal->currency_name; ?>" readonly>
                                          </div>
                                      </div>

                              </div>

                          </div>

                          <div class="button-block clearfix">
                            <div class="bttn-group">
                                  <button type="button" class="btn btn-primary btn-lg" onclick="saveData()">Add</button>
                                  <?php
                                  if($id_fee_details != NULL)
                                  {
                                    ?>
                                    <a href="<?php echo '../../addFeeStructure/'. $id_program_landscape ?>" class="btn btn-link">Cancel</a>
                                    <?php
                                  }
                                  ?>

                              </div>

                          </div>


                        </form>


                        <!-- <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Total Amount</label>
                                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $getProgrammeLandscapeLocal->total_amount; ?>" readonly="readonly" >
                              </div>
                          </div>

                        </div> -->


                        <div class="form-container">
                            <h4 class="form-group-title">Fee Structure List</h4> 


                        

                            <div class="custom-table">
                              <table class="table" id="list-table">
                                <thead>
                                  <tr>
                                    <th>Sl. No</th>
                                    <th>Fee Item</th>
                                    <th>Is Registration Fee</th>
                                    <th>Tax Applicable</th>
                                    <th>Total Amount ( <?php echo $feeStructureLocalList[0]->currency_code ?> )</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  if (!empty($feeStructureLocalList))
                                  {
                                    $i = 1;
                                    $total_amount = 0;
                                    foreach ($feeStructureLocalList as $record)
                                    {
                                      $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                                      $tax_amount = number_format($tax_amount, 2, '.', ',');
                                  ?>
                                      <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?>                                
                                        </td>
                                        <td>
                                            <?php
                                            if($record->is_registration_fee == '1')
                                            {
                                              echo 'Yes';
                                            }else
                                            {
                                              echo 'No';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($record->gst_tax == '1')
                                            {
                                              echo 'Yes';
                                            }else
                                            {
                                              echo 'No';
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $record->amount ?></td>
                                        <td>
                                          <a href='/finance/feeStructure/addFeeStructure/<?php echo $id_program_landscape;?>/<?php echo $record->id;?>'>Edit</a> | 
                                          <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Delete">Delete</a>
                                        </td>
                                      </tr>
                                  <?php
                                  $total_amount = $total_amount + $record->amount;
                                  $i++;
                                    }
                                     $total_amount = number_format($total_amount, 2, '.', ',');
                                    ?>

                                    <tr >
                                        <td bgcolor="" colspan="3"></td>
                                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                        <td bgcolor="">
                              <input type="hidden" id="local_amount" name="local_amount" value="<?php echo $total_amount; ?>">

                              <b><?php echo $total_amount . " ( " . $feeStructureLocalList[0]->currency_code . " ) ";  ?></b></td>
                                        <!-- <td class="text-center"> -->
                                        <td bgcolor=""></td>
                                      </tr>
                                    <?php
                                  }
                                  ?>
                                </tbody>
                              </table>
                            </div>

                        </div>


                      </div>

                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_two" action="" method="post">


                            <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (International) Details</h4> 

                              <div class="row">

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="two_id_fee_item" id="two_id_fee_item" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->code . " - " . $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>


                                      <!-- <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Trigger On <span class='error-text'>*</span></label>
                                              <select name="two_id_fee_structure_trigger" id="two_id_fee_structure_trigger" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($getFeeStructureTriggerList))
                                                  {
                                                      foreach ($getFeeStructureTriggerList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div> -->


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Amount <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="two_amount" name="two_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Currency <span class='error-text'>*</span></label>
                                              <input type="text" class="form-control" id="two_currency" name="two_currency" value="<?php echo $getProgrammeLandscapeLocal->currency_name; ?>" readonly>
                                          </div>
                                      </div>


                                    </div>


                                  <div class="row">
                                
                                      <div class="col-sm-3">
                                          <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveDataUSD()">Add</button>
                                      </div>
                                  </div>

                            </div>

                           


                        </form>


                       <!--  <div class="row">

                          <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Total Amount</label>
                                  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $getProgrammeLandscapeInternational->total_amount; ?>" readonly="readonly" >
                              </div>
                          </div>

                        </div> -->


                        <div class="form-container">
                              <h4 class="form-group-title">Fee Structure List</h4> 


                          

                          <div class="custom-table">
                            <table class="table" id="list-table">
                              <thead>
                                <tr>
                                  <th>Sl. No</th>
                                  <th>Fee Item</th>
                                  <th>Tax Applicable</th>
                                  <th>Total Amount</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                if (!empty($feeStructureInternationalList))
                                {
                                  $i = 1;
                                  $total_amount = 0;
                                  foreach ($feeStructureInternationalList as $record)
                                  {
                                    $tax_amount = ($record->amount * 0.01) * $tax_percentage;
                                    $tax_amount = number_format($tax_amount, 2, '.', ',');
                                ?>
                                    <tr>
                                      <td><?php echo $i ?></td>
                                      <td><?php echo $record->fee_structure_code . " - " . $record->fee_structure ?></td>
                                      <td>
                                        <?php
                                        if($record->gst_tax == '1')
                                        {
                                          echo 'Yes';
                                        }else
                                        {
                                          echo 'No';
                                        }
                                        ?>
                                      </td>
                                      <td><?php echo $record->amount ; ?></td>
                                        <td>
                                      <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Edit">Delete</a>
                                      </td>
                                    </tr>
                                <?php
                                $total_amount = $total_amount + $record->amount;
                                $i++;
                                  }
                                   $total_amount = number_format($total_amount, 2, '.', ',');
                                  ?>

                                  <tr >
                                      <td bgcolor="" colspan="2"></td>
                                      <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                      <td bgcolor="">
                                        <input type="hidden" id="international_amount" name="international_amount" value="<?php echo $total_amount; ?>">
                                        <b><?php echo $total_amount ?></b></td>
                                      <!-- <td class="text-center"> -->
                                      <td bgcolor=""></td>
                                    </tr>
                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>

                        </div>


                        </div> 
                    

                    </div>



                </div>




            </div>


        </div> 







        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form id="form_four" action="" method="post">


                  <div class="form-container">
                    <h4 class="form-group-title"> Installment Details</h4>

                    <div class="row">



                        <input type="hidden" class="form-control" id="trigger_id_fee_structure" name="trigger_id_fee_structure" readonly>

                        <input type="hidden" class="form-control" id="trigger_id_training_center" name="trigger_id_training_center" readonly>
                        <input type="hidden" class="form-control" id="trigger_id_program_landscape" name="trigger_id_program_landscape" readonly>






                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Installment Trigger On
                                  <!-- Hided This Due To Change In Flow As 2 Fields Required type and Total -->
                                 <!-- <?php echo $getProgrammeLandscapeLocal->program_landscape_type; ?> -->
                                 <span class='error-text'>*</span></label>
                                <select name="trigger_id_semester" id="trigger_id_semester" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                        for ($i=1; $i <= 8
                                          // $getProgrammeLandscapeLocal->total_semester
                                           ; $i++)
                                        {?>
                                    <option value="<?php echo $i;  ?>">
                                        <?php echo $i;?>
                                    </option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Fee Item <span class='error-text'>*</span></label>
                                  <select name="trigger_id_fee_item" id="trigger_id_fee_item" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($feeSetupList))
                                      {
                                          foreach ($feeSetupList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->code . " - " . $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>




                          <div class="col-sm-3">
                              <div class="form-group">
                                  <label>Trigger Fee On <span class='error-text'>*</span></label>
                                  <select name="id_fee_structure_trigger" id="id_fee_structure_trigger" class="form-control">
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($getFeeStructureTriggerList))
                                      {
                                          foreach ($getFeeStructureTriggerList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              ><?php echo $record->name;?>
                                              </option>
                                      <?php
                                          }
                                      }
                                      ?>
                                  </select>
                              </div>
                          </div>





                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Amount <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_amount" name="triggering_amount">
                            </div>
                        </div>


                      </div>



                      <div class="row">



                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Total Installment <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="triggering_installment_nos" name="triggering_installment_nos" readonly>
                            </div>
                        </div>


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Currency <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="triggering_data_currency" name="triggering_data_currency" readonly>
                            </div>
                        </div>
                        

                    </div>


                  </div>
              
              </form>

              <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="saveInstallmentData()">Add</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>








                <div class="form-container">
                        
                <div class="row">
                    <div id='view_model'>
                    </div>
                </div>



                </div>

            </div>
            </div>

          </div>
        

        </div>

      

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    updateMasterAmount();

    function updateMasterAmount()
    {
        var tempPR = {};
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";

        tempPR['id'] = id_program_landscape;
        tempPR['amount'] = $("#local_amount").val();
        tempPR['international_amount'] = $("#international_amount").val();
        
        // alert(tempPR['international_amount']);

            $.ajax(
            {
               url: '/finance/feeStructure/updateFeeStructureMasterAmount',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // window.location.reload();
               }
            });
      }

  

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id_frequency_mode").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }



    function showInstallments(is_installment)
    {
      // alert(is_installment);
      if(is_installment == 0)
      {
        $('#view_amount').show();
        $('#view_fee_item').show();
      }else
      {

        $('#view_amount').hide();
        $('#view_fee_item').hide();
      }
    }



    function saveData()
    {

      if($('#form_one').valid())
      {
        $('#form_one').submit();

        // var tempPR = {};
        // var id_programme = "<?php echo $programme->id; ?>";
        // var id_intake = "<?php echo '0'; ?>";
        // var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        // var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        // tempPR['id_programme'] = id_programme;
        // tempPR['id_program_scheme'] = id_program_scheme;
        // tempPR['id_program_landscape'] = id_program_landscape;
        // tempPR['id_intake'] = id_intake;
        // tempPR['id_training_center'] = <?php echo '1'; ?>;
        // tempPR['id_fee_item'] = $("#one_id_fee_item").val();
        // tempPR['id_fee_structure_trigger'] = $("#one_id_fee_structure_trigger").val();
        // tempPR['currency'] = 'MYR';
        // tempPR['amount'] = $("#one_amount").val();
        // tempPR['id_frequency_mode'] = $("#one_id_frequency_mode").val();

        //     $.ajax(
        //     {
        //        url: '/finance/feeStructure/tempadd',
        //         type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         // alert(result);
        //         // $("#view").html(result);
        //         // $('#myModal').modal('hide');
        //         window.location.reload();
        //        }
        //     });

      }
        
    }


    function saveDataUSD()
    {

      if($('#form_two').valid())
        {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_training_center'] = <?php echo '1'; ?>;
        tempPR['id_fee_item'] = $("#two_id_fee_item").val();
        tempPR['id_fee_structure_trigger'] = $("#two_id_fee_structure_trigger").val();
        tempPR['currency'] = 'USD';
        tempPR['amount'] = $("#two_amount").val();
        tempPR['id_frequency_mode'] = $("#two_id_frequency_mode").val();
            $.ajax(
            {
               url: '/finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                window.location.reload();
               }
            });

          }
        
    }



    function saveDataPartnerUSD()
    {

      if($('#form_three').valid())
        {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo '0'; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_program_landscape'] = id_program_landscape;
        tempPR['id_intake'] = id_intake;
        tempPR['id_fee_item'] = $("#three_id_fee_item").val();
        tempPR['currency'] = $("#three_currency").val();
        tempPR['amount'] = $("#three_amount").val();
        tempPR['id_frequency_mode'] = $("#three_id_frequency_mode").val();
        tempPR['id_training_center'] = $("#three_id_training_center").val();
        tempPR['is_installment'] = $("#three_is_installment").val();
        tempPR['installments'] = $("#three_installments").val();



        // alert(tempPR['three_id_fee_item']);
            $.ajax(
            {
               url: '/finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == 0)
                {
                  alert('Duplicate Training Center Data Not Allowed');
                }
                else
                {
                  window.location.reload();
                }
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });

          }
    }

    function viewFeeByTrainingCenter(id_fee_structure)
    {
      // alert(id_fee_structure);
      var tempPR = {};

        var id_program_landscape = "<?php echo $id_program_landscape; ?>";


        // tempPR['id_training_center'] = id_training_center;
        tempPR['id_fee_structure'] = id_fee_structure;
        tempPR['id_program_landscape'] = id_program_landscape;

        // alert(tempPR['three_id_fee_item']);
            $.ajax(
            {
               url: '/finance/feeStructure/getTrainingCenterFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // if(result == 0)
                // {
                //   alert('Duplicate Training Center Data Not Allowed');
                // }
                // alert(result);

                $("#view_model").html(result);

                var installment_amount_selected = $("#installment_amount_selected").val();
                var installment_nos = $("#installment_nos").val();
                var id_fee_structure = $("#id_fee_structure").val();     
                var id_training_center = $("#id_data_training_center").val();   
                var id_program_landscape = $("#id_data_program_landscape").val();
                var data_currency = $("#data_currency").val();







                $("#triggering_amount").val(installment_amount_selected);
                $("#triggering_installment_nos").val(installment_nos);
                $("#trigger_id_fee_structure").val(id_fee_structure);
                $("#trigger_id_training_center").val(id_training_center);
                $("#trigger_id_program_landscape").val(id_program_landscape);
                $("#triggering_data_currency").val(data_currency);

                $('#myModal').modal('show');
                // window.location.reload();
               }
            });
    }


    function saveInstallmentData()
    {

      if($('#form_four').valid())
        {
          var tempPR = {};

          tempPR['id_fee_item'] = $("#trigger_id_fee_item").val();
          tempPR['id_fee_structure_trigger'] = $("#id_fee_structure_trigger").val();
          tempPR['id_fee_structure'] = $("#trigger_id_fee_structure").val();
          tempPR['amount'] = $("#triggering_amount").val();
          tempPR['id_training_center'] = $("#trigger_id_training_center").val();
          tempPR['id_semester'] = $("#trigger_id_semester").val();
          tempPR['id_program_landscape'] = $("#trigger_id_program_landscape").val();

            $.ajax(
            {
               url: '/finance/feeStructure/saveInstallmentData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
          }
    }




    function tempDelete(id)
    {
      // alert(id);
         $.ajax(
            {
               url: '/finance/feeStructure/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

    function deleteDataByTrainingCenter(id_training_center)
    {
        $.ajax(
            {
               url: '/finance/feeStructure/deleteTrainingCenterByTrainingCenterId/'+id_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

    function deleteSemesterDataByIdFeeStructureTrainingCenter(id_fee_training_center)
    {
      $.ajax(
            {
               url: '/finance/feeStructure/deleteSemesterDataByIdFeeStructureTrainingCenter/'+id_fee_training_center,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

 //    function tempDelete(id){

 //     $.get("/finance/feeStructure/tempDelete/"+id, function(data, status){
   
 //        // $("#selectsubcategory").html(data);
 //    });
 // }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/feeStructure/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function()
    {
        $("#form_one").validate({
            rules: {
                one_id_fee_item: {
                    required: true
                },
                one_amount: {
                    required: true
                },
                one_id_frequency_mode: {
                    required: true
                },
                is_registration_fee: {
                  required: true
                }
            },
            messages: {
                one_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                one_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                one_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                is_registration_fee: {
                    required: "<p class='error-text'>Select Is Registration Fee</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_two").validate({
            rules: {
                two_id_fee_item: {
                    required: true
                },
                two_amount: {
                    required: true
                },
                two_id_frequency_mode: {
                    required: true
                },
                two_id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                two_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                two_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                two_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                two_id_fee_structure_trigger: {
                    required: "<p class='error-text'>Select Trigger</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function()
    {

        $("#form_three").validate({
            rules: {
                three_id_fee_item: {
                    required: true
                },
                three_amount: {
                    required: true
                },
                three_id_frequency_mode: {
                    required: true
                },
                three_id_training_center: {
                    required: true
                },
                three_is_installment: {
                    required: true
                },
                three_currency: {
                    required: true
                },
                three_installments: {
                  required: true
                },
                id_fee_structure_trigger: {
                  required: true
                }
            },
            messages: {
                three_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                three_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                three_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                three_id_training_center: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                three_is_installment: {
                    required: "<p class='error-text'>Select Installment Status</p>",
                },
                three_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                three_installments: {
                    required: "<p class='error-text'>Installments Required</p>",
                },
                id_fee_structure_trigger: {
                    required: "<p class='error-text'>Installments Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });








    $(document).ready(function()
    {
        $("#form_four").validate({
            rules: {
                trigger_id_semester: {
                    required: true
                },
                trigger_id_fee_item: {
                  required: true
                }
            },
            messages: {
                trigger_id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                trigger_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    

</script>
