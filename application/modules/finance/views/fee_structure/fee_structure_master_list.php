<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Fee Structure</h3>
      <a href="addFeeStructureMaster" class="btn btn-primary">+ Add Fee Structure</a>
    </div>


    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">



                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Product</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Category Type</label>
                      <div class="col-sm-8">
                        <select name="id_category" id="id_category" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryList))
                            {
                                foreach ($categoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_category'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>



                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Product Type</label>
                      <div class="col-sm-8">
                        <select name="id_programme_type" id="id_programme_type" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($productTypeSetupList))
                            {
                                foreach ($productTypeSetupList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id == $searchParam['id_programme_type'])
                              {
                                echo "selected";
                              }
                              ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                      </div>
                    </div>

                  </div>

                </div>




              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Programme Name</th>
            <th>Programme Code</th>
             <?php for($l=0;$l<count($feeStructureItemList);$l++) { ?>
              <th><?php echo $feeStructureItemList[$l]->name;?></th>
            <?php } ?> 
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($feeStructureMasterList)) {
            $i=1;
                    $this->load->model('fee_structure_model');
          	foreach ($feeStructureMasterList as $record) {

          		?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
              <?php for($l=0;$l<count($feeStructureItemList);$l++) { 
                   $feeItemDetails = $this->fee_structure_model->getFeeamountdetails($record->id,$feeStructureItemList[$l]->feeitemid);

                ?>
                <th><?php echo $feeItemDetails->amount;?>
                   <?php if($feeItemDetails->amount>0) { ?>
 | <a href="javascript:;" title="Add" onclick="fndelete(
 <?php echo $feeItemDetails->id;?>)">Delete</a>
                   <?php } ?> 
                </th>
              <?php } ?> 

               
                <td class="text-center">
                 
                  <a href="<?php echo 'addFeeStructure/' . $record->id; ?>" title="Add">Add New Item</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();


  
  function clearSearchForm()
      {
        window.location.reload();
      }

  function fndelete(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
          $.ajax(
            {
               url: '/finance/feeStructure/deleteitem/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
    }
  }
</script>