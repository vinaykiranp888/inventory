<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Sibbling Discount</h3>
        </div>
        <form id="form_grade" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Sibbling Discount Details</h4>            
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="currency" id="currency" class="form-control">
                                <option value="">Select</option>
                                <option value="MYR">MYR</option>
                                <option value="USD">USD</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                        </div>
                    </div>



                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>
                    
                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
  $('select').select2();
  
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );


    
    $(document).ready(function() {

        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 amount: {
                    required: true
                },
                 start_date: {
                    required: true
                },
                 end_date: {
                    required: true
                },
                 currency: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Discount Name required</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Start Date required</p>",
                },
                end_date: {
                    required: "<p class='error-text'>End Date required</p>",
                },
                currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>