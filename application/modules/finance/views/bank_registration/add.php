<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Bank</h3>
        </div>
        <form id="form_bank" action="" method="post">
        <div class="form-container">
                <h4 class="form-group-title">Bank Details</h4> 
            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Swift Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank ID <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="bank_id" name="bank_id">
                    </div>
                </div>
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address" name="address">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <span id='view_state'></span>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Landmark <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="landmark" name="landmark">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>

             </div>

           <!--  <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Fund Code<span class='error-text'>*</span></label>
                        <select name="cr_fund" id="cr_fund" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($fundCodeList))
                            {
                                foreach ($fundCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Department Code<span class='error-text'>*</span></label>
                        <select name="cr_department" id="cr_department" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($departmentCodeList))
                            {
                                foreach ($departmentCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Activity Code<span class='error-text'>*</span></label>
                        <select name="cr_activity" id="cr_activity" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($activityCodeList))
                            {
                                foreach ($activityCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>    

                
            </div> -->




            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account No <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="account_no" name="account_no">
                    </div>
                </div>

              <!--   <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Account Code<span class='error-text'>*</span></label>
                        <select name="cr_account" id="cr_account" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($accountCodeList))
                            {
                                foreach ($accountCodeList as $record)
                                {?>
                             <option value="<?php echo $record->code;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>     -->

                  <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
            </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function getStateByCountry(id)
    {
        $.get("/finance/bankRegistration/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }



    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                bank_id: {
                    required: true
                },
                address: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                landmark: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                cr_fund: {
                    required: true
                },
                cr_department: {
                    required: true
                },
                cr_activity: {
                    required: true
                },
                cr_account: {
                    required: true
                },
                account_no: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Swift Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Bank Name Required</p>",
                },
                bank_id: {
                    required: "<p class='error-text'>Bank ID Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                landmark: {
                    required: "<p class='error-text'>Enter Landmark</p>",
                },
                city: {
                    required: "<p class='error-text'>Enter City Name</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                cr_fund: {
                    required: "<p class='error-text'>Select Credit Fund Code</p>",
                },
                cr_department: {
                    required: "<p class='error-text'>Select Credit Department Code</p>",
                },
                cr_activity: {
                    required: "<p class='error-text'>Select Credit Activity Code</p>",
                },
                cr_account: {
                    required: "<p class='error-text'>Select Credit Account Code</p>",
                },
                account_no: {
                    required: "<p class='error-text'>Account No Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>