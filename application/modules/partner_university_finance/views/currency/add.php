<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Currency</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Currency Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Default Language Name </label>
                        <input type="text" class="form-control" id="name_optional_language" name="name_optional_language">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Symbol Prefix <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="prefix" name="prefix">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Symbol Syffix <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="suffix" name="suffix">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Decimal Places <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="decimal_place" name="decimal_place">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>
                    </div>
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                prefix: {
                    required: true
                },
                suffix: {
                    required: true
                },
                decimal_place: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                prefix: {
                    required: "<p class='error-text'>Prefix Required</p>",
                },
                suffix: {
                    required: "<p class='error-text'>Suffix Required</p>",
                },
                decimal_place: {
                    required: "<p class='error-text'>Decimal Places Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
