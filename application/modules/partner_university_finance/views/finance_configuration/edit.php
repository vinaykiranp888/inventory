<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Financial Configuration</h3>
        </div>
        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Financial Configuration Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax SST <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="tax_sst" name="tax_sst" value="<?php echo $financeConfiguration->tax_sst; ?>">
                        <input type="hidden" class="form-control" id="id_finance_configuration" name="id_finance_configuration" value="<?php echo $id_finance_configuration; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>SST Registration Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="sst_registration_number" name="sst_registration_number" value="<?php echo $financeConfiguration->sst_registration_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax Code </label>
                        <input type="text" class="form-control" id="tax_code" name="tax_code" value="<?php echo $financeConfiguration->tax_code; ?>">
                    </div>
                </div>

                
            </div>

        </div>

        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <!-- <a href="../list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>


        </form>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

  $('select').select2();
  
  $(function(){
    $(".datepicker").datepicker({
        changeYear: true,
        changeMonth: true
    });
    });



    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                tax_sst: {
                    required: true
                },
                sst_registration_number: {
                    required: true
                },
                tax_code: {
                    required: true
                }
            },
            messages: {
                tax_sst: {
                    required: "<p class='error-text'>Tax SST Required</p>",
                },
                sst_registration_number: {
                    required: "<p class='error-text'>SST Registration Number Required</p>",
                },
                tax_code: {
                    required: "<p class='error-text'>Tax Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>