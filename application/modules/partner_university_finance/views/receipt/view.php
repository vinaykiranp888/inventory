<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Approve Receipt</h3>
            <a href="../approvalList" class="btn btn-link btn-back">‹ Back</a>
        </div>

            <div class="form-container">
                <h4 class="form-group-title">Receipt Details</h4> 

      
              <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Receipt Number</label>
                          <input type="text" class="form-control" id="receipt_number" name="receipt_number" readonly="readonly" value="<?php echo $receipt->receipt_number;?>" >
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Receipt Amount <span class='error-text'>*</span></label>
                          <input type="Amount" class="form-control" id="receipt_amount" name="receipt_amount" readonly="readonly" value="<?php echo number_format($receipt->receipt_amount, 2, '.', ',');?>" >
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Remarks / Description</label>
                          <input type="text" readonly="readonly" class="form-control" id="remarks" name="remarks" value="<?php echo $receipt->remarks;?>">
                      </div>
                  </div>
              </div>

              <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Date Time <span class='error-text'>*</span></label>
                          <input type="text" id="date_time" name="date_time" class="form-control" value="<?php echo date('d-m-Y H:i:s', strtotime($receipt->created_dt_tm)); ?>" readonly>
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Status </label>
                          <input type="text" class="form-control" id="status" name="status" value="<?php 
                          if($receipt->status == '0')
                            {
                                echo 'Pending';
                            }
                            elseif($receipt->status == '1')
                            {
                                echo 'Approved';
                            }
                            elseif($receipt->status == '2')
                            {
                                echo 'Rejected';
                            }?>" readonly="readonly">
                      </div>
                  </div>

                   <?php
                if($receipt->status == '2')
                {
                 ?>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reject Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $receipt->reason; ?>" readonly>
                        </div>
                    </div>

                <?php
                }
                ?>
                
              </div>
            </div>



            


          <div class="form-container">
                <h4 class="form-group-title"><?php echo $receipt->type; ?> Details For Receipt</h4> 
                
                <div class="row">

                      <div class="col-sm-4">
                        <div class="form-group">
                            <label><?php echo $receipt->type; ?> Name</label>
                            <input type="text" class="form-control" id="receipt_number" name="receipt_number" readonly="readonly" value="<?php echo $receiptFor->full_name;?>" >
                        </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-group">
                              <label><?php echo $receipt->type;if($receipt->type == 'COPRORATE'){
                                echo ' Code';
                            } ?> <span class='error-text'>*</span></label>
                              <input type="Amount" class="form-control" id="receipt_amount" name="receipt_amount" readonly="readonly" value="<?php echo $receiptFor->nric;?>" >
                          </div>
                      </div>


                    </div>



            </div>




            <div class="page-title clearfix">
                <a href="<?php echo '/finance/receipt/generateReceipt/'.$receipt->id ?>" target="_blank" class="btn btn-link btn-back">
                    Download Receipt >>></a>
            </div>



            <div class="form-container">
                <h4 class="form-group-title">Paid Invoice Details</h4>                       
                 <div class="custom-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Sl. No </th>
                            <th>Invoice Number </th>
                            <th>Remarks </th>
                            <th>Currency </th>
                            <th>Invoice Total</th>
                            <th>Total Discount</th>
                            <th>Total Payable</th>
                            <th>Paid Amount</th>
                            <th class="text-center">Balance Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($invoiceDetails))
                          {
                            $i=1;
                            foreach ($invoiceDetails as $record)
                            {
                              $paid_amount = number_format($record->paid_amount, 2, '.', ',');
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->invoice_number ?></td>
                                <td><?php echo $record->remarks ?></td>
                                <td><?php
                                if($record->currency_name == '')
                                {
                                  echo $record->currency;
                                }else
                                {
                                  echo $record->currency_name;
                                }
                                ?></td>
                                <td><?php echo $record->invoice_total ?></td>
                                <td><?php echo $record->total_discount ?></td>
                                <td><?php echo $record->total_amount ?></td>
                                <td><?php echo $paid_amount ?></td>
                                <td class="text-center"><?php echo $record->balance_amount ?></td>

                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                  </div>
            </div>
          <div class="form-container">
                <h4 class="form-group-title">Receipt Payment Details</h4> 
                <div class="custom-table">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Sl. No </th>
                            <th>Payment Type </th>
                            <th>Paid Amount</th>
                            <th>Reference Number</th>
                            <?php
                            if($receipt->type == 'CORPORATE')
                            {
                              ?>
                              <th>Payment Date</th>
                              <th>File</th>
                              <?php
                            }
                            ?>

                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($paymentDetails))
                          {
                            $i=1;
                            foreach ($paymentDetails as $record)
                            {
                              $paid_amount = number_format($record->paid_amount, 2, '.', ',');
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->id_payment_type ?></td>
                                <td><?php echo $paid_amount ?></td>
                                <td><?php echo $record->payment_reference_number ?></td>
                                <?php
                                if($receipt->type == 'CORPORATE')
                                {
                                ?>
                                <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                                <td>

                                  <?php
                                  if($record->image != '')
                                  {

                                  ?>
                                  
                                  <a href="<?php echo '/assets/images/' . $record->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->image; ?>)" title="<?php echo $record->image; ?>">View</a>
                                  <?php
                                  }
                                  else
                                  {
                                    echo 'No Image';
                                  }
                                  ?>

                                </td>

                                <?php
                                }
                                ?>


                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>                          
          </div>

           </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
</form>
