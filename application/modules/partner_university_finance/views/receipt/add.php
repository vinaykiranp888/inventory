<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <form id="form_receipt" action="" method="post">
        <div class="page-title clearfix">
            <h3>Add Receipt</h3>
        </div>

                
        <div class="form-container">
            <h4 class="form-group-title">Receipt Details</h4> 
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type<span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control" onchange="getStudentByProgramme()">
                            <option value="">Select</option>
                            <option value="CORPORATE">CORPORATE</option>
                            <option value="Student">Student</option>
                        </select>
                    </div>
                </div> 


                <div class="col-sm-4" id="view_program">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" onchange="getStudentByProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Currency <span class='error-text'>*</span></label>
                      <select name="currency" id="currency" class="form-control" onchange="getStudentByProgramme()">
                          <option value="">Select</option>
                            <?php
                            if (!empty($currencyList))
                            {
                                foreach ($currencyList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                      </select>
                    </div>
                </div> 


            

                <span id='student'></span>

            </div>
            <div class="row">  

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label><span id='label_span_for_type'></span> <span class='error-text'>*</span></label>
                       <span id="view_student">
                          <select class="form-control" id='id_student' name='id_student'>
                              <option value=''></option>
                            </select>
                       </span>
                    </div>
                  </div>


            
                
                <!-- <div class="col-sm-4">
                    <div class="form-group"> -->
                        <!-- <label>Select Student <span class='error-text'>*</span></label> -->
                        
                   <!--  </div>
                </div>   --> 

            </div>
        </div>

        <div id="view_student_details"  style="display: none;">
        </div>

        <div class="form-container">
            <h4 class="form-group-title">Other Details</h4> 

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="receipt_date" name="receipt_date" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Receipt Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="receipt_amount" name="receipt_amount" readonly="readonly" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks / Description</label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>
            </div>
        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        

        <div class="form-container">
            <h4 class="form-group-title">Payment Information Details</h4> 
            <div class="row">

                <div class="col-sm-3">
                        <div class="form-group">
                            <label>Payment Mode <span class='error-text'>*</span></label>
                            <select name="id_payment_mode" id="id_payment_mode" class="form-control">
                                <option value="">Select</option>
                                <option value="Cash">Cash</option>
                                <option value="DD">DD</option>
                                
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label> Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="payment_mode_amount" name="payment_mode_amount">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Reference Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="payment_reference_number" name="payment_reference_number">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                        </div>

                    </div>

            </div>
            <div id="view"></div>                        
        </div>

        
            
    

        <!-- <h3>Receipt Details</h3> -->
        <!-- <button type="button" class="btn btn-info btn-lg" onclick="opendialog()">Add</button> -->







          
           <!--  <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Payment Mode <span class='error-text'>*</span></label>
                        <select name="id_payment_mode" id="id_payment_mode" class="form-control">
                             <option value="">Select</option>
                             <option value="Cash">Cash</option>
                             <option value="DD">DD</option>
                             
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label> Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="payment_mode_amount" name="payment_mode_amount">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="payment_reference_number" name="payment_reference_number">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-lg" onclick="saveData()">Add</button>
                </div>
            
                </div>
              </div> -->
           </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>




</form>
<script>

    $('select').select2();

    // function getStudentByProgramme(id)
    // {

    //  $.get("/finance/receipt/getStudentByProgrammeId/"+id, function(data, status){
   
    //     $("#student").html(data);
    //     });
    // }

    function getStudentByProgramme()
    {
        var labelnric = $("#type").val();
        $("#label_span_for_type").html(labelnric);

        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_program'] = $("#id_programme").val();
        tempPR['currency'] = $("#currency").val();

        if(tempPR['id_program'] != '' && tempPR['type'] != '' && tempPR['currency'] != '')
        {
            $.ajax(
            {
               url: '/finance/receipt/getStudentByProgram',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student").html(result);
               }
            });
        }
    }

    function getStudentByStudentId(id)
    {
        var tempPR = {};
        tempPR['id_student'] = id;
        tempPR['currency'] = $("#currency").val();
        tempPR['type'] = $("#type").val();
        
            $.ajax(
            {
               url: '/finance/receipt/getStudentByStudentId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_student_details").html(result);
                    $("#view_student_details").show();
               }
            });
    }

    function getApplicantByApplicantId(id)
     {
        var tempPR = {};
        tempPR['id_applicant'] = id;
        tempPR['currency'] = $("#currency").val();

            $.ajax(
            {
               url: '/finance/receipt/getApplicantByApplicantId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view_student_details").html(result);
                    $("#view_student_details").show();
               }
            });


        //  $.get("/finance/receipt/getApplicantByApplicantId/"+id, function(data, status){
       
            
        // });
     }

    function opendialog()
    {
        $("#id_main_invoice").val('');
        $("#invoice_amount").val('');
        $("#paid_amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData()
    {
        var tempPR = {};
        tempPR['id_payment_mode'] = $("#id_payment_mode").val();
        tempPR['payment_mode_amount'] = $("#payment_mode_amount").val();
        tempPR['payment_reference_number'] = $("#payment_reference_number").val();
        tempPR['id'] = '0';
            $.ajax(
            {
               url: '/finance/receipt/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ita = $("#invoice_total_amount").val();
                $("#receipt_amount").val(ita);
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/receipt/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
                    var ita = $("#invoice_total_amount").val();
                    $("#receipt_amount").val(ita);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/receipt/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }

    function payableAmountChane() {
         var tempAmount = {};
        tempAmount['payable_amount'] = $("#payable_amount").val();
        // alert(tempAmount['payable_amount']);

        $.ajax(
            {
               url: '/finance/receipt/payableAmountAdd',
                type: 'POST',
               data:
               {
                tempAmount: tempAmount
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // var total_payable = $("#total_amount_payable").val();
                // alert(result);
                $("#receipt_amount").val(result);
               }
            });
        
    }


    function getStudentByStudentIdNSponser()
    {
        var tempPR = {};
        tempPR['id_student'] = $("#id_student").val();
        tempPR['id_sponser'] = $("#id_sponser").val();
        tempPR['type'] = $("#type").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_student'] != '' && tempPR['id_sponser'] != '' && tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/receipt/getStudentByStudentIdNSponser',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view_student_details").html(result);
                $("#view_student_details").show();
                
               }
            });
        }
        else
        {
            $("#view_student_details").hide();
        }
    }

 $(document).ready(function() {
        $("#form_receipt").validate({
            rules: {
                id_programme: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_student: {
                    required: true
                },
                receipt_amount: {
                    required: true
                },
                type: {
                    required: true
                },
                id_sponser: {
                    required: true
                }
            },
            messages: {
                id_programme:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake:
                {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Receipt Generating For</p>",
                },
                receipt_amount:
                {
                    required: "<p class='error-text'>Enter Payment Info. For Total Amount </p>",
                },
                type:
                {
                    required: "<p class='error-text'>Select Receipt Type</p>",
                },
                id_sponser:
                {
                    required: "<p class='error-text'>Select Sponsor Type</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>


