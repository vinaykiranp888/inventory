<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Approval List Debit Note</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Debit Note</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                
                <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Debit Note Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="reference_number" value="<?php echo $searchParam['reference_number']; ?>">
                    </div>
                  </div>
                </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Program </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programmeList)) {
                          foreach ($programmeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <div class="row">

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Type </label>
                    <div class="col-sm-8">
                      <select name="type" id="type" class="form-control">
                        <option value="">Select</option>
                        <option value="CORPORATE" <?php if($searchParam['type']=='CORPORATE'){ echo "selected"; } ?>>CORPORATE</option>
                        <option value="Student" <?php if($searchParam['type']=='Student'){ echo "selected"; } ?>>Student</option>
                      </select>
                    </div>
                  </div>
                </div>       

              </div>

              


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="approvalList" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th >Sl. No</th>
            <th >Debit Note Number</th>
            <th >Type</th>
            <th >Programme</th>
            <!-- <th >Debit Note For</th> -->
            <th >Amount</th>
            <th >Description</th>
            <th >Debit Note Date</th>
            <th class="text-center">status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($debitNoteList))
          {
            $i=1;
            foreach ($debitNoteList as $record)
            {
              $amount = number_format($record->amount, 2, '.', ',');
          ?>
              <tr>
                <td ><?php echo $i ?></td>
                <td ><?php echo $record->reference_number ?></td>
                <td ><?php echo $record->type ?></td>
                <td ><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <!-- <td ><?php
                if($record->type == 'Applicant')
                {
                  foreach ($applicantList as $value)
                  {

                    if($value->id == $record->id_student)
                    {
                      echo $value->nric . " - " . $value->full_name;
                    }
                  }
                }elseif($record->type == 'Student')
                {
                  foreach ($studentList as $value)
                  {

                    if($value->id == $record->id_student)
                    {
                      echo $value->nric . " - " . $value->full_name;
                    }
                  }
                }elseif($record->type == 'Sponsor')
                {
                  foreach ($sponserList as $value)
                  {

                    if($value->id == $record->id_sponser)
                    {
                      echo $value->code . " - " . $value->name;
                    }
                  }
                }

                 ?>
                </td> -->
                <td ><?php echo $record->amount ?></td>
                <td ><?php echo $record->description ?></td>
                <td ><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
                <td class="text-center"><?php if( $record->status == '0')
                {
                  echo "Pending";
                }
                elseif( $record->status == '1')
                {
                  echo "Approved";
                }
                elseif( $record->status == '2')
                {
                  echo "Rejected";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="Approve">Approve</i></a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>