<?php
$roleModel = new Role_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];

$roleList  = $roleModel->getSideMenuListByModule($urlmodule);
// echo "<Pre>";print_r($roleList);exit();
?>
<div class="sidebar">
    <div class="user-profile clearfix">
        <a onclick="showModelPopUp()" class="user-profile-link">
            <span><img src="<?php echo BASE_PATH; ?>assets/images/<?php echo $partner_university_image; ?>"></span> <?php echo $partner_university_name; ?>
        </a>
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            </button>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                <li ><a href="/livechat/php/app.php?admin" target="_blank">Chat</a></li>
                <li><a href="/partner_university/partnerUniversity/logout">Logout</a></li>
            </ul>
        </div>
       
    </div>


    <div class="sidebar-nav">   
          <?php 
          for($i=0;$i<count($roleList);$i++)
          { 
            $parent_order = $roleList[$i]->parent_order;

            $data['module'] = $urlmodule;
            $data['parent_order'] = $parent_order;

             
            $urlmenuListDetails  = $roleModel->getMenuListByParentOrder($data);

            // echo "<Pre>";print_r($urlmenuListDetails);exit();

            $collapse = 'collapsed';
            $ulclass = 'collapse';
            for($a=0;$a<count($urlmenuListDetails);$a++)
            {                              
              if($urlcontroller==$urlmenuListDetails[$a]->controller)
              {
                  $collapse = "";
                  $ulclass = "";
              }
            }
            ?>                         
            <h4><a role="button" data-toggle="collapse" class="<?php echo $collapse;?>" href="#asdf<?php echo $parent_order;?>"><?php echo $roleList[$i]->parent_name;?> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a></h4>
            <ul class="<?php echo $ulclass;?>" id="asdf<?php echo $parent_order;?>">

            <?php

            $data['module'] = $urlmodule;
            $data['parent_order'] = $parent_order;

            $menuListDetails  = $roleModel->getMenuListByParentOrder($data);
            

              for($l=0;$l<count($menuListDetails);$l++)
              {
                $controller = $menuListDetails[$l]->controller;
                $action = $menuListDetails[$l]->action; ?>
                
                <li <?php
                if($controller == $urlcontroller)
                {
                  echo "class='active'";
                }
                ?>>
                    <a href="<?php echo '/' . $urlmodule . '/' . $controller . '/' . $action;?> "><?php echo $menuListDetails[$l]->menu_name;?>
                    </a>
                </li>

                <?php 
                  }
                ?>
            </ul>
        
            <?php 
              }
            ?>

        </div>
</div>


<div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!-- <h4 class="modal-title">Program Landscape</h4> -->
              </div>

              <div class="modal-body">

                <br>

                <form action="/partner_university/partnerUniversity/uploadImage" id="form_upload_image" action="" method="post" enctype="multipart/form-data">


                  <div class="form-container">
                    <h4 class="form-group-title"> Upload Image</h4>

                      <div class="container">
                      <!-- Page Heading -->
                          <div class="row">
                              <div class="col-6 offset-md-3">
                                

                                    <input type="hidden" name="id" id="id" value="<?php echo $id_partner_university; ?>">
                                    
                           
                                    <div class="form-group">
                                      <label for="exampleInputFile">File input</label>
                                      <input type="file" class="dropify" name="image" id="image" data-height="300" required>
                                       
                                    </div>
                       
                                  
                                   
                              </div>
                          </div>
                               
                      </div>


                  <button type="submit" class="btn btn-primary">Upload</button>

                  </div>
              
              </form>

              <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" onclick="updateCompanyUserPassword()">Update Password</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              </div> -->



            </div>
            </div>

          </div>
      

</div>

 
<script>
  function showModelPopUp()
  {
    $('#myModal').modal('show');
  }

</script>