<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Employee Discount</h3>
        </div>
        <form id="form_grade" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Employee Discount Details</h4>               
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Employee Name <span class='error-text'>*</span></label>
                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $employeeDiscountDetails->name ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $employeeDiscountDetails->amount ?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="currency" id="currency" class="form-control">
                                <option value="">Select</option>
                                <option value="MYR"
                                <?php
                                if($employeeDiscountDetails->currency == 'MYR')
                                {
                                    echo "selected";
                                }
                                ?>
                                >MYR</option>
                                <option value="USD"
                                <?php
                                if($employeeDiscountDetails->currency == 'USD')
                                {
                                    echo "selected";
                                }
                                ?>
                                >USD</option>
                            </select>
                        </div>
                    </div>

                    
                </div>
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date("d/m/Y", strtotime($employeeDiscountDetails->start_date)) ?>" autocomplete="off">
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date("d/m/Y", strtotime($employeeDiscountDetails->end_date)) ?>" autocomplete="off">
                        </div>
                    </div>


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($employeeDiscountDetails->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($employeeDiscountDetails->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
  $( function() {
    $( ".datepicker" ).datepicker();
  } );
  </script>

<script>

    $('select').select2();
    
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_employee: {
                    required: true
                },
                 amount: {
                    required: true
                },
                 start_date: {
                    required: true
                },
                 end_date: {
                    required: true
                },
                 currency: {
                    required: true
                }
            },
            messages: {
                id_employee: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount required</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Start Date required</p>",
                },
                end_date: {
                    required: "<p class='error-text'>End Date required</p>",
                },
                currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>