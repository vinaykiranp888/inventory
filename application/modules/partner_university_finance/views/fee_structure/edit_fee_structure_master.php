<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Edit Fee Structure </h3>
            <!-- <a href="<?php echo '../list' ?>" class="btn btn-link"> < Back</a> -->
        </div>


            
        



        <form id="form_grade" action="" method="post">


          <div class="form-container">
                <h4 class="form-group-title">Fee Structure Main Details</h4> 

            <div class="row">
                  

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Code <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="code" name="code" value="<?php echo $feeStructuremaster->code; ?>">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Fee Structure Name <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $feeStructuremaster->name; ?>">
                      </div>
                  </div>    

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Name Optional Language </label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $feeStructuremaster->name_optional_language; ?>">
                      </div>
                  </div>     


            </div>



            <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Description <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="description" name="description" value="<?php echo $feeStructuremaster->description; ?>">
                      </div>
                  </div>




                  


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="program" name="program" value="<?php echo $feeStructuremaster->program_code . " - " . $feeStructuremaster->program; ?>" readonly="readonly">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Currency <span class='error-text'>*</span></label>
                          <select name="id_currency" id="id_currency" class="form-control" disabled="true">
                              <option value="">Select</option>
                              <?php
                              if (!empty($currencyList))
                              {
                                  foreach ($currencyList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php
                                        if($record->id == $feeStructuremaster->id_currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name;?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                  </div>
      


            </div>



            <!-- <div class="row">


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>From Intake</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $intake->year . " - " . $intake->name; ?>" readonly="readonly">
                      </div>
                  </div>


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Program Scheme</label>
                          <input type="text" class="form-control" id="program_scheme" name="program_scheme" value="<?php echo $feeStructuremaster->scheme_code . " - " . $feeStructuremaster->scheme_name; ?>" readonly="readonly">
                      </div>
                  </div>



                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Learning Mode</label>
                          <input type="text" class="form-control" id="learmning_mode" name="learmning_mode" value="<?php echo $feeStructuremaster->mode_of_program . " - " . $feeStructuremaster->mode_of_study; ?>" readonly="readonly">
                      </div>
                  </div>

            </div> -->

          </div>



          <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary btn-lg">Save</button>
                  <a href="../list" class="btn btn-link">Back</a>
              </div>
          </div>




        </form>


                   
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
  
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                  required: true
                },
                code: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_education_level: {
                  required: true
                },
                id_programme: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_student: {
                    required: true
                },
                 id_course_registered_landscape: {
                    required: true
                },
                 id_learning_mode: {
                    required: true
                },
                 id_program_has_scheme: {
                    required: true
                },
                id_currency: {
                  required: true
                },
                description: {
                  required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Intake Required</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Semester Required</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Student Required</p>",
                },
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_learning_mode: {
                    required: "<p class='error-text'>Select Learning Mode</p>",
                },
                id_program_has_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                id_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    

</script>
