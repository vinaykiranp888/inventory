<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Fee Structure</h3>
      <a href="addFeeStructureMaster" class="btn btn-primary">+ Add Fee Structure</a>
    </div>


    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program Name/Code</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Name</th>
            <th>Programme Name</th>
            <th>Programme Code</th>
            <!-- <th>From Intake</th>
            <th>Education Level</th>
            <th>Learning Mode</th>
            <th>Scheme</th> -->
            <th>Created On</th>
            <th>Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($feeStructureMasterList)) {
            $i=1;
          	foreach ($feeStructureMasterList as $record) {
          		?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->program ?></td>
                <td><?php echo $record->program_code ?></td>
                <!-- <td><?php echo $record->intake_year . " - " . $record->intake; ?></td>
                <td><?php echo $record->education_level ?></td>
                <td><?php echo $record->mode_of_program . " - " . $record->mode_of_study; ?></td>
                <td><?php echo $record->scheme_code . " - " . $record->scheme_name; ?></td> -->
                <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td>
                <td><?php if ($record->status == '1') {
            			echo "Active";
            		} else {
            			echo "In-Active";
            		}
            		?></td>
                <td class="text-center">
                  <a href="<?php echo 'editFeeStructureMaster/' . $record->id; ?>" title="Add">Edit</a> | 
                  <a href="<?php echo 'addFeeStructure/' . $record->id; ?>" title="Add">Add</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>