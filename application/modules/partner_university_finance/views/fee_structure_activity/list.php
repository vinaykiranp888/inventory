<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Fee Structure - Activity</h3>
      <a href="add" class="btn btn-primary">+ Add Fee Structure - Activity</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Program </label>
                    <div class="col-sm-8">
                      <select name="id_programme" id="id_programme" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_programme']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Activity </label>
                    <div class="col-sm-8">
                      <select name="id_activity" id="id_activity" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($activityList)) {
                          foreach ($activityList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_activity']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name ."-".$record->description;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Fee Code </label>
                    <div class="col-sm-8">
                      <select name="id_fee_setup" id="id_fee_setup" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($feeSetupList)) {
                          foreach ($feeSetupList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_fee_setup']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Trigger </label>
                    <div class="col-sm-8">
                      <select name="trigger" id="trigger" class="form-control">
                        <option value="">Select</option>
                        <option value="Approval Level" <?php if($searchParam['trigger']=='Approval Level'){ echo "selected"; } ?>>Approval Level</option>
                        <option value="Application Level" <?php if($searchParam['trigger']=='Application Level'){ echo "selected"; } ?>>Application Level</option>
                        
                      </select>
                    </div>
                  </div>
                </div> 

              </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Program</th>
            <th>Activity</th>
            <th>Fee Code</th>
            <th>Trigger</th>
            <th>Performa</th>
            <th>Currency</th>
            <th>Amount</th>
            <!-- <th>Amount USD</th> -->
            <th>status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($feeStructureActivityList)) {
            $i=1;
            foreach ($feeStructureActivityList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->activity_name . " - " . $record->activity_description ?></td>
                <td><?php echo $record->fee_code . " - " . $record->fee_name ?></td>
                <td><?php echo $record->trigger ?></td>
                <td><?php if( $record->performa == '1')
                {
                  echo "Yes";
                }
                else
                {
                  echo "No";
                } 
                ?></td>
                <td><?php echo $record->currency_name ?></td>
                <td><?php echo $record->amount_local ?></td>
                <!-- <td><?php echo $record->amount_international ?></td> -->
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();


  function clearSearchForm()
      {
        window.location.reload();
      }
</script>