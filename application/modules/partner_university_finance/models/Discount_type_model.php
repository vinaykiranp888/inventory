<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Discount_type_model extends CI_Model
{
    function discountTypeList()
    {
        $this->db->select('*');
        $this->db->from('discount_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function discountTypeListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('discount_type');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDiscountType($id)
    {
        $this->db->select('*');
        $this->db->from('discount_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDiscountType($data)
    {
        $this->db->trans_start();
        $this->db->insert('discount_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDiscountType($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('discount_type', $data);
        return TRUE;
    }
}

