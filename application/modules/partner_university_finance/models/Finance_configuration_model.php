<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Finance_configuration_model extends CI_Model
{

    function getFinanceConfiguration()
    {
        $this->db->select('*');
        $this->db->from('finance_configuration');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function editFinanceConfiguration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('finance_configuration', $data);
        return TRUE;
    }
}