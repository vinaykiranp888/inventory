<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CreditNote extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('credit_note_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('credit_note.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['reference_number'] = $this->security->xss_clean($this->input->post('reference_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;

            $data['creditNoteList'] = $this->credit_note_model->creditNoteListSearch($formData);

            $data['programmeList'] = $this->credit_note_model->programmeListByStatus('1');
            
            // echo "<Pre>"; print_r($data['creditNoteList']);exit;
            

            $this->global['pageTitle'] = 'Inventory Management : Credit Note List';
            $this->loadViews("credit_note/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('credit_note.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_session = $this->session->my_session_id;

                $ratio = $this->security->xss_clean($this->input->post('ratio'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_sponser = $this->security->xss_clean($this->input->post('id_sponser'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_invoice = $this->security->xss_clean($this->input->post('id_invoice'));
                $id_type = $this->security->xss_clean($this->input->post('id_type'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));

                $generated_number = $this->credit_note_model->generateCreditNote();
                $data = array(
                    'reference_number' => $generated_number,
                    'ratio' => $ratio,
                    'amount' => $amount,
                    'description' => $description,
                    'type' => $type,
                    'id_program' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_sponser' => $id_sponser,
                    'id_student' => $id_student,
                    'id_invoice' => $id_invoice,
                    'id_type' => $id_type,
                    'total_amount' => $total_amount,
                    'status' => 0,
                    // 'created_by' => $
                );

                $inserted_id = $this->credit_note_model->addNewCreditNote($data);

                // echo "<Pre>"; print_r($inserted_id);exit;


                // $temp_details = $this->credit_note_model->getTempCreditNoteDetails($id_session);
                //  for($i=0;$i<count($temp_details);$i++)
                //  {
                //     $id_main_invoice = $temp_details[$i]->id_main_invoice;
                //     $invoice_amount = $temp_details[$i]->invoice_amount;
                //     $credit_note_amount = $temp_details[$i]->credit_note_amount;

                //      $detailsData = array(
                //         'id_credit_note' => $inserted_id,
                //         'id_main_invoice' => $id_main_invoice,
                //         'invoice_amount' => $invoice_amount,
                //         'credit_note_amount' => $credit_note_amount,
                //     );
                //     //print_r($details);exit;
                //     $result = $this->credit_note_model->addNewCreditNoteDetails($detailsData);
                //  }

                // $this->credit_note_model->deleteTempDataBySession($id_session);

                redirect('/finance/creditNote/list');
            }

            $data['programmeList'] = $this->credit_note_model->programmeListByStatus('1');
            $data['intakeList'] = $this->credit_note_model->intakeListByStatus('1');
            $data['sponserList'] = $this->credit_note_model->sponserListByStatus('1');
            $data['creditNoteTypeList'] = $this->credit_note_model->creditNoteTypeListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Credit Note Details';
            $this->loadViews("credit_note/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('credit_note.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/creditNote/list');
            }
            if($this->input->post())
            {

                redirect('/finance/creditNote/list');
            }

            $data['creditNote'] = $this->credit_note_model->getCreditNote($id);

            $data['invoiceDetails'] = $this->credit_note_model->getInvoice($data['creditNote']->id_invoice);

            if($data['creditNote']->type == 'CORPORATE')
            {
                $data['companyDetails'] = $this->credit_note_model->getCompanyDetails($data['creditNote']->id_student);
            }
            elseif($data['creditNote']->type == 'Student')
            {
                $data['studentDetails'] = $this->credit_note_model->getStudentData($data['creditNote']->id_student);
            }
            $data['programmeList'] = $this->credit_note_model->programmeListByStatus('1');
            $data['studentList'] = $this->credit_note_model->studentList();
            $data['creditNoteTypeList'] = $this->credit_note_model->creditNoteTypeListByStatus('1');

            // echo "<Pre>";print_r($data['creditNote']);exit();

            $this->global['pageTitle'] = 'Inventory Management : View Credit Note Details';
            $this->loadViews("credit_note/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('credit_note.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['reference_number'] = $this->security->xss_clean($this->input->post('reference_number'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;

            $data['creditNoteList'] = $this->credit_note_model->creditNoteListSearch($formData);

            $data['programmeList'] = $this->credit_note_model->programmeListByStatus('1');
            
            // echo "<Pre>"; print_r($data['creditNoteList']);exit;
            

            $this->global['pageTitle'] = 'Inventory Management : Credit Note List';
            $this->loadViews("credit_note/approval_list", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('credit_note.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/creditNote/approvalList');
            }
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'status' => $status
                );

                $inserted_id = $this->credit_note_model->editCreditNote($data,$id);

                redirect('/finance/creditNote/approvalList');
            }

            $data['creditNote'] = $this->credit_note_model->getCreditNote($id);
            $data['invoiceDetails'] = $this->credit_note_model->getInvoice($data['creditNote']->id_invoice);

            if($data['creditNote']->type == 'CORPORATE')
            {
                $data['companyDetails'] = $this->credit_note_model->getCompanyDetails($data['creditNote']->id_student);
            }
            elseif($data['creditNote']->type == 'Student')
            {
                $data['studentDetails'] = $this->credit_note_model->getStudentData($data['creditNote']->id_student);
            }
            $data['programmeList'] = $this->credit_note_model->programmeListByStatus('1');
            $data['studentList'] = $this->credit_note_model->studentList();
            $data['creditNoteTypeList'] = $this->credit_note_model->creditNoteTypeListByStatus('1');

            // echo "<Pre>";print_r($data['creditNote']);exit();

            $this->global['pageTitle'] = 'Inventory Management : View Credit Note Details';
            $this->loadViews("credit_note/view", $this->global, $data, NULL);
        }
    }


    function getStudentByProgrammeId($id)
     {       
            // print_r($id);exit;
            $results = $this->credit_note_model->getStudentByProgrammeId($id);
            $programme_data = $this->credit_note_model->getProgrammeById($id);

            // echo "<Pre>"; print_r($programme_data);exit;
            $programme_name = $programme_data->name;
            $programme_code = $programme_data->code;
            $total_cr_hrs = $programme_data->total_cr_hrs;
            $graduate_studies = $programme_data->graduate_studies;
            $foundation = $programme_data->foundation;


            

            $table="

            <script type='text/javascript'>
                $('select').select2();
            </script>

            <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $full_name = $results[$i]->full_name;
            $nric = $results[$i]->nric;
            $table.="<option value=".$id.">".$nric . " - " .$full_name.
                    "</option>";

            }
            $table.="</select>";

            $view  = "
            <table border='1px' style='width: 100%'>
                <tr>
                    <td colspan='4'><h5 style='text-align: center;'>Programme Details</h5></td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Programme Name</th>
                    <td style='text-align: center;'>$programme_name</td>
                    <th style='text-align: center;'>Programme Code</th>
                    <td style='text-align: center;'>$programme_code</td>
                </tr>
                <tr>
                    <th style='text-align: center;'>Total Credit Hours</th>
                    <td style='text-align: center;'>$total_cr_hrs</td>
                    <th style='text-align: center;'>Graduate Studies</th>
                    <td style='text-align: center;'>$graduate_studies</td>
                </tr>

            </table>
            <br>
            <br>
            ";

            // $d['table'] = $table;
            // $d['view'] = $view;

            echo $table;
            exit;
    }

    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
          
            $invoice_data = $this->credit_note_model->getInvoicesByStudentId($id);
            // echo "<Pre>";print_r($invoice_data);exit;
            $table = '';
            $table .= $this->showInvoices($invoice_data);

            // if(!$invoice_data)
            // {
            //     $table .= "
            //     <br>
            //     <br>
            //     <div class='custom-table'>
            //         <table align='center' class='table' id='list-table'>
            //           <tr>
            //             <h3 style='text-align: center;'>No Balance Invoices Available For This Student</h3>
            //         </tr>
            //         </table>
            //     </div>
            //     <br>
            //     <br>";
            //     // echo "<Pre>";print_r("No Data");exit;
            // }
            // else
            // {
            //     $table .= "
            //     <h3>Receipt Details</h3>
            //     <div class='custom-table'>
            //         <table class='table' id='list-table'>
            //           <tr>
            //             <th>Sl. No</th>
            //             <th>Invoice Number</th>
            //             <th>Total Amount</th>
            //             <th>Balance Amount</th>
            //             <th>Credit Note Amount</th>
            //         </tr>";


            //     for($i=0;$i<count($invoice_data);$i++)
            //         {
            //             $id = $invoice_data[$i]->id;
            //             $invoice_number = $invoice_data[$i]->invoice_number;
            //             $total_amount = $invoice_data[$i]->total_amount;
            //             $balance_amount = $invoice_data[$i]->balance_amount;
            //             $paid_amount = $invoice_data[$i]->paid_amount;
            //             $j=$i+1;
            //             $table .= "
            //         <tr>
            //             <td>$j
            //             <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
            //             <td>$invoice_number</td>
            //             <td>$total_amount</td>
            //             <td>$balance_amount</td>
            //             <td style='text-align: center;'>
            //             <div class='form-group'>
            //                 <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
            //             </div>
            //             </td>
            //         </tr>";
            //         }
                            
            //     $table .= "
            //     </table>";
            // }


            echo $table;
            exit;
    }








    function getStudentByProgram()
    {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $type = $formData['type'];
            switch ($type)
            {
                case 'CORPORATE':

                    $table = $this->getCorporateList($formData);

                    break;

                case 'Student':

                    $table = $this->getStudentList($formData);
                    
                    break;


                default:
                    # code...
                    break;
            }

            echo $table;
            exit;            
    }

    function getStudentList($data)
    {
        $data = $this->credit_note_model->getStudentListByData($data);
                // echo "<Pre>";print_r($data);exit();

        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_student' id='id_student' class='form-control' onchange='getInvoicesByData()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $data[$i]->id;
            $nric = $data[$i]->nric;
            $full_name = $data[$i]->full_name;

            $table.="<option value=".$id.">".$nric . " - " . $full_name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function getCorporateList($data)
    {
        $data = $this->credit_note_model->getCorporateListByData($data);
                // echo "<Pre>";print_r($data);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_student' id='id_student' class='form-control' onchange='getInvoicesByData()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $data[$i]->id;
            $registration_number = $data[$i]->registration_number;
            $name = $data[$i]->name;

            $table.="<option value=".$id.">".$registration_number . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getInvoicesByData()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));

        $data = $this->credit_note_model->getInvoicesByData($formData);
                // echo "<Pre>";print_r($data);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_invoice' id='id_invoice' class='form-control' onchange='getInvoiceByInvoiceId(this.value)'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $data[$i]->id;
            $invoice_number = $data[$i]->invoice_number;

            $table.="<option value=".$id.">".$invoice_number.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getApplicantByApplicantId($id)
    {
         
            $table="";
            $invoice_data = $this->credit_note_model->getInvoicesByApplicantId($id);
            // echo "<Pre>";print_r($invoice_data);exit;
            $table .= $this->showInvoices($invoice_data);
            // if(!$invoice_data)
            // {


            //     $table .= "
            //     <br>
            //     <br>
            //     <div class='custom-table'>
            //         <table align='center' class='table' id='list-table'>
            //           <tr>
            //             <h3 style='text-align: center;'>No Balance Invoices Available For This Applicant</h3>
            //         </tr>
            //         </table>
            //     </div>
            //     <br>
            //     <br>";
            //     // echo "<Pre>";print_r("No Data");exit;
            // }
            // else
            // {
            //     $table .= "
            //     <h3>Receipt Details</h3>
            //     <div class='custom-table'>
            //         <table class='table' id='list-table'>
            //           <tr>
            //             <th>Sl. No</th>
            //             <th>Invoice Number</th>
            //             <th>Total Amount</th>
            //             <th>Balance Amount</th>
            //             <th>Credot Note</th>
            //         </tr>";


            //     for($i=0;$i<count($invoice_data);$i++)
            //         {
            //             $id = $invoice_data[$i]->id;
            //             $invoice_number = $invoice_data[$i]->invoice_number;
            //             $total_amount = $invoice_data[$i]->total_amount;
            //             $balance_amount = $invoice_data[$i]->balance_amount;
            //             $paid_amount = $invoice_data[$i]->paid_amount;
            //             $j=$i+1;
            //             $table .= "
            //         <tr>
            //             <td>$j
            //             <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
            //             <td>$invoice_number</td>
            //             <td>$total_amount</td>
            //             <td>$balance_amount</td>
            //             <td style='text-align: center;'>
            //             <div class='form-group'>
            //                 <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
            //             </div>
            //             </td>
            //         </tr>";
            //         }
                            
            //     $table .= "
            //     </table>";
            // }


            echo $table;
            exit;
    }

    function showInvoices($data)
    {
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Invoice <span class='error-text'>*</span></label>
                <select name='id_invoice' id='id_invoice' class='form-control' onchange='getInvoiceByInvoiceId(this.value)'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $invoice_number = $data[$i]->invoice_number;
                $invoice_total = $data[$i]->invoice_total;

                $table.="<option value=".$id.">".$invoice_number. " - " . $invoice_total . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }
    


    function getStudentBySponser()
     {       
            // print_r($id);exit;
        $formData = $this->security->xss_clean($this->input->post('formData'));
            // echo "<Pre>"; print_r($formData);exit;

        $id_sponser = $formData['id_sponser'];
        $table = $this->getStudentBySponserId($id_sponser);
        echo $table;
        exit;   
    }

    function getStudentBySponserId($id_sponser)
    {
        $data = $this->credit_note_model->getStudentBySponser($id_sponser);
                // echo "<Pre>";print_r($data);exit();
        
            $table="
                <script type='text/javascript'>
                    $('select').select2();
                </script>

                 <div class='col-sm-4'>
                    <div class='form-group'>
                    <label>Student <span class='error-text'>*</span></label>
                <select name='id_student' id='id_student' class='form-control'  onchange='getStudentByStudentIdNSponser()'>";
                $table.="<option value=''>Select</option>";

                for($i=0;$i<count($data);$i++)
                {

                // $id = $results[$i]->id_procurement_category;
                $id = $data[$i]->id;
                $nric = $data[$i]->nric;
                $full_name = $data[$i]->full_name;

                $table.="<option value=".$id.">".$nric. " - " . $full_name . 
                        "</option>";

                }
                $table.="</select>
                </div>
                </div>
                ";

                echo $table;
    }

    function getStudentByStudentIdNSponser()
    {
        $formData = $this->security->xss_clean($this->input->post('formData'));

            // echo "<Pre>"; print_r($formData);exit;

        $id = $formData['id_student'];
        $id_sponser = $formData['id_sponser'];
        $type = $formData['type'];


            $invoice_data = $this->credit_note_model->getInvoicesByStudentIdNSponser($id);
            $table = '';
            // echo "<Pre>";print_r($invoice_data);exit;
            $table .= $this->showInvoices($invoice_data);

            // if(!$invoice_data)
            // {
            //     $table .= "
            //     <br>
            //     <br>
            //     <div class='custom-table'>
            //         <table align='center' class='table' id='list-table'>
            //           <tr>
            //             <h3 style='text-align: center;'>No Balance Invoices Available For This Student</h3>
            //         </tr>
            //         </table>
            //     </div>
            //     <br>
            //     <br>";
            //     // echo "<Pre>";print_r("No Data");exit;
            // }
            // else
            // {
            //     $table .= "
            //     <h3>Receipt Details</h3>
            //     <div class='custom-table'>
            //         <table class='table' id='list-table'>
            //           <tr>
            //             <th>Sl. No</th>
            //             <th>Invoice Number</th>
            //             <th>Total Amount</th>
            //             <th>Balance Amount</th>
            //             <th>Credit Note Amount</th>
            //         </tr>";


            //     for($i=0;$i<count($invoice_data);$i++)
            //         {
            //             $id = $invoice_data[$i]->id;
            //             $invoice_number = $invoice_data[$i]->invoice_number;
            //             $total_amount = $invoice_data[$i]->total_amount;
            //             $balance_amount = $invoice_data[$i]->balance_amount;
            //             $paid_amount = $invoice_data[$i]->paid_amount;
            //             $j=$i+1;
            //             $table .= "
            //         <tr>
            //             <td>$j
            //             <input type='number' hidden='hidden' readonly='readonly' id='invoice_id[]' name='invoice_id[]' value='$id'>
            //             <td>$invoice_number</td>
            //             <td>$total_amount</td>
            //             <td>$balance_amount</td>
            //             <td style='text-align: center;'>
            //             <div class='form-group'>
            //                 <input type='number' class='form-control' id='payable_amount[]' name='payable_amount[]' >
            //             </div>
            //             </td>
            //         </tr>";
            //         }
                            
            //     $table .= "
            //     </table>";
            // }


            echo $table;
            exit;
    }

    function getInvoiceByInvoiceId($id)
    {
        // print_r($id);exit;
            $invoice_data = $this->credit_note_model->getInvoice($id);
            // echo "<Pre>"; print_r($invoice_data);exit;

            $id = $invoice_data->id;
            $type = $invoice_data->type;
            $invoice_number = $invoice_data->invoice_number;
            $date_time = date('d-m-Y', strtotime($invoice_data->date_time));
            $remarks = $invoice_data->remarks;
            $total_amount = $invoice_data->total_amount;
            $invoice_total = $invoice_data->invoice_total;
            $balance_amount = $invoice_data->balance_amount;


            $table  = "

             <h4 class='sub-title'>Invoice Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                            <input type='hidden'  id='total_amount' name='total_amount' value='$invoice_total'>
                            <input type='hidden'  id='id_invoice' name='id_invoice' value='$id'>
                                <dt>Invoice Number :</dt>
                                <dd>$invoice_number</dd>
                            </dl>
                            <dl>
                                <dt>Invoice Date :</dt>
                                <dd>$date_time</dd>
                            </dl>
                            <dl>
                                <dt>Invoice Type :</dt>
                                <dd>$type</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Invoice Total :</dt>
                                <dd>
                                    $invoice_total
                                </dd>
                            </dl>
                            <dl>
                                <dt>Payable Amount :</dt>
                                <dd>$total_amount</dd>
                            </dl>
                            <dl>
                                <dt>Balance Amount</dt>
                                <dd>$balance_amount</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
                <br>";

        echo $table;exit();

    }


    function generateCreditNote($id)
    {
        // To Get Mpdf Library
        $this->getMpdfLibrary();

        // print_r($base_url);exit;            
        $mpdf=new \Mpdf\Mpdf();

        $currentDate = date('d-m-Y');
        $currentTime = date('h:i:s a');
        $currentDateTime = date('d_m_Y_His');

        $organisationDetails = $this->credit_note_model->getOrganisation();
        

        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.svg";

        if($organisationDetails->image != '')
        {
            $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        }




        $creditNote = $this->credit_note_model->getCreditNote($id);

        $invoiceDetails = $this->credit_note_model->getInvoice($creditNote->id_invoice);

        if($creditNote->type == 'CORPORATE')
        {
            $companyDetails = $this->credit_note_model->getCompanyDetails($creditNote->id_student);

            $invoice_generation_name = $companyDetails->name;
            $invoice_generation_nric = $companyDetails->registration_number;

        }
        elseif($creditNote->type == 'Student')
        {
            $studentDetails = $this->credit_note_model->getStudentData($creditNote->id_student);

            $invoice_generation_name = $studentDetails->full_name;
            $invoice_generation_nric = $studentDetails->nric;
        }


        // echo "<Pre>";print_r($creditNote);exit;


        $type = $creditNote->type;
        $reference_number = $creditNote->reference_number;
        $date_time = $creditNote->created_dt_tm;
        $programme_code = $creditNote->programme_code;
        $programme_name = $creditNote->programme_name;
        $description = $creditNote->description;
        $cn_amount = $creditNote->amount;

        if($date_time)
        {
            $date_time = date('d-m-Y', strtotime($date_time));
        }


        
        $file_data = "";

        $file_data.="<table align='center' width='100%'>
            <tr>
                <td style='text-align: left;font-size:30px;'><b>CREDIT NOTE</b></td>
                <td style='text-align: center' width='30%' ></td>
                <td style='text-align: right' width='40%' ><img src='$signature' width='180px' /></td>   
            </tr>
           
            <tr>
              <td style='text-align: center' width='100%'  colspan='3'> <br/><br/><br/></td>
            </tr>
        </table>";



        $file_data = $file_data ."

            <table width='100%' style='font-size:16px;'>
            <tr>
             <td>To : $invoice_generation_name </td>
             <td width='25%'></td>
             <td style='text-align:right;'>Credit No: $reference_number</td>
             <td style='text-align:right;'></td>
            </tr>
            <tr>
             <td>IC No / Passport No: $invoice_generation_nric </td>
             <td></td>
             <td style='text-align:right;'> Date: $date_time</td>
             <td style='text-align:right;'></td>
            </tr>";
            



             if($invoiceDetails)
             {

                $type = $invoiceDetails->type;
                $invoice_number = $invoiceDetails->invoice_number;
                $invoice_date_time = $invoiceDetails->date_time;
                $remarks = $invoiceDetails->remarks;
                $currency = $invoiceDetails->currency_name;
                $invoice_total_amount = $invoiceDetails->total_amount;
                $invoice_total = $invoiceDetails->invoice_total;
                $balance_amount = $invoiceDetails->balance_amount;
                $paid_amount = $invoiceDetails->paid_amount;

                if($invoice_date_time)
                {
                    $invoice_date_time = date('d-m-Y', strtotime($invoice_date_time));
                }

                $amount_c = $cn_amount;
                $cn_amount = number_format($cn_amount, 2, '.', ',');
                $amount_word = $this->getAmountWordings($amount_c);

                $amount_word = ucwords($amount_word);


                $file_data = $file_data ."
                <tr>
             <td>Invoice No: $invoice_number </td>
             <td></td>
             <td style='text-align:right;'></td>
             <td style='text-align:right;'></td>
            </tr>
             </table>



             <br/><br/>

            <table width='100%' height='50%'  style='margin-top:30px;border-collapse: collapse;padding:10px 10px;height:75%;font-size:16px;' border='1'>
              <tr>
               <th style='text-align:center;line-height:30px;'><b>No</b></th>
               <th style='text-align:center;'><b>DESCRIPTION</b></th>
               <th style='text-align:center;'><b>UNIT PRICE(RM)</b></th>
               <th style='text-align:center;'><b>TOTAL (RM)</b></th>
              </tr>
              <tr>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;text-align:center;'>$programme_code - $programme_name </td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
              </tr>
              <tr>
               <td style='padding-top:20px;padding-bottom:15px;'>1. </td>
               <td style='padding-top:20px;padding-bottom:15px;text-align:center;'>$description </td>
               <td style='padding-top:20px;padding-bottom:15px;'>$cn_amount</td>
               <td style='padding-top:20px;padding-bottom:15px;'></td>
              </tr>
              <tr>
               <td colspan='3' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>GRAND TOTAL</b></td>
               <td style='text-align:right;padding-top:20px;padding-bottom:10px;'><b>$cn_amount</b></td>
              </tr>
              <tr>
               <td colspan='4' style='text-align:center;padding-top:20px;padding-bottom:10px;'><b>$currency : $amount_word</b></td>
              </tr>
              <tr>
                <td colspan='4'  style='text-align:left;'><font size='3'>Issued by:<br>Finance & Accounts Department</font></td>
              </tr>
              <tr>
                <td colspan='4'  style='text-align:left;'><font size='3'>This is auto generated Credit Note. No signature is required. </font></td>
              </tr>
            </table>

               ";

        // echo "<Pre>";print_r($file_data);exit;

             }


    //     $bankDetails = $this->credit_note_model->getBankRegistration();


    //     if($bankDetails && $organisationDetails)
    //     {
    //         $bank_name = $bankDetails->name;
    //         $bank_code = $bankDetails->code;
    //         $account_no = $bankDetails->account_no;
    //         $state = $bankDetails->state;
    //         $country = $bankDetails->country;
    //         $address = $bankDetails->address;
    //         $city = $bankDetails->city;
    //         $zipcode = $bankDetails->zipcode;
            

    //         $organisation_name = $organisationDetails->name;




    //          $file_data = $file_data ."<br/><br/>
    //     <p>1. All cheque should be crossed and make payable to:: </p>
    // <table align='center' width='100%' style='font-size:16px;'>
    //   <tr>
    //         <td style='text-align: left' width='30%' valign='top'>PAYEE</td>
    //         <td style='text-align: center' width='5%' valign='top'>:</td>
    //         <td style='text-align: left' width='65%'>$organisation_name</td>
    //   </tr>

    //   <tr>
    //         <td style='text-align: left' width='30%' valign='top'>BANK</td>
    //         <td style='text-align: center' width='5%' valign='top'>:</td>
    //         <td style='text-align: left' width='65%'>$bank_name</td>
    //   </tr>

    //   <tr>
    //         <td style='text-align: left' width='30%' valign='top'>ADDRESS</td>
    //         <td style='text-align: center' width='5%' valign='top'>:</td>
    //         <td style='text-align: left' width='65%'>$address , $city , $state , $country - $zipcode</td>
    //   </tr>

    //   <tr>
    //         <td style='text-align: left' width='30%' valign='top'>ACCOUNT NO</td>
    //         <td style='text-align: center' width='5%' valign='top'>:</td>
    //         <td style='text-align: left' width='65%'>$account_no</td>
    //   </tr>
    //   <tr>
    //         <td style='text-align: left' width='30%' valign='top'>SWIFT CODE</td>
    //         <td style='text-align: center' width='5%' valign='top'>:</td>
    //         <td style='text-align: left' width='65%'>$bank_code</td>
    //   </tr>

      
    // </table>
    // <p> 2. This is auto generated Receipt. No signature is required. </p>
    //   ";


    //     }


      $file_data = $file_data ."
    <pagebreak>";

    
        // echo "<Pre>";print_r($file_data);exit;


            // $mpdf->SetFooter('<div>Inventory Management</div>');
            // echo $file_data;exit;

            $mpdf->WriteHTML($file_data);

            $mpdf->Output($type . '_CN_'.$reference_number.'_'.$currentDateTime.'.pdf', 'D');
            exit;
    }
}
