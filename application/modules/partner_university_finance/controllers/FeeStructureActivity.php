<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeStructureActivity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_structure_activity_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('fee_structure_activity.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_program'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_activity'] = $this->security->xss_clean($this->input->post('id_activity'));
            $formData['id_fee_setup'] = $this->security->xss_clean($this->input->post('id_fee_setup'));
            $formData['trigger'] = $this->security->xss_clean($this->input->post('trigger'));

            $data['searchParam'] = $formData;
            
            $data['feeStructureActivityList'] = $this->fee_structure_activity_model->feeStructureActivityListSearch($formData);

            $data['feeSetupList'] = $this->fee_structure_activity_model->feeSetupListByStatus('1');
            $data['activityList'] = $this->fee_structure_activity_model->activityListByStatus('1');
            $data['programList'] = $this->fee_structure_activity_model->programListByStatus('1');
                // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Fee Structure Activity List';
            $this->loadViews("fee_structure_activity/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('fee_structure_activity.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_activity = $this->security->xss_clean($this->input->post('id_activity'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $trigger = $this->security->xss_clean($this->input->post('trigger'));
                $id_fee_setup = $this->security->xss_clean($this->input->post('id_fee_setup'));
                $performa = $this->security->xss_clean($this->input->post('performa'));
                $amount_local = $this->security->xss_clean($this->input->post('amount_local'));
                $amount_international = $this->security->xss_clean($this->input->post('amount_international'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_program' => $id_program,
                    'id_activity' => $id_activity,
                    'id_currency' => $id_currency,
                    'trigger' => $trigger,
                    'id_fee_setup' => $id_fee_setup,
                    'performa' => $performa,
                    'amount_local' => $amount_local,
                    'amount_international' => $amount_international,
                    'status' => $status
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->fee_structure_activity_model->addNewFeeStructureActivity($data);
                redirect('/finance/feeStructureActivity/list');
            }
            
            $data['feeSetupList'] = $this->fee_structure_activity_model->feeSetupListByStatus('1');
            $data['activityList'] = $this->fee_structure_activity_model->activityListByStatus('1');
            $data['programList'] = $this->fee_structure_activity_model->programListByStatus('1');
            $data['currencyList'] = $this->fee_structure_activity_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Fee Structure Activity';
            $this->loadViews("fee_structure_activity/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('fee_structure_activity.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeStructureActivity/list');
            }
            if($this->input->post())
            {
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_activity = $this->security->xss_clean($this->input->post('id_activity'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $trigger = $this->security->xss_clean($this->input->post('trigger'));
                $id_fee_setup = $this->security->xss_clean($this->input->post('id_fee_setup'));
                $performa = $this->security->xss_clean($this->input->post('performa'));
                $amount_local = $this->security->xss_clean($this->input->post('amount_local'));
                $amount_international = $this->security->xss_clean($this->input->post('amount_international'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_program' => $id_program,
                    'id_activity' => $id_activity,
                    'id_currency' => $id_currency,
                    'trigger' => $trigger,
                    'id_fee_setup' => $id_fee_setup,
                    'performa' => $performa,
                    'amount_local' => $amount_local,
                    'amount_international' => $amount_international,
                    'status' => $status
                );

                $result = $this->fee_structure_activity_model->editFeeStructureActivity($data,$id);
                redirect('/finance/feeStructureActivity/list');
            }

            $data['feeStructureActivity'] = $this->fee_structure_activity_model->getFeeStructureActivity($id);
            $data['feeSetupList'] = $this->fee_structure_activity_model->feeSetupListByStatus('1');
            $data['activityList'] = $this->fee_structure_activity_model->activityListByStatus('1');
            $data['programList'] = $this->fee_structure_activity_model->programListByStatus('1');
            $data['currencyList'] = $this->fee_structure_activity_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Edit Fee Structure Activity';
            $this->loadViews("fee_structure_activity/edit", $this->global, $data, NULL);
        }
    }

    function addFeeStructureActivity()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();

        foreach ($tempData['id_program'] as $id_program)
        {
            $data = array(
                    'id_program' => $id_program,
                    'id_activity' => $tempData['id_activity'],
                    'id_currency' => $tempData['id_currency'],
                    'trigger' => $tempData['trigger'],
                    'id_fee_setup' => $tempData['id_fee_setup'],
                    'performa' => $tempData['performa'],
                    'amount_local' => $tempData['amount_local'],
                    'amount_international' => $tempData['amount_international'],
                    'status' => $tempData['status']
                );
                // echo "<Pre>"; print_r($data);exit;

                $result = $this->fee_structure_activity_model->addNewFeeStructureActivity($data);
        }

        // if($result)
        // {
            echo "success";exit();
        // }

        // redirect('/finance/feeStructureActivity/list');
    }
}
