<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Discount extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('discount_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('discount.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_discount_type'] = $this->security->xss_clean($this->input->post('id_discount_type'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $data['searchParam'] = $formData;

            $data['discountList'] = $this->discount_model->discountListSearch($formData);

            $data['discountTypeList'] = $this->discount_model->discountTypeListByStatus('1');
            $data['intakeList'] = $this->discount_model->intakeListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Discount List';
            $this->loadViews("discount/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('discount.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_discount_type = $this->security->xss_clean($this->input->post('id_discount_type'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $amount_type = $this->security->xss_clean($this->input->post('amount_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_discount_type' => $id_discount_type,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'amount_type' => $amount_type,
                    'amount' => $amount,
                    'id_intake' => $id_intake,
                    'currency' => $id_currency,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->discount_model->addNewDiscount($data);
                redirect('/finance/discount/list');
            }

            $data['discountTypeList'] = $this->discount_model->discountTypeListByStatus('1');
            $data['intakeList'] = $this->discount_model->intakeListByStatus('1');
            $data['currencyList'] = $this->discount_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Discount Type';
            $this->loadViews("discount/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('discount.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/discount/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;


                $id_discount_type = $this->security->xss_clean($this->input->post('id_discount_type'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $amount_type = $this->security->xss_clean($this->input->post('amount_type'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_discount_type' => $id_discount_type,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'amount_type' => $amount_type,
                    'amount' => $amount,
                    'id_intake' => $id_intake,
                    'currency' => $id_currency,
                    'status' => $status
                );

                $result = $this->discount_model->editDiscount($data,$id);
                redirect('/finance/discount/list');
            }
            
            $data['discount'] = $this->discount_model->getDiscount($id);

            $data['discountTypeList'] = $this->discount_model->discountTypeListByStatus('1');
            $data['intakeList'] = $this->discount_model->intakeListByStatus('1');
            $data['currencyList'] = $this->discount_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Edit Discount';
            $this->loadViews("discount/edit", $this->global, $data, NULL);
        }
    }
}
