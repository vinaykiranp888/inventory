<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ReceiptPaidDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('receipt_paid_details_model');
        $this->load->model('payment_type_model');
        $this->load->model('receipt_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('receipt_paid_details.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['receiptPaidDetailsList'] = $this->receipt_paid_details_model->receiptPaidDetailsList();
            $this->global['pageTitle'] = 'Inventory Management : Receipt Paid Details List';
            $this->loadViews("receipt_paid_details/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('receipt_paid_details.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_receipt = $this->security->xss_clean($this->input->post('id_receipt'));
                $id_payment_type = $this->security->xss_clean($this->input->post('id_payment_type'));
                $paid_amount = $this->security->xss_clean($this->input->post('paid_amount'));

                $data = array(
                    'id_receipt' => $id_receipt,
                    'id_payment_type' => $id_payment_type,
                    'paid_amount' => $paid_amount
                );
                $inserted_id = $this->receipt_paid_details_model->addNewReceiptPaidDetails($data);
                redirect('/finance/receiptPaidDetails/list');
            }
            $data['receiptList'] = $this->receipt_model->receiptList();
            $data['paymentTypeList'] = $this->payment_type_model->paymentTypeList();
            $this->global['pageTitle'] = 'Inventory Management : Add Receipt Paid Details';
            $this->loadViews("receipt_paid_details/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('receipt_paid_details.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/receipt_paid_details/list');
            }
            if($this->input->post())
            {
               $receipt_paid_details_number = $this->security->xss_clean($this->input->post('receipt_paid_details_number'));
                $receipt_paid_details_amount = $this->security->xss_clean($this->input->post('receipt_paid_details_amount'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                $approval_status = $this->security->xss_clean($this->input->post('approval_status'));

                $data = array(
                    'receipt_paid_details_number' => $receipt_paid_details_number,
                    'receipt_paid_details_amount' => $receipt_paid_details_amount,
                    'remarks' => $remarks,
                    'approval_status' => $approval_status
                );
                //print_r($data);exit;
                $result = $this->receipt_paid_details_model->editReceiptPaidDetails($data,$id);
                redirect('/finance/receipt_paid_details/list');
            }
            // $data['studentList'] = $this->receipt_paid_details_model->studentList();
            $data['receipt_paid_details'] = $this->receipt_paid_details_model->getReceiptPaidDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Receipt Paid Details';
            $this->loadViews("receipt_paid_details/edit", $this->global, $data, NULL);
        }
    }
}
