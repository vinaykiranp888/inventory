<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FinanceConfiguration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('finance_configuration_model');
        $this->isLoggedIn();
    }

    function edit()
    {
        if ($this->checkAccess('finance_configuration.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $formData = $this->input->post();

                // if($_FILES['image'])
                // {
                // // echo "<Pre>"; print_r($_FILES['image']);exit;

                //     $certificate_name = $_FILES['image']['name'];
                //     $certificate_size = $_FILES['image']['size'];
                //     $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                //     // echo "<Pre>"; print_r($certificate_tmp);exit();

                //     $certificate_ext=explode('.',$certificate_name);
                //     $certificate_ext=end($certificate_ext);
                //     $certificate_ext=strtolower($certificate_ext);


                //     $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                //     $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // }


                // echo "<Pre>"; print_r($image_file);exit;

                // $filename = $_FILES['image']['name']; //Command to assign the value
                $tax_sst = $this->security->xss_clean($this->input->post('tax_sst'));
                $sst_registration_number = $this->security->xss_clean($this->input->post('sst_registration_number'));
                $tax_code = $this->security->xss_clean($this->input->post('tax_code'));
                $id_finance_configuration = $this->security->xss_clean($this->input->post('id_finance_configuration'));

            
                $data = array(
                    'tax_sst' => $tax_sst,
                    'sst_registration_number' => $sst_registration_number,
                    'tax_code' => $tax_code
                );

                // if($image_file)
                // {
                //     $data['image'] = $image_file;
                // }

                $result = $this->finance_configuration_model->editFinanceConfiguration($data,$id_finance_configuration);
                redirect('/finance/financeConfiguration/edit');
            }
            
            $data['financeConfiguration'] = $this->finance_configuration_model->getFinanceConfiguration();
            // echo "<Pre>";print_r($data);exit();
            $data['id_finance_configuration'] = $data['financeConfiguration']->id;

            // getOrganisationBranchProgram
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Finance Configuration';
            $this->loadViews("finance_configuration/edit", $this->global, $data, NULL);
        }
    }
}