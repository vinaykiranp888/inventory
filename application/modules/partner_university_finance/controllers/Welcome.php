<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Welcome extends BaseController
{
	 public function __construct()
    {
        parent::__construct();
        $this->isPartnerUniversityLoggedIn();
    }

    public function index()
    {
        
        $this->global['pageTitle'] = 'Partner University Portal : Welcome To Curriculum Management';        
        $this->loadViews("includes/welcome", $this->global, NULL , NULL);
    }   

    
    public function comingsoon()
    {
        $this->global['pageTitle'] = 'Partner University Portal : Coming Soon';        
        $this->loadViews("includes/comingsoon", $this->global, NULL , NULL);
    }
}