<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Product_type_model extends CI_Model
{
    function productTypeList()
    {
        $this->db->select('*');
        $this->db->from('product_type');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getProductTypeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


      function getParentProduct()
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where("id_parent_product='99999'");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getChildForParent($id) {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where("id_parent_product",$id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function addNewProductType($data)
    {
        $this->db->trans_start();
        $this->db->insert('product_type', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProductTypeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('product_type', $data);
        return TRUE;
    }
    
    // function deleteActivityDetails($id, $date)
    // {
    //     $this->db->where('id', $countryId);
    //     $this->db->update('academic_year', $countryInfo);
    //     return $this->db->affected_rows();
    // }
}

