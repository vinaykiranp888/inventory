<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            
          <li><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          

          <?php
          if ($programmeDetails->send_for_approval == '0')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/sendForApproval/<?php echo $id_programme;?>">Send For Approval</a></li>
            
           <?php
          }
          else
          {
            ?>
        
            <li><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <?php
          }
          ?>
          
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Modules To Courses Details</h4>

             
                <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Courses <span class='error-text'>*</span></label>
                       <select name="id_child_programme" id="id_child_programme" class="form-control" required>
                          <option value="">Select</option>
                          <?php
                             if (!empty($programmeList))
                             {
                                 foreach ($programmeList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"
                            <?php 
                            if($record->id == $programModule->id_child_programme)
                            {
                              echo 'selected';
                            }
                            ?>><?php echo $record->code . " - " . $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                 </div>

                </div>

                 <div class="button-block clearfix">
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                       <?php
                    if($id_module != NULL)
                    {
                      ?>
                      <a href="<?php echo '../../modules/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                   </div>
                 </div>


                   
         </div>
     
      </form>


      <?php

        if(!empty($programHasModules))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Modules To Courses Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Course</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($programHasModules);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $programHasModules[$i]->programme_code . " - " . $programHasModules[$i]->programme_name;?></td>
                            <td class="text-center">
                                <a href='/partner_university_prdtm/programme/modules/<?php echo $id_programme;?>/<?php echo $programHasModules[$i]->id;?>'>Edit</a> | 
                                <a onclick="deleteProgramHasModules(<?php echo $programHasModules[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

    function deleteProgramHasModules(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
         $.ajax(
            {
               url: '/partner_university_prdtm/programme/deleteProgramHasModules/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
       }
    }


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">



CKEDITOR.replace('overview',{
  width: "100%",
  height: "300px"

}); 

</script>