<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">


        <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>

          <li><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>

            <li class="active"><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            

          <!--     <?php
          if ($programmeDetails->id_programme_type == '1')
          {
          ?>
            <li><a href="/prdtm/programmeApproval/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/prdtm/programmeApproval/syllabus/<?php echo $id_programme;?>">Learning Outcomes</a></li>
            <li><a href="/prdtm/programmeApproval/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/prdtm/programmeApproval/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/prdtm/programmeApproval/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/prdtm/programmeApproval/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/prdtm/programmeApproval/award/<?php echo $id_programme;?>">Award</a></li>
            <li><a href="/prdtm/programmeApproval/discount/<?php echo $id_programme;?>">Discounts</a></li>
            
          <?php
          }
          elseif ($programmeDetails->id_programme_type == '2')
          {
          ?>

            <li><a href="/prdtm/programmeApproval/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/prdtm/programmeApproval/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/prdtm/programmeApproval/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?> -->            
        </ul>


        
      <!-- <form id="form_programme" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Product Approval Details</h4>
            
            <div class="row">
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Status <span class='error-text'>*</span></label>
                     <select name="status" id="status" class="form-control" onchange="showRejectField()">
                        <option value="">Select</option>
                        <?php
                           if (!empty($statusList))
                           {
                               foreach ($statusList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->status == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               
               <div class="col-sm-4" id="view_reject" style="display: none">
                  <div class="form-group">
                     <label>Reason <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $programmeDetails->reason; ?>">
                  </div>
               </div>

                <div class="col-sm-4" id="view_percentage" style="display: none">
                    <div class="form-group">
                        <label>Percentage <span class='error-text'>*</span></label>
                        <input type="number" id="percentage" name="percentage" class="form-control" min="1" max="100" value="<?php echo $programmeDetails->percentage; ?>">
                    </div>
                </div>
            </div>

         </div>


          <div class="button-block clearfix">
           <div class="bttn-group">
              <button type="submit" class="btn btn-primary btn-lg">Save</button>
              <a href="../list" class="btn btn-link">Back</a>
           </div>
          </div>

      </form> -->



      <div class="form-container">
        <h4 class="form-group-title">Previous Status Details</h4>

          <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>Status</th>
                     <th>Reason</th>
                     <th>Date Time</th>
                     <th>Updated By</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                  for($i=0;$i<count($programmeStatusChange);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $programmeStatusChange[$i]->status_name;?></td>
                    <td><?php echo $programmeStatusChange[$i]->reason;?></td>
                    <td><?php echo date('d-m-Y H:i:s', strtotime($programmeStatusChange[$i]->created_dt_tm));?></td>
                    <td><?php echo $programmeStatusChange[$i]->created_by;?></td>
                     </tr>
                  <?php 
              } 
              ?>
                </tbody>
            </table>
          </div>

        </div>





    <footer class="footer-wrapper">
       <p>&copy; 2019 All rights, reserved</p>
    </footer>


   </div>

</div>

<script>

  $('select').select2();


    function showRejectField()
    {
      var status = $("#status").val();

      if(status == '5')
      {
        $("#view_percentage").show();
        $("#view_reject").hide();
      }
      else
      {
        $("#view_percentage").hide();
        $("#view_reject").show();
      }
    }
   
  
   $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                status: {
                    required: true
                },
                reason: {
                    required: true
                },
                percentage: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Percentage Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   
   
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
