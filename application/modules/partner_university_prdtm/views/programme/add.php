
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Add Product</h3>
      </div>
      <form id="form_programme" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Product Details</h4>

            <div class="row">

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Type <span class='error-text'>*</span></label>
                     <select name="id_programme_type" id="id_programme_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($productTypeSetupList))
                           {
                               foreach ($productTypeSetupList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_programme_type == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Code <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>" onblur="getProgrammeCodeDuplication()">
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Name <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>" onblur="getProgrammeNameDuplication()">
                  </div>
               </div>

            </div>


            <div class="row">
               
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Student Learning Hours <span class='error-text'>*</span></label>
                     <input type="number" class="form-control" id="student_learning_hours" name="student_learning_hours" value="<?php echo $programmeDetails->student_learning_hours; ?>">
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Total Cr. Hours </label>
                     <input type="number" class="form-control" id="total_cr_hrs" name="total_cr_hrs" value="<?php echo $programmeDetails->total_cr_hrs; ?>">
                  </div>
               </div>
               
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Owner <span class='error-text'>*</span></label>
                     <select name="internal_external" id="internal_external" class="form-control" onchange="showPartner(this.value)" disabled>
                        <option value="" >Select</option>
                        <option value="External" selected>External</option>
                     </select>
                  </div>
               </div>

            </div>

            
            <div class="row">


               <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                  <div class="form-group">
                     <label>Partner Name <span class='error-text'>*</span></label>
                     <select name="id_partner_university" id="id_partner_university" class="form-control" disabled>
                        <option value="">Select</option>
                        <?php
                           if (!empty($partnerList))
                           {
                               foreach ($partnerList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($record->id == $id_partner_university)
                              {
                                  echo "selected=selected";
                              } ?>>
                           <?php echo $record->code . " - " . $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Category Type <span class='error-text'>*</span></label>
                     <select name="id_category" id="id_category" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($categoryList))
                           {
                               foreach ($categoryList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_category == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Delivery Mode <span class='error-text'>*</span></label>
                     <select name="id_delivery_mode" id="id_delivery_mode" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($deliveryModeList))
                           {
                               foreach ($deliveryModeList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_delivery_mode == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

            </div>

            
            <div class="row">


               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Competency Level <span class='error-text'>*</span></label>
                     <select name="id_study_level" id="id_study_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($studyLevelList))
                           {
                               foreach ($studyLevelList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_study_level == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

            


               <div class="col-sm-4">
                  <label>Max Duration<span class='error-text'>*</span></label>
                  <div class="row">
                     <div class="col-sm-6">
                        <input type="number" class="form-control" id="max_duration" name="max_duration" min='1' value="<?php echo $programmeDetails->max_duration; ?>">
                     </div>
                     <div class="col-sm-6">
                        <select name="duration_type" id="duration_type" class="form-control" required>
                           <option value="">Select</option>
                           <option value="Days"
                              <?php
                                 if ($programmeDetails->duration_type == 'Days')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Days</option>
                           <option value="Weeks"
                              <?php
                                 if ($programmeDetails->duration_type == 'Weeks')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Weeks</option>
                           <option value="Months"
                              <?php
                                 if ($programmeDetails->duration_type == 'Months')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Months</option>
                           <option value="Years"
                              <?php
                                 if ($programmeDetails->duration_type == 'Years')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Years</option>
                        </select>
                     </div>
                  </div>
               </div>
               
               <!-- <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Status Type <span class='error-text'>*</span></label>
                     <select name="trending" id="trending" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="HOME" <?php if ($programmeDetails->trending == 'HOME')
                           {
                               echo "selected";
                           } ?>>HOME</option>
                        <option value="TRENDING" <?php if ($programmeDetails->trending == 'TRENDING')
                           {
                               echo "selected";
                           } ?>>TRENDING</option>
                        <option value="POPULAR" <?php if ($programmeDetails->trending == 'POPULAR')
                           {
                               echo "selected";
                           } ?>>POPULAR</option>
                     </select>
                  </div>
               </div> -->


            


               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Level </label>
                     <select name="id_education_level" id="id_education_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($educationLevelList))
                           {
                               foreach ($educationLevelList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_education_level == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>


            </div>

            
            <div class="row">



               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Language <span class='error-text'>*</span></label>
                     <select name="language" id="language" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="1" <?php if ($programmeDetails->language == '1')
                           {
                               echo "selected";
                           } ?>>English</option>
                        <option value="2" <?php if ($programmeDetails->language == '2')
                           {
                               echo "selected";
                           } ?>>Bahasa Melayu</option>
                     </select>
                  </div>
               </div>


               <div class="col-sm-4">
                  <div class="form-group">
                     <p>Is Free Course <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="is_free_course" id="is_free_course" value="1" <?php if ($programmeDetails->is_free_course == '1')
                        {
                            echo "chsecked=checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="is_free_course" id="is_free_course" value="0" <?php if ($programmeDetails->is_free_course == '0')
                        {
                            echo "checked=checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
               </div>

            
              

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>FILE 
                     <span class='error-text'>*</span>
                     <?php
                        if ($programmeDetails->image != '')
                        {
                        ?>
                     <a href="<?php echo '/assets/images/' . $programmeDetails->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $programmeDetails->image; ?>)" title="<?php echo $programmeDetails->image; ?>"> View </a>
                     <?php
                        }
                        ?>
                     </label>
                     <input type="file" name="image" id="image">
                  </div>
               </div>


            </div>

            
            <div class="row">
               

               <!-- <div class="col-sm-4">
                  <div class="form-group">
                     <p>Sold Seperately <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="sold_separately" id="sold_separately" value="1" <?php if ($programmeDetails->sold_separately == '1')
                        {
                            echo "chsecked=checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="sold_separately" id="sold_separately" value="0" <?php if ($programmeDetails->sold_separately == '0')
                        {
                            echo "checked=checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
               </div> -->




            </div>

            <div class="row">

              
              <div class="col-sm-8">
                  <div class="form-group">
                     <label>Short Description <span class='error-text'>*</span></label>
                     <textarea class="form-control z-depth-1" rows="3" placeholder="Short Descripton..." name="short_description" id="short_description"><?php echo $programmeDetails->short_description; ?></textarea>
                  </div>
               </div>


            </div>

            <div class="row">


               <div class="col-sm-8">
                  <div class="form-group">
                     <label>Target Audience <span class='error-text'>*</span></label>
                     <textarea class="form-control z-depth-1" rows="3" placeholder="Target Audience..." name="target_audience" id="target_audience"><?php echo $programmeDetails->target_audience; ?></textarea>
                  </div>
               </div>


            </div>



            <div class="row">


              <!-- <div class="col-sm-4">
                  <div class="form-group">
                     <label>Status <span class='error-text'>*</span></label>
                     <select name="status" id="status" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($statusList))
                           {
                               foreach ($statusList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->status == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div> -->

         
            </div>


         </div>
      <div class="button-block clearfix">
         <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
         </div>
      </div>
   </div>
         </form>

</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">



CKEDITOR.replace('short_description',{
  width: "100%",
  height: "100px"

}); 
CKEDITOR.replace('target_audience',{
  width: "100%",
  height: "100px"

}); 
</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script>

      $('select').select2();


   $(function()
   {
       showPartner();
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
   });
   
   
   function showPartner(){
       var value = $("#internal_external").val();
       if(value=='Internal') {
            $("#partnerdropdown").hide();
   
       } else if(value=='External') {
            $("#partnerdropdown").show();
   
       }
   }

   function getProgrammeCodeDuplication()
   {
      var code = $("#code").val()

      if(code != '')
      {

        var tempPR = {};
        tempPR['code'] = code;
        tempPR['name'] = '';
        tempPR['id_programme'] = '';
        

        $.ajax(
        {
           url: '/partner_university_prdtm/programme/getProgrammeDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Code Not Allowed, Programme Already Registered With The Given Code : '+ code );
                  $("#code").val('');
              }
           }
        });
      }
   }


   function getProgrammeNameDuplication()
   {
      var name = $("#name").val()

      if(name != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = name;
        tempPR['id_programme'] = '';
        

        $.ajax(
        {
           url: '/partner_university_prdtm/programme/getProgrammeDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Programme Name Not Allowed, Programme Already Registered With The Given Name : '+ name );
                  $("#name").val('');
              }
           }
        });
      }
   }


   $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                internal_external: {
                    required: true
                },
                id_partner_university: {
                    required: true
                },
                id_category: {
                    required: true
                },
                id_category_setup: {
                    required: true
                },
                id_programme_type: {
                    required: true
                },
                max_duration: {
                    required: true
                },
                duration_type: {
                    required: true
                },
                trending: {
                    required: true
                },
                target_audience: {
                    required: true
                },
                student_learning_hours: {
                    required: true
                },
                short_description: {
                    required: true
                },
                id_delivery_mode: {
                    required: true
                },
                id_study_level: {
                    required: true
                },
                sold_separately: {
                    required: true
                },
                status: {
                    required: true
                },
                image: {
                    required: true
                },
                language: {
                    required: true
                },
                is_free_course: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                internal_external: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_category: {
                    required: "<p class='error-text'>Select Cateory Type</p>",
                },
                id_category_setup: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_programme_type: {
                    required: "<p class='error-text'>Select Product Type</p>",
                },
                max_duration: {
                    required: "<p class='error-text'>Max. Duration Required</p>",
                },
                duration_type: {
                    required: "<p class='error-text'>Select Duration Type</p>",
                },
                trending: {
                    required: "<p class='error-text'>Select Status Type</p>",
                },
                target_audience: {
                    required: "<p class='error-text'>Tagset Audience Reuired</p>",
                },
                student_learning_hours: {
                    required: "<p class='error-text'>Learning Hours Required</p>",
                },
                short_description: {
                    required: "<p class='error-text'>Short Description Required</p>",
                },
                id_delivery_mode: {
                    required: "<p class='error-text'>Select Delicery Mode</p>",
                },
                id_study_level: {
                    required: "<p class='error-text'>Select Competency Level</p>",
                },
                sold_separately: {
                    required: "<p class='error-text'>Select Sold Seperately</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                image: {
                    required: "<p class='error-text'>Select Image File</p>",
                },
                language: {
                    required: "<p class='error-text'>Select Language</p>",
                },
                is_free_course: {
                    required: "<p class='error-text'>Select Is Free Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   
   
   
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

