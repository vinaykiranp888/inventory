<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <form id="form_programme" action="" method="post" enctype="multipart/form-data">


        <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
            

          <li><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          

          <?php
          if ($programmeDetails->send_for_approval == '0')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/sendForApproval/<?php echo $id_programme;?>">Send For Approval</a></li>
            
           <?php
          }
          else
          {
            ?>
        
            <li><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <?php
          }
          ?>
          
        </ul>

        <div class="form-container">
            <h4 class="form-group-title">Discount Details</h4>
            
            <div class="row">

               <div class="col-sm-4">
                  <div class="form-group">
                     <p>Is Staff Discount <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="is_staff_discount" id="is_staff_discount" value="1" onclick="showStaffDiscountField()"
                     <?php if ($programmeDiscount->is_staff_discount == '1')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="is_staff_discount" id="is_staff_discount" value="0" onclick="hideStaffDiscountField()" <?php if ($programmeDiscount->is_staff_discount == '0')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
               </div>

               <div class="col-sm-4" id="view_staff_type" style="display: none;">
                  <div class="form-group">
                     <p>Discount Type <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="staff_discount_type" id="staff_discount_type" value="Amount" 
                     <?php if ($programmeDiscount->staff_discount_type == 'Amount')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Amount
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="staff_discount_type" id="staff_discount_type" value="Percentage" <?php if ($programmeDiscount->staff_discount_type == 'Percentage')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> Percentage
                     </label>
                  </div>
               </div>

               <div class="col-sm-4" id="view_staff_value" style="display: none;">
                  <div class="form-group">
                     <label>Value <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="staff_value" name="staff_value" value="<?php echo $programmeDiscount->staff_value; ?>">
                     <input type="hidden" class="form-control" id="id_programme_discount" name="id_programme_discount" value="<?php echo $programmeDiscount->id; ?>">

                  </div>
               </div>
              
            </div>


            <div class="row">

               <div class="col-sm-4">
                  <div class="form-group">
                     <p>Is Staff Discount <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="is_student_discount" id="is_student_discount" value="1"onclick="showStudentDiscountField()" 
                     <?php if ($programmeDiscount->is_student_discount == '1')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="is_student_discount" id="is_student_discount" value="0" onclick="hideStudentDiscountField()" <?php if ($programmeDiscount->is_student_discount == '0')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
               </div>

               <div class="col-sm-4" id="view_student_type" style="display: none;">
                  <div class="form-group">
                     <p>Discount Type <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="student_discount_type" id="student_discount_type" value="Amount" 
                     <?php if ($programmeDiscount->student_discount_type == 'Amount')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Amount
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="student_discount_type" id="student_discount_type" value="Percentage" <?php if ($programmeDiscount->student_discount_type == 'Percentage')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> Percentage
                     </label>
                  </div>
               </div>

               <div class="col-sm-4" id="view_student_value" style="display: none;">
                  <div class="form-group">
                     <label>Value <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="student_value" name="student_value" value="<?php echo $programmeDiscount->student_value; ?>">
                  </div>
               </div>
              
            </div>
   
        </div>


      <div class="button-block clearfix">
         <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            <!-- <a href="../list" class="btn btn-link">Back</a> -->
         </div>
      </div>
   </div>
  </form>

</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>


<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script type="text/javascript">



CKEDITOR.replace('short_description',{
  width: "100%",
  height: "100px"

}); 
CKEDITOR.replace('target_audience',{
  width: "100%",
  height: "100px"

}); 
</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script>

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveAcceredationData()
  {
     if($('#form_programme').valid())
      {
        $('#form_programme').submit();
      }
  }

  // checkHideShow();

  // function checkHideShow()
  // {
  //   var is_staff_discount = <?php if($programmeDiscount){ echo $programmeDiscount->is_staff_discount; } ?>;
  //   var is_student_discount = <?php if($programmeDiscount){ echo $programmeDiscount->is_student_discount; } ?>;

  //   if(is_staff_discount == '1')
  //   {
  //     showStaffDiscountField();
  //   }
  //   else
  //   {
  //     hideStaffDiscountField();
  //   }

  //   if(is_student_discount == '1')
  //   {
  //     showStudentDiscountField();
  //   }
  //   else
  //   {
  //     hideStudentDiscountField();
  //   }
  // }

  function showStaffDiscountField()
  {
      $("#view_staff_type").show();
      $("#view_staff_value").show();
  }

  function hideStaffDiscountField()
  {
      $("#view_staff_type").hide();
      $("#view_staff_value").hide();
  }

  function showStudentDiscountField()
  {
      $("#view_student_type").show();
      $("#view_student_value").show();
  }

  function hideStudentDiscountField()
  {
      $("#view_student_type").hide();
      $("#view_student_value").hide();
  }

    $(document).ready(function()
    {
      // checkHideShow();
        $("#form_programme").validate({
            rules: {
                is_staff_discount: {
                    required: true
                },
                staff_discount_type: {
                    required: true
                },
                staff_value: {
                    required: true
                },
                is_student_discount: {
                    required: true
                },
                student_discount_type: {
                    required: true
                },
                student_value: {
                    required: true
                }
            },
            messages: {
                is_staff_discount: {
                    required: "<p class='error-text'>Select Is Staff Discount</p>",
                },
                staff_discount_type: {
                    required: "<p class='error-text'>Select Staff Discount Type</p>",
                },
                staff_value: {
                    required: "<p class='error-text'>Value Required</p>",
                },
                is_student_discount: {
                    required: "<p class='error-text'>Select Is Student Discount</p>",
                },
                student_discount_type: {
                    required: "<p class='error-text'>Select Student Discount Type</p>",
                },
                student_value: {
                    required: "<p class='error-text'>Value Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });  
   
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
