<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
          

          <li><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          

          <?php
          if ($programmeDetails->send_for_approval == '0')
          {
          ?>

            <li class="active"><a href="/partner_university_prdtm/programme/sendForApproval/<?php echo $id_programme;?>">Send For Approval</a></li>
            
           <?php
          }
          else
          {
            ?>
        
            <li><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <?php
          }
          ?>

          
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Send For Approval</h4>

                  

                  <div class="row">

                      <h4> 1. Product Image 
                        <?php
                        if($image == '')
                        {
                          echo ' Not Added';
                        }
                        else
                        {
                          echo ' Added';
                        }
                        ?>
                      </h4>

                      <h4> 2. Product Fee Structure 
                      <?php
                        if($id_programme_fee_structure == '0')
                        {
                          echo ' Not Added';
                        }
                        else
                        {
                          echo ' Added';
                        }
                        ?>
                      </h4>
                      
                      <h4> 3. Product Description 
                      <?php
                        if($is_overview == '0')
                        {
                          echo ' Not Added';
                        }
                        else
                        {
                          echo ' Added';
                        }
                        ?>                          
                      </h4>

                      <h4> 4. Product Topic's 
                      <?php
                        if($is_topic == '0')
                        {
                          echo ' Not Added';
                        }
                        else
                        {
                          echo ' Added';
                        }
                        ?>    
                      </h4>
                      
                  </div>


                 <div class="button-block clearfix">
                  <div class="form-group">




                    <input type="hidden" name="send_for_approval" id="send_for_approval" value="1">

                      <button type="button" class="btn btn-primary btn-lg" onclick="checkIsDetailsPresent()">Send For Approve</button>
                      <?php
                    if($id_programme != NULL)
                    {
                      ?>
                      <a href="<?php echo '../list' ?>" class="btn btn-link">Cancel</a>
                      <?php
                    }
                    ?>
                   </div>
                 </div>

         </div>
     
      </form>


      <div class="form-container">
        <h4 class="form-group-title">Previous Status Details</h4>

          <div class="custom-table">
            <table class="table">
                <thead>
                    <tr>
                    <th>Sl. No</th>
                     <th>Status</th>
                     <th>Reason</th>
                     <th>Date Time</th>
                     <th>Updated By</th>
                    </tr>
                </thead>
                <tbody>
                     <?php
                  for($i=0;$i<count($programmeStatusChange);$i++)
                 { ?>
                    <tr>
                    <td><?php echo $i+1;?></td>
                    <td><?php echo $programmeStatusChange[$i]->status_name;?></td>
                    <td><?php echo $programmeStatusChange[$i]->reason;?></td>
                    <td><?php echo date('d-m-Y H:i:s', strtotime($programmeStatusChange[$i]->created_dt_tm));?></td>
                    <td><?php echo $programmeStatusChange[$i]->created_by;?></td>
                     </tr>
                  <?php 
              } 
              ?>
                </tbody>
            </table>
          </div>

      </div>


     
   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

    function checkIsDetailsPresent(id)
    {
      var image = "<?php echo $programmeDetails->image; ?>";
      var id_programme_fee_structure = "<?php echo $id_programme_fee_structure; ?>";
      var is_overview = "<?php echo $is_overview; ?>";
      var is_topic = "<?php echo $is_topic; ?>";

      if(image == '')
      {
        alert('Image File Not Uploaded, Upload Image File To Send For Approval');
      }
      else if(id_programme_fee_structure == '0')
      {
        alert('Fee Structure Not Added, Add Fee Structure To  Send For Approval');
      }
      else if(is_overview == '0')
      {
        alert('Description Not Added For The Product, Add Description To Send For Approval');
      }
      else if(is_topic == '0')
      {
        alert("Topic's Not Added To The Product, Add Topic To Send For Approval");
      }

      if(image != '' && id_programme_fee_structure != '0' && is_overview != '0' && is_topic != '0')
      {
        var cnf= confirm('Do you want to Send the product for approval?');
        if(cnf==true)
        {
          $('#form_programme').submit();
        }
      }
    }


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>