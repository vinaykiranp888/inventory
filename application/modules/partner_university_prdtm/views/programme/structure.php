<?php
$programme_model = new Programme_Model();

$urlarray = explode ('/',$_SERVER['REQUEST_URI']);

$urlmodule = $urlarray['1'];
$urlcontroller = $urlarray['2'];
$urlaction = $urlarray['3'];

$id_product_type = $programmeDetails->id_programme_type;

$programme_tabs  = $programme_model->getProductTabsByProductId($id_product_type);

// echo "<Pre>";print_r($programme_tabs);exit();
?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
            foreach ($programme_tabs as $individual_tab)
            {
              $tab = $individual_tab->tab;
              $title = $individual_tab->title;
            ?>
              <li 
              <?php
              if($tab == $urlaction)
              {
                echo 'class="active"';
              }
              ?>
              ><a href="/partner_university_prdtm/programme/<?php echo $tab;?>/<?php echo $id_programme;?>"><?php echo $title ?></a></li>
            <?php
            }
            ?>
          

          <li><a href="/partner_university_prdtm/programme/fee/<?php echo $id_programme;?>">Fee Structure</a></li>
          <li><a href="/partner_university_prdtm/programme/markDistribution/<?php echo $id_programme;?>">Mark Distribution</a></li>
          

          <?php
          if ($programmeDetails->send_for_approval == '0')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/sendForApproval/<?php echo $id_programme;?>">Send For Approval</a></li>
            
           <?php
          }
          else
          {
            ?>
        
            <li><a href="/partner_university_prdtm/programme/approval/<?php echo $id_programme;?>">Approval Details</a></li>

            <?php
          }
          ?>
          
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Structure Details</h4>

             <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                     <textarea type="text" class="form-control" id="structure" name="structure"><?php echo $structure[0]->structure; ?></textarea>
                  </div>
               </div>
                </div>
                <div class="row">
               
                   <div class="col-sm-2 pt-10">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-lg" value="Structure" name="save">Save</button>
                     </div>
                   </div>

                </div>
         </div>
     
      </form>
   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">
      $('select').select2();


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">



CKEDITOR.replace('structure',{
  width: "100%",
  height: "300px"

}); 

</script>