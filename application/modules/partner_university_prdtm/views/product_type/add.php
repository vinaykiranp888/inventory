<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Category Type</h3>
            </div>


    <form id="form_main" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Category Type Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Parent Category <span class='error-text'>*</span></label>
                        <select name="id_parent_product" id="id_parent_product" class="form-control" onchange="showchildCategory(this.value)">
                            <option value="99999">Top</option>

                            <?php for($i=0;$i<count($getParentProduct);$i++) { ?>
                            <option value="<?php echo $getParentProduct[$i]->id;?>"><?php echo $getParentProduct[$i]->name;?></option>
                        <?php } ?> 
                           
                        </select>
                     </div>
                </div>
           
                <div class="col-sm-4" id='showChildDiv' style="display: none;">
                    <div class="form-group">
                        <label>Child Category <span class='error-text'>*</span></label>
                            <div id="optionDiv"></div>

                     </div>
                </div>
            </div>


             <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
          
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Category Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>
            </div>


            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>
            </div>

                    <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


    </form>


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

   $(document).ready(function() {
        $('select').select2();


        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


   function showchildCategory(id) {
      if(id=='99999') {
         $("#showChildDiv").hide();
      } else {
         $.get("/prdtm/productType/getChild/"+id, function(data, status)
        {
             $("#optionDiv").html(data);
        });

        $("#showChildDiv").show();
      }

   }

</script>