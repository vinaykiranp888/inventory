<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_model extends CI_Model
{

    function partnerUniversityListByStatus($status)
    {
        $this->db->select('d.*');
        $this->db->from('partner_university as d');
        $this->db->where('status', $status);
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function getExaminationComponent() {
        $this->db->select('d.*');
        $this->db->from('examination_components as d');
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function fetFacultySearch(){
        $this->db->select('d.*');
        $this->db->from('staff as d');
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function addsyllabus($data) {
        $this->db->trans_start();
        $this->db->insert('programme_has_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }
    function addProgrammetoStaff($data) {
        $this->db->trans_start();
        $this->db->insert('programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

 

    function addProgramOverview($data) {
        $this->db->trans_start();
        $this->db->insert('programme_has_overview', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }
    function addassessment($data) {
        $this->db->trans_start();
        $this->db->insert('programme_has_assessment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function addassessmentmain($data) {
        $this->db->trans_start();
        $this->db->insert('programme_has_assessment_main', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    
    

    function addtopic($data){
        
        $this->db->trans_start();
        $this->db->insert('programme_has_topic', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }

    function getNames($ids) {
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where("id in ($ids)");

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAssessmentDetails($id) {
        $this->db->select('p.*, e.name as component');
        $this->db->from('programme_has_assessment as p');
        $this->db->join('examination_components as e', 'p.id_examination_components = e.id','left');
        
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAssessmentDetailsMain($id) {
        $this->db->select('*');
        $this->db->from('programme_has_assessment_main');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramOverview($id) {
        $this->db->select('*');
        $this->db->from('programme_has_overview');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    

    function getProgrammeSyllabus($id) {
        $this->db->select('*');
        $this->db->from('programme_has_syllabus');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getprogrammeTopic($id) {
        $this->db->select('*');
        $this->db->from('programme_has_topic');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function educationLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    
    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function categoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function categoryTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function productTypeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function awardListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programmeConditionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme_condition');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function schemeList()
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programmeListSearch($data)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        // $this->db->join('scheme as sc', 'p.id_scheme = sc.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(p.name  LIKE '%" . $data['name'] . "%' or p.name_optional_language  LIKE '%" . $data['name'] . "%' or p.code  LIKE '%" . $data['name'] . "%' or p.foundation  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('p.id_category', $data['id_category']);
        }
        if ($data['id_category_setup'] != '')
        {
            $this->db->where('p.id_category_setup', $data['id_category_setup']);
        }
        if ($data['id_programme_type'] != '')
        {
            $this->db->where('p.id_programme_type', $data['id_programme_type']);
        }
        if($data['id_partner_university'] != '')
        {
            $this->db->where('p.id_partner_university', $data['id_partner_university']);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    
    function addNewProgrammeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme', $data);
        return TRUE;
    }

    function addNewTempProgrammeHasDean($data)
    {

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasDean($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewProgrammeHasCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addTempCourseDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_programme_has_dean', $data);
        return TRUE;
    }

    function updateTempCourseDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_programme_has_course', $data);
        return TRUE;
    }

    function getTempProgrammeHasDean($id_session)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation');
        $this->db->from('temp_programme_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempProgrammeHasCourse($id_session)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('temp_programme_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasDean($id_programme)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation,st.ic_no');
        $this->db->from('programme_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');     
        $this->db->where('tphd.id_programme', $id_programme);
        $this->db->order_by("st.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasCourse($id)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('programme_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_programme', $id);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_programme_has_dean');
    }

    function deleteassessmentmain($id) {
        $this->db->where('id_programme', $id);
       $this->db->delete('programme_has_assessment_main');

    }

    function deleteprogramoverview($id) {
        $this->db->where('id_programme', $id);
       $this->db->delete('programme_has_overview');

    }

    function deleteTempProgHasDeanDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_programme_has_dean');
    }

    function deleteTempCourseData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_programme_has_course');
    }

    function deleteProgrammeHasDean($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('programme_has_dean');
    }

    function deleteProgrammeHasCourse($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('programme_has_course');
    }

    function addTempProgramHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_programme_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempProgrammeHasScheme($id_session)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('temp_programme_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');     
        $this->db->where('tphd.id_session', $id_session);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempProgramHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_programme_has_scheme');
        return TRUE;
    }

    function deleteTempProgramHasSchemeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_programme_has_scheme');
        return TRUE;
    }

    function addNewProgrammeHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgrammeHasScheme($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');
        $this->db->where('tphd.id_program', $id);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programmeObjectiveList($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_programme', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeCourseOverviewList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_course_overview as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeDeliveryModeList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_delivery_mode as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeAssesmentList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_assesment as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programmeSyllabusList($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_syllabus as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getDeliveryMode(){
        $this->db->select('tphd.*');
        $this->db->from('delivery_mode as tphd');
        $query = $this->db->get();
        return $query->result();
    }
    function getStudyLevel() {
                $this->db->select('tphd.*');
        $this->db->from('study_level as tphd');
        $query = $this->db->get();
        return $query->result();

    }


    function programmeObjective($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_programme', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeCourseOverview($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_course_overview as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeDeliveryMode($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_delivery_mode as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeAssesment($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_assesment as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function programmeSyllabus($id_programme)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_syllabus as tphd');
        $this->db->where('tphd.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->row();
    }


    function deleteProgrammeHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_scheme');
        return TRUE;
    }

    function programmeAwardList($id_programme)
    {
        $this->db->select('tphd.*, aw.code as award_code, aw.name as award_name, pc.code as program_condition_code, pc.name as program_condition_name');
        $this->db->from('programme_has_award as tphd');
        $this->db->join('award as aw', 'tphd.id_award = aw.id');     
        $this->db->join('programme_condition as pc', 'tphd.id_program_condition = pc.id');     
        $this->db->where('tphd.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeLearningModeList($id_program)
    {
        $this->db->select('tphd.*, sc.name as program_name, sc.code as program_code');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->join('program_type as sc', 'tphd.id_program_type = sc.id');     
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeSchemeList($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('program_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');     
        $this->db->where('tphd.id_program', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeMajoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_major_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeMinoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_minor_details as tphd');  
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeConcurrentList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_concurrent_program as tphd');   
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeAcceredationList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_acceredation_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewProgrammeHasMajor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_major_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasMinor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_minor_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasConcurrent($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_concurrent_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasAcceredation($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_acceredation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgramSchemeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveProgramObjectiveData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_objective', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveCourseOverviewData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_course_overview', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveDeliveryModeData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_delivery_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveAssesmentData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_assesment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveSyllabusData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteProgramObjectiveDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_objective');
        return TRUE;
    }

    function deleteCourseOverviewDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_course_overview');
        return TRUE;
    }


    function deleteDeliveryModeDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_delivery_mode');
        return TRUE;
    }


    function deleteAssesmentDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_assesment');
        return TRUE;
    }


    function deleteSyllabusDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_syllabus');
        return TRUE;
    }

    function updateAllFacilitatorToInactive($idprogramme)
    {
        $data['status'] = 0;
        $this->db->where('id_programme', $idprogramme);
        $this->db->update('programme_has_dean', $data);
        return $this->db->affected_rows();
    }

     function updateFacilitatorToActive($id)
    {
        $data['status'] = 1;
        $this->db->where('id', $id);
        $this->db->update('programme_has_dean', $data);
        return $this->db->affected_rows();
    }


    function updateProgramObjectiveData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_objective', $data);
        return $this->db->affected_rows();
    }

    function updateCourseOverviewData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_course_overview', $data);
        return $this->db->affected_rows();
    }

    function updateDeliveryModeData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_delivery_mode', $data);
        return $this->db->affected_rows();
    }

    function updateAssesmentData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_assesment', $data);
        return $this->db->affected_rows();
    }

    function updateSyllabusData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('program_syllabus', $data);
        return $this->db->affected_rows();
    }


    function deleteAcceredationDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_acceredation_details');
        return TRUE;
    }

    function saveAwardData($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_award', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteAwardData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_award');
        return TRUE;
    }

    
    function addProgramStructure($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgramStructure($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_structure');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteProgramStructure($id)
    {
        $this->db->where('id_programme', $id);
        $this->db->delete('programme_has_structure');
        return TRUE;
    }

    function addProgramAim($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_aim', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getProgramAim($id)
    {
        $this->db->select('*');
        $this->db->from('programme_has_aim');
        $this->db->where('id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteProgramAim($id)
    {
        $this->db->where('id_programme', $id);
        $this->db->delete('programme_has_aim');
        return TRUE;
    }

    function addProgramModules($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_ref_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgramHasModules($id)
    {
        $this->db->select('phrp.*, p.name as programme_name, p.code as programme_code');
        $this->db->from('programme_has_ref_program as phrp');
        $this->db->join('programme as p', 'phrp.id_child_programme = p.id');
        $this->db->where('phrp.id_programme', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteProgramHasModules($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_ref_program');
        return TRUE;
    }

    function getProgrammeListByIdCategory($id_category)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id_category', $id_category);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}