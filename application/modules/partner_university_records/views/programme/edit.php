
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <form id="form_programme" action="" method="post" enctype="multipart/form-data">


        <ul class="page-nav-links">
            <li class="active"><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

          <?php
          if ($programmeDetails->id_category == '1')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/partner_university_prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Objective</a></li>
            <li><a href="/partner_university_prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/partner_university_prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/partner_university_prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/partner_university_prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/partner_university_prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>

          <?php
          }
          else
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/partner_university_prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of te Program</a></li>
            <li><a href="/partner_university_prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?>

        </ul>

         <div class="form-container">
            <h4 class="form-group-title">Program Details</h4>
            <div class="row">
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Type <span class='error-text'>*</span></label>
                     <select name="id_category" id="id_category" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($categoryList))
                           {
                               foreach ($categoryList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_category == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Code <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>">
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Name <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>">
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Target Audience <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="target_audience" name="target_audience" value="<?php echo $programmeDetails->target_audience; ?>">
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Student Learning Hours <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="student_learning_hours" name="student_learning_hours" value="<?php echo $programmeDetails->student_learning_hours; ?>">
                  </div>
               </div>


               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Total Cr. Hours <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="total_cr_hrs" name="total_cr_hrs" value="<?php echo $programmeDetails->total_cr_hrs; ?>">
                  </div>
               </div>
               
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Owner <span class='error-text'>*</span></label>
                     <select name="internal_external" id="internal_external" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="Internal" <?php if ($programmeDetails->internal_external == 'Internal')
                           {
                               echo "selected";
                           } ?>>Internal</option>
                        <option value="External" <?php if ($programmeDetails->internal_external == 'External')
                           {
                               echo "selected";
                           } ?>>External</option>
                     </select>
                  </div>
               </div>
               <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                  <div class="form-group">
                     <label>Partner Name <span class='error-text'>*</span></label>
                     <select name="id_partner_university" id="id_partner_university" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($partnerList))
                           {
                               foreach ($partnerList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($record->id == $programmeDetails->id_partner_university)
                              {
                                  echo "selected=selected";
                              } ?>>
                           <?php echo $record->code . " - " . $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Category Type <span class='error-text'>*</span></label>
                     <select name="id_programme_type" id="id_programme_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($productTypeSetupList))
                           {
                               foreach ($productTypeSetupList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_programme_type == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-4">
                  <label>Max Duration<span class='error-text'>*</span></label>
                  <div class="row">
                     <div class="col-sm-6">
                        <input type="number" class="form-control" id="max_duration" name="max_duration" min='1' value="<?php echo $programmeDetails->max_duration; ?>">
                     </div>
                     <div class="col-sm-6">
                        <select name="duration_type" id="duration_type" class="form-control" required>
                           <option value="">Select</option>
                           <option value="Weeks"
                              <?php
                                 if ($programmeDetails->duration_type == 'Weeks')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Weeks</option>
                           <option value="Months"
                              <?php
                                 if ($programmeDetails->duration_type == 'Months')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Months</option>
                           <option value="Years"
                              <?php
                                 if ($programmeDetails->duration_type == 'Years')
                                 {
                                     echo 'selected';
                                 }
                                 ?>
                              >Years</option>
                        </select>
                     </div>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Delivery Mode <span class='error-text'>*</span></label>
                     <select name="id_delivery_mode" id="id_delivery_mode" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($deliveryModeList))
                           {
                               foreach ($deliveryModeList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_delivery_mode == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Study Level <span class='error-text'>*</span></label>
                     <select name="id_study_level" id="id_study_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($studyLevelList))
                           {
                               foreach ($studyLevelList as $record)
                               { ?>
                        <option value="<?php echo $record->id; ?>"
                           <?php
                              if ($programmeDetails->id_study_level == $record->id)
                              {
                                  echo 'selected';
                              }
                              ?>
                           ><?php echo $record->name; ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
               
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Product Trending Type<span class='error-text'>*</span></label>
                     <select name="trending" id="trending" class="form-control" onchange="showPartner(this.value)">
                        <option value="" >Select</option>
                        <option value="HOME" <?php if ($programmeDetails->trending == 'HOME')
                           {
                               echo "selected";
                           } ?>>HOME</option>
                        <option value="TRENDING" <?php if ($programmeDetails->trending == 'TRENDING')
                           {
                               echo "selected";
                           } ?>>TRENDING</option>
                        <option value="POPULAR" <?php if ($programmeDetails->trending == 'POPULAR')
                           {
                               echo "selected";
                           } ?>>POPULAR</option>
                     </select>
                  </div>
               </div>

                <div class="col-sm-4">
                  <div class="form-group">
                     <label>FILE 
                     <span class='error-text'>*</span>
                     <?php
                        if ($programmeDetails->image != '')
                        {
                        ?>
                     <a href="<?php echo '/assets/images/' . $programmeDetails->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $programmeDetails->image; ?>)" title="<?php echo $programmeDetails->image; ?>"> View </a>
                     <?php
                        }
                        ?>
                     </label>
                     <input type="file" name="image" id="image">
                     <span style="font-size: 12px;
                      color: #c22026;">Width:825px; Height:490px;</span>
                  </div>
                </div>


                <div class="col-sm-4">
                  <div class="form-group">
                     <p>Sold Seperately <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="sold_separately" id="sold_separately" value="1" 
                     <?php if ($programmeDetails->sold_separately == '1')
                        {
                            echo "checked";
                        }; ?>><span class="check-radio"></span> Yes
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="sold_separately" id="sold_separately" value="0" <?php if ($programmeDetails->sold_separately == '0')
                        {
                            echo "checked";
                        }; ?>>
                     <span class="check-radio"></span> No
                     </label>
                  </div>
                </div>


                <?php
                if ($programmeDetails->percentage > 0 && $programmeDetails->status != '0')
                {
                ?>



                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Percentage <span class='error-text'>*</span></label>
                       <input type="text" class="form-control" id="percentage" name="percentage" value="<?php echo $programmeDetails->percentage; ?>">
                    </div>
                 </div>


              <?php
              }
              ?>



                <div class="col-sm-4">
                  <div class="form-group">
                     <p>Status <span class='error-text'>*</span></p>
                     <label class="radio-inline">
                     <input type="radio" name="status" id="status" value="1" <?php if ($programmeDetails->status == '1')
                        {
                            echo "checked=checked";
                        }; ?> disabled><span class="check-radio"></span> Active
                     </label>
                     <label class="radio-inline">
                     <input type="radio" name="status" id="status" value="0" <?php if ($programmeDetails->status == '0')
                        {
                            echo "checked=checked";
                        }; ?> disabled>
                     <span class="check-radio"></span> In-Active
                     </label>
                  </div>
               </div>


              </div>

              


              <div class="row">


                 <div class="col-sm-8">
                  <div class="form-group">
                     <label>Short Description <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="short_description" name="short_description" value="<?php echo $programmeDetails->short_description; ?>">
                  </div>
               </div>

            </div>


          


         </div>
      <div class="button-block clearfix">
         <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            <a href="../list" class="btn btn-link">Back</a>
         </div>
      </div>
   </div>
         </form>

</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script>

        $('select').select2();



   $(function()
   {
       showPartner();
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
   });
   
   
   function showPartner(){
       var value = $("#internal_external").val();
       if(value=='Internal') {
            $("#partnerdropdown").hide();
   
       } else if(value=='External') {
            $("#partnerdropdown").show();
   
       }
   }
   
   
   
</script>

<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
