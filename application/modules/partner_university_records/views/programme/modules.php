
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

          <?php
          if ($programmeDetails->id_category == '1')
          {
          ?>
            <li><a href="/partner_university_prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/partner_university_prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Objective</a></li>
            <li><a href="/partner_university_prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/partner_university_prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/partner_university_prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/partner_university_prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/partner_university_prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>

          <?php
          }
          elseif ($programmeDetails->id_category == '2')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/partner_university_prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li class="active"><a href="/partner_university_prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Modules To Courses Details</h4>

             
                <div class="row">

                  <div class="col-sm-4">
                    <div class="form-group">
                       <label>Courses <span class='error-text'>*</span></label>
                       <select name="id_child_programme" id="id_child_programme" class="form-control">
                          <option value="">Select</option>
                          <?php
                             if (!empty($programmeList))
                             {
                                 foreach ($programmeList as $record)
                                 { ?>
                          <option value="<?php echo $record->id; ?>"><?php echo $record->code . " - " . $record->name; ?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                 </div>


                 <div class="col-sm-4 pt-10">
                  <div class="form-group">

                      <button type="submit" class="btn btn-primary btn-lg" value="Modules" name="save">Save</button>
                   </div>
                 </div>


                   
                </div>
         </div>
     
      </form>


      <?php

        if(!empty($programHasModules))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Modules To Courses Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Course</th>
                             <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($programHasModules);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $programHasModules[$i]->programme_code . " - " . $programHasModules[$i]->programme_name;?></td>
                            <td class="text-center">
                                <a onclick="deleteProgramHasModules(<?php echo $programHasModules[$i]->id; ?>)">Delete</a>
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>


   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

    function deleteProgramHasModules(id)
    {
         $.ajax(
            {
               url: '/partner_university_prdtm/programme/deleteProgramHasModules/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">



CKEDITOR.replace('overview',{
  width: "100%",
  height: "300px"

}); 

</script>