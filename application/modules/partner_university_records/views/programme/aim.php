
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>

            <?php
          if ($programmeDetails->id_category == '1')
          {
          ?>
            <li><a href="/partner_university_prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/partner_university_prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Objective</a></li>
            <li><a href="/partner_university_prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/partner_university_prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/partner_university_prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/partner_university_prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/partner_university_prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>

          <?php
          }
          elseif ($programmeDetails->id_category == '2')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li class="active"><a href="/partner_university_prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/partner_university_prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Aim Of The Programme Details</h4>

             <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                     <textarea type="text" class="form-control" id="aim" name="aim"><?php echo $aim[0]->aim; ?></textarea>
                  </div>
               </div>
                </div>
                <div class="row">
               
                   <div class="col-sm-2 pt-10">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary btn-lg" value="Aim" name="save">Save</button>
                     </div>
                   </div>
                   
                </div>
         </div>
     
      </form>
   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">
      $('select').select2();


</script>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">



CKEDITOR.replace('overview',{
  width: "100%",
  height: "300px"

}); 

</script>