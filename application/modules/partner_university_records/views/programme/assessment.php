
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
       <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>
          <?php
          if ($programmeDetails->id_category == '1')
          {
          ?>
            <li><a href="/partner_university_prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/partner_university_prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Objective</a></li>
            <li><a href="/partner_university_prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/partner_university_prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li class="active"><a href="/partner_university_prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/partner_university_prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li><a href="/partner_university_prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>

          <?php
          }
          elseif ($programmeDetails->id_category == '2')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/partner_university_prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li class="active"><a href="/partner_university_prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?>
        </ul>

      <form id="form_programme" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Assessment Details</h4>
            <div class="row">


             

              
               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Total Weightage (in Percentage %) <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="total_weightage_percentage" name="total_weightage_percentage" value="<?php echo $assessmentDetailsListMain[0]->total_weightage_percentage;?>" required>
                  </div>
               </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label>Passing Mark (in Percentage %) <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="passing_mark_percentage" name="passing_mark_percentage" value="<?php echo $assessmentDetailsListMain[0]->passing_mark_percentage;?>" required>
                  </div>
               </div>

               <div class="col-sm-3">
                  <div class="form-group pt-10">
                     <button type="submit" class="btn btn-primary btn-lg" value="Objective" name="save">Update</button>
                  </div>
               </div>
               
              
            </div>
         </div>
     
      </form>

         <div class="form-container">
            <h4 class="form-group-title">Assessment Details</h4>
            <div class="row">


               <div class="col-sm-3">
                  <div class="form-group">
                     <label>Assessment Component <span class='error-text'>*</span></label>
                   
                     <select name='id_examination_components' id='id_examination_components' class="form-control">
                      <option value="">Select</option>
                      <?php for($i=0;$i<count($examinationComponent);$i++) { ?>

                      <option value="<?php echo $examinationComponent[$i]->id;?>"><?php echo $examinationComponent[$i]->name;?></option>
                    <?php } ?> 
                     </select>

                  </div>
               </div>

              
               <div class="col-sm-2">
                  <div class="form-group">
                     <label>Weightage <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="weightage" name="weightage" value="" required>
                  </div>
               </div>

               <div class="col-sm-2">
                  <div class="form-group">
                     <label>Total Mark <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="total_mark" name="total_mark" value="" required>
                  </div>
               </div>

               <div class="col-sm-2">
                  <div class="form-group">
                     <label>Passing Mark <span class='error-text'>*</span></label>
                     <input type="text" class="form-control" id="passing_mark" name="passing_mark" value="" required>
                  </div>
               </div>
               
               <div class="col-sm-2 pt-10">
                <div class="form-group">

                    <button type="submit" class="btn btn-primary btn-lg" value="Objective" name="save">Save</button>
                 </div>
               </div>
            </div>
         </div>
     
      </form>
       <div class="custom-table">
        <table class="table" id="list-table">
          <thead>
            <tr>
              <th>Sl. No</th>
              <th>Assessment Component</th>
              <th>Weightage</th>
              <th>Total Mark</th>
              <th>Passing Mark</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            $i=1;
              foreach ($assessmentDetailsList as $record) {
            ?>
                <tr>
                  <td><?php echo $i ?></td>
                  <td><?php echo ucfirst($record->component) ?></td>
                  <td><?php echo $record->weightage; ?></td>
                  <td><?php echo $record->total_mark; ?></td>
                  <td><?php echo $record->passing_mark; ?></td>
                  </td>
                  <td class="text-center">
                    <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Delete</a>
                  </td>
                </tr>
            <?php
            $i++;
              }
            
            ?>
          </tbody>
        </table>
       </div>



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">
      $('select').select2();


</script>