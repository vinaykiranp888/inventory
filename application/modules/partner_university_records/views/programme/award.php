
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">
            <li><a href="/partner_university_prdtm/programme/edit/<?php echo $id_programme;?>">Product Details</a></li>
          <?php
          if ($programmeDetails->id_category == '1')
          {
          ?>
            <li><a href="/partner_university_prdtm/programme/overview/<?php echo $id_programme;?>">Description</a></li>
            <li><a href="/partner_university_prdtm/programme/syllabus/<?php echo $id_programme;?>">Learning Objective</a></li>
            <li><a href="/partner_university_prdtm/programme/topic/<?php echo $id_programme;?>">Topic</a></li>
            <li><a href="/partner_university_prdtm/programme/faculty/<?php echo $id_programme;?>">Facilitator</a></li>
            
            <li><a href="/partner_university_prdtm/programme/assessment/<?php echo $id_programme;?>">Assessment</a></li>
            <li><a href="/partner_university_prdtm/programme/accreditation/<?php echo $id_programme;?>">Accreditation</a></li>
            <li class="active"><a href="/partner_university_prdtm/programme/award/<?php echo $id_programme;?>">Award</a></li>

          <?php
          }
          elseif ($programmeDetails->id_category == '2')
          {
          ?>

            <li><a href="/partner_university_prdtm/programme/structure/<?php echo $id_programme;?>">Programme Structure</a></li>
            <li><a href="/partner_university_prdtm/programme/aim/<?php echo $id_programme;?>">Aim Of The Program</a></li>
            <li><a href="/partner_university_prdtm/programme/modules/<?php echo $id_programme;?>">Modules to Courses</a></li>

          <?php
          }
          ?>

        </ul>

         <div role="tabpanel" class="tab-pane" id="program_accerdation">
                        <div class="mt-4">


          <form id="form_main" action="" method="post">
              <div class="form-container">
                  <h4 class="form-group-title"> Award Details</h4>

                  <div class="row">

                     
                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Award Name <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="award_name" name="award_name" autocomplete="off">
                          </div>
                      </div>

                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Award Level <span class='error-text'>*</span></label>
                            <select name="id_award" id="id_award" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($awardList))
                                {
                                    foreach ($awardList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                      </div>


                      <div class="col-sm-4">
                        <div class="form-group">
                            <label>Condition To Meet <span class='error-text'>*</span></label>
                            <select name="id_program_condition" id="id_program_condition" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programmeConditionList))
                                {
                                    foreach ($programmeConditionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                      </div>




                  </div>


              </div>


              <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="button" class="btn btn-primary btn-lg" onclick="saveAcceredationData()">Save</button>
                      <!-- <a href="../list" class="btn btn-link">Back</a> -->
                  </div>
              </div>

          </form>




              <?php

              if(!empty($programmeAwardList))
              {
                  ?>
                  <br>

                  <div class="form-container">
                          <h4 class="form-group-title">Award Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                  <th>Sl. No</th>
                                   <th>Name</th>
                                   <th>Award Level</th>
                                   <th>Condition</th>
                                   <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php
                               $total = 0;
                                for($i=0;$i<count($programmeAwardList);$i++)
                               { ?>
                                  <tr>
                                  <td><?php echo $i+1;?></td>
                                  <td><?php echo $programmeAwardList[$i]->name;?></td>
                                  <td><?php echo $programmeAwardList[$i]->award_name ;?></td>
                                  <td><?php echo $programmeAwardList[$i]->program_condition_code . " - " . $programmeAwardList[$i]->program_condition_name ;?></td>
                                  <td>
                                  <a onclick="deleteAwardData(<?php echo $programmeAwardList[$i]->id; ?>)">Delete</a>
                                  </td>

                                   </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }
               ?>







          </div>
      
      </div>




         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveAcceredationData()
    {
        if($('#form_main').valid())
        {

        var tempPR = {};
        tempPR['name'] = $("#award_name").val();
        tempPR['id_award'] = $("#id_award").val();
        tempPR['id_program_condition'] = $("#id_program_condition").val();
        tempPR['id_program'] = <?php echo $id_programme;?>;

            $.ajax(
            {
               url: '/prdtm/programme/saveAwardData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
        }
    }



    function deleteAwardData(id)
    {
        $.ajax(
            {
               url: '/prdtm/programme/deleteAwardData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
    }



    $(document).ready(function()
     {
        $("#form_main").validate({
            rules: {
                award_name: {
                    required: true
                },
                id_award: {
                    required: true
                },
                id_program_condition: {
                    required: true
                }
            },
            messages: {
                award_name: {
                    required: "<p class='error-text'>Award Name Required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                id_program_condition: {
                    required: "<p class='error-text'>Select Programme Condition</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>