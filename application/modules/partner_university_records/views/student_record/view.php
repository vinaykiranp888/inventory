<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Student Record</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>
      <form id="form_main_invoice" action="" method="post">



         <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Organisation :</dt>
                                <dd><?php

                                if($getStudentData->id_university != 1 && $getStudentData->id_university != 0)
                                {
                                    echo $getStudentData->partner_university_code , " - " . $getStudentData->partner_university_name;
                                }
                                else
                                {
                                    echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                                }
                                
                                ?></dd>
                            </dl> 

                            <!-- <dl>
                                <dt>Research Supervisor :</dt>
                                <dd><?php 
                                if($getStudentData->supervisor_type == '0')
                                {
                                    echo 'External';
                                }
                                elseif($getStudentData->supervisor_type == '1')
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->supervisor_name ; ?></dd>
                            </dl> -->

                            
                            
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->programme_code . " - " . $getStudentData->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                            <dl>
                                <dt>Passport Number :</dt>
                                <dd><?php echo $getStudentData->passport_number; ?></dd>
                            </dl>
                            <?php 
                            if($getStudentData->qualification_name == 'POSTGRADUATE')
                            {
                            ?>
                            <dl>
                                <dt>Academic Advisor :</dt>
                                <dd><?php 
                                if($getStudentData->advisor_type == '0')
                                {
                                    echo 'External';
                                }
                                elseif($getStudentData->advisor_type == '1')
                                {
                                    echo 'Internal';
                                }
                                echo " - " . $getStudentData->advisor_name ; ?></dd>
                            </dl>

                            
                            <!-- <?php 
                            }
                            ?>
                            <?php 
                            if($getStudentData->qualification_name == 'MASTER' || $getStudentData->qualification_name == 'POSTGRADUATE')
                            {
                            ?>

                               <dl>
                                  <dt>Current Phd Duration :</dt>
                                  <dd><?php echo $getStudentData->phd_duration ?></dd>
                               </dl>

                            <?php
                            }
                            ?> -->
                        </div>
    
                    </div>
                </div>


         </div>


         



         <div class="form-container">
            <h4 class="form-group-title">Student Records Details</h4>
            <div class="m-auto text-center">
               <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
               <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                  <li role="presentation" class="active" ><a href="#tab_4" class="nav-link border rounded text-center"
                     aria-controls="tab_4" aria-selected="true"
                     role="tab" data-toggle="tab">Student Status</a>
                  </li>
                 <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center"
                     aria-controls="tab_2" role="tab" data-toggle="tab">Barring / Release</a>
                  </li>
                  <li role="presentation"><a href="#tab_3" class="nav-link border rounded text-center"
                     aria-controls="tab_3" role="tab" data-toggle="tab">Program Registration</a>
                  </li>
                  <!-- <li role="presentation"><a href="#tab_4" class="nav-link border rounded text-center"
                     aria-controls="tab_4" role="tab" data-toggle="tab">Student Status</a>
                  </li> -->
                  <!-- <li role="presentation"><a href="#tab_5" class="nav-link border rounded text-center"
                     aria-controls="tab_5" role="tab" data-toggle="tab">Semester Status</a>
                  </li>
                  <li role="presentation"><a href="#tab_6" class="nav-link border rounded text-center"
                     aria-controls="tab_6" role="tab" data-toggle="tab">Academic Advisor</a>
                  </li>
                  <li role="presentation"><a href="#tab_7" class="nav-link border rounded text-center"
                     aria-controls="tab_7" role="tab" data-toggle="tab">Academic Progress</a>
                  </li>
                  <li role="presentation"><a href="#tab_8" class="nav-link border rounded text-center"
                     aria-controls="tab_8" role="tab" data-toggle="tab">Credit Transfer</a>
                  </li> -->
                  <!-- <li role="presentation"><a href="#tab_14" class="nav-link border rounded text-center"
                     aria-controls="tab_14" role="tab" data-toggle="tab">Notes</a>
                  </li>
                  <li role="presentation"><a href="#tab_9" class="nav-link border rounded text-center"
                     aria-controls="tab_9" role="tab" data-toggle="tab">Visa Details</a> -->
                  </li>
                  <!-- <?php 
                  if($studentDetails->qualification_name == 'MASTER' || $studentDetails->qualification_name == 'POSTGRADUATE')
                  {

                  ?>

                  <li role="presentation" style="display: none;"><a href="#tab_10" class="nav-link border rounded text-center"
                     aria-controls="tab_10" role="tab" data-toggle="tab">Duration Details</a>
                  </li>
                  <?php
                  }  
                  ?> -->
               </ul>


               <div class="tab-content offers-tab-content">


                  <div role="tabpanel" class="tab-pane" id="tab_1">
                     <div class="mt-4">
                        
                        <br>

                        <div class="form-container">
                           <h4 class="form-group-title">Personal Details</h4>
                           <div class='data-list'>
                              <div class='row'>
                                 <div class='col-sm-6'>
                                    <dl>
                                       <dt>Salutation :</dt>
                                       <dd><?php echo ucwords($studentDetails->salutation);?></dd>
                                    </dl>
                                    <dl>
                                       <dt>First Name :</dt>
                                       <dd><?php echo $studentDetails->first_name ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>ID Type :</dt>
                                       <dd><?php echo $studentDetails->id_type ?></dd>
                                    </dl>
                                    <!-- <dl>
                                       <dt>Passport Expiry Date :</dt>
                                       <dd><?php if($studentDetails->passport_expiry_date){ echo date('d-m-Y', strtotime($studentDetails->passport_expiry_date)); } ?></dd>
                                    </dl> -->
                                    <dl>
                                       <dt>Date Of Birth :</dt>
                                       <dd><?php if($studentDetails->date_of_birth){ echo date('d-m-Y', strtotime($studentDetails->date_of_birth)); } ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Maritual Status :</dt>
                                       <dd><?php echo $studentDetails->id_type ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Nationality :</dt>
                                       <dd><?php echo $studentDetails->nationality ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Race :</dt>
                                       <dd><?php echo $studentDetails->race ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Email ID :</dt>
                                       <dd><?php echo $studentDetails->email_id ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Phone Number :</dt>
                                       <dd><?php echo $studentDetails->phone ?></dd>
                                    </dl>
                                    <dl>
                                       <dt style="color: black"><br><b>Permanent Mailing Address :</b></dt>
                                       <dd></dd>
                                    </dl>
                                    <dl>
                                       <dt>Address :</dt>
                                       <dd><?php echo $studentDetails->permanent_address1 ?></dd>
                                    </dl>
                                    <dl>
                                       <dt></dt>
                                       <dd><?php echo $studentDetails->permanent_address2 ?></dd>
                                    </dl>
                                    <dl>
                                       <dt style="color: black"><br><br><br><b>Correspondance Mailing Address :</b></dt>
                                       <dd></dd>
                                    </dl>
                                    <dl>
                                       <dt>Address :</dt>
                                       <dd><?php echo $studentDetails->mail_address2 ?></dd>
                                    </dl>
                                    <dl>
                                       <dt></dt>
                                       <dd><?php echo $studentDetails->mail_address2 ?></dd>
                                    </dl>
                                 </div>
                                 <div class='col-sm-6'>
                                    <dl>
                                       <dt>Program Name :</dt>
                                       <dd><?php echo ucwords($studentDetails->programme_code . " - " . $studentDetails->programme_name);?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Last Name :</dt>
                                       <dd><?php echo $studentDetails->last_name; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>ID No. :</dt>
                                       <dd><?php echo $studentDetails->passport; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Gender :</dt>
                                       <dd><?php echo $studentDetails->gender; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Age :</dt>
                                       <dd><?php if($studentDetails->date_of_birth){
                                          echo date('Y') - date('Y', strtotime($studentDetails->date_of_birth)); } ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Religion :</dt>
                                       <dd><?php echo $studentDetails->religion; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Contact Email ID :</dt>
                                       <dd><?php echo $studentDetails->contact_email ?></dd>
                                    </dl>
                                    <dl>
                                       <dt> <br><br></dt>
                                       <dd> </dd>
                                    </dl>
                                    <dl>
                                       <dt>Postal/ Zipcode :</dt>
                                       <dd><?php echo $studentDetails->permanent_zipcode ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Country :</dt>
                                       <dd><?php echo $studentDetails->permanent_country ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>State :</dt>
                                       <dd><?php echo $studentDetails->permanent_state ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>City :</dt>
                                       <dd><?php echo $studentDetails->permanent_city ?></dd>
                                    </dl>
                                    <dl>
                                       <dt> <br><br><br></dt>
                                       <dd> </dd>
                                    </dl>
                                    <dl>
                                       <dt>Postal/ Zipcode :</dt>
                                       <dd><?php echo $studentDetails->mailing_zipcode ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Country :</dt>
                                       <dd><?php echo $studentDetails->mailing_country ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>State :</dt>
                                       <dd><?php echo $studentDetails->mailing_state ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>City :</dt>
                                       <dd><?php echo $studentDetails->mailing_city ?></dd>
                                    </dl>
                                 </div>
                              </div>
                           </div>
                        </div>


                        <!-- <br>
                        <div class="form-container">
                           <h4 class="form-group-title">Emergency Contact Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Relationship</th>
                                       <th>Name</th>
                                       <th>Address</th>
                                       <th>Contact Details</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($emergencyContactDetails))
                                       {
                                         $i=1;
                                         foreach ($emergencyContactDetails as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->relationship ?></td>
                                       <td><?php echo $record->relative_name ?></td>
                                       <td><?php echo $record->relative_address ?></td>
                                       <td> Mobile : <?php echo $record->relative_mobile ?>
                                          <br>
                                          Home : <?php echo $record->relative_home_phone ?>
                                          <br>
                                          Office : <?php echo $record->relative_office_phone ?>
                                       </td>
                                    </tr>
                                    <?php
                                       $i++;
                                         }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>



                        <br>
                        <div class="form-container">
                           <h4 class="form-group-title">Education Qualification Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Qualification Level</th>
                                       <th>Degree Awarded</th>
                                       <th>Result / CGPA</th>
                                       <th>Year Of Graduation</th>
                                       <th>College Name</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($examDetails))
                                       {
                                         $i=1;
                                         foreach ($examDetails as $record)
                                         {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->qualification_level ?></td>
                                       <td><?php echo $record->degree_awarded ?></td>
                                       <td><?php echo $record->result ?></td>
                                       <td><?php echo $record->year ?></td>
                                       <td><?php echo $record->college_name ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                       }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>


                        <br>                        
                        <div class="form-container">
                           <h4 class="form-group-title">English Proficiency Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Test</th>
                                       <th>Date</th>
                                       <th>Score</th>
                                       <th>File</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($proficiencyDetails))
                                       {
                                         $i=1;
                                         foreach ($proficiencyDetails as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->test ?></td>
                                       <td><?php echo $record->date ?></td>
                                       <td><?php echo $record->score ?></td>
                                       <td><?php echo $record->file ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                         }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>



                        <br>
                        
                        <div class="form-container">
                           <h4 class="form-group-title">Employment Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Company Name</th>
                                       <th>Company Address</th>
                                       <th>Designation</th>
                                       <th>Position</th>
                                       <th>Year Of Service</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($employmentDetails))
                                       {
                                         $i=1;
                                         foreach ($employmentDetails as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->company_name ?></td>
                                       <td><?php echo $record->company_address ?></td>
                                       <td><?php echo $record->designation ?></td>
                                       <td><?php echo $record->position ?></td>
                                       <td><?php if($record->service_year == ""){ echo "0";} else {
                                          echo $record->service_year; } ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                         }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>



                        <br>
                        
                        <div class="form-container">
                           <h4 class="form-group-title">Other Document Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Name</th>
                                       <th>File</th>
                                       <th>Remarks</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($otherDocuments))
                                       {
                                         $i=1;
                                         foreach ($otherDocuments as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->doc_name ?></td>
                                       <td><?php echo $record->doc_file ?></td>
                                       <td><?php echo $record->remarks ?></td>
                                       <td class="text-center"><?php echo anchor('admission/student/delete_document?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                       }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>



                        <br>
                        
                        <div class="form-container">
                           <h4 class="form-group-title">Accomadation Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Room</th>
                                       <th>Apartment</th>
                                       <th>Unit</th>
                                       <th>Hostel</th>
                                       <th>From Date</th>
                                       <th>To Date</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($hostelRoomDetails))
                                       {
                                         $i=1;
                                         foreach ($hostelRoomDetails as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->room_code . " - " . $record->room_name ?></td>
                                       <td><?php echo $record->apartment_code . " - " . $record->apartment_name ?></td>
                                       <td><?php echo $record->unit_code . " - " . $record->unit_name ?></td>
                                       <td><?php echo $record->hostel_code . " - " . $record->hostel_name ?></td>
                                       <td><?php echo date('d-m-Y', strtotime($record->from_dt)) ?></td>
                                       <td><?php echo date('d-m-Y', strtotime($record->to_dt)) ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                       }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>



                        <br>
                        <div class="form-container">
                           <h4 class="form-group-title">Discount Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Room</th>
                                       <th>Apartment</th>
                                       <th>Unit</th>
                                       <th>Hostel</th>
                                       <th>From Date</th>
                                       <th>To Date</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($hostelRoomDetails))
                                       {
                                         $i=1;
                                         foreach ($hostelRoomDetails as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->room_code . " - " . $record->room_name ?></td>
                                       <td><?php echo $record->apartment_code . " - " . $record->apartment_name ?></td>
                                       <td><?php echo $record->unit_code . " - " . $record->unit_name ?></td>
                                       <td><?php echo $record->hostel_code . " - " . $record->hostel_name ?></td>
                                       <td><?php echo date('d-m-Y', strtotime($record->from_dt)) ?></td>
                                       <td><?php echo date('d-m-Y', strtotime($record->to_dt)) ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                       }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div> -->

                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_2">
                     <div class="mt-4">

                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Type</th>
                                    <th>ID Type</th>
                                    <th>Reason</th>
                                    <th>Date</th>
                                    <th>User</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($barrReleaseByStudentId)) {
                                        $i=1;
                                        foreach ($barrReleaseByStudentId as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->type ?></td>
                                    <td><?php echo $record->barring_code . " - " . $record->barring_name ?></td>
                                    <td><?php echo $record->reason ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_3">
                     <div class="mt-4">



                      <div class="form-container">
                        <h4 class="form-group-title">Program Registration Details</h4>



                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Programme Code</th>
                                    <th>Programme Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Registered On</th>
                                    <th>Invoice</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($studentHasProgramme)) {
                                        $i=1;
                                        foreach ($studentHasProgramme as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->programme_code; ?></td>
                                    <td><?php echo $record->programme_name; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->start_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->end_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->invoice_number ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                          

                      </div>





                        
                        <!-- <?php
                           if(!empty($courseRegisteredLandscapeFBySemester))
                           {
                               ?> <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                 <?php
                                    if (!empty($courseRegisteredLandscapeFBySemester))
                                    {
                                        foreach ($courseRegisteredLandscapeFBySemester as $record)
                                        {
                                    ?>
                                 <td>
                                    <h3><?php echo $record->code . " - " . $record->name; ?></h3>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                
                                 <div class="custom-table" id="printReceipt">
                                    <table class="table" id="list-table">
                                       <thead>
                                          <tr>
                                             <th>Sl. No</th>
                                             <th>Course Code</th>
                                             <th>Course name</th>
                                             <th>Credit Hours</th>
                                             <th>Status</th>                                        
                                             <th>Withdraw</th>                                        
                                             <th>Grade</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             if (!empty($record->course)) {
                                                 $i=1;
                                                 foreach ($record->course as $record_course) {
                                             ?>
                                          <tr>
                                             <td><?php echo $i ?></td>
                                             <td><?php echo $record_course->course_code ?></td>
                                             <td><?php echo $record_course->course_name ?></td>
                                             <td><?php echo $record_course->credit_hours ?></td>

                                             <td>
                                                <?php if($record_course->is_result_announced > 0)
                                                   {
                                                       echo $record_course->total_result;
                                                   }
                                                   elseif($record_course->is_exam_registered > 0)
                                                   {
                                                       echo 'Exam Registered';
                                                   }
                                                   else{
                                                       echo "Exam Not Registered";
                                                   } ?>
                                             </td>
                                             <td><?php
                                             if($record_course->is_bulk_withdraw > 0)
                                                   {
                                                       echo 'Withdraw';
                                                   }
                                              ?></td>
                                             <td><?php echo $record_course->grade ?></td>

                                          </tr>
                                          <?php
                                             $i++;
                                                 }
                                             }
                                             ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <?php
                                    }
                                    }
                                    ?>
                                 </td>
                              </tr>
                              </tbody>
                           </table>
                        </div><?php
                         }
                       ?> -->


                     </div>

                  </div>


                  <div role="tabpanel" class="tab-pane active" id="tab_4">
                     <div class="mt-4">
                        <div class="form-container">
                           <h4 class="form-group-title">Student Status</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Date</th>
                                       <th>Reason</th>
                                       <th>By</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    <tr>
                                       <td><?php echo '1'; ?></td>
                                       <td><?php 
                                       if($studentStatus->id_applicant > 0)
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                       else
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                         ?>     
                                       </td>
                                       <td> </td>
                                       <td><?php
                                       if($studentStatus->added_by_partner_university > 0)
                                       {
                                        echo "Partner University";
                                       }
                                       else
                                       {
                                          echo 'Administrator';
                                       } ?></td>
                                       <td>Active</td>
                                    </tr>
                                    <?php
                                    if(!empty($applyChangeStatusListByStudentId))
                                    {
                                      $j=1;
                                        foreach ($applyChangeStatusListByStudentId as $record)
                                        {
                                    ?>
                                   <tr>
                                      <td><?php echo $j+1 ?></td>
                                      <td><?php echo date('d-m-Y H:i:s',strtotime($record->created_dt_tm)); ?></td>
                                      <td><?php echo $record->change_status ?></td>
                                      <td><?php echo $record->created_by ?></td>
                                      <td>
                                      <?php 
                                         if($record->status == 0)
                                         {
                                             echo "Pending"; 
                                         }if($record->status == 1)
                                         {
                                             echo "Approved"; 
                                         }if($record->status == 2)
                                         {
                                             echo "Rejected"; 
                                         }?>
                                         
                                      </td>
                                   </tr>
                                   <?php
                                      $j++;
                                      }
                                    }
                                    ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  
                  </div>


                  <div role="tabpanel" class="tab-pane" id="tab_5">
                     <div class="mt-4">


                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Semester Code</th>
                                    <th>Semester Name</th>
                                    <th>Status</th>
                                    <th>Remarks</th>
                                    <th>Date</th>
                                    <th>Registered By</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($courseRegisteredList)) {
                                        $i=1;
                                        foreach ($courseRegisteredList as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->semester_code ?></td>
                                    <td><?php echo $record->semester_name ?></td>
                                    <td>
                                    <?php 
                                       if($record->status == 0)
                                       {
                                           echo "Pending"; 
                                       }if($record->status == 1)
                                       {
                                           echo "Approved"; 
                                       }if($record->status == 2)
                                       {
                                           echo "Rejected"; 
                                       }?>
                                       
                                    </td>
                                    <td><?php echo '';
                                    // $record->remarks
                                     ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                                    <td><?php echo 'Administrator'; ?></td>
                                    
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>

                     </div>
                  
                  </div>



                  <div role="tabpanel" class="tab-pane" id="tab_6">
                     <div class="mt-4">
                        <div class="custom-table" id="printInvoice">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Advisor Name</th>
                                    <th>Date</th>
                                    <th>Updated By</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($advisorTaggingDetails)) {
                                        $i=1;
                                        foreach ($advisorTaggingDetails as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->ic_no . " - " . $record->advisor_name  ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_7">
                     <div class="mt-4">
                        <div class="form-container">
                           <h4 class="form-group-title">Course Landscape Details</h4>
                           <div class="custom-table" id="printReceipt">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Course Type</th>
                                       <th>Course Name</th>
                                       <th>Credit Hours</th>
                                       <th>Status</th>
                                       <th>Grade</th>
                                       <th>Remarks</th>
                                       <!-- <th>Barring To Date</th> -->
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($courseRegisteredLandscape)) {
                                           $i=1;
                                           foreach ($courseRegisteredLandscape as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->course_type ?></td>
                                       <td><?php echo $record->course_code . " - " . $record->course_name ?></td>
                                       <td><?php echo $record->credit_hours ?></td>
                                       <td><?php if($record->is_result_announced > 0)
                                          {
                                              echo $record->total_result;
                                          }
                                          elseif($record->is_bulk_withdraw > 0)
                                          {
                                              echo 'Withdraw';
                                          }elseif($record->is_exam_registered > 0)
                                          {
                                              echo 'Exam Registered';
                                          }elseif($record->is_registered > 0)
                                          {
                                              echo 'Course Registered';
                                          }
                                          else{
                                              echo "Not Registered";
                                          } ?></td>
                                       <td><?php if($record->is_result_announced > 0)
                                          {
                                           echo $record->grade; 
                                          } ?></td>
                                       <td><?php echo ""; ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                           }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>

                        
                        <?php
                           if($courseRegisteredLandscape && $landscapeCount)
                           {
                               ?>
                        <br>
                        <div class="form-container">
                           <h4 class="form-group-title">Total Credit Hours Details</h4>
                           <div class='data-list'>
                              <div class='row'>
                                 <div class='col-sm-6'>
                                    <dl>
                                       <dt>Total Compulsary  :</dt>
                                       <dd><?php echo ucwords($landscapeCount['compulsary']);?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Total Not Compulsary :</dt>
                                       <dd><?php echo $landscapeCount['notCompulsary']; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Total Major :</dt>
                                       <dd><?php echo $landscapeCount['manjor']; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Total Minor :</dt>
                                       <dd><?php echo $landscapeCount['minor']; ?></dd>
                                    </dl>
                                 </div>
                                 <div class='col-sm-6'>
                                    <dl>
                                       <dt>Total Earned Compulsary :</dt>
                                       <dd><?php echo $landscapeRegisteredCount['compulsary']; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Total Earned Not Compulsary :</dt>
                                       <dd><?php echo $landscapeRegisteredCount['notCompulsary']; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Total Earned Major :</dt>
                                       <dd><?php echo $landscapeRegisteredCount['manjor']; ?></dd>
                                    </dl>
                                    <dl>
                                       <dt>Total Earned Not Major :</dt>
                                       <dd><?php echo $landscapeRegisteredCount['minor']; ?></dd>
                                    </dl>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php
                           }
                           
                           ?>
                     </div>
                  
                  </div>



                  <div role="tabpanel" class="tab-pane" id="tab_8">
                     <div class="mt-4">


                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Institution Type</th>
                                    <th>Course / Subject</th>
                                    <th>Credit Hours</th>
                                    <th>Remarks</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($creditTransferByStudentId)) {
                                        $i=1;
                                        foreach ($creditTransferByStudentId as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->institution_type ?></td>
                                    <td><?php 
                                       if($record->institution_type == 'Internal')
                                       {
                                           echo $record->course_code . " - " . $record->course_code; 
                                       }else
                                       {
                                           echo $record->subject_course;
                                       }?></td>
                                    <td><?php echo $record->credit_hours ?></td>
                                    <td><?php echo $record->remarks ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>
                                    <td><?php 
                                       if($record->status == 0)
                                       {
                                           echo "Pending"; 
                                       }if($record->status == 1)
                                       {
                                           echo "Approved"; 
                                       }if($record->status == 2)
                                       {
                                           echo "Rejected"; 
                                       }?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>



                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_9">
                     <div class="mt-4">
                        <div class="form-container">
                           <h4 class="form-group-title">Visa Details</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Is Malaysian Visa</th>
                                       <th>Visa Number</th>
                                       <th>Expire Date</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       if (!empty($visaDetails))
                                       {
                                         $i=1;
                                         foreach ($visaDetails as $record) {
                                       ?>
                                    <tr>
                                       <td><?php echo $i ?></td>
                                       <td><?php echo $record->malaysian_visa  ?>     
                                       </td>
                                       <td><?php echo $record->visa_number ?></td>
                                       <td><?php echo date('d-m-Y',strtotime($record->visa_expiry_date)); ?></td>
                                    </tr>
                                    <?php
                                       $i++;
                                         }
                                       }
                                       ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  
                  </div>



                  <div role="tabpanel" class="tab-pane" id="tab_10">

                     <?php 

                     if($studentDetails->qualification == 'MASTER' || $studentDetails->qualification == 'POSTGRADUATE')
                     {
                     ?>

                     <br>

                     <div class="page-title clearfix">
                         <a href="<?php echo '/records/studentRecord/generateDeliverableReport/'.$studentDetails->id ?>" target="_blank" class="btn btn-link btn-back">
                             Download Phd Deliverables >>></a>
                     </div>

                     <br>

                     <?php
                     }
                     ?>



                     <div class="mt-4">
                        <div class="custom-table">
                           <table class="table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Old Phd Duration</th>
                                    <th>New Phd Duration</th>
                                    <th>Date</th>
                                    <th>Updated By</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($phdDurationTaggingDetails)) {
                                        $i=1;
                                        foreach ($phdDurationTaggingDetails as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->old_phd_duration; ?></td>
                                    <td><?php echo $record->new_phd_duration; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->created_by; ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                     
                     </div>

                  </div>

        

           </div>
        </div>
     </div>
  </form>
</div>
   
</div>

   <footer class="footer-wrapper">
      <p>&copy; 2019 All rights, reserved</p>
   </footer>
<script type="text/javascript">
   function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
   }
</script>
<script>
   saveData();
   
   function saveData()
   {
       // if($("#note").val() != '')
       // {
   
       var tempPR = {};
       tempPR['note'] = $("#note").val();
       tempPR['id_student'] = <?php echo $studentDetails->id ?>;
           $.ajax(
           {
              url: '/records/studentRecord/addStudentNote',
               type: 'POST',
              data:
              {
               tempData: tempPR
              },
              error: function()
              {
               alert('Something is wrong');
              },
              success: function(result)
              {
               // alert(result);
               // window.location.reload();
               $("#view_student_note").html(result);
               // $('#myModal').modal('hide');
              }
           });
       // }
   }
   
   
   function deleteStudentNote(id)
   {
   
       var tempPR = {};
       tempPR['id'] = id;
       tempPR['id_student'] = <?php echo $studentDetails->id ?>;
           $.ajax(
           {
              url: '/records/studentRecord/deleteStudentNote',
               type: 'POST',
              data:
              {
               tempData: tempPR
              },
              error: function()
              {
               alert('Something is wrong');
              },
              success: function(result)
              {
               // alert(result);
               // window.location.reload();
               $("#view_student_note").html(result);
               // $('#myModal').modal('hide');
              }
           });
   }
   
   
   
    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               note:
               {
                   required: true
               }         
           },
           messages:
           {
               note:
               {
                   required: "<p class='error-text'>Note Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
   
   
</script>