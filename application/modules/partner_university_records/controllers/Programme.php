<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Programme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_model');
        $this->isPartnerUniversityLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        $formData['name'] = $this->security->xss_clean($this->input->post('name'));
        $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category_type'));
        $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
        $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
        $formData['id_partner_university'] = $this->session->id_partner_university;


        $data['searchParam'] = $formData;

        // $data['programmeList'] = $this->programme_model->programmeList();
        $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
        $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
        $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');

        $data['programmeList'] = $this->programme_model->programmeListSearch($formData);

        $this->global['pageTitle'] = 'Inventory Management : Program List';
        $this->loadViews("programme/list", $this->global, $data, NULL);
    }
    
    function add()
    {
        $id_session = $this->session->my_session_id;
        $id_partner_university = $this->session->id_partner_university;

        if($this->input->post())
        {                

            // For file validation from 36 to 44 , file size validationss
            // echo "<Pre>";print_r($this->input->post());exit;

            // echo "<Pre>"; print_r($_FILES['image']);exit;
            if($_FILES['image'])
            {

                $certificate_name = $_FILES['image']['name'];
                $certificate_size = $_FILES['image']['size'];
                $certificate_tmp =$_FILES['image']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
            }


            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $total_cr_hrs = $this->security->xss_clean($this->input->post('total_cr_hrs'));
            $internal_external = $this->security->xss_clean($this->input->post('internal_external'));
            $status = $this->security->xss_clean($this->input->post('status'));
            
            $id_category = $this->security->xss_clean($this->input->post('id_category'));
            $id_category_setup = $this->security->xss_clean($this->input->post('id_category_setup'));
            $id_programme_type = $this->security->xss_clean($this->input->post('id_programme_type'));
            $max_duration = $this->security->xss_clean($this->input->post('max_duration'));
            $duration_type = $this->security->xss_clean($this->input->post('duration_type'));
            $trending = $this->security->xss_clean($this->input->post('trending'));
            $target_audience = $this->security->xss_clean($this->input->post('target_audience'));
            $student_learning_hours = $this->security->xss_clean($this->input->post('student_learning_hours'));

            $short_description = $this->security->xss_clean($this->input->post('short_description'));

            $id_delivery_mode = $this->security->xss_clean($this->input->post('id_delivery_mode'));
            $id_study_level = $this->security->xss_clean($this->input->post('id_study_level'));
            $sold_separately = $this->security->xss_clean($this->input->post('sold_separately'));



            // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        
            $data = array(
                'name' => $name,
                'code' => $code,
                'total_cr_hrs' => $total_cr_hrs,
                'target_audience' => $target_audience,
                'student_learning_hours' => $student_learning_hours,
                'internal_external' => 'External',
                'id_category' => $id_category,
                'id_study_level' => $id_study_level,
                'id_delivery_mode' => $id_delivery_mode,
                'id_category_setup' => $id_category_setup,
                'id_programme_type' => $id_programme_type,
                'max_duration' => $max_duration,
                'duration_type' => $duration_type,
                'trending' => $trending,
                'id_partner_university'=>$id_partner_university,
                'status' => $status,
                'short_description' => $short_description,
                'sold_separately' => $sold_separately,
                'updated_by' => $id_user
            );


            // echo "<Pre>"; print_r($data);exit;

            if($image_file)
            {
                $data['image'] = $image_file;
            }

            $inserted_id = $this->programme_model->addNewProgrammeDetails($data);

            if($inserted_id)
            {
                $details_data['id_programme'] = $inserted_id;

                $this->programme_model->saveProgramObjectiveData($details_data);
                $this->programme_model->saveCourseOverviewData($details_data);
                $this->programme_model->saveDeliveryModeData($details_data);
                $this->programme_model->saveAssesmentData($details_data);
                $this->programme_model->saveSyllabusData($details_data);
            }

            $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session);
             for($i=0;$i<count($temp_details);$i++)
             {
                // echo "<Pre>";print_r($temp_details[$i]);exit;

                $id_staff = $temp_details[$i]->id_staff;
                $id_programme = $temp_details[$i]->id_programme;
                $effective_start_date = $temp_details[$i]->effective_start_date;

                 $detailsData = array(
                    'id_programme' => $inserted_id,
                    'id_staff' => $id_staff,
                    // 'id_programme' => $id_programme,
                    'effective_start_date' => date('Y-m-d',strtotime($effective_start_date)),
                );
                // echo "<Pre>";print_r($detailsData);exit;
                $result = $this->programme_model->addNewProgrammeHasDean($detailsData);
             }

            redirect('/partner_university_prdtm/programme/edit/'. $inserted_id);
        }
        else
        {
            $this->programme_model->deleteTempProgHasDeanDataBySession($id_session);
        }

        $data['staffList'] = $this->programme_model->staffListByStatus('1');
        $data['awardList'] = $this->programme_model->awardLevelListByStatus('1');
        $data['schemeList'] = $this->programme_model->schemeListByStatus('1');

        $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
        $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
        $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');
        $data['partnerList'] = $this->programme_model->partnerUniversityListByStatus('1');

        $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

        $data['studyLevelList'] = $this->programme_model->getStudyLevel();


        $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');
        
        // echo "<Pre>";print_r($data['programmeDetails']);exit();

        $this->global['pageTitle'] = 'Inventory Management : Add Programme';
        $this->loadViews("programme/add", $this->global, $data, NULL);
    }


    function edit($id = NULL)
    {
        if ($id == null)
        {
            redirect('/setup/programme/list');
        }
        if($this->input->post())
        {

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $total_cr_hrs = $this->security->xss_clean($this->input->post('total_cr_hrs'));
            $internal_external = $this->security->xss_clean($this->input->post('internal_external'));
            $status = $this->security->xss_clean($this->input->post('status'));
            
            $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
            $id_category = $this->security->xss_clean($this->input->post('id_category'));
            $id_category_setup = $this->security->xss_clean($this->input->post('id_category_setup'));
            $id_programme_type = $this->security->xss_clean($this->input->post('id_programme_type'));
            $max_duration = $this->security->xss_clean($this->input->post('max_duration'));
            $duration_type = $this->security->xss_clean($this->input->post('duration_type'));
            $trending = $this->security->xss_clean($this->input->post('trending'));
            $target_audience = $this->security->xss_clean($this->input->post('target_audience'));
            $student_learning_hours = $this->security->xss_clean($this->input->post('student_learning_hours'));

            $short_description = $this->security->xss_clean($this->input->post('short_description'));

            $id_delivery_mode = $this->security->xss_clean($this->input->post('id_delivery_mode'));
            $id_study_level = $this->security->xss_clean($this->input->post('id_study_level'));
            $sold_separately = $this->security->xss_clean($this->input->post('sold_separately'));



            // $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        
            $data = array(
                'name' => $name,
                'code' => $code,
                'total_cr_hrs' => $total_cr_hrs,
                'target_audience' => $target_audience,
                'student_learning_hours' => $student_learning_hours,
                'internal_external' => $internal_external,
                'id_category' => $id_category,
                'id_study_level' => $id_study_level,
                'id_delivery_mode' => $id_delivery_mode,
                'id_category_setup' => $id_category_setup,
                'id_programme_type' => $id_programme_type,
                'max_duration' => $max_duration,
                'duration_type' => $duration_type,
                'trending' => $trending,
                'id_partner_university'=>$id_partner_university,
                'status' => $status,
                'short_description' => $short_description,
                'sold_separately' => $sold_separately,
                'updated_by' => $id_user
            );

            // echo "<Pre>";print_r($_FILES['image']);exit;
            
            if($_FILES['image'])
            {

                $certificate_name = $_FILES['image']['name'];
                $certificate_size = $_FILES['image']['size'];
                $certificate_tmp =$_FILES['image']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // echo "<Pre>"; print_r($image_file);exit();
            }

            if($image_file)
            {
                $data['image'] = $image_file;
            }


            // echo "<Pre>";print_r($data);exit;
            $result = $this->programme_model->editProgrammeDetails($data,$id);
            redirect('/partner_university_prdtm/programme/list');
        }
        $data['id_programme'] = $id;
        $data['staffList'] = $this->programme_model->staffListByStatus('1');
        $data['programmeHasDeanList'] = $this->programme_model->getProgrammeHasDean($id);
        $data['programmeHasCourseList'] = $this->programme_model->getProgrammeHasCourse($id);
        $data['awardList'] = $this->programme_model->awardLevelListByStatus('1');
        $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
        $data['programTypeList'] = $this->programme_model->programTypeListByStatus('1');
        // $data['schemeList'] = $this->programme_model->schemeListByStatus('1');
        $data['educationLevelList'] = $this->programme_model->educationLevelListByStatus('1');

        $data['categoryList'] = $this->programme_model->categoryListByStatus('1');
        $data['categoryTypeList'] = $this->programme_model->categoryTypeListByStatus('1');
        $data['productTypeSetupList'] = $this->programme_model->productTypeSetupListByStatus('1');

        $data['deliveryModeList'] = $this->programme_model->getDeliveryMode();

        $data['studyLevelList'] = $this->programme_model->getStudyLevel();

        $data['partnerList'] = $this->programme_model->partnerUniversityListByStatus('1');
        

        $data['programmeHasDeanList'] = $this->programme_model->getProgrammeHasDean($id);
        $data['programmeObjective'] = $this->programme_model->programmeObjective($id);
        $data['programmeCourseOverview'] = $this->programme_model->programmeCourseOverview($id);
        $data['programmeDeliveryMode'] = $this->programme_model->programmeDeliveryMode($id);
        $data['programmeAssesment'] = $this->programme_model->programmeAssesment($id);
        $data['programmeSyllabus'] = $this->programme_model->programmeSyllabus($id);


        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);

        $data['id_programme'] = $id;
        
        // echo "<Pre>";print_r($data['programmeDetails']);exit;
        $this->global['pageTitle'] = 'Inventory Management : Edit Programme';
        $this->loadViews("programme/edit", $this->global, $data, NULL);
    }


    function syllabus($id)
    {
        $data = array();

         if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $learning_objective = $this->security->xss_clean($this->input->post('learning_objective'));
                $data = array();
                $data['learning_objective'] = $learning_objective;
                $data['id_programme'] = $id;

                $result = $this->programme_model->addsyllabus($data);
              
                redirect("/partner_university_/programme/syllabus/".$id);
            }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['syllabus'] = $this->programme_model->getProgrammeSyllabus($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/syllabus", $this->global, $data, NULL);
    }


    function overview($id)
    {
        $data = array();

        if($this->input->post())
        {


                $result = $this->programme_model->deleteprogramoverview($id);
                $overview = $this->security->xss_clean($this->input->post('overview'));
                             
                $data = array();
                $data['overview'] = $overview;
                $data['id_programme'] = $id;

                $result = $this->programme_model->addProgramOverview($data);
            
            redirect("/partner_university_prdtm/programme/overview/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['overview'] = $this->programme_model->getProgramOverview($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/overview", $this->global, $data, NULL);
    }


    function assessment($id)
    {
        $data = array();

        if($this->input->post())
        {

            $total_weightage_percentage = $this->security->xss_clean($this->input->post('total_weightage_percentage'));
            $passing_mark_percentage = $this->security->xss_clean($this->input->post('passing_mark_percentage'));
                            
            $datas = array();

            if($passing_mark_percentage>0 && $total_weightage_percentage>0)
            {

                $result = $this->programme_model->deleteassessmentmain($id);


                $datas['total_weightage_percentage'] = $total_weightage_percentage;
                $datas['passing_mark_percentage'] = $passing_mark_percentage;
                $datas['id_programme'] = $id;


                $result = $this->programme_model->addassessmentmain($datas);
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            $id_examination_components = $this->security->xss_clean($this->input->post('id_examination_components'));
            $weightage = $this->security->xss_clean($this->input->post('weightage'));
            $total_mark = $this->security->xss_clean($this->input->post('total_mark'));
            $passing_mark = $this->security->xss_clean($this->input->post('passing_mark')); 

            if($weightage>0 && $total_mark>0)
            {                   
                $data = array();
                $data['weightage'] = $weightage;
                $data['total_mark'] = $total_mark;
                $data['passing_mark'] = $passing_mark;
                $data['id_programme'] = $id;
                $data['id_examination_components'] = $id_examination_components;

                $result = $this->programme_model->addassessment($data);
            }
            redirect("/partner_university_prdtm/programme/assessment/".$id);
        }


        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['assessmentDetailsList'] = $this->programme_model->getAssessmentDetails($id);
        $data['assessmentDetailsListMain'] = $this->programme_model->getAssessmentDetailsMain($id);
        $data['examinationComponent'] = $this->programme_model->getExaminationComponent();

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/assessment", $this->global, $data, NULL);
    }

    function faculty($id)
    {
        $data = array();

         if($this->input->post())
        {



            if($_POST['save']=='Search') {
            $data['facultySearchList'] = $this->programme_model->fetFacultySearch();


            }

            if($_POST['save']=='Add') {

                for($l=0;$l<count($_POST['faculty']);$l++) {

                    $id_staff = $_POST['faculty'][$l];
                    $dataprogram['id_programme'] = $id;
                    $dataprogram['id_staff'] = $id_staff;
            $result = $this->programme_model->addProgrammetoStaff($dataprogram);

                }
            }           
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['facultyList'] = $this->programme_model->getProgrammeHasDean($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/faculty", $this->global, $data, NULL);
    }


   function topic($id)
   {
        $data = array();

         if($this->input->post())
        {

            $id_programme_has_syllabus=0;
   
            for($j=0;$j<count($_POST['id_programme_has_syllabus']);$j++)
            {
                $id_programme_has_syllabus = $id_programme_has_syllabus.','.$_POST['id_programme_has_syllabus'][$j];
            }

            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            $topic = $this->security->xss_clean($this->input->post('topic'));

            $message= $this->security->xss_clean($this->input->post('message'));
            $data = array();
            $data['topic'] = $topic;
            $data['id_programme'] = $id;
            $data['id_programme_has_syllabus'] = $id_programme_has_syllabus;
            $data['message'] = $message;

            // print_r($)
            $result = $this->programme_model->addtopic($data);
            redirect("/partner_university_prdtm/programme/topic/".$id);
        }


        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['syllabus'] = $this->programme_model->getProgrammeSyllabus($id);
        $data['topic'] = $this->programme_model->getprogrammeTopic($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/topic", $this->global, $data, NULL);
    }


    function updatefacilitator($id,$idprogramme)
    {
        $this->programme_model->updateAllFacilitatorToInactive($idprogramme);
        $this->programme_model->updateFacilitatorToActive($id);
    }


    function accreditation($id)
    {
        $data = array();

         if($this->input->post())
        {
            redirect("/partner_university_prdtm/programme/accreditation/".$id);
        }


        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeAcceredationList'] = $this->programme_model->programmeAcceredationList($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/accreditation", $this->global, $data, NULL);
    }


    function award($id)
    {
        $data = array();

         if($this->input->post())
        {
            redirect("/partner_university_prdtm/programme/award/".$id);
        }


        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeAwardList'] = $this->programme_model->programmeAwardList($id);
        $data['awardList'] = $this->programme_model->awardListByStatus('1');
        $data['programmeConditionList'] = $this->programme_model->programmeConditionListByStatus('1');

        // echo "<Pre>";print_r($data['programmeAwardList']);exit;


        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/award", $this->global, $data, NULL);
    }

    function structure($id)
    {
        $data = array();

        if($this->input->post())
        {
            $result = $this->programme_model->deleteProgramStructure($id);
            $structure = $this->security->xss_clean($this->input->post('structure'));
                         
            $data = array();
            $data['structure'] = $structure;
            $data['id_programme'] = $id;

            $result = $this->programme_model->addProgramStructure($data);
            
            redirect("/partner_university_prdtm/programme/structure/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['structure'] = $this->programme_model->getProgramStructure($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/structure", $this->global, $data, NULL);

    }

    function aim($id)
    {
        $data = array();

        if($this->input->post())
        {
            $result = $this->programme_model->deleteProgramAim($id);
            $aim = $this->security->xss_clean($this->input->post('aim'));
                         
            $data = array();
            $data['aim'] = $aim;
            $data['id_programme'] = $id;

            $result = $this->programme_model->addProgramAim($data);
            
            redirect("/partner_university_prdtm/programme/aim/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['aim'] = $this->programme_model->getProgramAim($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/aim", $this->global, $data, NULL);
    }

    function modules($id)
    {
        $data = array();

        if($this->input->post())
        {
            // $result = $this->programme_model->deleteProgramModules($id);
            $id_child_programme = $this->security->xss_clean($this->input->post('id_child_programme'));
                         
            $data = array();
            $data['id_child_programme'] = $id_child_programme;
            $data['id_programme'] = $id;

            $result = $this->programme_model->addProgramModules($data);
            
            redirect("/partner_university_prdtm/programme/modules/".$id);
        }

        $data['id_programme'] = $id;
        $data['programmeDetails'] = $this->programme_model->getProgrammeDetails($id);
        $data['programmeList'] = $this->programme_model->getProgrammeListByIdCategory('1');
        $data['programHasModules'] = $this->programme_model->getProgramHasModules($id);

        $this->global['pageTitle'] = 'Speed Management System : Edit Programme';
        $this->loadViews("programme/modules", $this->global, $data, NULL);
    }


    function saveAcceredationData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['acceredation_dt'] = date('Y-m-d', strtotime($tempData['acceredation_dt']));
        $tempData['valid_from'] = date('Y-m-d', strtotime($tempData['valid_from']));
        $tempData['valid_to'] = date('Y-m-d', strtotime($tempData['valid_to']));
        $tempData['approval_dt'] = date('Y-m-d', strtotime($tempData['approval_dt']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->addNewProgrammeHasAcceredation($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }


    function deleteAcceredationDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteAcceredationDetails($id_details);
        
        echo "Success"; 
    }

    function saveAwardData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $inserted_id = $this->programme_model->saveAwardData($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteAwardData($id_details)
    {
        $inserted_id = $this->programme_model->deleteAwardData($id_details);
        echo "Success"; 
    }

    function deleteProgramHasModules($id)
    {
        $inserted_id = $this->programme_model->deleteProgramHasModules($id);
        echo $inserted_id; 
    }
    
    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        $tempData['effective_start_date'] = date('Y-m-d',strtotime($tempData['effective_start_date']));
        
            $inserted_id = $this->programme_model->addTempDetails($tempData);
        // }
        $data = $this->displaytempdata();
        
        echo $data;
    }

    function tempaddcourse()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->programme_model->updateTempCourseDetails($tempData,$id);
        }
        else
        {

            unset($tempData['id']);
            $inserted_id = $this->programme_model->addTempCourseDetails($tempData);
        }
        $data = $this->displaytempCoursedata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . " - " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;
                    $effective_start_date = date('d-m-Y',strtotime($effective_start_date));
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$staff</td>
                            <td>$effective_start_date</td>                            
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function displaytempCoursedata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasCourse($id_session); 
        // echo "<Pre>";print_r($details);exit;
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Name</th>
                    <th class='text-centre'>Action</th>
                </tr>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $name = $temp_details[$i]->courseName;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>            
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempCourseData($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "</table>";
        return $table;
    }

    function tempDetailsDataAdd()
    {
        
        $id_session = $this->session->my_session_id;

        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));
             
        // echo "<Pre>";  print_r($effective_start_date);exit;
        $data = array(
            'id_session' => $id_session,
            'id_staff' => $id_staff,
            'effective_start_date' => $effective_start_date
        );   
        $inserted_id = $this->programme_model->addNewTempProgrammeHasDean($data);

        $temp_details = array(
                'id' => $inserted_id,
                'id_staff' => $id_staff,
                'effective_start_date' => date('Y-m-d',strtotime($effective_start_date))
            );

        $temp_details = $this->programme_model->getTempProgrammeHasDean($id_session);
        // echo "<Pre>";  print_r($temp_details);exit;

        if(!empty($temp_details))
        {  
            $table = "
            <table  class='table' id='list-table'>
                <tr>
                    <th>Sl. No</th>
                    <th>Staff Name</th>
                    <th>Effective Date</th>
                    <th>Delete</th>
                </tr>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $staff = $temp_details[$i]->salutation . " - " . $temp_details[$i]->staff;
                    $effective_start_date = $temp_details[$i]->effective_start_date;

                    $table .= "
                <tr>
                    <td>$i+1</td>
                    <td>$staff</td>
                    <td>$effective_start_date</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </table>";
            echo $table;
        }
    }

    function add_new_dean_on_edit()
    {
        // echo "<Pre>";print_r("jk");exit;
        $id_programme= $_POST['id_programme'];
        $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
        $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));

         $detailsData = array(
            'id_programme' => $id_programme,
            'id_staff' => $id_staff,
            // 'id_programme' => $id_programme,
            'effective_start_date' => date('Y-m-d',strtotime($effective_start_date)),
        );
        // echo "<Pre>";print_r($detailsData);exit;
        $result = $this->programme_model->addNewProgrammeHasDean($detailsData);
        // $data['programmeHasDeanList'] = $this->programme_has_dean_model->getProgrammeHasDeanByProgrammeId($id);
        redirect($_SERVER['HTTP_REFERER']);

    }

    function delete_programme_has_dean()
    {
        $id = $this->input->get('id');
        // echo "<Pre>";print_r($id);exit;

       $this->programme_model->deleteProgrammeHasDean($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function delete_programme_has_course()
    {
        $id = $this->input->get('id');
        // echo "<Pre>";print_r($id);exit;

       $this->programme_model->deleteProgrammeHasCourse($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function tempedit($id){
        $data = $this->programme_model->getTempDetails($id);
        echo json_encode($data);

    }

    function tempDelete($id)
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->programme_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    }

    function tempCourseDelete($id)
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->programme_model->deleteTempCourseData($id);
        $data = $this->displaytempCoursedata();
        echo $data; 
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_staff'] =  $tempData['id_staff'];
        $data['effective_start_date'] =  date('Y-m-d',strtotime($tempData['effective_start_date']));
        $data['id_programme'] =  $tempData['id'];
        $inserted_id = $this->programme_model->addNewProgrammeHasDean($data);

        if($inserted_id)
        {
            echo "success";exit;
        }
        
        //  $temp_details = $this->programme_model->getProgrammeHasDean($tempData['id']);
        // if(!empty($temp_details))
        // {

        // // echo "<Pre>";print_r($temp_details);exit;
        // $table = "<table  class='table' id='list-table'>
        //           <tr>
        //             <th>Sl. No</th>
        //             <th>Staff Name</th>
        //             <th>Effective Start Date</th>
        //             <th>Action</th>
        //         </tr>";
        //             for($i=0;$i<count($temp_details);$i++)
        //             {
        //             $id = $temp_details[$i]->id;
        //             $staff_name = $temp_details[$i]->salutation . " - " .$temp_details[$i]->staff;
        //             $effective_start_date = $temp_details[$i]->effective_start_date;
        //             $effective_start_date = date('d-m-Y',strtotime($effective_start_date));
        //             $j = $i+1;
        //                 $table .= "
        //                 <tr>
        //                     <td>$j</td>
        //                     <td>$staff_name</td>                         
        //                     <td>$effective_start_date</td>                         
        //                     <td>
        //                         <span onclick='deleteStaffDetailData($id)'>Delete</a>
        //                     <td>
        //                 </tr>";
        //             }
        // $table.= "</table>";

        // }
        // else
        // {
        //     $table="";
        // }

        // echo $table;           
    }

    function deleteStaffDetailData($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgrammeHasDean($id_details);
        
        echo "Success"; 
    }

    function tempSchemeAdd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        
        $inserted_id = $this->programme_model->addTempProgramHasScheme($tempData);
        
        $data = $this->displayTempSchemeData();
        
        echo $data;        
    }


    function displayTempSchemeData()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->programme_model->getTempProgrammeHasScheme($id_session); 

        if(!empty($temp_details))
        {

        // echo "<Pre>";print_r($details);exit;
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Scheme</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $scheme = $temp_details[$i]->scheme_code . " - " . $temp_details[$i]->scheme_name;
                    $j = $i+1;
                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$scheme</td>                           
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempProgramHasScheme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempProgramHasScheme($id)
    {
        $inserted_id = $this->programme_model->deleteTempProgramHasScheme($id);
        $data = $this->displayTempSchemeData();
        echo $data; 
    }




    function saveProgramObjectiveData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveProgramObjectiveData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveCourseOverviewData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveCourseOverviewData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveDeliveryModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveDeliveryModeData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveAssesmentData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveAssesmentData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function saveSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->saveSyllabusData($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteProgramObjectiveDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteProgramObjectiveDetails($id_details);
        
        echo "Success"; 
    }

    function deleteCourseOverviewDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteCourseOverviewDetails($id_details);
        
        echo "Success"; 
    }

    function deleteDeliveryModeDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteDeliveryModeDetails($id_details);
        
        echo "Success"; 
    }

    function deleteAssesmentDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteAssesmentDetails($id_details);
        
        echo "Success"; 
    }

    function deleteSyllabusDetails($id_details)
    {
        $inserted_id = $this->programme_model->deleteSyllabusDetails($id_details);
        
        echo "Success"; 
    }



    function updateProgramObjectiveData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);

        // echo "<Pre>"; print_r($tempData);exit();
        
        $inserted_id = $this->programme_model->updateProgramObjectiveData($tempData,$id);

        // echo "<Pre>"; print_r($inserted_id);exit();
        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateCourseOverviewData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateCourseOverviewData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateDeliveryModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateDeliveryModeData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateAssesmentData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateAssesmentData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }


    function updateSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        unset($tempData['id']);
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->programme_model->updateSyllabusData($tempData,$id);

        if($inserted_id)
        {
            echo "success";
        }

    }
}