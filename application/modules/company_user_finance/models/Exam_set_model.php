<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_set_model extends CI_Model
{
    function examSetList()
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function examSetListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        if (!empty($search)) {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or instruction  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function checkExamAnswer($id)
    {
        $this->db->select('is_correct_answer');
        $this->db->from('question_has_option');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function getExamSet($id)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewExamSet($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    function saveUserExam($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_question_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    function saveExamAnswer($data)
    {
        $this->db->select('*');
        $this->db->from('user_exam_details');
        $this->db->where('id_question', $data['id_question']);
        $this->db->where('id_userexam', $data['id_userexam']);
        $answer_id = $this->db->get()->row_array();
        $this->db->trans_start();
        if (!empty($answer_id)) {
            $this->db->where('id', $answer_id['Id']);
            $result = $this->db->update('user_exam_details', $data);
        } else {
            $this->db->insert('user_exam_details', $data);
            $result = $this->db->insert_id();
        }
        $this->db->trans_complete();
        return $result;
    }
    function endExam($current_exam_id)
    {
        $this->db->trans_start();

        $this->db->select('count(valid) as marks');
        $this->db->from('user_exam_details');
        $this->db->where('id_userexam', $current_exam_id);
        $result = $this->db->get()->row_array();
        $marks = $result['marks'];

        $this->db->where('id', $current_exam_id);
        $data = array(
            'correct' => $marks,
            'status'  => 1
        );
        $this->db->update('user_exams', $data)              ;

        $this->db->trans_complete();
        return ;
    }
    function failExam($current_exam_id)
    {
        $this->db->trans_start();

        $this->db->select('count(valid) as marks');
        $this->db->from('user_exam_details');
        $this->db->where('id_userexam', $current_exam_id);
        $result = $this->db->get()->row_array();
        $marks = $result['marks'];

        $this->db->where('id', $current_exam_id);
        $data = array(
            'correct' => $marks,
            'status'  => 2
        );
        $this->db->update('user_exams', $data)              ;

        $this->db->trans_complete();
        return ;
    }

    function editExamSet($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_set', $data);
        return $result;
    }
    function getQuestionDetails($id, $exam_id)
    {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $id);
        $question = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('question_has_option');
        $this->db->where('id_question', $id);
        $question['options'] = $this->db->get()->result_array();
        $this->db->select('*');
        $this->db->from('user_exam_details');
        $this->db->where('id_question', $id);
        $this->db->where('id_userexam', $exam_id);
        $answered = $this->db->get()->row_array();
        // echo "<pre>";print_r($answered);die;

        if (!empty($answered)) {
            $question['answered'] = $answered['id_answer'];
        } else {
            $question['answered'] = 0;
        }
        return $question;
    }
    function getFirstQuestionDetails($id)
    {
        $this->db->select('id_question');
        $this->db->from('examset_questions');
        $this->db->where('examset_code', $id);
        $this->db->limit(1);
        $exam_set = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $exam_set['id_question']);
        $question = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('question_has_option');
        $this->db->where('id_question', $exam_set['id_question']);
        $question['options'] = $this->db->get()->result_array();
        return $question;
    }
    function getExamSetQuestions($exam_id)
    {
        $this->db->select('id_question');
        $this->db->from('examset_questions');
        $this->db->where('examset_code', $exam_id);
        $questions = $this->db->get()->result_array();
        return $questions;
    }
}
