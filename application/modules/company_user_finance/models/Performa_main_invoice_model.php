<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Performa_main_invoice_model extends CI_Model
{

    function getPerformaMainInvoiceListSearch($data)
    {

        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name, cmp.name as company_name, cmp.registration_number');
        $this->db->from('performa_main_invoice as mi');
        $this->db->join('company as cmp', 'mi.id_student = cmp.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('mi.id_student', $data['id_company']);
        }
        // if ($data['id_company_user'] != '')
        // {
        //     $this->db->where('mi.id_company_user', $data['id_company_user']);
        // }
        if ($data['from_date'] != '')
        {
            $data['from_date'] = date('Y-m-d', strtotime($data['from_date']));
            $this->db->where('mi.date_time >', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $data['to_date'] = date('Y-m-d', strtotime($data['to_date']));
            $this->db->where('mi.date_time <', $data['to_date']);
        }
        if ($data['status'] != '')
        {
            if($data['status'] == '0')
            {
                $this->db->where('mi.id_main_invoice', $data['status']);
            }
            if($data['status'] == '1')
            {
                $this->db->where('mi.id_main_invoice !=', '0');
            }
        }
        $this->db->where("mi.type ", "CORPORATE");
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getPerformaMainInvoiceListSearchForApproval($data)
    {

        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name, cmp.name as company_name, cmp.registration_number');
        $this->db->from('performa_main_invoice as mi');
        $this->db->join('company as cmp', 'mi.id_student = cmp.id');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['invoice_number'] != '')
        {
            $likeCriteria = "(mi.invoice_number  LIKE '%" . $data['invoice_number'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('mi.id_student', $data['id_company']);
        }
        // if ($data['id_company_user'] != '')
        // {
        //     $this->db->where('mi.id_company_user', $data['id_company_user']);
        // }
        if ($data['from_date'] != '')
        {
            $data['from_date'] = date('Y-m-d', strtotime($data['from_date']));
            $this->db->where('mi.date_time >', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $data['to_date'] = date('Y-m-d', strtotime($data['to_date']));
            $this->db->where('mi.date_time <', $data['to_date']);
        }
        if ($data['status'] != '')
        {
            if($data['status'] == '0')
            {
                $this->db->where('mi.id_main_invoice', $data['status']);
            }
            if($data['status'] == '1')
            {
                $this->db->where('mi.id_main_invoice !=', '0');
            }
        }
        $this->db->where("mi.status",'0');
        $this->db->where("mi.type ", "CORPORATE");
        $this->db->order_by("mi.id", "DESC");
        // $this->db->join('country as c', 'sp.id_country = c.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();      
         return $result;
    }

    function companyUserRoleListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('company_user_role');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function parentCompanyList($level)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('level', $level);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getCompanyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getPerformaMainInvoice($id)
    {
        $this->db->select('mi.*, cs.name as currency_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('performa_main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getPerformaMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('performa_main_invoice_details as mid');
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getPerformaMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('performa_main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getPerformaMainInvoiceHasStudentList($id)
    {
        $this->db->select('pmhs.*, s.full_name as student_name, s.nric');
        $this->db->from('performa_main_invoice_has_students as pmhs');   
        $this->db->join('student as s', 'pmhs.id_student = s.id');
        $this->db->where('pmhs.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function editPerformaMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('performa_main_invoice', $data);
        return TRUE;
        
        // return $this->db->affected_rows();
    }

    function movePerformaInvoiceToMainInvoice($id_performa_invoice)
    {
        $performa_main_invoice = $this->getPerformaMainInvoice($id_performa_invoice);

        if($performa_main_invoice)
        {
            unset($performa_main_invoice->id);
            unset($performa_main_invoice->id_main_invoice);
            unset($performa_main_invoice->currency_name);
            unset($performa_main_invoice->programme_name);
            unset($performa_main_invoice->programme_code);

            $performa_main_invoice->invoice_number = $this->generateMainInvoiceNumber();


            $id_main_invoice = $this->addNewMainInvoice($performa_main_invoice);

            // echo "<Pre>";print_r($id_main_invoice);exit;

            if($id_main_invoice)
            {
                $update_student_has_programme['id_invoice'] = $id_main_invoice;
                $updated_student_has_programme = $this->editStudentHasProgramme($update_student_has_programme,$id_performa_invoice);

                $performa_main_invoice_details = $this->getPerformaMainInvoiceDetails($id_performa_invoice);

                foreach ($performa_main_invoice_details as $detail)
                {
                    unset($detail->id);
                    unset($detail->fee_setup);
                    $detail->id_main_invoice = $id_main_invoice;

                   $id_main_invoice_details = $this->addNewMainInvoiceDetails($detail);
                }


                $performa_main_invoice_discount_details = $this->getPerformaMainInvoiceDiscountDetails($id_performa_invoice);

                foreach ($performa_main_invoice_discount_details as $discount_detail)
                {
                    unset($discount_detail->id);
                    unset($discount_detail->fee_setup);
                    $discount_detail->id_main_invoice = $id_main_invoice;
                    
                   $id_main_invoice_discount_details = $this->addNewMainInvoiceDiscountDetail($discount_detail);
                }


                $performa_main_invoice_student = $this->getPerformaMainInvoiceHasStudentList($id_performa_invoice);

                foreach ($performa_main_invoice_student as $student_detail)
                {
                    unset($student_detail->id);
                    unset($student_detail->student_name);
                    unset($student_detail->nric);
                    $student_detail->id_main_invoice = $id_main_invoice;
                    
                   $id_main_invoice_student_details = $this->addNewMainInvoiceHasStudents($student_detail);
                }

                $update_performa_data['id_main_invoice'] = $id_main_invoice;

                $updated_performa_invoice = $this->updatePerformaMainInvoice($update_performa_data, $id_performa_invoice);
            }

            return $id_main_invoice;
        }

        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }


    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceHasStudents($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_has_students', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

       return $insert_id;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updatePerformaMainInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('performa_main_invoice', $data);
        return $this->db->affected_rows();
    }

    function editStudentHasProgramme($data,$id_invoice)
    {
        $this->db->where('id_performa_invoice', $id_invoice);
        $this->db->update('student_has_programme', $data);
        return $this->db->affected_rows();
    }
}