<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamSet extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_set_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_set.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['examSetList'] = $this->exam_set_model->examSetListSearch($name);
            $this->global['pageTitle'] = 'Speed Management System : ExamSet List';
            $this->global['pageCode'] = 'exam_set.list';
            $this->loadViews("exam_set/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_set.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $instruction = $this->security->xss_clean($this->input->post('instruction'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'instruction' => $instruction,
                    'status' => $status
                );
               
                $result = $this->exam_set_model->addNewExamSet($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New ExamSet created successfully');
                } else {
                    $this->session->set_flashdata('error', 'ExamSet creation failed');
                }
                redirect('/exam/examSet/list');
            }
            $this->global['pageTitle'] = 'Speed Management System : Add ExamSet';
            $this->global['pageCode'] = 'exam_set.add';
            $this->loadViews("exam_set/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_set.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examSet/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $instruction = $this->security->xss_clean($this->input->post('instruction'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'instruction' => $instruction,
                    'status' => $status
                );

                $result = $this->exam_set_model->editExamSet($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Exam Set edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Set edit failed');
                }
                redirect('/exam/examSet/list');
            }
            $data['examSet'] = $this->exam_set_model->getExamSet($id);
            
            $this->global['pageCode'] = 'exam_set.edit';
            $this->global['pageTitle'] = 'Speed Management System : Edit Exam Set';
            $this->loadViews("exam_set/edit", $this->global, $data, NULL);
        }
    }
}
