<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Exam Event</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Event details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $getExamEvent->name ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" value="<?php echo date('d-m-Y',strtotime($getExamEvent->from_dt)) ?>">
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event Start Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="time" class="form-control" id="from_tm" name="from_tm" value="<?php echo 
                        date("H:i", strtotime($getExamEvent->from_tm))
                         // $getExamEvent->from_tm 
                         ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event End Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="time" class="form-control" id="to_tm" name="to_tm" value="<?php echo 
                        date("H:i", strtotime($getExamEvent->to_tm));
                         // $getExamEvent->to_tm 
                         ?>">
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Location <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_location" id="id_location" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($locationList))
                            {
                                foreach ($locationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamEvent->id_location)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Exam Center <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_exam_center" id="id_exam_center" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($examCenterList))
                            {
                                foreach ($examCenterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php 
                                if($record->id == $getExamEvent->id_exam_center)
                                    { echo "selected"; }
                                ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                  </div>

                  <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Quota No. <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="max_count" name="max_count" value="<?php echo $getExamEvent->max_count ?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Type <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="type" name="type" value="<?php echo $getExamEvent->type ?>" readonly>
                        </div>
                      </div>
                    </div>

                  </div>



                  <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Exam Set <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_exam_set" id="id_exam_set" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($examSetList))
                            {
                                foreach ($examSetList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php 
                                if($record->id == $getExamEvent->id_exam_set)
                                    { echo "selected"; }
                                ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                            <?php if($getExamEvent->status==1)
                            {
                                 echo "checked=checked";
                            };?>
                              >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                            <?php if($getExamEvent->status==0)
                            {
                                 echo "checked=checked";
                            };?>

                            >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 from_tm: {
                    required: true
                },
                 id_exam_center: {
                    required: true
                },
                 id_location: {
                    required: true
                },
                 max_count: {
                    required: true
                },
                 status: {
                    required: true
                },
                type : {
                    required: true
                },
                to_tm : {
                    required: true
                },
                id_exam_set : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Event Date</p>",
                },
                from_tm: {
                    required: "<p class='error-text'>Select Event Start Time</p>",
                },
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                },
                max_count: {
                    required: "<p class='error-text'>Quota No. Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Event Type</p>",
                },
                to_tm: {
                    required: "<p class='error-text'>Select Event End Time</p>",
                },
                id_exam_set: {
                    required: "<p class='error-text'>Select Exam Set</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
          yearRange: "1960:2001"
      });
    } );

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>