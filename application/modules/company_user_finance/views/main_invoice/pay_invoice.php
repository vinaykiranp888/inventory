<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">

    <div class="page-title clearfix">
      <h3>Invoice Payment</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Invoice</a> -->
    </div>

    <form id="form_main" action="" method="post"  enctype="multipart/form-data">
    

        <div class="custom-table">
          <table class="table">
            <thead>
              <tr>
              <th>Sl. No</th>
                <th>Invoice Number</th>
                <th>Type</th>
                <th>Remarks</th>
                <th>Currency</th>
                <th>Status</th>
                <th class="text-center">Invoice Total</th>
                <?php
                if($companyDetails->staff_credit == 1)
                {
                  ?>
                <th class="text-center">Staff Claimable Amount</th>

                  <?php
                }
                ?>
                <th class="text-center">Total Payable</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if (!empty($mainInvoiceList)) {
                $i=1;
                $total_payable_amount = 0;
                foreach ($mainInvoiceList as $record)
                {
                  $total_amount = number_format($record->total_amount, 2, '.', ',');
                  $balance_amount = number_format($record->balance_amount, 2, '.', ',');
                  $paid_amount = number_format($record->paid_amount, 2, '.', ',');

                $total_payable_amount = $total_payable_amount + $record->total_amount;

              ?>
                  <tr>
                    <td>
                      <input type='hidden' name='approval[]' value="<?php echo $record->id;?>" />
                      <?php echo $i ?></td>
                    <td><?php echo $record->invoice_number ?></td>
                    <td><?php echo $record->type ?></td>
                    <td><?php echo $record->remarks ?></td>
                    <td><?php 
                    if($record->currency_name == '')
                    {
                      echo $record->currency;
                    }else
                    {
                      echo $record->currency_name;
                    }
                     ?>   
                    </td>
                    <td><?php if( $record->status == '1')
                    {
                      echo "Approved";
                    }
                    else if( $record->status == '0')
                    {
                       echo "Pending";
                    }
                    else if( $record->status == '2')
                    {
                      echo "Cancelled";
                    } 
                    ?></td>
                    <td class="text-center"><?php echo $record->invoice_total ?></td>
                    <?php
                    $cols = 6;
                    if($companyDetails->staff_credit == 1)
                    {
                      $cols++;
                      ?>
                    <td class="text-center"><?php echo $record->invoice_total ?></td>

                      <?php
                    }
                    ?>
                    <td class="text-center"><?php echo $total_amount ?></td>
                    
                  </tr>
              <?php
              $i++;
                }
                  $total_payable_amount = number_format($total_payable_amount, 2, '.', ',');

                ?>
                  <tr>
                    <th colspan="<?php echo $cols; ?>" class="text-right"> Total : </th>
                    <th class="text-center"><?php echo $total_payable_amount ?></th>
                    <th class="text-center"><?php echo $total_payable_amount ?></th>
                  </tr>

                <?php
              }
              ?>
            </tbody>
          </table>

        </div>



        <div class="form-container">
                <h4 class="form-group-title">Payment Mode</h4>

            <div class="row">

                <input type="hidden" id="receipt_amount" name="receipt_amount" value="<?php echo $total_payable_amount; ?>">


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Payment Mode <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                            <input type="radio" name="payment_mode" id="payment_mode" value="IBG" checked="checked" onclick="showIGBField()"><span class="check-radio"></span> IBG
                            </label>
                            <?php
                            if($companyDetails->staff_credit == 1)
                            {
                              ?>

                            <label class="radio-inline">
                            <input type="radio" name="payment_mode" id="payment_mode" value="Staff Credit" onclick="hideIGBField()"><span class="check-radio"></span> Staff Credit
                            </label>

                            <?php
                            }
                            ?>

                        </div>                         
                </div>

            </div>


            <div class="row" id="igb_field">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Payment Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reference <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload File <span class='error-text'>*</span></label>
                        <input type="file" class="form-control" id="image" name="image">
                    </div>
                </div>
                
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" id="button" name="button" value="Payment" class="btn btn-primary btn-lg">Proceed Payment</button>
                <a href="/company_user_finance/mainInvoice/approvalList" class="btn btn-link">Back</a>
            </div>
        </div>

    </form>







  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>


  
    </div>
</div>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });


  $(document).ready(function()
  {
        $("#form_main").validate({
            rules: {
                date_time: {
                    required: true
                },
                reference_number: {
                    required: true
                },
                image: {
                    required: true
                }
            },
            messages: {
                date_time: {
                    required: "<p class='error-text'>Select payment Date</p>",
                },
                reference_number: {
                    required: "<p class='error-text'>Reference Number Required</p>",
                },
                image: {
                    required: "<p class='error-text'>Select Image</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  

  function showIGBField()
  {
    $("#igb_field").show();
  }

  function hideIGBField()
  {
    $("#igb_field").hide();
  }

  function clearSearchForm()
  {
    window.location.reload();
  }

</script>