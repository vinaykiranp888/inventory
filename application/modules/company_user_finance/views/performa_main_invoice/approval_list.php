<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Proforma Invoice Approval</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Invoice</a> -->
    </div>
    
  <form action="" method="post" id="searchForm">

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>


          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

              



              <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">From Date</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="from_date" id="from_date" value="<?php echo $searchParam['from_date']; ?>" autocomplete="off">
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">To Date</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="to_date" id="to_date" value="<?php echo $searchParam['to_date']; ?>" autocomplete="off">
                      </div>
                    </div>
                  </div>

              </div>


              <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Proforma Invoice Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="invoice_number" value="<?php echo $searchParam['invoice_number']; ?>">
                    </div>
                  </div>
                </div>


                <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Status </label>
                    <div class="col-sm-8">
                      <select name="status" id="status" class="form-control">
                        <option value="">Select</option>
                        <option value="0" <?php if($searchParam['status']=='0'){ echo "selected"; } ?>>Pending</option>
                        <option value="1" <?php if($searchParam['status']=='1'){ echo "selected"; } ?>>Approved</option>
                      </select>
                    </div>
                  </div>
                </div> -->               

              </div>
              

              </div>

              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="approvalList" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <th>Sl. No</th>
            <th style="text-align: center;"><input type="checkbox" id="checkAll" name="checkAll" onclick="checkAllCheckBoxes()"> Check All</th>
            <th class="text-center">Proforma Invoice Number</th>
            <th class="text-center">Type</th>
            <!-- <th class="text-center">Invoice For</th> -->
            <!-- <th class="text-center">Intake</th> -->
            <th class="text-center">Programme</th>
            <th class="text-center">Invoice Total</th>
            <th class="text-center">Total Discount</th>
            <th class="text-center">Total Payable</th>
            <th class="text-center">Remarks</th>
            <th class="text-center">Invoice Date</th>
            <th class="text-center">Currency</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($performaInvoiceList)) {
            $i=1;
            foreach ($performaInvoiceList as $record)
            {
              $total_amount = number_format($record->total_amount, 2, '.', ',');
              $balance_amount = number_format($record->balance_amount, 2, '.', ',');
              $paid_amount = number_format($record->paid_amount, 2, '.', ',');
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td>
                  <input type='checkbox' name='approval[]' id="approval[]" class="check" value="<?php echo $record->id;?>">
                </td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo $record->type ?></td>
                <!-- <td><?php echo $record->registration_number . " - " . $record->company_name ?></td> -->
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php echo $record->invoice_total ?></td>
                <td><?php echo $record->total_discount ?></td>
                <td><?php echo $total_amount ?></td>
                <td><?php echo $record->remarks ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->date_time)) ?></td>
                <td><?php 
                if($record->currency_name == '')
                {
                  echo $record->currency;
                }else
                {
                  echo $record->currency_name;
                }
                 ?></td>
                <td><?php 
                if( $record->status == '2')
                {
                  echo "Rejected";
                }
                elseif( $record->id_main_invoice > '0')
                {
                  echo "Approved";
                }
                else if( $record->id_main_invoice == '0')
                {
                   echo "Pending";
                }
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'view/' . $record->id; ?>" title="View">View</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>

          <tr>
            <td><button type="submit" name="button" id="button" class="btn btn-primary" value="Approve">Approve</button></td>
            <td><button type="submit" name="button" id="button" class="btn btn-primary" value="Reject">Reject</button></td>
          </tr>


        </tbody>
      </table>
    </div>

  </form>

    
  </div>


  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function checkAllCheckBoxes()
  {
    var statuscheck = $("#checkAll").is(':checked');
    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }

</script>