<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Exam Center Room Capacity</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <!-- <form id="form_main" action="" method="post"> -->

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Center details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $getExamCenterList->name ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Address <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo $getExamCenterList->address ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Person <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_person" name="contact_person" placeholder="Contact Person" value="<?php echo $getExamCenterList->contact_person ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Contact Number <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Contact Person" value="<?php echo $getExamCenterList->contact_number ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country</label>
                          <div class="col-sm-8">
                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamCenterList->id_country)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Country</label>
                          <div class="col-sm-8">
                            <select name="id_state" id="id_state" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>" <?php 
                                if($record->id == $getExamCenterList->id_state)
                                    { echo "selected"; }
                                ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6" style="display: none;">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">State</label>
                          <div class="col-sm-8">
                            <span id='view_state'></span>
                          </div>
                        </div>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">City <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" value="<?php echo $getExamCenterList->city ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Zipcode <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="zipcode" name="zipcode" placeholder="Zipcode" value="<?php echo $getExamCenterList->zipcode ?>" readonly>
                        </div>
                      </div>
                    </div>

                  </div>


                  <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Location</label>
                          <div class="col-sm-8">
                            <select name="id_location" id="id_location" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($locationList))
                            {
                                foreach ($locationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamCenterList->id_location)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Exam Type</label>
                          <div class="col-sm-8">
                          <select name="exam_type" id="exam_type" class="form-control" disabled>
                            <option value="">Select</option>
                            <option value="Face to Face"
                            <?php
                            if($getExamCenterList->exam_type == 'Face To Face')
                            {
                              echo 'selected';
                            }
                             ?> >Face to Face</option>
                            <option value="Online"
                            <?php
                            if($getExamCenterList->exam_type == 'Online')
                            {
                              echo 'selected';
                            }
                             ?> >Online</option>
                            <option value="Both"
                            <?php
                            if($getExamCenterList->exam_type == 'Both')
                            {
                              echo 'selected';
                            }
                             ?> >Both</option>
                          </select>
                          </div>
                        </div>
                    </div>


                </div>




                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $getExamCenterList->email ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Username <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Username" value="<?php echo $getExamCenterList->user_name ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $getExamCenterList->password ?>" readonly>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Rooms <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="rooms" name="rooms" placeholder="Total No Of Rooms" value="<?php echo $getExamCenterList->rooms ?>" readonly>
                        </div>
                      </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">TOS</label>
                          <div class="col-sm-8">
                            <select name="id_tos" id="id_tos" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($tosList))
                            {
                                foreach ($tosList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                               <?php 
                                if($record->id == $getExamCenterList->id_tos)
                                    { echo "selected"; }
                                ?>
                              >
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                            <?php if($getExamCenterList->status==1)
                            {
                                 echo "checked=checked";
                            };?>
                              >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                            <?php if($getExamCenterList->status==0)
                            {
                                 echo "checked=checked";
                            };?>

                            >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>   


            <div class="form-container">
            <h4 class="form-group-title"> Room Capacity Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Options</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_comitee" action="" method="post" enctype="multipart/form-data">


                                <br>

                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                          <label class="col-sm-4 col-form-label">Room</label>
                                          <div class="col-sm-8">
                                            <select name="room" id="room" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($getExamCenterList->rooms))
                                            {
                                              for ($i=1; $i <= $getExamCenterList->rooms; $i++)
                                                {
                                                  ?>
                                             <option value="<?php echo 'Room ' . $i;  ?>">
                                                <?php echo 'Room ' . $i;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                          </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Capacity <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="number" class="form-control" id="capacity" name="capacity" placeholder="Capacity">
                                        </div>
                                      </div>
                                    </div>


                                    
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-lg form-row-btn">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($centerHasRoomCapacity))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Option Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Room</th>
                                                 <th>Capacity</th>
                                                 <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($centerHasRoomCapacity);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $centerHasRoomCapacity[$i]->room;?></td>
                                                <td><?php echo $centerHasRoomCapacity[$i]->capacity;?></td>
                                                <td class="text-center">
                                                <a onclick="deleteRoomCapacity(<?php echo $centerHasRoomCapacity[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>




                </div>

            </div>
        </div> 


        </div>

</main>

<script>

     function deleteRoomCapacity(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/exam/examCenter/deleteRoomCapacity/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        $("#form_comitee").validate(
        {
            rules:
            {
                room:
                {
                    required: true
                },
                capacity:
                {
                    required: true
                }
            },
            messages:
            {
                room:
                {
                    required: "<p class='error-text'>Select Room</p>",
                },
                capacity:
                {
                    required: "<p class='error-text'>Capacity Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();


</script>
<script type="text/javascript">

    function reloadPage()
    {
      window.location.reload();
    }
</script>