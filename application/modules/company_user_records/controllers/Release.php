<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Release extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('release_model');
        $this->load->model('setup/semester_model');
        $this->load->model('barring_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('release.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->release_model->studentList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_barring_type'] = $this->security->xss_clean($this->input->post('id_barring_type'));
            $formData['id_student'] = $this->security->xss_clean($this->input->post('id_student'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            
            $data['searchParameters'] = $formData;
            $data['releaseList'] = $this->release_model->releaseListSearch($formData);
            // echo "<Pre>"; print_r($data['barringList']);exit;

            $this->global['pageTitle'] = 'Corporate User Portal : Release List';
            $this->loadViews("release/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('release.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_barring_type = $this->security->xss_clean($this->input->post('id_barring_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $release_date = $this->security->xss_clean($this->input->post('release_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_barring_type' => $id_barring_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'release_date' => date('Y-m-d',strtotime($release_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );
                $result = $this->release_model->addNewRelease($data);
                redirect('/records/release/list');
            }
            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->release_model->studentList();

            $this->global['pageTitle'] = 'Corporate User Portal : Add New Release';
            $this->loadViews("release/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('release.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        } else {

            if ($id == null)
            {
                redirect('/records/release/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_barring_type = $this->security->xss_clean($this->input->post('id_barring_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $release_date = $this->security->xss_clean($this->input->post('release_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_barring_type' => $id_barring_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'release_date' => date('Y-m-d',strtotime($release_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );
                $result = $this->release_model->editRelease($data, $id);
                redirect('/records/release/list');
            }

            $data['barringTypeList'] = $this->barring_type_model->barringTypeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['studentList'] = $this->release_model->studentList();
            $data['release'] = $this->release_model->getRelease($id);
            // echo "<Pre>";print_r($data['release']);exit;
            $this->global['pageTitle'] = 'Corporate User Portal : Edit Release';
            $this->loadViews("release/edit", $this->global, $data, NULL);
        }
    }
}
