<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BarringType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('barring_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('barring_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['barringTypeList'] = $this->barring_type_model->barringTypeListSearch($name);
            $this->global['pageTitle'] = 'Corporate User Portal : Barring Type List';
            $this->loadViews("barring_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('barring_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->barring_type_model->addNewBarringType($data);
                redirect('/records/barringType/list');
            }
            $this->global['pageTitle'] = 'Corporate User Portal : Add Barring Type';
            $this->loadViews("barring_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('barring_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/records/barringType/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->barring_type_model->editBarringType($data,$id);
                redirect('/records/barringType/list');
            }
            $data['barringType'] = $this->barring_type_model->getBarringType($id);
            $this->global['pageTitle'] = 'Corporate User Portal : Edit Barring Type';
            $this->loadViews("barring_type/edit", $this->global, $data, NULL);
        }
    }
}
