<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Barring_model extends CI_Model
{
    
    function barringList()
    {
        $this->db->select('b.*, stu.full_name as student, sem.code as program_code, sem.name as program_name, bt.name as barring_type');
        $this->db->from('barring as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('programme as sem', 'b.id_program = sem.id');
        $this->db->join('barring_type as bt', 'b.id_barring_type = bt.id');
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function barringListSearch($formData)
    {
        $this->db->select('b.*, stu.full_name as student, sem.code as program_code, sem.name as program_name, bt.name as barring_type');
        $this->db->from('barring as b');
        $this->db->join('student as stu', 'b.id_student = stu.id');
        $this->db->join('programme as sem', 'b.id_program = sem.id');
        $this->db->join('barring_type as bt', 'b.id_barring_type = bt.id');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(b.reason  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_program']))
        {
            $this->db->where('b.id_program', $formData['id_program']);
        }
        if (!empty($formData['id_student']))
        {
            $this->db->where('b.id_student', $formData['id_student']);
        }
        if (!empty($formData['id_barring_type']))
        {
            $this->db->where('b.id_barring_type', $formData['id_barring_type']);
        }
        $this->db->order_by("b.id", "DESC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getBarring($id)
    {
        $this->db->select('*');
        $this->db->from('barring');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    

    function addNewBarring($data)
    {
        $this->db->trans_start();
        $this->db->insert('barring', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editBarring($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('barring', $data);

        return TRUE;
    }

    function deleteBarring($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function studentList()
    {
        $this->db->select('*, first_name as name');
        // $this->db->from('student');
        $this->db->from('student');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }
    

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }


    function getStudentByProgNIntake($data)
    {
        $this->db->select('DISTINCT(er.id) as id, er.*');
        $this->db->from('student as er');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_program', $data['id_programme']);
        }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('er.id_intake', $data['id_intake']);
        // }
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.type as advisor_type, st.full_name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name, q.name as qualification_name, q.short_name as qualification_code, ress.type as supervisor_type, ress.full_name as supervisor_name, pt.code as program_structure_code, pt.name as program_structure_name, n.name as nationality');
        $this->db->from('student as s');
        $this->db->join('program_type as pt', 's.id_program_structure_type = pt.id','left');
        $this->db->join('programme as p', 's.id_program = p.id','left'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id','left'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id','left'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id','left');
        $this->db->join('state as ps', 's.permanent_state = ps.id','left'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id','left'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id','left'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('research_advisor as st', 's.id_advisor = st.id','left'); 
        $this->db->join('research_supervisor as ress', 's.id_supervisor = ress.id','left'); 
        $this->db->join('education_level as q', 's.id_degree_type = q.id','left');
        $this->db->join('nationality as n', 's.nationality = n.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }


}
