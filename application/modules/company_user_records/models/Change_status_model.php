<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Change_status_model extends CI_Model
{
    function changeStatusList()
    {
        $this->db->select('*');
        $this->db->from('change_status');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function changeStatusListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('change_status');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getChangeStatus($id)
    {
        $this->db->select('*');
        $this->db->from('change_status');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewChangeStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('change_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editChangeStatus($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('change_status', $data);
        return TRUE;
    }
}

