<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Apply Change Status</h3>
      <a href="add" class="btn btn-primary">+ Add Apply Change Status</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Reason</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $searchParameters['name']; ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student</label>
                      <div class="col-sm-8">
                        <select name="id_student" id="id_student" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($studentList)) {
                            foreach ($studentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParameters['id_student'])
                              {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo $record->name;  ?>
                                  
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>


                  </div>
                  <div class="col-sm-6">

                    <!-- <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($semesterList)) {
                            foreach ($semesterList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_semester']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>><?php echo $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div> -->

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Change Status</label>
                      <div class="col-sm-8">
                        <select name="id_change_status" id="id_change_status" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($changeStatusList)) {
                            foreach ($changeStatusList as $record) {
                              $selected = '';
                              if ($record->id == $searchParameters['id_change_status']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>><?php echo $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No.</th>
            <th>Student Name</th>
            <th>Student NRIC</th>
            <th>Programme</th>
            <th>Change Status</th>
            <th>Reason</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($applyChangeStatusList)) {
            $i=1;
            foreach ($applyChangeStatusList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->student_name ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php echo $record->change_status ?></td>
                <td><?php echo $record->reason ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                  echo "Pending";
                } 
                ?></td>
                <td class="text-center">

                  <?php 

                  if ($record->status == '1')
                  {
                    ?>
                    <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a>
                    <?php
                  }
                  else if($record->status == '0')
                  {
                    ?>
                    <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">View</a>

                    <?php
                  }
                  ?>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
    $('select').select2();
    
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>