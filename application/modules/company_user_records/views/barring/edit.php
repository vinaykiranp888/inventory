<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Barring</h3>
        </div>
        <form id="form_barring" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Barring Details</h4>        
                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_semester" id="id_semester" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $barring->id_program)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
        

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Barring Type <span class='error-text'>*</span></label>
                            <select name="id_barring_type" id="id_barring_type" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($barringTypeList))
                                {
                                    foreach ($barringTypeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $barring->id_barring_type)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>   

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Student <span class='error-text'>*</span></label>
                            <select name="id_student" id="id_student" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($studentList))
                                {
                                    foreach ($studentList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $barring->id_student)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->nric . " - " . $record->full_name;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 


                </div>


                <div class="row">

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $barring->reason; ?>" disabled>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Barring From Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="barring_date" name="barring_date" value="<?php echo date('d-m-Y',strtotime($barring->barring_date)); ?>" autocomplete="off" disabled>
                        </div>
                    </div> 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Barring To Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="barring_to_date" name="barring_to_date" value="<?php echo date('d-m-Y',strtotime($barring->barring_to_date)); ?>" autocomplete="off" disabled>
                        </div>
                    </div> 


                </div>


                <div class="row">


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($barring->status=='1') {
                                    echo "checked=checked";
                                };?> disabled><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($barring->status=='0') {
                                    echo "checked=checked";
                                };?> disabled>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#form_barring").validate(
        {
            rules:
            {
                id_student:
                {
                    required: true
                },
                id_barring_type:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                barring_date:
                {
                    required: true
                }
            },
            messages:
            {
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                id_barring_type:
                {
                    required: "<p class='error-text'>Select Barring Type</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                barring_date:
                {
                    required: "<p class='error-text'>Select Barring Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
  </script>
  <script type="text/javascript">
    $('select').select2();
</script>