<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Apply Change Program</h3>
        </div>


             <h4 class='sub-title'>Student Profile</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $studentDetails->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $studentDetails->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $studentDetails->programme_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $studentDetails->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $studentDetails->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $studentDetails->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>






            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Changing Program *</label>
                        <select name="id_new_programme" disabled="disabled" id="id_new_programme" class="form-control"> <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $applyChangeProgramme->id_new_programme)
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;  ?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Changing Intake *</label>
                        <select name="id_new_intake" disabled="disabled" id="id_new_intake" class="form-control" onchange="getFeeByProgrammeNIntake()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $applyChangeProgramme->id_new_intake)
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->name;  ?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Changing Semester *</label>
                        <select name="id_new_semester" disabled="disabled" id="id_new_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $applyChangeProgramme->id_new_semester)
                                {
                                    echo "selected=selected";
                                } ?>>
                                <?php echo $record->code . " - " . $record->name;  ?>
                                </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

            </div>

                <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Learning Mode <span class="error-text">*</span></label>
                            <select name="id_new_program_scheme" disabled id="id_new_program_scheme" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programScheme))
                                {
                                    foreach ($programScheme as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $applyChangeProgramme->id_new_program_scheme)
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo $record->mode_of_program . " - " . $record->mode_of_study;  ?>
                                    </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 



                    <?php

                    if($applyChangeProgramme->by_student > 0)
                    {
                        ?>


                        <div class="col-sm-4" id="div_fee">
                            <div class="form-group">
                                <label>Applied By <span class="error-text">*</span></label>
                                <span id='fee_v'>
                                    <input type="text" class="form-control" id="fee" name="fee" value="<?php echo $studentDetails->full_name; ?>" readonly="readonly">
                                </span>
                            </div>
                        </div> 



                        <?php
                    }

                    ?>




                <div class="col-sm-4" id="div_fee">
                    <div class="form-group">
                        <label>Fee *</label>
                        <span id='fee_v'>
                            <input type="text" class="form-control" id="fee" name="fee" value="<?php echo $applyChangeProgramme->fee; ?>" readonly="readonly">
                        </span>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reason *</label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $applyChangeProgramme->reason; ?>" readonly>
                    </div>
                </div>      

            </div>

            <div class="row">

            <?php
            if($applyChangeProgramme->status == '2')
            {
             ?>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reject Reason *</label>
                        <input type="text" class="form-control" id="reject_reason" name="reject_reason" value="<?php echo $applyChangeProgramme->reject_reason; ?>" readonly>
                    </div>
                </div>   


             <?php
            }
             ?>
            </div>




        <form id="form_main" action="" method="post">

             <?php
            if($applyChangeProgramme->status == '0')
            {
             ?>


        


             <div class="form-container">
                <h4 class="form-group-title">Approval Details For Programme Change</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p> Approval <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                                <input type="radio" id="ed1" name="status" value="1" onclick="hideRejectField()"><span class="check-radio"></span> Approve
                            </label>
                            <label class="radio-inline">
                                <input type="radio" id="ed2" name="status" value="2" onclick="showRejectField()"><span class="check-radio"></span> Reject
                            </label>
                        </div>
                    </div>


                <input type="hidden" id="id_student" name="id_student" value="<?php echo $applyChangeProgramme->id_student; ?>">


                     <div class="col-sm-4" id="view_reject" style="display: none">
                        <div class="form-group">
                            <label>Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control">
                        </div>
                    </div>

                </div>


            </div>



          <?php
            }
            ?>

        


        <div class="button-block clearfix">
                <div class="bttn-group">

             <?php
            if($applyChangeProgramme->status == '0')
            {
             ?>
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>

             <?php
            }
             ?>
                    <a href="../approval_list" class="btn btn-link">Back</a>
                

                </div>

        </div>



        </form>



            <h3>Fee Structure Details</h3>


            <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>SL. No</th>
                                <th>Fee Item</th>
                                <th>Currency</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                             $total = 0;
                              for($i=0;$i<count($feeStructure);$i++)
                             { ?>
                                <tr>
                                <td><?php echo $i+1;?></td>
                                <td><?php echo $feeStructure[$i]->fee_code . " - " . $feeStructure[$i]->fee_setup;?></td>
                                <td><?php echo $feeStructure[$i]->currency;?></td>
                                <td><?php echo $feeStructure[$i]->amount;?></td>
                                </tr>
                              <?php 
                              $total = $total + $feeStructure[$i]->amount;
                          } 
                          $total = number_format($total, 2, '.', ',');
                          ?>
                            <tr>
                                <td bgcolor=""></td>
                                <td bgcolor=""></td>
                                <td bgcolor=""><b> Total : </b></td>
                                <td bgcolor=""><b><?php echo $total; ?></b></td>
                            </tr>

                        </tbody>
                    </table>
                </div>







            <!-- <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../approval_list" class="btn btn-link">Back</a>
                </div>
            </div> -->
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    $('select').select2();

    function getFeeByProgrammeNIntake()
    {
        var tempPR = {};
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
            $.ajax(
            {
               url: '/records/applyChangeProgramme/getFeeByProgrammeNIntake',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $('#div_fee').modal('hide');
                $('#div_fee_v').modal('show');
                $("#view_fee").html(result);
               }
            });
        
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }
</script>