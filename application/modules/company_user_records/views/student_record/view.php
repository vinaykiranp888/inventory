<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Employee Record</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>
      <form id="form_main_invoice" action="" method="post">



         <div class="form-container">
            <h4 class="form-group-title">Employee Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Employee Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Employee NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>                          
                            <dl>
                                <dt>Company :</dt>
                                <dd><?php echo ucwords($getStudentData->company_name); ?></dd>
                            </dl>                                                      
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Employee Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>  
                            <dl>
                              <dt>Academic Advisor :</dt>
                              <dd><?php echo $getStudentData->advisor_icno . " - " . $getStudentData->advisor_name ; ?></dd>
                          </dl>              
                        </div>
    
                    </div>
                </div>


         </div>


         



         <div class="form-container">
            <h4 class="form-group-title">Employee Records Details</h4>
            <div class="m-auto text-center">
               <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
               <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                  <li role="presentation" class="active" ><a href="#tab_4" class="nav-link border rounded text-center"
                     aria-controls="tab_4" aria-selected="true"
                     role="tab" data-toggle="tab">Employee Status</a>
                  </li>
                  <!-- <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center"
                     aria-controls="tab_2" role="tab" data-toggle="tab">Barring / Release</a>
                  </li> -->
                  <li role="presentation"><a href="#tab_3" class="nav-link border rounded text-center"
                     aria-controls="tab_3" role="tab" data-toggle="tab">Program Registration</a>
                  </li>
               </ul>


               <div class="tab-content offers-tab-content">


                  <div role="tabpanel" class="tab-pane" id="tab_2">
                     <div class="mt-4">

                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Type</th>
                                    <th>ID Type</th>
                                    <th>Reason</th>
                                    <th>Date</th>
                                    <th>User</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($barrReleaseByStudentId)) {
                                        $i=1;
                                        foreach ($barrReleaseByStudentId as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->type ?></td>
                                    <td><?php echo $record->barring_code . " - " . $record->barring_name ?></td>
                                    <td><?php echo $record->reason ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->created_by ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                     </div>
                  
                  </div>




                  <div role="tabpanel" class="tab-pane" id="tab_3">
                     <div class="mt-4">



                      <div class="form-container">
                        <h4 class="form-group-title">Program Registration Details</h4>



                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th>Sl. No</th>
                                    <th>Programme Code</th>
                                    <th>Programme Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Registered On</th>
                                    <th>Invoice</th>
                                    <th>Payment Status</th>
                                    <th>Marks Obtained</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($studentHasProgramme)) {
                                        $i=1;
                                        foreach ($studentHasProgramme as $record) {
                                    ?>
                                 <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->programme_code; ?></td>
                                    <td><?php echo $record->programme_name; ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->start_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->end_date)); ?></td>
                                    <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                    <td><?php echo $record->invoice_number ?></td>
                                    <td><?php
                                    if($record->status == '0')
                                    {
                                      echo 'Not Paid';
                                    }
                                    else
                                    {
                                      echo 'Paid';
                                    }
                                    ?></td>
                                    <td><?php if($record->id_marks_entry == 0 && $record->id_marks_adjustment == 0)
                                    {
                                      echo ' - ';
                                    }else
                                    {
                                      echo $record->marks;
                                    } 
                                       ?></td>
                                 </tr>
                                 <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>


                          

                      </div>





                        
                        <!-- <?php
                           if(!empty($courseRegisteredLandscapeFBySemester))
                           {
                               ?> <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                 <?php
                                    if (!empty($courseRegisteredLandscapeFBySemester))
                                    {
                                        foreach ($courseRegisteredLandscapeFBySemester as $record)
                                        {
                                    ?>
                                 <td>
                                    <h3><?php echo $record->code . " - " . $record->name; ?></h3>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                
                                 <div class="custom-table" id="printReceipt">
                                    <table class="table" id="list-table">
                                       <thead>
                                          <tr>
                                             <th>Sl. No</th>
                                             <th>Course Code</th>
                                             <th>Course name</th>
                                             <th>Credit Hours</th>
                                             <th>Status</th>                                        
                                             <th>Withdraw</th>                                        
                                             <th>Grade</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             if (!empty($record->course)) {
                                                 $i=1;
                                                 foreach ($record->course as $record_course) {
                                             ?>
                                          <tr>
                                             <td><?php echo $i ?></td>
                                             <td><?php echo $record_course->course_code ?></td>
                                             <td><?php echo $record_course->course_name ?></td>
                                             <td><?php echo $record_course->credit_hours ?></td>

                                             <td>
                                                <?php if($record_course->is_result_announced > 0)
                                                   {
                                                       echo $record_course->total_result;
                                                   }
                                                   elseif($record_course->is_exam_registered > 0)
                                                   {
                                                       echo 'Exam Registered';
                                                   }
                                                   else{
                                                       echo "Exam Not Registered";
                                                   } ?>
                                             </td>
                                             <td><?php
                                             if($record_course->is_bulk_withdraw > 0)
                                                   {
                                                       echo 'Withdraw';
                                                   }
                                              ?></td>
                                             <td><?php echo $record_course->grade ?></td>

                                          </tr>
                                          <?php
                                             $i++;
                                                 }
                                             }
                                             ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <?php
                                    }
                                    }
                                    ?>
                                 </td>
                              </tr>
                              </tbody>
                           </table>
                        </div><?php
                         }
                       ?> -->


                     </div>

                  </div>



                  <div role="tabpanel" class="tab-pane active" id="tab_4">
                     <div class="mt-4">
                        <div class="form-container">
                           <h4 class="form-group-title">Student Status</h4>
                           <div class="custom-table">
                              <table class="table" id="list-table">
                                 <thead>
                                    <tr>
                                       <th>Sl. No</th>
                                       <th>Date</th>
                                       <th>Reason</th>
                                       <th>By</th>
                                       <th>Status</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                    <tr>
                                       <td><?php echo '1'; ?></td>
                                       <td><?php 
                                       if($studentStatus->id_applicant > 0)
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                       else
                                       {
                                          echo date('d-m-Y H:i:s',strtotime($studentStatus->created_dt_tm));
                                       }
                                         ?>     
                                       </td>
                                       <td> </td>
                                       <td><?php
                                       if($studentStatus->added_by_partner_university > 0)
                                       {
                                        echo "Partner University";
                                       }
                                       else
                                       {
                                          echo 'Administrator';
                                       } ?></td>
                                       <td>Active</td>
                                    </tr>
                                    <?php
                                    if(!empty($applyChangeStatusListByStudentId))
                                    {
                                      $j=1;
                                        foreach ($applyChangeStatusListByStudentId as $record)
                                        {
                                    ?>
                                   <tr>
                                      <td><?php echo $j+1 ?></td>
                                      <td><?php echo date('d-m-Y H:i:s',strtotime($record->created_dt_tm)); ?></td>
                                      <td><?php echo $record->change_status ?></td>
                                      <td><?php echo $record->created_by ?></td>
                                      <td>
                                      <?php 
                                         if($record->status == 0)
                                         {
                                             echo "Pending"; 
                                         }if($record->status == 1)
                                         {
                                             echo "Approved"; 
                                         }if($record->status == 2)
                                         {
                                             echo "Rejected"; 
                                         }?>
                                         
                                      </td>
                                   </tr>
                                   <?php
                                      $j++;
                                      }
                                    }
                                    ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  
                  </div>
        

           </div>
        </div>
     </div>
  </form>
</div>
   
</div>

   <footer class="footer-wrapper">
      <p>&copy; 2019 All rights, reserved</p>
   </footer>
<script type="text/javascript">
   function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
   }
</script>
<script>
   saveData();
   
   function saveData()
   {
       // if($("#note").val() != '')
       // {
   
       var tempPR = {};
       tempPR['note'] = $("#note").val();
       tempPR['id_student'] = <?php echo $studentDetails->id ?>;
           $.ajax(
           {
              url: '/records/studentRecord/addStudentNote',
               type: 'POST',
              data:
              {
               tempData: tempPR
              },
              error: function()
              {
               alert('Something is wrong');
              },
              success: function(result)
              {
               // alert(result);
               // window.location.reload();
               $("#view_student_note").html(result);
               // $('#myModal').modal('hide');
              }
           });
       // }
   }
   
   
   function deleteStudentNote(id)
   {
   
       var tempPR = {};
       tempPR['id'] = id;
       tempPR['id_student'] = <?php echo $studentDetails->id ?>;
           $.ajax(
           {
              url: '/records/studentRecord/deleteStudentNote',
               type: 'POST',
              data:
              {
               tempData: tempPR
              },
              error: function()
              {
               alert('Something is wrong');
              },
              success: function(result)
              {
               // alert(result);
               // window.location.reload();
               $("#view_student_note").html(result);
               // $('#myModal').modal('hide');
              }
           });
   }
   
   
   
    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               note:
               {
                   required: true
               }         
           },
           messages:
           {
               note:
               {
                   required: "<p class='error-text'>Note Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
   
   
</script>