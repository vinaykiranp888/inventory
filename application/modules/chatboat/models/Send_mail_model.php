<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Send_mail_model extends CI_Model
{    
    function templateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('communication_template');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getApplicantListByData($data)
    {
        $id_programme = $data['id_programme'];
        $id_intake = $data['id_intake'];
        $student = $data['student'];

        $this->db->select('s.*, p.name as program_name, p.code as program_code, i.year as intake_year, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('applicant as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        if ($id_programme != '')
        {
            $this->db->where('s.id_program', $id_programme);
            
        }
        if ($id_intake != '')
        {
            $this->db->where('s.id_intake', $id_intake);
            
        }
        if ($student != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $student . "%' or s.nric  LIKE '%" . $student . "%')";
            $this->db->where($likeCriteria);
            
        }
        $this->db->where('s.applicant_status', 'Approved');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;

    }


    function getStudentListByData($data)
    {
        $id_programme = $data['id_programme'];
        $id_intake = $data['id_intake'];
        $student = $data['student'];

        $this->db->select('s.*, p.name as program_name, p.code as program_code, i.year as intake_year, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        if ($id_programme != '')
        {
            $this->db->where('s.id_program', $id_programme);
            
        }
        if ($id_intake != '')
        {
            $this->db->where('s.id_intake', $id_intake);
            
        }
        if ($student != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $student . "%' or s.nric  LIKE '%" . $student . "%')";
            $this->db->where($likeCriteria);
            
        }
        $this->db->where('s.applicant_status', 'Approved');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStaffListByData($data)
    {
        $staff_name = $data['staff_name'];
        $staff_ic_no = $data['staff_ic_no'];

        $this->db->select('s.*');
        $this->db->from('staff as s');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        // if ($id_programme != '')
        // {
        //     $this->db->where('s.id_program', $id_programme);
            
        // }
        // if ($id_intake != '')
        // {
        //     $this->db->where('s.id_intake', $id_intake);
            
        // }
        if ($staff_name != '')
        {
            $likeCriteria = "(s.name  LIKE '%" . $staff_name . "%')";
            $this->db->where($likeCriteria);
            
        }
        if ($staff_ic_no != '')
        {
            $likeCriteria = "(s.ic_no  LIKE '%" . $staff_ic_no . "%')";
            $this->db->where($likeCriteria);
            
        }
        $this->db->where('s.status', '1');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;

    }


    













    

    function getStudentByProgrammeId($id_programme)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id_program', $id_programme);
        $this->db->order_by("full_name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getProgrammeById($id_programme)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id_programme);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    

    function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('sponser_has_students');
    }
}

