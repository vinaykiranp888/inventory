<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class TemplateMessage extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('template_message_model');
        $this->isLoggedIn();
    }


    function list()
    {
        if ($this->checkAccess('template_message.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;

            $data['templateList'] = $this->template_message_model->templateListSearch($formData);
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : Communication Message Template List';
            $this->global['pageCode'] = 'template_message.list';

            $this->loadViews("template_message/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('template_message.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $subject = $this->security->xss_clean($this->input->post('subject'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'subject' => $subject,
                    'message' => $message,
                    'id_university' => $id_university,
                    'id_education_level' => $id_education_level,
                    'status' => $status,
                    'created_by' => $user_id
                );

                
                $result = $this->template_message_model->addNewTemplate($data);
                redirect('/communication/templateMessage/list');
            }


            $data['universityList'] = $this->template_message_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->template_message_model->educationLevelListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Communication Message Template';
            $this->global['pageCode'] = 'template_message.add';

            $this->loadViews("template_message/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('template_message.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/templateMessage/list');
            }
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $subject = $this->security->xss_clean($this->input->post('subject'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'subject' => $subject,
                    'message' => $message,
                    'id_university' => $id_university,
                    'id_education_level' => $id_education_level,
                    'status' => $status,
                    'updated_by' => $user_id
                );


                $result = $this->template_message_model->editTemplate($data,$id);
                redirect('/communication/templateMessage/list');
            }

            $data['universityList'] = $this->template_message_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->template_message_model->educationLevelListByStatus('1');
            
            $data['template'] = $this->template_message_model->getTemplate($id);
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Communication Message Template';
            $this->global['pageCode'] = 'template_message.edit';

            $this->loadViews("template_message/edit", $this->global, $data, NULL);
        }
    }
}
