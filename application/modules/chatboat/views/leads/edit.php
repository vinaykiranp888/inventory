<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Lead Messages</h3>
        </div>




        <div class="form-container">
          <h4 class="form-group-title">Lead Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Lead Name :</dt>
                              <dd><?php echo ucwords($lead->Name); ?></dd>
                          </dl>
                          <dl>
                              <dt>IP :</dt>
                              <dd><?php echo $lead->IP ?></dd>
                          </dl>                          
                          <dl>
                              <dt>Start Date Time</dt>
                              <dd><?php echo date('d-m-Y H:i:s', strtotime($lead->StartTime)); ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Lead Email :</dt>
                              <dd><?php echo $lead->Email ?></dd>
                          </dl>         
                          <dl>
                              <dt>Phone :</dt>
                              <dd><?php echo $lead->Phone; ?></dd>
                          </dl>
                          <dl>
                              <dt>Status </dt>
                              <dd><?php
                              if($lead->lead_status != NULL)
                              {
                               echo $lead->status_name;
                              }
                              else
                              {
                               echo 'In-Active';
                              }
                               ?></dd>
                          </dl>
                          
                      </div>
  
                  </div>
              </div>


       </div>




        



        <?php

      if(!empty($messasesList))
      {
          ?>
          <br>

          <div class="form-container">
                  <h4 class="form-group-title">Message Details</h4>

              

                <div class="custom-table">
                  <table class="table">
                      <thead>
                          <tr>
                          <th>Sl. No</th>
                           <th>Content</th>
                           <th>Date Time</th>
                           <!-- <th style="text-align: center;">Action</th> -->
                          </tr>
                      </thead>
                      <tbody>
                           <?php
                       $total = 0;
                        for($i=0;$i<count($messasesList);$i++)
                       { ?>
                          <tr>
                          <td><?php echo $i+1;?></td>
                          <td><?php
                          if($messasesList[$i]->programme_name == '')
                          {
                           echo $messasesList[$i]->Content;
                          }
                          else
                          {
                            echo $messasesList[$i]->programme_code . " - " . $messasesList[$i]->programme_name;
                          }
                           ?></td>
                          <td><?php echo date('d-m-Y H:i:s', strtotime($messasesList[$i]->SentOn)) ;?></td>
                          <!-- <td><?php echo $messasesList[$i]->grade ;?></td>
                          <td>
                          <?php if($messasesList[$i]->is_fail == 1)
                          {
                            echo 'Yes';
                          }
                          elseif ($messasesList[$i]->is_fail == 0)
                          {
                            echo 'No';
                          } ?>  
                          </td>
                          <td style="text-align: center;">
                          <a href='/examination/gradeSetup/add/<?php echo $id_programme;?>/<?php echo $messasesList[$i]->id;?>'>Edit</a> |
                           <a onclick="deleteMarksDistribution(<?php echo $messasesList[$i]->id; ?>)">Delete</a>
                          </td> -->

                          </tr>
                        <?php 
                    } 
                    ?>
                      </tbody>
                  </table>
                </div>

              </div>


        <form id="form_main" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Lead Details</h4>        
                

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Status <span class='error-text'>*</span></label>
                            <select name="lead_status" id="lead_status" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($statusList))
                                {
                                    foreach ($statusList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($record->id == $lead->lead_status)
                                    {
                                        echo 'selected';
                                    }
                                    ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-events">
                            <label>Next Action <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="next_action" name="next_action" value="<?php echo $lead->next_action;?>">
                        </div>
                    </div>

                </div>


                <div class="row">

                    <div class="col-sm-12">
                          <div class="form-group">
                            <label>Comments <span class='error-text'>*</span></label>
                             <textarea type="text" class="form-control" id="comment" name="comment"><?php echo $lead->comment; ?></textarea>
                          </div>
                    </div>

                </div>


            </div>

            <div class="button-block clearfix">
                <div class="bttn-events">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>

      <?php
      
      }
       ?>




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

    $('select').select2();

    CKEDITOR.replace('comment',{
    width: "100%",
    height: "300px"
    }); 

     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                lead_status: {
                    required: true
                },
                next_action: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                lead_status: {
                    required: "<p class='error-text'>Select Lead Status</p>",
                },
                next_action: {
                    required: "<p class='error-text'>Next Action Required</p>",
                },
                comment: {
                    required: "<p class='error-text'>Comment Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
