<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Template</h3>
        </div>
        <form id="form_sponser" action="" method="post">
            <div class="form-container">
                    <h4 class="form-group-title">Template Details</h4>  


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Subject <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="subject" name="subject">
                        </div>
                    </div>
                </div>



                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>University </label>
                            <select name="id_university" id="id_university" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($universityList))
                                {
                                    foreach ($universityList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>Education Level </label>
                            <select name="id_education_level" id="id_education_level" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($educationLevelList))
                                {
                                    foreach ($educationLevelList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>">
                                    <?php echo  $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>




                <div class="row custom-table">
                    <h3>Table Variables</h3>
                     <div class="col-sm-12 custom-table">
                        <table class="table">
                            <tr>
                                 <th>Text</th>
                                 <th>Variables</th>
                                 <th>Text</th>
                                 <th>Variables</th>
                            </tr>
                            <tr>
                                 <td>Student NAme</td>
                                 <td>@studentname</td>
                                 <td>Program</td>
                                 <td>@program</td>
                            </tr>
                            <tr>
                                <td>NRIC/Passport</td>
                                <td>@nric</td>
                                <td>Email</td>
                                <td>@email</td>
                            </tr>
                        </table>
                     </div>
                </div>
                <div class="row">


                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                          <label for="message">Description <span class='error-text'>*</span></label>
                          <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="message" id="message"></textarea>
                        </div>
                    </div>

                </div>

                <div class="row">
                    
                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "800px",
  height: "200px"

}); 

</script>
<script>

    $('select').select2();

    function getStateByCountry(id)
    {

        $.get("/sponser/sponser/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }


    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                subject: {
                    required: true
                },
                message: {
                    required: true
                },
                status: {
                    required: true
                },
                id_university: {
                    required: true
                },
                id_education_level: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                subject: {
                    required: "<p class='error-text'>Subject Required</p>",
                },
                message: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_university: {
                    required: "<p class='error-text'>Select University</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<style type="text/css">
    .shadow-textarea textarea.form-control::placeholder {
    font-weight: 300;
}
.shadow-textarea textarea.form-control {
    padding-left: 0.8rem;
}
</style>