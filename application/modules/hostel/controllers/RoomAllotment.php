<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RoomAllotment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_allotment_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('room_allotment.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $formData['id_building'] = $this->security->xss_clean($this->input->post('id_building'));
            $formData['id_block'] = $this->security->xss_clean($this->input->post('id_block'));
            $formData['id_room_type'] = $this->security->xss_clean($this->input->post('id_room_type'));
            $data['searchParam'] = $formData;

            $data['roomAllotmentList'] = $this->room_allotment_model->roomAllotmentListSearchActive($formData);
            // $data['studentList'] = $this->room_allotment_model->studentList('1');
            $data['buildingList'] = $this->room_allotment_model->buildingListByStatus('1');
            $data['hostelList'] = $this->room_allotment_model->hostelListByStatus('1');
            $data['roomTypeList'] = $this->room_allotment_model->roomTypeListByStatus('1');
            $data['blockList'] = $this->room_allotment_model->blockListByStatus('1');
            // echo "<Pre>"; print_r($data['roomAllotmentList']);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Room Allotment';
            $this->loadViews("room_allotment/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('room_allotment.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $id_room = $this->security->xss_clean($this->input->post('id_room'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $to_dt = $this->security->xss_clean($this->input->post('to_dt'));
                $id_room_type = $this->security->xss_clean($this->input->post('id_room_type'));

            
                $data = array(
                    'id_hostel' => $id_hostel,
                    'id_room' => $id_room,
                    'id_student' => $id_student,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'to_dt' => date('Y-m-d', strtotime($to_dt)),
                    'id_room_type' => $id_room_type,
                    'status' => 1,
                    'created_by' => $user_id
                );
            
                $inserted_id = $this->room_allotment_model->addNewRoomAllotment($data);
                redirect('/hostel/roomAllotment/list');
            }
            
            $data['programmeList'] = $this->room_allotment_model->programListByStatus('1');
            $data['degreeTypeList'] = $this->room_allotment_model->qualificationListByStatus('1');
            $data['hostelList'] = $this->room_allotment_model->getHostelRegistrationListByStatus('1');
            $data['roomTypeList'] = $this->room_allotment_model->getRoomTypeListByStatus('1');

            
            $this->global['pageTitle'] = 'Inventory Management : Add Room Allotment';
            $this->loadViews("room_allotment/add", $this->global, $data, NULL);
        }
    }

    function summaryList()
    {

        if ($this->checkAccess('room_allotment.summary_list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $formData['id_building'] = $this->security->xss_clean($this->input->post('id_building'));
            $formData['id_block'] = $this->security->xss_clean($this->input->post('id_block'));
            $formData['id_room_type'] = $this->security->xss_clean($this->input->post('id_room_type'));
            $data['searchParam'] = $formData;

            $data['roomAllotmentList'] = $this->room_allotment_model->roomAllotmentListSearch($formData);
            // $data['studentList'] = $this->room_allotment_model->studentList('1');
            $data['buildingList'] = $this->room_allotment_model->buildingListByStatus('1');
            $data['hostelList'] = $this->room_allotment_model->hostelListByStatus('1');
            $data['roomTypeList'] = $this->room_allotment_model->roomTypeListByStatus('1');
            $data['blockList'] = $this->room_allotment_model->blockListByStatus('1');
            // echo "<Pre>"; print_r($data['roomAllotmentList']);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Room Allotment';
            $this->loadViews("room_allotment/summary_list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('room_allotment.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/roomAllotment/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'percentage' => $percentage,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                
                $result = $this->room_allotment_model->editRoomAllotment($data,$id);
                redirect('/hostel/activityCode/list');
            }
            $data['roomAllotment'] = $this->room_allotment_model->getRoomAllotment($id);
            $data['studentDetail'] = $this->room_allotment_model->getStudentByStudentId ($data['roomAllotment']->id_student);
            $data['roomDetail'] = $this->room_allotment_model->getRoomByRoomId($data['roomAllotment']->id_room);

            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : View Room Allotment';
            $this->loadViews("room_allotment/edit", $this->global, $data, NULL);
        }
    }


    function view($id = NULL)
    {
        if ($this->checkAccess('room_allotment.summary_view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/roomAllotment/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'percentage' => $percentage,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                
                $result = $this->room_allotment_model->editRoomAllotment($data,$id);
                redirect('/hostel/activityCode/list');
            }
            $data['roomAllotment'] = $this->room_allotment_model->getRoomAllotment($id);
            $data['studentDetail'] = $this->room_allotment_model->getStudentByStudentId ($data['roomAllotment']->id_student);
            $data['roomDetail'] = $this->room_allotment_model->getRoomByRoomId($data['roomAllotment']->id_room);

            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : View Room Allotment';
            $this->loadViews("room_allotment/view", $this->global, $data, NULL);
        }
    }

    // For Student Search

     function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->room_allotment_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByProgNIntake()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $table.="<option value=".$id.">".$intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getStudentByProgNIntake()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->room_allotment_model->getStudentByProgNIntake($tempData);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_student' id='id_student' class='form-control' onchange='displaydata()'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // echo "<Pre>";print_r($student_list_data[$i]);exit;
        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id;
        $full_name = $student_list_data[$i]->full_name;
        $nric = $student_list_data[$i]->nric;
        $table.="<option value=".$id.">".$nric. " - " . $full_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function displaydata()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        // $temp_details = $this->room_allotment_model->getPerogramLandscape($id_intake,$id_programme);
        $student_data = $this->room_allotment_model->getStudentByStudentId($id_student);
        $student_allotment_count = $this->room_allotment_model->getRoomAllotmentDetailsByStudentId($id_student);

        // echo "<Pre>"; print_r($student_allotment_count);exit();
        $room_details = $this->room_allotment_model->getRoomAllotmentByStud($id_student);

        // echo "<Pre>"; print_r($room_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $gender = $student_data->gender;



        $table = "

            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>
                                <input type='hidden' name='student_allotment_count' id='student_allotment_count' value='$student_allotment_count' />
                                $student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>";
                    if($student_allotment_count > 0)
                        {
                            $id_room = $room_details->id_room;

                        $table.="
                    
                            <dl>
                                <dt>
                                <a href='../roomVacancy/view/$id_room/allotment' class='btn btn-link'><span style='font-size:18px;'>&#128065;</a>
                                Room Alloted :</dt>
                                <dd>$room_details->room_code - $room_details->room_name</dd>
                            </dl>
                            <dl>
                                <dt>
                                    Alloted From Date :</dt>
                                <dd>$room_details->from_dt</dd>
                            </dl>

                            ";

                        }

                            $table.="
                    
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>
                                Gender :</dt>
                                <dd>$gender</dd>
                            </dl>";

                        if($student_allotment_count > 0)
                        {
                            $id_room = $room_details->id_room;
                        $table.="
                    
                            <dl>
                                <dt>

                                   <a href='../roomVacancy/view/$id_room/allotment' class='btn btn-link'><span style='font-size:18px;'>&#128065;</a>
                                   Room Type :

                                   </dt>
                                <dd>$room_details->room_type_code - $room_details->room_type_name</dd>
                            </dl>
                            <dl>
                                <dt>
                                    Alloted To Date :</dt>
                                <dd>$room_details->to_dt</dd>
                            </dl>
                            ";

                        }

                        $table.="
                        </div>
    
                    </div>
                </div>";
        
        echo $table;
    }


    // For Room Search

     function getBuildingListByHostelId($id_hostel)
    {
            // echo "<Pre>"; print_r($id_hostel);exit;
            $results = $this->room_allotment_model->getBuildingListByHostelId($id_hostel);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
                <select name='id_building' id='id_building' class='form-control' onchange='getBlockListData()'>
                <option value=''>Select</option>

                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $code = $results[$i]->code;
            $table.="<option value=" . $id . ">" . $code . " - " . $name . 
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

     function getBlockList()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        $data['level'] = 2;
            // echo "<Pre>"; print_r($data);exit;
        $results = $this->room_allotment_model->getHostelRoomByData($data);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_block' id='id_block' class='form-control' onchange='getRoomByBlockNHostelId()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $code = $results[$i]->code;
            $table.="<option value=" . $id . ">" . $code . " - " . $name . 
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

     function getRoomByBlockNHostelId()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        $data['level'] = 3;
            // echo "<Pre>"; print_r($data);exit;
        $results = $this->room_allotment_model->getRoomByBlockNHostel($data);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_room' id='id_room' class='form-control' onchange='getRoomDetails()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $code = $results[$i]->code;
            $table.="<option value=" . $id . ">" . $code . " - " . $name . 
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }



    function getRoomDetails()
    {
        
        $data = $this->security->xss_clean($this->input->post('data'));
        $data['level'] = 3;
            // echo "<Pre>"; print_r($data);exit;
        $room_data = $this->room_allotment_model->getRoomDetails($data);

        // echo "<Pre>"; print_r($room_data);exit();


            $name = $room_data->name;
            $code = $room_data->code;
            $short_code = $room_data->short_code;
            $max_capacity = $room_data->max_capacity;
            $room_type_name = $room_data->room_type_name;
            $room_type_code = $room_data->room_type_code;
            $filled_seats = $room_data->filled_seats;
            $id_room_type = $room_data->id_room_type;
            $vacant_seats = $max_capacity - $filled_seats;



        $table = "

            <h4 class='sub-title'>Room Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Room Name :</dt>
                                <dd>$name</dd>
                            </dl>
                             <dl>
                                <dt>Room Type :</dt>
                                <dd><input type='hidden' name='id_room_type' id='id_room_type' value='$id_room_type'/>
                                $room_type_name - $room_type_code
                                </dd>
                            </dl>
                            <dl>
                                <dt>Filled Seats :</dt>
                                <dd>$filled_seats</dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Room Code :</dt>
                                <dd>$code</dd>
                            </dl>
                           <dl>
                                <dt>Max Capacity :</dt>
                                <dd>$max_capacity</dd>
                            </dl>
                            <dl>
                                <dt>Vacant Seats :</dt>
                                <dd><input type='hidden' name='vacant_seats' id='vacant_seats' value='$vacant_seats'/>
                                $vacant_seats</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";
        
        echo $table;
    }
}
