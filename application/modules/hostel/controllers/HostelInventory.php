<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class HostelInventory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('hostel_inventory_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('hostel_inventory.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['hostelInventoryList'] = $this->hostel_inventory_model->hostelInventoryListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : List Hostel Inventory';
            //print_r($subjectDetails);exit;
            $this->loadViews("hostel_inventory/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('hostel_inventory.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );
            
                $duplicate_row = $this->hostel_inventory_model->checkHostelInventoryDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Hostel Inventory Not Allowed";exit();
                }

                $result = $this->hostel_inventory_model->addNewHostelInventory($data);
                redirect('/hostel/hostelInventory/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add Hostel Inventory';
            $this->loadViews("hostel_inventory/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('hostel_inventory.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostelInventory/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $duplicate_row = $this->hostel_inventory_model->checkHostelInventoryDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Hostel Inventory Not Allowed";exit();
                }
                
                $result = $this->hostel_inventory_model->editHostelInventory($data,$id);
                redirect('/hostel/hostelInventory/list');
            }
            $data['hostelInventory'] = $this->hostel_inventory_model->getHostelInventory($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Hostel Inventory';
            $this->loadViews("hostel_inventory/edit", $this->global, $data, NULL);
        }
    }
}
