<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RoomVacancy extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_vacancy_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('room_vacancy.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $formData['id_building'] = $this->security->xss_clean($this->input->post('id_building'));
            $formData['id_block'] = $this->security->xss_clean($this->input->post('id_block'));
            $formData['id_room_type'] = $this->security->xss_clean($this->input->post('id_room_type'));
            $data['searchParam'] = $formData;

            $data['roomVacancyList'] = $this->room_vacancy_model->roomVacancyListSearch($formData);
            // $data['studentList'] = $this->room_vacancy_model->studentList('1');
            $data['hostelList'] = $this->room_vacancy_model->hostelListByStatus('1');
            $data['roomTypeList'] = $this->room_vacancy_model->roomTypeListByStatus('1');
            $data['buildingList'] = $this->room_vacancy_model->buildingListByStatus('1');
            // echo "<Pre>"; print_r($data['roomVacancyList']);exit;
            //print_r($subjectDetails);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Room Vacancy';
            $this->loadViews("room_vacancy/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_room = NULL)
    {
        if ($this->checkAccess('room_vacancy.allot_room') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $to_dt = $this->security->xss_clean($this->input->post('to_dt'));

                $room_detail = $this->room_vacancy_model->getRoomByRoomId($id_room);
            // echo "<Pre>"; print_r($room_detail);exit;

            
                $data = array(
                    'id_hostel' => $room_detail->id_hostel,
                    'id_room' => $id_room,
                    'id_student' => $id_student,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'to_dt' => date('Y-m-d', strtotime($to_dt)),
                    'id_room_type' => $room_detail->id_room_type,
                    'status' => 1,
                    'created_by' => $user_id
                );
            
                $inserted_id = $this->room_vacancy_model->addNewRoomAllotment($data);
                if($inserted_id)
                {

                }
                redirect('/hostel/roomVacancy/list');
            }
            $data['roomDetail'] = $this->room_vacancy_model->getRoomByRoomId($id_room);
            $data['programmeList'] = $this->room_vacancy_model->programListByStatus('1');
            $data['degreeTypeList'] = $this->room_vacancy_model->qualificationListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Add Room Allotment';
            $this->loadViews("room_vacancy/add", $this->global, $data, NULL);
        }
    }


    function view($id_room = NULL,$root)
    {
        if ($this->checkAccess('room_vacancy.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_room == null)
            {
                redirect('/hostel/roomVacancy/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'percentage' => $percentage,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                
                $result = $this->room_vacancy_model->editRoomAllotment($data,$id);
                redirect('/hostel/activityCode/list');
            }
            $data['roomDetail'] = $this->room_vacancy_model->getRoomByRoomId($id_room);
            $data['roomAllocationList'] = $this->room_vacancy_model->getRoomAllotmentListByRoomId($id_room);
            $data['root'] = $root;
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Room Allotment';
            $this->loadViews("room_vacancy/view", $this->global, $data, NULL);
        }
    }

    function displaydata()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));
         // echo "<Pre>"; print_r($id_student);exit();

        // $temp_details = $this->room_vacancy_model->getPerogramLandscape($id_intake,$id_programme);
        $student_data = $this->room_vacancy_model->getStudentByStudentId($id_student);
        $student_allotment_count = $this->room_vacancy_model->getRoomAllotmentDetailsByStudentId($id_student);

        // echo "<Pre>"; print_r($student_allotment_count);exit();
        $room_details = $this->room_vacancy_model->getRoomAllotmentByStud($id_student);

        // echo "<Pre>"; print_r($room_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $gender = $student_data->gender;



        $table = "

            <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>
                                <input type='hidden' name='student_allotment_count' id='student_allotment_count' value='$student_allotment_count' />
                                $student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>";
                    if($student_allotment_count > 0)
                        {
                            $id_room = $room_details->id_room;

                        $table.="
                    
                            <dl>
                                <dt>
                                <a href='../view/$id_room/vacancy_add' class='btn btn-link'><span style='font-size:18px;'>&#128065;</a>
                                Room Alloted :</dt>
                                <dd>$room_details->room_code - $room_details->room_name</dd>
                            </dl>
                            <dl>
                                <dt>
                                    Alloted From Date :</dt>
                                <dd>$room_details->from_dt</dd>
                            </dl>
                            ";

                        }

                            $table.="
                    
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>
                                Gender :</dt>
                                <dd>$gender</dd>
                            </dl>";

                        if($student_allotment_count > 0)
                        {
                            $id_room = $room_details->id_room;
                        $table.="
                    
                            <dl>
                                <dt>

                                   <a href='../view/$id_room/vacancy_add' class='btn btn-link'><span style='font-size:18px;'>&#128065;</a>
                                   Room Type :

                                   </dt>
                                <dd>$room_details->room_type_code - $room_details->room_type_name</dd>
                            </dl>

                            <dl>
                                <dt>
                                    Alloted To Date :</dt>
                                <dd>$room_details->to_dt</dd>
                            </dl>

                            
                            ";

                        }

                        $table.="
                        </div>
    
                    </div>
                </div>";
        
        echo $table;
    }
}
