<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BlockSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('block_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('block_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_hostel'] = $this->security->xss_clean($this->input->post('id_hostel'));
            $formData['id_building'] = $this->security->xss_clean($this->input->post('id_building'));
            $data['searchParam'] = $formData;

            $data['hostelList'] = $this->block_setup_model->getHostelRegistrationListByStatus('1');
            $data['blockList'] = $this->block_setup_model->blockListListSearch($formData);
            // echo "<Pre>";print_r($data);exit;
            $this->global['pageTitle'] = 'Inventory Management : List Unit Setup';
            $this->loadViews("block_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('block_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $id_building = $this->security->xss_clean($this->input->post('id_building'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 2,
                    'id_hostel' => $id_hostel,
                    'id_parent' => $id_building,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->block_setup_model->addNewHostelRoom($data);
            
                // $duplicate_row = $this->block_setup_model->checkRoomTypeDuplication($data);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }

                // $result = $this->block_setup_model->addNewRoomType($data);
                redirect('/hostel/blockSetup/list');
            }
            $data['hostelList'] = $this->block_setup_model->getHostelRegistrationListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Unit Setup';
            $this->loadViews("block_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('block_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/blockSetup/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $id_hostel = $this->security->xss_clean($this->input->post('id_hostel'));
                $id_building = $this->security->xss_clean($this->input->post('id_building'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'short_code' => $code,
                    'level' => 2,
                    'id_hostel' => $id_hostel,
                    'id_parent' => $id_building,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                // $duplicate_row = $this->block_setup_model->checkRoomTypeDuplicationEdit($data,$id);

                // if($duplicate_row)
                // {
                //     echo "Duplicate Room Type Not Allowed";exit();
                // }
                
                $result = $this->block_setup_model->editHostelRoom($data,$id);
                redirect('/hostel/blockSetup/list');
            }
            $data['hostelList'] = $this->block_setup_model->getHostelRegistrationListByStatus('1');
            $data['buildingList'] = $this->block_setup_model->getBuildingList();
            $data['blockSetup'] = $this->block_setup_model->getHostelRoom($id);
            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Unit Setup';
            $this->loadViews("block_setup/edit", $this->global, $data, NULL);
        }
    }

     function getBuildingListByHostelId($id_hostel)
    {
            // echo "<Pre>"; print_r($id_hostel);exit;
            $results = $this->block_setup_model->getBuildingListByHostelId($id_hostel);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_building' id='id_building' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $code = $results[$i]->code;
            $table.="<option value=" . $id . ">" . $code . " - " . $name . 
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
