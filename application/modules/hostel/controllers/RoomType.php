<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class RoomType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('room_type_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('room_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['roomTypeList'] = $this->room_type_model->roomTypeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : List Room Type';
            //print_r($subjectDetails);exit;
            $this->loadViews("room_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('room_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );
            
                $duplicate_row = $this->room_type_model->checkRoomTypeDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Room Type Not Allowed";exit();
                }

                $result = $this->room_type_model->addNewRoomType($data);
                redirect('/hostel/roomType/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add Room Type';
            $this->loadViews("room_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('room_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/room_type/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $duplicate_row = $this->room_type_model->checkRoomTypeDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate Room Type Not Allowed";exit();
                }
                
                $result = $this->room_type_model->editRoomType($data,$id);
                redirect('/hostel/roomType/list');
            }
            $data['roomType'] = $this->room_type_model->getRoomType($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Room Type';
            $this->loadViews("room_type/edit", $this->global, $data, NULL);
        }
    }
}
