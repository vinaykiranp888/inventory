<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class HostelItemRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('hostel_item_registration_model');
    }

    function list()
    {

        if ($this->checkAccess('hostel_item_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['hostelItemRegistrationList'] = $this->hostel_item_registration_model->hostelItemRegistrationListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : List Item Registration';
            //print_r($subjectDetails);exit;
            $this->loadViews("hostel_item_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('hostel_item_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );
            
                $duplicate_row = $this->hostel_item_registration_model->checkHostelItemRegistrationDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate hostel Item Registration Not Allowed";exit();
                }

                $result = $this->hostel_item_registration_model->addNewHostelItemRegistration($data);
                redirect('/hostel/hostelItemRegistration/list');
            }
            
            $this->global['pageTitle'] = 'Inventory Management : Add Item Registration';
            $this->loadViews("hostel_item_registration/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('hostel_item_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/hostel/hostel_item_registration/list');
            }
            
            $user_id = $this->session->userId;
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'name_in_malay' => $name_in_malay,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $duplicate_row = $this->hostel_item_registration_model->checkHostelItemRegistrationDuplicationEdit($data,$id);

                if($duplicate_row)
                {
                    echo "Duplicate hostel Item Registration Not Allowed";exit();
                }
                
                $result = $this->hostel_item_registration_model->editHostelItemRegistration($data,$id);
                redirect('/hostel/hostelItemRegistration/list');
            }
            $data['hostelItemRegistration'] = $this->hostel_item_registration_model->getHostelItemRegistration($id);
            // echo "<Pre>";print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Item Registration';
            $this->loadViews("hostel_item_registration/edit", $this->global, $data, NULL);
        }
    }
}
