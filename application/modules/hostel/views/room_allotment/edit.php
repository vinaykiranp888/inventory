<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Room Allotment</h3>
        </div>
        <form id="form_grade" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetail->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetail->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetail->email_id; ?></dd>
                            </dl>                         
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetail->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetail->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Room Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Room Name :</dt>
                                <dd><?php echo ucwords($roomDetail->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Room Type :</dt>
                                <dd><?php echo $roomDetail->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Max Capacity :</dt>
                                <dd><?php echo $roomDetail->max_capacity; ?></dd>
                            </dl>                         
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Hostel :</dt>
                                <dd><?php echo $roomDetail->hostel_name . " - " . $roomDetail->hostel_code ?></dd>
                            </dl>
                            <dl>
                                <dt>Vacant Seats :</dt>
                                <dd><?php echo $roomDetail->max_capacity - $roomAllotment->filled_count; ?></dd>
                            </dl>
                            <dl>
                                <dt>Filled Seats :</dt>
                                <dd><?php echo $roomAllotment->filled_count; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
            <h4 class="form-group-title">Room Allocation Details</h4>



                <div class="row">
                   
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>From Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="from_dt" name="from_dt" value="<?php echo $roomAllotment->from_dt; ?>" readonly>
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <label>TO Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="to_dt" name="to_dt" value="<?php echo $roomAllotment->to_dt; ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Allotment Status <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                            if($roomAllotment->to_dt > date('Y-m-d'))
                            {
                                echo "Active";
                            }
                            else
                            {
                                echo "Vacated";
                            }
                             ?>" readonly="readonly">
                        </div>
                    </div>



                </div>


            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
   $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                },
                 percentage: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                percentage: {
                    required: "<p class='error-text'>percentage required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>