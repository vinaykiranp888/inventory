<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        
        <form id="form_grade" action="" method="post">

            <div class="page-title clearfix">
            <h3>Hostel Details</h3>
            </div>
            <div class="form-container">
                <h4 class="form-group-title">Hostel Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Hostel Name :</dt>
                                <dd><?php echo ucwords($hostelDetails->name);?></dd>
                            </dl>
                            <dl>
                                <dt>Staff Incharge :</dt>
                                <dd><?php echo $hostelDetails->ic_no . " - " . $hostelDetails->staff_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel Address :</dt>
                                <dd>
                                    <?php echo $hostelDetails->address ?></dd>
                            </dl> 
                            <dl>
                                <dt>Hostel City :</dt>
                                <dd><?php echo $hostelDetails->city ?></dd>
                            </dl>  
                                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Hostel Code :</dt>
                                <dd><?php echo $hostelDetails->code ?></dd>
                            </dl>
                            <dl>
                                <dt>Contact Number :</dt>
                                <dd><?php echo $hostelDetails->contact_number ?></dd>
                            </dl>  
                            <dl>
                                <dt>Hostel Landmark :</dt>
                                <dd><?php echo $hostelDetails->landmark; ?></dd>
                            </dl>
                            <dl>
                                <dt>Hostel State :</dt>
                                <dd><?php echo $hostelDetails->state; ?></dd>
                            </dl>
                            
                        </div>
                    </div>
                </div>
            </div>

            <br>



        <div class="page-title clearfix">
        <h3>Edit Building</h3>
        </div>

        <div class="form-container">
                <h4 class="form-group-title">Building Details</h4>
            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $hostelRoom->short_code; ?>">
                        <input type="hidden" class="form-control" id="code1" name="code1" value="<?php echo $hostelDetails->code; ?>">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $hostelRoom->name; ?>">
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="<?php echo '../../add/'.$hostelDetails->id?>" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
   $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>