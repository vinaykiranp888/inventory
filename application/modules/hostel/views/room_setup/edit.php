<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Room</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Room Details</h4>



            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $roomSetup->code;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $roomSetup->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Hostel <span class='error-text'>*</span></label>
                        <select name="id_hostel" id="id_hostel" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $roomSetup->id_hostel)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Apartment <span class='error-text'>*</span></label>
                        <select name="id_building" id="id_building" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($buildingList))
                            {
                                foreach ($buildingList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $blockSetup->id_parent)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Unit <span class='error-text'>*</span></label>
                        <select name="id_block" id="id_block" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($blockList))
                            {
                                foreach ($blockList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $roomSetup->id_parent)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Room Type <span class='error-text'>*</span></label>
                            <select name="id_room_type" id="id_room_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($roomTypeList))
                                {
                                    foreach ($roomTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $roomSetup->id_room_type)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Capacity <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="max_capacity" name="max_capacity" value="<?php echo $roomSetup->max_capacity; ?>" min="1" max="10">
                    </div>
                </div>


               <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($roomSetup->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($roomSetup->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>



        <div class="form-container">
            <h4 class="form-group-title"> Program Tagging</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Inventory Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">


                        <form id="form_programme_head" action="" method="post">


                            <div class="form-container">
                                <h4 class="form-group-title">Inventory Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Inventory <span class='error-text'>*</span></label>
                                            <select name="id_inventory" id="id_inventory" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($inventoryList))
                                                {
                                                    foreach ($inventoryList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="forintake_has_programmem-group">
                                            <label>Quantity <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="quantity" name="quantity" autocomplete="off">
                                        </div>
                                    </div>
                                  
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveInventoryData()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>


                        </form>


                        </div> 
                    </div>

                </div>



            </div>
        </div>









    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function saveInventoryData()
    {
        // if($('#form_programme_head').valid())
        // {

        var tempPR = {};

        tempPR['id_inventory'] = $("#id_inventory").val();
        tempPR['quantity'] = $("#quantity").val();
        tempPR['id_room'] = <?php echo $id_room;?>;

            $.ajax(
            {
               url: '/hostel/roomSetup/addInventoryData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                // window().location.reload();
                // $('#myModal').modal('hide');
               }
            });
        // }
    }


    function deleteRoomInventory(id)
    {
        // alert(id);
        var tempPR = {};

        tempPR['id'] = id;
        tempPR['id_room'] = <?php echo $id_room;?>;

        $.ajax(
            {
               url: '/hostel/roomSetup/deleteRoomInventory',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                // window().location.reload();
                // $('#myModal').modal('hide');
               }
            });

        

        // $.ajax(
        // {
        //    url: '/hostel/roomSetup/deleteRoomInventory/'+id,
        //    type: 'GET',
        //    error: function()
        //    {
        //     alert('Something is wrong');
        //    },
        //    success: function(result)
        //    {
        //     $("#view").html(result);
        //     window().location.reload();
        //    }
        // });
    }







   $(document).ready(function() {
    saveInventoryData();
        $("#form_bank").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                id_hostel: {
                    required: true
                },
                id_building: {
                    required: true
                },
                id_block: {
                    required: true
                },
                id_room_type: {
                    required: true
                },
                max_capacity: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_hostel: {
                    required: "<p class='error-text'>Select Hostel</p>",
                },
                id_building: {
                    required: "<p class='error-text'>Select Apartment</p>",
                },
                id_block:{
                    required: "<p class='error-text'>Select Unit</p>",
                },
                id_room_type:{
                    required: "<p class='error-text'>Select Room</p>",
                },
                max_capacity:{
                    required: "<p class='error-text'>Max. Capacity Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
