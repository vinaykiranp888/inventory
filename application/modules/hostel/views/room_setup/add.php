<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Room</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Room Details</h4>

            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Hostel<span class='error-text'>*</span></label>
                        <select name="id_hostel" id="id_hostel" class="form-control" onchange="getBuildingByHostel(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
            </div>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Apartment <span class='error-text'>*</span></label>
                        <span id="view_building"></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Unit <span class='error-text'>*</span></label>
                        <span id="view_block"></span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Room Type <span class='error-text'>*</span></label>
                        <select name="id_room_type" id="id_room_type" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($roomTypeList))
                            {
                                foreach ($roomTypeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Capacity <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_capacity" name="max_capacity" min="1" max="10">
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
            
        </form>


        <div class="form-container">
            <h4 class="form-group-title"> Program Tagging</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Inventory Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">


                        <form id="form_programme_head" action="" method="post">


                            <div class="form-container">
                                <h4 class="form-group-title">Inventory Details</h4>


                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Inventory <span class='error-text'>*</span></label>
                                            <select name="id_inventory" id="id_inventory" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($inventoryList))
                                                {
                                                    foreach ($inventoryList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code ." - ".$record->name; ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="forintake_has_programmem-group">
                                            <label>Quantity <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="quantity" name="quantity" autocomplete="off">
                                        </div>
                                    </div>
                                  
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>

                                </div>

                                <div class="row">
                                    <div id="view"></div>
                                </div>

                            </div>


                        </form>


                        </div> 
                    </div>

                </div>



            </div>
        </div> 

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    function saveData()
    {
        if($('#form_programme_head').valid())
        {

        var tempPR = {};

        tempPR['id_inventory'] = $("#id_inventory").val();
        tempPR['quantity'] = $("#quantity").val();

            $.ajax(
            {
               url: '/hostel/roomSetup/tempAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }


    function getBuildingByHostel(id)
    {

        $.get("/hostel/roomSetup/getBuildingListByHostelId/"+id, function(data, status){
       
            $("#view_building").html(data);
        });
    }

    function getBlockListData()
    {
            // alert('id_hostel');

        var id_hostel = $("#id_hostel").val();
        var id_building = $("#id_building").val();

        if(id_hostel != '' && id_building != '')
        {

        var tempPR = {};
        tempPR['id_hostel'] = id_hostel;
        tempPR['id_building'] = id_building;

            $.ajax(
            {
               url: '/hostel/roomSetup/getBlockList',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Block Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_block").html(result);
                }
               }
            });
        }
    }

    function validateDetailsData()
    {
        if($('#form_bank').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam == '')
            {
                alert("Add Inventory To The Room");
            }
            else
            {
                $('#form_bank').submit();
            }
        }    
    }


    function deleteTempRoomInventory(id)
    {
        // alert(id);
        $.ajax(
        {
           url: '/hostel/roomSetup/deleteTempRoomInventory/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
                $("#view").html(result);
           }
        });
    }




    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                id_hostel: {
                    required: true
                },
                id_building: {
                    required: true
                },
                id_block: {
                    required: true
                },
                id_room_type: {
                    required: true
                },
                max_capacity:{
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_hostel: {
                    required: "<p class='error-text'>Select Hostel</p>",
                },
                id_building: {
                    required: "<p class='error-text'>Select Apartment</p>",
                },
                id_block:{
                    required: "<p class='error-text'>Select Unit</p>",
                },
                id_room_type:{
                    required: "<p class='error-text'>Select Room</p>",
                },
                max_capacity:{
                    required: "<p class='error-text'>Max. Capacity Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_programme_head").validate({
            rules: {
                id_inventory: {
                    required: true
                },
                quantity: {
                    required: true
                }
            },
            messages: {
                id_inventory: {
                    required: "<p class='error-text'>Select Inventory</p>",
                },
                quantity: {
                    required: "<p class='error-text'>Effective Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




</script>
<script type="text/javascript">
    $('select').select2();
</script>