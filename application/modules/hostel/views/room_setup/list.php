<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Room</h3>
      <a href="add" class="btn btn-primary">+ Add Room</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Room Name </label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php $searchParam['name'] ?>">
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Hostel </label>
                    <div class="col-sm-8">
                      <select name="id_hostel" id="id_hostel" class="form-control" onchange="getBuildingByHostel(this.value)">
                        <option value="">Select</option>
                        <?php
                        if (!empty($hostelList)) {
                          foreach ($hostelList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_hostel']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                </div>

                <div class="row">



                  <div class="col-sm-6" id="view_apartment">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Apartment </label>
                      <div class="col-sm-8">
                        <!-- <span id="view_building"></span> -->
                        <select name="id_building" id="id_building" class="form-control" onchange="getBlockListData()" id="view_building_select">
                          <option value="">Select</option>
                          <?php
                          if (!empty($apartmentList)) {
                            foreach ($apartmentList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_building']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6" id="view_by_building" style="display: none;">
                   <div class="form-group">
                    <label class="col-sm-4 control-label">Apartment </label>
                    <div class="col-sm-8">
                      <span id="view_building" style="width: 398px"></span>
                    </div>
                   </div>
                  </div>



                <div class="col-sm-6" id="view_block_list">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Unit </label>
                    <div class="col-sm-8">
                      <!-- <span id="view_building"></span> -->
                      <select name="id_block" id="id_block" class="form-control" onchange="getBlockListData()">
                        <option value="">Select</option>
                        <?php
                        if (!empty($blockList)) {
                          foreach ($blockList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_block']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6" id="view_by_block" style="display: none;">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Unit </label>
                    <div class="col-sm-8">
                      <span id="view_block" style="width: 398px"></span>
                    </div>
                  </div>
                </div>

                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Code</th>
            <th>Name</th>
            <th>Max. Capacity</th>
            <th>Hostel</th>
            <th>Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($roomList))
          {
            $i=1;
            foreach ($roomList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->max_capacity ?></td>
                <td><?php echo $record->hostel_code . " - " . $record->hostel_name ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  function getBuildingByHostel(id)
    {
        $.get("/hostel/roomSetup/getBuildingListByHostelId/"+id,
          function(data, status)
          {
            $("#view_building").html(data);
            $("#view_by_building").show();
            $("#view_apartment").hide();
            $("#view_building_select").hide();
            
          });
    }


    function getBlockListData()
    {

        var id_hostel = $("#id_hostel").val();
        var id_building = $("#id_building").val();

            // alert(id_building);
        if(id_hostel != '' && id_building != '')
        {

        var tempPR = {};
        tempPR['id_hostel'] = id_hostel;
        tempPR['id_building'] = id_building;

            $.ajax(
            {
               url: '/hostel/roomSetup/getBlockList',
               type: 'POST',
               data:
               {
                data: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                if(result == '')
                {
                    alert('No Block Found For Entered Data')
                }
                else
                {
                    // alert(result);
                    $("#view_block").html(result);
                    $("#view_by_block").show();
                    $("#view_block_list").hide();
                }
               }
            });
        }
    }
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>