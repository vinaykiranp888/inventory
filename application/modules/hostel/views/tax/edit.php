<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Tax</h3>
        </div>
        <form id="form_tax" action="" method="post">
            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax Code *</label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $tax->code; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax Name *</label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $tax->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Tax Percentage *</label>
                        <input type="number" class="form-control" id="percentage" name="percentage" value="<?php echo $tax->percentage; ?>" min="1" max="99">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($tax->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($tax->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_tax").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                percentage: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Name required",
                },
                code: {
                    required: "Code required",
                },
                percentage: {
                    required: "Enter Percentage",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
