<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Unit</h3>
        </div>
        <form id="form_bank" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Unit Details</h4>



            <div class="row">

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $blockSetup->code;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $blockSetup->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Hostel <span class='error-text'>*</span></label>
                        <select name="id_hostel" id="id_hostel" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($hostelList))
                            {
                                foreach ($hostelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $blockSetup->id_hostel)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Apartment <span class='error-text'>*</span></label>
                        <select name="id_building" id="id_building" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($buildingList))
                            {
                                foreach ($buildingList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $blockSetup->id_parent)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


               <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($blockSetup->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($blockSetup->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

   $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                id_hostel: {
                    required: true
                },
                id_building: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                id_hostel: {
                    required: "<p class='error-text'>Select Hostel</p>",
                },
                id_building: {
                    required: "<p class='error-text'>Select Building</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
