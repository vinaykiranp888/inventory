<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model
{
    function roleListingCount()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role, BaseTbl.status');
        $this->db->from('roles as BaseTbl');
         $query = $this->db->get();
        
        return $query->num_rows();
    }

    function roleListForUserAdd()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('roles as BaseTbl');
        $this->db->where('id !=', 1);
         $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    function latestRoleListing()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role, BaseTbl.status');
        $this->db->from('roles as BaseTbl');
        $this->db->limit(10, 0);
        $this->db->order_by("id_role", "desc");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function roleListing($role)
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role, BaseTbl.status');
        $this->db->from('roles as BaseTbl');
        if(!empty($role)) {
        $likeCriteria = "(BaseTbl.role  LIKE '%".$role."%')";
        $this->db->where($likeCriteria);
        }
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function rolePermissions($roleId)
    {
        $this->db->select('BaseTbl.id');
        $this->db->from('role_permissions as BaseTbl');
        $this->db->where('id_role', $roleId);
        $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function addNewRole($roleInfo)
    {
        $this->db->trans_start();
        $this->db->insert('roles', $roleInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    function updateRolePermissions($permissions,$roleId)
    {
        $this->db->trans_start();
        $SQL = "delete from role_permissions where id_role = ".$roleId;
        $query = $this->db->query($SQL);
        foreach($permissions as $permission)
        {
            $info = array(
                'id_role'=>$roleId,
                'id'=>$permission
            );
            $this->db->insert('role_permissions', $info);
        }
        
        $this->db->trans_complete();
        
        return ;
    }
    
    function getRoleInfo($roleId)
    {
        $this->db->select('id as roleId, role, status');
        $this->db->from('roles');
        $this->db->where('id', $roleId);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }

    function checkAccess($id_role,$code)
    {
        // return 0;
        $this->db->select('rp.id');
        $this->db->from('role_permissions as rp');
        $this->db->join('permissions as p', 'rp.id_permission = p.id');

        $this->db->where('rp.id_role', $id_role);
        $this->db->where('p.code', $code);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;
        if(empty($result))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
    function editRole($roleInfo, $roleId)
    {
        $this->db->where('id', $roleId);
        $this->db->update('roles', $roleInfo);
        
        return TRUE;
    }

    function deletePermission($id)
    {
         $this->db->where('id_role', $id);
        $this->db->delete('role_permissions');
        return TRUE;

    }

    function addPermission($permissionInfo)
    {

        $this->db->trans_start();
        $this->db->insert('role_permissions', $permissionInfo);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function deleteRole($roleId, $roleInfo)
    {
        $this->db->where('id', $roleId);
        $this->db->update('roles', $roleInfo);
        
        return $this->db->affected_rows();
    }

    function getMenu()
    {
        $this->db->select('c.*');
        $this->db->from('menu as c');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function menuList()
    {
        $this->db->select('DISTINCT(module_name) as name');
        $this->db->from('menu');
        $query = $this->db->get();
        
        return $query->result();
    }

    function getUsermenu($id)
    {
        $this->db->select('c.*');
        $this->db->from('permissions as c');
        $this->db->where('c.id_menu', $id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function checkPermission($roleId,$id)
    {
        $this->db->select('c.*');
        $this->db->from('role_permissions as c');
        $this->db->where('c.id_role', $roleId);
        $this->db->where('c.id_permission', $id);

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}