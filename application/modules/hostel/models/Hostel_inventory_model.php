<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Hostel_inventory_model  extends CI_Model
{

    function hostelInventoryListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_inventory_registration');
        if (!empty($data))
        {
            $likeCriteria = "(code  LIKE '%" . $data . "%' or name  LIKE '%" . $data . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getHostelInventory($id)
    {
        $this->db->select('*');
        $this->db->from('hostel_inventory_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkHostelInventoryDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_inventory_registration');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function checkHostelInventoryDuplicationEdit($data,$id)
    {
        $this->db->select('*');
        $this->db->from('hostel_inventory_registration');
        $this->db->where('code', $data['code']);
        $this->db->where('id !=', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewHostelInventory($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_inventory_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editHostelInventory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_inventory_registration', $data);
        return TRUE;
    }
}

