<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Room_setup_model extends CI_Model
{
    function getHostelRegistrationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getBuildingListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('status', $status);
        $this->db->where('level', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getBuildingList()
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function blockListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '2');
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function roomListListSearch($data)
    {
        $this->db->select('fc.*, hr.name as hostel_name, hr.code as hostel_code');
        $this->db->from('hostel_room as fc');
        $this->db->join('hostel_registration as hr', 'fc.id_hostel = hr.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(fc.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        {
        if ($data['id_hostel'] != '')
            $this->db->where('fc.id_hostel', $data['id_hostel']);
        }
        if ($data['id_block'] != '')
        {
            $this->db->where('fc.id_parent', $data['id_block']);
        }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('fc.id_intake', $data['id_intake']);
        // }

        $this->db->where('fc.level', '3');
        $this->db->order_by("fc.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getHostelRoom($id)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function addNewHostelRoom($data)
    {
        $this->db->trans_start();
        $this->db->insert('hostel_room', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editHostelRoom($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('hostel_room', $data);
        return TRUE;
    }

    function checkHostelRoomDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('code', $data['code']);
        $query = $this->db->get();
        return $query->row();
    }

    function deleteInventoryAllotment($id_inventory_allotment)
    {
        $this->db->where('id', $id_inventory_allotment);
        $this->db->delete('inventory_allotment');
        return TRUE;
    }

    function getBuildingListByHostelId($id_hostel)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '1');
        $this->db->where('id_hostel', $id_hostel);
        $this->db->where('status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    function getHostelRoomByData($data)
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        if($data['level'] != '')
        {
            $this->db->where('level', $data['level']);
        }
         if($data['id_building'] != '')
        {
            $this->db->where('id_parent', $data['id_building']);
        }
        if($data['id_hostel'] != '')
        {
            $this->db->where('id_hostel', $data['id_hostel']);
        }
            $this->db->where('status', '1');
        $this->db->order_by("id", "ASC");
         // print_r($db->query);exit();     
         $query = $this->db->get();
         $result = $query->result();   
         return $result;
    }

    function getRoomTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function inventoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('hostel_inventory_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getRoomTypeList()
    {
         $this->db->select('*');
        $this->db->from('hostel_room_type');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function blockList()
    {
        $this->db->select('*');
        $this->db->from('hostel_room');
        $this->db->where('level', '2');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addRoomInventoryAllotment($data)
    {
        $this->db->trans_start();
        $this->db->insert('inventory_allotment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;       

    }

    function addTempRoomInventoryAllotment($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_inventory_allotment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempRoomInventoryAllotmentBySession($id_session)
    {
        $this->db->select('tia.*, ia.name as inventory_name, ia.code as inventory_code');
        $this->db->from('temp_inventory_allotment as tia');
        $this->db->join('hostel_inventory_registration as ia', 'tia.id_inventory = ia.id');
        $this->db->where('tia.id_session', $id_session);
        // $this->db->where('hr.level', '3');
        $query = $this->db->get();
        return $query->result();
    }

    function getTempRoomInventory($id_session)
    {
        $this->db->select('tia.*');
        $this->db->from('temp_inventory_allotment as tia');
        $this->db->join('hostel_inventory_registration as ia', 'tia.id_inventory = ia.id');
        $this->db->where('tia.id_session', $id_session);
        // $this->db->where('hr.level', '3');
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempRoomInventoryAllotment($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_inventory_allotment');
        return TRUE;
    }

    function deleteRoomInventory($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('inventory_allotment');
        return TRUE;
    }

    function deleteTempRoomInventoryAllotmentBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_inventory_allotment');
        return TRUE;
    }

    function moveTempToDetails($id_room)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempRoomInventory($id_session);

        foreach ($temp_details as $temp_detail)
        {
            unset($temp_detail->id_session);
            unset($temp_detail->id);
            $temp_detail->id_room = $id_room;

            $inserted_id = $this->addRoomInventoryAllotment($temp_detail);
        }

        $deleted = $this->deleteTempRoomInventoryAllotmentBySession($id_session);
    }

    function getRoomInventoryAllotment($id_room)
    {
        $this->db->select('tia.*, ia.name as inventory_name, ia.code as inventory_code');
        $this->db->from('inventory_allotment as tia');
        $this->db->join('hostel_inventory_registration as ia', 'tia.id_inventory = ia.id');
        $this->db->where('tia.id_room', $id_room);
        // $this->db->where('hr.level', '3');
        $query = $this->db->get();
        return $query->result();
    }
}