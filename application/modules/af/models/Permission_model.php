<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Permission_model (Permission Model)
 * Permission model class to get to handle permission related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Permission_model extends CI_Model
{
    /**
     * This function is used to get the permission listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function permissionListingCount()
    {
        $this->db->select('id, code,description, module ');
        $this->db->from('permissions');
         $query = $this->db->get();
        
        return $query->num_rows();
    }
    /**
     * This function is used to get the permission listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function latestPermissionListing()
    {
        $this->db->select('BaseTbl.id, BaseTbl.code,BaseTbl.description');
        $this->db->from('permissions as BaseTbl');
        $this->db->limit(10, 0);
        $this->db->order_by("id", "desc");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    /**
     * This function is used to get the permission listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function permissionListingSearch($data)
    {
        $this->db->select('id, code,description, module');
        $this->db->from('permissions');
        if($data['name'] != '')
        {
            $likeCriteria = "(code  LIKE '%" . $data['name'] . "%' or description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['module'] != '')
        {
            $likeCriteria = "(module  LIKE '%" . $data['module'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("id", "desc");
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

    function permissionListing()
    {
        $this->db->select('id, code,description, module');
        $this->db->from('permissions');
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }

     /**
     * This function is used to add new permission to system
     * @return number $insert_id : This is last inserted id
     */
    function addNewPermission($permissionInfo)
    {
        $this->db->trans_start();
        $this->db->insert('permissions', $permissionInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    /**
     * This function used to get permission information by id
     * @param number $id : This is permission id
     * @return array $result : This is permission information
     */
    function getPermissionInfo($id)
    {
        $this->db->select('id, code,description, module, id_menu');
        $this->db->from('permissions');
        $this->db->where('id', $id);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    
    /**
     * This function is used to update the permission information
     * @param array $permissionInfo : This is permissions updated information
     * @param number $id : This is permission id
     */
    function editPermission($permissionInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('permissions', $permissionInfo);
        
        return TRUE;
    }
    
    
    
    /**
     * This function is used to delete the permission information
     * @param number $id : This is permission id
     * @return boolean $result : TRUE / FALSE
     */
    function deletePermission($id, $permissionInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('permissions', $permissionInfo);
        
        return $this->db->affected_rows();
    }

    function menuList()
    {
        $this->db->select('DISTINCT(module_name) as name');
        $this->db->from('menu');
        $query = $this->db->get();
        
        return $query->result();
    }


    function getSubMenuByModule($menu)
    {
        $this->db->select('*');
        $this->db->from('menu');
        $this->db->where('module_name', $menu);
        $query = $this->db->get();
        
        return $query->result();
    }

}

  