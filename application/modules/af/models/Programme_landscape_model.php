<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_landscape_model extends CI_Model
{
    function programmeLandscapeList()
    {
        $this->db->select('pl.*, p.name as programme, i.name as intake');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function getProgramme($id)
    {
        $this->db->select('p.*, el.name as education_level_name');
        $this->db->from('programme as p');
        $this->db->join('education_level as el', 'p.id_education_level = el.id','left');
        $this->db->where('p.id', $id);
         $query = $this->db->get();
         
         $result = $query->row();  
         return $result;
    }

    function programmeLandscapeListSearch($formData)
    {

        $this->db->select('pl.*, p.name as programme, i.name as intake');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->order_by("pl.id", "desc");
        if($formData['id_programme']) {
            $likeCriteria = "(pl.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['name'])
        {
            $likeCriteria = "(pl.name  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }

         if($formData['id_intake']) {
            $likeCriteria = "(pl.id_intake  LIKE '%" . $formData['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function programmeLandscapeByProgrammeId($id_programme,$formData)
    {
        $this->db->select('pl.*,p.code as programme_code, p.name as programme, i.year as intake_year, i.name as intake, phs.mode_of_study, phs.mode_of_program');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
         $this->db->join('intake as i', 'pl.id_intake = i.id');
         // $this->db->join('scheme as s', 'pl.program_scheme = s.id','left');
         // , s.description as scheme_name
         $this->db->join('programme_has_scheme as phs', 'pl.learning_mode = phs.id','left');
        if($formData['id_programme']) {
            $likeCriteria = "(pl.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }

        if($formData['name']) {
            $likeCriteria = "(pl.name  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }

         if($formData['id_intake']) {
            $likeCriteria = "(pl.id_intake  LIKE '%" . $formData['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function programmeLandscapeListByProgrammeId($id_programme)
    {
        $this->db->select('pl.*,p.code as programme_code, p.name as programme, i.year as intake_year, i.name as intake, s.description as scheme_name, phs.mode_of_study, phs.mode_of_program');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake = i.id');
        $this->db->join('scheme as s', 'pl.program_scheme = s.id');
        $this->db->join('programme_has_scheme as phs', 'pl.learning_mode = phs.id','left');
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->order_by("pl.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         // echo "<pre>";print_r($result);exit;
         return $result;
    }

    function addCourseToProgramLandscape($data)
    {
        $this->db->trans_start();
        $this->db->insert('add_course_to_program_landscape', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCompulsoryCourse($id)
    {
        $this->db->select('cpl.*, c.name as course_name, c.code as course_code, c.credit_hours,pl.name as programName, i.name as intake');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        // $this->db->join('landscape_course_type as lct', 'cpl.course_type = lct.id');
        // ,lct.name as courseTypename
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('id_program_landscape', $id);
        $this->db->order_by("cpl.id_semester", "ASC");
                $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $results = $query->result();

         $data = array();
         foreach ($results as $result)
         {
            $id_compulsary_course = $result->id;
            $compulsary_requisites = $this->getCourseRequisitData($id_compulsary_course);
            $result->compulsary_requisites = $compulsary_requisites;

            array_push($data, $result);
         }

        return $data;
    }


    function getCompulsoryCourseForCopy($id)
    {
        $this->db->select('cpl.*');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('landscape_course_type as lct', 'cpl.course_type = lct.id');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('id_program_landscape', $id);
        $this->db->order_by("cpl.id_semester", "ASC");
                $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        $results = $query->result();

         // $data = array();
         // foreach ($results as $result)
         // {
         //    $id_compulsary_course = $result->id;
         //    $compulsary_requisites = $this->getCourseRequisitData($id_compulsary_course);
         //    $result->compulsary_requisites = $compulsary_requisites;

         //    array_push($data, $result);
         // }

        return $results;
    }


    function getMajorCourse($id)
    {
        $ctype ="Major";
        $this->db->select('cpl.*, c.name as course_name, c.code as course_code, c.credit_hours, pl.name as programName, i.name as intake, phmd.code as major_code, phmd.name as major_name');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('program_has_major_details as phmd', 'cpl.id_program_major = phmd.id');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('cpl.id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getMinorCourse($id)
    {
        $ctype ="Minor";
        $this->db->select('cpl.*, c.name as course_name, c.code as course_code, c.credit_hours,pl.name as programName, i.name as intake, phmd.code as minor_code, phmd.name as minor_name');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('program_has_minor_details as phmd', 'cpl.id_program_minor = phmd.id');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('cpl.id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getNotCompulsoryCourse($id)
    {
        $ctype ="Not-Compulsory";
        $this->db->select('cpl.*, c.name as course_name, c.code as course_code, c.credit_hours,  pl.name as programName, i.name as intake');
        $this->db->from('add_course_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->where('cpl.id_program_landscape', $id);
        $this->db->where('course_type', $ctype);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getLearningModeByPLID($id)
    {
        $this->db->select('cpl.*, c.name as course_name, c.code as course_code, c.credit_hours,pl.name as programName, i.name as intake, phs.mode_of_study, phs.mode_of_program');
        $this->db->from('add_learning_mode_to_program_landscape as cpl');
        $this->db->join('course as c', 'cpl.id_course = c.id');
        $this->db->join('intake as i', 'cpl.id_intake = i.id');
        $this->db->join('programme_landscape as pl', 'cpl.id_program_landscape = pl.id');
        $this->db->join('programme_has_scheme as phs', 'cpl.id_learning_mode = phs.id','left');
        $this->db->where('cpl.id_program_landscape', $id);
        $this->db->order_by("cpl.id_semester", "ASC");
        $this->db->order_by("cpl.id_semester", "ASC");
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function landscapeCourseTypeListSearch()
    {
        $this->db->select('*');
        $this->db->from('landscape_course_type');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



    function getProgrammeLandscapeDetails($id)
    {
        $this->db->select('pl.*, p.name as programme, i.name as intake, i.year as intake_year');
        $this->db->from('programme_landscape as pl');
        $this->db->join('programme as p', 'pl.id_programme = p.id');
        $this->db->join('intake as i', 'pl.id_intake_to = i.id','left');
        $this->db->where('pl.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramLearningModeProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }



 function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('s.description,s.id');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as s', 'phs.id_scheme = s.id');

        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    
    function addNewProgrammeLandscapeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_landscape', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addProgramObjectiveToProgramLandscape($data)
    {
        $this->db->trans_start();
        $this->db->insert('add_program_objective_to_program_landscape', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addLearningModeToProgramLandscape($data)
    {
        $this->db->trans_start();
        $this->db->insert('add_learning_mode_to_program_landscape', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeLandscapeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme_landscape', $data);
        return TRUE;
    }

    function editProgramObjectiveProgrammeLandscapeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('add_program_objective_to_program_landscape', $data);
        return TRUE;
    }

    function deleteCourseFromProgramLandscape($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('add_course_to_program_landscape');
        return TRUE;
    }

    function deletePLLearningMode($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('add_learning_mode_to_program_landscape');
        return TRUE;
    }

    function programmeLandscapeDuplicationCheck($id_programme,$id_intake,$program_scheme)
    {
         // echo "<Pre>";print_r($program_scheme);exit;
        $this->db->select('pl.*');
        $this->db->from('programme_landscape as pl');
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->where('pl.id_intake', $id_intake);
        $this->db->where('pl.program_scheme', $program_scheme);
         $query = $this->db->get();
         
         $result = $query->row();

         // echo "<Pre>";print_r($result);exit;
         if($result)
         {
            return 1;
         }
         else
         {
             return 0;
         }
    }

    function programmeLandscapeCourseDuplicationCheck($id_landscape, $id_course)
    {
        $this->db->select('acpl.*');
        $this->db->from('add_course_to_program_landscape as acpl');
        $this->db->where('acpl.id_program_landscape', $id_landscape);
        $this->db->where('acpl.id_course', $id_course);
         $query = $this->db->get();
         
         $result = $query->row();

         // echo "<Pre>";print_r($result);exit;
         if($result)
         {
            return 1;
         }
         else
         {
             return 0;
         }
    }

    function intakeListForLandscape($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }


    function programMajorList($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('program_has_major_details as ihs');
        $this->db->where('ihs.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }


    function programMinorList($id_program)
    {
        $this->db->select('ihs.*');
        $this->db->from('program_has_minor_details as ihs');
        $this->db->where('ihs.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

     function getCourse($id)
    {
         $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        // echo "<pre>";print_r($query);die;

        return $query->row();
    }

    function getGradeSetupDetailsByCourse($id_course)
    {
        $this->db->select('cr.*');
        $this->db->from('grade_setup as cr');
        $this->db->where('cr.id_course', $id_course);
        $this->db->where('cr.based_on', 'Program & Subject');
        $this->db->where('cr.status', '1');
        $this->db->order_by("cr.id", "DESC");
        $query = $this->db->get();
        $result = $query->row();

        if(!empty($result))
        {
            $id_master = $result->id;

            $this->db->select('cr.*');
            $this->db->from('grade_setup_details as cr');
            $this->db->where('cr.id_grade_setup', $id_master);
            $query = $this->db->get();
            $result = $query->result();

            return $result;
        }
        else
        {
            return array();
        }
    }

    function courseListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('semester as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function getCourseData($id)
    {
        $this->db->select('cr.*, c.name as course_name, c.code as course_code, gsd.description as min_pass_grade');
        $this->db->from('program_landscape_requisite_details as cr');
        $this->db->join('course as c', 'cr.id_course = c.id','left');
        $this->db->join('grade_setup_details as gsd', 'cr.min_pass_grade = gsd.id','left');
        $this->db->where('id_master_course', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    // Not Required
    function getCourseRequisitData($id)
    {
        $this->db->select('cr.*, c.name as course_name, c.code as course_code, gsd.description as min_pass_grade');
        $this->db->from('program_landscape_requisite_details as cr');
        $this->db->join('course as c', 'cr.id_course = c.id','left');
        $this->db->join('grade_setup_details as gsd', 'cr.min_pass_grade = gsd.id','left');
        $this->db->where('id_master_course', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function getCourseRequisitDataForCopy($id)
    {
        $this->db->select('cr.*');
        $this->db->from('program_landscape_requisite_details as cr');
        $this->db->join('course as c', 'cr.id_course = c.id','left');
        $this->db->join('grade_setup_details as gsd', 'cr.min_pass_grade = gsd.id','left');
        $this->db->where('id_master_course', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function addCourseToRequisite($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_landscape_requisite_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteProgramLandscapeRequisiteDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_landscape_requisite_details');
         return TRUE;
    }

    function tempSemesterInfoAdd($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_program_landscape_semester_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempSemesterInfoBySession($id_session)
    {
        $this->db->select('cr.*');
        $this->db->from('temp_program_landscape_semester_details as cr');
        $this->db->where('cr.id_session', $id_session);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function deleteTempSemesterInfo($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_program_landscape_semester_details');
         return TRUE;
    }

    function moveSemesterInfoFromTempToDetails($id_program_landscape)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempSemesterInfoBySession($id_session);

        foreach ($temp_details as $temp_detail)
        {
           unset($temp_detail->id);
           unset($temp_detail->id_session);
           $temp_detail->id_program_landscape = $id_program_landscape;

           $inserted_req = $this->addSemesterDetailsInfo($temp_detail);
        }

        $deleted_temp_req = $this->deleteTempSemesterInfoBySession($id_session);
    }

    function addSemesterDetailsInfo($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_landscape_semester_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempSemesterInfoBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_program_landscape_semester_details');
         return TRUE;
    }

    function programLandscapeHasSemesterList($id_program_landscape)
    {
        $this->db->select('cr.*');
        $this->db->from('program_landscape_semester_details as cr');
        $this->db->where('cr.id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function semesterInfoAdd($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_landscape_semester_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteSemesterInfo($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_landscape_semester_details');
         return TRUE;        
    }

    function landscapeCourseTypeListByStatus($status)
    {
        $this->db->select('cr.*');
        $this->db->from('landscape_course_type as cr');
        $this->db->where('cr.status', $status);
        $query = $this->db->get();
         $result = $query->result();
        return $result;        
    }

    function programLandscapeHasRequirementList($id_program_landscape)
    {
        $this->db->select('cr.*, c.name as course_type_name, c.code as course_type_code');
        $this->db->from('program_landscape_requirement_details as cr');
        $this->db->join('landscape_course_type as c', 'cr.id_landscape_course_type = c.id');
        $this->db->where('id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
         $result = $query->result();
        return $result;        
    }

    function programInfoAdd($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_landscape_requirement_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteProgramInfo($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_landscape_requirement_details');
         return TRUE;   
    }

    function copyFromLandscape($id_old_program_landscape,$id_new_program_landscape)
    {
        $landscape_requirement_list = $this->programLandscapeHasRequirementList($id_old_program_landscape);
        $program_objective_course_list = $this->getProgramObjectiveByLandscapeIdForCopy($id_old_program_landscape);

        // echo "<Pre>";print_r($program_objective_course_list);exit();

        
        $compulsory_course_list = $this->getCompulsoryCourseForCopy($id_old_program_landscape);
        $learning_mode_by_PLID = $this->getLearningModeByPLID($id_old_program_landscape);
        // $major_course_list = $this->getMajorCourse($id_old_program_landscape);
        // $minor_course = $this->getMinorCourse($id_old_program_landscape);
        // $not_compulsory_course = $this->getNotCompulsoryCourse($id_old_program_landscape);



        foreach ($landscape_requirement_list as $requirement)
        {
            unset($requirement->course_type_name);
            unset($requirement->course_type_code);
            unset($requirement->id);
            $requirement->id_program_landscape = $id_new_program_landscape;

            $added_requirement = $this->programInfoAdd($requirement);
        }


        foreach ($program_objective_course_list as $program_objective_course)
        {
            unset($program_objective_course->created_dt_tm);
            unset($program_objective_course->id);
            $program_objective_course->id_program_landscape = $id_new_program_landscape;

            $added_program_objective = $this->addProgramObjectiveToProgramLandscape($program_objective_course);
        }


        foreach ($compulsory_course_list as $compulsary_course)
        {
            $id_compulsary_requisite = $compulsary_course->id;


            unset($compulsary_course->id);
            $compulsary_course->id_program_landscape = $id_new_program_landscape;

            $added_compulsary_course = $this->addCourseToProgramLandscape($compulsary_course);


            $compulsary_course_requisite_list = $this->getCourseRequisitDataForCopy($id_compulsary_requisite);

            foreach ($compulsary_course_requisite_list as $compulsary_course_requisite)
            {
                unset($compulsary_course_requisite->id);
                $compulsary_course_requisite->id_program_landscape = $id_new_program_landscape;
                $compulsary_course_requisite->id_master_course = $added_compulsary_course;

                $added_course_reuisite = $this->addCourseToRequisite($compulsary_course_requisite);
            }


        }


        foreach ($learning_mode_by_PLID as $learning_mode)
        {
            unset($learning_mode->course_name);
            unset($learning_mode->course_code);
            unset($learning_mode->credit_hours);
            unset($learning_mode->programName);
            unset($learning_mode->intake);
            unset($learning_mode->mode_of_study);
            unset($learning_mode->mode_of_program);
            unset($learning_mode->id);
            $learning_mode->id_program_landscape = $id_new_program_landscape;

            $added_learning_mode = $this->addLearningModeToProgramLandscape($learning_mode);
        }

        return TRUE;
    }

    function courseListForLandscapeCourseAdd($name)
    {
        if($name == 'POSTGRADUATE')
        {
            $this->db->select('c.*');
            $this->db->from('course as c');
            $this->db->join('course_type as ct', 'c.id_course_type = ct.id');
            $this->db->where('c.status', 1);
            $this->db->where('ct.name', 'Audit');
            $this->db->order_by("c.name", "ASC");
                    }
        else
        {
            $this->db->select('c.*');
            $this->db->from('course as c');
            $this->db->join('course_type as ct', 'c.id_course_type = ct.id');
            $this->db->where('c.status', 1);
            $this->db->where('ct.name !=', 'Audit');
            $this->db->order_by("c.name", "ASC");
        }

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function getProgramObjectiveByProgramLandscape($id_program_landscape)
    {
        $this->db->select('cr.*, ct.name as course_registration_type_code, ct.code as course_registration_type_name, c.name as course_name, c.code as course_code, c.credit_hours, po.name as objective');
        $this->db->from('add_program_objective_to_program_landscape as cr');
        $this->db->join('course_type as ct', 'cr.id_course_registration_type = ct.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('program_objective as po', 'cr.id_objective = po.id');
        $this->db->where('id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getProgramObjectiveByLandscapeIdForCopy($id_program_landscape)
    {
        $this->db->select('cr.*');
        $this->db->from('add_program_objective_to_program_landscape as cr');
        $this->db->join('course_type as ct', 'cr.id_course_registration_type = ct.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('program_objective as po', 'cr.id_objective = po.id');
        $this->db->where('id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function programmeObjectiveListByProgramId($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_programme', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function courseTypeListByStatus($status)
    {
        $this->db->select('cr.*');
        $this->db->from('course_type as cr');
        $this->db->where('cr.status', $status);
        $query = $this->db->get();
         $result = $query->result();
        return $result;    
    }

    function getProgramObjectiveLandscape($id)
    {
        $this->db->select('cr.*');
        $this->db->from('add_program_objective_to_program_landscape as cr');
        $this->db->where('cr.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        
        return $result;
    }

    function courseListForLandscapeCourses($id_program_landscape)
    {
        $this->db->select('DISTINCT(cr.id_course) as id_course');
        $this->db->from('add_program_objective_to_program_landscape as cr');
        $this->db->join('course_type as ct', 'cr.id_course_registration_type = ct.id');
        $this->db->join('course as c', 'cr.id_course = c.id');
        $this->db->join('program_objective as po', 'cr.id_objective = po.id');
        $this->db->where('id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_course = $result->id_course;
            $course = $this->getCourse($id_course);

            if($course)
            {
                array_push($details, $course);
            }
        }

        return $details;
    }

    function getSubjectRegistrationLandscape($id)
    {
        $this->db->select('cr.*');
        $this->db->from('add_course_to_program_landscape as cr');
        $this->db->where('cr.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        
        return $result;
    }

    function editSubjectRegistrationProgrammeLandscapeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('add_course_to_program_landscape', $data);
        return TRUE;
    }

    function saveCreditHourDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_credit_hour_program_landscape_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempCreditHourDetailsBySession($id_session)
    {
        $this->db->select('cr.*, ct.code as course_type_code, ct.name as course_type_name');
        $this->db->from('temp_credit_hour_program_landscape_details as cr');
        $this->db->join('landscape_course_type as ct', 'cr.id_landscape_course_type = ct.id');
        $this->db->where('cr.id_session', $id_session);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function getTempCreditHourDetailsBySessionForCopy($id_session)
    {
        $this->db->select('cr.*');
        $this->db->from('temp_credit_hour_program_landscape_details as cr');
        $this->db->join('landscape_course_type as ct', 'cr.id_landscape_course_type = ct.id');
        $this->db->where('cr.id_session', $id_session);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function deleteTempCreditHourDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_credit_hour_program_landscape_details');
         return TRUE;
    }

    function moveCourseCreditHourDetailsFromTempToDetails($id_program_landscape)
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->getTempCreditHourDetailsBySessionForCopy($id_session);

        foreach ($temp_details as $temp_detail)
        {
           unset($temp_detail->id);
           unset($temp_detail->id_session);
           $temp_detail->id_program_landscape = $id_program_landscape;

           $inserted_req = $this->addCourseCreditHoursDetails($temp_detail);
        }

        $deleted_temp_req = $this->deleteTempCourseCreditHourDetailsBySession($id_session);
    }

    function addCourseCreditHoursDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('credit_hour_program_landscape_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempCourseCreditHourDetailsBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_credit_hour_program_landscape_details');
         return TRUE;
    }

    function getCreditHourDetailsByProgramLandscapeId($id)
    {
        $this->db->select('cr.*, ct.code as course_type_code, ct.name as course_type_name');
        $this->db->from('credit_hour_program_landscape_details as cr');
        $this->db->join('landscape_course_type as ct', 'cr.id_landscape_course_type = ct.id');
        $this->db->where('cr.id_program_landscape', $id);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }

    function deleteCourseCreditHour($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('credit_hour_program_landscape_details');
         return TRUE; 
    }
}