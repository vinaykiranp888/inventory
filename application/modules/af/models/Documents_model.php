<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Documents_model extends CI_Model
{
    function documentsList()
    {
        $this->db->select('a.*');
        $this->db->from('documents as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function documentsListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('documents as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getDocuments($id)
    {
        $this->db->select('*');
        $this->db->from('documents');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDocuments($data)
    {
        $this->db->trans_start();
        $this->db->insert('documents', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDocuments($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('documents', $data);
        return TRUE;
    }

    function getDocumentsProgramListByDocumentId($id_document)
    {
        $this->db->select('tpe.*, p.code as program_code, p.name as program_name');
        $this->db->from('documents_program_details as tpe');
        $this->db->join('programme as p','tpe.id_program = p.id');
        $this->db->where('tpe.id_document', $id_document);
        $query = $this->db->get();
        return $query->result();   
    }


    function addNewDocumentsProgramDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('documents_program_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function deleteDocumentsProgramDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('documents_program_details');
        return TRUE;
    }

    function programListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('programme as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
}

