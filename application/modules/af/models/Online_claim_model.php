<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Online_claim_model extends CI_Model
{
    function onlineClaimListSearch($data)
    {
        $this->db->select('oc.*, s.name as staff_name, s.ic_no, p.name as programme_name, p.code as programme_code, pu.name as partner_university_name, pu.code as partner_university_code, otc.name as branch_name, otc.code as branch_code, sem.code as semester_code, sem.name as semester_name');
        $this->db->from('online_claim as oc');
        $this->db->join('staff as s','oc.id_staff = s.id');
        $this->db->join('semester as sem','oc.id_semester = sem.id');
        $this->db->join('programme as p','oc.id_programme = p.id');
        $this->db->join('organisation_has_training_center as otc','oc.id_branch = otc.id');
        $this->db->join('partner_university as pu','oc.id_university = pu.id','left');
        // $this->db->where('rd.id_student', $id_student);
        if ($data['name'] != '')
        {
            $likeCriteria = "(s.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['id_programme'] != '')
        {
            $this->db->where('oc.id_programme', $data['id_programme']);
        }
        if($data['id_semester'] != '')
        {
            $this->db->where('oc.id_semester', $data['id_semester']);
        }
        if($data['id_university'] != '')
        {
            $this->db->where('oc.id_university', $data['id_university']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function organisationListByStatus($status)
    {
        $organisation = $this->getOrganisation();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    } 

    function getOrganisation()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function getBranchesByPartnerUniversity($id_organisation)
    {
        $this->db->select('ahemd.*');
        $this->db->from('organisation_has_training_center as ahemd');
        $this->db->where('ahemd.id_organisation', $id_organisation);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function paymentRateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('payment_rate');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewOnlineClaim($data)
    {
        $this->db->trans_start();
        $this->db->insert('online_claim', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getOnlineClaim($id)
    {
        $this->db->select('*');
        $this->db->from('online_claim');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addTempClaimDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_online_claim_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempClaimDetailsBySessionId($id_session)
    {
        $this->db->select('ocd.*, c.name as course_name, c.code as course_code, pr.learning_mode, pr.teaching_component');
        $this->db->from('temp_online_claim_details as ocd');
        $this->db->join('course as c','ocd.id_course = c.id');
        $this->db->join('payment_rate as pr','ocd.id_payment_rate = pr.id');
        $this->db->where('ocd.id_session', $id_session);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getTempClaimDetailsBySessionIdForCopy($id_session)
    {
        $this->db->select('ocd.*');
        $this->db->from('temp_online_claim_details as ocd');
        $this->db->join('course as c','ocd.id_course = c.id');
        $this->db->join('payment_rate as pr','ocd.id_payment_rate = pr.id');
        $this->db->where('ocd.id_session', $id_session);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function deleteTempClaimDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_online_claim_details');
        return $this->db->affected_rows();
    }

    function moveTempToDetails($id_online_claim)
    {
        $id_session = $this->session->my_session_id;
        $temp_details = $this->getTempClaimDetailsBySessionIdForCopy($id_session);

        // echo "<Pre>"; print_r($temp_details);exit;

        foreach ($temp_details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);

            $detail->id_online_claim = $id_online_claim;
        
            // echo "<Pre>"; print_r($detail);exit;

            $this->addClaimDetails($detail);
        }
        
        $deleted = $this->deleteTempClaimDetailsBySessionId($id_session);
    }

    function addClaimDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('online_claim_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempClaimDetailsBySessionId($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_online_claim_details');
        return $this->db->affected_rows();
    }

    function getOnlineClaimDetails($id)
    {
        $this->db->select('ocd.*, c.name as course_name, c.code as course_code, pr.learning_mode, pr.teaching_component');
        $this->db->from('online_claim_details as ocd');
        $this->db->join('course as c','ocd.id_course = c.id');
        $this->db->join('payment_rate as pr','ocd.id_payment_rate = pr.id');
        $this->db->where('ocd.id_online_claim', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }


    

    // function editAcademicYearDetails($data, $id)
    // {
    //     $this->db->where('id', $id);
    //     $this->db->update('academic_year', $data);
    //     return TRUE;
    // }
}