<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Staff_model extends CI_Model
{
    function nationalityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('nationality');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function staffList()
    {
        $this->db->select('s.*');
        $this->db->from('staff as s');
        // $this->db->join('department as d','s.id_department = d.id');
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function staffListSearch($data)
    {
        $this->db->select('s.*, d.code as department_code, d.name as department, ss.name as change_status_name');
        $this->db->from('staff as s');
        $this->db->join('department as d','s.id_department = d.id','left');
        $this->db->join('staff_status as ss','s.status = ss.id','left');
        if (!empty($data['name']))
        {
            $likeCriteria = "(s.name  LIKE '%" . $data['name'] . "%' or s.ic_no  LIKE '%" . $data['name'] . "%' or s.mobile_number  LIKE '%" . $data['name'] . "%' or s.staff_id  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_department']))
        {
            $this->db->where('s.id_department', $data['id_department']);
        }
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function staffChangeStatusListSearch($data)
    {
        
        $this->db->select('scsd.*, s.name as staff_name, s.ic_no as ic_no, s.mobile_number, s.email, d.code as department_code, d.name as department, ss.name as change_status_name');
        $this->db->from('staff_change_status_details as scsd');
        $this->db->join('staff_status as ss','scsd.id_change_status = ss.id');
        $this->db->join('staff as s','scsd.id_staff = s.id');
        $this->db->join('department as d','s.id_department = d.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(s.name  LIKE '%" . $data['name'] . "%' or s.ic_no  LIKE '%" . $data['name'] . "%' or s.mobile_number  LIKE '%" . $data['name'] . "%' or s.staff_id  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_department']))
        {
            $this->db->where('s.id_department', $data['id_department']);
        }
        $this->db->order_by("scsd.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();
         // echo "<Pre>";print_r($result);exit();     
         return $result;
    }

    function getStaff($id)
    {
        $this->db->select('s.*, d.name as department');
        $this->db->from('staff as s');
        $this->db->join('department as d','s.id_department = d.id','left');
        $this->db->where('s.id', $id);
        $this->db->order_by("s.name", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getDepartmentByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }


    function getSpecializationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('specialization_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function bankListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('bank_registration');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();        
    }

    function getFacultyProgramListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('faculty_program');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getCountryByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function checkStaffDuplication($data)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('mobile_number', $data['mobile_number']);
        $this->db->or_where('ic_no', $data['ic_no']);
        $this->db->or_where('staff_id', $data['staff_id']);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewStaff($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStaff($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('staff', $countryInfo);
        return $this->db->affected_rows();
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_staff_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_staff_has_course', $data);
        return TRUE;
    }

    function getTempStaff($id_session)
    {
        $this->db->select('a.*, c.name');
        $this->db->from('temp_staff_has_course as a');
        $this->db->join('course as c', 'a.id_course = c.id');        
        $this->db->where('id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffCourse($id)
    {
        $this->db->select('a.*, c.name as coursename');
        $this->db->from('staff_has_course as a');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->where('a.id_staff', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffEducationQualificationDetails($id)
    {
        $this->db->select('a.*');
        $this->db->from('staff_education_qualification as a');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStaffEducationQualificationList($id_staff)
    {
        $this->db->select('a.*, c.name as country');
        $this->db->from('staff_education_qualification as a');    
        $this->db->join('country as c', 'a.country = c.id');
        $this->db->where('a.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffWorkExperienceList($id_staff)
    {
        $this->db->select('a.*');
        $this->db->from('staff_work_experience as a');  
        $this->db->where('a.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffWorkExperienceDetails($id)
    {
        $this->db->select('a.*');
        $this->db->from('staff_work_experience as a');  
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStaffSpecializationList($id_staff)
    {
        $this->db->select('a.*, sc.code as specialization_code, sc.name as specialization_name');
        $this->db->from('staff_specialization as a');  
        $this->db->join('specialization_setup as sc', 'a.id_specialization = sc.id');
        $this->db->where('a.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffSpecializationDetails($id)
    {
        $this->db->select('a.*');
        $this->db->from('staff_specialization as a');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStaffHasProgramme($id_staff)
    {
        $this->db->select('a.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('staff_has_programme as a');  
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->where('a.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('temp_staff_has_course');
       return TRUE;
    }

    function deleteCourseData($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('staff_has_course');
       return TRUE;
    }

    function deleteTempDataBySession($id_session)
    { 
       $this->db->where('id_session', $id_session);
       $this->db->delete('temp_staff_has_course');
       return TRUE;
    }

    function addNewStaffCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function salutationListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('a.*');
        $this->db->from('salutation_setup as a');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function modeOfStudyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme_has_scheme');
        // $this->db->where('status', $status);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function learningCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('organisation_has_training_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function addStaffTeachingDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_teaching_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function changeStatusListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff_status');
        $this->db->where('status', $status);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffTeachingDetails($id_staff)
    {
        $this->db->select('stad.*, s.code as semester_code, s.name as semester_name, p.code as program_code, p.name as program_name, c.code as course_code, c.name as course_name, phs.mode_of_study, phs.mode_of_program, o.name as organisation_name, o.code as organisation_code, u.name as user');
        $this->db->from('staff_teaching_details as stad');
        $this->db->join('semester as s', 'stad.id_semester = s.id');        
        $this->db->join('programme as p', 'stad.id_programme = p.id');        
        $this->db->join('course as c', 'stad.id_course = c.id');        
        $this->db->join('programme_has_scheme as phs', 'stad.id_mode_of_study = phs.id');        
        $this->db->join('organisation_has_training_center as o', 'stad.id_learning_center = o.id');        
        $this->db->join('users as u', 'stad.created_by = u.id');
        $this->db->where('stad.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTeachingDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_teaching_details');
        return TRUE;
    }

    function addStaffChangeStatus($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_change_status_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getStaffChangeStatusDetails($id_staff)
    {
        $this->db->select('stad.*, s.name as change_status, u.name as user');
        $this->db->from('staff_change_status_details as stad');
        $this->db->join('staff_status as s', 'stad.id_change_status = s.id');
        $this->db->join('users as u', 'stad.created_by = u.id');
        $this->db->where('stad.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteStaffChangeStatus($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_change_status_details');
        return TRUE;
    }

    function addStaffLeaveDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_leave_records', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getStaffLeaveDetails($id_staff)
    {
        $this->db->select('stad.*, u.name as user');
        $this->db->from('staff_leave_records as stad');
        $this->db->join('users as u', 'stad.created_by = u.id');
        $this->db->where('stad.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteStaffLeaveDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_leave_records');
        return TRUE;
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function addStaffBankDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_bank_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateStaffBankDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff_bank_details', $data);
        return TRUE;
    }

    function getStaffBankList($id_staff)
    {
        $this->db->select('stad.*, br.code as bank_registration_code, br.name as bank_registration_name, u.name as user');
        $this->db->from('staff_bank_details as stad');
        $this->db->join('bank_registration as br', 'stad.id_bank = br.id');
        $this->db->join('users as u', 'stad.created_by = u.id');
        $this->db->where('stad.id_staff', $id_staff);
        $query = $this->db->get();
        return $query->result();
    }

    function getStaffBankDetails($id)
    {
        $this->db->select('stad.*');
        $this->db->from('staff_bank_details as stad');
        $this->db->where('stad.id', $id);
        $query = $this->db->get();
        $result = $query->row();
         return $result;
    }

    function deleteStaffBankDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_bank_details');
        return TRUE;
    }

    function getStaffChangeStatus($id)
    {
        $this->db->select('ihs.*');
        $this->db->from('staff_change_status_details as ihs');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewStaffEducatinQualification($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_education_qualification', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateStaffEducatinQualification($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff_education_qualification', $data);
        return TRUE;
    }

    function addNewStaffWorkExperience($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_work_experience', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateStaffWorkExperience($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff_work_experience', $data);
        return TRUE;
    }


    function addNewStaffSpecialization($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_specialization', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateStaffSpecialization($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('staff_specialization', $data);
        return TRUE;
    }

    function addNewStaffHasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('staff_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteEducationQualificationDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_education_qualification');
        return TRUE;
    }

    function deleteWorkExperienceDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_work_experience');
        return TRUE;
    }

    function deleteSpecializationDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_specialization');
        return TRUE;
    }

    function deleteStaffHasProgrammeDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('staff_has_programme');
        return TRUE;
    }
}