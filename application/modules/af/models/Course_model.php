<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_model extends CI_Model
{
        
    function courseList()
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        // $this->db->join('department as d', 'c.id_department = d.id');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseDescriptionListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course_description_setup as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseTypeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course_type as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function facultyProgramListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('faculty_program as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function departmentListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('department as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        // $this->db->join('department as d', 'c.id_department = d.id');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_in_malay  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_staff'] != '')
        // {
        //     $this->db->where('c.id_staff_coordinator', $data['id_staff']);
        // }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getCourse($id)
    {
         $this->db->select('c.*');
        $this->db->from('course as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCourse($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course', $data);

        return TRUE;
    }

    function deleteState($id, $courseInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('course', $courseInfo);

        return $this->db->affected_rows();
    }
}
