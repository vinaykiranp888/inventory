<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View AF Online Claim</h3>
        </div>
        <form id="form_data" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">AF Online Claim Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control" disabled>
                            <option value="">Select</option>
                            <option value="Academic Facilitator" selected>Academic Facilitator</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Academic Facilitator <span class='error-text'>*</span></label>
                       <select name="id_staff" id="id_staff" class="form-control selitemIcon" disabled>
                          <option value="">Select</option>
                          <?php
                             if (!empty($staffList))
                             {
                                 foreach ($staffList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>"

                            <?php
                            if($record->id == $onlineClaim->id_staff)
                            {
                                echo "selected";
                            }
                            ?>
                            >
                             <?php echo $record->ic_no . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                 </div>


                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Partner University <span class='error-text'>*</span></label>
                       <select name="id_university" id="id_university" class="form-control selitemIcon"  onchange="getBranchesByPartnerUniversity(this.value)" disabled>
                          <option value="">Select</option>
                          <?php
                             if (!empty($organisationList))
                             {
                                 foreach ($organisationList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>"
                            <?php
                            if($record->id == $onlineClaim->id_university)
                            {
                                echo "selected";
                            }
                            ?>
                            >
                             <?php echo $record->code . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                </div> 


              </div>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Learning Center <span class='error-text'>*</span></label>
                       <span id="view_branch">
                           <select class="form-control" id='id_branch' name='id_branch'>
                              <option value=''></option>
                            </select>
                       </span>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Programme <span class='error-text'>*</span></label>
                       <select name="id_programme" id="id_programme" class="form-control selitemIcon" disabled>
                          <option value="">Select</option>
                          <?php
                             if (!empty($programmeList))
                             {
                                 foreach ($programmeList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>"
                            <?php
                            if($record->id == $onlineClaim->id_programme)
                            {
                                echo "selected";
                            }
                            ?>>

                             <?php echo $record->code . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                       <label>Semester <span class='error-text'>*</span></label>
                       <select name="id_semester" id="id_semester" class="form-control selitemIcon" disabled>
                          <option value="">Select</option>
                          <?php
                             if (!empty($semesterList))
                             {
                                 foreach ($semesterList as $record)
                                 {?>
                          <option value="<?php echo $record->id;  ?>"
                            <?php
                            if($record->id == $onlineClaim->id_semester)
                            {
                                echo "selected";
                            }
                            ?>
                            >
                             <?php echo $record->code . " - " . $record->name;?>
                          </option>
                          <?php
                             }
                             }
                             ?>
                       </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="total_amount" name="total_amount" value="<?php echo $onlineClaim->total_amount; ?>" readonly>
                    </div>
                </div>
              

            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>

        
        <br>


        <?php

        if(!empty($onlineClaimDetails))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Online Claim Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Course</th>
                            <th>Date</th>
                            <th>Hours</th>
                            <th>Payment Rate</th>
                            <th>Amount</th>
                            <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($onlineClaimDetails);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $onlineClaimDetails[$i]->course_code . " - " . $onlineClaimDetails[$i]->course_name;?></td>
                            <td><?php
                            if($onlineClaimDetails[$i]->date_time)
                            {
                             echo date('d-m-Y', strtotime($onlineClaimDetails[$i]->date_time));
                            }
                             ?></td>
                            <td><?php echo $onlineClaimDetails[$i]->hours;?></td>
                            <td><?php echo $onlineClaimDetails[$i]->learning_mode . " - " . $onlineClaimDetails[$i]->teaching_component;?></td>
                            <td><?php echo $onlineClaimDetails[$i]->amount;?></td>
                            <!-- <td>
                            <a onclick="deleteSchemeDetails(<?php echo $onlineClaimDetails[$i]->id; ?>)">Delete</a>
                            </td> -->

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>





        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

     $('select').select2();

     function getBranchesByPartnerUniversity(id)
     {
        $.get("/af/onlineClaim/getBranchesByPartnerUniversity/"+id, function(data, status)
        {
          $("#view_branch").html(data);
          $("#view_branch").show();
        });
     }


    $(document).ready(function()
    {
         var id_university = "<?php echo $onlineClaim->id_university; ?>";

         if(id_university != '' && id_university != 0)
         {
            $.get("/af/onlineClaim/getBranchesByPartnerUniversityDisabled/"+id_university, function(data, status)
            {
              $("#view_branch").html(data);
              $("#view_branch").show();

              var id_branch = "<?php echo $onlineClaim->id_branch; ?>";

              $("#id_branch").find('option[value="'+id_branch+'"]').attr('selected',true);
              $('select').select2();
                
            });
         }


        $("#form_data").validate(
        {
            rules:
            {
                type:
                {
                    required: true
                },
                id_staff:
                {
                    required: true
                },
                id_university:
                {
                    required: true
                },
                id_branch:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                id_semester:
                {
                    required: true
                },
                total_amount:
                {
                    required: true
                }
            },
            messages:
            {
                type:
                {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_staff:
                {
                    required: "<p class='error-text'>Select Academic Facilitator</p>",
                },
                id_university:
                {
                    required: "<p class='error-text'>Select Organisation</p>",
                },
                id_branch:
                {
                    required: "<p class='error-text'>Select Learning Center</p>",
                },
                id_programme:
                {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_semester:
                {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                total_amount:
                {
                    required: "<p class='error-text'>Add Details For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>