<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Online Claim</h3>
      <a href="add" class="btn btn-primary">+ Add Online Claim</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">AF Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParameters['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Organisation</label>
                        <div class="col-sm-8">
                          <select name="id_university" id="id_university" class="form-control selitemIcon">
                              <option value="">Select</option>
                              <?php
                              if (!empty($organisationList))
                              {
                                  foreach ($organisationList as $record)
                                  {?>
                                      <option value="<?php echo $record->id; ?>"
                                          <?php 
                                          if($record->id == $searchParameters['id_university'])
                                          {
                                              echo "selected=selected";
                                          } ?>>
                                          <?php echo $record->code . " - " . $record->name;  ?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                    </div>
                  </div>

                  

                </div>



                <div class="row">



                  <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Programme</label>
                        <div class="col-sm-8">
                          <select name="id_programme" id="id_programme" class="form-control selitemIcon">
                              <option value="">Select</option>
                              <?php
                              if (!empty($programmeList))
                              {
                                  foreach ($programmeList as $record)
                                  {?>
                                      <option value="<?php echo $record->id; ?>"
                                          <?php 
                                          if($record->id == $searchParameters['id_programme'])
                                          {
                                              echo "selected=selected";
                                          } ?>>
                                          <?php echo $record->code . " - " . $record->name;  ?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                    </div>
                  </div>


                  



                  <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Semester</label>
                        <div class="col-sm-8">
                          <select name="id_semester" id="id_semester" class="form-control selitemIcon">
                              <option value="">Select</option>
                              <?php
                              if (!empty($semesterList))
                              {
                                  foreach ($semesterList as $record)
                                  {?>
                                      <option value="<?php echo $record->id; ?>"
                                          <?php 
                                          if($record->id == $searchParameters['id_semester'])
                                          {
                                              echo "selected=selected";
                                          } ?>>
                                          <?php echo $record->code . " - " . $record->name;  ?>
                                      </option>
                              <?php
                                  }
                              }
                              ?>
                          </select>
                      </div>
                    </div>
                  </div>

                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Academic Facilitator</th>
            <th>Programme</th>
            <th>Organisation</th>
            <th>Learning Center</th>
            <th>Semester</th>
            <th>Total Amount</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($onlineClaimList)) {
            $i=1;
            foreach ($onlineClaimList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->ic_no . " - " . $record->staff_name ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php
                if($record->id_university == 1)
                {
                  echo $organisation->code . " - " . $organisation->name;
                }
                else
                {
                 echo $record->partner_university_code . " - " . $record->partner_university_name;
                }
                  ?>   
                </td>
                <td><?php echo $record->branch_code . " - " . $record->branch_name ?></td>
                <td><?php echo $record->semester_code . " - " . $record->semester_name ?></td>
                <td><?php echo $record->total_amount ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="View Online Claim">View</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
  $('select').select2();
  
  function clearSearchForm()
  {
    window.location.reload();
  }
</script>