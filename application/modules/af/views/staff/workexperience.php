
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">

            <li><a href="/af/staff/edit/<?php echo $id_staff;?>">Faculty Details</a></li>
            <li><a href="/af/staff/bankaccount/<?php echo $id_staff;?>">Bank Account Details</a></li>
            <li><a href="/af/staff/qualification/<?php echo $id_staff;?>">Qualification Details</a></li>
            <li class="active"><a href="/af/staff/workexperience/<?php echo $id_staff;?>">Work Experience Details</a></li>
            <li><a href="/af/staff/specialization/<?php echo $id_staff;?>">Specialization Details</a></li>

        </ul>

        <div role="tabpanel" class="tab-pane" id="program_accerdation">
            <div class="mt-4">


            

                <form id="form_experience" action="" method="post" enctype="multipart/form-data">



                    <div class="form-container">
                            <h4 class="form-group-title">Work Experience Details</h4>

                            <div class="row">

                                <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="8">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Organisation Name <span class='error-text'>*</span> </label>
                                        <input type="text" class="form-control" id="organisation_name" name="organisation_name" value="<?php echo $workExperience->organisation_name; ?>">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Designation <span class='error-text'>*</span> </label>
                                        <input type="text" class="form-control" id="designation" name="designation" value="<?php echo $workExperience->designation; ?>">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Level <span class='error-text'>*</span> </label>
                                        <input type="text" class="form-control" id="level" name="level" value="<?php echo $workExperience->level; ?>">
                                    </div>
                                </div>


                            </div>

                            <div class="row">


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Start Date <span class='error-text'>*</span> </label>
                                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php if($workExperience->start_date){ echo date('d-m-Y', strtotime($workExperience->start_date)); } ?>">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>End Date <span class='error-text'>*</span> </label>
                                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php if($workexperience->end_date){ echo date('d-m-Y', strtotime($workExperience->end_date)); } ?>">
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Upload Employment Letter
                                        <?php
                                        if ($workExperience->employment_letter != '')
                                        {
                                        ?>
                                        <a href="<?php echo '/assets/images/' . $workExperience->employment_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $workExperience->employment_letter; ?>)" title="<?php echo $workExperience->employment_letter; ?>"> View </a>
                                        <?php
                                        }
                                        ?>
                                        </label>
                                        <input type="file" class="form-control" id="employment_letter" name="employment_letter" value="<?php if($workExperience){ echo $workExperience->employment_letter; } ?>">
                                    </div>
                                </div>

                                
                            </div>



                    </div>
                            
                        


                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                            <button type="button" onclick="saveWorkExperienceDetails()" class="btn btn-primary btn-lg">Save</button>
                            <?php
                            if($id_workexperience != NULL)
                            {
                              ?>
                              <a href="<?php echo '../../workexperience/'. $id_staff ?>" class="btn btn-link">Cancel</a>
                              <?php
                            }
                            ?>
                        </div>
                    </div>


                

                </form>



                <div class="form-container">
                        <h4 class="form-group-title">Work Experience Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Organisation Name</th>
                            <th>Designation</th>
                            <th>Level</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Employment Letter</th>
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($staffWorkExperienceList))
                          {
                            $i=1;
                            foreach ($staffWorkExperienceList as $record) {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->organisation_name ?></td>
                                <td><?php echo $record->designation ?></td>
                                <td><?php echo $record->level ?></td>
                                <td><?php 
                                if($record->start_date)
                                {
                                 echo date('d-m-Y', strtotime($record->start_date));
                                }
                                 ?></td>
                                <td><?php
                                if($record->end_date)
                                {
                                 echo date('d-m-Y', strtotime($record->end_date));
                                }
                                 ?></td>
                                <td class="text-center">
                                    <?php 
                                    if($record->employment_letter)
                                    {
                                        ?>


                                    <a href="<?php echo '/assets/images/' . $record->employment_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->employment_letter; ?>)" title="<?php echo $record->employment_letter; ?>">View</a>
                                    <?php
                                    }
                                    else
                                    {
                                        echo 'No File';
                                    }
                                    ?>
                                </td>
                                <td class="text-center">
                                    <a href='/af/staff/workexperience/<?php echo $id_staff;?>/<?php echo $record->id;?>'>Edit</a> |
                                    <a onclick="deleteWorkExperienceDetails(<?php echo $record->id; ?>)">Delete</a>
                                </td>
                                </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>








          </div>
      
      </div>




         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

    $('select').select2();

    $(function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });



    function saveWorkExperienceDetails()
    {
        if($('#form_experience').valid())
        {
            $('#form_experience').submit();    
        }
    }




    function deleteWorkExperienceDetails(id)
    {
       var cnf= confirm('Do you really want to delete?');
       if(cnf==true)
       {
       
        $.ajax(
            {
               url: '/af/staff/deleteWorkExperienceDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
        }
    }



    $(document).ready(function()
    {
        $("#form_experience").validate(
        {
            rules:
            {
                organisation_name:
                {
                    required: true
                },
                designation:
                {
                    required: true
                },
                level:
                {
                    required: true
                },
                start_date:
                {
                    required: true
                }
            },
            messages:
            {
                organisation_name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                designation:
                {
                    required: "<p class='error-text'>Designatin Required</p>",
                },
                level:
                {
                    required: "<p class='error-text'>Level Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Select Start Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>