
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">


        <ul class="page-nav-links">
            <li class="active"><a href="/af/staff/edit/<?php echo $id_staff;?>">Faculty Details</a></li>
            <li><a href="/af/staff/bankaccount/<?php echo $id_staff;?>">Bank Account Details</a></li>
            <li><a href="/af/staff/qualification/<?php echo $id_staff;?>">Qualification Details</a></li>
            <li><a href="/af/staff/workexperience/<?php echo $id_staff;?>">Work Experience Details</a></li>
            <li><a href="/af/staff/specialization/<?php echo $id_staff;?>">Specialization Details</a></li>
        </ul>

      <form id="form_staff" action="" method="post">

                            <div class="form-container">
                                <h4 class="form-group-title">Faculty Details</h4>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Salutation <span class='error-text'>*</span></label>
                                             <select name="salutation" id="salutation" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($salutationList)) {
                                                    foreach ($salutationList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php if($staffDetails->salutation==$record->id)
                                                            {
                                                                echo "selected=selected";
                                                            }
                                                            ?>
                                                            >
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>First Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $staffDetails->first_name;?>">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Last Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $staffDetails->last_name;?>">
                                        </div>
                                    </div>

                                      
                                    
                                </div>


                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <label>ID Type <span class='error-text'>*</span></label>
                                           <select name="id_type" id="id_type" class="form-control" required onchange="getlabel()">
                                            <option value="">Select</option>
                                              <option value="NRIC" <?php if($staffDetails->id_type=='NRIC') { echo "selected=selected";}?>>NRIC</option>
                                              <option value="PASSPORT" <?php if($staffDetails->id_type=='PASSPORT') { echo "selected=selected";}?>>PASSPORT</option>
                                          </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                         <div class="form-group">
                                            <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="<?php echo $staffDetails->ic_no ?>">
                                         </div>
                                    </div>



                                    <!-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>IC No. <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="ic_no" name="ic_no" value="<?php echo $staffDetails->ic_no;?>">
                                        </div>
                                    </div> -->

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Gender <span class='error-text'>*</span></label>
                                             <select name="gender" id="gender" class="form-control">
                                                <option value="Male" <?php if($staffDetails->gender=='Male')
                                                {
                                                    echo "selected=selected";
                                                }
                                                ?>
                                                >Male</option>
                                                <option value="Female" <?php if($staffDetails->gender=='Female')
                                                {
                                                    echo "selected=selected";
                                                }?>
                                                >Female</option>
                                            </select>
                                        </div>
                                    </div>

                                
                                </div>

                                <div class="row"> 



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mobile  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="mobile_number" name="mobile_number" value="<?php echo $staffDetails->mobile_number;?>">
                                        </div>
                                    </div> 
                                   


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" value="<?php echo $staffDetails->email;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty ID <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="staff_id" name="staff_id" value="<?php echo $staffDetails->staff_id;?>">
                                        </div>
                                    </div> 


                                    

                                </div>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Faculty Type <span class='error-text'>*</span></label>
                                             <select name="job_type" id="job_type" class="form-control">
                                                <option value="">Select</option>
                                                <option value="0" <?php if($staffDetails->job_type=='0') { echo "selected=selected";}?>>Part Time</option>
                                                <option value="1" <?php if($staffDetails->job_type=='1') { echo "selected=selected";}?>>Full TIme</option>
                                            </select>
                                        </div>
                                    </div>  


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>DOB <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="dob" name="dob" value="<?php echo date('d-m-Y',strtotime($staffDetails->dob));?>">
                                        </div>
                                    </div>
                                    
                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Academic Type <span class='error-text'>*</span></label>

                                             <select name="academic_type" id="academic_type" class="form-control">
                                                <option value="1" <?php if($staffDetails->academic_type=='1') { echo "selected=selected";}?>>Academic Faculty</option>
                                                <option value="0" <?php if($staffDetails->academic_type=='0') { echo "selected=selected";}?>>Non-Academic Faculty</option>
                                            </select>
                                        </div>
                                    </div>



                                </div>



                                <div class="row">



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Joined Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" autocomplete="off" value="<?php
                                            if($staffDetails->joined_date)
                                            {
                                                echo date('d-m-Y',strtotime($staffDetails->joined_date));
                                            }
                                            ?>">
                                        </div>
                                    </div> 




                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Nationality <span class='error-text'>*</span></label>
                                            <select name="nationality" id="nationality" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($nationalityList)) {
                                                foreach ($nationalityList as $record) {
                                            ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->nationality)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>
                                                    >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                      <div class="form-group">
                                         <label>FILE 
                                         <span class='error-text'>*</span>
                                         <?php
                                            if ($staffDetails->image != '')
                                            {
                                            ?>
                                         <a href="<?php echo '/assets/images/' . $staffDetails->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $staffDetails->image; ?>)" title="<?php echo $staffDetails->image; ?>"> View </a>
                                         <?php
                                            }
                                            ?>
                                         </label>
                                         <input type="file" name="image" id="image">
                                      </div>
                                    </div>




                                </div>


                                <div class="row">


                                    <div class="col-sm-4">
                                            <div class="form-group">
                                                <p>Status <span class='error-text'>*</span></p>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="1" <?php if($staffDetails->status=='1') {
                                                     echo "checked=checked";
                                                  };?>><span class="check-radio"></span> Active
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="status" id="status" value="0" <?php if($staffDetails->status=='0') {
                                                     echo "checked=checked";
                                                  };?>>
                                                  <span class="check-radio"></span> In-Active
                                                </label>                              
                                            </div>                         
                                    </div>
                                    
                                </div>


                            </div>

                            <div class="form-container">
                                <h4 class="form-group-title">Contact Details</h4>

                                <div class="row">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Phone  Number <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="phone_number" name="phone_number" value="<?php echo $staffDetails->phone_number;?>">
                                        </div>
                                    </div>     

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address" name="address" value="<?php echo $staffDetails->address;?>">
                                        </div>
                                    </div>    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="address_two" name="address_two" value="<?php echo $staffDetails->address_two;?>">
                                        </div>
                                    </div> 

                                    
                                </div>  

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Select Country <span class='error-text'>*</span></label>
                                            <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                 <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $staffDetails->id_country)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?></option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                     <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select State <span class='error-text'>*</span></label>
                                                <span id='view_state'>
                                                    <select name='id_state' id='id_state' class='form-control'>
                                                        <option value=''></option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $staffDetails->zipcode;?>">
                                            <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="1">
                                        </div>
                                    </div>

                                      
                                </div>


                                

                            </div>




                            <div class="form-container">
                             <h4 class="form-group-title">Other Details</h4>
                             <div class="row">
                                <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Whatsapp Number <span class='error-text'>*</span></label>
                                      <input type="number" class="form-control" id="whatsapp_number" name="whatsapp_number" value="<?php echo $staffDetails->whatsapp_number; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>LinkedIn ID/Link: <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="linked_in" name="linked_in" value="<?php echo $staffDetails->linked_in; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Facebook ID/ Link <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="facebook_id" name="facebook_id" value="<?php echo $staffDetails->facebook_id; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Twitter ID/Link: <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="twitter_id" name="twitter_id" value="<?php echo $staffDetails->twitter_id; ?>">
                                   </div>
                                </div>

                                 <div class="col-sm-4">
                                  <div class="form-group">
                                      <label>Instagram ID/ Link <span class='error-text'></span></label>
                                      <input type="text" class="form-control" id="ig_id" name="ig_id" value="<?php echo $staffDetails->ig_id; ?>">
                                   </div>
                                </div>

                              </div>
                            
                           </div>








                            <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                        <a href="../list" class="btn btn-link">Cancel</a>
                                    </div>
                            </div>


                            




                        </form>


</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
</div>


<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script type="text/javascript">
  
  $('select').select2();

   $(function()
   {
       showPartner();
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
   });

    getlabel();
    getStateFill();

   function getlabel()
    {
      var labelnric = $("#id_type").val();
      //alert(labelnric);
      $("#labelspanid").html(labelnric);
    }
   
   
   function showPartner(){
       var value = $("#internal_external").val();
       if(value=='Internal') {
            $("#partnerdropdown").hide();
   
       } else if(value=='External') {
            $("#partnerdropdown").show();
   
       }
   }


   function getStateByCountry(id)
    {
     $.get("/af/staff/getStateByCountry/"+id, function(data, status)
     {
        $("#view_state").html(data);
     });
    }
    

    function getStateFill()
    {
        // alert('dasd');

        $.get("/af/staff/getStateByCountry/"+<?php echo $staffDetails->id_country;?>, function(data, status)
        {
            var idstateselected = "<?php echo $staffDetails->id_state;?>";

            $("#view_state").html(data);
            $("#id_state").find('option[value="'+idstateselected+'"]').attr('selected',true);
            $('select').select2();
        });
    }


   $(document).ready(function()
    {

        $("#form_staff").validate(
        {
            rules:
            {
                salutation:
                {
                    required: true
                },
                first_name:
                {
                    required: true
                },
                last_name:
                {
                    required: true
                },
                ic_no:
                {
                    required: true
                },
                gender:
                {
                    required: true
                },
                mobile_number:
                {
                    required: true
                },
                phone_number:
                {
                    required: true
                },
                address:
                {
                    required: true
                },
                address_two:
                {
                    required: true
                },
                id_country:
                {
                    required: true
                },
                id_state:
                {
                    required: true
                },
                zipcode:
                {
                    required: true
                },
                job_type:
                {
                    required: true
                },
                email:
                {
                    required: true
                },
                staff_id:
                {
                    required: true
                },
                dob:
                {
                    required: true
                },
                academic_type:
                {
                    required: true
                },
                id_faculty_program:
                {
                    required: true
                },
                id_education_level:
                {
                    required: true
                },
                id_type:
                {
                    required: true
                },
                status:
                {
                    required: true
                },
                nationality:
                {
                    required: true
                },
                whatsapp_number : {
                    required: true
                },
                joined_date: {
                    required: true
                }
            },
            messages:
            {
                salutation:
                {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name:
                {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name:
                {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                ic_no:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                gender:
                {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                mobile_number:
                {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                phone_number:
                {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                address:
                {
                    required: "<p class='error-text'>Faculty Address Required</p>",
                },
                address_two:
                {
                    required: "<p class='error-text'>Address 2 Required</p>",
                },
                id_country:
                {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state:
                {
                    required: "<p class='error-text'>Select State</p>",
                },
                zipcode:
                {
                    required: "<p class='error-text'>Enter Zipcode</p>",
                },
                job_type:
                {
                    required: "<p class='error-text'>Select Job Type</p>",
                },
                email:
                {
                    required: "<p class='error-text'>Email Required</p>",
                },
                staff_id:
                {
                    required: "<p class='error-text'>Enter Faculty ID</p>",
                },
                dob:
                {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                academic_type:
                {
                    required: "<p class='error-text'>Select Academic Type</p>",
                },
                id_faculty_program:
                {
                    required: "<p class='error-text'>Select Faculty Program</p>",
                },
                id_education_level:
                {
                    required: "<p class='error-text'>Select Highest Education Level</p>",
                },
                id_type:
                {
                    required: "<p class='error-text'>Select ID Type</p>",
                },
                status:
                {
                    required: "<p class='error-text'>Select Status</p>",
                },
                nationality:
                {
                    required: "<p class='error-text'>Select Nationality</p>",
                },
                whatsapp_number:
                {
                    required: "<p class='error-text'>WhatsApp No. Required</p>",
                },
                joined_date:
                {
                    required: "<p class='error-text'>Select Joined Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
   

</script>

<style type="text/css">
   .shadow-textarea textarea.form-control::placeholder {
   font-weight: 300;
   }
   .shadow-textarea textarea.form-control {
   padding-left: 0.8rem;
   }
</style>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
