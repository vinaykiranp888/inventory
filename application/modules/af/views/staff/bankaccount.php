
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">

            <li><a href="/af/staff/edit/<?php echo $id_staff;?>">Faculty Details</a></li>
            <li class="active"><a href="/af/staff/bankaccount/<?php echo $id_staff;?>">Bank Account Details</a></li>
            <li><a href="/af/staff/qualification/<?php echo $id_staff;?>">Qualification Details</a></li>
            <li><a href="/af/staff/workexperience/<?php echo $id_staff;?>">Work Experience Details</a></li>
            <li><a href="/af/staff/specialization/<?php echo $id_staff;?>">Specialization Details</a></li>

        </ul>

         <div role="tabpanel" class="tab-pane" id="program_accerdation">
                        <div class="mt-4">


          <form id="form_bank_details" action="" method="post">


              <br>

              <div class="form-container">
                  <h4 class="form-group-title">Bank Account Details</h4>

                  <div class="row">


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Bank Name <span class='error-text'>*</span></label>
                               <select name="id_bank" id="id_bank" class="form-control">
                                  <option value="">Select</option>
                                  <?php
                                  if (!empty($bankList)) {
                                      foreach ($bankList as $record) {
                                  ?>
                                          <option value="<?php echo $record->id;  ?>"
                                            <?php
                                            if($staffBank->id_bank == $record->id)
                                            {
                                              echo 'selected';
                                            }
                                            ?>>
                                              <?php echo $record->code . " - " . $record->name;  ?>        
                                          </option>
                                  <?php
                                      }
                                  }
                                  ?>
                              </select>
                          </div>
                      </div>




                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Account Name <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="bank_account_name" name="bank_account_name"  value="<?php echo $staffBank->bank_account_name; ?>">
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Account Number <span class='error-text'>*</span></label>
                              <input type="number" class="form-control" id="bank_account_number" name="bank_account_number" value="<?php echo $staffBank->bank_account_number ?>">
                          </div>
                      </div>



                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Bank Code <span class='error-text'>*</span></label>
                              <input type="text" class="form-control" id="bank_code" name="bank_code" value="<?php echo $staffBank->bank_code ?>">
                          </div>
                      </div>


                      <div class="col-sm-4">
                          <div class="form-group">
                              <label>Bank Address </label>
                              <input type="text" class="form-control" id="bank_address" name="bank_address" value="<?php echo $staffBank->bank_address ?>">
                              <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="6">
                          </div>
                      </div>



                  </div>

              </div>


              <div class="button-block clearfix">
                  <div class="bttn-group">
                          <button type="button" onclick="saveBankDetails()" class="btn btn-primary btn-lg">Save</button>
                          <?php
                        if($id_bank_account != NULL)
                        {
                          ?>
                          <a href="<?php echo '../../bankaccount/'. $id_staff ?>" class="btn btn-link">Cancel</a>
                          <?php
                        }
                        ?>
                  </div>
              </div>

                


          </form>



              <?php

              if(!empty($getStaffBankList))
              {
                  ?>
                  <br>

                  <div class="form-container">
                          <h4 class="form-group-title">Staff Change Status Details</h4>

                      

                        <div class="custom-table">
                          <table class="table">
                              <thead>
                                  <tr>
                                  <th>Sl. No</th>
                                   <th>Bank</th>
                                   <th>Account Name</th>
                                   <th>Account Number</th>
                                   <th>Bank Code</th>
                                   <th>Address</th>
                                   <th>Created On</th>
                                   <th>Created By</th>
                                   <th>Action</th>
                                  </tr>
                              </thead>
                              <tbody>
                                   <?php
                               $total = 0;
                                for($i=0;$i<count($getStaffBankList);$i++)
                               { ?>
                                  <tr>
                                  <td><?php echo $i+1;?></td>
                                  <td><?php echo $getStaffBankList[$i]->bank_registration_code . " - " . $getStaffBankList[$i]->bank_registration_name;?></td>
                                  <td><?php echo $getStaffBankList[$i]->bank_account_name;?></td>
                                  <td><?php echo $getStaffBankList[$i]->bank_account_number;?></td>
                                  <td><?php echo $getStaffBankList[$i]->bank_code;?></td>
                                  <td><?php echo $getStaffBankList[$i]->bank_address;?></td>
                                  <td><?php if($getStaffBankList[$i]->created_dt_tm)
                                  {
                                      echo date('d-m-Y',strtotime($getStaffBankList[$i]->created_dt_tm));
                                  } 
                                  ?></td>
                                  <td><?php echo $getStaffBankList[$i]->user;?></td>
                                  <td>
                                      <a href='/af/staff/bankaccount/<?php echo $id_staff;?>/<?php echo $getStaffBankList[$i]->id;?>'>Edit</a> |
                                      <a onclick="deleteStaffBankDetails(<?php echo $getStaffBankList[$i]->id; ?>)">Delete</a>
                                  </td>

                                   </tr>
                                <?php 
                            } 
                            ?>
                              </tbody>
                          </table>
                        </div>

                      </div>




              <?php
              
              }
               ?>







          </div>
      
      </div>




         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveBankDetails()
    {
        if($('#form_bank_details').valid())
        {
            $('#form_bank_details').submit();    
        }
    }



    function deleteStaffBankDetails(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {
        $.ajax(
            {
               url: '/af/staff/deleteStaffBankDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
      }
    }


    $(document).ready(function()
    {
        $("#form_bank_details").validate(
        {
            rules:
            {
                id_bank:
                {
                    required: true
                },
                bank_account_name:
                {
                    required: true
                },
                bank_account_number:
                {
                    required: true
                },
                bank_code:
                {
                    required: true
                },
                bank_address:
                {
                    required: true
                }
            },
            messages:
            {
                id_bank:
                {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                bank_account_name:
                {
                    required: "<p class='error-text'>Account Holder Name Required</p>",
                },
                bank_account_number:
                {
                    required: "<p class='error-text'>Bank Account Number Required</p>",
                },
                bank_code:
                {
                    required: "<p class='error-text'>Bank Code Required</p>",
                },
                bank_address:
                {
                    required: "<p class='error-text'>Bank Address Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>