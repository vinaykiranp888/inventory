
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <ul class="page-nav-links">

            <li><a href="/af/staff/edit/<?php echo $id_staff;?>">Faculty Details</a></li>
            <li><a href="/af/staff/bankaccount/<?php echo $id_staff;?>">Bank Account Details</a></li>
            <li><a href="/af/staff/qualification/<?php echo $id_staff;?>">Qualification Details</a></li>
            <li><a href="/af/staff/workexperience/<?php echo $id_staff;?>">Work Experience Details</a></li>
            <li class="active"><a href="/af/staff/specialization/<?php echo $id_staff;?>">Specialization Details</a></li>

        </ul>

         <div role="tabpanel" class="tab-pane" id="program_accerdation">
            <div class="mt-4">


          



              <form id="form_specialization" action="" method="post">


                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Specialization Details</h4>

                                <div class="row">

                                    <input type="hidden" class="form-control" id="btn_submit" name="btn_submit"  value="9">


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Specialization <span class='error-text'>*</span></label>
                                             <select name="id_specialization" id="id_specialization" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($specializationList)) {
                                                    foreach ($specializationList as $record) {
                                                ?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                          <?php 
                                                          if($record->id == $staffSpecialization->id_specialization)
                                                          {
                                                              echo "selected";
                                                          } ?>>
                                                            <?php echo $record->code . " - " . $record->name;  ?>        
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>
                                
                            <div class="button-block clearfix">
                                <div class="bttn-group pull-right">
                                        <button type="button" onclick="saveSpecializationDetails()" class="btn btn-primary btn-lg">Save</button>
                                        <?php
                                        if($id_specialization != NULL)
                                        {
                                          ?>
                                          <a href="<?php echo '../../specialization/'. $id_staff ?>" class="btn btn-link">Cancel</a>
                                          <?php
                                        }
                                        ?>
                                </div>
                            </div>

                    </form>

                    <div class="form-container">
                            <h4 class="form-group-title">Specialization Details</h4>

                        <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Specialization Code</th>
                                <th>Specialization Name</th>
                                <th class="text-center">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($staffSpecializationList))
                              {
                                $i=1;
                                foreach ($staffSpecializationList as $record) {
                              ?>
                                  <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->specialization_code ?></td>
                                    <td><?php echo $record->specialization_name ?></td>
                                    <td class="text-center">
                                        <a href='/af/staff/specialization/<?php echo $id_staff;?>/<?php echo $record->id;?>'>Edit</a> |
                                        <a onclick="deleteSpecializationDetails(<?php echo $record->id; ?>)">Delete</a>
                                    </td>
                                  </tr>
                              <?php
                              $i++;
                                }
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>

                    </div>










          </div>    
        </div>




         



   </div>
</div>
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>

<script type="text/javascript">

  $('select').select2();

  $(function()
  {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  });

  function saveSpecializationDetails()
  {
      if($('#form_specialization').valid())
      {
          $('#form_specialization').submit();    
      }
  }



  function deleteSpecializationDetails(id)
    {
      var cnf= confirm('Do you really want to delete?');
      if(cnf==true)
      {

        $.ajax(
            {
               url: '/af/staff/deleteSpecializationDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    location.reload();
               }
            });
      }
    }


    $(document).ready(function()
    {
        $("#form_specialization").validate(
        {
            rules:
            {
                id_specialization:
                {
                    required: true
                }
            },
            messages:
            {
                id_specialization:
                {
                    required: "<p class='error-text'>Select Specialization</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>