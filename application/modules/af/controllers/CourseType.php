<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['courseTypeList'] = $this->course_type_model->courseTypeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Course Type List';
            $this->loadViews("course_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $credit_hours_mandatory = $this->security->xss_clean($this->input->post('credit_hours_mandatory'));
                $ppp = $this->security->xss_clean($this->input->post('ppp'));
                $project_paper = $this->security->xss_clean($this->input->post('project_paper'));
                $class_time_table = $this->security->xss_clean($this->input->post('class_time_table'));
                $exam_time_table = $this->security->xss_clean($this->input->post('exam_time_table'));
                $articleship = $this->security->xss_clean($this->input->post('articleship'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'credit_hours_mandatory' => $credit_hours_mandatory,
                    'ppp' => $ppp,
                    'project_paper' => $project_paper,
                    'class_time_table' => $class_time_table,
                    'exam_time_table' => $exam_time_table,
                    'articleship' => $articleship,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->course_type_model->addNewCourseType($data);
                redirect('/setup/courseType/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Course Type';
            $this->loadViews("course_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/courseType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $credit_hours_mandatory = $this->security->xss_clean($this->input->post('credit_hours_mandatory'));
                $ppp = $this->security->xss_clean($this->input->post('ppp'));
                $project_paper = $this->security->xss_clean($this->input->post('project_paper'));
                $class_time_table = $this->security->xss_clean($this->input->post('class_time_table'));
                $exam_time_table = $this->security->xss_clean($this->input->post('exam_time_table'));
                $articleship = $this->security->xss_clean($this->input->post('articleship'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'credit_hours_mandatory' => $credit_hours_mandatory,
                    'ppp' => $ppp,
                    'project_paper' => $project_paper,
                    'class_time_table' => $class_time_table,
                    'exam_time_table' => $exam_time_table,
                    'articleship' => $articleship,
                    'status' => $status
                );

                $result = $this->course_type_model->editCourseType($data,$id);
                redirect('/setup/courseType/list');
            }
            $data['courseType'] = $this->course_type_model->getCourseType($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Course Type';
            $this->loadViews("course_type/edit", $this->global, $data, NULL);
        }
    }
}
