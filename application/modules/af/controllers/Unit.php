<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Unit extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('unit_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('unit.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['unitList'] = $this->unit_model->unitListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Unit List';
            $this->loadViews("unit/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('unit.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->unit_model->addNewUnit($data);
                redirect('/setup/unit/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Unit';
            $this->loadViews("unit/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('unit.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/unit/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->unit_model->editUnit($data,$id);
                redirect('/setup/unit/list');
            }
            $data['unit'] = $this->unit_model->getUnit($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Unit';
            $this->loadViews("unit/edit", $this->global, $data, NULL);
        }
    }
}
