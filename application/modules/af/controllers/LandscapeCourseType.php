<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class LandscapeCourseType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('landscape_course_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('landscape_course_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['landscapeCourseTypeList'] = $this->landscape_course_type_model->landscapeCourseTypeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Course Type List';
            $this->loadViews("landscape_course_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('landscape_course_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->landscape_course_type_model->addNewLandscapeCourseType($data);
                redirect('/setup/landscapeCourseType/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Course Type';
            $this->loadViews("landscape_course_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('landscape_course_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/landscapeCourseType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->landscape_course_type_model->editLandscapeCourseType($data,$id);
                redirect('/setup/landscapeCourseType/list');
            }
            $data['landscapeCourseType'] = $this->landscape_course_type_model->getLandscapeCourseType($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Course Type';
            $this->loadViews("landscape_course_type/edit", $this->global, $data, NULL);
        }
    }
}
