<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class OnlineClaim extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('online_claim_model');
        $this->isLoggedIn();
    }

    function list()
    {
       if ($this->checkAccess('online_claim.list') == 0)
       {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['semesterList'] = $this->online_claim_model->semesterListByStatus('1');
            $data['programmeList'] = $this->online_claim_model->programmeListByStatus('1');
            $data['organisationList'] = $this->online_claim_model->organisationListByStatus('1');

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_university'] = $this->security->xss_clean($this->input->post('id_university'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            
            $data['searchParameters'] = $formData;
            $data['onlineClaimList'] = $this->online_claim_model->onlineClaimListSearch($formData);
            $data['organisation'] = $this->online_claim_model->getOrganisation();
            
            // echo "<Pre>"; print_r($data['organisaton']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Online Claim List';
            $this->loadViews("online_claim/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('online_claim.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;

                $type = $this->security->xss_clean($this->input->post('type'));
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $id_branch = $this->security->xss_clean($this->input->post('id_branch'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));

                $data = array(
                    'type' => $type,
                    'id_staff' => $id_staff,
                    'id_university' => $id_university,
                    'id_branch' => $id_branch,
                    'id_programme' => $id_programme,
                    'id_semester' => $id_semester,
                    'total_amount' => $total_amount,
                    'status' => 1,
                    'created_by' => $id_user
                );

                $inserted_online_cliam_id = $this->online_claim_model->addNewOnlineClaim($data);

                if($inserted_online_cliam_id)
                {
                    $this->online_claim_model->moveTempToDetails($inserted_online_cliam_id);
                }

                redirect('/af/onlineClaim/list');
            }
            else
            {
                $deleted = $this->online_claim_model->deleteTempClaimDetailsBySessionId($id_session);
            }

            $data['semesterList'] = $this->online_claim_model->semesterListByStatus('1');
            $data['programmeList'] = $this->online_claim_model->programmeListByStatus('1');
            $data['organisationList'] = $this->online_claim_model->organisationListByStatus('1');
            $data['staffList'] = $this->online_claim_model->staffListByStatus('1');

            $data['courseList'] = $this->online_claim_model->courseListByStatus('1');
            $data['paymentRateList'] = $this->online_claim_model->paymentRateListByStatus('1');

            // echo "<Pre>"; print_r($data['organisationList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Add New Online Claim';
            $this->loadViews("online_claim/add", $this->global, $data, NULL);
        }
    }

    function edit($id)
    {

        if ($this->checkAccess('online_claim.edit') == 0)
        {
                      
            $this->loadAccessRestricted();
        
        }
        else
        {

            if ($id == null)
            {
                redirect('/af/onlineClaim/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_online_claim_type = $this->security->xss_clean($this->input->post('id_online_claim_type'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $online_claim_date = $this->security->xss_clean($this->input->post('online_claim_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'id_student' => $id_student,
                    'id_online_claim_type' => $id_online_claim_type,
                    'id_semester' => $id_semester,
                    'reason' => $reason,
                    'online_claim_date' => $online_claim_date,
                    'status' => $status,
                    'created_by' => $id_user
                );
                
                $result = $this->online_claim_model->editOnlineClaim($data, $id);

                redirect('/af/onlineClaim/list');
            }
            
            $data['onlineClaim'] = $this->online_claim_model->getOnlineClaim($id);
            $data['onlineClaimDetails'] = $this->online_claim_model->getOnlineClaimDetails($id);

            $data['semesterList'] = $this->online_claim_model->semesterListByStatus('1');
            $data['programmeList'] = $this->online_claim_model->programmeListByStatus('1');
            $data['organisationList'] = $this->online_claim_model->organisationListByStatus('1');
            $data['staffList'] = $this->online_claim_model->staffListByStatus('1');

            // echo "<Pre>";print_r($data['onlineClaimDetails']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Online Claim';
            $this->loadViews("online_claim/edit", $this->global, $data, NULL);
        }
    }

    function getBranchesByPartnerUniversity($id_partner_university)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $intake_data = $this->online_claim_model->getBranchesByPartnerUniversity($id_partner_university);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_branch' id='id_branch' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $code = $intake_data[$i]->code;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }

    function getBranchesByPartnerUniversityDisabled($id_partner_university)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $intake_data = $this->online_claim_model->getBranchesByPartnerUniversity($id_partner_university);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_branch' id='id_branch' class='form-control' disabled>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $code = $intake_data[$i]->code;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }

    function addTempClaimDetails()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['date_time'] = date('Y-m-d', strtotime($tempData['date_time']));
        $tempData['id_session'] = $id_session;
        
        $inserted_id = $this->online_claim_model->addTempClaimDetails($tempData);

        $data = $this->displayTempClaimDetails();
        
        echo $data;
    }

    function displayTempClaimDetails()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->online_claim_model->getTempClaimDetailsBySessionId($id_session); 
        // echo "<Pre>";print_r($details);exit;
         if(!empty($temp_details))
        {
            $table = "<table  class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Course</th>
                        <th>Date</th>
                        <th>Hours</th>
                        <th>Payment Rate</th>
                        <th>Amount</th>
                        <th>Action</th>
                    </tr>";

                        $total_detail_amount = 0;
                        for($i=0;$i<count($temp_details);$i++)
                        {
                        
                        $id = $temp_details[$i]->id;
                        $course_name = $temp_details[$i]->course_name;
                        $course_code = $temp_details[$i]->course_code;
                        $learning_mode = $temp_details[$i]->learning_mode;
                        $teaching_component = $temp_details[$i]->teaching_component;
                        $date_time = $temp_details[$i]->date_time;
                        $hours = $temp_details[$i]->hours;
                        $amount = $temp_details[$i]->amount;

                        if($date_time)
                        {
                            $date_time = date('d-m-Y', strtotime($date_time));
                        }

                        $j = $i+1;
                            $table .= "
                            <tr>
                                <td>$j</td>
                                <td>$course_code - $course_name</td>                       
                                <td>$date_time</td>                       
                                <td>$hours</td>                       
                                <td>$learning_mode - $teaching_component</td>                       
                                <td>$amount</td>                       
                                <td>
                                    <a onclick='deleteTempClaimDetails($id)'>Delete</a>
                                <td>
                            </tr>";

                            $total_detail_amount = $total_detail_amount + $amount;
                        }

                        $total_detail_amount = number_format($total_detail_amount,2,",",".");

                        $table .= "
                            <tr>
                                <td colspan='5'></td>
                                <td> <input type='hidden' value='$total_detail_amount' id='total_detail_amount' name='total_detail_amount'> $total_detail_amount</td>
                            </tr>";

            $table.= "</table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempClaimDetails($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $inserted_id = $this->online_claim_model->deleteTempClaimDetails($id);
        $data = $this->displayTempClaimDetails();
        echo $data; 
    }
}