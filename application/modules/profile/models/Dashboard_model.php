<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    function roleListingCount()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role');
        $this->db->from('roles as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }


    function getCourses($id)
    {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('temp_cart as tc');
        $this->db->join('product as c', 'tc.id_product = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
     }

    function salutationListSearch() {

        $this->db->select('ct.*');
        $this->db->from('salutation_setup as ct');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;

    }


     function getProgramme($id)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

   
    function addNewStudentHasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getStudentDetailsById(){
        $this->db->select('p.*');
        $this->db->from('student as p');
        $this->db->where("p.id",$id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeDetailsById($id) {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where("p.id",$id);
        $query = $this->db->get();
        return $query->row();
        
    }



    function getStudentProgramme($id) {
         $this->db->select('p.*,sp.start_date,sp.end_date');
        $this->db->from('programme as p');
        $this->db->join('student_has_programme as sp', 'sp.id_programme = p.id');
        $this->db->where("sp.id_student",$id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    
    function insertBeforeTransaction($data){
          $this->db->trans_start();
        $this->db->insert('migs_payment_transaction', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;

    }

     function insertafterTransaction($data){
          $this->db->trans_start();
        $this->db->insert('migs_payment_response', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;

    }



    function getMainInvoiceByStudentId($id)
    {
        $this->db->select('mi.*');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }


  function getProgrammeByProductTypeId($id) {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id_programme_type', $id);

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



    function getStudentDetais($id)
    {
        $this->db->select('ct.*');
        $this->db->from('student as ct');
        $this->db->where('ct.id', $id);

        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }


  function getProductType() {
        $this->db->select('ct.*');
        $this->db->from('product_type as ct');
        $this->db->join('programme as p', 'p.id_programme_type=ct.id');
        $this->db->group_by('ct.id');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getCertificateNames($id_category){
         $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->where('p.id_category', $id_category);
         $query = $this->db->get();
         $result = $query->result();  
        return $result;
     }


    function getCoursesById($id)
    {
        $this->db->select('ct.*');
        $this->db->from('product as ct');
        $this->db->where('ct.id', $id);

        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }
    
      function gerProgrammeFromSession($id) {
        $this->db->select('p.*,tc.id as tempid,tc.amount,s.name as staffname,s.ic_no,s.image as staffimage,tc.id_programme');
        $this->db->from('temp_cart as tc');
        $this->db->join('programme as p', 'tc.id_programme = p.id');
        $this->db->join('programme_has_dean as pd', 'pd.id_programme = p.id');    
        $this->db->join('staff as s', 'pd.id_staff = s.id');             
        $this->db->where("pd.status='0'");

        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }




    // Generation Of Main invoice

    function createNewMainInvoiceForStudent($data)
    {
        $id_student = $data['id_student'];
        $type = $data['type'];
        $is_discount = $data['is_discount'];
        $id_programme = $data['id_programme'];
        $inserted_id = 0;


        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id_student);
        $query = $this->db->get();
        $applicant_data = $query->row();

        $nationality = $applicant_data->nationality;



        $fee_structure = $this->getFeeStructureByIdProgramme($id_programme);

        
        if($fee_structure)
        {
            $id_fee_structure = $fee_structure->id;

            if($id_fee_structure != 0)
            {
                $fee_structure_master = $this->getFeeStructureMasterById($id_fee_structure);
            }

            // echo "<Pre>";print_r($fee_structure_master);exit;
            

            $is_sibbling_discount = '';
            $is_employee_discount = '';
            $is_alumni_discount = '';


            // echo "<Pre>";print_r($id_program_landscape);exit;


            $is_installment = 0;
            $installments = 0;
            $discount_amount = 0;
            $currency = 1;
           
            // echo "<Pre>";print_r($fee_structure_training_data);exit;

            if($fee_structure_master)
            {

                // $trigger = 'OFFER ACCEPTED';
                $trigger = '';

                // if($id_university == 1)
                // {

                    
                    // echo "<Pre>";print_r($fee_structure_master);exit;

                    $id_currency = $fee_structure_master->id_currency;
                    $currency = $id_currency;

                    $get_data['id_fee_structure'] = $id_fee_structure;
                    $get_data['id_training_center'] = 1;
                    $get_data['trigger'] = $trigger;

                   
                        $get_data['currency'] = 'MYR';
                        $detail_data = $this->getFeeStructureByData($get_data);
                   

                    if(!empty($detail_data))
                    {

                        // echo "<Pre>";print_r($detail_data);exit;
                        
                        $finance_configuration = $this->getFinanceConfiguration();
                        $gst_percentage = $finance_configuration->tax_sst;


                        $invoice_number = $this->generateMainInvoiceNumber();

                        $invoice['invoice_number'] = $invoice_number;
                        $invoice['type'] = $type;
                        $invoice['remarks'] = 'Payment';
                        $invoice['id_application'] = '0';
                        $invoice['id_student'] = $id_student;
                        $invoice['id_program'] = $id_programme;
                        $invoice['id_intake'] = $id_intake;
                        $invoice['currency'] = $id_currency;
                        $invoice['total_amount'] = '0';
                        $invoice['balance_amount'] = '0';
                        $invoice['paid_amount'] = '0';
                        $invoice['amount_before_gst'] = '0';
                        $invoice['gst_amount'] = '0';
                        $invoice['gst_percentage'] = $gst_percentage;
                        $invoice['status'] = '1';
                        $invoice['is_migrate_applicant'] = $id;
                        $invoice['created_by'] = $user_id;

                        // $detail_data = $this->getFeeStructure('13','5');

                        
                        $inserted_id = $this->addNewMainInvoice($invoice);

                        // echo "<Pre>";print_r($inserted_id);exit;
                        // if($inserted_id)
                        // {
                        //     $applicant_update_data['is_invoice_generated'] = $inserted_id;
                        //     $updated_applicant = $this->editApplicantDetails($applicant_update_data,$id_student);
                        // }



                        $total_amount = 0;
                        $total_amount_before_gst = 0;
                        $total_gst_amount = 0;
                        $total_discount_amount = 0;
                        $sibling_discount_amount = 0;
                        $employee_discount_amount = 0;
                        $alumni_discount_amount = 0;


                        // echo "<Pre>";print_r($detail_data);exit;

                        foreach ($detail_data as $fee_structure)
                        {
                            $gst_tax = '0';
                            $one_percent = 0;
                            $details_gst_amount = 0;
                            $amount_before_gst = 0;

                            $is_installment = $fee_structure->is_installment;
                            $id_training_center = $fee_structure->id_training_center;
                            $id_fee_item = $fee_structure->id_fee_item;
                            $amount = $fee_structure->amount;


                            $fee_setup = $this->getFeeSetup($id_fee_item);
                            // echo "<Pre>";print_r($fee_setup);exit;

                            if($fee_setup)
                            {
                                $gst_tax = $fee_setup->gst_tax;
                            }
                            
                            $amount_before_gst = $amount;

                            if($gst_tax == '1')
                            {
                                if($gst_percentage > 0)
                                {
                                    $one_percent = $amount * 0.01;
                                    $details_gst_amount = $one_percent * $gst_percentage;
                                }

                                // echo "<Pre>";print_r($details_gst_amount);exit;

                                
                                if($details_gst_amount > 0)
                                {
                                    $amount = $amount + $details_gst_amount;
                                }
                            }


                                $data = array(
                                    'id_main_invoice' => $inserted_id,
                                    'id_fee_item' => $id_fee_item,
                                    'amount' => $amount,
                                    'amount_before_gst' => $amount_before_gst,
                                    'gst_amount' => $details_gst_amount,
                                    'gst_percentage' => $gst_tax,
                                    'price' => $amount,
                                    'quantity' => 1,
                                    'id_reference' => $fee_structure->id,
                                    'description' => 'Course Registration Fee',
                                    'status' => 1,
                                    'created_by' => $user_id
                                );

                                $total_amount = $total_amount + $amount;
                                $total_amount_before_gst = $total_amount_before_gst + $amount_before_gst;
                                $total_gst_amount = $total_gst_amount + $details_gst_amount;

                                $this->addNewMainInvoiceDetails($data);
                            // }
                            // echo "<Pre>";print_r($data);exit;
                        }

                        $total_invoice_amount = $total_amount;


                        if($is_discount > 0)
                        {
                            $this->db->select('d.*, dt.name as discount_name');
                            $this->db->from('discount as d');
                            $this->db->join('discount_type as dt', 'd.id_discount_type = dt.id');
                            $this->db->where('d.currency', $currency);
                            $this->db->where('d.id', $is_discount);
                            $this->db->where('status', '1');
                            $query = $this->db->get();
                            $discount_data = $query->row();

                            if($discount_data)
                            {
                                $amount = $discount_data->amount;
                                $id_discount = $discount_data->id;
                                $discount_name = $discount_data->discount_name;

                                $sibling_insert = array(
                                    'id_main_invoice' => $inserted_id,
                                    'id_student' => $id_student,
                                    'name' => $discount_name,
                                    'amount' => $amount,
                                    'id_reference' => $id_discount,
                                );

                                $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
                                
                                if($discount_inserted_id)
                                {
                                    $total_amount = $total_amount - $discount_data->amount;
                                    $discount_amount = $discount_data->amount;
                                }
                            }
                        // echo "<Pre>";print_r($amount);exit;
                        }

                        $total_discount_amount = $discount_amount;


                        // $total_amount = number_format($total_amount, 2, '.', ',');
                        // echo "<Pre>";print_r($total_amount);exit;

                        $invoice_update['total_amount'] = $total_amount;
                        $invoice_update['amount_before_gst'] = $total_amount_before_gst;
                        $invoice_update['gst_amount'] = $total_gst_amount;
                        $invoice_update['balance_amount'] = $total_amount;
                        $invoice_update['invoice_total'] = $total_invoice_amount;
                        $invoice_update['total_discount'] = $total_discount_amount;
                        $invoice_update['paid_amount'] = '0';
                        // $invoice_update['inserted_id'] = $inserted_id;
                        // echo "<Pre>";print_r($invoice_update);exit;
                        $this->editMainInvoice($invoice_update,$inserted_id);
                    }
            }

        }

         return $inserted_id;
    }

       function updateProfile($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);

        return $this->db->affected_rows();
    }



    function getFeeStructureByIdProgramme($id_programme)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_programme', $id_programme);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        $result = $query->row();
        // print_r($result);exit();     
        return $result;
    }

    function getFeeStructureMasterById($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id');
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left'); 
        // $this->db->where('fst.id_training_center', $data['id_training_center']);
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure']);
        $this->db->where('fst.currency', $data['currency']);
        // $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function getFinanceConfiguration()
    {
        $this->db->select('*');
        $this->db->from('finance_configuration');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function generateMainInvoiceNumber()
    {
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editApplicantDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }

    function getFeeSetup($id)
    {
        $this->db->select('fst.*');
        $this->db->from('fee_setup as fst');
        $this->db->where('fst.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getReceiptList($id){
        $this->db->select('mi.*');
        $this->db->from('receipt as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }



    // Generate Receipt

    function getPaymentType($id)
    {
        $this->db->select('ct.*');
        $this->db->from('payment_type as ct');
        $this->db->where('ct.id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
        
        $this->db->select('j.*');
        $this->db->from('receipt as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
       $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
       return $jrnumber;
    }

    function addNewReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, cs.name as currency_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('main_invoice as mi');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    // function editMainInvoice($data,$id)
    // {
    //     $this->db->where('id', $id);
    //     $this->db->update('main_invoice', $data);
    //     return $this->db->affected_rows();
    // }

    function addNewReceiptPaymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_paid_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}