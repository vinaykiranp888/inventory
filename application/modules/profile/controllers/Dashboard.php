<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

// Create the Razorpay Order

use Razorpay\Api\Api;

class Dashboard extends BaseController
{
    public function __construct()
    {

        parent::__construct();
        $this->load->model('dashboard_model');
        $this->id_student =  $this->session->userdata['id_student'];
        if(empty($this->id_student)){
                redirect('/index');

        }
        error_reporting(0);
    }

    function index()
    {
        $id_session = session_id();

        if($this->input->post())
        {

            $billing['name'] = $this->input->post('name');
            $billing['email'] = $this->input->post('email');
            $billing['mobile'] = $this->input->post('mobile');

            $this->dashboard_model->updateProfile($billing,$this->id_student);
        }

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);
        $data['courseList'] = $this->dashboard_model->getStudentProgramme($this->id_student);
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/index", $this->global, $data, NULL);
    }

    function autologin() {


        // $functionName = 'core_user_create_users';
        // $rand = rand(00000000,999999999);

                 $studentDetails = $this->dashboard_model->getStudentDetais($this->id_student);


        // $user1 = new stdClass();
         $email = $studentDetails->email_id;

         // $users = array($user1);
         $params = [
            'user' => [
                'email' => 't@g.com'
            ]
        ];


        // $params['user']['email'] = $studentDetails->email_id;


        /// REST CALL
        $serverurl = "https://degreebybits.aeu.edu.my/webservice/rest/server.php?wstoken=".LOGIN."&wsfunction=auth_userkey_request_login_url&moodlewsrestformat=json";
        require_once ('curl.php');
        $curl = new curl();

          // print_R($params);

        $resp = $curl->post($serverurl, $params);


        // echo '</br>************************** Server Response    createUser()**************************</br></br>';
        // echo $serverurl . '</br></br>';
 $responseArray = json_decode($resp);
        // print_R($responseArray);


       $url = $responseArray->loginurl.'&wantsurl=https://degreebybits.aeu.edu.my/course/view.php?id=63';

       echo $url;exit;


    }

     function coursedetails()
    {
        $id_session = session_id();

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);


        $data['courseList'] = $this->dashboard_model->getStudentProgramme($this->id_student);
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/coursedetails", $this->global, $data, NULL);
    }

    function invoice()
    {
        $id_session = session_id();

        $data['listOfInvoices'] = $this->dashboard_model->getMainInvoiceByStudentId($this->id_student);
        // print_r($data);
        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);

        $this->global = '';
        $this->loadViews('dashboard/invoice',$this->global,$data,NULL);
    }

    function paymentgateway()
    {
        $id_session = session_id();

        $data['listOfCourses'] = $this->dashboard_model->gerProgrammeFromSession($id_session);


        $this->loadViews('dashboard/paymentgateway',$this->global,$data,NULL);
    }


     function soa()
    {
        $id_session = session_id();

        $data['listOfInvoices'] = $this->dashboard_model->getMainInvoiceByStudentId($this->id_student);
        // print_r($data);
        $data['listOfReceipts'] = $this->dashboard_model->getReceiptList($this->id_student);
        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);

        $this->global = '';
        $this->loadViews('dashboard/soa',$this->global,$data,NULL);
    }

    function profile()
    {
        if($this->input->post())
        {

               if($_FILES['image'])
                {

                    $certificate_name = date('YmdHis').$_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                    $billing['profile_pic'] = $certificate_name;
                }

                      $billing['whatsapp_number'] = $this->input->post('whatsapp_number');
                      $billing['phone'] = $this->input->post('phone');
                      $billing['email_id'] = $this->input->post('email_id');
                      $billing['last_name'] = $this->input->post('last_name');
                      $billing['first_name'] = $this->input->post('first_name');
                      $billing['salutation'] = $this->input->post('salutation');

            $this->dashboard_model->updateProfile($billing,$this->id_student);
                redirect('/profile/dashboard/profile');

             }

                   $data['salutationList'] = $this->dashboard_model->salutationListSearch();

        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);
         
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/profile", $this->global, $data, NULL);
    }


    function password()
    {


      

        if($this->input->post())
             {
                      $password = $this->input->post('salutation');

                $billing['password'] = md5($password);

            $this->dashboard_model->updateProfile($billing,$this->id_student);
                redirect('/profile/dashboard/password');

             }


        $data['studentDetails'] = $this->dashboard_model->getStudentDetais($this->id_student);
      
            
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/password", $this->global, $data, NULL);
    }


    function success()
    {

        $id_session = session_id();
        $listOfCourses = $this->dashboard_model->gerProgrammeFromSession($id_session);
        for($k=0;$k<count($listOfCourses);$k++)
        {
          $data = array();

          $data['id_student'] = $this->id_student;
          $data['type'] = 'Student';
          $data['is_discount'] = 0;
          $data['id_programme'] = $listOfCourses[$k]->id_programme;


          $idinvoiceNumber = $this->generateInvoice($data);

          $invoicegenerated = array();
          array_push($invoicegenerated,$idinvoiceNumber);         
        }

        $datareceipt['id_student'] = $this->id_student;
        $datareceipt['type'] = 'Student';
        $datareceipt['id_currency'] = '1';
        $datareceipt['receipt_amount'] = '100';
        $datareceipt['id_payment_type'] = 3;
        $datareceipt['id_invoice'] = $invoicegenerated;
        $this->generateReceipt($datareceipt);



        $this->global = '';
        $data = '';
        $this->loadViews('dashboard/success',$this->global,$data,NULL);

    }
}