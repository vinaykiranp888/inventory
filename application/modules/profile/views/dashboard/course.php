
  
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Registered Course Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../dashboard/course"> Course Details</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<section class="padding-y-10 border-bottom border-light">
  <div class="container">
       <div class="row">
                <div class="col-lg-10 mx-auto">
                  <div class="table-responsive my-4">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col" class="font-weight-semiBold">Sl No</th>
                          <th scope="col">Invoice Number</th>
                          <th scope="col">Invoice Amount (Rs)</th>
                          <th scope="col">Invoice Date</th>
                          <th scope="col">Order Status</th>
                          <th scope="col">Order Delivery Date</th>

                          <th scope="col">View Details</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php for($i=0;$i<count($courseList);$i++) {

                          if($courseList[$i]->completed_order=='0') {
                            $orderstatus = 'Dispatching Soon';
                            $orderDeliverDate = 'Coming Soon';

                          } else {
$orderstatus = 'Dispatched';
                            $orderDeliverDate = $courseList[$i]->dispact_date;
                          }

                            ?>
                        <tr>
                                                    <td><?php echo $i+1;?></td>

                        
                          <td><?php echo $courseList[$i]->invoice_number;?></td>
                          <td><?php echo $courseList[$i]->invoice_total;?></td>
                          <td><?php echo date('d-m-Y',strtotime($courseList[$i]->date_time));?></td>
                          <td><?php echo $orderstatus;?></td>
                          <td><?php echo $orderDeliverDate;?></td>
                        
                          <td><a href="/profile/dashboard/invoicedetails/<?php echo $courseList[$i]->id;?>" style="color:#2f3984;">Click Here</a></td>


                        </tr>
                      <?php } ?>
                        
                      </tbody>
                    </table>
                  </div>        
                </div>      
              </div> <!-- END row-->
    </div> <!-- END row-->
  </div> <!-- END container-->
</section> <!-- END section-->
    