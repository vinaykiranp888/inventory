
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    
    <!-- Title-->
    <title>Complete education theme for School, College, University, E-learning</title>
    
    <!-- SEO Meta-->
    <meta name="description" content="Education theme by EchoTheme">
    <meta name="keywords" content="HTML5 Education theme, responsive HTML5 theme, bootstrap 4, Clean Theme">
    <meta name="author" content="education">
    
    <!-- viewport scale-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
            
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon/favicon.ico">
    <link rel="shortcut icon" href="assets/img/favicon/114x114.png">
    <link rel="apple-touch-icon-precomposed" href="assets/img/favicon/96x96.png">
    
    
    <!--Google fonts-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">
    
    
    <!-- Icon fonts -->
    <link rel="stylesheet" href="assets/fonts/fontawesome/css/all.css">
    <link rel="stylesheet" href="assets/fonts/themify-icons/css/themify-icons.css">
    
    
    <!-- stylesheet-->    
    <link rel="stylesheet" href="assets/css/vendors.bundle.css">
    <link rel="stylesheet" href="assets/css/style.css">
    
  </head>
  
  <body>
   


  



<div class="py-2 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Shop</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Shop</a>
        </li>
        <li class="breadcrumb-item">
          Checkout
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>






<section class="padding-y-10">
  <div class="container">
   <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-1">
            <span>Your cart</span>
            <span class="badge badge-primary badge-pill"><?php echo count($listOfCourses);?></span>
          </h4>
          <ul class="list-group mb-1">
             <table width="100%" class="table table-bordered">
            <tr>
              <th><b>Product</b></th>
              <th><b>Quantity</b></th>
              <th><b>Price</b></th>
              <th><b>Total</b></th>
            </tr>
<?php for($i=0;$i<count($listOfCourses);$i++) {
$tempid = $listOfCourses[$i]->id;

$rowAmount =  $listOfCourses[$i]->amount *  $listOfCourses[$i]->quantity;;
$totalfinal = $totalfinal + $rowAmount;

 ?>
      
         <tr>
          <td><?php echo $listOfCourses[$i]->coursename;?></td>
          <td><?php echo $listOfCourses[$i]->quantity;?></td>
          <td><?php echo $listOfCourses[$i]->amount;?></td>
          <td><?php echo $rowAmount;?></td>
        </tr>



         
            <?php } ?>
             <tr>
          <th colspan="3"><b>Total</b></th>
          <th><b><?php echo $totalfinal;?></b></th>
        </tr>

                               </table>

          
          </ul>
        </div>
        <div class="col-md-8 order-md-1">
          <h4 class="mb-1">Billing address</h4>
          <form class="needs-validation" method="POST" action="">
            <div class="row">
              <div class="col-md-6 mb-1">
                <label for="firstName">Name</label>
                <input type="text" class="form-control" id="billing-name" name="billing-name" placeholder="" value="<?php echo $billingaddress->name;?>" required="required">
                <div class="invalid-feedback">
                  Name is required.
                </div>
              </div>
              <div class="col-md-6 mb-1">
                <label for="lastName">Phone</label>
                <input type="text" class="form-control" id="billing-phone" name="billing-phone" placeholder="" value="<?php echo $billingaddress->phone;?>" required="">
                <div class="invalid-feedback">
                  Phone is required.
                </div>
              </div>
            </div>

            <div class="mb-1">
              <label for="email">Email <span class="text-muted">(Optional)</span></label>
              <input type="email" class="form-control" id="billing-email" name="billing-email" placeholder="you@example.com" value="<?php echo $billingaddress->email;?>">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="mb-1">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="billing-address" name="billing-address" placeholder="1234 Main St" required="" value="<?php echo $billingaddress->address;?>">
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-1">
              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
              <input type="text" class="form-control" id="billing-address2" name="billing-address2" placeholder="Apartment or suite" value="<?php echo $billingaddress->address_two;?>">
            </div>

            <div class="row">
              <div class="col-md-4 mb-1">
                <label for="state">State</label>
                <select class="custom-select d-block w-100" name="billing-state" id="billing-state" required="">
                  <option value="1">Karnataka</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-5 mb-5">
                <label for="City">City</label>
                <input type="text" class="form-control" id="billing-city" placeholder="" name="billing-city" required="" value="<?php echo $billingaddress->city;?>">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>

              <div class="col-md-3 mb-1">
                <label for="zip">Zip</label>
                <input type="text" class="form-control" id="billing-zip" name="billing-zip" placeholder="" required=""  value="<?php echo $billingaddress->zip;?>">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>
            
            <hr class="mb-4">
            <div class="clearfix">
              <label class="ec-checkbox check-xs mb-1 mr-4">
                <input type="checkbox" name="billingtoshipping" id="billingtoshipping" onclick="getBillingtoShipping()" value="1">
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel">Shipping address is the same as my billing address</span>
              </label>
            </div>
            
         
            <h4 class="mb-1">Shipping Address</h4>

            <div class="row">
              <div class="col-md-6 mb-1">
                <label for="firstName">Name</label>
                <input type="text" class="form-control" id="shipping-name" name="shipping-name" placeholder="" value="<?php echo $shippingaddress->name;?>"  required="required">
                <div class="invalid-feedback">
                  Name is required.
                </div>
              </div>
              <div class="col-md-6 mb-1">
                <label for="lastName">Phone</label>
                <input type="text" class="form-control" id="shipping-phone" name="shipping-phone" placeholder="" value="<?php echo $shippingaddress->phone;?>"  required="">
                <div class="invalid-feedback">
                  Phone is required.
                </div>
              </div>
            </div>

            <div class="mb-1">
              <label for="email">Email <span class="text-muted">(Optional)</span></label>
              <input type="email" class="form-control" id="shipping-email" name="shipping-email" placeholder="you@example.com" value="<?php echo $shippingaddress->email;?>" >
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="mb-1">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="shipping-address" name="shipping-address" placeholder="1234 Main St" required="" value="<?php echo $shippingaddress->address;?>" >
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-1">
              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
              <input type="text" class="form-control" id="shipping-address2" name="billing-address2" placeholder="Apartment or suite" value="<?php echo $shippingaddress->address_two;?>" >
            </div>

                <div class="row">
              <div class="col-md-4 mb-1">
                <label for="state">State</label>
                <select class="custom-select d-block w-100" name="shipping-state" id="shipping-state" required="">
                  <option value="1">Karnataka</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-5 mb-5">
                <label for="City">City</label>
                <input type="text" class="form-control" id="shipping-city" placeholder="" name="shipping-city" required="" value="<?php echo $shippingaddress->city;?>" >
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>

              <div class="col-md-3 mb-1">
                <label for="zip">Zip</label>
                <input type="text" class="form-control" id="shipping-zip" name="shipping-zip" placeholder="" required="" value="<?php echo $shippingaddress->zip;?>" >
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>

            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
          </form>
        </div>
          
      </div>
  </div> <!-- END container-->
</section>




<footer class="site-footer">
  <div class="footer-top bg-dark text-white-0_6 pt-5 paddingBottom-100">
    <div class="container"> 
      <div class="row">

        <div class="col-lg-3 col-md-6 mt-5">
         <img class="footer-logo" src="/assets/images/ezzibuy_logo_white.svg" alt="Ezzibuy">
         <div class="margin-y-40">
           <p>
            Nunc placerat mi id nisi interdm they mtolis. Praesient is haretra justo ught scel erisque placer.
          </p>
         </div>
          <ul class="list-inline"> 
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-facebook"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-twitter"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-linkedin"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-pinterest"></i></a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 mt-5">
          <h4 class="h5 text-white">Contact Us</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-1"><i class="ti-headphone mr-3"></i><a href="tel:+8801740411513">800 567.890.576 </a></li>
            <li class="mb-1"><i class="ti-email mr-3"></i><a href="mailto:support@educati.com">support@educati.com</a></li>
            <li class="mb-1">
             <div class="media">
              <i class="ti-location-pin mt-2 mr-3"></i>
              <div class="media-body">
                <span> 184 Main Collins Street Chicago, United States</span>
              </div>
             </div>
            </li>
          </ul>
        </div>

        <div class="col-lg-2 col-md-6 mt-5">
          <h4 class="h5 text-white">Quick links</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-2"><a href="page-about.html">About Us</a></li>
            <li class="mb-2"><a href="page-contact.html">Contact Us</a></li>
            <li class="mb-2"><a href="page-sp-student-profile.html">Students</a></li>
            <li class="mb-2"><a href="page-sp-admission-apply.html">Admission</a></li>
            <li class="mb-2"><a href="page-events.html">Events</a></li>
            <li class="mb-2"><a href="blog-card.html">Latest News</a></li>
          </ul>
        </div>


        
      </div> <!-- END row-->
    </div> <!-- END container-->
  </div> <!-- END footer-top-->

  <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2018 Educati. All rights reserved. Created by <a href="http://echotheme.com" target="_blunk">EchoTheme</a></p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> <!-- END site-footer -->


<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="assets/js/vendors.bundle.js"></script>
    <script src="assets/js/scripts.js"></script>
  </body>
</html>

<script>

  function getBillingtoShipping() {

       $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $("#shipping-name").val($("#billing-name").val());
                $("#shipping-phone").val($("#billing-phone").val());
                $("#shipping-email").val($("#billing-email").val());
                $("#shipping-address").val($("#billing-address").val());
                $("#shipping-address2").val($("#billing-address2").val());
                $("#shipping-city").val($("#billing-city").val());
                $("#shipping-zip").val($("#billing-zip").val());
            }
            
        });

  }
  </script>