
    <!-- MAIN CONTAINER BLOCK STARTS HERE -->
    <div class="mt-3 container-fluid">
      <div class="row">
        <!-- SIDEBAR NAV STARTS HERE -->
        <div class="col-xl-2 col-md-3">
         <nav
            class="navbar navbar-expand-md navbar-light shadow-sm mb-4 mb-lg-0 small-sidenav"
          >
            <!--MENU-->
            <a
              href="#"
              class="d-xl-none d-lg-none d-md-none text-inherit font-weight-bold"
              >Menu</a
            >
            <button
              class="navbar-toggler d-md-none icon-shape icon-sm rounded bg-primary text-light"
              type="button"
              data-toggle="collapse"
              data-target="#smallSidenav"
              aria-controls="smallSidenav"
              aria-expanded="true"
              aria-label="Toggle navigation"
            >
              <span class="fe fe-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="smallSidenav">
              <div class="navbar-nav flex-column w-100">
                <div class="d-flex mb-3 align-items-center">
                  <div class="avatar avatar-md avatar-indicators avatar-online">

                 <?php if($studentDetails->image=='') { ?> 

                    <img
                      alt="avatar"
                      src="/website/img/blank.png"
                      class="rounded-circle"
                    />
                  <?php } else { ?> 

                    <img
                      alt="avatar"
                      src="/assets/images/<?php echo $studentDetails->profile_pic;?>"
                      class="rounded-circle"
                    />

                  <?php } ?> 


                  </div>
                  <div class="ml-3 lh-1">
                    <h5 class="mb-1"><?php echo $studentDetails->full_name;?></h5>
                    <p class="mb-0 text-muted"><?php echo $studentDetails->nric;?></p>
                  </div>
                </div>
                <ul class="list-unstyled mb-0">
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/index" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> Dashboard</a
                    >
                  </li>
                   <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/profile" class="nav-link"
                      ><i class="fe fe-user nav-icon"></i> Profile</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/coursedetails" class="nav-link"
                      ><i class="fe fe-book nav-icon"></i> Register Courses</a
                    >
                  </li>
                  <li class="list-unstyled nav-item ">
                    <a href="/profile/dashboard/invoice" class="nav-link"
                      ><i class="fe fe-clipboard nav-icon"></i> Invoice</a
                    >
                  </li>
                  <li class="list-unstyled nav-item active">
                    <a href="/profile/dashboard/soa" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> SOA</a
                    >
                  </li>
                 
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/password" class="nav-link"
                      ><i class="fe fe-clock nav-icon"></i> Change Password</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/logout" class="nav-link"
                      ><i class="fe fe-power nav-icon"></i> Logout</a
                    >
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>

          <div class="col-xl-7 col-md-6">
          <div class="card-header border-bottom-0 p-0 bg-light">
            <div>
              <!-- Nav -->
              <ul class="nav nav-lb-tab" id="tab" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="inProgressCourses-tab"
                    data-toggle="pill"
                    href="#inProgressCourses"
                    role="tab"
                    aria-controls="table"
                    aria-selected="true"
                    >Invoice </a
                  >
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="completedCourses-tab"
                    data-toggle="pill"
                    href="#completedCourses"
                    role="tab"
                    aria-controls="description"
                    aria-selected="false"
                    >Receipt</a
                  >
                </li>
              </ul>
            </div>
          </div>
          <!-- Card Body -->
          <div class="card-body">
            <div class="tab-content" id="tabContent">
              <div
                class="tab-pane fade show active"
                id="inProgressCourses"
                role="tabpanel"
                aria-labelledby="inProgressCourses-tab"
              >
                <div class="course-card pt-0">
                  <div class="row">
                      <div class="table-responsive">

                          <table class="table table-sm">
                      <thead>
                        <tr>
                                              <th scope="col">SL No</th>

                          <th scope="col">INVOICE ID</th>
                          <th scope="col">DATE</th>
                          <th scope="col">AMOUNT</th>
                                    <th>Download</th>
                        </tr>
                      </thead>
                      <tbody>

                        <?php for($i=0;$i<count($listOfInvoices);$i++) { ?> 
                        <tr>
                                                                              <td><?php echo $i+1;?></td>

                          <td><a href="Javascript:;"><?php echo $listOfInvoices[$i]->invoice_number;?></a></td>
                          <td><?php echo date('d-m-Y H:i',strtotime($listOfInvoices[$i]->date_time));?></td>
                          <td><?php echo $listOfInvoices[$i]->total_amount;?></td>
                          <td>
                            <a href="#"><i class="fe fe-download"></i></a>
                          </td>
                        </tr>
                        <?php } ?> 
                        
                      </tbody>
                    </table>
                  </div>

                   
                  </div>
                </div>
              </div>
              <div
                class="tab-pane fade"
                id="completedCourses"
                role="tabpanel"
                aria-labelledby="completedCourses-tab"
              >
                <div class="course-card pt-0">
                  <div class="row">

                      <div class="table-responsive">
                        <table class="table table-sm">
                         <thead>
                                  <tr>
                                    <th scope="col" class="font-weight-semiBold">Sl No</th>
                                    <th scope="col">Receipt Number</th>
                                    <th scope="col">Receipt Date</th>
                                    <th scope="col">Receipt Amount</th>
                                    <th>Download</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php for($i=0;$i<count($listOfReceipts);$i++) {?>
                                  <tr>
                                                              <td><?php echo $i+1;?></td>

                                    <td><a href="Javascript:;"><?php echo $listOfReceipts[$i]->receipt_number;?></a></td>
                                    <td><?php echo date('d-m-Y H:i:s',strtotime($listOfReceipts[$i]->receipt_date));?></td>
                                    <td><?php echo $listOfReceipts[$i]->receipt_amount;?></td>
                                    <td>
                                <a href="#"><i class="fe fe-download"></i></a>
                              </td>

                                  </tr>
                                <?php } ?>
                                  
                                </tbody>
                        </table>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
               <?php include('announcement.php');?>

        <!-- SIDEBAR NAV ENDS HERE -->
      </div>
    </div>
    <!-- MAIN CONTAINER BLOCK ENDS HERE -->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
    <script src="./js/main.js"></script>
  </body>
</html>



  