
    <!-- MAIN CONTAINER BLOCK STARTS HERE -->
    <div class="mt-3 container-fluid">
      <div class="row">
        <!-- SIDEBAR NAV STARTS HERE -->
        <div class="col-xl-2 col-md-3">
          <nav
            class="navbar navbar-expand-md navbar-light shadow-sm mb-4 mb-lg-0 small-sidenav"
          >
            <!--MENU-->
            <a
              href="#"
              class="d-xl-none d-lg-none d-md-none text-inherit font-weight-bold"
              >Menu</a
            >
            <button
              class="navbar-toggler d-md-none icon-shape icon-sm rounded bg-primary text-light"
              type="button"
              data-toggle="collapse"
              data-target="#smallSidenav"
              aria-controls="smallSidenav"
              aria-expanded="true"
              aria-label="Toggle navigation"
            >
              <span class="fe fe-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="smallSidenav">
              <div class="navbar-nav flex-column w-100">
                <div class="d-flex mb-3 align-items-center">
                  <div class="avatar avatar-md avatar-indicators avatar-online">

                 <?php if($studentDetails->image=='') { ?> 

                    <img
                      alt="avatar"
                      src="/website/img/blank.png"
                      class="rounded-circle"
                    />
                  <?php } else { ?> 

                    <img
                      alt="avatar"
                      src="/assets/images/<?php echo $studentDetails->profile_pic;?>"
                      class="rounded-circle"
                    />

                  <?php } ?> 


                  </div>
                  <div class="ml-3 lh-1">
                    <h5 class="mb-1"><?php echo $studentDetails->full_name;?></h5>
                    <p class="mb-0 text-muted"><?php echo $studentDetails->nric;?></p>
                  </div>
                </div>
                <ul class="list-unstyled mb-0">
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/index" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> Dashboard</a
                    >
                  </li>
                   <li class="list-unstyled nav-item active">
                    <a href="/profile/dashboard/profile" class="nav-link"
                      ><i class="fe fe-user nav-icon"></i> Profile</a
                    >
                  </li>
                   <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/coursedetails" class="nav-link"
                      ><i class="fe fe-book nav-icon"></i> Register Courses</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/invoice" class="nav-link"
                      ><i class="fe fe-clipboard nav-icon"></i> Invoice</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/soa" class="nav-link"
                      ><i class="fe fe-home nav-icon"></i> SOA</a
                    >
                  </li>
                 
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/password" class="nav-link"
                      ><i class="fe fe-clock nav-icon"></i> Change Password</a
                    >
                  </li>
                  <li class="list-unstyled nav-item">
                    <a href="/profile/dashboard/logout" class="nav-link"
                      ><i class="fe fe-power nav-icon"></i> Logout</a
                    >
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <div class="col-xl-7 col-md-6">
          <div class="card mb-3">
            <div class="card-header bg-light">
              <h4 class="mb-0">Profile Details</h4>
              <p class="mb-0">
                You have full control to manage your own account setting.
              </p>
            </div>
            <div class="card-body">
              <form class="form-row" action="" method="post" enctype="multipart/form-data">
                <!-- First name -->

                 <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="fname">Salutation</label>

                  <select name='salutation' id='salutation' class="form-control">
                    <?php for($s=0;$s<count($salutationList);$s++) { ?> 
                      <option value="<?php echo $salutationList[$s]->id;?>"
                         <?php if($salutationList[$s]->id==$studentDetails->salutation) {
                           echo "selected=selected"; 
                         } ?>><?php echo $salutationList[$s]->name;?></option>

                    <?php } ?> 
                  </select>
 
                </div>


                <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="fname">First Name</label>
                  <input
                    type="text"
                    id="fname"
                    class="form-control"
                    name="first_name"
                    required=""
                    value="<?php echo $studentDetails->first_name;?>"
                  />
                </div>
                <!-- Last name -->
                <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="lname">Last Name</label>
                  <input
                    type="text"
                    id="lname"
                    class="form-control"
                    name="last_name"
                    required=""
                    value="<?php echo $studentDetails->last_name;?>"                    
                  />
                </div>
                <!-- Phone -->
                <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="phone">Email</label>
                  <input
                    type="text"
                    id="phone"
                    class="form-control"
                    name="email_id"
                    required=""
                    value="<?php echo $studentDetails->email_id;?>"                    

                  />
                </div>
                  <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="phone">Phone Number</label>
                  <input
                    type="text"
                    id="phone"
                    class="form-control"
                    name="phone"
                    required=""
                    value="<?php echo $studentDetails->phone;?>"                    

                  />
                </div>

                <!-- Birthday -->
                 <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="phone">Whatsapp Number</label>
                  <input
                    type="text"
                    id="whatsapp_number"
                    class="form-control"
                    name="whatsapp_number"
                    required=""
                    value="<?php echo $studentDetails->whatsapp_number;?>"                    

                  />
                </div>

                 <div class="form-group col-12 col-md-6">
                  <label class="form-label" for="phone">Profile Picture</label>
                  <input
                    type="file"
                    name="image"
                    required=""
                    value=""                    

                  />
                </div>

              
                <div class="col-12">
                  <!-- Button -->
                  <button class="btn btn-primary" type="submit">
                    Update Profile
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
                <?php include('announcement.php');?>


        <!-- SIDEBAR NAV ENDS HERE -->
      </div>
    </div>
    <!-- MAIN CONTAINER BLOCK ENDS HERE -->

    <div class="footer">
      <div class="container">
        <div class="row align-items-center no-gutters border-top py-2">
          <!-- Desc -->
          <div class="col-md-6 col-12">
            <span>&copy; 2021 Speed. All Rights Reserved.</span>
          </div>
          <!-- Links -->
          <div class="col-12 col-md-6">
            <nav class="nav justify-content-center justify-content-md-end">
              <a class="nav-link active pl-0" href="#!">Privacy</a>
              <a class="nav-link" href="#!">Terms </a>
              <a class="nav-link" href="#!">Feedback</a>
              <a class="nav-link" href="#!">Support</a>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script
      src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
      integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
      crossorigin="anonymous"
    ></script>
    <script src="./js/main.js"></script>
  </body>
</html>
