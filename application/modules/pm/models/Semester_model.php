<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Semester_model extends CI_Model
{
    function semesterList()
    {
        $this->db->select('s.*, ay.name as academic_year');
        $this->db->from('semester as s');
        $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function schemeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('scheme as s');
        $this->db->where('s.status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function academicYearListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('academic_year as s');
        $this->db->where('s.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }
    

    function semesterListSearch($search)
    {
        $this->db->select('s.*, ay.name as academic_year');
        $this->db->from('semester as s');
        $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year','left');
        if (!empty($search))
        {
            $likeCriteria = "(s.name  LIKE '%" . $search . "%' or s.code  LIKE '%" . $search . "%' or ay.name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("academic_year", "ASC");
         $this->db->order_by("start_date", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    

    function getSemester($id)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        // $this->db->join('academic_year as ay', 'ay.id = s.id_academic_year');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSemester($data)
    {
        $this->db->trans_start();
        $this->db->insert('semester', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSemester($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('semester', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($countryId, $countryInfo)
    {
        $this->db->where('id', $countryId);
        $this->db->update('semester', $countryInfo);
        return $this->db->affected_rows();
    }


    function semesterHasScheme($id_semester)
    {
        $this->db->select('shn.*, n.description as scheme_name, n.code as scheme_code');
        $this->db->from('semester_has_scheme as shn');
        $this->db->join('scheme as n','shn.id_scheme = n.id');
        $this->db->where('shn.id_semester', $id_semester);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function tempSemesterHasScheme($data)
    {
         $this->db->trans_start();
        $this->db->insert('temp_semester_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempSemesterHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_semester_has_scheme');
        return TRUE;
    }

    function deleteTempSemesterHasSchemeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_semester_has_scheme');
        return TRUE;
    }

    function addSemesterHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('semester_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteSemesterHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('semester_has_scheme');
        return TRUE;
    }

    function getTempSemesterHasSchemeBySessionId($id_session)
    {
        $this->db->select('s.*');
        $this->db->from('temp_semester_has_scheme as s');
        $this->db->where('s.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempSemesterHasSchemeBySessionIdDisplay($id_session)
    {
        $this->db->select('s.*, n.description as scheme_name, n.code as scheme_code');
        $this->db->from('temp_semester_has_scheme as s');
        $this->db->join('scheme as n','s.id_scheme = n.id');
        $this->db->where('s.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function moveTempToDetailsTable($id_semester)
    {
        $id_session = $this->session->my_session_id;

        $details = $this->getTempSemesterHasSchemeBySessionId($id_session);

        foreach ($details as $detail)
        {
            unset($detail->id);
            unset($detail->id_session);
            $detail->id_semester = $id_semester;

            $added_details = $this->addSemesterHasScheme($detail);
        }

        $this->deleteTempSemesterHasSchemeBySession($id_session);
    }
}