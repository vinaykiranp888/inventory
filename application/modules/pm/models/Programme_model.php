<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_model extends CI_Model
{

    function partnerUniversityListSearch()
    {
        $this->db->select('d.*');
        $this->db->from('partner_university as d');

        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function sponserListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('sponser');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function educationLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    
    
    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function schemeList()
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programmeListSearch($search)
    {
        $this->db->select('p.*, aw.name as award, el.name as education_level');
        $this->db->from('programme as p');
        $this->db->join('award as aw', 'p.id_award = aw.id','left');
        $this->db->join('education_level as el', 'p.id_education_level = el.id','left');
        // $this->db->join('scheme as sc', 'p.id_scheme = sc.id');
        if (!empty($search))
        {
            $likeCriteria = "(p.name  LIKE '%" . $search . "%' or p.name_optional_language  LIKE '%" . $search . "%' or p.code  LIKE '%" . $search . "%' or p.foundation  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgrammeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('programme', $data);
        return TRUE;
    }

    function addNewTempProgrammeHasDean($data)
    {

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasDean($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewProgrammeHasCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addTempCourseDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_programme_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_programme_has_dean', $data);
        return TRUE;
    }

    function updateTempCourseDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_programme_has_course', $data);
        return TRUE;
    }

    function getTempProgrammeHasDean($id_session)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation');
        $this->db->from('temp_programme_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempProgrammeHasCourse($id_session)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('temp_programme_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasDean($id_programme)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation');
        $this->db->from('programme_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');     
        $this->db->where('tphd.id_programme', $id_programme);
        $this->db->order_by("st.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasCourse($id)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('programme_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_programme', $id);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_programme_has_dean');
    }

    function deleteTempProgHasDeanDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_programme_has_dean');
    }

    function deleteTempCourseData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_programme_has_course');
    }

    function deleteProgrammeHasDean($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('programme_has_dean');
    }

    function deleteProgrammeHasCourse($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('programme_has_course');
    }

    function addTempProgramHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_programme_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempProgrammeHasScheme($id_session)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('temp_programme_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');     
        $this->db->where('tphd.id_session', $id_session);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempProgramHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_programme_has_scheme');
        return TRUE;
    }

    function deleteTempProgramHasSchemeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_programme_has_scheme');
        return TRUE;
    }

    function addNewProgrammeHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('programme_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgrammeHasScheme($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');
        $this->db->where('tphd.id_program', $id);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function programmeObjectiveList($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_programme', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteProgrammeHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('programme_has_scheme');
        return TRUE;
    }

    function programmeLearningModeList($id_program)
    {
        $this->db->select('tphd.*, sc.name as program_name, sc.code as program_code');
        $this->db->from('programme_has_scheme as tphd');
        $this->db->join('program_type as sc', 'tphd.id_program_type = sc.id');     
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeSchemeList($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('program_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');     
        $this->db->where('tphd.id_program', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeMajoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_major_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeMinoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_minor_details as tphd');  
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeConcurrentList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_concurrent_program as tphd');   
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function programmeAcceredationList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_acceredation_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewProgrammeHasMajor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_major_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasMinor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_minor_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasConcurrent($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_concurrent_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasAcceredation($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_acceredation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgramSchemeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function saveProgramObjectiveData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_objective', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteProgramSchemeDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_scheme');
        return TRUE;
    }


    function deleteMajorDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_major_details');
        return TRUE;
    }

    function deleteMinorDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_minor_details');
        return TRUE;
    }

    function deleteCuncurrentDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_concurrent_program');
        return TRUE;
    }

    function deleteAcceredationDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_acceredation_details');
        return TRUE;
    }

    function deleteProgramObjectiveDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_objective');
        return TRUE;
    }

}