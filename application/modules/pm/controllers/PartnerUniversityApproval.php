<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerUniversityApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('partner_university.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['code'] = $this->security->xss_clean($this->input->post('code'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['short_name'] = $this->security->xss_clean($this->input->post('short_name'));
            $formData['status'] = '0';
 
            $data['searchParam'] = $formData;
            $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListSearch($formData);
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Partner University List';
            //print_r($subjectDetails);exit;
            $this->loadViews("partner_university/approval_list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('partner_university.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {

                 $formData = $this->input->post();

                // $certificate_name = $_FILES['certificate']['name'];
                // $certificate_size = $_FILES['certificate']['size'];
                // $certificate_tmp =$_FILES['certificate']['tmp_name'];

                // $certificate_ext=explode('.',$certificate_name);
                // $certificate_ext=end($certificate_ext);
                // $certificate_ext=strtolower($certificate_ext);


                // $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                // $certificate = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');


                    // echo "<Pre>";print_r($certificate);exit;



                // echo "<Pre>"; print_r($formData);exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $billing_to = $this->security->xss_clean($this->input->post('billing_to'));
                $login_id = $this->security->xss_clean($this->input->post('login_id'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $pay_as_agent = $this->security->xss_clean($this->input->post('pay_as_agent'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $account_number = $this->security->xss_clean($this->input->post('account_number'));
                $swift_code = $this->security->xss_clean($this->input->post('swift_code'));
                $bank_address = $this->security->xss_clean($this->input->post('bank_address'));



            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'id_partner_category' => $id_partner_category,
                    'id_partner_university' => $id_partner_university,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    // 'certificate' => $certificate,
                    'email' => $email,
                    'billing_to' => $billing_to,
                    'login_id' => $login_id,
                    'password' => md5($password),
                    'pay_as_agent' => $pay_as_agent,
                    'id_bank' => $id_bank,
                    'account_number' => $account_number,
                    'swift_code' => $swift_code,
                    'bank_address' => $bank_address,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->partner_university_model->addNewPartnerUniversity($data);
                redirect('/pm/partnerUniversity/edit/'.$result);
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('partner_university_model');
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
            $data['bankList'] = $this->partner_university_model->bankListByStatus('1');
            $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
            $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
            $data['statusListByType'] = $this->partner_university_model->statusListByType('PartnerUniversity');

            $this->global['pageTitle'] = 'Inventory Management : Add Partner University';
            $this->loadViews("partner_university/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('partner_university.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if ($id == null)
            {
                redirect('/pm/partnerUniversity/list');
            }


            if($this->input->post())
            {

            $resultprint = $this->input->post();

            // echo "<Pre>"; print_r($resultprint);exit();
            
            if($resultprint)
            {
             switch ($resultprint['btn_submit'])
             {

                case '1':


                 $formData = $this->input->post();



                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $billing_to = $this->security->xss_clean($this->input->post('billing_to'));
                $old_cert = $this->security->xss_clean($this->input->post('cert'));
                $pay_as_agent = $this->security->xss_clean($this->input->post('pay_as_agent'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $account_number = $this->security->xss_clean($this->input->post('account_number'));
                $swift_code = $this->security->xss_clean($this->input->post('swift_code'));
                $bank_address = $this->security->xss_clean($this->input->post('bank_address'));


            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'id_partner_category' => $id_partner_category,
                    'id_partner_university' => $id_partner_university,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    // 'certificate' => $certificate,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'billing_to' => $billing_to,
                    'pay_as_agent' => $pay_as_agent,
                    'id_bank' => $id_bank,
                    'account_number' => $account_number,
                    'swift_code' => $swift_code,
                    'bank_address' => $bank_address,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                // $certificate_name = $_FILES['certificate']['name'];

                // if($certificate_name != '')
                // {                
                //     $certificate_name = $_FILES['certificate']['name'];
                //     $certificate_size = $_FILES['certificate']['size'];
                //     $certificate_tmp =$_FILES['certificate']['tmp_name'];

                //     $certificate_ext=explode('.',$certificate_name);
                //     $certificate_ext=end($certificate_ext);
                //     $certificate_ext=strtolower($certificate_ext);


                //     $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                //     $certificate = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');
                // echo "<Pre>"; print_r($old_cert);exit;

                    // if($certificate != '' & $old_cert != '')
                    // {
                    //     $this->deleteServerFile($old_cert);
                    // }

                //     $data['certificate'] = $certificate;
                // }

                // echo "<Pre>"; print_r($data);exit;
                
                $result = $this->partner_university_model->editPartnerUniversity($data,$id);
                redirect('/pm/partnerUniversity/list');

                break;



                case '4':

                // echo "<Pre>"; print_r($_FILES);exit();

                if($_FILES['moa_file'])
                {  


                $certificate_name = $_FILES['moa_file']['name'];
                $certificate_size = $_FILES['moa_file']['size'];
                $certificate_tmp =$_FILES['moa_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'MOA File');

                $moa_file = $this->uploadFile($certificate_name,$certificate_tmp,'MOA File');

                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $start_date = $this->security->xss_clean($this->input->post('moa_start_date'));
                $end_date = $this->security->xss_clean($this->input->post('moa_end_date'));
                $name = $this->security->xss_clean($this->input->post('moa_name'));
                $id_currency = $this->security->xss_clean($this->input->post('moa_id_currency'));
                $reminder_months = $this->security->xss_clean($this->input->post('moa_reminder_months'));

                $moa = array(

                        'name' => $name,
                        'id_currency' => $id_currency,
                        'reminder_months' => $reminder_months,
                        'start_date' => date('Y-m-d', strtotime($start_date)),
                        'end_date' => date('Y-m-d', strtotime($end_date)),
                        'id_partner_university' => $id,
                        'updated_by' => $id_user,
                        'updated_dt_tm' => date('Y-m-d H:i:s')
                    );


                if($moa_file != '')
                {
                    $moa['file'] = $moa_file;
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->partner_university_model->addNewAggrement($moa);

                
                redirect($_SERVER['HTTP_REFERER']);
                    
                break;
                
                }

             }
             
            }

            $data['id'] = $id;
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
            $data['bankList'] = $this->partner_university_model->bankListByStatus('1');
            $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
            $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
            $data['stateList'] = $this->partner_university_model->stateListByStatus('1');
            $data['staffList'] = $this->partner_university_model->staffListByStatus('1');
            $data['programList'] = $this->partner_university_model->programListByStatus('1');
            $data['moduleTypeList'] = $this->partner_university_model->moduleTypeListByStatus('1');
            $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');
            $data['statusListByType'] = $this->partner_university_model->statusListByType('PartnerUniversity');


            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
            $data['comiteeList'] = $this->partner_university_model->comiteeList($id);


            $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);
            $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);
            $data['partnerProgramDetails'] = $this->partner_university_model->partnerProgramDetails($id);
            if($data['partnerProgramDetails'])
            {
                $data['partnerProgramStudyModeDetails'] = $this->partner_university_model->partnerProgramStudyModeDetails($id,$data['partnerProgramDetails']->id);
                $data['partnerProgramApprenticeshipDetails'] = $this->partner_university_model->partnerProgramApprenticeshipDetails($id,$data['partnerProgramDetails']->id);
                $data['partnerProgramInternshipDetails'] = $this->partner_university_model->partnerProgramInternshipDetails($id,$data['partnerProgramDetails']->id);
                $data['partnerProgramSyllabusDetails'] = $this->partner_university_model->partnerProgramSyllabusDetails($id,$data['partnerProgramDetails']->id);
            }


            $this->global['pageTitle'] = 'Inventory Management : Edit Partner University';
            $this->loadViews("partner_university/edit_profile", $this->global, $data, NULL);
            // $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }




    function view($id = NULL)
    {
        if ($this->checkAccess('partner_university.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/pm/partnerUniversityApproval/list');
            }


            if($this->input->post())
            {

            $resultprint = $this->input->post();

            // echo "<Pre>"; print_r($resultprint);exit();
            
            if($resultprint)
            {
             switch ($resultprint['btn_submit'])
             {

                case '1':

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                 $formData = $this->input->post();



                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $billing_to = $this->security->xss_clean($this->input->post('billing_to'));
                $old_cert = $this->security->xss_clean($this->input->post('cert'));
                $pay_as_agent = $this->security->xss_clean($this->input->post('pay_as_agent'));
                $id_bank = $this->security->xss_clean($this->input->post('id_bank'));
                $account_number = $this->security->xss_clean($this->input->post('account_number'));
                $swift_code = $this->security->xss_clean($this->input->post('swift_code'));
                $bank_address = $this->security->xss_clean($this->input->post('bank_address'));


            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'id_partner_category' => $id_partner_category,
                    'id_partner_university' => $id_partner_university,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    // 'certificate' => $certificate,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'billing_to' => $billing_to,
                    'pay_as_agent' => $pay_as_agent,
                    'id_bank' => $id_bank,
                    'account_number' => $account_number,
                    'swift_code' => $swift_code,
                    'bank_address' => $bank_address,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                // $certificate_name = $_FILES['certificate']['name'];

                // if($certificate_name != '')
                // {                
                //     $certificate_name = $_FILES['certificate']['name'];
                //     $certificate_size = $_FILES['certificate']['size'];
                //     $certificate_tmp =$_FILES['certificate']['tmp_name'];

                //     $certificate_ext=explode('.',$certificate_name);
                //     $certificate_ext=end($certificate_ext);
                //     $certificate_ext=strtolower($certificate_ext);


                //     $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                //     $certificate = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');
                // echo "<Pre>"; print_r($old_cert);exit;

                    // if($certificate != '' & $old_cert != '')
                    // {
                    //     $this->deleteServerFile($old_cert);
                    // }

                //     $data['certificate'] = $certificate;
                // }

                // echo "<Pre>"; print_r($data);exit;
                
                $result = $this->partner_university_model->editPartnerUniversity($data,$id);
                redirect('/pm/partnerUniversityApproval/list');

                break;



                case '4':

                // echo "<Pre>"; print_r($_FILES);exit();

                if($_FILES['moa_file'])
                {  


                $certificate_name = $_FILES['moa_file']['name'];
                $certificate_size = $_FILES['moa_file']['size'];
                $certificate_tmp =$_FILES['moa_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'MOA File');

                $moa_file = $this->uploadFile($certificate_name,$certificate_tmp,'MOA File');

                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $start_date = $this->security->xss_clean($this->input->post('moa_start_date'));
                $end_date = $this->security->xss_clean($this->input->post('moa_end_date'));
                $name = $this->security->xss_clean($this->input->post('moa_name'));
                $id_currency = $this->security->xss_clean($this->input->post('moa_id_currency'));
                $reminder_months = $this->security->xss_clean($this->input->post('moa_reminder_months'));

                $moa = array(

                        'name' => $name,
                        'id_currency' => $id_currency,
                        'reminder_months' => $reminder_months,
                        'start_date' => date('Y-m-d', strtotime($start_date)),
                        'end_date' => date('Y-m-d', strtotime($end_date)),
                        'id_partner_university' => $id
                    );


                if($moa_file != '')
                {
                    $moa['file'] = $moa_file;
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->partner_university_model->addNewAggrement($moa);

                
                redirect($_SERVER['HTTP_REFERER']);
                    
                break;
                
                }

             }
             
            }

            $data['id'] = $id;
            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
            $data['stateList'] = $this->partner_university_model->stateListByStatus('1');
            $data['bankList'] = $this->partner_university_model->bankListByStatus('1');
            $data['statusListByType'] = $this->partner_university_model->statusListByType('PartnerUniversity');
            
            // $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
            // $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
            // $data['staffList'] = $this->partner_university_model->staffListByStatus('1');
            // $data['programList'] = $this->partner_university_model->programListByStatus('1');
            // $data['moduleTypeList'] = $this->partner_university_model->moduleTypeListByStatus('1');
            // $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');


            // $data['comiteeList'] = $this->partner_university_model->comiteeList($id);


            // $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);
            // $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);
            // $data['partnerProgramDetails'] = $this->partner_university_model->partnerProgramDetails($id);
            // if($data['partnerProgramDetails'])
            // {
            //     $data['partnerProgramStudyModeDetails'] = $this->partner_university_model->partnerProgramStudyModeDetails($id,$data['partnerProgramDetails']->id);
            //     $data['partnerProgramApprenticeshipDetails'] = $this->partner_university_model->partnerProgramApprenticeshipDetails($id,$data['partnerProgramDetails']->id);
            //     $data['partnerProgramInternshipDetails'] = $this->partner_university_model->partnerProgramInternshipDetails($id,$data['partnerProgramDetails']->id);
            //     $data['partnerProgramSyllabusDetails'] = $this->partner_university_model->partnerProgramSyllabusDetails($id,$data['partnerProgramDetails']->id);
            // }


            $this->global['pageTitle'] = 'Inventory Management : Approve Partner University';
            $this->loadViews("partner_university/view", $this->global, $data, NULL);
            // $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }





    function addTrainingCenterInfo($id = NULL)
    {
        if ($this->checkAccess('partner_university.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/pm/partnerUniversity/list');
            }



            // $resultprint = $this->input->post();

            // echo "<Pre>"; print_r($resultprint);exit();
            
            if($this->input->post())
            {
                redirect($_SERVER['HTTP_REFERER']);             
            }

            $data['id'] = $id;
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
            $data['staffList'] = $this->partner_university_model->staffListByStatus('1');


            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
            $data['comiteeList'] = $this->partner_university_model->comiteeList($id);


            $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Partner University';
            $this->loadViews("partner_university/edit_training_center", $this->global, $data, NULL);
            // $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }


    function addAggrementInfo($id = NULL)
    {
        if ($this->checkAccess('partner_university.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/pm/partnerUniversity/list');
            }


            if($this->input->post())
            {

            $resultprint = $this->input->post();

            // echo "<Pre>"; print_r($resultprint);exit();
            
            switch ($resultprint['btn_submit'])
            {

                case '4':

                // echo "<Pre>"; print_r($_FILES);exit();

                if($_FILES['moa_file'])
                {  


                $certificate_name = $_FILES['moa_file']['name'];
                $certificate_size = $_FILES['moa_file']['size'];
                $certificate_tmp =$_FILES['moa_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'MOA File');

                $moa_file = $this->uploadFile($certificate_name,$certificate_tmp,'MOA File');

                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $start_date = $this->security->xss_clean($this->input->post('moa_start_date'));
                $end_date = $this->security->xss_clean($this->input->post('moa_end_date'));
                $name = $this->security->xss_clean($this->input->post('moa_name'));
                $id_currency = $this->security->xss_clean($this->input->post('moa_id_currency'));
                $reminder_months = $this->security->xss_clean($this->input->post('moa_reminder_months'));

                $moa = array(

                        'name' => $name,
                        'id_currency' => $id_currency,
                        'reminder_months' => $reminder_months,
                        'start_date' => date('Y-m-d', strtotime($start_date)),
                        'end_date' => date('Y-m-d', strtotime($end_date)),
                        'id_partner_university' => $id
                    );


                if($moa_file != '')
                {
                    $moa['file'] = $moa_file;
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->partner_university_model->addNewAggrement($moa);

                
                redirect($_SERVER['HTTP_REFERER']);;
                    
                    break;


                }             
            }

            $data['id'] = $id;

            $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');

            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
            $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);


            $this->global['pageTitle'] = 'Inventory Management : Edit Partner University';
            $this->loadViews("partner_university/edit_aggrement_info", $this->global, $data, NULL);
            // $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }



    function addProgramAggrementInfo($id_aggrement,$id_partner_university)
    {
        if ($this->checkAccess('partner_university.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<Pre>"; print_r($id_aggrement);exit();
            if ($id_aggrement == null)
            {
                redirect('/pm/partnerUniversity/list');
            }


            if($this->input->post())
            {
                $resultprint = $this->input->post();

                // echo "<Pre>"; print_r($resultprint);exit();

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_intake_to = $this->security->xss_clean($this->input->post('id_intake_to'));
                $id_learning_mode = $this->security->xss_clean($this->input->post('id_learning_mode'));
                $id_program_scheme = $this->security->xss_clean($this->input->post('id_program_has_scheme'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'id_education_level' => $id_education_level,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_intake_to' => $id_intake_to,
                    'id_learning_mode' => $id_learning_mode,
                    'id_program_scheme' => $id_program_scheme,
                    'id_partner_university' => $id_partner_university,
                    'id_aggrement' => $id_aggrement,
                    'status' => $status
                );

                $inserted_id = $this->partner_university_model->addNewFeeStructureMaster($data);
                redirect('/pm/partnerUniversity/addProgramAggrementInfo/'.$id_aggrement . '/' . $id_partner_university);

            }
            


            $data['id_aggrement'] = $id_aggrement;
            $data['id_partner_university'] = $id_partner_university;

            $data['feeStructureMasterListSearch'] = $this->partner_university_model->feeStructureMasterListSearch($data);

            // echo "<Pre>"; print_r($data['feeStructureMasterListSearch']);exit();

            $data['degreeTypeList'] = $this->partner_university_model->qualificationListByStatus('1');
            $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');
            $data['feeSetupList'] = $this->partner_university_model->feeSetupListByStatus('1');

            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id_partner_university);
            $data['partnerUniversityAggrement'] = $this->partner_university_model->getPartnerUniversityAggrement($id_aggrement);



            $this->global['pageTitle'] = 'Inventory Management : Edit Partner University';
            $this->loadViews("partner_university/add_fee_structure", $this->global, $data, NULL);
            // $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }


    function addFeeStructureData($id_fee_structure_master,$id_aggrement,$id_partner_university)
    {
        if ($this->checkAccess('partner_university.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<Pre>"; print_r($id_aggrement);exit();
            if ($id_fee_structure_master == null)
            {
                redirect('/pm/partnerUniversity/addProgramAggrementInfo/' . $id_aggrement . '/' . $id_partner_university);
            }


            if($this->input->post())
            {
                $resultprint = $this->input->post();

                // echo "<Pre>"; print_r($resultprint);exit();

                $is_installment = $this->security->xss_clean($this->input->post('is_installment'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_fee_item = $this->security->xss_clean($this->input->post('id_fee_item'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $installments = $this->security->xss_clean($this->input->post('installments'));
                $id_currency = $this->security->xss_clean($this->input->post('id_currency'));
                $id_fee_structure_trigger = $this->security->xss_clean($this->input->post('pu_id_fee_structure_trigger'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $aggrement = $this->partner_university_model->getAggrementByPartnerUnversityNAggrementId($id_aggrement,$id_partner_university);


                $data = array(
                    'is_installment' => $is_installment,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'id_fee_item' => $id_fee_item,
                    'amount' => $amount,
                    'installments' => $installments,
                    'currency' => $aggrement->id_currency,
                    'id_fee_structure_trigger' => $id_fee_structure_trigger,
                    'id_training_center' => $id_partner_university,
                    'id_aggrement' => $id_aggrement,
                    'id_program_landscape' => $id_fee_structure_master,
                    'status' => 1
                );

                // echo "<Pre>"; print_r($data);exit();
                
                $inserted_id = $this->partner_university_model->addNewFeeStructure($data);
                
                redirect($_SERVER['HTTP_REFERER']);

            }

            $data['id_aggrement'] = $id_aggrement;
            $data['id_partner_university'] = $id_partner_university;
            $data['id_fee_structure_master'] = $id_fee_structure_master;

            // echo "<Pre>"; print_r($data);exit();

            $data['feeStructureListSearch'] = $this->partner_university_model->feeStructureListSearch($data);

            // echo "<Pre>"; print_r($data['feeStructureListSearch']);exit();

            $data['degreeTypeList'] = $this->partner_university_model->qualificationListByStatus('1');
            $data['currencyList'] = $this->partner_university_model->currencyListByStatus('1');
            $data['feeSetupList'] = $this->partner_university_model->feeSetupListByStatus('1');
                $data['getFeeStructureTriggerList'] = $this->partner_university_model->getFeeStructureTriggerListByStatus('1');

            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id_partner_university);
            $data['partnerUniversityAggrement'] = $this->partner_university_model->getPartnerUniversityAggrement($id_aggrement);
            $data['partnerUniversityAggrementFeeMaster'] = $this->partner_university_model->getPartnerUniversityAggrementFeeMaster($id_fee_structure_master);


            // echo "<Pre>"; print_r($data['partnerUniversityAggrementFeeMaster']);exit();




            $this->global['pageTitle'] = 'Inventory Management : Edit Partner University';
            $this->loadViews("partner_university/add_fee_structure_data", $this->global, $data, NULL);
            // $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }



    function addComitee()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addComitee($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteComitee($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteComitee($id);
        
        echo "Success"; 
    }

    function getPartnerUniversity()
    {
        $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));

        $partner_category = $this->partner_university_model->getPartnerUniversityCategory($id_partner_category);

        $name = $partner_category->name;
        $code = $partner_category->code;
        $value = 0;

        if($name == 'Franchise')
        {
            $value= 1;
        }
        echo $value;exit();
    }

    function getStateByCountry($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    // Copied From Scholarship Partner University


    function getStateByCountryForTraining($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_training_state' id='id_training_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }



    function deleteMoaAggrement($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteMoaAggrement($id);
        
        echo "Success"; 
    }

     function addTrainingCenter()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addTrainingCenter($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteTrainingCenter($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteTrainingCenter($id);
        
        echo "Success"; 
    }

    function saveProgramDetailsData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $id = $tempData['id'];
        unset($tempData['id']);

        if($id > 0)
        {
            $inserted_id = $this->partner_university_model->editPartnerUniversityProgram($tempData,$id);
        }
        else
        {
            $inserted_id = $this->partner_university_model->addNewPartnerUniversityProgram($tempData);
        }

        if($inserted_id)
        {
            echo "success";
        }
        // echo "<Pre>"; print_r($tempData);exit();
    }


    function addProgramStudyModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        $inserted_id = $this->partner_university_model->addNewPartnerProgramStudyMode($tempData);
        // echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramInternshipData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        $inserted_id = $this->partner_university_model->addNewPartnerProgramInternship($tempData);
        // echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramApprenticeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addNewPartnerProgramApprenticeship($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addNewPartnerProgramSyllabus($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteProgramModeOfStudy($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramStudyMode($id);
        echo "Success";
    }

    function deleteProgramInternship($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramInternship($id);
        echo "Success";
    }

    function deleteProgramApprenticeship($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramApprenticeship($id);
        echo "Success";
    }

    function deleteProgramSyllabus($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramSyllabus($id);
        echo "Success";
    }



    function getProgrammeByEducationLevelId($id_education_level)
    {
            // echo "<Pre>"; print_r($id_education_level);exit;
            $intake_data = $this->partner_university_model->getProgrammeByEducationLevelId($id_education_level);

             $table="
            <script type='text/javascript'>
                $('select').select2();                
            </script>


            <select name='id_programme' id='id_programme' class='form-control' onchange='getIntakes()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;       
    }


    function getIntakes()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->partner_university_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake' id='id_intake' class='form-control'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }


    function getIntakesTo()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;
        // $tempData['id_semester'] = '';
        // $tempData['status'] = '';

        $student_list_data = $this->partner_university_model->getIntakeListByProgramme($tempData['id_programme']);
        // echo "<Pre>";print_r($student_list_data);exit;


        $table="

        <script type='text/javascript'>
            $('select').select2();
        </script>

        <select name='id_intake_to' id='id_intake_to' class='form-control'>";
        $table.="<option value=''>Select</option> 

        
                ";

        for($i=0;$i<count($student_list_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_list_data[$i]->id_intake;
        $intake_name = $student_list_data[$i]->intake_name;
        $intake_year = $student_list_data[$i]->intake_year;


        $table.="<option value=".$id.">". $intake_year . " - " . $intake_name.
                "</option>";

        }
        $table.="</select>";
        
        echo $table;
        exit();
    }

    function getLearningModeByProgramId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_program = $tempData['id_program'];

        $intake_data = $this->partner_university_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_learning_mode' id='id_learning_mode' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;


            $table.="<option value=".$id.">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";



            // $table.="<option value='".$id . "'";
            // if($id_learning_mode == $id)
            // {
            //    $table.="selected";
            // } $table.= ">". $mode_of_program . " - " .  $mode_of_study .
            //         "</option>";

            }
            $table.="</select>";

            echo $table;  
    }


    function getSchemeByProgramId($id_program)
    {
       $intake_data = $this->partner_university_model->getSchemeByProgramId($id_program);

        // echo "<Pre>"; print_r($intake_data);exit;
         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_program_has_scheme' id='id_program_has_scheme' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $code = $intake_data[$i]->code;
        $name = $intake_data[$i]->description;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }

    function getTrainingCenterFeeStructure()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;


        $fee_structure_data = $this->partner_university_model->getTrainingCenterFeeStructure($tempData); 
        $temp_details = $this->partner_university_model->getTrainingCenterInstallmentDetails($tempData);
        $data = $this->displayTrainingUniversityData($temp_details,$fee_structure_data);
        echo $data; 
    }

    function displayTrainingUniversityData($temp_details,$fee_structure_data)
    {
        // echo "<Pre>";print_r($fee_structure_data);exit;

        $table = "";
         if(!empty($fee_structure_data))
        {
            $amount = $fee_structure_data->amount;
            $installments = $fee_structure_data->installments;
            $id_fee_structure = $fee_structure_data->id;
            $id_training_center = $fee_structure_data->id_training_center;
            // $id_program_landscape = 0;
            $id_program_landscape = $fee_structure_data->id_program_landscape;
            $currency = $fee_structure_data->currency_code . " - " . $fee_structure_data->currency;

            $table .= "
        <input type='hidden' id='installment_amount_selected' name='installment_amount_selected' value='$amount'>
        <input type='hidden' id='installment_nos' name='installment_nos' value='$installments'>
        <input type='hidden' id='id_fee_structure' name='id_fee_structure' value='$id_fee_structure'>
        <input type='hidden' id='id_data_training_center' name='id_data_training_center' value='$id_training_center'>
        <input type='hidden' id='id_data_program_landscape' name='id_data_program_landscape' value='$id_program_landscape'>
        <input type='hidden' id='data_currency' name='data_currency' value='$currency'>
        ";
        }

        if(!empty($temp_details))
        {
            

        $table .= "
        <div class='form-container'>
         <h4 class='form-group-title'>Installment List</h4>

        <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Installment Trigger On</th>
                    <th>Fee Item</th>
                    <th>Trgger Point</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
        // echo "<Pre>";print_r($temp_details);exit;
                    $id = $temp_details[$i]->id;
                    $fee_name = $temp_details[$i]->fee_name;
                    $fee_code = $temp_details[$i]->fee_code;
                    $frequency_mode = $temp_details[$i]->frequency_mode;
                    $id_semester = $temp_details[$i]->id_semester;
                    $fee = $fee_code . " - " . $fee_name;
                    $trigger_name = $temp_details[$i]->trigger_name;
                    $amount = $temp_details[$i]->amount;

                    $j = $i+1;

                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$id_semester</td>
                            <td>$fee</td>
                            <td>$trigger_name</td>
                            <td>$frequency_mode</td>
                            <td>$amount</td>
                            <td>
                                <a onclick='deleteSemesterDataByIdFeeStructureTrainingCenter($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td colspan='4'></td>
                            <td>Total : </td>
                            <td>$total_amount</td>                           
                            <td>
                            <td>
                        </tr>";
        $table.= "</table>
        </div>
        ";


        }
        
        // $table .= "";
        
        return $table;
    }

    function saveInstallmentData()
    {

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->partner_university_model->saveInstallmentData($tempData);
        echo $inserted_id;exit;
    }

    function deleteSemesterDataByIdFeeStructureTrainingCenter($id_fee_training_center)
    {   
        $this->partner_university_model->deleteSemesterDataByIdFeeStructureTrainingCenter($id_fee_training_center);
    }
}