<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgrammeHasDean extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_has_dean_model');
        $this->load->model('programme_model');
        $this->load->model('staff_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('programme_has_dean.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['staffList'] = $this->staff_model->staffList();
            $data['programmeList'] = $this->programme_model->programmeList();

            $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
 
            $data['searchParameters'] = $formData; 

            $data['programmeHasDeanList'] = $this->programme_has_dean_model->programmeHasDeanListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Staff Has Course List';
            $this->loadViews("programme_has_dean/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('programme_has_dean.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                     
                $data = array(
                    'id_staff' => $id_staff,
                    'id_programme' => $id_programme,
                    'effective_start_date' => $effective_start_date,
                    'status' => $status
                );   

                $result = $this->programme_has_dean_model->addNewProgrammeHasDean($data);
                redirect('/setup/staffHasCourse/list');
            }
            $data['staffList'] = $this->staff_model->staffList();
            $data['programmeList'] = $this->programme_model->programmeList();
            //echo "<Pre>";print_r($data);exit();    
            $this->global['pageTitle'] = 'Inventory Management : Add Staff Has Course';
            $this->loadViews("programme_has_dean/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('programme_has_dean.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/staffHasCourse/list');
            }
            if($this->input->post())
            {
                $id_staff = $this->security->xss_clean($this->input->post('id_staff'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $effective_start_date = $this->security->xss_clean($this->input->post('effective_start_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                     
                $data = array(
                    'id_staff' => $id_staff,
                    'id_programme' => $id_programme,
                    'effective_start_date' => $effective_start_date,
                    'status' => $status
                );   
                $result = $this->programme_has_dean_model->editProgrammeHasDean($data,$id);
                redirect('/setup/staffHasCourse/list');
            }
            $data['staffList'] = $this->staff_model->staffList();
            $data['courseList'] = $this->programme_model->courseList();
            $data['programmeHasDean'] = $this->programme_has_dean_model->getProgrammeHasDean($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Staff Has Course';
            $this->loadViews("programme_has_dean/edit", $this->global, $data, NULL);
        }
    }
}