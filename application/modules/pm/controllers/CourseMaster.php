<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseMaster extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
        $this->load->model('course_master_model');
        $this->load->model('department_model');
        $this->load->model('staff_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_master.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $data['departmentList'] = $this->department_model->departmentList();
            // $data['staffList'] = $this->staff_model->staffList();

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            // $formData['id_staff'] = $this->security->xss_clean($this->input->post('id_staff'));
            // $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));

            $data['searchParam'] = $formData;
            $data['courseList'] = $this->course_model->courseListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Course Requisite';
            //print_r($subjectDetails);exit;
            $this->loadViews("course_master/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course_master.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
            $code = $this->security->xss_clean($this->input->post('code'));
            $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
            $id_staff_coordinator = $this->security->xss_clean($this->input->post('id_staff_coordinator'));
            $id_department = $this->security->xss_clean($this->input->post('id_department'));
            $credit_hours = $this->security->xss_clean($this->input->post('credit_hours'));
                            $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'name_in_malay' => $name_in_malay,
                    'id_staff_coordinator' => $id_staff_coordinator,
                    'id_department' => $id_department,
                    'credit_hours' => $credit_hours,
                    'status' => $status
                );
                
                $result = $this->course_model->addNewCourse($data);
                redirect('/setup/course_master/list');
            }
            //print_r($data['stateList']);exit;
            $data['departmentList'] = $this->department_model->departmentList();
            $data['staffList'] = $this->staff_model->staffList();
            $this->global['pageTitle'] = 'Inventory Management : Add Course';
            $this->loadViews("course_master/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course_master.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/course_master/list');
            }
            if($this->input->post())
            {
            
            $id_master_course = $id;
            $requisite_type = $this->security->xss_clean($this->input->post('requisite_type'));
            $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $min_pass_grade = $this->security->xss_clean($this->input->post('min_pass_grade'));
            $min_total_credit = $this->security->xss_clean($this->input->post('min_total_credit'));
           

                $data = array(
                    'id_master_course'=> $id_master_course,
                    'requisite_type' => $requisite_type,
                    'id_course' => $id_course,
                    'min_pass_grade' => $min_pass_grade,
                    'min_total_credit' => $min_total_credit
                );
                
                $result = $this->course_master_model->addCourseData($data,$id);
               redirect($_SERVER['HTTP_REFERER']);
            }
            $data['courseList'] = $this->course_model->courseListByStatus('1');
            $data['gradeList'] = $this->course_master_model->getGradeSetupDetailsByCourse($id);
            // $data['courseList'] = $this->course_master_model->getGradeSetupDetailsByCourse($id);
            $data['departmentList'] = $this->department_model->departmentList();
            $data['staffList'] = $this->staff_model->staffList();
            $data['courseDetails'] = $this->course_model->getCourse($id);
            $data['courseData'] = $this->course_master_model->getCourseData($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Course';
            $this->loadViews("course_master/edit", $this->global, $data, NULL);
        }
    }

    function delete_course()
    {
        $id = $this->input->get('id');

       $this->course_master_model->deleteCourseData($id);

       redirect($_SERVER['HTTP_REFERER']);
    }
}
