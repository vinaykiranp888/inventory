<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Permission</h3>
        </div>
        

            <form id="form_unit" permission="form" id="addPermission" action="" method="post" permission="form">
                <div class="box-body">


                    <div class="row">
                        <div class="col-md-6">                                
                            <div class="form-group">
                                <label for="code">Code</label>
                                <input type="text" class="form-control required" value="<?php echo set_value('code'); ?>" id="code" name="code" maxlength="128">
                            </div>
                            
                        </div>
                        <div class="col-md-6">                                
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" class="form-control required" value="<?php echo set_value('description'); ?>" id="description" name="description" maxlength="128">
                            </div>
                            
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Select Module <span class='error-text'>*</span></label>
                                <select name="module" id="module" class="form-control" onchange="getSubMenuByModule(this.value)">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($menuList))
                                    {
                                        foreach ($menuList as $record)
                                        {?>
                                            <option value="<?php echo $record->name;?>"
                                            ><?php echo $record->name;?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                          <div class="form-group">
                             <label>Submenu <span class='error-text'>*</span></label>
                             <span id="view_sub_menu">
                                <select class="form-control" id='id_menu' name='id_menu'>
                                    <option value=''></option>
                                  </select>
                             </span>
                          </div>
                       </div>

                    </div>


                </div><!-- /.box-body -->

                <div class="box-footer">
                    <input type="submit" class="btn btn-primary" value="Submit" />
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </form>


            <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>

    $('select').select2();

    function getSubMenuByModule(menu)
    {
        if(menu != '')
        {
            var tempPR = {};
            tempPR['menu'] = menu;

            $.ajax(
            {
               url: '/setup/permission/getSubMenuByModule',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_sub_menu").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                code: {
                    required: true
                },
                description: {
                    required: true
                },
                status: {
                    required: true
                },
                id_menu: {
                    required: true
                },
                module: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_menu: {
                    required: "<p class='error-text'>Select Sub Menu</p>",
                },
                module: {
                    required: "<p class='error-text'>Select Module</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>