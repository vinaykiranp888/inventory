<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Course Type</h3>
        </div>
        <form id="form_program_type" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Course Type Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $courseType->code; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $courseType->name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Credit Hours Mandatory <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="credit_hours_mandatory" id="credit_hours_mandatory" value="1" <?php if($courseType->credit_hours_mandatory=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="credit_hours_mandatory" id="credit_hours_mandatory" value="0" <?php if($courseType->credit_hours_mandatory=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Professional Project Paper <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="ppp" id="ppp" value="1" <?php if($courseType->ppp=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="ppp" id="ppp" value="0" <?php if($courseType->ppp=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>




                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Project Paper / Thesis / Desortation <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="project_paper" id="project_paper" value="1" <?php if($courseType->project_paper=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="project_paper" id="project_paper" value="0" <?php if($courseType->project_paper=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Class Timetable <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="class_time_table" id="class_time_table" value="1" <?php if($courseType->class_time_table=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="class_time_table" id="class_time_table" value="0" <?php if($courseType->class_time_table=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>



            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Exam Time Table <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="exam_time_table" id="exam_time_table" value="1" <?php if($courseType->exam_time_table=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="exam_time_table" id="exam_time_table" value="0" <?php if($courseType->exam_time_table=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Articleship <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="articleship" id="articleship" value="1" <?php if($courseType->articleship=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="articleship" id="articleship" value="0" <?php if($courseType->articleship=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($courseType->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($courseType->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>
        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form_program_type").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
