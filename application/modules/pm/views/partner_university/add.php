<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Partner University</h3>
        </div>
        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Partner University Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" onblur="getPartnerUniversityCodeDuplication()">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" onblur="getPartnerUniversityNameDuplication()">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="short_name" name="short_name">
                    </div>
                </div>


                
            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language </label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Website / URL </label>
                        <input type="text" class="form-control" id="url" name="url">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Login Id <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="login_id" name="login_id" onblur="getPartnerUniversityLoginIdDuplication()">
                    </div>
                </div>
               


            </div>

            <div class="row">


                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password <span class='error-text'>*</span></label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>


                <div class="col-sm-4">
                  <div class="form-group">
                     <label>FILE <span class='error-text'>*</span></label>
                     <input type="file" name="image" id="image">
                  </div>
                </div>

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($statusListByType))
                            {
                                foreach ($statusListByType as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>  -->



            </div>

            <!-- <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Partner Category <span class='error-text'>*</span></label>
                        <select name="id_partner_category" id="id_partner_category" class="form-control" style="width: 408px" onchange="getPartnerUniversity()">
                            <option value="">Select</option>
                            <?php
                            if (!empty($partnerCategoryList))
                            {
                                foreach ($partnerCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code ." - ".$record->name; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Billing To <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="billing_to" id="billing_to" value="Student" checked="checked"><span class="check-radio"></span> Student
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="billing_to" id="billing_to" value="Partner University"><span class="check-radio"></span> Partner University
                            </label>                              
                        </div>                         
                </div> 


                 

                <br>

                &emsp;<input type="checkbox" id="pay_as_agent" name="pay_as_agent" value="1" >&emsp;
                Pay As Agent.


            </div> -->

        </div>

        <div class="form-container" style="display: none;" id="view_partner_uninversity">
        <h4 class="form-group-title">Partner University Details</h4>


            <div class="row">


                <div class="col-sm-4">
                    <div class="forintake_has_programmem-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="forintake_has_programmem-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Upload Certificate <span class='error-text'>*</span></label>
                        <input type="file" class="form-control" id="certificate" name="certificate" >
                    </div>
                </div>

            </div>



        </div>



        <div class="form-container">
            <h4 class="form-group-title">Contact Details</h4>


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contace Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" >
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contace Email <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" onblur="getPartnerUniversityEmailIdDuplication()">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2 </label>
                        <input type="text" class="form-control" id="address2" name="address2">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <span id="view_state">
                          <select class="form-control" id='id_state' name='id_state'>
                            <option value=''></option>
                          </select>
                        </span>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>
            </div>
        </div>




        <div class="form-container">
            <h4 class="form-group-title">Bank Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank <span class='error-text'>*</span></label>
                        <select name="id_bank" id="id_bank" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bankList))
                            {
                                foreach ($bankList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Account Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="account_number" name="account_number">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Swift Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="swift_code" name="swift_code">
                    </div>
                </div>


            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="bank_address" name="bank_address">
                    </div>
                </div>


            </div>

        </div>


        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


    $('select').select2();

    $(function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
    });


    function getStateByCountry(id)
    {
        $.get("/pm/partnerUniversity/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }



    function getPartnerUniversity()
    {
         var id_partner_category = $("#id_partner_category").val();
             $.ajax(
                {
                   url: '/pm/partnerUniversity/getPartnerUniversity',
                    type: 'POST',
                   data:
                   {
                    id_partner_category: id_partner_category
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    if(result == 1)
                    {
                        $("#view_partner_uninversity").show();
                    }else
                    {
                        $("#view_partner_uninversity").hide(); 
                    }
                    // $("#view").html(result);
                    // $('#myModal').modal('hide');
                   }
                });
    }


    function getPartnerUniversityCodeDuplication()
    {
      var code = $("#code").val()

      if(code != '')
      {

        var tempPR = {};
        tempPR['code'] = code;
        tempPR['name'] = '';
        tempPR['login_id'] = '';
        tempPR['email'] = '';
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Code Not Allowed, Partner Already Registered With The Given Code : '+ code );
                  $("#code").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityNameDuplication()
    {
      var name = $("#name").val()

      if(name != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = name;
        tempPR['login_id'] = '';
        tempPR['email'] = '';
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Name Not Allowed, Partner Already Registered With The Given Name : '+ name );
                  $("#name").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityLoginIdDuplication()
    {
      var login_id = $("#login_id").val()

      if(login_id != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = '';
        tempPR['login_id'] = login_id;
        tempPR['email'] = '';
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Login ID Not Allowed, Partner Already Registered With The Given Login ID : '+ login_id );
                  $("#login_id").val('');
              }
           }
        });
      }
    }


    function getPartnerUniversityEmailIdDuplication()
    {
      var email = $("#email").val()

      if(email != '')
      {

        var tempPR = {};
        tempPR['code'] = '';
        tempPR['name'] = '';
        tempPR['login_id'] = '';
        tempPR['email'] = email;
        tempPR['id_partner'] = '';
        
        // alert(tempPR['email_id']);

        $.ajax(
        {
           url: '/pm/partnerUniversity/getPartnerUniversityDuplication',
            type: 'POST',
           data:
           {
            tempData: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
              // alert(result);
              if(result == '0')
              {
                  alert('Duplicate Partner Email ID Not Allowed, Partner Already Registered With The Given Email ID Id : '+ email );
                  $("#email").val('');
              }
           }
        });
      }
    }



    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                short_name: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address1: {
                    required: true
                },
                id_registrar: {
                    required: true
                },
                date_time: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                },
                id_partner_category: {
                    required: true
                },
                id_partner_university : {
                    required: true
                },
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                certificate : {
                    required: true
                },
                id_state : {
                    required: true
                },
                city : {
                    required: true
                },
                zipcode : {
                    required: true
                },
                login_id: {
                    required: true
                },
                password : {
                    required: true
                },
                id_bank : {
                    required: true
                },
                account_number : {
                    required: true
                },
                swift_code: {
                    required: true
                },
                bank_address : {
                    required: true
                },
                image : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                id_registrar: {
                    required: "<p class='error-text'>Select Registrar</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Joining Date</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                id_partner_category: {
                    required: "<p class='error-text'>Select Partner Category</p>",
                },
                id_partner_university: {
                    required: "<p class='error-text'>Select Partner Uiversity</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                certificate: {
                    required: "<p class='error-text'>Select Certificate To Upload</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                login_id: {
                    required: "<p class='error-text'>Login ID Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                id_bank: {
                    required: "<p class='error-text'>Select Bank</p>",
                },
                account_number: {
                    required: "<p class='error-text'>Account Number Required</p>",
                },
                swift_code: {
                    required: "<p class='error-text'>Swift Code Required</p>",
                },
                bank_address: {
                    required: "<p class='error-text'>Bank Address Required</p>",
                },
                image: {
                    required: "<p class='error-text'>Image Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>