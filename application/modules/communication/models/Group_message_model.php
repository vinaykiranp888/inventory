<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Group_message_model extends CI_Model
{
    function groupList()
    {
        $this->db->select('sp.*');
        $this->db->from('communication_group_message_recepients as sp');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function templateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('communication_template_message');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function templateList()
    {
        $this->db->select('*');
        $this->db->from('communication_template_message');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function groupListSearch($data)
    {
        $this->db->select('sp.*, i.name as template');
        $this->db->from('communication_group_message as sp');
        $this->db->join('communication_template_message as i', 'sp.id_template = i.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(sp.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['type'] != '')
        {
            $this->db->where('sp.type', $data['type']);
        }
        if ($data['id_template'] != '')
        {
            $this->db->where('sp.id_template', $data['id_template']);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  

         $recepients = array();
         foreach ($results as $result)
         {
             $count = $this->getRecepientCount($result->id);
             $result->count = $count;
             array_push($recepients, $result);

         }
         // echo "<Pre>"; print_r($recepients);exit;
         return $recepients;
    }

    function getRecepientCount($id_group)
    {
        $this->db->select('*');
        $this->db->from('communication_group_message_recepients');
        $this->db->where('id_group', $id_group);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function getGroup($id)
    {
        $this->db->select('*');
        $this->db->from('communication_group_message');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGroup($data)
    {
        $this->db->trans_start();
        $this->db->insert('communication_group_message', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function editGroup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('communication_group_message_recepients', $data);
        return TRUE;
    }

    function getGroupRecepientsListByMaster($id_group,$type)
    {

            // echo "<Pre>"; print_r($type);exit;
        switch ($type)
        {
            case 'Applicant':

                $recepient_list = $this->getGroupRecepientsByApplicant($id_group);
                break;

            case 'Student':
                $recepient_list = $this->getGroupRecepientsByStudent($id_group);
                break;

            case 'Staff':
                $recepient_list = $this->getGroupRecepientsByStaff($id_group);
                break;
            
            default:
                break;
        }

        return $recepient_list;
    }


    function getGroupRecepientsByApplicant($id_group)
    {
        $this->db->select('cgr.*, s.nric, s.full_name, s.email_id, s.gender, p.name as program_name, p.code as program_code, i.year as intake_year, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code ');
        $this->db->from('communication_group_message_recepients as cgr');
        $this->db->join('applicant as s', 'cgr.id_recepient = s.id'); 
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('cgr.id_group', $id_group);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getGroupRecepientsByStudent($id_group)
    {
        $this->db->select('cgr.*, s.nric, s.full_name, s.email_id, s.gender, p.name as program_name, p.code as program_code, i.year as intake_year, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code ');
        $this->db->from('communication_group_message_recepients as cgr');
        $this->db->join('student as s', 'cgr.id_recepient = s.id');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        $this->db->where('cgr.id_group', $id_group);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getGroupRecepientsByStaff($id_group)
    {
        $this->db->select('cgr.*, s.ic_no, s.staff_id, s.name, s.email, s.gender ');
        $this->db->from('communication_group_message_recepients as cgr');
        $this->db->join('staff as s', 'cgr.id_recepient = s.id'); 
        $this->db->where('cgr.id_group', $id_group);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }



    function getGroupRecepientsByApplicantEmail($id_group)
    {
        $this->db->select('s.email_id as email');
        $this->db->from('communication_group_message_recepients as cgr');
        $this->db->join('applicant as s', 'cgr.id_recepient = s.id'); 
        // $this->db->join('programme as p', 's.id_program = p.id'); 
        // $this->db->join('intake as i', 's.id_intake = i.id');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('cgr.id_group', $id_group);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getGroupRecepientsByStudentEmail($id_group)
    {
        $this->db->select('s.email_id as email');
        $this->db->from('communication_group_message_recepients as cgr');
        $this->db->join('student as s', 'cgr.id_recepient = s.id');
        // $this->db->join('programme as p', 's.id_program = p.id'); 
        // $this->db->join('intake as i', 's.id_intake = i.id');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('cgr.id_group', $id_group);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getGroupRecepientsByStaffEmail($id_group)
    {
        $this->db->select('s.email');
        $this->db->from('communication_group_message_recepients as cgr');
        $this->db->join('staff as s', 'cgr.id_recepient = s.id'); 
        $this->db->where('cgr.id_group', $id_group);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }




    function addGroupHasRecepient($data)
    {
        $this->db->trans_start();
        $this->db->insert('communication_group_message_recepients', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }





    function getApplicantListByData($data)
    {
        $id_programme = $data['id_programme'];
        $id_intake = $data['id_intake'];
        $student = $data['student'];

        $this->db->select('s.*');
        $this->db->from('applicant as s');
        // $this->db->join('programme as p', 's.id_program = p.id'); 
        // $this->db->join('intake as i', 's.id_intake = i.id');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        // if ($id_programme != '')
        // {
        //     $this->db->where('s.id_program', $id_programme);
            
        // }
        // if ($id_intake != '')
        // {
        //     $this->db->where('s.id_intake', $id_intake);
            
        // }
        if ($student != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $student . "%' or s.nric  LIKE '%" . $student . "%')";
            $this->db->where($likeCriteria);
            
        }
        $this->db->where('s.applicant_status', 'Approved');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;

    }


    function getStudentListByData($data)
    {
        // $id_programme = $data['id_programme'];
        // $id_intake = $data['id_intake'];
        $student = $data['student'];

        $this->db->select('s.*');
        $this->db->from('student as s');
        // $this->db->join('programme as p', 's.id_program = p.id'); 
        // $this->db->join('intake as i', 's.id_intake = i.id');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        // if ($id_programme != '')
        // {
        //     $this->db->where('s.id_program', $id_programme);
            
        // }
        // if ($id_intake != '')
        // {
        //     $this->db->where('s.id_intake', $id_intake);
            
        // }
        if ($student != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $student . "%' or s.nric  LIKE '%" . $student . "%')";
            $this->db->where($likeCriteria);
            
        }
        // $this->db->where('s.applicant_status', 'Approved');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function getStaffListByData($data)
    {
        $staff_name = $data['staff_name'];
        $staff_ic_no = $data['staff_ic_no'];

        $this->db->select('s.*');
        $this->db->from('staff as s');
        // $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        // if ($id_programme != '')
        // {
        //     $this->db->where('s.id_program', $id_programme);
            
        // }
        // if ($id_intake != '')
        // {
        //     $this->db->where('s.id_intake', $id_intake);
            
        // }
        if ($staff_name != '')
        {
            $likeCriteria = "(s.name  LIKE '%" . $staff_name . "%')";
            $this->db->where($likeCriteria);
            
        }
        if ($staff_ic_no != '')
        {
            $likeCriteria = "(s.ic_no  LIKE '%" . $staff_ic_no . "%')";
            $this->db->where($likeCriteria);
            
        }
        $this->db->where('s.status', '1');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

     function deleteStaffRecepient($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id', $id);
       $this->db->delete('communication_group_message_recepients');
    }

}

