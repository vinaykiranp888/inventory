<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SendMail extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('send_mail_model');
        $this->isLoggedIn();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Inventory Management : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkAccess('send_mail.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['sponserList'] = $this->sponser_model->sponserListSearch($name);

            $this->global['pageTitle'] = 'Inventory Management : Sponser List';
            $this->loadViews("send_mail/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('send_mail.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

         //    $id_session = $this->session->my_session_id;
         //    $user_id = $this->session->userId;

         //    $array = $this->security->xss_clean($this->input->post('checkvalue'));
         //        if (!empty($array))
         //        {

         //            foreach ($array as $value)
         //            {
         // // echo "<Pre>"; print_r($value);exit;
         //                $data = array(
         //                    'id_sponser' => $id,
         //                    'id_student' => $value,
         //                    'amount' => '0',
         //                    'status' => '1',
         //                    'created_by' => $user_id

         //                );
         //            $result = $this->send_mail_model->addNewSponserHasStudents($data);
         //            }
                    redirect($_SERVER['HTTP_REFERER']);
                // }
            }
            $data['templateList'] = $this->send_mail_model->templateListByStatus('1');
            $data['intakeList'] = $this->send_mail_model->intakeListByStatus('1');
            $data['programmeList'] = $this->send_mail_model->programListByStatus('1');

            // echo "<Pre>"; print_r($data['sponser']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Send Mail';
            $this->loadViews("send_mail/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('send_mail.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/sponser/list');
            }
            if($this->input->post())
            {
                $id_sponser = $this->security->xss_clean($this->input->post('id_sponser'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_sponser' => $id_sponser,
                    'id_student' => $id_student,
                    'amount' => $amount,
                    'status' => $status
                );
                
                $result = $this->send_mail_model->editSponserHasStudents($data,$id);
                redirect('/finance/sponserHasStudents/list');
            }
            // $data['sponserList'] = $this->sponser_model->sponserList();
            // $data['studentList'] = $this->send_mail_model->studentList();
            // $data['sponserHasStudentsDetails'] = $this->send_mail_model->getSponserHasStudents($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Sponser';
            $this->loadViews("send_mail/edit", $this->global, $data, NULL);
        }
    }

     function getStudentListByType()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        $type = $data['type'];
        
        // echo "<Pre>";print_r($data);exit();
        switch ($type)
        {
            case 'Applicant':

                $table = $this->getApplicantList($data);

                break;

            case 'Student':

                $table = $this->getStudentList($data);
                
                break;


            default:
                # code...
                break;
        }
        echo $table;        
    }

    function getApplicantList($data)
    {
        // echo "<Pre>";print_r($data);exit();
        $applicant_data = $this->send_mail_model->getApplicantListByData($data);
        
        // echo "<Pre>";print_r($applicant_data);exit();

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Applicant Name</th>
                    <th>NRIC</th>
                    <th>E Mail</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($applicant_data);$i++)
                    {
                    $id = $applicant_data[$i]->id;
                    $full_name = $applicant_data[$i]->full_name;
                    $program_code = $applicant_data[$i]->program_code;
                    $program_name = $applicant_data[$i]->program_name;
                    $intake_name = $applicant_data[$i]->intake_name;
                    $intake_year = $applicant_data[$i]->intake_year;
                    $email = $applicant_data[$i]->email_id;
                    $nric = $applicant_data[$i]->nric;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$nric</td>                           
                            <td>$email</td>                           
                            <td>$program_code - $program_name</td>                           
                            <td>$intake_year - $intake_name</td>                        
                            
                            <td class='text-center'>
                          <input type='checkbox' name='id_recepient[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        return $table;
    }

    function getStudentList($data)
    {
        // echo "<Pre>";print_r("d");exit();
        $student_data = $this->send_mail_model->getStudentListByData($data);
        
        // echo "<Pre>";print_r($student_data);exit();

        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>NRIC</th>
                    <th>E Mail</th>
                    <th>Programme</th>
                    <th>Intake</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($student_data);$i++)
                    {
                    $id = $student_data[$i]->id;
                    $full_name = $student_data[$i]->full_name;
                    $program_code = $student_data[$i]->program_code;
                    $program_name = $student_data[$i]->program_name;
                    $intake_name = $student_data[$i]->intake_name;
                    $intake_year = $student_data[$i]->intake_year;
                    $email = $student_data[$i]->email_id;
                    $nric = $student_data[$i]->nric;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$full_name</td>
                            <td>$nric</td>                           
                            <td>$email</td>                           
                            <td>$program_code - $program_name</td>                           
                            <td>$intake_year - $intake_name</td>                        
                            
                            <td class='text-center'>
                          <input type='checkbox' name='id_recepient[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        return $table;
    }

    function getStaffList()
    {
        $data = $this->security->xss_clean($this->input->post('data'));
        // echo "<Pre>";print_r($data);exit();
        $type = $data['type'];
        if($type == 'Staff')
        {

            $staff_data = $this->send_mail_model->getStaffListByData($data);
            // echo "<Pre>";print_r($staff_data);exit();

            $table = "<table  class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Staff Name</th>
                        <th>IC No.</th>
                        <th>E Mail</th>
                        <th>Staff ID</th>
                        <th>Gender</th>
                        <th>Mobile No.</th>
                        <th>DOB</th>
                        <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>";


                $total_amount = 0;
                    for($i=0;$i<count($staff_data);$i++)
                    {

                    $id = $staff_data[$i]->id;
                    $name = $staff_data[$i]->name;
                    $ic_no = $staff_data[$i]->ic_no;
                    $mobile_number = $staff_data[$i]->mobile_number;
                    $email = $staff_data[$i]->email;
                    $staff_id = $staff_data[$i]->staff_id;
                    $gender = $staff_data[$i]->gender;
                    $dob = $staff_data[$i]->dob;
                    $j = $i+1;

                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$name</td>
                            <td>$ic_no</td>                           
                            <td>$email</td>                       
                            <td>$staff_id</td>                       
                            <td>$gender</td>                       
                            <td>$mobile_number</td>                      
                            <td>$dob</td>                      
                            
                            <td class='text-center'>
                          <input type='checkbox' name='id_recepient[]' class='check' value='".$id."'>
                        </td>
                       
                        </tr>";
                    }


        $table.= "</table>";
        }
        echo $table;exit();
    }
}