<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Bulk Withdraw</h3>
        </div>
         <!-- <h4>Select Student Program & Intake For Course Registration Details</h4> -->

        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Bulk Withdraw Details</h4>


            <div class="row">

             <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program</label>
                        <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $withdrawData->programme_code ." - ". $withdrawData->programme_name?>" readonly="readonly">
                    </div>
              </div>


              <div class="col-sm-4">
                 <div class="form-group">
                  <label>Intake</label>
                  <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $withdrawData->intake_name?>" readonly="readonly">
                    </div>
              </div>

              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Name</label>
                        <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $withdrawData->course_code . " - " . $withdrawData->course_name?>" readonly="readonly">
                    </div>
              </div>
                
            </div>


             <div class="row">

              <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course Credit Hours</label>
                        <input type="text" class="form-control" id="programme" name="programme" value="<?php echo $withdrawData->credit_hours?>" readonly="readonly">
                    </div>
              </div>

            </div>


          </div>

          </form>
         <br>

            <div class="page-title clearfix">
                <h3>Bulk Courses Withdraw</h3>
            </div>

        <div class="form-container">
            <h4 class="form-group-title">Bulk Courses Withdraw Details</h4>


         <!--  <div class="custom-table">
                <div id="view"></div>
          </div> -->

        




           <div class="custom-table">
            <table class="table" id="list-table">
              <thead>
                <tr>
                  <th>Sl. No</th>
                  <th>Semester</th>
                  <th>Student Name</th>
                  <th>NRIC</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Phone</th>
                  <!-- <th>Pre-Requisit</th> -->
                </tr>
              </thead>
              <tbody>
                <?php
                if (!empty($courseList))
                {
                  $i=1;
                  foreach ($courseList as $record) {
                ?>
                    <tr>
                    <td><?php echo $i?></td>
                    <td><?php echo $record->semester_code . " - " . $record->semester_name ?></td>
                    <td><?php echo $record->full_name?></td>
                    <td><?php echo $record->nric ?></td>
                    <td><?php echo $record->email_id ?></td>
                    <td><?php echo $record->gender ?></td>
                    <td><?php echo $record->phone ?></td>
                      <!-- <td><?php echo $record->pre_requisite ?></td> -->
                     
                    </tr>
                <?php
                $i++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>