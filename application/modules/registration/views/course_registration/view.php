<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Student Course Registration</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>
      <form id="form_main_invoice" action="" method="post">
         <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
               <div class='row'>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                     </dl>
                     <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                     </dl>
                     <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id; ?></dd>
                     </dl>
                     <dl>
                        <dt>Learning Mode :</dt>
                        <dd><?php echo $studentDetails->program_scheme ?></dd>
                     </dl>
                     <dl>
                        <dt>Organisation :</dt>
                        <dd><?php
                           if($studentDetails->id_university != 1 && $studentDetails->id_university != 0)
                           {
                               echo $studentDetails->partner_university_code . " - " . $studentDetails->partner_university_name;
                           }
                           else
                           {
                               echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                           }
                             ?></dd>
                     </dl>

                     <dl>
                        <dt>Education Level :</dt>
                        <dd><?php echo $studentDetails->qualification ?></dd>
                     </dl>

                     <dl>
                        <dt>Programme Landscape :</dt>
                        <dd><?php echo $studentDetails->programme_landscape ?></dd>
                     </dl>
                     
                     <dl>
                        <dt>Current Semester :</dt>
                        <dd><?php echo $studentDetails->current_semester ?></dd>
                     </dl>
                     
                     
                  </div>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Intake :</dt>
                        <dd><?php echo $studentDetails->intake_name ?></dd>
                     </dl>
                     <dl>
                        <dt>Program :</dt>
                        <dd><?php echo $studentDetails->programme_code . " - " . $studentDetails->programme_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                     </dl>
                     <dl>
                        <dt>Advisor :</dt>
                        <dd><?php echo $studentDetails->ic_no . " - " . $studentDetails->advisor_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Branch :</dt>
                        <dd><?php
                           if($studentDetails->id_branch != 1 && $studentDetails->id_branch != 0)
                           {
                               echo $studentDetails->branch_code . " - " . $studentDetails->branch_name;
                           }
                           else
                           {
                               echo $organisationDetails->short_name . " - " . $organisationDetails->name;
                           }
                             ?></dd>
                     </dl>

                     <?php 
                  if($studentDetails->qualification == 'MASTER' || $studentDetails->qualification == 'POSTGRADUATE')
                  {
                  ?>

                     <dl>
                        <dt>Current Phd Duration :</dt>
                        <dd><?php echo $studentDetails->phd_duration ?></dd>
                     </dl>

                  <?php
                  }
                  ?>
                     <dl>
                        <dt>Landscape Type :</dt>
                        <dd><?php echo $studentDetails->program_landscape_type ?></dd>
                     </dl>
                     

                  </div>
               </div>
            </div>
         </div>


         



         <div class="form-container">
            <h4 class="form-group-title">Course Registration Details</h4>
            <div class="m-auto text-center">
               <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
               <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                  <li role="presentation" class="active" ><a href="#tab_1" class="nav-link border rounded text-center"
                     aria-controls="tab_1" aria-selected="true"
                     role="tab" data-toggle="tab">Course Registration</a>
                  </li>
               </ul>


               <div class="tab-content offers-tab-content">




                  <div role="tabpanel" class="tab-pane active" id="tab_1">
                     <div class="mt-4">
                        
                        <?php
                           if(!empty($courseRegisteredLandscapeFBySemester))
                           {
                               ?>
                        <div class="custom-table" id="printReceipt">
                           <table class="table" id="list-table">
                              <thead>
                                 <tr>
                                    <th></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                    if (!empty($courseRegisteredLandscapeFBySemester))
                                    {
                                        foreach ($courseRegisteredLandscapeFBySemester as $record)
                                        {
                                    ?>
                                 <td>
                                    <h3><?php echo $record->code . " - " . $record->name; ?></h3>
                                 </td>
                                 <br>
                                 <div class="custom-table" id="printReceipt">
                                    <table class="table" id="list-table">
                                       <thead>
                                          <tr>
                                             <th>Sl. No</th>
                                             <th>Course Code</th>
                                             <th>Course name</th>
                                             <th>Credit Hours</th>
                                             <th>Registered Semester</th>                         
                                             <th>Registered On</th>
                                             <th>Status</th>
                                             <th>Withdraw</th>                                        
                                             <th>Grade</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <?php
                                             if (!empty($record->course)) {
                                                 $i=1;
                                                 foreach ($record->course as $record_course) {
                                             ?>
                                          <tr>
                                             <td><?php echo $i ?></td>
                                             <td><?php echo $record_course->course_code ?></td>
                                             <td><?php echo $record_course->course_name ?></td>
                                             <td><?php echo $record_course->credit_hours ?></td>
                                             <td><?php echo $record_course->student_current_semester ?></td>
                                             <td>
                                                  <?php if($record_course->created_dt_tm) echo date('d-m-Y', strtotime($record_course->created_dt_tm));  ?>        
                                             </td>
                                             <td>
                                                <?php if($record_course->is_result_announced > 0)
                                                   {
                                                       echo $record_course->total_result;
                                                   }
                                                   elseif($record_course->is_exam_registered > 0)
                                                   {
                                                       echo 'Exam Registered';
                                                   }
                                                   else{
                                                       echo "Exam Not Registered";
                                                   } ?>
                                             </td>
                                             <td><?php
                                             if($record_course->is_bulk_withdraw > 0)
                                                   {
                                                       echo 'Withdraw';
                                                   }
                                              ?></td>
                                             <td><?php echo $record_course->grade ?></td>

                                          </tr>
                                          <?php
                                             $i++;
                                                 }
                                             }
                                             ?>
                                       </tbody>
                                    </table>
                                 </div>
                                 <?php
                                    }
                                    }
                                    ?>
                              </tbody>
                           </table>
                        </div>
                        <?php
                           }
                           ?>


                     </div>

                  </div>



            </div>
      </div>
   


   <footer class="footer-wrapper">
      <p>&copy; 2019 All rights, reserved</p>
   </footer>
</div>
</form>
<script type="text/javascript">
   function printDiv(divName)
   {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
   }

    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               note:
               {
                   required: true
               }         
           },
           messages:
           {
               note:
               {
                   required: "<p class='error-text'>Note Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });

</script>