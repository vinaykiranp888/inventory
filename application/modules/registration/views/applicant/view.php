<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Student</h3>

        </div>
               
               <!-- <h4 style="text-align: center;">Application Submitted Successfully On : <?php echo date('d-m-Y h: i: s A', strtotime($getApplicantDetails->submitted_date)); ?></h4>

 -->


            


<form id="form_applicant" action="" method="post" enctype="multipart/form-data">


            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#program_detail" class="nav-link border rounded text-center"
                            aria-controls="program_detail" aria-selected="true"
                            role="tab" data-toggle="tab">Profile Details</a>
                    </li>

                    <li role="presentation"><a href="#program_scheme" class="nav-link border rounded text-center"
                            aria-controls="program_scheme" role="tab" data-toggle="tab">Contact Information</a>
                    </li>

                    <!-- <li role="presentation"><a href="#program_majoring" class="nav-link border rounded text-center"
                            aria-controls="program_majoring" role="tab" data-toggle="tab">Program Interest</a>
                    </li>

                    <li role="presentation"><a href="#program_minoring" class="nav-link border rounded text-center"
                            aria-controls="program_minoring" role="tab" data-toggle="tab">Document Upload</a>
                    </li>
                    <li role="presentation"><a href="#tab_one" class="nav-link border rounded text-center"
                            aria-controls="tab_one" role="tab" data-toggle="tab">Decleration Form</a>
                    </li>

                    <li role="presentation"><a href="#tab_fee_structure" class="nav-link border rounded text-center"
                            aria-controls="tab_fee_structure" role="tab" data-toggle="tab">Fee Structure Details</a>
                    </li>


                    <?php

                    if(((!$receiptStatus) && $getApplicantDetails->applicant_status =='Approved') || $getApplicantDetails->applicant_status =='Migrated')
                    {
                    ?>

                    

                    <li role="presentation"><a href="#tab_offer_acceptance" class="nav-link border rounded text-center"
                            aria-controls="tab_offer_acceptance" role="tab" data-toggle="tab">Invoice & Offer Acceptance Details</a>
                    </li>


                    <?php
                    }
                    ?> -->

                    
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="program_detail">
                        <div class="col-12 mt-4">






            <div class="form-container">
                <h4 class="form-group-title">Profile Details</h4>      


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control" disabled>
                                <?php
                                if (!empty($salutationList)) {
                                    foreach ($salutationList as $record) {
                                ?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php if($getApplicantDetails->salutation==$record->id)
                                            {
                                                echo "selected=selected";
                                            }
                                            ?>
                                            >
                                            <?php echo $record->name;  ?>        
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-4">
                       <div class="form-group">
                          <label>Type Of Nationality <span class='error-text'>*</span></label>
                          <select name="nationality" id="nationality" class="form-control" disabled="true">
                             <option value="">Select</option>
                             <?php
                                if(!empty($nationalityList))
                                {
                                  foreach ($nationalityList as $record)
                                  {
                                ?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php if($getApplicantDetails->nationality==$record->id)
                                   {
                                       echo "selected=selected";
                                   }
                                   ?>
                                >
                                <?php echo $record->name;  ?>        
                             </option>
                              <?php
                                  }
                                }
                                ?>
                          </select>
                       </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>" readonly>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status <span class='error-text'>*</span></label>
                            <select class="form-control" id="martial_status" name="martial_status" disabled>
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getApplicantDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getApplicantDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($raceList))
                                {
                                    foreach ($raceList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_race)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span></label>
                            <select name="religion" id="religion" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($religionList))
                                {
                                    foreach ($religionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->religion)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>    
                    
                </div>


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Password <span class='error-text'>*</span></label>
                            <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                        </div>
                    </div>


                </div>


                <div class="row">
                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender" disabled>
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_program)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " .$record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    
                </div>


            </div>



            <div class="form-container">
                <h4 class="form-group-title">Application Details</h4>
                <div class="row">  

                 <div class="col-sm-4">
                            <div class="form-group">
                                <label>Applicant Status <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $getApplicantDetails->applicant_status ?>" readonly="readonly">
                            </div>
                    </div>

                </div>

                      
                </div>  



                         



                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="program_scheme">
                        <div class="mt-4">


                            <br>

                            


                            <div class="form-container">
                                <h4 class="form-group-title">Mailing Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Address 2</label>
                                            <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Country <span class='error-text'>*</span></label>
                                            <select name="mailing_country" id="mailing_country" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing State <span class='error-text'>*</span></label>
                                            <select name="mailing_state" id="mailing_state" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>



                            &emsp;<input type="checkbox" id="present_address_same_as_mailing_address" name="present_address_same_as_mailing_address" value="1" disabled <?php 
                          if ($getApplicantDetails->present_address_same_as_mailing_address  == 1)
                          {
                            echo "checked";
                          }
                           ?>>&emsp;
                           Present Address Same as mailing address

                            

                            <div class="form-container">
                                <h4 class="form-group-title">Permanent Address</h4>

                                <div class="row">
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>" readonly>
                                        </div>
                                
                                    </div><div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>" readonly>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Country <span class='error-text'>*</span></label>
                                            <select name="permanent_country" id="permanent_country" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($countryList))
                                                {
                                                    foreach ($countryList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent State <span class='error-text'>*</span></label>
                                            <select name="permanent_state" id="permanent_state" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($stateList))
                                                {
                                                    foreach ($stateList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                                    <?php echo $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent City <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>" readonly>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>" readonly>
                                        </div>
                                    </div>

                                </div>
                            </div>



                            <div class="form-container">
                           <h4 class="form-group-title">Other Details</h4>
                           <div class="row">
                              <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Whatsapp Number <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="whatsapp_number" name="whatsapp_number" value="<?php echo $getApplicantDetails->whatsapp_number ?>" readonly>
                                 </div>
                              </div>

                               <div class="col-sm-4">
                                <div class="form-group">
                                    <label>LinkedIn ID/Link: <span class='error-text'></span></label>
                                    <input type="text" class="form-control" id="linked_in" name="linked_in" value="<?php echo $getApplicantDetails->linked_in ?>" readonly>
                                 </div>
                              </div>

                               <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Facebook ID/ Link <span class='error-text'></span></label>
                                    <input type="text" class="form-control" id="facebook_id" name="facebook_id" value="<?php echo $getApplicantDetails->facebook_id ?>" readonly>
                                 </div>
                              </div>

                               <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Twitter ID/Link: <span class='error-text'></span></label>
                                    <input type="text" class="form-control" id="twitter_id" name="twitter_id" value="<?php echo $getApplicantDetails->twitter_id ?>" readonly>
                                 </div>
                              </div>

                               <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Instagram ID/ Link <span class='error-text'></span></label>
                                    <input type="text" class="form-control" id="ig_id" name="ig_id" value="<?php echo $getApplicantDetails->ig_id ?>" readonly>
                                 </div>
                              </div>

                            </div>
                          
                         </div>





                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="program_majoring">
                        <div class="mt-4">




                            <br>

                            <div class="form-container">
                                <h4 class="form-group-title"> Program Interests</h4>


                                <div class="row">

                                     <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Degree Level <span class='error-text'>*</span></label>
                                            <select name="id_degree_type" id="id_degree_type" class="form-control" disabled>
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($degreeTypeList))
                                                {
                                                    foreach ($degreeTypeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_degree_type)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program <span class='error-text'>*</span></label>
                                            <select name="id_program" id="id_program" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programList))
                                                {
                                                    foreach ($programList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_program)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " .$record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Learning Mode <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="program_scheme" name="program_scheme" value="<?php echo $getApplicantDetails->program_scheme ?>" readonly>
                                        </div>
                                    </div>



                                </div>

                                <div class="row">



                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Scheme <span class='error-text'>*</span></label>
                                            <select name="id_program_has_scheme" id="id_program_has_scheme" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($schemeList))
                                                {
                                                    foreach ($schemeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_program_has_scheme)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " . $record->description;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div> 


                              


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Intake <span class='error-text'>*</span></label>
                                            <select name="id_intake" id="id_intake" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($intakeList))
                                                {
                                                    foreach ($intakeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_intake)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->year . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>University <span class='error-text'>*</span></label>
                                            <!-- <a href="editProgram" class="btn btn-link"><span style='font-size:18px;'>&#9998;</span></a> -->
                                            <select name="id_university" id="id_university" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($partnerUniversityList))
                                                {
                                                    foreach ($partnerUniversityList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_university)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>



                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Branch <span class='error-text'>*</span></label>
                                            <!-- <a href="editProgram" class="btn btn-link"><span style='font-size:18px;'>&#9998;</span></a> -->
                                            <select name="id_intake" id="id_intake" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($branchList))
                                                {
                                                    foreach ($branchList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_branch)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Structure Type <span class='error-text'>*</span></label>
                                            <select name="id_program_structure_type" id="id_program_structure_type" class="form-control" disabled="true">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($programStructureTypeList))
                                                {
                                                    foreach ($programStructureTypeList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>"
                                                            <?php 
                                                            if($record->id == $getApplicantDetails->id_program_structure_type)
                                                            {
                                                                echo "selected=selected";
                                                            } ?>>
                                                            <?php echo $record->code . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                </div>




                            </div>





                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="program_minoring">
                        <div class="mt-4">

                            <br>

                            <!-- <div class="form-container" id="view_document" style="display: show">
                                <h4 class="form-group-title">Documents To Upload</h4> -->
                             
                                <div id='doc'>
                                </div>


                                <?php

                              if(!empty($applicantUploadedFiles))
                              {
                                  ?>
                                  <br>

                                  <div class="form-container">
                                          <h4 class="form-group-title">Document Uploaded Details</h4>

                                      

                                        <div class="custom-table">
                                          <table class="table">
                                              <thead>
                                                  <tr>
                                                  <th>Sl. No</th>
                                                   <th>Name</th>
                                                   <th style="text-align: center;">File</th>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                   <?php
                                               $total = 0;
                                                for($i=0;$i<count($applicantUploadedFiles);$i++)
                                               { ?>
                                                  <tr>
                                                  <td><?php echo $i+1;?></td>
                                                  <td><?php echo $applicantUploadedFiles[$i]->document_name;?></td>
                                                  <td class="text-center">

                                                      <a href="<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>)" title="<?php echo $applicantUploadedFiles[$i]->file; ?>">View</a>
                                                  </td>

                                                   </tr>
                                                <?php
                                            } 
                                            ?>
                                              </tbody>
                                          </table>
                                        </div>

                                      </div>

                              <?php
                              
                              }
                               ?>

                            <!-- </div> -->







                        </div>
                    
                    </div>




            <div role="tabpanel" class="tab-pane" id="tab_one">
                    <div class="mt-4">


                         <div class="form-container">
                                    
                                <h4 class="form-group-title">Decleration</h4>

                                <br>



                                <h4 class="modal-title">Agree To Terms & Condition To Submit The Application</h4>


                                <br>



                                    <p>
                                     1. These terms and conditions represent an agreement between the University of Edinburgh ("University") and you, a prospective student. By accepting the University's offer of a place on a programme, you accept these terms and conditions in full, which along with your offer and the University's rules, regulations, policies and procedures and the most recently published prospectus (as applicable), form the contract between you and the University in relation to your studies at the University as amended from time to time pursuant to Clause 1.3 (the "Contract"). 
                                    </p>

                                    <p>

                                     2.    If you have any questions or concerns about these terms and conditions, please contact the University's Student Recruitment and Admission Office:
                                    </p>  

                                    <p>Requirements
                                    </p>  

                                   


                                    <br>

                                    &emsp;<input type="checkbox" id="is_submitted" name="is_submitted" value="1"
                                    <?php
                                     echo 'checked'; ?> disabled>&emsp;
                                    By Checking This Button I accept the <u><a onclick="showModel()">Terms and Conditions</a> <span class='error-text'>*</span></u>



                            </div>

                    <br>

                    <div class="form-container">
                            <h4 class="form-group-title">Program Requirement Details</h4>
                         

                            <div class="row">

                                
                        
                    <?php
                        if($programDetails->is_apel == 1)
                            {

                                ?>


                                <input type='radio' name='entry' id="entry"
                                <?php
                                    if($getApplicantDetails->id_program_requirement == 1)
                                    {
                                        echo 'checked';
                                    }
                                ?>
                                >Appel Entry</td>


                                <?php
                            }
                    ?>




                     <?php
                        if (!empty($programEntryRequirementList))
                        {
                            foreach ($programEntryRequirementList as $record)
                            {
                                ?>
                                    <br>
                                      <input type="radio" name="entry" id="entry" 
                                      <?php
                                      if($record->id == $getApplicantDetails->id_program_requirement)
                                    {
                                        echo 'checked';
                                    }
                                      ?>
                                      disabled>
                                    
                                <?php 
                                 $and = '';

                                if($record->age == 1)
                                {
                                    echo "Age Min. " . $record->min_age . " Year, Max. " . $record->max_age . " Year" ;
                                    $and = ", AND <br>";
                                }
                                if($record->education == 1)
                                {
                                    echo $and . "Education Requirement is : " 
                                    // . $programEntryRequirementList[$i]->qualification_code  .  " - "
                                     . $record->qualification_name ;
                                    $and = " ,  AND <br>";

                                }
                                if($record->work_experience == 1)
                                {
                                    echo $and . "Work Experience is : " . $record->work_code . " - " . $record->work_name . ", With Min. Experience Of " . $record->min_work_experience . " Years";
                                    $and = " ,  AND <br>";
                                    
                                }
                                if($record->other == 1)
                                {
                                    echo $and . "Other Requirements Description : " . $record->other_description. " .";
                                }
                               ?>
                                           


                           <?php
                            }
                        }
                        ?>

                                
                        </div>

                    </div>







                    </div>
                
                </div>























            

              <div role="tabpanel" class="tab-pane" id="tab_fee_structure">
                      
                  <div class="mt-4">


                  <br>



                        <?php

                        if(!empty($feeStructureDetails))
                        {
                            ?>
                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Fee Structure Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Fee Item</th>
                                             <th>Trigger On</th>
                                             <th>Frequency Mode</th>
                                             <th>Currency</th>
                                             <th>Installment(if)</th>
                                             <th>Type</th>
                                             <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($feeStructureDetails);$i++)
                                         { ?>
                                          
                                          <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $feeStructureDetails[$i]->fee_structure_code . " - " . $feeStructureDetails[$i]->fee_structure;?></td>
                                            <td><?php echo $feeStructureDetails[$i]->trigger_name;?></td>
                                            <td><?php echo $feeStructureDetails[$i]->frequency_mode;?></td>
                                            <td><?php
                                            if($getApplicantDetails->id_university == 1)
                                            {
                                             echo $feeStructureDetails[$i]->currency;
                                            }
                                            else
                                            {
                                              echo $feeStructureDetails[$i]->currency_name;
                                            }
                                            ?></td>
                                            <td><?php 
                                            if($feeStructureDetails[$i]->is_installment == 1)
                                            {
                                              echo $feeStructureDetails[$i]->installments;
                                            }
                                            ?></td>
                                            <td><?php 
                                            if($feeStructureDetails[$i]->is_installment == 1)
                                            {
                                              echo 'Installment';
                                            }
                                            else
                                            {
                                              echo 'Per Semester';
                                            }
                                            ;?></td>
                                            <td style="padding: right"><?php echo $feeStructureDetails[$i]->amount;?></td>
                                          </tr>
                                          <?php
                                          $total = $total + $feeStructureDetails[$i]->amount;
                                      } 
                                      ?>
                                          <tr>
                                            <td style="background:" colspan="7"><?php echo '';?></td>
                                            <td style="padding: right"> Total : <?php echo $total;?></td>
                                          </tr>

                                        </tbody>
                                    </table>
                                  </div>

                                </div>

                        <?php


                        }
                        else
                        {
                          ?>

                          <a class="btn btn-link btn-back">
                          No Fee Structure Available, Define Fee Structure For The Program Interest </a>


                        <?php
                        }
                        
                        if($getApplicantDetails->applicant_status == 'Draft')
                        {
                            if(isset($sibblingDiscount) || isset($employeeDiscount) || isset($alumniDiscount))
                            {
                            ?>


                           <a class="btn btn-link btn-back">
                          Discount Details If Applied</a>



                            <div class="form-container">
                                    <h4 class="form-group-title">Discount Details</h4>

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Discount Name</th>
                                             <th>Currency</th>
                                             <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if($sibblingDiscount && ($getApplicantDetails->is_sibbling_discount == 0 || $getApplicantDetails->is_sibbling_discount == 1))
                                        {
                                        ?>
                                        <tr>    
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo " Sibbling Discount ";?></td>
                                            <td><?php echo $sibblingDiscount->currency;?></td>
                                            <td><?php echo $sibblingDiscount->amount;?></td>
                                        </tr>

                                        <?php
                                        }
                                        if($employeeDiscount && ($getApplicantDetails->is_employee_discount == 0 || $getApplicantDetails->is_employee_discount == 1))
                                        {
                                        ?>
                                        <tr>    
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo " Employee Discount";?></td>
                                            <td><?php echo $employeeDiscount->currency;?></td>
                                            <td><?php echo $employeeDiscount->amount;?></td>
                                        </tr>

                                        <?php
                                        }
                                        if($alumniDiscount && ($getApplicantDetails->is_alumni_discount == 0 || $getApplicantDetails->is_alumni_discount == 1))
                                        {
                                        ?>
                                        <tr>    
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo " Alumni Discount ";?></td>
                                            <td><?php echo $alumniDiscount->currency;?></td>
                                            <td><?php echo $alumniDiscount->amount;?></td>
                                        </tr>

                                        <?php
                                        }
                                      ?>

                                        </tbody>
                                    </table>
                                  </div>

                            </div>

                            <?php
                            }
                        }
                        ?>




                    <?php
                      
                      if($applicantInvoice)
                      {

                        if(!empty($applicantInvoiceDiscountDetails))
                        {
                            ?>
                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Discount Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Discount Description</th>
                                             <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total_discount = 0;
                                          for($i=0;$i<count($applicantInvoiceDiscountDetails);$i++)
                                         { ?>
                                          
                                          <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $applicantInvoiceDiscountDetails[$i]->name;?></td>
                                            <td><?php echo $applicantInvoiceDiscountDetails[$i]->amount;?></td>
                                          </tr>
                                          <?php
                                          $total_discount = $total_discount + $applicantInvoiceDiscountDetails[$i]->amount;
                                      } 
                                      ?>
                                          <tr>
                                            <td colspan="2"><?php echo '';?></td>
                                            <td style="padding: right"> Total : <?php echo $total_discount;?></td>
                                          </tr>

                                        </tbody>
                                    </table>
                                  </div>

                                </div>

                        <?php
                        
                        }
                      }
                      
                      ?>


                  </div>
                  
              </div>




              <div role="tabpanel" class="tab-pane" id="tab_offer_acceptance">
                      
                  <div class="mt-4">


                  <input type="hidden" id="id_applicant" name="id_applicant" class="form-control" value="<?php echo $getApplicantDetails->id ?>" readonly="readonly">


                  <?php

                    if($getApplicantDetails->applicant_status == 'Migrated')
                    {
                        echo "<h3> ";  echo $getApplicantDetails->full_name; " , Your Succesfully Migrated As Student By Accepting The Offer On : "; echo date('d-m-Y', strtotime($getApplicantDetails->approved_dt_tm));  "</h3>"; 
                    }


                    if(((!$receiptStatus) && $getApplicantDetails->applicant_status =='Approved') || $getApplicantDetails->applicant_status !='Migrated')
                    {

                        

                    ?>



                  <!-- <div class="button-block clearfix">
                      <div class="bttn-group">
                          <button type="submit" class="btn btn-primary btn-lg" onclick="generateReceiptAndMoveApplicant()">Accept & Pay Fee</button>
                      </div>
                  </div>


                  <p>
                    <h3>Congrates <?php echo $getApplicantDetails->full_name; ?> .</h3>

                    We have offered you the admission, and below is the fees to be paid the invoice Generated For The Fee.
                    <br>
                    &emsp;<input type="checkbox" id="check_offer" name="check_offer" value="1" >&emsp;
                    By Checking This Button I accept the Admisson Offer 

                  </p>-->


                  <br>

                  <?php
                  }
                  ?>



                  <h4 class="sub-title">Main Invoice Details</h4>


                  <?php
                  if($applicantInvoice)
                  {
                    ?>



                  <div class="data-list">
                      <div class="row">
      
                          <div class="col-sm-6">
                              <dl>
                                  <dt>Invoice Type :</dt>
                                  <dd><?php echo $applicantInvoice->type;?></dd>
                                <input type="hidden" id="id_main_invoice" name="id_main_invoice" class="form-control" value="<?php echo $applicantInvoice->id ?>" readonly="readonly">
                                <input type="hidden" id="total_amount" name="total_amount" class="form-control" value="<?php echo $applicantInvoice->total_amount ?>" readonly="readonly">
                              </dl>

                              <dl>
                                  <dt>Invoice Date & Time :</dt>
                                  <dd><?php echo $applicantInvoice->date_time;?></dd>
                              </dl>

                              <dl>
                                  <dt>Invoice Currency :</dt>
                                  <dd><?php echo $applicantInvoice->currency;?></dd>
                              </dl>

                              <dl>
                                  <dt>Payable Amount :</dt>
                                  <dd><?php echo $applicantInvoice->total_amount;?></dd>
                              </dl>

                              <dl>
                                  <dt>Paid Amount :</dt>
                                  <dd><?php echo $applicantInvoice->paid_amount;?></dd>
                                <input type="hidden" id="paid_amount" name="paid_amount" class="form-control" value="<?php echo $applicantInvoice->paid_amount ?>" readonly="readonly">
                              </dl>
                          </div>        
                          
                          <div class="col-sm-6">
                              <dl>
                                  <dt>Invoice Number :</dt>
                                  <dd><?php echo $applicantInvoice->invoice_number;?></dd>
                              </dl>

                              <dl>
                                  <dt>Remarks :</dt>
                                  <dd><?php echo $applicantInvoice->remarks;?></dd>
                              </dl>

                              <dl>
                                  <dt>Invoice Total :</dt>
                                  <dd><?php echo $applicantInvoice->invoice_total;?></dd>
                              </dl>

                              <dl>
                                  <dt>Invoice Discount :</dt>
                                  <dd><?php echo $applicantInvoice->total_discount;?></dd>
                              </dl>

                              <dl>
                                  <dt>Balance Amount :</dt>
                                  <dd><?php echo $applicantInvoice->balance_amount;?></dd>
                                <input type="hidden" id="balance_amount" name="balance_amount" class="form-control" value="<?php echo $applicantInvoice->balance_amount ?>" readonly="readonly">
                              </dl>

                          </div>
      
                      </div>
                  </div>






                    <?php

                      if(!empty($applicantInvoiceDetails))
                      {
                            ?>
                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Invoice Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Fee Item</th>
                                             <th>Frequency Mode</th>
                                             <th>Currency</th>
                                             <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($applicantInvoiceDetails);$i++)
                                         { ?>
                                          
                                          <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $applicantInvoiceDetails[$i]->fee_structure_code . " - " . $applicantInvoiceDetails[$i]->fee_structure;?></td>
                                            <td><?php echo $applicantInvoiceDetails[$i]->frequency_mode;?></td>
                                            <td><?php echo $applicantInvoice->currency;?></td>
                                            <td style="padding: right"><?php echo $applicantInvoiceDetails[$i]->amount;?></td>
                                          </tr>
                                          <?php
                                          $total = $total + $applicantInvoiceDetails[$i]->amount;
                                      } 
                                      ?>
                                          <tr>
                                            <td style="background:" colspan="4"><?php echo '';?></td>
                                            <td style="padding: right"> Total : <?php echo $total;?></td>
                                          </tr>

                                        </tbody>
                                    </table>
                                  </div>

                                </div>

                        <?php
                        
                        }
                      
                      ?>




                      <?php

                        if(!empty($applicantInvoiceDiscountDetails))
                        {
                            ?>
                            <br>

                            <div class="form-container">
                                    <h4 class="form-group-title">Invoice Discount Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Discount Description</th>
                                             <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total_discount = 0;
                                          for($i=0;$i<count($applicantInvoiceDiscountDetails);$i++)
                                         { ?>
                                          
                                          <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $applicantInvoiceDiscountDetails[$i]->name;?></td>
                                            <td><?php echo $applicantInvoiceDiscountDetails[$i]->amount;?></td>
                                          </tr>
                                          <?php
                                          $total_discount = $total_discount + $applicantInvoiceDiscountDetails[$i]->amount;
                                      } 
                                      ?>
                                          <tr>
                                            <td colspan="2"><?php echo '';?></td>
                                            <td style="padding: right"> Total : <?php echo $total_discount;?></td>
                                          </tr>

                                        </tbody>
                                    </table>
                                  </div>

                                </div>

                        <?php
                        
                        }
                      
                      ?>












                    <?php

                  }
                  else
                  {
          
                  ?>

                    <a class="btn btn-link btn-back">
                    No Invoice Available </a>

                    <?php
                  }

                  ?>



                  </div>
                  
              </div>





                </div>
                


            </div>
            
        </form>

        <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
                


        </div>


    </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>




    $(document).ready(function()
    {
        $("#form_applicant").validate({
            rules: {
                sibbling_status: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                sibbling_status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function submitApp()
    {
        $('#id_intake').prop('disabled', false);
        $('#id_program').prop('disabled', false);
        if($('#form_applicant').valid())
        {    
            $('#form_applicant').submit();
        }
    }

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>