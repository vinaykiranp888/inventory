<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Data Analysis By Learning Center And Semester</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Exam Event</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


              

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Learning Center </label>
                      <div class="col-sm-8">
                        <select name="id_university" id="id_university" class="form-control selitemIcon" onchange="getBranchesByPartnerUniversity(this.value)">
                            <option value="">Select</option>
                              <?php
                            if (!empty($partnerUniversityList)) {
                              foreach ($partnerUniversityList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_university']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester </label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control selitemIcon">
                            <option value="">Select</option>
                              <?php
                            if (!empty($semesterList)) {
                              foreach ($semesterList as $record)
                              {
                                $selected = '';
                                if ($record->id == $searchParam['id_semester']) {
                                  $selected = 'selected';
                                }
                            ?>
                                <option value="<?php echo $record->id;  ?>"
                                  <?php echo $selected;  ?>>
                                  <?php echo  $record->code . " - " . $record->name;  ?>
                                  </option>
                            <?php
                            }
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>



              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="learningCenterNSemester" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
       <table class="table" border='1'>
        
        <thead>
          <tr>
            <th rowspan="2">Program</th>
              <th colspan="<?php echo count($intakeList);?>" class="text-center">Full Time</th>
              <th colspan="<?php echo count($intakeList);?>" class="text-center">Part Time</th>
              <th colspan="<?php echo count($intakeList);?>" class="text-center">Online</th>
            </tr>

         


             <tr>
               

              <?php
              foreach ($intakeList as $record)
              { ?>
               <th class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
              <?php
              }
            ?>


               <?php
           
              foreach ($intakeList as $record)
              {
            ?>
              <th class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
            <?php
              }
            ?>

               <?php
           
              foreach ($intakeList as $record)
              {
            ?>
              <th class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
            <?php
              }
            ?>




            </tr>

        </thead>
             <?php 
             $this->dataanalysisModel = new Data_analysis_model();

                foreach ($programmeList as $record)
               {
             ?>
                <tr>
                  <th>
                    <a href="<?php echo 'view/' . $record->id ; ?>" title="View">
                      
                    <?php echo $record->code . " - " . $record->name; ?>
                    </a>
                      
                    </th>


                <?php foreach ($intakeList as $intakeindividual)
                {
                  //full time 
                  
                  $learning_mode = 'Full Time';
                  $id_program = $record->id;
                  $id_intake = $intakeindividual->id;

                  $fulltimeCount = $this->dataanalysisModel->getStudentByProgramModeIntake($id_intake,$id_program,$learning_mode,$searchParam);

              ?>
                <!-- <th class="text-center">
                  <a href="<?php echo 'view/' . $id_program . '/' . $id_intake ; ?>" title="View"><?php echo $fulltimeCount ?></a>
                </th> -->

                <th class="text-center"><?php echo $fulltimeCount ?></th>
              <?php
                }
              ?>


              <?php foreach ($intakeList as $intakeindividual)
                {

                  //full time 
                  $learning_mode = 'Part Time';
                  $id_program = $record->id;
                  $id_intake = $intakeindividual->id;

                  $partTimeCount = $this->dataanalysisModel->getStudentByProgramModeIntake($id_intake,$id_program,$learning_mode,$searchParam);

              ?>
                <!-- <th class="text-center">
                  <a href="<?php echo 'view/' . $id_program . '/' . $id_intake ; ?>" title="View"><?php echo $partTimeCount ?></a>
                </th> -->

                <th class="text-center"><?php echo $partTimeCount ?></th>
              <?php
                }
              ?>



              <?php foreach ($intakeList as $intakeindividual)
                {

                  //full time 
                  $learning_mode = 'Online';
                  $id_program = $record->id;
                  $id_intake = $intakeindividual->id;
                  $onlinecount = $this->dataanalysisModel->getStudentByProgramModeIntake($id_intake,$id_program,$learning_mode,$searchParam);

              ?>
                <!-- <th class="text-center">
                <a href="<?php echo 'view/' . $id_program . '/' . $id_intake ; ?>" title="View"><?php echo $onlinecount ?></a>
                </th> -->
                <th class="text-center"><?php echo $onlinecount ?></th>

              
              <?php
                }
              ?>


                
                </tr>

               <?php
                }
                ?>
    
      </table>

      <!-- <table class="table" border='1' style="display: none;">
        <thead >
          <tr>
            <th rowspan="3" >Programme</th>
            <?php
            if (!empty($intakeList))
            {
              foreach ($intakeList as $record)
              {
            ?>
              <th colspan='6' class="text-center"><?php echo $record->year . " - " . $record->name; ?></th>
            <?php
              }
            }
            ?>
         </tr>

             <tr>
            <?php 
             foreach ($intakeList as $record)
              {
            ?>
            <th class="text-center" colspan='2'>Blended </th>
            <th class="text-center" colspan='2'>Face to Face</th>
            <th class="text-center" colspan='2'>Online</th>
            <?php
              
            }
            ?>
          </tr>


           <tr>
            <?php 
             foreach ($intakeList as $record)
              {
            ?>
            <th class="text-center">Part Time </th>
            <th class="text-center">Full Time</th>
            <th class="text-center">Part Time </th>
            <th class="text-center">Full Time</th>
            <th class="text-center">Part Time </th>
            <th class="text-center">Full Time</th>
            <?php
              
            }
            ?>
          </tr>
        </thead>
      </table> -->

    </div>



  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
