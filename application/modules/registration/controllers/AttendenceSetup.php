<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AttendenceSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('attendence_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('attendence_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
 

            $data['searchParam'] = $formData;


            $data['attendenceSetupList'] = $this->attendence_setup_model->attendenceSetupListSearch($formData);
            $data['programList'] = $this->attendence_setup_model->programListByStatus('1');
            // $name = $this->security->xss_clean($this->input->post('name'));

            // $data['searchName'] = $name;
            // $data['attendenceSetupList'] = $this->attendence_setup_model->attendenceSetupListSearch();


            $this->global['pageTitle'] = 'Inventory Management : Attendence Setup';
            $this->loadViews("attendence_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('attendence_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));
                $min_warning = $this->security->xss_clean($this->input->post('min_warning'));
                $max_warning = $this->security->xss_clean($this->input->post('max_warning'));
                $is_barring_applicable = $this->security->xss_clean($this->input->post('is_barring_applicable'));
                $min_barring = $this->security->xss_clean($this->input->post('min_barring'));
                $max_barring = $this->security->xss_clean($this->input->post('max_barring'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                   'id_program' => $id_program,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'program_scheme' => $program_scheme,
                    'min_warning' => $min_warning,
                    'max_warning' => $max_warning,
                    'is_barring_applicable' => $is_barring_applicable,
                    'min_barring' => $min_barring,
                    'max_barring' => $max_barring,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->attendence_setup_model->addAttendenceSetup($data);
                redirect('/registration/attendenceSetup/list');
            }

            $data['programList'] = $this->attendence_setup_model->programListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Attendence Setup';
            $this->loadViews("attendence_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('attendence_setup.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/attendenceSetup/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $program_scheme = $this->security->xss_clean($this->input->post('program_scheme'));
                $min_warning = $this->security->xss_clean($this->input->post('min_warning'));
                $max_warning = $this->security->xss_clean($this->input->post('max_warning'));
                $is_barring_applicable = $this->security->xss_clean($this->input->post('is_barring_applicable'));
                $min_barring = $this->security->xss_clean($this->input->post('min_barring'));
                $max_barring = $this->security->xss_clean($this->input->post('max_barring'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                   'id_program' => $id_program,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'program_scheme' => $program_scheme,
                    'min_warning' => $min_warning,
                    'max_warning' => $max_warning,
                    'is_barring_applicable' => $is_barring_applicable,
                    'min_barring' => $min_barring,
                    'max_barring' => $max_barring,
                    'status' => $status,
                    'created_by' => $user_id
                );

                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->attendence_setup_model->editAttendenceSetup($data,$id);
                redirect('/registration/attendenceSetup/list');
            }
            $data['programList'] = $this->attendence_setup_model->programListByStatus('1');
            $data['attendenceSetup'] = $this->attendence_setup_model->getAttendenceSetup($id);
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Attendence Setup';
            $this->loadViews("attendence_setup/edit", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
            $results = $this->attendence_setup_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
