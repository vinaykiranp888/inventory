<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamEvent extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_event_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->exam_event_model->examEventListSearch($formData);
            $data['locationList'] = $this->exam_event_model->examCenterLocationList();
            $data['examCenterList'] = $this->exam_event_model->examCenterList();
                // echo "<Pre>";print_r($data['examEventList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Exam Events';
            $this->loadViews("exam_event/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_event.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                // echo "<Pre>";print_r($this->input->post());exit();

                $name = $this->security->xss_clean($this->input->post('name'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();
                
                $data = array(
                   'name' => $name,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->exam_event_model->addExamEvent($data);
                redirect('/registration/examEvent/list');
            }

            $data['locationList'] = $this->exam_event_model->examCenterLocationListByStatus('1');
            $data['examCenterList'] = $this->exam_event_model->examCenterListByStatus('1');
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();


            $this->global['pageTitle'] = 'Inventory Management : Add Exam Event';
            $this->loadViews("exam_event/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_event.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/registration/examEvent/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();
                
                $data = array(
                   'name' => $name,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'status' => $status,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->exam_event_model->editExamEvent($data,$id);
                redirect('/registration/examEvent/list');
            }
            $data['locationList'] = $this->exam_event_model->examCenterLocationListByStatus('1');
            $data['examCenterList'] = $this->exam_event_model->examCenterListByStatus('1');
            
            $data['getExamEvent'] = $this->exam_event_model->getExamEvent($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Exam Event';
            $this->loadViews("exam_event/edit", $this->global, $data, NULL);
        }
    }


    function getCentersByLocatioin($id_location)
    {
            $results = $this->exam_event_model->getCentersByLocatioin($id_location);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
