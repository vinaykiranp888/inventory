<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentSemester extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_semester_model');
        $this->isLoggedIn();
    }

    function add()
    {
        if ($this->checkAccess('student_duration_tagging.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;
                
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $formData = $this->input->post();
                
                $id_student = $this->security->xss_clean($this->input->post('id_student'));



                for($i=0;$i<count($formData['id_student']);$i++)
                {
                    $id_student = $formData['id_student'][$i];

                    if($id_student > 0)
                    {
                        $student = $this->student_semester_model->getStudent($id_student);

                        // echo "<Pre>"; print_r($student);exit;
                        if($student)
                        {
                            $current_semester = $student->current_semester;
                            
                            $next_semester = $current_semester + 1;

                            // $new_current_deliverable = $new_deliverable_month . '-' . $new_deliverable_year;

                            // echo "<Pre>"; print_r($new_current_deliverable);exit;

                            $data = array(
                                'current_semester'=>$next_semester,
                                'updated_by'=>$id_user
                            );

                        // echo "<Pre>"; print_r($data);exit;

                            $updated_student_data = $this->student_semester_model->updateStudent($data,$id_student);

                            if($updated_student_data)
                            {
                                $data_advisor = array(
                                'old_semester'=>$current_semester,
                                'new_semester'=>$next_semester,
                                'id_student'=>$id_student,
                                'status'=> 1,
                                'created_by'=>$id_user
                                );
                                
                        // echo "<Pre>"; print_r($data_advisor);exit;
                                
                                $added_advisor_data = $this->student_semester_model->addNewSemesterHistory($data_advisor);

                                $student = $this->student_semester_model->getStudent($id_student);
                                $id_university = $student->id_university;
                                $partner_university = $this->student_semester_model->getPartnerUniversity($id_university);


                                if($id_university == 1)
                                {
                                    $invoice_generated = $this->student_semester_model->generateNewMainInvoiceForCourseRegistration($id_student);
                                }
                                elseif($partner_university->billing_to == 'Student')
                                {
                                    $invoice_generated = $this->student_semester_model->generateNewMainInvoiceForCourseRegistration($id_student);

                                }     
                            }
                        }
                    }
                }
                
                redirect($_SERVER['HTTP_REFERER']);
            }


            $data['intakeList'] = $this->student_semester_model->intakeListByStatus('1');
            $data['programList'] = $this->student_semester_model->programListByStatus('1');
            $data['semesterList'] = $this->student_semester_model->semesterListByStatus('1');
            $data['staffList'] = $this->student_semester_model->staffListByStatus('1');
            $data['qualificationList'] = $this->student_semester_model->qualificationListByStatus('1');

            // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Student Semester';
            $this->loadViews("student_semester/add", $this->global, $data, NULL);
        }
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $staffList = $this->student_semester_model->staffListByStatus('1');
        
        $student_data = $this->student_semester_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         // $table = "

         // <script type='text/javascript'>
         //     $('select').select2();
         // </script>

         // <h4>Supervisor Tagging For Students</h4>

         // <div class='row'>
         //    <div class='col-sm-4'>
         //        <div class='form-group'>
         //        <label>Select Supervisor </label>
         //        <select name='id_student' id='id_student' class='form-control'>";
         //    $table.="<option value=''>Select</option>";

         //    for($i=0;$i<count($staffList);$i++)
         //    {

         //    // $id = $results[$i]->id_procurement_category;
         //    $id = $staffList[$i]->id;
         //    $type = $staffList[$i]->type;
         //    if($type != '' && $type == 0)
         //    {
         //        $type = 'External';

         //    }elseif($type == 1)
         //    {
         //        $type = 'Internal';
         //    }
         //    $full_name = $staffList[$i]->full_name;
         //    $table.="<option value=".$id.">".$type . " - " . $full_name .
         //            "</option>";

         //    }
         //    $table .="
         //        </select>
         //        </div>
         //      </div>

         //    </div>
         //    ";


         $table = "
         <br>
         <h4> Select Students For Semester Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Current Semester</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Supervisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $type = $student_data[$i]->type;
                $advisor_name = $student_data[$i]->advisor_name;
                $current_semester = $student_data[$i]->current_semester;

                if($type != '' && $type == 0)
                {
                    $type = 'External';

                }elseif($type == 1)
                {
                    $type = 'Internal';
                }

                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>
                    <td>$nric</td>
                    <td>$current_semester</td>
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                           
                    <td>$qualification_name - $qualification_name</td>                           
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$type - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}