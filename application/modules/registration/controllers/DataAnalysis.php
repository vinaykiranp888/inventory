<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DataAnalysis extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('data_analysis_model');
        $this->isLoggedIn();
    }

    function learningCenterNSemester()
    {
        if ($this->checkAccess('data_analysis.learning_center_semester') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $formData['id_university'] = $this->security->xss_clean($this->input->post('id_university'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));

            $data['searchParam'] = $formData;

            $data['learningCenterNSemesterList'] = $this->data_analysis_model->learningCenterNSemesterListSearch($formData);

            $data['programmeList'] = $this->data_analysis_model->progrmmeListByStatus('1');
            $data['intakeList'] = $this->data_analysis_model->intakeListByStatus('1');
            $data['partnerUniversityList'] = $this->data_analysis_model->branchListByStatus('1');
            $data['semesterList'] = $this->data_analysis_model->semesterListByStatus('1');


            // echo "<Pre>";print_r($data['programmeList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Report Course Count';
            ///$this->loadViews("data_analysis/list", $this->global, $data, NULL);
             $this->loadViews("data_analysis/learning_center_semester", $this->global, $data, NULL);
        }
    }

    function view($id_program)
    {
        if ($this->checkAccess('bulk_withdraw.view') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_program == null)
            {
                redirect('/registration/bulkWithdraw/list');
            }
            if($this->input->post())
            {
                
                redirect('/registration/bulkWithdraw/list');
            }


            $data['id_program'] = $id_program;
            $data['analysisList'] = array();
            $data['intakeList'] = $this->data_analysis_model->intakeListByStatus('1');
            $data['courseListByProgrammeIdForLandscapeCourses'] = $this->data_analysis_model->getCourseListByProgrammeIdForLandscapeCourses($id_program);
            
            $data['programme'] = $this->data_analysis_model->getProgram($id_program);

            // $data['courseList'] = $this->data_analysis_model->bulkWithdrawListByIntakeProgrammeStudent($id);

             // echo "<Pre>";print_r($data['courseListByProgrammeIdForLandscapeCourses']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Assigned Student To Exam Center';
            $this->loadViews("data_analysis/view", $this->global, $data, NULL);
        }
    }


    function getProgramSchemeByProgramId()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_program = $tempData['id_program'];
        $id_learning_mode = $tempData['id_learning_mode'];

        $intake_data = $this->data_analysis_model->getProgramSchemeByProgramId($id_program);

            // echo "<Pre>"; print_r($intake_data);exit;
             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_learning_mode' id='id_learning_mode' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $mode_of_program = $intake_data[$i]->mode_of_program;
            $mode_of_study = $intake_data[$i]->mode_of_study;

            $table.="<option value='".$id . "'";
            if($id_learning_mode == $id)
            {
               $table.="selected";
            } $table.= ">". $mode_of_program . " - " .  $mode_of_study .
                    "</option>";

            }
            $table.="</select>";

            echo $table;  
    }

    function getBranchesByPartnerUniversity()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_partner_university = $tempData['id_university'];
        $id_branch = $tempData['id_branch'];

            // echo "<Pre>"; print_r($tempData);exit;
            
            $intake_data = $this->data_analysis_model->getBranchesByPartnerUniversity($id_partner_university);
            

            // echo "<Pre>"; print_r($intake_data);exit;

             $table="
            <script type='text/javascript'>

            $('select').select2();
                            
            </script>


            <select name='id_branch' id='id_branch' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $code = $intake_data[$i]->code;

            // $table.="<option value=".$id.">". $code . " - " .  $name .
            //         "</option>";


            $table.="<option value='".$id . "'";
            if($id_branch == $id)
            {
               $table.="selected";
            }
            $table.= ">". $code . " - " .  $name .
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }
}