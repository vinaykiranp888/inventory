<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_registration_model');
        $this->load->model('setup/programme_model');
        $this->load->model('setup/intake_model');
        $this->load->model('admission/student_model');
        $this->load->model('registration/exam_center_model');
        $this->load->model('registration/course_registration_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_registration.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $data['intakeList'] = $this->course_registration_model->intakeList();
            $data['programList'] = $this->course_registration_model->programList();

                $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
                $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
                $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
                $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
                $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
 

            $data['searchParam'] = $formData;
            

            $data['examRegistrationList'] = $this->exam_registration_model->examRegistrationList($formData);


            $this->global['pageTitle'] = 'Inventory Management : Course Registration';
            $this->loadViews("exam_registration/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_registration.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $courseCheck = $this->security->xss_clean($this->input->post('courseCheck'));
                $courseRegister = $this->security->xss_clean($this->input->post('courseRegister'));
                $exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));


                 $master = array(
                   'id_intake' => $id_intake,
                    'id_programme' => $id_programme,
                    'id_student' => $id_student,
                    'id_semester' => $id_semester,
                    'id_location' => $id_location,
                    'status' => '1',
                    'created_by' => $user_id
                );

                $insert_master_id = $this->exam_registration_model->addNewExamRegister($master);

                if($insert_master_id)
                {

                    for($i=0;$i<count($courseCheck);$i++)
                    {
                        $id_course_register = $courseRegister[$i];

                         $data = array(
                       'id_course' => $courseCheck[$i],
                       'id_intake' => $id_intake,
                       'id_exam' => $insert_master_id,
                        'id_programme' => $id_programme,
                        'id_student' => $id_student,
                        'id_semester' => $id_semester,
                        'id_location' => $id_location,
                        'id_exam_center'=>$exam_center[$i],
                        'status' => '1',
                        'created_by' => $user_id
                    );
                         // echo "<Pre>";print_r($data);exit();
                    $insert_id = $this->exam_registration_model->addExamRegistration($data);
                        if($insert_id)
                        {
                            $update = array(
                                'is_exam_registered' => $insert_id
                            );
                         // echo "<Pre>";print_r($update);exit();
                            $updated = $this->exam_registration_model->editCoureRegistration($update,$id_course_register);
                        }
                    }
                }              

                redirect('/registration/examRegistration/list');
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['studentList'] = $this->student_model->studentList();
            $data['examCenterList'] = $this->exam_center_model->examCenterList();
            $data['examLocationList'] = $this->exam_registration_model->examLocationListByStatus('1');

            $data['semesterList'] = $this->exam_registration_model->semesterListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add Course Registration';
            $this->loadViews("exam_registration/add", $this->global, $data, NULL);
        }
    }


    function edit($id_student, $id_intake, $id_programme, $id)
    {
        if ($this->checkAccess('exam_registration.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_student == null)
            {
                redirect('/registration/examRegistration/list');
            }
            if($this->input->post())
            {
                
                
                redirect('/registration/examRegistration/list');
            }
          
            $data['studentData'] = $this->course_registration_model->getStudentByStudentId($id_student);
            $data['examRegistration'] = $this->exam_registration_model->getExamRegistration($id);
            // $data['courseList'] = $this->exam_registration_model->getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student);
            $data['courseList'] = $this->exam_registration_model->getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student,$id);
            
            // echo "<Pre>";print_r($data['courseList']); exit();

            $this->global['pageTitle'] = 'Inventory Management : Edit Course Registration';
            $this->loadViews("exam_registration/edit", $this->global, $data, NULL);
        }
    }

    function displaydata()
    {
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_location = $this->security->xss_clean($this->input->post('id_location'));
        $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
        $id_student = $this->security->xss_clean($this->input->post('id_student'));

        $temp_details = $this->exam_registration_model->getCourseFromCourseRegistr($id_intake,$id_programme,$id_student);

        // $examDetails = $this->exam_registration_model->getExamRegistrerDetails($id_intake,$id_programme,$id_student);
        // echo "<pre>";print_r($id_location);exit();


        

            $examCenterList = $this->exam_registration_model->examEventListByLocation($id_location);
        // echo "<pre>";print_r($examCenterList);exit();

        $student_data = $this->course_registration_model->getStudentByStudentId($id_student);

        // echo "<Pre>"; print_r($temp_details);exit();


            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;


        // echo $temp_details;
        $table = "



        <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>

                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' /> $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";


        if($temp_details==NULL)
        {
             $table.= "


                <h4 class='sub-title'>No Course Registered With Selected Data</h4>

                ";
        }
        else
        {



                $table.= "


                <h4 class='sub-title'>Registered Course Details</h4>



        <script type='text/javascript'>
            $('select').select2();
        </script>


        <table class='table' id='list-table'>
                  <tr>
                    <th>Select Course</th>
                    <th>Course Name</th>
                    <th>Exam Event & Timings</th>
                </tr>";
                
        
            

                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $mainId = $temp_details[$i]->id_course;
                    $courseName = $temp_details[$i]->course_name;
                    
                        $table.= "
                        <tr>
                            <td>
                                <input type='text' hidden='hidden' name='courseRegister[]' id='courseRegister' value='$id'>
                                <input type='checkbox' name='courseCheck[]' id='courseCheck' value='$mainId'>
                            </td>

                            <td>
                                $courseName
                            </td>

                            <td>
                            <select name='id_exam_center[]' class='form-control' style='width:580px'>
                            <option value=''>select</option>";

                        foreach ($examCenterList as $record)
                        {
                            // Here id_exam_center == id_exam_event Flow Change
                            $table.="<option value=".$record->id.">".$record->name. " - " . date('d-m-Y',strtotime($record->from_dt)) . " - " . $record->from_tm . "
                            </option>";
                        }

                            $table.= "</select>
                            </td>
                        </tr>";
                    }
                $table.= "</table>";

        }
        
        echo $table;
    }
}
