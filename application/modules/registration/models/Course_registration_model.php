<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Course_registration_model extends CI_Model
{
    function courseRegistrationList($applicantList)
    {
        $this->db->select('DISTINCT(a.id_student) as id_student, std.nric, std.email_id, in.id as id_intake, in.name as intake, p.id as id_programme, p.code as program_code, p.name as program, std.full_name, qs.name as education_level');
        $this->db->from('course_register as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->join('education_level as qs', 'std.id_degree_type = qs.id','left'); 
        if($applicantList['first_name']) {
            $likeCriteria = "(std.full_name  LIKE '%" . $applicantList['first_name'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['email_id']) {
            $likeCriteria = "(std.email_id  LIKE '%" . $applicantList['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['nric']) {
            $likeCriteria = "(std.nric  LIKE '%" . $applicantList['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_program']) {
            $likeCriteria = "(std.id_program  LIKE '%" . $applicantList['id_program'] . "%')";
            $this->db->where($likeCriteria);
        }
         if($applicantList['id_intake']) {
            $likeCriteria = "(std.id_intake  LIKE '%" . $applicantList['id_intake'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('qs.name !=', 'POSTGRADUATE');
         // $this->db->where('a.is_bulk_withdraw', '0');
         // $this->db->where('a.is_exam_registered', '0');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function programListNotForPostgraduate($name)
    {
        $this->db->select('p.*');
        $this->db->from('programme as p');
        $this->db->join('education_level as el', 'p.id_education_level = el.id');
        $this->db->where('el.name !=', $name);
        $this->db->where('p.status', '1');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getPartnerUniversity($id)
    {
        $this->db->select('d.*, pc.name as partner_category');
        $this->db->from('partner_university as d');
        $this->db->join('partner_category as pc','d.id_partner_category = pc.id');
        $this->db->where('d.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function examCenterListSearch($search)
    {
        // $date = 
        $this->db->select('*');
        $this->db->from('exam_center');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getPerogramLandscape($data)
    {
        $this->db->select('c.id, a.id_program_landscape, a.pre_requisite, a.id as id_course_registered_landscape, c.name as courseName, c.code as course_code, pl.name as plName,c.credit_hours, a.course_type, a.id_semester');
        $this->db->from('add_course_to_program_landscape as a');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'a.id_program_landscape = pl.id');
        $this->db->where('a.id_intake', $data['id_intake']);
        $this->db->where('pl.id_programme', $data['id_programme']);
        $this->db->where('pl.id', $data['id_program_landscape']);
        // $this->db->where('a.id_program_scheme', $data['id_learning_mode']);
        // $this->db->where('pl.program_scheme', $data['id_program_scheme']);
        $this->db->where('a.id_semester <=', $data['current_semester']);


        $likeCriteria = " a.id NOT IN (SELECT cr.id_course_registered_landscape FROM course_registration cr where cr.id_student = " . $data['id_student'] . ")";
        $this->db->where($likeCriteria);  

        $query = $this->db->get();
        $result = $query->result();
        // echo "<Pre>"; print_r($query);exit();
        return $result;
    }

    function getCoursePerogramLandscape($id)
    {
        $this->db->select('a.id, b.pre_requisite, c.name as courseName, pl.name as plName, pl.min_total_cr_hrs');

        $this->db->from('courses_from_programme_landscape as a');
        $this->db->join('add_course_to_program_landscape as b', 'a.id_course_programme = b.id');
        $this->db->join('course as c', 'b.id_course = c.id');
        $this->db->join('programme_landscape as pl', 'b.id_program_landscape = pl.id');
        $this->db->where('a.id_course_registration', $id);
        $query = $this->db->get();
         $result = $query->result();
         return $result; 
    }

    function getCourseRegistrationList($id)
    {
        $this->db->select('*');
        $this->db->from('course_registration');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    function getCourseRegistered($id)
    {

         $this->db->select('*');
        $this->db->from('course_register');
        $this->db->where('id', $id);
        $query = $this->db->get();
         return $query->row();


        //  $this->db->select('a.*, c.name as course_name, c.code as course_code,c.credit_hours, in.year as intake_year, in.name as intake_name, p.code as program_code, p.name as program_name, sem.code as semester_code, sem.name as semester_name');
        // $this->db->from('course_register as a');
        // $this->db->join('intake as in', 'a.id_intake = in.id');
        // $this->db->join('programme as p', 'a.id_programme = p.id');
        // $this->db->join('course as c', 'a.id_course = c.id');
        // $this->db->join('semester as sem', 'a.id_semester = sem.id');
        // $this->db->where('a.id', $id);
        // $query = $this->db->get();
        //  return $query->row();
    }

    function studentList($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addCoureRegister($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_register', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addCoureRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('course_registration', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCoureRegistration($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_registration', $data);
        return TRUE;
    }

    function addCourseList($array, $insert_id)
    {
        foreach ($array as $row)
        {
          $data = ['id_course_programme'=>$row];
          $this->db->insert('courses_from_programme_landscape', $data);
        }
        $data = ['id_course_registration'=>$insert_id];
        $this->db->where_in('id_course_programme', $array);
        $this->db->update('courses_from_programme_landscape', $data);
        return TRUE;
    }

    function getIntakeListByProgramme($id_programme)
    {
        $this->db->select('DISTINCT(ihp.id_intake) as id_intake, ihp.*, in.name as intake_name, in.year as intake_year');
        $this->db->from('intake_has_programme as ihp');
        $this->db->join('intake as in', 'ihp.id_intake = in.id');
        $this->db->where('ihp.id_programme', $id_programme);
        $query = $this->db->get();
         return $query->result();
    }

    function getStudentByProgNIntake($data)
    {
        $this->db->select('DISTINCT(er.id) as id, er.*, qs.name as qualificatoin_level');
        $this->db->from('student as er');
        $this->db->join('education_level as qs', 'er.id_degree_type = qs.id','left'); 
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        if ($data['id_learning_mode'] != '')
        {
            $this->db->where('er.id_program_scheme', $data['id_learning_mode']);
        }
        if ($data['id_program_has_scheme'] != '')
        {
            $this->db->where('er.id_program_has_scheme', $data['id_program_has_scheme']);
        }
        if ($data['id_degree_type'] != '')
        {
            $this->db->where('er.id_degree_type', $data['id_degree_type']);
        }
        $this->db->where('qs.name', 'POSTGRADUATE');
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    // function getStudentByStudentId($id_student)
    // {
    //     $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.id as id_intake, i.name as intake_name');
    //     $this->db->from('student as s');
    //     $this->db->join('programme as p', 's.id_program = p.id'); 
    //     $this->db->join('intake as i', 's.id_intake = i.id'); 
    //     $this->db->where('s.id', $id_student);
    //     $query = $this->db->get();
    //     $result = $query->row(); 

    //     return$result;
    // }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, i.name as intake_name, st.ic_no, st.name as advisor_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, brch.code as branch_code, brch.name as branch_name, salut.name as salutation, pu.code as partner_university_code, pu.name as partner_university_name, sch.code as scheme_code, sch.description as scheme_name, qs.name as qualification, pl.name as programme_landscape, pl.program_landscape_type');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('organisation_has_training_center as brch', 's.id_branch = brch.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left'); 
        $this->db->join('partner_university as pu', 's.id_university = pu.id','left'); 
        $this->db->join('scheme as sch', 's.id_program_has_scheme = sch.id','left'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('state as ms', 's.mailing_state = ms.id'); 
        $this->db->join('country as mc', 's.mailing_country = mc.id');
        $this->db->join('state as ps', 's.permanent_state = ps.id'); 
        $this->db->join('country as pc', 's.permanent_country = pc.id'); 
        $this->db->join('race_setup as rs', 's.id_race = rs.id'); 
        $this->db->join('religion_setup as rels', 's.religion = rels.id','left');
        $this->db->join('staff as st', 's.id_advisor = st.id','left'); 
        $this->db->join('education_level as qs', 's.id_degree_type = qs.id','left'); 
        $this->db->join('programme_landscape as pl', 's.id_program_landscape = pl.id','left'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getCourseRegisteredLandscapeBySemesterForDisplay($id_program,$id_intake,$id_program_landscape,$id_student)
    {
            // echo "<Pre>";print_r($results);exit;
        $this->db->select('DISTINCT(mi.id_semester) as id_semester');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $query = $this->db->get();
        $results = $query->result();

           // echo "<Pre>";print_r($results);exit;
        $details = array();
        foreach ($results as $result)
        {

            $semester = $this->getSemester($result->id_semester);
            $course_data = $this->getProgramLandscapeDetails($id_program,$id_intake,$id_student,$result->id_semester);

            $data = $semester;
            $data->course = $course_data;

            array_push($details, $data);
        }
            // echo "<Pre>";print_r($details);exit;

        return $details;
    }

    function getSemester($id_semester)
    {
        $this->db->select('mi.*');
        $this->db->from('semester as mi');
        $this->db->where('mi.id', $id_semester);
        $query = $this->db->get();
        $result = $query->row();

        return $result; 
    }

    function getProgramLandscapeDetails($id_program,$id_intake,$id_student,$id_semester)
    {
        $this->db->select('DISTINCT(mi.id_course) as id_course, mi.id, mi.student_current_semester, mi.created_dt_tm');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id_student', $id_student);
        $this->db->where('mi.id_programme', $id_program);
        $this->db->where('mi.id_intake', $id_intake);
        $this->db->where('mi.id_semester', $id_semester);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();

        foreach ($results as $value)
        {
           $value->is_exam_registered = 0;
           $value->is_result_announced = 0;
           $value->is_bulk_withdraw = 0;
           $value->total_result = 0;
           $value->grade = '';


            $course = $this->getCourse($value->id_course);
            $course_registered = $this->getCourseRegisteredDetailsForLandscape($value->id,$id_student);

            if($course_registered)
           {
               $value->is_exam_registered = $course_registered->is_exam_registered;
               $value->is_result_announced = $course_registered->is_result_announced;
               $value->is_bulk_withdraw = $course_registered->is_bulk_withdraw;
               $value->total_result = $course_registered->total_result;
               $value->grade = $course_registered->grade;
           }

           if($course)
           {
               $value->course_code = $course->code;
               $value->course_name = $course->name;
               $value->credit_hours = $course->credit_hours;
           }

           // echo "<Pre>";print_r($value);exit;


            array_push($details, $value);
        }

        return $details;
    }

    function getCourseRegisteredDetailsForLandscape($id,$id_student)
    {
        $this->db->select('mi.*');
        $this->db->from('course_registration as mi');
        $this->db->where('mi.id', $id);
        $this->db->where('mi.id_student', $id_student);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }


    function getOrganisation()
    {
        $this->db->select('fc.*');
        $this->db->from('organisation as fc');
        $this->db->where('fc.status', 1);
        $this->db->order_by("fc.id", "DESC");
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }


    function getProgrammeListByStudentId($id_student)
    {
        $this->db->select(' DISTINCT(a.id_programme) as id, p.code as program_code, p.name as program, std.full_name');
        $this->db->from('course_registration as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->where('a.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getIntakeListByStudentId($id_student)
    {
        $this->db->select(' DISTINCT(a.id_intake) as id, in.name as intake_name');
        $this->db->from('course_registration as a');
        $this->db->join('intake as in', 'a.id_intake = in.id');
        $this->db->join('programme as p', 'a.id_programme = p.id');
        $this->db->join('student as std', 'a.id_student = std.id');
        $this->db->where('a.id_student', $id_student);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getCourseRegistrationByMasterId($id)
    {
        //  // print_r($id_intake);exit();
        // $this->db->select('a.by_student, stu.id as id_student, stu.full_name, stu.nric, stu.gender, stu.email_id, stu.phone, rl.name as role');
        // $this->db->from('course_registration as a');
        // $this->db->join('student as stu', 'a.id_student = stu.id');
        // $this->db->join('users as rl', 'a.created_by = rl.id','left');
        // $this->db->where('a.id_course_register', $id);
        // // $this->db->where('a.is_exam_registered', '0');
        // // $this->db->where('a.is_bulk_withdraw', '0');
        //  $query = $this->db->get();
        //  $result = $query->result();
        //  return $result;

         // print_r($id_intake);exit();
        $this->db->select('a.*, rl.name as role');
        $this->db->from('course_registration as a');
        // $this->db->join('course as cou', 'a.id_course = cou.id');
        // $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
        // $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('users as rl', 'a.created_by = rl.id','left');
        $this->db->where('a.id_course_register', $id);
        // $this->db->where('a.is_exam_registered', '0');
        // $this->db->where('a.is_bulk_withdraw', '0');
         $query = $this->db->get();
         $results = $query->result();

         $details = array();
         foreach ($results as $result)
         {
            $id_course_registered_landscape = $result->id_course_registered_landscape;
            $course = $this->getCourseRegisteredLandscape($id_course_registered_landscape);
            if($course)
            {
                $result->course_code = $course->code;
                $result->course_name = $course->name;
                $result->credit_hours = $course->credit_hours;
                $result->pre_requisite = $course->pre_requisite;
                $result->course_type = $course->course_type;

                array_push($details, $result);
            }
         }

         return $details;
    }

    function getCoursesByProgramNIntakeNStudent($id_intake,$id_programme,$id_student,$id_semester)
    {
         // print_r($id_intake);exit();
        $this->db->select('a.by_student, cou.id as id_course, cou.name, cou.name_in_malay, cou.code, acpl.pre_requisite, cou.credit_hours, rl.name as role');
        $this->db->from('course_registration as a');
        $this->db->join('course as cou', 'a.id_course = cou.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
        $this->db->join('programme_landscape as pl', 'acpl.id_program_landscape = pl.id');
        $this->db->join('users as rl', 'a.created_by = rl.id','left');
        $this->db->where('pl.id_programme', $id_programme);
        $this->db->where('pl.id_intake', $id_intake);
        $this->db->where('a.id_semester', $id_semester);
        $this->db->where('a.id_programme', $id_programme);
        $this->db->where('a.id_intake', $id_intake);
        $this->db->where('a.id_student', $id_student);
        $this->db->where('a.is_exam_registered', '0');
        $this->db->where('a.is_bulk_withdraw', '0');
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function courseList()
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getCoursesIdByLandscapeIdForDetailsAdd($id)
    {
        $this->db->select('s.*');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('programme as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function semesterListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('semester as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('s.*');
        $this->db->from('intake as s');
        $this->db->where('s.status', $status);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function studentListByStatus()
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

    function semesterDetails($id)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureMaster($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function generateNewMainInvoiceForCourseRegistration($datas,$id_student,$id_semester)
    {

        // echo "<Pre>";print_r($id_semester);exit();
        
        $user_id = $this->session->userId;
        $student_data = $this->getStudent($id_student);

        // echo "<Pre>";print_r($student_data);exit();

        $id_student = $student_data->id;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;
        $nationality = $student_data->nationality;
        $id_program_scheme = $student_data->id_program_scheme;
        $id_program_has_scheme = $student_data->id_program_has_scheme;
        $id_university = $student_data->id_university;
        $id_branch = $student_data->id_branch;
        $current_semester = $student_data->current_semester;
        $id_program_landscape = $student_data->id_program_landscape;
        $id_fee_structure = $student_data->id_fee_structure;
        $student_current_semester = $student_data->current_semester;

        // echo "<Pre>";print_r($student_data);exit();

        $get_data['id_intake'] = $id_intake;
        $get_data['id_programme'] = $id_program;
        $get_data['id_programme_scheme'] = $id_program_scheme;
        $get_data['id_program_has_scheme'] = $id_program_has_scheme;

        // $programme_landscape = $this->getProgramLandscapeByData($get_data);


        if($id_fee_structure != 0)
        {
            // echo "<Pre>";print_r($id_branch);exit();

            $trigger = 'COURSE REGISTRATION';
            $is_installment = 0;
            $installments = 0;

            // Hided university == 1 && For Per Semester Based Fee Generation 
                // $id_university == 1 && 
            

            // if($id_university == 1)
            // {


                $fee_structure_master = $this->getFeeStructureMaster($id_fee_structure);
                $id_currency = $fee_structure_master->id_currency;


                if($nationality == '1')
                {
                    $currency = 'MYR';

                    // $fix_amount_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme,'MYR','FIX AMOUNT');
                    $fix_amount_data = $this->getFeeStructure($id_fee_structure,'MYR','FIX AMOUNT',$id_university,$trigger);
                    $subject_multiplier_amount_data = $this->getFeeStructure($id_fee_structure,'MYR','SUBJECT MULTIPLICATION',$id_university,$trigger);
                    $cr_hr_multiflier_amount_data = $this->getFeeStructure($id_fee_structure,'MYR','CREDIT HOUR MULTIPLICATION',$id_university,$trigger);
                }
                elseif($nationality != '')
                {
                    $currency = 'USD';
                    $fix_amount_data = $this->getFeeStructure($id_fee_structure,'USD','FIX AMOUNT',$id_university,$trigger);
                    $subject_multiplier_amount_data = $this->getFeeStructure($id_fee_structure,'USD','SUBJECT MULTIPLICATION',$id_university,$trigger);
                    $cr_hr_multiflier_amount_data = $this->getFeeStructure($id_fee_structure,'USD','CREDIT HOUR MULTIPLICATION',$id_university,$trigger);
                }



                // echo "<Pre>";print_r($fix_amount_data);exit();


                if(isset($fix_amount_data) || isset($subject_multiplier_amount_data) || isset($cr_hr_multiflier_amount_data))
                {

                    // echo "<Pre>";print_r($cr_hr_multiflier_amount_data);exit;

                
                    $invoice_number = $this->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'Student';
                    $invoice['remarks'] = 'Student Course Registration';
                    $invoice['id_application'] = '';
                    $invoice['id_student'] = $id_student;
                    $invoice['id_program'] = $id_program;
                    $invoice['id_intake'] = $id_intake;
                    $invoice['currency'] = $id_currency;
                    $invoice['invoice_total'] = 0;
                    $invoice['total_amount'] = 0;
                    $invoice['balance_amount'] = 0;
                    $invoice['paid_amount'] = 0;
                    $invoice['status'] = '1';
                    $invoice['created_by'] = $user_id;
                    
                    // echo "<Pre>";print_r($detail_data);exit;
                    $inserted_id = $this->addNewMainInvoice($invoice);



                    $total = 0;


                    if($fix_amount_data)
                    {
                        $detail_data = array(
                            'id_main_invoice' => $inserted_id,
                            'id_fee_item' => $fix_amount_data->id_fee_item,
                            'amount' => $fix_amount_data->amount,
                            'price' => $fix_amount_data->amount,
                            'quantity' => 1,
                            'description' => 'FIX AMOUNT',
                            'status' => '1',
                            'created_by' => $user_id
                        );

                        $this->addNewMainInvoiceDetails($detail_data);

                        $total = $total + $fix_amount_data->amount;
                    }

                    if($subject_multiplier_amount_data)
                    {
                        $count = count($datas);

                        $amount = $count * $subject_multiplier_amount_data->amount;

                        $detail_data = array(
                            'id_main_invoice' => $inserted_id,
                            'id_fee_item' => $subject_multiplier_amount_data->id_fee_item,
                            'amount' => $amount,
                            'price' => $subject_multiplier_amount_data->amount,
                            // 'quantity' => 1,
                            'quantity' => $count,
                            'description' => 'SUBJECT MULTIPLICATION',
                            'status' => '1',
                            'created_by' => $user_id
                        );

                        $this->addNewMainInvoiceDetails($detail_data);
                        $total = $total + $amount;

                    }


                    if($cr_hr_multiflier_amount_data)
                    {
                        foreach ($datas as $data)
                        {
                           
                        
                            $id_course = $data['id_course'];
                            $id_course_registered = $data['id_course_registered'];

                            $course_data = $this->getCourse($id_course);

                            $credit_hours = $course_data->credit_hours;

                            $detail_total = $credit_hours * $cr_hr_multiflier_amount_data->amount;
                       
                            $data = array(
                                    'id_main_invoice' => $inserted_id,
                                    'id_fee_item' => $cr_hr_multiflier_amount_data->id_fee_item,
                                    'amount' => $detail_total,
                                    'price' => $cr_hr_multiflier_amount_data->amount,
                                    'quantity' => 1,
                                    'id_reference' => $id_course_registered,
                                    'description' => 'CREDIT HOUR MULTIPLICATION',
                                    'status' => '1',
                                    'created_by' => $user_id
                                );

                            $total = $total + $detail_total;

                            $this->addNewMainInvoiceDetails($data);

                        }
                    }

                    $invoice_update['total_amount'] = $total;
                    $invoice_update['balance_amount'] = $total;
                    $invoice_update['invoice_total'] = $total;
                    $invoice_update['total_discount'] = 0;
                    $invoice_update['paid_amount'] = 0;
                    // $invoice_update['inserted_id'] = $inserted_id;
                    // echo "<Pre>";print_r($invoice_update);exit;
                    $this->editMainInvoice($invoice_update,$inserted_id);

                }


            // }



            // echo "<Pre>";print_r($installments);exit();

            // if($id_university > 1)
            // {
            //     $currency = 'USD';

            //     $fee_structure_data = $this->getFeeStructureByTrainingCenterForInvoiceGeneration($id_program,$id_intake,$id_fee_structure,$id_university,'COURSE REGISTRATION');

            //     // $fee_structure_training_data = $this->getFeeStructureInstallmentByDataNSemester($id_programme_landscape,$id_university,$current_semester);

            //     // $count_fee_struture = count($fee_structure_data);

            //     // echo "<Pre>";print_r($count_fee_struture);exit();

            //     $count_installment_fee = 0;
            //     $count_per_semester_fee = 0;

            //     foreach ($fee_structure_data as $fee_structure)
            //     {
            //         $currency = $fee_structure->currency;
            //         $is_installment = $fee_structure->is_installment;
            //         $id_training_center = $fee_structure->id_training_center;
            //         $trigger_name = $fee_structure->trigger_name;
            //         $installments = $fee_structure->installments;

            //         $instllment_data['id_fee_structure'] = $fee_structure->id;
            //         $instllment_data['trigger_code'] = 'COURSE REGISTRATION';
            //         $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;


            //         if($is_installment == 1)
            //         {
            //             $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

            //             if($installment_details)
            //             {
            //                 foreach ($installment_details as $installment_detail)
            //                 {
                                
            //                     $installment_trigger_name = $installment_detail->trigger_name;
            //                     $trigger_semester = $installment_detail->id_semester;


            //                     if($installment_trigger_name == 'COURSE REGISTRATION' && $trigger_semester == $student_current_semester)
            //                     {
            //                         $count_installment_fee ++;   
            //                     }
            //                 }
            //             }
            //         }
            //         else
            //         {
            //             if($trigger_name == 'COURSE REGISTRATION')
            //             {
            //                 $count_per_semester_fee ++;    
            //             }
            //         }
            //     }

            //         // echo "<Pre>";print_r($count_per_semester_fee . '1' . $count_installment_fee);exit();
            //         // echo "<Pre>";print_r('1');exit();

            //     if(!empty($fee_structure_data) && ($count_per_semester_fee > 0 || $count_installment_fee > 0))
            //     {

            //         $amount = 0;

            //         $invoice_number = $this->generateMainInvoiceNumber();

            //         $invoice['invoice_number'] = $invoice_number;
            //         $invoice['type'] = 'Student';
            //         $invoice['remarks'] = 'Student Course Registration Fee';
            //         $invoice['id_application'] = '';
            //         $invoice['id_student'] = $id_student;
            //         $invoice['id_program'] = $id_program;
            //         $invoice['id_intake'] = $id_intake;
            //         $invoice['currency'] = $currency;
            //         $invoice['invoice_total'] = $amount;
            //         $invoice['total_amount'] = $amount;
            //         $invoice['balance_amount'] = $amount;
            //         $invoice['paid_amount'] = 0;
            //         $invoice['status'] = '1';
            //         $invoice['created_by'] = $user_id;
                    
            //         // echo "<Pre>";print_r($detail_data);exit;
            //         $inserted_id = $this->addNewMainInvoice($invoice);


            //         if($inserted_id)
            //         {
            //             $invoice_total_amount = 0;

            //             foreach ($fee_structure_data as $fee_structure)
            //             {
            //                 $is_installment = $fee_structure->is_installment;
            //                 $id_training_center = $fee_structure->id_training_center;
            //                 $trigger_name = $fee_structure->trigger_name;
            //                 $installments = $fee_structure->installments;

            //                 $instllment_data['id_fee_structure'] = $fee_structure->id;
            //                 $instllment_data['trigger_code'] = 'COURSE REGISTRATION';
            //                 $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;



            //                 $total_amount = 0;

            //                 if($is_installment == 1)
            //                 {

            //                     $installment_details = $this->getTrainingCenterInstallmentDetails($instllment_data);

            //                     if($installment_details)
            //                     {
            //                         foreach ($installment_details as $installment_detail)
            //                         {
                                        
            //                             $installment_trigger_name = $installment_detail->trigger_name;
            //                             $trigger_semester = $installment_detail->id_semester;


            //                             if($installment_trigger_name == 'COURSE REGISTRATION' && $trigger_semester == $student_current_semester)
            //                             {

            //                                 $data = array(
            //                                     'id_main_invoice' => $inserted_id,
            //                                     'id_fee_item' => $installment_detail->id_fee_item,
            //                                     'amount' => $installment_detail->amount,
            //                                     'price' => $installment_detail->amount,
            //                                     'quantity' => 1,
            //                                     'id_reference' => $installment_detail->id,
            //                                     'description' => 'Student Course Registration Installment Trigger Fee',
            //                                     'status' => 1,
            //                                     'created_by' => $user_id
            //                                 );

            //                                 $total_amount = $total_amount + $installment_detail->amount;
                            
            //                                 $this->addNewMainInvoiceDetails($data);
            //                             }
            //                         }
            //                     }
            //                 }
            //                 else
            //                 {
            //                     if($trigger_name == 'COURSE REGISTRATION')
            //                     {
            //                         $data = array(
            //                             'id_main_invoice' => $inserted_id,
            //                             'id_fee_item' => $fee_structure->id_fee_item,
            //                             'amount' => $fee_structure->amount,
            //                             'price' => $fee_structure->amount,
            //                             'quantity' => 1,
            //                             'id_reference' => $fee_structure->id,
            //                             'description' => 'Student Course Registration Per Semester Trigger Fee',
            //                             'status' => 1,
            //                             'created_by' => $user_id
            //                         );

            //                         $total_amount = $total_amount + $fee_structure->amount;

            //                         $this->addNewMainInvoiceDetails($data);
            //                     }
            //                 }

            //                 $invoice_total_amount = $invoice_total_amount + $total_amount;
            //             }



            //             $invoice_update['total_amount'] = $invoice_total_amount;
            //             $invoice_update['balance_amount'] = $invoice_total_amount;
            //             $invoice_update['invoice_total'] = $invoice_total_amount;
            //             $invoice_update['total_discount'] = 0;
            //             $invoice_update['paid_amount'] = 0;

            //             $updated_invoice = $this->editMainInvoice($invoice_update,$inserted_id);
            //         }

            //     }

            // }

        }
        return TRUE;
    }

    function getFeeStructureByTrainingCenterForInvoiceGeneration($id_programme,$id_intake,$id_fee_structure,$id_training_center,$trigger_code)
    {
        $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id','left');
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_landscape', $id_fee_structure);
        $this->db->where('p.id_training_center', $id_training_center);
        $query = $this->db->get();
        $fee_structures = $query->result();

        $details = array();

        foreach ($fee_structures as $fee_structure)
        {
            $is_installment = $fee_structure->is_installment;
            $trigger_name = $fee_structure->trigger_name;

            if($is_installment == 0)
            {
                if($trigger_name == $trigger_code)
                {
                    array_push($details, $fee_structure);
                }
            }
            else
            {
                array_push($details, $fee_structure);
            }
        }

        return $details;
    }

    function getTrainingCenterInstallmentDetails($data)
    {
        $this->db->select('p.*, sem.name as fee_name, sem.code as fee_code, fm.name as frequency_mode, fstp.name as trigger_name');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_setup as sem', 'p.id_fee_item = sem.id');
        $this->db->join('frequency_mode as fm', 'sem.id_frequency_mode = fm.id');
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id');
        $this->db->where('p.id_fee_structure', $data['id_fee_structure']);
        $this->db->where('p.id_program_landscape', $data['id_fee_structure_master']);
        $query = $this->db->get(); 
        $results = $query->result();  
        
        $details = array();

        foreach ($results as $result)
        {
            $trigger_name = $result->trigger_name;

            if($trigger_name == $data['trigger_code'])
            {
                array_push($details, $result);
            }
            
        }
        return $details;
    }


    function getProgramLandscapeByData($data)
    {
        $this->db->select('pl.*');
        $this->db->from('programme_landscape as pl');
        $this->db->where('pl.id_intake', $data['id_intake']);
        $this->db->where('pl.id_programme', $data['id_programme']);
        $this->db->where('pl.learning_mode', $data['id_programme_scheme']);
        $this->db->where('pl.program_scheme', $data['id_program_has_scheme']);
        $this->db->order_by('pl.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row();
        return $result; 
    }


    function getFeeStructureInstallmentByDataNSemester($id_programme_landscape,$id_training_center,$current_semester)
    {
        $this->db->select('p.*, cs.name as currency');
        $this->db->from('fee_structure_has_training_center as p');
        $this->db->join('fee_structure as fs', 'p.id_fee_structure = fs.id'); 
        $this->db->join('currency_setup as cs', 'fs.currency = cs.id'); 
        $this->db->where('p.id_program_landscape', $id_programme_landscape);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('p.id_semester', $current_semester);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }


    function getFeeStructure($id_program_landscape,$currency,$code,$id_training_center,$trigger_name)
    {
       $this->db->select('p.*, fstp.name as trigger_name');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id'); 
        $this->db->join('amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id'); 
        $this->db->join('fee_structure_triggering_point as fstp', 'p.id_fee_structure_trigger = fstp.id'); 
        // $this->db->where('p.id_programme', $id_programme);
        // $this->db->where('p.id_intake', $id_intake);
        // $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $this->db->where('p.currency', $currency);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('fstp.name', $trigger_name);
        $this->db->where('fm.code', 'PER SEMESTER');
        $this->db->where('amt.code', $code);
        $this->db->order_by('p.id', 'DESC');
        $query = $this->db->get();
        $fee_structure = $query->row();
        return $fee_structure;
    }

    function getFeeStructureForTrainingCenterInstallment($id_programme,$id_intake,$id_program_scheme,$id_training_center,$id_program_landscape)
    {
       $this->db->select('p.*');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id','left'); 
        $this->db->join('frequency_mode as fm', 'fs.id_frequency_mode = fm.id','left'); 
        $this->db->where('p.id_programme', $id_programme);
        $this->db->where('p.id_intake', $id_intake);
        $this->db->where('p.id_program_scheme', $id_program_scheme);
        $this->db->where('p.id_training_center', $id_training_center);
        $this->db->where('p.id_program_landscape', $id_program_landscape);
        $query = $this->db->get();
        $fee_structure = $query->row();

        return $fee_structure;
    }



    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $applicant_data = $query->row();

        return $applicant_data;
    }

    function getCourse($id)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $course_data = $query->row();

        return $course_data;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }

    function getCourseLandscapeByByProgNIntake($data)
    {
        $this->db->select('DISTINCT(s.id_course) as id_course, s.id as id_course_registered_landscape');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.id_program', $data['id_programme']);
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
           $id_course = $result->id_course;
           $course = $this->getCourse($id_course);
           if($course)
           {

            $course->id_course_registered_landscape = $result->id_course_registered_landscape;
            array_push($details, $course);
           }
        }
        return $details;
    }

    function getCourseRegisteredLandscape($id)
    {
        $this->db->select('c.*, s.course_type, s.course_type, s.pre_requisite');
        $this->db->from('add_course_to_program_landscape as s');
        $this->db->join('course as c', 's.id_course = c.id'); 
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getStudentListByCourseRegistrationData($data)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id_intake', $data['id_intake']);
        $this->db->where('s.id_program', $data['id_programme']);
        // $this->db->where('a.id_course_registered_landscape', $data['id_course_registered_landscape']);
        // $this->db->where('a.is_exam_registered !=', 0);
        // $this->db->where('a.is_bulk_withdraw', 0);

        if($data['name'])
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'])
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['email_id'])
        {
            $likeCriteria = "(s.email_id  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }

        $likeCriteria = "s.id NOT IN (SELECT a.id_student FROM course_registration as a WHERE a.id_programme =" . $data['id_programme'] . " AND a.id_intake =" . $data['id_intake'] ." AND a.id_course_registered_landscape = " . $data['id_course_registered_landscape'] . ")";
        
        $this->db->where($likeCriteria);  

         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();
         return $result;
    }

    function getProgramSchemeByProgramId($id_programme)
    {
        $this->db->select('DISTINCT(ihs.mode_of_program) as mode_of_program, ihs.*');
        $this->db->from('programme_has_scheme as ihs');
        $this->db->where('ihs.id_program', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getSchemeByProgramId($id_programme)
    {
        // echo "<Pre>"; print_r($id_programme);exit;
        $this->db->select('DISTINCT(phs.id_scheme) as id_scheme');
        $this->db->from('program_has_scheme as phs');
        $this->db->join('scheme as sch', 'phs.id_scheme = sch.id');
        $this->db->where('phs.id_program', $id_programme);
        $query = $this->db->get();
        $results = $query->result();
            
        // echo "<Pre>"; print_r($results);exit;

        $details = array();

        foreach ($results as $result)
        {

            $id_scheme = $result->id_scheme;

            $scheme = $this->getScheme($id_scheme);
            if($scheme)
            {
                $result = $scheme;
                array_push($details, $result);
            }
        }

        return $details;
    }

    function getScheme($id_scheme)
    {
        $this->db->select('ihs.*');
        $this->db->from('scheme as ihs');
        $this->db->where('ihs.id', $id_scheme);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgrammeByEducationLevelId($id_education_level)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        // $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_education_level', $id_education_level);
        $this->db->where('ihs.status', 1);
        $query = $this->db->get();
        return $query->result();
    }

     function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function displayStudentsForCourseRegistrationAttendance($data)
    {
        $this->db->select('DISTINCT(a.id_student) as id_student, a.id as id_course_registration');
        $this->db->from('course_registration as a');
        $this->db->join('course as cou', 'a.id_course = cou.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'acpl.id_course = cou.id');
        $this->db->join('student as stu', 'a.id_student = stu.id');
        $this->db->join('course_register as cr', 'a.id_course_register = cr.id');
        $this->db->where('a.id_course_registered_landscape', $data['id_course_registered_landscape']);
        $this->db->where('a.id_programme', $data['id_programme']);
        $this->db->where('a.id_intake', $data['id_intake']);
        $this->db->where('cr.id_education_level', $data['id_education_level']);
        // $this->db->where('a.id_program_scheme', $data['id_program_scheme']);
        // $this->db->where('a.id_program_has_scheme', $data['id_program_has_scheme']);

        $query = $this->db->get();
        $results =  $query->result();

        $details = array();

        foreach ($results as $result)
        {
            $id_course_registration = $result->id_course_registration;

            $id_student = $result->id_student;

            $student = $this->getStudent($id_student);
            if($student)
            {
                $student->id_course_registration = $id_course_registration;
                array_push($details, $student);
            }
        }

        return $details;
    }
}