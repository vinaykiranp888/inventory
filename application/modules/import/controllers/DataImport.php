<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DataImport extends BaseController
{
    public function __construct()
    {
   		// echo "<pre>";print_r($con); exit;
        parent::__construct();
        // $this->load->model('company_registration_model');
    }


    //Get Connection To the Database
    function getConnection()
    {
    	$base_url = $_SERVER['HTTP_HOST'];

		 // echo $base_url;exit;
		 
		 if($base_url == "eag.com")
		 {
		 	$con  = mysqli_connect('localhost', 'root', 'root', 'college');
		 }
		 else
		 {
		 	$con  = mysqli_connect('localhost', 'camsedu_collegeuser', 'Ctemp098$#', 'camsedu_eag');
		 }

		 // Check connection
		if (mysqli_connect_errno())
		{
		  	echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}

		return $con;
    }


    // Import Course Data

    function importCourseData()
    {
	  	$csvFile = file('course.csv');

		// $sql = "TRUNCATE table student";
		//    $result = $con->query($sql);

		for($i=0;$i<count($csvFile);$i++)
		{
			$FirstArray = explode(",",$csvFile[$i]);

			echo "<pre>";print_r($FirstArray);exit;
			$last_inserted_id = $this->insertCourseData($FirstArray);

			echo "Succesfully Imported Course : " . $last_inserted_id . " :- <br>";

			 // continue;
		}
    }

    function insertCourseData($FirstArray)
	{

	  	$course_name = $FirstArray[0];
		$course_code = $FirstArray[1];
		$faculty = $FirstArray[2];
		$credit_hours = $FirstArray[3];

		$id_faculty = $this->getFacultyByName($faculty);
		// echo $id_faculty;die();

		$con = $this->getConnection();

	  	$sql = "INSERT INTO `course` (`id`, `name`, `name_in_malay`, `code`, `credit_hours`,`id_faculty_program`, `status`, `created_by`) VALUES (NULL, '$course_name', '', '$course_code', '$credit_hours',$id_faculty ,1, 1)";

	  	 // echo $sql;die();

	    try
	    {
	    	$result = $con->query($sql);
	    	$parentId = mysqli_insert_id($con);
	 		// echo "Inserted Course Data --" . $parentId . " -- <br>";
	    }
	    catch(Exception $e)
	    {
	    	echo $e;
	    }
	    
	    return $parentId;
	}

	function getFacultyByName($name)
	{
		$con = $this->getConnection();

		$sql = "SELECT * FROM faculty_program where name = '$name' order by id DESC ";

		// echo $sql;die();

		$result  = $con->query($sql);
      	$id_faculty_program = 0;
		// echo "<Pre>";print_r($result); die();

      	while ($row = $result->fetch_assoc())
      	{
			// echo "<Pre>";print_r($row); die();
      		$id_faculty_program = $row['id'];
      	}
      	return $id_faculty_program;
	}





	// Importing Program Data

    function importProgramData()
    {

    	$con = $this->getConnection();
		// echo "<pre>";print_r($con); exit;


	  	$csvFile = file('program.csv');

		for($i=0;$i<count($csvFile);$i++)
		{
			$FirstArray = explode(",",$csvFile[$i]);

			echo "<pre>";print_r($FirstArray);exit;
			$last_inserted_id = $this->insertProgramData($FirstArray);

			echo "Succesfully Imported Program : " . $last_inserted_id . " :- <br>";

			 // continue;
		}
    }

    function insertProgramData($FirstArray)
	{

	  	$name = $FirstArray[0];
		$code = $FirstArray[1];
		$education_level = $FirstArray[2];
		$total_credit_hrs = $FirstArray[3];

		$id_education_level = $this->getEducationLevelByName($education_level);


		$con = $this->getConnection();

	  	$sql = "INSERT INTO `programme` (`id`, `name`, `code`, `total_cr_hrs`,`id_education_level`,`status`, `created_by`) VALUES (NULL, '$name', '$code', '$total_credit_hrs',$id_education_level,1, 1)";
	  	
	    try
	    {
	    	$result = $con->query($sql);
	    	$parentId = mysqli_insert_id($con);
	 		// echo "Inserted Course Data --" . $parentId . " -- <br>";
	    }
	    catch(Exception $e)
	    {
	    	echo $e;
	    }
	    
	    return $parentId;
	}

	function getEducationLevelByName($name)
	{
		$con = $this->getConnection();

		$sql = "SELECT * FROM education_level where name = '$name' order by id DESC ";

		$result  = $con->query($sql);
      	$id_education_level = 0;
		// echo "<Pre>";print_r($result); die();

      	while ($row = $result->fetch_assoc())
      	{
			// echo "<Pre>";print_r($row); die();
      		$id_education_level = $row['id'];
      	}
      	return $id_education_level;
	}




	// Importing Intake Data

    function importIntakeData()
    {

    	$con = $this->getConnection();
		// echo "<pre>";print_r($con); exit;


	  	$csvFile = file('intake.csv');

		for($i=0;$i<count($csvFile);$i++)
		{
			$FirstArray = explode(",",$csvFile[$i]);

			echo "<pre>";print_r($FirstArray);exit;
			$last_inserted_id = $this->insertIntakeData($FirstArray);

			echo "Succesfully Imported Intake : " . $last_inserted_id . " :- <br>";

			 // continue;
		}
    }

    function insertIntakeData($FirstArray)
	{

	  	$name = $FirstArray[0];
		$year = $FirstArray[1];
		$code = $FirstArray[2];

		$con = $this->getConnection();

	  	$sql = "INSERT INTO `intake` (`id`, `name`, `code`, `year`,`status`, `created_by`) VALUES (NULL, '$name', '$code', '$year',1, 1)";

	    try
	    {
	    	$result = $con->query($sql);
	    	$parentId = mysqli_insert_id($con);
	 		// echo "Inserted Course Data --" . $parentId . " -- <br>";
	    }
	    catch(Exception $e)
	    {
	    	echo $e;
	    }
	    
	    return $parentId;
	}







	// Importing Semester Data

    function importSemesterData()
    {

    	$con = $this->getConnection();
		// echo "<pre>";print_r($con); exit;


	  	$csvFile = file('semester.csv');

		for($i=0;$i<count($csvFile);$i++)
		{
			$FirstArray = explode(",",$csvFile[$i]);

			echo "<pre>";print_r($FirstArray);exit;
			$last_inserted_id = $this->insertSemesterData($FirstArray);

			echo "Succesfully Imported Semester : " . $last_inserted_id . " :- <br>";

			 // continue;
		}
    }

    function insertSemesterData($FirstArray)
	{

	  	$name = $FirstArray[0];
		$year = $FirstArray[1];
		$code = $FirstArray[2];

		$con = $this->getConnection();

	  	$sql = "INSERT INTO `semester` (`id`, `name`, `code`, `year`,`status`, `created_by`) VALUES (NULL, '$name', '$code', '$year',1, 1)";

	  	 // echo $sql;die();

	    try
	    {
	    	$result = $con->query($sql);
	    	$parentId = mysqli_insert_id($con);
	 		// echo "Inserted Course Data --" . $parentId . " -- <br>";
	    }
	    catch(Exception $e)
	    {
	    	echo $e;
	    }
	    
	    return $parentId;
	}





	// Importing Nationality Data

    function importNationalityData()
    {

    	$con = $this->getConnection();
		// echo "<pre>";print_r($con); exit;


	  	$csvFile = file('nationality.csv');

		for($i=0;$i<count($csvFile);$i++)
		{
			$FirstArray = explode(",",$csvFile[$i]);

			echo "<pre>";print_r($FirstArray);exit;
			$last_inserted_id = $this->insertIntakeData($FirstArray);

			echo "Succesfully Imported Intake : " . $last_inserted_id . " :- <br>";

			 // continue;
		}
    }

    function insertNationalityData($FirstArray)
	{

	  	$country = $FirstArray[0];
		$code = $FirstArray[1];
		$name = $FirstArray[2];
		$person = $FirstArray[3];

		$con = $this->getConnection();

	  	$sql = "INSERT INTO `nationality` (`id`, `name`, `code`, `country`, `person`,`status`, `created_by`) VALUES (NULL, '$name', '$code', '$country' , '$person',1, 1)";

	    try
	    {
	    	$result = $con->query($sql);
	    	$parentId = mysqli_insert_id($con);
	 		// echo "Inserted Course Data --" . $parentId . " -- <br>";
	    }
	    catch(Exception $e)
	    {
	    	echo $e;
	    }
	    
	    return $parentId;
	}


}