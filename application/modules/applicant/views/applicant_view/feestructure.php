<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Account Statement</h3>
        </div>    
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd><?php echo ucwords($applicantDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC / Passport :</dt>
                                <dd><?php echo $applicantDetails->nric ?></dd>
                            </dl>
                            
                                                       
                        </div>
                        
                        <div class='col-sm-6'>  
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $applicantDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Learning Mode :</dt>
                                <dd><?php echo $applicantDetails->program_scheme; ?></dd>
                            </dl>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                   <div class="custom-table">
                          <table class="table dataTable no-footer" id="list-table">
                            <thead>
                              <tr>
                                <th class="sorting_disabled">Sl. No</th>
                                <th class="sorting_disabled">Fee Item</th>
                                <th class="sorting_disabled">Frequency Mode</th>
                                <th class="sorting_disabled">Amount</th>
                              </tr>
                            </thead>
                            <tbody>

                                <?php for($i=0;$i<count($feedetails);$i++) { 
                                    ?> 
                                <tr role="row">
                                    <td>1</td>
                                     <td><?php echo $feedetails[$i]->fee_structure;?></td>
                                    <td><?php echo $feedetails[$i]->frequency_mode;?></td>
                                    <td><?php echo $feedetails[$i]->amount;?></td>
                                    
                                  </tr>
                                   <?php } ?> 
                                                             
                                                            </tbody>
                          </table>

            </div>


    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
