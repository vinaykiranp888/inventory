<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Applicant</h3>

        </div>

            <?php
                if($getApplicantDetails->is_submitted == 1 && $getApplicantDetails->applicant_status =='Draft')
                {
                
                ?>
               
               <h4 style="text-align: center;">Application Submitted Successfully</h4>


            <?php
                }
                elseif((!$receiptStatus) && $getApplicantDetails->applicant_status =='Approved')
                {
                    
                    ?>


                <!-- <div class="alert"> -->
              <!-- <span class="closebtn" onclick="this.parentElement.style.display='none';" style="font-size: 30px"> -->
               <h4 style="text-align: center;">Application Approved, Payment Pending Make A Payment To Proceed</h4>
              <!-- &times;</span> -->
              
            <!-- </div> -->


            <?php
            }elseif($receiptStatus->status == '0' && $getApplicantDetails->applicant_status =='Approved')
                {
                    
                    ?>

               <h4 style="text-align: center;">Fee Payment Added Succesfully, Wait Till Further Approval Process </h4>



            <?php
            }elseif($receiptStatus->status == '1' && $getApplicantDetails->applicant_status =='Approved')
                {
                     ?>

               <h4 style="text-align: center;">Payment Done Succesfully, Wait For Further Student Migration Process</h4>



            <?php
            }
            elseif($receiptStatus->status == '2' && $getApplicantDetails->applicant_status =='Approved')
                {
                     ?>
                    <h4 style="text-align: center;">Paid Payment Rejected, Contact College For Further Details</h4>



            <?php
            }
            elseif($getApplicantDetails->applicant_status =='Migrated')
            {
                    
                    ?>

                <div class="page-title clearfix">
                    <a href="../../studentLogin" target="_blank" class="btn btn-link btn-back"><?php echo '" Congratulation  '.$applicant_name.'   "' ?> Your Applicant Application Approved <br> Login To Student Portal >>></a>
                </div>
<!-- ‹ -->


            <?php
            }
            elseif($getApplicantDetails->applicant_status =='Approved')
            {    ?>

                </span>
                <?php
            }
            ?>


        <form id="form_programgrade" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Applicant Details</h4>            
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Salutation <span class='error-text'>*</span></label>
                            <select name="salutation" id="salutation" class="form-control" disabled>
                                <option value="">Select</option>
                                <option value="Mr" <?php if($getApplicantDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                <option value="Mrs" <?php if($getApplicantDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                <option value="Dr" <?php if($getApplicantDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>First Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Last Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Phone Number <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $getApplicantDetails->phone ?>" readonly>
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Email <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Password <span class='error-text'>*</span></label>
                            <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>NRIC <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Gender <span class='error-text'>*</span></label>
                            <select class="form-control" id="gender" name="gender" disabled>
                                <option value="">SELECT</option>
                                <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date Of Birth <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Martial Status <span class='error-text'>*</span></label>
                            <select class="form-control" id="martial_status" name="martial_status" disabled>
                                <option value="">SELECT</option>
                                <option value="Single" <?php if($getApplicantDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                <option value="Married" <?php if($getApplicantDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Religion <span class='error-text'>*</span></label>
                            <select name="religion" id="religion" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($religionList))
                                {
                                    foreach ($religionList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->religion)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Type Of Nationality <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="nationality" name="nationality" value="<?php echo $getApplicantDetails->nationality ?>" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($raceList))
                                {
                                    foreach ($raceList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_race)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label> Race <span class='error-text'>*</span></label>
                            <select name="id_race" id="id_race" class="form-control" disabled>
                                <option value="">Select</option>
                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'Bhumiputra')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "Bhumiputra";  ?>
                                </option>

                                <option value="<?php echo $getApplicantDetails->id_race;?>"
                                    <?php 
                                    if ($getApplicantDetails->id_race == 'International')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                            <?php echo "International";  ?>
                                </option>
                            </select>
                        </div>
                    </div> -->

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Degree Level <span class='error-text'>*</span></label> <a href="editProgram" class="btn btn-link">
                                <!-- <span style='font-size:18px;'>&#9998;</span> -->
                            </a>
                            <select name="id_degree_type" id="id_degree_type" class="form-control" disabled="true">
                                <option value="">Select</option>
                                <?php
                                if (!empty($degreeTypeList))
                                {
                                    foreach ($degreeTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $getApplicantDetails->id_degree_type)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code . " - " . $record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <p>Do you Wish To Apply For Hostel Accomodation <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_hostel" id="sd1" value="1" <?php if($getApplicantDetails->is_hostel=='1'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_hostel" id="sd2" value="0" <?php if($getApplicantDetails->is_hostel=='0'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div> -->

                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_program==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Intake <span class='error-text'>*</span></label>
                            <select name="id_intake" id="id_intake" class="form-control selitemIcon" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($intakeList))
                                {
                                    foreach ($intakeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->id_intake==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                     
                    
                </div>
            </div>        

            <div class="form-container">
                <h4 class="form-group-title">Mailing Address</h4>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Address 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $getApplicantDetails->mail_address1 ?>" readonly>
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Address 2 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $getApplicantDetails->mail_address2 ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Country <span class='error-text'>*</span></label>
                            <select name="mailing_country" id="mailing_country" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing State <span class='error-text'>*</span></label>
                            <select name="mailing_state" id="mailing_state" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $getApplicantDetails->mailing_city ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Mailing Zipcode <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $getApplicantDetails->mailing_zipcode ?>" readonly>
                        </div>
                    </div>
                </div>
            </div>
        

            <div class="form-container">
                <h4 class="form-group-title">Permanent Address</h4>            
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Address 1 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $getApplicantDetails->permanent_address1 ?>" readonly>
                        </div>
                    </div><div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Address 2 <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $getApplicantDetails->permanent_address2 ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Country <span class='error-text'>*</span></label>
                            <select name="permanent_country" id="permanent_country" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($countryList))
                                {
                                    foreach ($countryList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent State <span class='error-text'>*</span></label>
                            <select name="permanent_state" id="permanent_state" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($stateList))
                                {
                                    foreach ($stateList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>" <?php if($getApplicantDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent City <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $getApplicantDetails->permanent_city ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Permanent Zipcode <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $getApplicantDetails->permanent_zipcode ?>" readonly> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Other Details</h4>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Do you have sibbling/s studying with university? <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                        <input type="radio" id="sd1" name="sibbling_discount" value="Yes" disabled="disabled" <?php if($getApplicantDetails->sibbling_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                            <input type="radio" id="sd2" name="sibbling_discount" value="No" disabled="disabled" <?php if($getApplicantDetails->sibbling_discount=='No'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                        </label>
                    </div>
                </div>
            </div>


            
           <?php
            if($getApplicantDetails->sibbling_discount=='Yes')
            {
                ?>

                <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" id="sibbling_name" name="sibbling_name" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_name ?>" readonly="readonly">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC <span class='error-text'>*</span></label>
                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_nric ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $sibblingDiscountDetails->sibbling_status ?>" readonly="readonly">
                    </div>
                </div>
            </div> 

                <?php
                if($sibblingDiscountDetails->sibbling_status=='Reject')
                {
                ?>

                <div class="row">
                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Reject Reason <span class='error-text'>*</span></label>
                                <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $sibblingDiscountDetails->reason ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($sibblingDiscountDetails->rejected_on)) ?>" readonly="readonly">
                            </div>
                    </div>
                </div> 


                <?php
                }
                elseif($sibblingDiscountDetails->sibbling_status=='Approved')
                {
                    ?>
                    

                    <div class="row">

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $sibblingDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $sibblingDiscountDetails->rejected_on ?>" readonly="readonly">
                            </div>
                    </div>

                    
                </div> 

                    <?php
                }
                ?> 

            <?php
            }
             ?>


             <br>

            
            <div class="row">      
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Do you eligible for Employee discount <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="employee_discount" id="ed1" value="Yes" disabled="disabled" <?php if($getApplicantDetails->employee_discount=='Yes'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="employee_discount" id="ed2" value="No" disabled="disabled" <?php if($getApplicantDetails->employee_discount=='No'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                        </label>                              
                    </div>                         
                </div>
            </div>


             <?php
            if($getApplicantDetails->employee_discount=='Yes')
            {
                ?>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" id="employee_name" name="employee_name" class="form-control" value="<?php echo $employeeDiscountDetails->employee_name ?>" readonly="readonly">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>NRIC <span class='error-text'>*</span></label>
                        <input type="text" id="employee_nric" name="employee_nric" class="form-control" value="<?php echo $employeeDiscountDetails->employee_nric ?>" readonly="readonly">
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Designation <span class='error-text'>*</span></label>
                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_designation ?>" readonly="readonly">
                    </div>
                </div>
            </div> 


            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" id="employee_designation" name="employee_designation" class="form-control" value="<?php echo $employeeDiscountDetails->employee_status ?>" readonly="readonly">
                    </div>
                </div>
            </div>


                <?php
                if($employeeDiscountDetails->employee_status=='Reject')
                {
                ?>

                <div class="row">

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected Reason <span class='error-text'>*</span></label>
                                <input type="text" id="sibbling_nric" name="sibbling_nric" class="form-control" value="<?php echo $employeeDiscountDetails->reason ?>" readonly="readonly">
                            </div>
                    </div>

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>
                </div> 


                <div class="row">

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo date('d-m-Y h:i:s a', strtotime($employeeDiscountDetails->rejected_on)) ?>" readonly="readonly">
                            </div>
                    </div>


                </div> 


                <?php
                }
                elseif($employeeDiscountDetails->employee_status=='Approved')
                {
                    ?>

                <div class="row">

                     <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved By <span class='error-text'>*</span></label>
                                <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $employeeDiscountDetails->user_name ?>" readonly="readonly">
                            </div>
                    </div>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Approved On <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $employeeDiscountDetails->rejected_on ?>" readonly="readonly">
                            </div>
                    </div>

                    
                </div> 

                    <?php
                }
                ?> 

            <?php
            }
             ?>
             
        </div>

            <br>

        <div class="form-container">
                <h4 class="form-group-title">Applicant Approval</h4>

            <div class="row">

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Approval Status <span class='error-text'>*</span></label>
                        <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $getApplicantDetails->applicant_status; ?>" readonly="readonly">
                        </div>
                </div>

                 <?php
                if($getApplicantDetails->applicant_status=='Rejected')
                {
                    ?>

                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Rejected Reason <span class='error-text'>*</span></label>
                                <input type="text" id="rejected_on" name="rejected_on" class="form-control" value="<?php echo $getApplicantDetails->reason; ?>" readonly="readonly">
                            </div>
                    </div>


                 <?php
                }
             ?>
                

              

            </div>

        </div>

           <!--  <div class="row">                
                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Applicant Status Approval <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                        <input type="radio" id="sd1" name="applicant_status" disabled="disabled" value="Approved" <?php if($getApplicantDetails->applicant_status=='Approved'){ echo "checked";}?> ><span class="check-radio"></span> Approve
                    </label>
                        <label class="radio-inline">

                        <input type="radio" id="sd2" name="applicant_status" disabled="disabled" value="Draft" <?php if($getApplicantDetails->applicant_status=='Draft'){ echo "checked";}?>><span class="check-radio"></span> Draft
                    </label>
                    </div>
                </div>
            </div> -->
        
            
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<style>
    /* The alert message box */
.alert {
  padding: 20px;
  background-color: #f44336; /* Red */
  color: white;
  margin-bottom: 15px;
}

/* The close button */
.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

/* When moving the mouse over the close button */
.closebtn:hover {
  color: black;
}
</style>