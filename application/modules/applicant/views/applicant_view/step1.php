    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">

            <div class="main-container clearfix">
                <div class="page-title clearfix">
                    <h3>Programme Application</h3>                    
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                      <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step1" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step2" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step3" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step4" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step5" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="step6" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">



              <div class="row">
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Salutation <span class='error-text'>*</span></label>
                        <select name="salutation" id="salutation" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($salutationList)) {
                                  foreach ($salutationList as $record) {
                              ?>
                           <option value="<?php echo $record->id;  ?>"
                              <?php if($getApplicantDetails->salutation==$record->id)
                                 {
                                     echo "selected=selected";
                                 }
                                 ?>
                              >
                              <?php echo $record->name;  ?>        
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>First Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $getApplicantDetails->first_name ?>">
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Last Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $getApplicantDetails->last_name ?>">
                     </div>
                  </div>
              
              </div>






              <div class="row">

                
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Nationality <span class='error-text'>*</span></label>
                        <select name="nationality" id="nationality" class="form-control" onchange="checkNationality(this.value)">
                           <option value="">Select</option>
                           <?php
                              if(!empty($nationalityList))
                              {
                                foreach ($nationalityList as $record)
                                {
                              ?>
                           <option value="<?php echo $record->id;  ?>"
                              <?php if($getApplicantDetails->nationality==$record->id)
                                 {
                                     echo "selected=selected";
                                 }
                                 ?>
                              >
                              <?php echo $record->name;  ?>        
                           </option>
                            <?php
                                }
                              }
                              ?>
                        </select>
                     </div>
                  </div>

               


                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>ID Type <span class='error-text'>*</span></label>
                       <select name="id_type" id="id_type" class="form-control" required onchange="getlabel()">
                        <option value="">Select</option>
                          <option value="NRIC" <?php if($getApplicantDetails->id_type=='NRIC') { echo "selected=selected";}?>>NRIC</option>
                          <option value="PASSPORT" <?php if($getApplicantDetails->id_type=='PASSPORT') { echo "selected=selected";}?>>PASSPORT</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-sm-4">
                     <div class="form-group">
                        <label><span id='labelspanid'></span> <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $getApplicantDetails->nric ?>">
                     </div>
                  </div>


                
            
              </div>
                





              <div class="row">


                  <div class="col-sm-4" id="view_race">
                     <div class="form-group">
                        <label>Race <span class='error-text'>*</span></label>
                        <select name="id_race" id="id_race" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($raceList))
                              {
                                  foreach ($raceList as $record)
                                  {?>
                           <option value="<?php echo $record->id;  ?>"
                              <?php 
                                 if($record->id == $getApplicantDetails->id_race)
                                 {
                                     echo "selected=selected";
                                 } ?>>
                              <?php echo $record->name;  ?>
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Religion <span class='error-text'>*</span></label>
                        <select name="religion" id="religion" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($religionList))
                              {
                                  foreach ($religionList as $record)
                                  {?>
                           <option value="<?php echo $record->id;  ?>"
                              <?php 
                                 if($record->id == $getApplicantDetails->religion)
                                 {
                                     echo "selected=selected";
                                 } ?>>
                              <?php echo $record->name;  ?>
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>


                  <div class="col-sm-4">
                     <div class="form-group">
                        <label>Date Of Birth <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" autocomplete="off" value="<?php echo $getApplicantDetails->date_of_birth ?>">
                     </div>
                  </div>
              
              </div>






               <div class="row">

                        <div class="col-sm-4">

                           <div class="form-group">
                              <label>Marital status <span class='error-text'>*</span></label>
                              <select class="form-control" id="martial_status" name="martial_status" required>
                                 <option value="">SELECT</option>
                                 <option value="Single" 
                                    <?php 
                                       if($getApplicantDetails->martial_status=='Single')
                                           { echo "selected"; } 
                                       ?>>
                                    SINGLE
                                 </option>
                                 <option value="Married" 
                                    <?php 
                                       if($getApplicantDetails->martial_status=='Married')
                                           { echo "selected"; }
                                            ?>>
                                    MARRIED
                                 </option>
                                 <option value="Divorced" <?php if($getApplicantDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                              </select>
                           </div>
                        </div>

                     

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label>Email <span class='error-text'>*</span></label>
                              <input type="email" class="form-control" id="email_id" name="email_id" value="<?php echo $getApplicantDetails->email_id ?>" readonly>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label>Password <span class='error-text'>*</span></label>
                              <input type="password" class="form-control" id="password" name="password" value="<?php echo $getApplicantDetails->password ?>" readonly>
                           </div>
                        </div>
                      
                      

                </div>



                <div class="row">

                          <div class="col-sm-4">
                            <label>Phone Number <span class='error-text'>*</span></label>
                              <div class="row">
                                 <div class="col-sm-4">

                                   <select name="country_code" id="country_code" class="form-control" required>
                                    <option value="">Select</option>                    
                                    <?php
                                        if (!empty($countryList))
                                        {
                                          foreach ($countryList as $record)
                                          {
                                        ?>
                                     <option value="<?php echo $record->phone_code;  ?>"
                                          <?php 
                                         if($record->phone_code == $getApplicantDetails->country_code)
                                         {
                                           echo "selected";
                                         } ?>
                                        >
                                        <?php echo $record->phone_code . "  " . $record->name;  ?>
                                     </option>
                                       <?php
                                        }
                                      }
                                    ?>
                                  </select>
                            </div>
                            <div class="col-sm-8">
                              <input type="number" class="form-control" id="phone" name="phone"  required value="<?php echo $getApplicantDetails->phone ?>">
                            </div>
                            </div>

                          </div>

                        
                              <div class="col-sm-4">
                                 <div class="form-group">
                                    <label>Gender <span class='error-text'>*</span></label>
                                    <select class="form-control" id="gender" name="gender">
                                       <option value="">SELECT</option>
                                       <option value="Male" <?php if($getApplicantDetails->gender=='Male'){ echo "selected"; } ?>>MALE</option>
                                       <option value="Female" <?php if($getApplicantDetails->gender=='Female'){ echo "selected"; } ?>>FEMALE</option>
                                       <!-- <option value="Others"<?php if($getApplicantDetails->gender=='Others'){ echo "selected"; } ?> >OTHERS</option> -->
                                    </select>
                                 </div><br/><br/>
                              </div>
                     </div>

                     
                </div>
    
                      
    
                      </div>
                       <div class="wizard__footer">
                        <button class="btn btn-link mr-3">Cancel</button>
                        <button class="btn btn-primary next" type="submit">Save & Continue</button>
                      </div>

    
                     
                    </div>
    
                   
                  </div>



                
               

                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
                
            </div>        
        </div>
    </div>      
    </form>
      


<script type="text/javascript">

    $('select').select2();

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
    });

    
     function checkNationality(nationality)
    {
        if(nationality != '')
        {
            if(nationality == '1')
            {
                $('#view_nric').show();
                $('#view_race').show();
                $('#view_passport').hide();

            }else{
                $('#view_nric').hide();
                $('#view_race').hide();
                $('#view_passport').show();
            }
        }
    }

    function getlabel() {
      var labelnric = $("#id_type").val();
      //alert(labelnric);
      $("#labelspanid").html(labelnric);
    }
    
    $(document).ready(function()
    {
      var nationality = "<?php echo $getApplicantDetails->nationality; ?>";
      
      // alert(nationality);
      if(nationality != '')
      {
          checkNationality(nationality);
      }
      getlabel();

        $("#form_applicant").validate({
            rules: {
                salutation: {
                    required: true
                },
                 first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 phone: {
                    required: true
                },
                 email_id: {
                    required: true
                },
                 password: {
                    required: true
                },
                 nric: {
                    required: true
                },
                 gender: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_intake: {
                    required: true
                },
                employee_discount :{
                    required : true
                },
                sibbling_discount :{
                    required : true
                },
                 sibbling_name: {
                    required: true
                },
                 sibbling_nric: {
                    required: true
                },
                 employee_name: {
                    required: true
                },
                 employee_nric: {
                    required: true
                },
                 employee_designation: {
                    required: true
                },
                 date_of_birth: {
                    required: true
                },
                 nationality: {
                    required: true
                },
                 id_race: {
                    required: true
                },
                 mail_address1: {
                    required: true
                },
                 mailing_city: {
                    required: true
                },
                 mailing_country: {
                    required: true
                },
                 mailing_state: {
                    required: true
                },
                 mailing_zipcode: {
                    required: true
                },
                 permanent_address1: {
                    required: true
                },
                 permanent_city: {
                    required: true
                },
                 permanent_country: {
                    required: true
                },
                 permanent_state: {
                    required: true
                },
                 permanent_zipcode: {
                    required: true
                },
                 is_submitted: {
                    required: true
                },
                is_hostel: {
                    required: true
                },
                id_degree_type: {
                    required: true
                },
                passport: {
                    required: true
                },
                program_scheme: {
                    required: true
                },
                 alumni_discount: {
                    required: true
                },
                alumni_name: {
                    required: true
                },
                alumni_email: {
                    required: true
                },
                religion: {
                    required: true
                },
                id_program_scheme: {
                    required: true
                },
                martial_status: {
                    required: true
                }
            },
            messages: {
                salutation: {
                    required: "<p class='error-text'>Salutation required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name required</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Email required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Gender required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                employee_discount: {
                    required: "<p class='error-text'>Employee Discount required</p>",
                },
                sibbling_discount: {
                    required: "<p class='error-text'>Sibbling Discount required</p>",
                },
                sibbling_name: {
                    required: "<p class='error-text'>Sibling Name required</p>",
                },
                sibbling_nric: {
                    required: "<p class='error-text'>Sibling NRIC required</p>",
                },
                employee_name: {
                    required: "<p class='error-text'>Employee Name required</p>",
                },
                employee_nric: {
                    required: "<p class='error-text'>Employee NRIC required</p>",
                },
                employee_designation: {
                    required: "<p class='error-text'>Employee Designation required</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Type Of Nationality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Race</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Enter Mailing Address 1</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Enter Mailimg City</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Enter Mailing Zipcode</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Enter Permanent Address 1</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Enter Permanent City</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Enter Permanent Zipcode</p>",
                },
                is_submitted: {
                    required: "<p class='error-text'>Check Indicate that you accept the Terms and Conditions</p>",
                },
                is_hostel: {
                    required: "<p class='error-text'>Select Accomodation Required</p>",
                },
                id_degree_type: {
                    required: "<p class='error-text'>Select Degree Level</p>",
                },
                passport: {
                    required: "<p class='error-text'>Passport No. Required</p>",
                },
                program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                alumni_discount: {
                    required: "<p class='error-text'>Select Alumni Discount Applicable </p>",
                },
                alumni_name: {
                    required: "<p class='error-text'>Alumni Name Required</p>",
                },
                alumni_email: {
                    required: "<p class='error-text'>Alumni Email Required </p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                id_program_scheme: {
                    required: "<p class='error-text'>Select Program Scheme</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Marital Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
  });

</script>