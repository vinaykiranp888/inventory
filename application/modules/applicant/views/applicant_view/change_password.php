<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Account Statement</h3>
        </div>    
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Applicant Name :</dt>
                                <dd><?php echo ucwords($applicantDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $applicantDetails->nric ?></dd>
                            </dl>
                           
                                                       
                        </div>
                        
                        <div class='col-sm-6'>  
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $applicantDetails->email_id; ?></dd>
                            </dl>
                            
                        </div>
                    </div>
                </div>
            </div>
<form method="post" action="" id="target">
            <div class="form-container">
                <h4 class="form-group-title">New Password</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>New Password :</dt>
                                <dd><input type='password' class='form-control' name='pwd' id='pwd' value=''/></dd>
                            </dl>
                            
                                                       
                        </div>
                    </div>
                    <div class='row'> 
                        
                        <div class='col-sm-6'>  
                             <dl>
                                <dt>Confirm Password :</dt>
                                <dd><input type='password' class='form-control' name='cnfpwd' id='cnfpwd' value=''/></dd>
                            </dl>
                                                       
                        </div>
                    </div>
                    <div class="row">
                         <div class='col-sm-6'>  
                             <dl>
                        <input type='button' class="btn btn-primary next" value="Update Password" onclick="verifypwd();">
                    </dl>
                </div>
                    </div>
                </div>
            </div>

</form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


<script>
function verifypwd() {
    var pwd = $("#pwd").val();
    var cnfpwd = $("#cnfpwd").val();

    if(pwd!=cnfpwd) {
        alert("New password and Confirm password are not same");
        return false;
    } else {
        alert("Your password has been changed successfully");
$( "#target" ).submit();
    }
}
</script>