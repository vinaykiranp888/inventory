    <form id="form_applicant" action="" method="post" enctype="multipart/form-data">

            <div class="main-container clearfix">
                <div class="page-title clearfix">
                    <h3>Programme Application</h3>                    
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                         <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep1" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep2" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step -completed">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep3" class="step__text">Program Interest</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep4" class="step__text">Document Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep5" class="step__text">Discount Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="rstep6" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">
                          
                           <div class="clearfix">
                            
                          <?php
                            if(empty($applicantUploadedFiles))
                            {
                                ?>
                              

                            
                                <div class="form-container">
                                        <h4 class="form-group-title">Document Uploaded Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>File Name</th>
                                                 <th>Supporting Format</th>
                                                 <th>File Size</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($getFileList);$i++)
                                             { 
                                               $id = $getFileList[$i]->id;
            $name = $getFileList[$i]->name;
             $file_size = $getFileList[$i]->file_size;    ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $name;?></td>
                                                <td> - </td>
                                                <td> <?php echo $file_size;?> MB</td>
                                                <td><input type='file' name='file[]'>
                                                <input type='hidden' name='fileid[]' value='$id' /></td>
                                               

                                                 </tr>
                                              <?php
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

      </tr>
                                              <?php
                                          } 
                                          ?>

                            <?php

                            if(!empty($applicantUploadedFiles))
                            {
                                ?>
                              

                                <div class="form-container">
                                        <h4 class="form-group-title">Document Uploaded Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Name</th>
                                                 <th style="text-align: center;">File</th>
                                                 <th style="text-align: center;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($applicantUploadedFiles);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $applicantUploadedFiles[$i]->document_name;?></td>
                                                <td class="text-center">

                                                    <a href="<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $applicantUploadedFiles[$i]->file; ?>)" title="<?php echo $applicantUploadedFiles[$i]->file; ?>">View</a>
                                                </td>
                                                <td class="text-center">
                                                    <a onclick="deleteApplicantUploadedDocument(<?php echo $applicantUploadedFiles[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>

                            <?php
                            
                            }
                             ?>


                           </div>              
                        </div>    
                      </div>
    
                      
                      <div class="wizard__footer">
                        <a href="/applicant/applicant/rstep3" class="btn btn-primary">Previous</a>

                        <button class="btn btn-link mr-3"></button>
                        <a href="rstep5" class="btn btn-primary next">Next</a>
                      </div>
                    </div>
                    </div>
    
                  </div>
                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
            </div>        
        </div>
    </div>      
    </form>
    
 


<script type="text/javascript">

  $(document).ready(function() {

        var idprogram = "<?php echo $getApplicantDetails->id_program;?>";


                    getDocumentByProgramme(idprogram);
                });


   function getDocumentByProgramme(id)
    {
        if(id != '')
        {

            $.get("/applicant/applicant/getDocumentByProgramme/"+id, function(data, status){
           
                if(data != '')
                {
                    $("#doc").html(data);
                    // $("#view_document").show();
                }else
                {
                    // $("#view_document").hide();
                    alert('No Records Defined To Upload');
                }

            });
        }
    }

     function deleteApplicantUploadedDocument(id)
    {
      if(id != '')
        {

            $.get("/applicant/applicant/deleteApplicantUploadedDocument/"+id, function(data, status)
            {
              alert('Document File Deleted Successfully');
              window.location.reload();
            });
        }
      
    }
</script>