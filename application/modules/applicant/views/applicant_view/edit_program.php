<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program & Intake</h3>
            <!-- <a href="../list" class="btn btn-link btn-back">‹ Back</a> -->
        </div>    
        <form id="form_main_invoice" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Selected Program & Intake Details</h4>
                <div class='data-list'>

                <?php
                if(isset($getApplicantDetails))
                {
                ?>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $getApplicantDetails->program_code . " - " .ucwords($getApplicantDetails->program_name);?></dd>
                            </dl>                  
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getApplicantDetails->intake_year . " - " .$getApplicantDetails->intake_name ?></dd>
                            </dl>
                        </div>
                    </div>

                <?php
                }else
                {
                	?>
                	<div class='row'> 
                        <div class='col-sm-12'>
                            <dl>
                                <dt>Data Not Available</dt>
                                <!-- <dd><?php echo $getApplicantDetails->program_code . " - " .ucwords($getApplicantDetails->program_name);?></dd> -->
                            </dl>                  
                        </div>        
                    </div>
                <?php
                }
                ?>
                </div>
            </div>



        <div class="form-container">
            <h4 class="form-group-title">New Program & Intake Details</h4>



            <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control selitemIcon"  onchange="getIntakeByProgramme(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label> Intake <span class='error-text'>*</span></label>
                            <span id="view_intake"></span>
                        </div>
                    </div>
                    
            </div>

        </div>


       <!--  <div class="modal-footer">
            <button type="submit" class="btn btn-default" onclick="">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div> -->

          <div class="button-block clearfix">
                <div class="bttn-group">
                   <button type="submit" class="btn btn-primary btn-lg">Save</button>
                   <a href="edit" class="btn btn-link">Back</a>
                </div>
            </div>


            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<script>
    
    $('select').select2();

    function getIntakeByProgramme(id)
     {
        if(id != '')
        {
            $.get("/applicant/applicant/getIntakeByProgramme/"+id, function(data, status){
           
                $("#view_intake").html(data);
                $("#view_intake").show();
            });
        }
     }

     function checkFeeStructure()
     {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_intake'] = $("#id_intake").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '' )
        {
            // alert(tempPR['id_program']);

            $.ajax(
            {
               url: '/applicant/applicant/checkFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                // window.location.reload();
                    if(result == '0')
                    {
                        alert('No Fee Structure Defined For This Programme & Intake, Select Another Combination');
                        // document.getElementById("id_program").setTextValue() = "";
                        // document.getElementById("id_intake").setTextValue() = "";
                        // $("#id_program").val('');
                        // $("#id_intake").val('');
                        // $("#id_program").html("");
                        $(this).data('options', $('#id_intake option').clone());
                        $("#id_intake").html('<option value="">').append(options);
                        $("#id_intake").val('');
                        
                    }
               }
            });
        }
     }

    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>