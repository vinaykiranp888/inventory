<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Asset Item</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Asset Item Details</h4>


            <div class="row">   

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Asset Category <span class='error-text'>*</span></label>
                        <select name="id_asset_category" id="id_asset_category" class="form-control" onchange="getSubcategory(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($assetCategoryList))
                            {
                                foreach ($assetCategoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->description;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>                  
            
            </div>

            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Asset Sub-Category *</label>
                        <span id='selectsubcategory'></span>
                    </div>
                </div>    

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

            
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function getSubcategory(id)
    {
        $.get("/asset/assetItem/getSubcategory/"+id, function(data, status)
        {
            $("#selectsubcategory").html(data);
        });
    }

    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                code: {
                    required: true
                },
                 description: {
                    required: true
                },
                 id_asset_category: {
                    required: true
                },
                 id_asset_sub_category: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Asset Sub Category Code required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description required</p>",
                },
                id_asset_category: {
                    required: "<p class='error-text'>Select Category Code</p>",
                },
                id_asset_sub_category: {
                    required: "<p class='error-text'>Select Sub-Category Code</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>