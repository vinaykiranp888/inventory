<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Edit_profile_model extends CI_Model
{
	function addNewExamSet($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editData($data,$id)
    {
    	$this->db->where('id', $id);
        $this->db->update('performa_main_invoice', $data);
        return $this->db->affected_rows();
    }
}