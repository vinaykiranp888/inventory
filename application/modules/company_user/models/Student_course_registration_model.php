<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Student_course_registration_model extends CI_Model
{

     function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }


    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }


    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('qualification_setup');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('s.*, adt.ic_no, adt.name as advisor_name, cm.name as company_name, cm.registration_number');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id','left');
        $this->db->join('staff as adt', 's.id_advisor = adt.id','left');
        $this->db->join('company as cm', 's.id_company = cm.id');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $this->db->where('s.email_id', $data['email_id']);
        }
        if ($data['id_advisor'] != '')
        {
            $this->db->where('s.id_advisor', $data['id_advisor']);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('s.id_company', $data['id_company']);
        }
        $this->db->where('s.applicant_status !=', 'Graduated');
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }
    

    function addAdvisorTagging($data)
    {
        $this->db->trans_start();
        $this->db->insert('advisor_tagging', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function updateStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);

        return TRUE;
    }

    function createNewMainInvoiceForStudent($id)
    {
        $id_student = $id;
        $user_id = $this->session->userId;

        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $applicant_data = $query->row();

        $id_applicant = $applicant_data->id;
        $id_branch = $applicant_data->id_branch;
        $id_university = $applicant_data->id_university;
        $id_fee_structure = $applicant_data->id_fee_structure;

        if($id_fee_structure != 0)
        {
            // $fee_structure = $this->getFeeStructureMasterIdByData($id_fee_structure);
            $fee_structure_master = $this->getFeeStructureMasterById($id_fee_structure);
        }


        $id_program = $applicant_data->id_program;
        $id_intake = $applicant_data->id_intake;
        $nationality = $applicant_data->nationality;
        $id_program_scheme = $applicant_data->id_program_scheme;
        $id_program_has_scheme = $applicant_data->id_program_has_scheme;
        $id_program_landscape = $applicant_data->id_program_landscape;
        $is_sibbling_discount = '';
        $is_employee_discount = '';
        $is_alumni_discount = '';


        // echo "<Pre>";print_r($id_program_landscape);exit;


        $is_installment = 0;
        $installments = 0;
        $currency = 1;

       
        // echo "<Pre>";print_r($fee_structure_training_data);exit;

        // echo "<Pre>";print_r($is_installment);exit;
        
        // echo "<Pre>";print_r($fee_structure_master);exit;

        if($fee_structure_master)
        {

            // $trigger = 'OFFER ACCEPTED';
            $trigger = '';

            // if($id_university == 1)
            // {

                
                // echo "<Pre>";print_r($fee_structure_master);exit;

                $id_currency = $fee_structure_master->id_currency;
                $currency = $id_currency;

                $get_data['id_fee_structure'] = $id_fee_structure;
                $get_data['id_training_center'] = 1;
                $get_data['trigger'] = $trigger;

                if($nationality == '1')
                {
                    $get_data['currency'] = 'MYR';
                    $detail_data = $this->getFeeStructureByData($get_data);
                }
                elseif($nationality != '')
                {
                    $get_data['currency'] = 'USD';
                    $detail_data = $this->getFeeStructureByData($get_data);
                }

                // echo "<Pre>";print_r($detail_data);exit;



                if(!empty($detail_data))
                {
                    $invoice_number = $this->generateMainInvoiceNumber();

                    $invoice['invoice_number'] = $invoice_number;
                    $invoice['type'] = 'CORPORATE';
                    $invoice['remarks'] = 'OFFER ACCEPTED Fee';
                    $invoice['id_application'] = '0';
                    $invoice['id_student'] = $id_student;
                    $invoice['id_program'] = $id_program;
                    $invoice['id_intake'] = $id_intake;
                    $invoice['currency'] = $id_currency;
                    $invoice['total_amount'] = '0';
                    $invoice['balance_amount'] = '0';
                    $invoice['paid_amount'] = '0';
                    $invoice['status'] = '1';
                    $invoice['is_migrate_applicant'] = $id;
                    $invoice['created_by'] = $user_id;

                    // $detail_data = $this->getFeeStructure('13','5');

                    
                    // echo "<Pre>";print_r($invoice);exit;
                    $inserted_id = $this->addNewMainInvoice($invoice);

                    if($inserted_id)
                    {
                        $applicant_update_data['is_invoice_generated'] = $inserted_id;
                        $updated_applicant = $this->editStudent($applicant_update_data,$id_student);
                    }



                    $total_amount = 0;
                    $total_discount_amount = 0;
                    $sibling_discount_amount = 0;
                    $employee_discount_amount = 0;
                    $alumni_discount_amount = 0;


                    // echo "<Pre>";print_r($detail_data);exit;

                    foreach ($detail_data as $fee_structure)
                    {
                        $is_installment = $fee_structure->is_installment;
                        $id_training_center = $fee_structure->id_training_center;
                        // $trigger_name = $fee_structure->trigger_name;

                        // $instllment_data['id_fee_structure'] = $fee_structure->id;
                        // $instllment_data['id_fee_structure_master'] = $fee_structure->id_program_landscape;

            
                        // if($trigger_name == 'OFFER ACCEPTED')
                        // {
                            $data = array(
                                'id_main_invoice' => $inserted_id,
                                'id_fee_item' => $fee_structure->id_fee_item,
                                'amount' => $fee_structure->amount,
                                'price' => $fee_structure->amount,
                                'quantity' => 1,
                                'id_reference' => $fee_structure->id,
                                'description' => 'Application Registration Fee',
                                'status' => 1,
                                'created_by' => $user_id
                            );

                            $total_amount = $total_amount + $fee_structure->amount;

                            $this->addNewMainInvoiceDetails($data);
                        // }
                        // echo "<Pre>";print_r($data);exit;
                    }

                    $total_invoice_amount = $total_amount;



                    if($is_sibbling_discount == '1')
                    {
                        $this->db->select('*');
                        $this->db->from('sibbling_discount');
                        // $likeCriteria = "(date(start_date)  <= '" . date('Y-m-d') . "')";
                        // $this->db->where($likeCriteria);
                        // $likeCriteria = "(date(end_date)  <= '" . date('Y-m-d') . "')";
                        // $this->db->where($likeCriteria);

                    //     $SQL = "Select * From sibbling_discount where date(start_date) <= 'getdate()' and date(end_date) >= 'getdate()' order by id DESC limit 0,1";
                    //     $query = $this->db->query($SQL);
                       
                    // echo "<Pre>";print_r($query->row());exit;

                        $this->db->where('currency', $currency);
                        $this->db->where('status', '1');
                        $query = $this->db->get();
                        $sibling_discount_data = $query->row();

                        if($sibling_discount_data)
                        {
                            $amount = $sibling_discount_data->amount;
                            $id_discount = $sibling_discount_data->id;

                            $sibling_insert = array(
                                'id_main_invoice' => $inserted_id,
                                'id_student' => $id_student,
                                'name' => 'Sibbling Discount Applied',
                                'amount' => $amount,
                                'id_reference' => $id_discount,
                            );
                            $discount_inserted_id = $this->addNewMainInvoiceDiscountDetail($sibling_insert);
                            if($discount_inserted_id)
                            {
                                $total_amount = $total_amount - $sibling_discount_data->amount;
                                $sibling_discount_amount = $sibling_discount_data->amount;
                            }
                        }

                    // echo "<Pre>";print_r($amount);exit;
                    }

                    if($is_employee_discount == '1')
                    {

                        $this->db->select('*');
                        $this->db->from('employee_discount');
                        // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
                        // $this->db->where('date(start_date) >=', date('Y-m-d'));
                        // $this->db->where('date(end_date) <=', date('Y-m-d'));
                        $this->db->where('currency', $currency);
                        $this->db->where('status', '1');
                        $query = $this->db->get();
                        $employee_discount_data = $query->row();
                        if($employee_discount_data)
                        {
                            $amount = $employee_discount_data->amount;
                            $id_discount = $employee_discount_data->id;

                            $employee_insert = array(
                                'id_main_invoice' => $inserted_id,
                                'id_student' => $id_student,
                                'name' => 'Employee Discount Applied',
                                'amount' => $amount,
                                'id_reference' => $id_discount,
                            );
                            $sibbling_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                            if($sibbling_inserted_id)
                            {
                                $total_amount = $total_amount - $employee_discount_data->amount;
                                $employee_discount_amount = $employee_discount_data->amount;
                            }
                        }
                    }




                    if($is_alumni_discount == '1')
                    {

                        $this->db->select('*');
                        $this->db->from('alumni_discount');
                        // $this->db->where(date('Y-m-d').' BETWEEN  date(start_date) and date(end_date)');
                        // $this->db->where('date(start_date) >=', date('Y-m-d'));
                        // $this->db->where('date(end_date) <=', date('Y-m-d'));
                        $this->db->where('currency', $currency);
                        $this->db->where('status', '1');
                        $query = $this->db->get();
                        $alumni_discount_data = $query->row();
                        if($alumni_discount_data)
                        {
                            $amount = $alumni_discount_data->amount;
                            $id_discount = $alumni_discount_data->id;

                            $employee_insert = array(
                                'id_main_invoice' => $inserted_id,
                                'id_student' => $id_student,
                                'name' => 'Alumni Discount Applied',
                                'amount' => $amount,
                                'id_reference' => $id_discount,
                            );
                            $alumni_inserted_id = $this->addNewMainInvoiceDiscountDetail($employee_insert);
                            if($alumni_inserted_id)
                            {
                                $total_amount = $total_amount - $alumni_discount_data->amount;
                                $alumni_discount_amount = $alumni_discount_data->amount;
                            }
                        }
                    }


                    $total_discount_amount = $sibling_discount_amount + $employee_discount_amount + $alumni_discount_amount;
                    // $total_amount = number_format($total_amount, 2, '.', ',');
                    // echo "<Pre>";print_r($total_amount);exit;

                    $invoice_update['total_amount'] = $total_amount;
                    $invoice_update['balance_amount'] = $total_amount;
                    $invoice_update['invoice_total'] = $total_invoice_amount;
                    $invoice_update['total_discount'] = $total_discount_amount;
                    $invoice_update['paid_amount'] = '0';
                    // $invoice_update['inserted_id'] = $inserted_id;
                    // echo "<Pre>";print_r($invoice_update);exit;
                    $this->editMainInvoice($invoice_update,$inserted_id);
                }
        }

        return $inserted_id;
    }

    function getFeeSetup($id)
    {
        $this->db->select('fst.*');
        $this->db->from('fee_setup as fst');
        $this->db->where('fst.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getFeeStructureMaster($id_programme)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_programme', $id_programme);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getFinanceConfiguration()
    {
        $this->db->select('*');
        $this->db->from('finance_configuration');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureMasterById($id)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getFeeStructureByData($data)
    {
        $this->db->select('fst.*, fs.name as fee_structure, fs.code as fee_structure_code, cs.name as currency_name');
        $this->db->from('fee_structure as fst');
        $this->db->join('fee_setup as fs', 'fst.id_fee_item = fs.id','left');
        $this->db->join('currency_setup as cs', 'fst.currency = cs.id','left');
        $this->db->where('fst.id_programme', $data['id_programme']);
        $this->db->where('fst.id_program_landscape', $data['id_fee_structure_master']);
        $this->db->where('fst.currency', $data['currency']);
        // $this->db->where('fstp.name', $data['trigger']);
        $this->db->where('fst.status', '1');
        $query = $this->db->get();

        $result = $query->result();
        // echo "<Pre>";print_r($result);exit();
        return $result;
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('performa_main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceHasStudents($id_students,$id_main_invoice,$amount)
    {
        $student_count = count($id_students);
        foreach ($id_students as $id_student)
        {
            // echo "<Pre>";print_r($id_student);exit;

            $details['id_student'] = $id_student;
            $details['id_main_invoice'] = $id_main_invoice;
            $details['amount'] = $amount / $student_count;

            $this->db->trans_start();
            $this->db->insert('performa_main_invoice_has_students', $details);
            $insert_id = $this->db->insert_id();
            $this->db->trans_complete();

        }
        
        return $insert_id;
    }

    function addNewMainInvoiceDiscountDetail($data)
    {
        $this->db->trans_start();
        $this->db->insert('performa_main_invoice_discount_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('performa_main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('performa_main_invoice', $data);
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('performa_main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "PIN" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewStudentHasProgramme($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgramme($id)
    {
        $this->db->select('ihs.*');
        $this->db->from('programme as ihs');
        $this->db->where('ihs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructureByIdProgramme($id_programme)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_master');
        $this->db->where('id_programme', $id_programme);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        $result = $query->row();
        // print_r($result);exit();     
        return $result;
    }

    function editStudent($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('student', $data);
        return TRUE;
    }
}