<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends CI_Model
{

    function partnerUniversityListByStatus($status)
    {
        $this->db->select('d.*');
        $this->db->from('partner_university as d');
        $this->db->where('status', $status);
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function companyList()
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function companyUserRoleListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('company_user_role');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('award_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function companyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('program_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function educationLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    
    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->where('status', $status);
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function parentCompanyList($level)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('level', $level);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function categoryTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('category_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function productTypeSetupListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('product_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function schemeList()
    {
        $this->db->select('*');
        $this->db->from('scheme');
        $this->db->order_by("description", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function companyListSearch($data)
    {
        $this->db->select('p.*');
        $this->db->from('company as p');
        if ($data['name'] != '')
        {
            $likeCriteria = "(p.name  LIKE '%" . $data['name'] . "%' or p.registration_number  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['level'] != '')
        {
            $this->db->where('p.level', $data['level']);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCompanyDetails($id)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkCompanyUserPassword($data)
    {
        $this->db->select('*');
        $this->db->from('company_users');
        $this->db->where('id_company', $data['id_company']);
        $this->db->where('id', $data['id_company_user']);
        $this->db->where('password', $data['password']);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgrammeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('company', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCompanyDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('company', $data);
        return TRUE;
    }

    function addNewTempProgrammeHasDean($data)
    {

        $this->db->trans_start();
        $this->db->insert('temp_company_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasDean($data)
    {
        $this->db->trans_start();
        $this->db->insert('company_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewProgrammeHasCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('company_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addTempDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_company_has_dean', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addTempCourseDetails($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('temp_company_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateTempDetails($data,$id) {
        $this->db->where('id', $id);
        $this->db->update('temp_company_has_dean', $data);
        return TRUE;
    }

    function updateTempCourseDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('temp_company_has_course', $data);
        return TRUE;
    }

    function editCompanyUser($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('company_users', $data);
        return TRUE;
    }

    function getTempProgrammeHasDean($id_session)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation');
        $this->db->from('temp_company_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempProgrammeHasCourse($id_session)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('temp_company_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getProgrammeHasDean($id_company)
    {
        $this->db->select('tphd.*, st.name as staff, st.salutation');
        $this->db->from('company_has_dean as tphd');
        $this->db->join('staff as st', 'tphd.id_staff = st.id');     
        $this->db->where('tphd.id_company', $id_company);
        $this->db->order_by("st.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function statusListByType($type)
    {
        $this->db->select('*');
        $this->db->from('status_table');
        $this->db->where('type', $type);
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getProgrammeHasCourse($id)
    {
        $this->db->select('tphd.*, c.name as courseName');
        $this->db->from('company_has_course as tphd');
        $this->db->join('course as c', 'tphd.id_course = c.id');     
        $this->db->where('tphd.id_company', $id);
        $this->db->order_by("c.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_company_has_dean');
    }

    function deleteTempProgHasDeanDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_company_has_dean');
    }

    function deleteTempCourseData($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_company_has_course');
    }

    function deleteProgrammeHasDean($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('company_has_dean');
    }

    function deleteProgrammeHasCourse($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('company_has_course');
    }

    function addTempProgramHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_company_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempProgrammeHasScheme($id_session)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('temp_company_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');     
        $this->db->where('tphd.id_session', $id_session);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempProgramHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('temp_company_has_scheme');
        return TRUE;
    }

    function deleteTempProgramHasSchemeBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_company_has_scheme');
        return TRUE;
    }

    function addNewProgrammeHasScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('company_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getProgrammeHasScheme($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('company_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');
        $this->db->where('tphd.id_program', $id);
        $this->db->order_by("sc.description", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function companyObjectiveList($id)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_objective as tphd');
        $this->db->where('tphd.id_company', $id);
        $query = $this->db->get();
        return $query->result();
    }


    function companyCourseOverviewList($id_company)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_course_overview as tphd');
        $this->db->where('tphd.id_company', $id_company);
        $query = $this->db->get();
        return $query->result();
    }


    function companyDeliveryModeList($id_company)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_delivery_mode as tphd');
        $this->db->where('tphd.id_company', $id_company);
        $query = $this->db->get();
        return $query->result();
    }


    function companyAssesmentList($id_company)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_assesment as tphd');
        $this->db->where('tphd.id_company', $id_company);
        $query = $this->db->get();
        return $query->result();
    }


    function companySyllabusList($id_company)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_syllabus as tphd');
        $this->db->where('tphd.id_company', $id_company);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteProgrammeHasScheme($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('company_has_scheme');
        return TRUE;
    }

    function companyLearningModeList($id_program)
    {
        $this->db->select('tphd.*, sc.name as program_name, sc.code as program_code');
        $this->db->from('company_has_scheme as tphd');
        $this->db->join('program_type as sc', 'tphd.id_program_type = sc.id');     
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function companySchemeList($id)
    {
        $this->db->select('tphd.*, sc.description as scheme_name, sc.code as scheme_code');
        $this->db->from('program_has_scheme as tphd');
        $this->db->join('scheme as sc', 'tphd.id_scheme = sc.id');
        $this->db->where('tphd.id_program', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function companyMajoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_major_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function companyMinoringList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_minor_details as tphd');  
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function companyConcurrentList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_concurrent_program as tphd');   
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function companyAcceredationList($id_program)
    {
        $this->db->select('tphd.*');
        $this->db->from('program_has_acceredation_details as tphd');
        $this->db->where('tphd.id_program', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewProgrammeHasMajor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_major_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasMinor($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_minor_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasConcurrent($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_concurrent_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgrammeHasAcceredation($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_acceredation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewProgramSchemeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function saveCompanyUserDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('company_users', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteCompanyUsers($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('company_users');
        return TRUE;
    }

    function getCompanyUser($id_user)
    {
        $this->db->select('tphd.*, cur.name as user_role');
        $this->db->from('company_users as tphd');  
        $this->db->join('company_user_role as cur', 'tphd.id_user_role = cur.id');
        $this->db->where('tphd.id', $id_user);
        $query = $this->db->get();
        return $query->row();
    }
}