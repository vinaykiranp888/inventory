<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Company extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('company_model');
        $this->isCompanyUserLoggedIn();
    }

    function edit()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        $id = $id_company;
        
        $resultprint = $this->input->post();

        if($resultprint)
        {
            // echo "<Pre>";print_r($resultprint);exit;

            // if($resultprint)
            // {
            
            // echo "<Pre>"; print_r($resultprint['btn']);exit();

            $tab = $resultprint['btn'];


            switch ($tab)
            {

                case 'company':

                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $level = $this->security->xss_clean($this->input->post('level'));
                $id_parent_company = $this->security->xss_clean($this->input->post('id_parent_company'));
                $registration_number = $this->security->xss_clean($this->input->post('registration_number'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
                $website = $this->security->xss_clean($this->input->post('website'));
                $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $ceo = $this->security->xss_clean($this->input->post('ceo'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $country_code = $this->security->xss_clean($this->input->post('country_code'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $corporate_membership_status = $this->security->xss_clean($this->input->post('corporate_membership_status'));
                $staff_strength = $this->security->xss_clean($this->input->post('staff_strength'));
                $debtor_code = $this->security->xss_clean($this->input->post('debtor_code'));
                $credit_term = $this->security->xss_clean($this->input->post('credit_term'));
                $control_account = $this->security->xss_clean($this->input->post('control_account'));
                $credit_term_code = $this->security->xss_clean($this->input->post('credit_term_code'));
                $primary_contact_name = $this->security->xss_clean($this->input->post('primary_contact_name'));
                $primary_contact_designation = $this->security->xss_clean($this->input->post('primary_contact_designation'));
                $primary_contact_email = $this->security->xss_clean($this->input->post('primary_contact_email'));
                $primary_country_code = $this->security->xss_clean($this->input->post('primary_country_code'));
                $primary_phone = $this->security->xss_clean($this->input->post('primary_phone'));
                $second_contact_name = $this->security->xss_clean($this->input->post('second_contact_name'));
                $second_contact_designation = $this->security->xss_clean($this->input->post('second_contact_designation'));
                $second_contact_email = $this->security->xss_clean($this->input->post('second_contact_email'));
                $second_country_code = $this->security->xss_clean($this->input->post('second_country_code'));
                $second_phone = $this->security->xss_clean($this->input->post('second_phone'));
                $staff_credit = $this->security->xss_clean($this->input->post('staff_credit'));
                $status = $this->security->xss_clean($this->input->post('status'));



            
                $company_data = array(
                    'level' => $level,
                    'id_parent_company' => $id_parent_company,
                    'registration_number' => $registration_number,
                    'name' => $name,
                    'joined_date' => date('Y-m-d', strtotime($joined_date)),
                    'website' => $website,
                    'institution_type' => $institution_type,
                    'chairman' => $chairman,
                    'ceo' => $ceo,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'country_code' => $country_code,
                    'phone' => $phone,
                    'corporate_membership_status' => $corporate_membership_status,
                    'staff_strength' => $staff_strength,
                    'debtor_code' => $debtor_code,
                    'credit_term' => $credit_term,
                    'control_account' => $control_account,
                    'credit_term_code' => $credit_term_code,
                    'primary_contact_name' => $primary_contact_name,
                    'primary_contact_designation' => $primary_contact_designation,
                    'primary_contact_email' => $primary_contact_email,
                    'primary_country_code' => $primary_country_code,
                    'primary_phone' => $primary_phone,
                    'second_contact_name' => $second_contact_name,
                    'second_contact_designation' => $second_contact_designation,
                    'second_contact_email' => $second_contact_email,
                    'second_country_code' => $second_country_code,
                    'second_phone' => $second_phone,
                    'staff_credit' => $staff_credit
                );


                if($_FILES['image'])
                {  


                $certificate_name = $_FILES['image']['name'];
                $certificate_size = $_FILES['image']['size'];
                $certificate_tmp =$_FILES['image']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');

                }

                if($image_file != '')
                {
                    $company_data['image'] = $image_file;
                    $sessionArray = array(
                    'company_image' => $image_file
                    );
                }

            

                // echo "<Pre>";print_r($data);exit;
                
                $result = $this->company_model->editCompanyDetails($company_data,$id);
                if($result && $sessionArray)
                {
                    $this->session->set_userdata($sessionArray);
                }

                redirect($_SERVER['HTTP_REFERER']);
                    // redirect('/af/staff/list');

                break;




                case 'user':


                $name = $this->security->xss_clean($this->input->post('user_name1'));
                $phone = $this->security->xss_clean($this->input->post('user_number'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $designation = $this->security->xss_clean($this->input->post('user_designation'));
                $user_name = $this->security->xss_clean($this->input->post('user_name'));
                $id_user_role = $this->security->xss_clean($this->input->post('id_user_role'));
                $status = $this->security->xss_clean($this->input->post('active_status'));
                $password = $this->security->xss_clean($this->input->post('user_password'));



            
                $user_data = array(
                    'name' => $name,
                    'phone' => $phone,
                    'designation' => $designation,
                    'user_name' => $user_name,
                    'id_user_role' => $id_user_role
                );

                // echo "<Pre>";print_r($data);exit;
                
                $result = $this->company_model->editCompanyUser($user_data,$id_company_user);

                redirect($_SERVER['HTTP_REFERER']);
                    // redirect('/af/staff/list');

                break;
            }

        }

        $data['id_company'] = $id;
        
        $data['countryList'] = $this->company_model->countryListByStatus('1');
        $data['companyUserRoleList'] = $this->company_model->companyUserRoleListByStatus('1');
        $data['parentCompanyList'] = $this->company_model->parentCompanyList('Parent');
        $data['statusListByType'] = $this->company_model->statusListByType('Company');

        $data['companyDetails'] = $this->company_model->getCompanyDetails($id);
        $data['companyUser'] = $this->company_model->getCompanyUser($id_company_user);
        
        // echo "<Pre>";print_r($data['companyUser']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Edit Company';
        $this->loadViews("company/edit", $this->global, $data, NULL);
    }

    function changePassword()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        $id = $id_company;
        
        $resultprint = $this->input->post();

        if($resultprint)
        {
            // echo "<Pre>";print_r($resultprint);exit;
            $new_password = $this->security->xss_clean($this->input->post('new_password'));
            $data_company_user['password'] = md5($new_password);

            $updated = $this->company_model->editCompanyUser($data_company_user,$id_company_user);

            redirect('/company_user/company/edit');
        }

        $data['id_company'] = $id_company;
        $data['id_company_user'] = $id_company_user;

        $data['companyUserRoleList'] = $this->company_model->companyUserRoleListByStatus('1');
        
        $data['companyDetails'] = $this->company_model->getCompanyDetails($id_company);
        $data['companyUser'] = $this->company_model->getCompanyUser($id_company_user);
        
        // echo "<Pre>";print_r($data['companyUserRoleList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Change Company User Password';
        $this->loadViews("company/change_password", $this->global, $data, NULL);
    }

    function getStateByCountry($id_country)
    {
            $results = $this->company_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>  
                    $('select').select2();
                </script>
                ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }

    function checkCompanyUserPassword()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        // echo "<Pre>"; print_r($tempData);exit();
        
        $tempData['password'] = md5($tempData['old_password']);
        $company_user = $this->company_model->checkCompanyUserPassword($tempData);

        if($company_user)
        {
            echo '1';exit;
        }
        else
        {
            echo '0';exit;
        }
    }

    function changeCompanyUserPassword()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        echo "<Pre>"; print_r($tempData);exit();
        
        $tempData['password'] = md5($tempData['new_password']);
        $id_company_user = $tempData['id_company_user'];
        unset($tempData['id_company_user']);

        $updated = $this->company_model->editCompanyUser($tempData,$id_company_user);

        echo $updated;exit;
    }

    function saveCompanyUserDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
            // echo "<Pre>"; print_r($tempData);exit();
        $tempData['password'] = md5($tempData['password']);
        $inserted_id = $this->company_model->saveCompanyUserDetails($tempData);

        if($inserted_id)
        {
            echo $inserted_id;
        }

    }

    function deleteCompanyUsers($id_details)
    {
        $inserted_id = $this->company_model->deleteCompanyUsers($id_details);
        
        echo "Success"; 
    }

    function uploadImage()
    {
        $resultprint = $this->input->post();

        $id = $this->security->xss_clean($this->input->post('id'));
        
        if($_FILES['image'])
        {  


        $certificate_name = $_FILES['image']['name'];
        $certificate_size = $_FILES['image']['size'];
        $certificate_tmp =$_FILES['image']['tmp_name'];
        
        // echo "<Pre>"; print_r($certificate_tmp);exit();

        $certificate_ext=explode('.',$certificate_name);
        $certificate_ext=end($certificate_ext);
        $certificate_ext=strtolower($certificate_ext);


        $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

        $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');

            if($image_file != '')
            {
                $company_data['image'] = $image_file;
                $sessionArray = array(
                'company_image' => $image_file
                );
            }

        

            // echo "<Pre>";print_r($data);exit;
            
            $result = $this->company_model->editCompanyDetails($company_data,$id);
            if($result && $sessionArray)
            {
                $this->session->set_userdata($sessionArray);
            }

            redirect($_SERVER['HTTP_REFERER']);

        }
    }
}