<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Applicant extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;
    
        $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
        $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
        $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
        $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
        $formData['id_company_user'] = $id_company_user;
        $formData['id_company'] = $id_company;

        $data['searchParam'] = $formData;
        $data['applicantList'] = $this->applicant_model->applicantList($formData);

        // echo "<pre>";print_r($data['applicantList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Applicant';
        $this->loadViews("applicant/list", $this->global, $data, NULL);
        
    }
    
    function add()
    {

        if($this->input->post())
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->company_user_session_id;
            $id_company = $this->session->id_company;
            $id_company_user = $this->session->id_company_user;


            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
            $country_code = $this->security->xss_clean($this->input->post('country_code'));
            $id_programme = $this->security->xss_clean($this->input->post('id_programme'));

            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            $id_type = $this->security->xss_clean($this->input->post('id_type'));


            $whatsapp_number = $this->security->xss_clean($this->input->post('whatsapp_number'));
            $linked_in = $this->security->xss_clean($this->input->post('linked_in'));
            $facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
            $twitter_id = $this->security->xss_clean($this->input->post('twitter_id'));
            $ig_id = $this->security->xss_clean($this->input->post('ig_id'));

           
            $salutationInfo = $this->applicant_model->getSalutation($salutation);
            $fee_structure = $this->applicant_model->getFeeStructureByIdProgramme($id_programme);
            
            // echo "<Pre>"; print_r($id_programme);exit;

            if($nric == '')
            {
                $nric = $passport;
            }



            $data = array(

                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'country_code' => $country_code,
                'contact_email' => $contact_email,
                'email_id' => $email_id,
                'password' => md5($password),
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'id_type' => $id_type,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                'is_hostel' => 1,
                'applicant_status' => 'Approve',
                'nationality' => $nationality,
                'id_program' => $id_programme,
                'id_race' => $id_race,
                'id_company' => $id_company,
                'id_company_user' => $id_company_user,
                'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'whatsapp_number' => $whatsapp_number,
                'linked_in' => $linked_in,
                'facebook_id' => $facebook_id,
                'twitter_id' => $twitter_id,
                'ig_id' => $ig_id,
                'phd_duration' => 1,
                'id_program_landscape' => 0,
                'submitted_date' => date('Y-m-d H:i:s')             
            );

            // echo "<Pre>"; print_r($fee_structure);exit;

            $inserted_id = $this->applicant_model->addNewStudent($data);
            redirect('/company_user/applicant/list');
        }

        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');


        $this->global['pageTitle'] = 'Corporate User Portal : Add Applicant';
        $this->loadViews("applicant/add", $this->global, $data, NULL);
    }


    function edit($id = NULL)
    {
        if ($id == null)
        {
            redirect('/company_user/applicant/list');
        }


        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;

        if($this->input->post())
        {
            $salutation = $this->security->xss_clean($this->input->post('salutation'));
            $first_name = $this->security->xss_clean($this->input->post('first_name'));
            $last_name = $this->security->xss_clean($this->input->post('last_name'));
            $phone = $this->security->xss_clean($this->input->post('phone'));
            $email_id = $this->security->xss_clean($this->input->post('email_id'));
            $contact_email = $this->security->xss_clean($this->input->post('contact_email'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $passport = $this->security->xss_clean($this->input->post('passport'));
            $nric = $this->security->xss_clean($this->input->post('nric'));
            $gender = $this->security->xss_clean($this->input->post('gender'));
            $date_of_birth = $this->security->xss_clean($this->input->post('date_of_birth'));
            $martial_status = $this->security->xss_clean($this->input->post('martial_status'));
            $religion = $this->security->xss_clean($this->input->post('religion'));
            $nationality = $this->security->xss_clean($this->input->post('nationality'));
            $id_race = $this->security->xss_clean($this->input->post('id_race'));
            $country_code = $this->security->xss_clean($this->input->post('country_code'));
            $id_programme = $this->security->xss_clean($this->input->post('id_programme'));

            $present_address_same_as_mailing_address = $this->security->xss_clean($this->input->post('present_address_same_as_mailing_address'));
            $mail_address1 = $this->security->xss_clean($this->input->post('mail_address1'));
            $mail_address2 = $this->security->xss_clean($this->input->post('mail_address2'));
            $mailing_country = $this->security->xss_clean($this->input->post('mailing_country'));
            $mailing_state = $this->security->xss_clean($this->input->post('mailing_state'));
            $mailing_city = $this->security->xss_clean($this->input->post('mailing_city'));
            $mailing_zipcode = $this->security->xss_clean($this->input->post('mailing_zipcode'));
            $permanent_address1 = $this->security->xss_clean($this->input->post('permanent_address1'));
            $permanent_address2 = $this->security->xss_clean($this->input->post('permanent_address2'));
            $permanent_country = $this->security->xss_clean($this->input->post('permanent_country'));
            $permanent_state = $this->security->xss_clean($this->input->post('permanent_state'));
            $permanent_city = $this->security->xss_clean($this->input->post('permanent_city'));
            $permanent_zipcode = $this->security->xss_clean($this->input->post('permanent_zipcode'));
            $id_type = $this->security->xss_clean($this->input->post('id_type'));


            $whatsapp_number = $this->security->xss_clean($this->input->post('whatsapp_number'));
            $linked_in = $this->security->xss_clean($this->input->post('linked_in'));
            $facebook_id = $this->security->xss_clean($this->input->post('facebook_id'));
            $twitter_id = $this->security->xss_clean($this->input->post('twitter_id'));
            $ig_id = $this->security->xss_clean($this->input->post('ig_id'));

           
            $salutationInfo = $this->applicant_model->getSalutation($salutation);
            $fee_structure = $this->applicant_model->getFeeStructureByIdProgramme($id_programme);
            
            // echo "<Pre>"; print_r($id_programme);exit;

            if($nric == '')
            {
                $nric = $passport;
            }


            $data = array(
                'salutation' => $salutation,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'full_name' => $salutationInfo->name . ". " . $first_name." ".$last_name,
                'phone' => $phone,
                'country_code' => $country_code,
                'contact_email' => $contact_email,
                'email_id' => $email_id,
                'nric' => $nric,
                'passport' => $passport,
                'gender' => $gender,
                'id_type' => $id_type,
                'date_of_birth' => date("Y-m-d", strtotime($date_of_birth)),
                'martial_status' => $martial_status,
                'religion' => $religion,
                'nationality' => $nationality,
                'id_program' => $id_programme,
                'id_race' => $id_race,
                'id_company' => $id_company,
                'id_company_user' => $id_company_user,
                'present_address_same_as_mailing_address' => $present_address_same_as_mailing_address,
                'mail_address1' => $mail_address1,
                'mail_address2' => $mail_address2,
                'mailing_country' => $mailing_country,
                'mailing_state' => $mailing_state,
                'mailing_city' => $mailing_city,
                'mailing_zipcode' => $mailing_zipcode,
                'permanent_address1' => $permanent_address1,
                'permanent_address2' => $permanent_address2,
                'permanent_country' => $permanent_country,
                'permanent_state' => $permanent_state,
                'permanent_city' => $permanent_city,
                'permanent_zipcode' => $permanent_zipcode,
                'whatsapp_number' => $whatsapp_number,
                'linked_in' => $linked_in,
                'facebook_id' => $facebook_id,
                'twitter_id' => $twitter_id,
                'ig_id' => $ig_id           
            );
            
            // $result = $this->applicant_model->editEmployeeDetails($employData, $id);

            $result = $this->applicant_model->editApplicantDetails($data,$id);
            redirect('/company_user/applicant/list');

        }

        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
        
        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetails($id);
        // $data['companyDetails'] = $this->applicant_model->getCompanyDetails($id_company);

            // echo "<Pre>"; print_r($this->session->id_company);exit;

        // echo "<Pre>"; print_r($data['degreeTypeList']);exit;

        $this->global['pageTitle'] = 'Corporate User Portal : Edit Applicant';
        $this->loadViews("applicant/edit", $this->global, $data, NULL);
        
    }

    function approvalList()
    {
        $data['intakeList'] = $this->applicant_model->intakeList();
        $data['programList'] = $this->applicant_model->programList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));

        $data['applicantList'] = $this->applicant_model->applicantListForApproval($formData);
        $data['searchParam'] = $formData;



        $this->global['pageTitle'] = 'Corporate User Portal : Applicant Approval';
        //print_r($subjectDetails);exit;
        $this->loadViews("applicant/approval_list", $this->global, $data, NULL);
        
    }

    function view($id = NULL)
    {
        if ($id == null)
        {
            redirect('/company_user/applicant/list');
        }

        $id_company = $this->session->id_company;
        $id_company_user = $this->session->id_company_user;


        if($this->input->post())
        {
            
            $applicant_status = $this->security->xss_clean($this->input->post('applicant_status'));


            $data = array(
                'applicant_status' => $applicant_status,
                'approved_dt_tm' => date('Y-m-d')
            );
            $result = $this->applicant_model->editApplicantDetails($data,$id);
            if ($applicant_status == "Approved")
            {
                $this->applicant_model->createNewMainInvoiceForStudent($id);
                // $insert_id =  $this->applicant_approval_model->addNewStudent($id);
                // if($insert_id)
                // {
                //     $this->applicant_approval_model->addStudentProfileDetail($insert_id);

                    // echoPavan "<Pre>";print_r($insert_id);exit;
                    // $this->applicant_approval_model->createNewMainInvoiceForStudent($id,'4');
                // }
            }
            redirect('/company_user/applicant/list');
        }

        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        $data['companyUserRoleList'] = $this->applicant_model->companyUserRoleListByStatus('1');
        $data['parentCompanyList'] = $this->applicant_model->parentCompanyList('Parent');
        $data['salutationList'] = $this->applicant_model->salutationListByStatus('1'); 
        $data['nationalityList'] = $this->applicant_model->nationalityListByStatus('1');
        $data['countryList'] = $this->applicant_model->countryListByStatus('1');
        
        // $data['companyDetails'] = $this->applicant_model->getCompanyDetails($id_company);

            // echo "<Pre>"; print_r($this->session->id_company);exit;

        $data['getApplicantDetails'] = $this->applicant_model->getApplicantDetails($id);

        $this->global['pageTitle'] = 'Corporate User Portal : View Applicant Approval';
        $this->loadViews("applicant/view", $this->global, $data, NULL);
        
    }

    function getStateByCountry($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>  
                    $('select').select2();
                </script>
                ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }

    function getStateByCountryPermanent($id_country)
    {
            $results = $this->applicant_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
        <script type='text/javascript'>
             $('select').select2();
         </script>
         ";

            $table.="
            <select name='permanent_state' id='permanent_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->applicant_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='checkFeeStructure()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function checkFeeStructure()
    {
        $id_session = $this->session->company_user_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $result = $this->applicant_model->checkFeeStructure($tempData);
        if($result == '')
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }
    }

    function getIntakeDetails($id_intake)
    {
        $intake_data = $this->applicant_model->getIntakeDetails($id_intake);
        // echo "<Pre>"; print_r($intake_data);exit;

        $is_alumni_discount = $intake_data->is_alumni_discount;
        $is_employee_discount = $intake_data->is_employee_discount;
        $is_sibbling_discount = $intake_data->is_sibbling_discount;


        $table = "

        <input type='hidden' name='is_intake_alumni_discount' id='is_intake_alumni_discount' value='$is_alumni_discount' />
        <input type='hidden' name='is_intake_employee_discount' id='is_intake_employee_discount' value='$is_employee_discount' />
        <input type='hidden' name='is_intake_sibbling_discount' id='is_intake_sibbling_discount' value='$is_sibbling_discount' />";
        
        echo $table;
    }

    function getEmailIdDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id = $tempData['id_employee'];
        // $email_id = $tempData['email_id'];

        $result = $this->applicant_model->getEmailIdDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }       
    }

    function getNRICDuplication()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id = $tempData['id_employee'];
        // $email_id = $tempData['email_id'];

        $result = $this->applicant_model->getEmailIdDuplication($tempData);

        if($result)
        {
            print_r('0');exit;
        }
        else
        {
            print_r('1');exit;
        }  
    }
}
