<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Organisation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('organisation_model');
                $this->load->model('role_model');
        
        $this->isLoggedIn();
    }

    function edit()
    {
        if ($this->checkAccess('organisation.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $formData = $this->input->post();

                if($_FILES['image'])
                {
                // echo "<Pre>"; print_r($_FILES['image']);exit;

                    $certificate_name = $_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext=explode('.',$certificate_name);
                    $certificate_ext=end($certificate_ext);
                    $certificate_ext=strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                    $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                }


                // echo "<Pre>"; print_r($formData);exit;

                // $filename = $_FILES['image']['name']; //Command to assign the value
                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_registrar = $this->security->xss_clean($this->input->post('id_registrar'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                // $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $gst_in = $this->security->xss_clean($this->input->post('gst_in'));
                $cin = $this->security->xss_clean($this->input->post('cin'));
                $id_organisation = $this->security->xss_clean($this->input->post('id_organisation'));

            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'id_registrar' => $id_registrar,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'contact_number' => $contact_number,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'gst_in' => $gst_in,
                    'cin' => $cin,
                    // 'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                if($image_file)
                {
                    $data['image'] = $image_file;
                }

                // echo "<Pre>"; print_r($data);exit;

                $result = $this->organisation_model->editOrganisation($data,$id_organisation);
                redirect('/setup/organisation/edit');
            }
            $data['organisation'] = $this->organisation_model->getOrganisation();
            // echo "<Pre>";print_r($data);exit();
            $data['id_organisation'] = $data['organisation']->id;
            $data['stateList'] = $this->organisation_model->stateListByStatus('1');
            $data['countryList'] = $this->organisation_model->countryListByActivity('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Organisation';
            $this->loadViews("organisation/edit", $this->global, $data, NULL);
        }
    }

    function addOrganisationComitee()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->organisation_model->addNewOrganisationComitee($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteOrganisationConitee($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->organisation_model->deleteOrganisationComitee($id);
        
        echo "Success"; 
    }

     function addTrainingCenter()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->organisation_model->addTrainingCenter($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteTrainingCenter($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->organisation_model->deleteTrainingCenter($id);
        
        echo "Success"; 
    }

    function getStateByCountryForTraining($id_country)
    {
            $results = $this->organisation_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($results);exit;
         
            $table.="
            <select name='id_training_state' id='id_training_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function getOrganisationBranchProgram()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $program_details = $this->organisation_model->getOrganisationBranchProgram($tempData);
        // echo "<Pre>"; print_r($program_details);exit();

         if(!empty($program_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table = "

        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Program</th>
                    <th>Action</th>
                    </tr>
                </thead>";

                $total_detail = 0;
                    for($i=0;$i<count($program_details);$i++)
                    {
                        $id = $program_details[$i]->id;
                        $program_code = $program_details[$i]->program_code;
                        $program_name = $program_details[$i]->program_name;
                        $j=$i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$program_code - $program_name</td>
                            
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteProgramData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $table = "
            ";
        }
        print_r($table);exit();

    }

    function addProgramData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $program_details = $this->organisation_model->addOrganisationBranchProgram($tempData);

        echo "success";

    }

    function deleteProgramDatail($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      $inserted_id = $this->organisation_model->deleteProgramDatail($id);
        
        echo "Success"; 
    }

    function uploadUserImage()
    {
        $resultprint = $this->input->post();

        $id = $this->security->xss_clean($this->input->post('id'));

        
        // echo "<Pre>"; print_r($id);exit();

        // echo "<Pre>"; print_r($_FILES['image']);exit;

        if($_FILES['image'])
        {  


        $certificate_name = $_FILES['image']['name'];
        $certificate_size = $_FILES['image']['size'];
        $certificate_tmp =$_FILES['image']['tmp_name'];
        
        // echo "<Pre>"; print_r($certificate_tmp);exit();

        $certificate_ext=explode('.',$certificate_name);
        $certificate_ext=end($certificate_ext);
        $certificate_ext=strtolower($certificate_ext);


        $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

        $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');

        

            if($image_file != '')
            {
                $data['image'] = $image_file;

                $sessionArray = array(
                    'user_image' => $image_file
                    );

            }

            // echo "<Pre>"; print_r($data);exit;
            
            $result = $this->organisation_model->editUser($data,$id);

            if($result && $sessionArray)
            {
                $this->session->set_userdata($sessionArray);
            }

            redirect($_SERVER['HTTP_REFERER']);

        }
    }
}
