<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ChecklistVisa extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('checklist_visa_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('checklist_visa.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_university'] = $this->security->xss_clean($this->input->post('id_university'));
            $formData['id_education_level'] = $this->security->xss_clean($this->input->post('id_education_level'));
            $data['searchParam'] = $formData;

            $data['universityList'] = $this->checklist_visa_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->checklist_visa_model->educationLevelListByStatus('1');
            $data['organisation'] = $this->checklist_visa_model->getOrganisaton();

            $data['checklistVisaList'] = $this->checklist_visa_model->checklistVisaListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Student Checklist Visa List';
            $this->loadViews("checklist_visa/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('checklist_visa.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'id_education_level' => $id_education_level,
                    'id_university' => $id_university,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->checklist_visa_model->addNewChecklistVisa($data);
                redirect('/setup/checklistVisa/list');
            }

            $data['universityList'] = $this->checklist_visa_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->checklist_visa_model->educationLevelListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Add New Student Checklist Visa';
            $this->loadViews("checklist_visa/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('checklist_visa.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/checklistVisa/list');
            }
            if($this->input->post())
            {
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_education_level = $this->security->xss_clean($this->input->post('id_education_level'));
                $id_university = $this->security->xss_clean($this->input->post('id_university'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'description' => $description,
                    'id_education_level' => $id_education_level,
                    'id_university' => $id_university,
                    'status' => $status
                );

                $result = $this->checklist_visa_model->editChecklistVisa($data,$id);
                redirect('/setup/checklistVisa/list');
            }

            $data['universityList'] = $this->checklist_visa_model->getUniversityListByStatus('1');
            $data['educationLevelList'] = $this->checklist_visa_model->educationLevelListByStatus('1');

            $data['checklistVisa'] = $this->checklist_visa_model->getChecklistVisa($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit New Student Checklist Visa';
            $this->loadViews("checklist_visa/edit", $this->global, $data, NULL);
        }
    }
}
