<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Welcome extends BaseController
{
	 public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();
        $this->load->model('role_model');

    }

    public function index()
    {
        $id_role = $this->session->role;
        $data['id_role'] = $id_role;
        // $data['student_count'] = $this->role_model->getStudentsCount();
        // $data['employee_count'] = $this->role_model->getStudentsCountOfCompany();
        // $data['company_active_count'] = $this->role_model->getCompanyCount('1');
        // $data['company_pending_count'] = $this->role_model->getCompanyCount('0');

        $this->global['pageTitle'] = 'Inventory Management : Welcome To Curriculum Management';        
        $this->loadViews("includes/welcome", $this->global, $data , NULL);
    }   
}