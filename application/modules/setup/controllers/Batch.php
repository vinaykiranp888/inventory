<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Batch extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('batch_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('batch.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;

            $data['batchList'] = $this->batch_model->batchListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : Batch List';
            $this->loadViews("batch/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('batch.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {


            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'description' => $description,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->batch_model->addNewBatch($data);

                // echo "<Pre>";// print_r($data);exit();


                redirect('/setup/batch/list');
            }

            // $data['programmeList'] = $this->batch_model->programmeList();


            $this->global['pageTitle'] = 'Inventory Management : Add Award';
            $this->loadViews("batch/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('batch.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/batch/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'description' => $description,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->batch_model->editBatch($data,$id);
                
                redirect('/setup/batch/list');
            }

            $data['id_batch'] = $id;
            $data['batch'] = $this->batch_model->getBatch($id);

            // echo "<Pre>";print_r($data['batch']);exit();

            
            $this->global['pageTitle'] = 'Inventory Management : Edit Batch';
            $this->loadViews("batch/edit", $this->global, $data, NULL);
        }
    }


    function programme($id = NULL)
    {
        if ($this->checkAccess('batch.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/batch/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
            
                $data = array(
                    'id_batch' => $id,
                    'id_programme' => $id_programme,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'status' => 1,
                    'Created_by' => $id_user
                );

                $result = $this->batch_model->addBatchhasProgramme($data);
                redirect($_SERVER['HTTP_REFERER']);
            }

            $data['id_batch'] = $id;
            $data['batch'] = $this->batch_model->getBatch($id);
            $data['batchHasProgramme'] = $this->batch_model->getBatchHasProgramme($id);
            $data['programmeList'] = $this->batch_model->getProrammeListByStatus('1');

            // echo "<Pre>";print_r($data['prorammeList']);exit();

            
            $this->global['pageTitle'] = 'Inventory Management : Edit Batch';
            $this->loadViews("batch/programme", $this->global, $data, NULL);
        }
    }

    function deleteBatchHasProgramme($id)
    {
        $deleted = $this->batch_model->deleteBatchHasProgramme($id);
        return TRUE;
    }
}
