<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ClassSubjectTagging extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('class_subject_tagging_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('class_subject_tagging.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['listData'] = $this->class_subject_tagging_model->getClassSubjectTaggingList();
            $this->global['pageTitle'] = 'Inventory Management : Class Subject Listing';
            $this->loadViews("class_subject_tagging/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('class_subject_tagging.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
            
                $insertData = array(
                    'name' => $name
                );
                $result = $this->class_subject_tagging_model->addNewClassSubjectTaggingDetails($insertData);
                redirect('/setup/classDetails/list');
            }

            $this->load->model('class_subject_tagging_model');
            $this->global['pageTitle'] = 'Inventory Management : Add Class Subject School';
            $this->loadViews("class_subject_tagging/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('class_subject_tagging.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/subjectDetails/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $updateData = array(
                    'name' => $name
                );
                $result = $this->class_subject_tagging_model->editClassDetails($updateData,$id);
                redirect('/setup/classDetails/list');
            }
            $data['classDetail'] = $this->class_subject_tagging_model->getClassDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Subject';
            $this->loadViews("class_subject_tagging/edit", $this->global, $data, NULL);
        }
    }
}
