<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Documents extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('documents_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('documents.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParameters'] = $formData;
            $data['documentsList'] = $this->documents_model->documentsListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Documents List';
            $this->loadViews("documents/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('documents.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;


                for($f=0;$f<count($this->input->post('file_format'));$f++) {
                    if($f==0) {
                       $fileformat = $this->input->post('file_format')[$f];
                    } 
                    if($f>0) {
                       $fileformat = $fileformat.",".$this->input->post('file_format')[$f];

                    }
                }

                 $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $is_required = $this->security->xss_clean($this->input->post('is_required'));
                $file_size = $this->security->xss_clean($this->input->post('file_size'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user,
                    'is_required' => $is_required,
                    'file_size' => $file_size,
                    'file_format' =>$fileformat,
                    'created_by' =>$id_user
                );

                $result = $this->documents_model->addNewDocuments($data);
                redirect('/setup/documents/list');
            }
                        $data['fileType'] = $this->documents_model->getFileType();

            $this->global['pageTitle'] = 'Inventory Management : Add Documents';
            $this->loadViews("documents/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('documents.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/documents/list');
            }
            if($this->input->post())
            {

                for($f=0;$f<count($this->input->post('file_format'));$f++) {
                    if($f==0) {
                       $fileformat = $this->input->post('file_format')[$f];
                    } 
                    if($f>0) {
                       $fileformat = $fileformat.",".$this->input->post('file_format')[$f];

                    }
                }
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $is_required = $this->security->xss_clean($this->input->post('is_required'));
                $file_size = $this->security->xss_clean($this->input->post('file_size'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user,
                    'is_required' => $is_required,
                    'file_size' => $file_size,
                    'file_format' =>$fileformat,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->documents_model->editDocuments($data,$id);
                redirect('/setup/documents/list');
            }
            $data['documentDetails'] = $this->documents_model->getDocuments($id);
            $data['fileType'] = $this->documents_model->getFileType();
            $this->global['pageTitle'] = 'Inventory Management : Edit Documents';
            $this->loadViews("documents/edit", $this->global, $data, NULL);
        }
    }



    function addProgram($id_document)
    {
        if ($this->checkAccess('documents.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_document == null)
            {
                redirect('/setup/documents/list');
            }
            if($this->input->post())
            {
                redirect('/setup/documents/list');
            }
            $data['documentDetails'] = $this->documents_model->getDocuments($id_document);
            $data['documentsProgramList'] = $this->documents_model->getDocumentsProgramListByDocumentId($id_document);
            $data['programList'] = $this->documents_model->programListByStatus('1');


            // echo "<Pre>";print_r($data);exit();
                        $data['fileType'] = $this->documents_model->getFileType();

            $this->global['pageTitle'] = 'Inventory Management : Edit Documents';
            $this->loadViews("documents/add_program", $this->global, $data, NULL);
        }
    }

    function addProgramToDocument()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->documents_model->addNewDocumentsProgramDetails($tempData);
        echo $inserted_id;
    }

    function deleteDocumentsProgram($id)
    {
        $inserted_id = $this->documents_model->deleteDocumentsProgramDetails($id);
        echo $inserted_id;
    }
}
