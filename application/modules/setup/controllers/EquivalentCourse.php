<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EquivalentCourse extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('equivalent_course_model');
        $this->load->model('course_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('equivalent_course.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['courseList'] = $this->course_model->courseList();

            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            // $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            // $formData['id_equivalent_course'] = $this->security->xss_clean($this->input->post('id_equivalent_course'));
 
            // $data['searchParameters'] = $formData; 

            $data['courseList'] = $this->equivalent_course_model->getCourseSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Course Equivalent List';
            // echo "<Pre>"; print_r($data['courseList']);exit;
            $this->loadViews("equivalent_course/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('equivalent_course.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
            
            $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $id_equivalent_course = $this->security->xss_clean($this->input->post('id_equivalent_course'));
            $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_course' => $id_course,
                    'id_equivalent_course' => $id_equivalent_course,
                    'status' => $status
                );
                
                $result = $this->equivalent_course_model->addNewEquivalentCourse($data);
                redirect('/setup/equivalentCourse/list');
            }
            //print_r($data['stateList']);exit;
            $data['courseList'] = $this->course_model->courseList();
            $this->global['pageTitle'] = 'Inventory Management : Add Course Equivalent';
            $this->loadViews("equivalent_course/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('equivalent_course.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<Pre>"; print_r($id);exit;
            if ($id == null)
            {
                redirect('/setup/equivalentCourse/list');
            }
            if($this->input->post())
            {
                $course = $id;
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $equi_data = $this->equivalent_course_model->getCourseByIdForCheckup($course);

                // echo "<Pre>";print_r($equi_data);exit;
                if($equi_data)
                {
                    $id_course = $this->security->xss_clean($this->input->post('id_course'));

                    $detailsData = array(
                        'id_equivalent_course' => $equi_data->id,
                        'id_course' => $id_course,
                        'status' => '1',
                        'created_by' => $id_user
                    );
                    $inserted_id = $this->equivalent_course_model->addNewEquivalentCourseDetails($detailsData);
                }
                else
                {
                    $data = array(
                        'id_course' => $id,
                        'status' => '1',
                        'created_by' => $id_user
                    );
                
                    $inserted_id = $this->equivalent_course_model->addNewEquivalentCourse($data);
                    $id_course = $this->security->xss_clean($this->input->post('id_course'));
                

                    $detailsData = array(
                        'id_equivalent_course' => $inserted_id,
                        'id_course' => $id_course,
                        'status' => '1',
                        'created_by' => $id_user
                    );
                    // echo "<Pre>";print_r($detailsData);exit;
                    $result = $this->equivalent_course_model->addNewEquivalentCourseDetails($detailsData);

                }
                redirect($_SERVER['HTTP_REFERER']);
            }

            $e_data = $this->equivalent_course_model->getCourseByIdForCheckup($id);
            
            if($e_data)
            {
                $id_ecd = $e_data->id;
                $data['equivalentCourseDetailsList'] = $this->equivalent_course_model->getEquivalentCourseDetails($id_ecd);
            }
            // echo "<Pre>"; print_r($data['equivalentCourseDetailsList']);exit;

            $data['courseList'] = $this->course_model->courseList();
            $data['courseData'] = $this->course_model->getCourse($id);
            
            // echo "<Pre>"; print_r($data['courseData']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Course Equivalent';
            $this->loadViews("equivalent_course/edit", $this->global, $data, NULL);
        }
    }

    function tempDetailsDataAdd()
    {
        $id_session = $this->session->my_session_id;
        $id_course = $this->security->xss_clean($this->input->post('id_course'));

        $data = array(
                'id_session' => $id_session,
                'id_course' => $id_course
            );
        // echo "<Pre>";  print_r($data);exit;
        $inserted_id = $this->equivalent_course_model->addNewTempEquivalentCourseDetails($data);
        // echo "<Pre>";  print_r($inserted_id);exit;

        $temp_details = array(
                'id' => $inserted_id,
                'id_course' => $id_course,
            );
        $temp_details = $this->equivalent_course_model->getTempEquivalentCourseDetails($id_session);

        if(!empty($temp_details))
        {  
            $table = "
           <div class='custom-table'>
            <table  class='table' id='list-table'>
                <thead>
                <tr>
                    <th>Equivalent Course</th>
                    <th class='text-centre'>Delete</th>
                </tr>
                </thead>";
                for($i=0;$i<count($temp_details);$i++)
                {
                    $id = $temp_details[$i]->id;
                    $course_name = $temp_details[$i]->course_name;

                    $table .= "
                <tbody>
                <tr>
                    <td>$course_name</td>
                    <td>
                        <span onclick='deleteid($id)'>Delete</a>
                    <td>
                </tr>";
                }
                        
            $table .= "
            </tbody>
            </table>";
            echo $table;
        }
    }

    function delete_equivalent_course_details()
    {
        $id = $this->input->get('id');

       $this->equivalent_course_model->deleteEquivalentCourseDetails($id);

       redirect($_SERVER['HTTP_REFERER']);
    }

    function viewHour()
    {
        $id_course = $this->security->xss_clean($this->input->post('id_course'));
        
        $temp_details = $this->equivalent_course_model->getHourDetails($id_course); 
        $crdHrs = $temp_details->credit_hours;
        
        echo $crdHrs;
    }
}
