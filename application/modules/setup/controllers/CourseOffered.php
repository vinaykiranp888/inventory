<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseOffered extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_offered_model');
                        $this->load->model('role_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_offered.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_scheme'] = $this->security->xss_clean($this->input->post('id_scheme'));

            $data['searchParam'] = $formData;

            $data['semesterDetails'] = $this->course_offered_model->semesterListSearch($formData);
            $data['schemeList'] = $this->course_offered_model->schemeListByStatus('1');

            // echo "<Pre>";print_r($data['semesterDetails']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Course Offered List';
            

            $this->loadViews("course_offered/list", $this->global, $data, NULL);
        }
    }


    function add($id_semester = NULL)
    {
        if ($this->checkAccess('course_offered.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            // echo "Add";exit;
            if ($id_semester == null)
            {
                redirect('/setup/courseOffered/list');
            }
            $id_faculty_program = '';
            if($this->input->post())
            {
                $formData = $this->input->post();

            // echo "<Pre>";print_r($formData);exit;

                $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));


                for($i=0;$i<count($formData['id_course']);$i++)
                {
                    $id_course = $formData['id_course'][$i];
                    $minimum_quota = $formData['minimum_quota'][$i];
                    $maximum_quota = $formData['maximum_quota'][$i];
                    $reservation = $formData['reservation'][$i];

                    if($id_course > 0)
                    {
                            $detailsData = array(
                                'id_semester'=>$id_semester,
                                'id_faculty_program'=>$id_faculty_program,
                                'id_course'=>$id_course,
                                'minimum_quota'=> $minimum_quota,
                                'maximum_quota'=>$maximum_quota,
                                'reservation' => $reservation,
                                'status' => 1,
                                'created_by' => $user_id,
                            );
                    // echo "<Pre>"; print_r($detailsData);exit;
                        $inserted_detail_id = $this->course_offered_model->addCourseOffered($detailsData);
                    }
                }

                redirect('/setup/CourseOffered/edit/'.$id_semester);

            }

            $data['id_semester'] = $id_semester;
            $data['id_faculty_program'] = $id_faculty_program;
            $data['semesterList'] = $this->course_offered_model->semesterListByStatus('1');
            $data['facultyProgramList'] = $this->course_offered_model->facultyProgramListByStatus('1');

            //echo "<Pre>"; print_r($data['semesterDetails']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Semester';
            $this->loadViews("course_offered/add", $this->global, $data, NULL);
        }
    }


    function edit($id_semester = NULL)
    {
        if ($this->checkAccess('course_offered.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_semester == null)
            {
                redirect('/setup/courseOffered/list');
            }
            $id_faculty_program = '';

            if($this->input->post())
            {
                $formData = $this->input->post();
                // echo"<Pre>";print_r($formData);exit();
                $btn = $formData['btn'];

                if($btn == 'remove')
                {

                    if(!empty($formData['id_course_offered']))
                    {
                        $id_course_offered = $formData['id_course_offered'];
                        // echo"<Pre>";print_r($id_course_offered);exit();

                        if(count($id_course_offered) > 0)
                        {
                            foreach ($id_course_offered as $id)
                            {
                                $deleted = $this->course_offered_model->deleteCourseOffered($id);
                            }
                        }
                    }
                }

                // echo"<Pre>";print_r(count($id_course_offered));exit();
                $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
            }

            $data['id_semester'] = $id_semester;
            $data['id_faculty_program'] = $id_faculty_program;

            $data['courseOfferedList'] = $this->course_offered_model->courseOfferedListSearch($data);


            $data['semesterList'] = $this->course_offered_model->semesterListByStatus('1');
            $data['facultyProgramList'] = $this->course_offered_model->facultyProgramListByStatus('1');

            // echo "<Pre>"; print_r($data['courseOfferedList']);exit;
            $this->global['pageTitle'] = 'Inventory Management : Edit Semester';
            $this->loadViews("course_offered/list_course_offered", $this->global, $data, NULL);
        }
    }


    function searchCourseData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit;
        
        $details = $this->course_offered_model->searchCourseByFacultyProgram($tempData); 
        
        // echo "<Pre>";print_r($details);exit;
        if($details)
        {
            
        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Course Code</th>
                    <th>Course Name</th>
                    <th>Credit Hours</th>
                    <th style='text-align: center;'>Minimum Quota</th>
                    <th style='text-align: center;'>Maximum Quota</th>
                    <th style='text-align: center;'>Reservation</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                    </tr>
                </thead>";
                    for($i=0;$i<count($details);$i++)
                    {
                        $id = $details[$i]->id;
                        $course_code = $details[$i]->code;
                        $course_name = $details[$i]->name;
                        $credit_hours = $details[$i]->credit_hours;


                        $j=$i+1;
                        
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$course_code</td>
                            <td>$course_name</td>                            
                            <td>$credit_hours</td>  
                            <td style='text-align: center;'>
                            <div class='form-group'>
                                <input type='text' class='form-control' id='minimum_quota[]' name='minimum_quota[]' >
                            </div>
                            </td>
                            <td style='text-align: center;'>
                            <div class='form-group'>
                                <input type='text' class='form-control' id='maximum_quota[]' name='maximum_quota[]' >
                            </div>
                            </td>
                            <td style='text-align: center;'>
                            <div class='form-group'>
                                <input type='text' class='form-control' id='reservation[]' name='reservation[]' >
                            </div>
                            </td>                          
                            <td class='text-center'>
                                <input type='checkbox' id='id_course[]' name='id_course[]' class='check' value='".$id."'>
                            </td>
                        </tr>";

                    }

                     $table .= "
                    </tbody>";
        $table.= "</table>
        </div>";
        }
        else
        {
            $table = "";
        }

        print_r($table);exit;
    }


    function deleteCourseOffered()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        echo "<Pre>";print_r($tempData);exit();
        $id_course_offered = $tempData['id_course_offered'];


        foreach ($id_course_offered as $id)
        {
            $deleted = $this->course_offered_model->deleteCourseOffered($id);
        }
        echo "success";
    }
}