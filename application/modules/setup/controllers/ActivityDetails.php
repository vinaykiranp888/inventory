<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ActivityDetails extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('activity_details_model');
                        $this->load->model('role_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('activity.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['activityDetails'] = $this->activity_details_model->activityDetailsListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Activity List';
            //print_r($subjectDetails);exit;
            $this->loadViews("activity_details/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('activity.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                            $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status'=>$status,
                    'created_by'=>$user_id
                );
                $result = $this->activity_details_model->addNewActivityDetails($data);
                redirect('/setup/activityDetails/list');
            }
            //print_r($data['stateList']);exit;
            $this->load->model('activity_details_model');
            $this->global['pageTitle'] = 'Inventory Management : Add Activity';
            $this->loadViews("activity_details/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('activity.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/setup/subjectDetails/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'status'=>$status,
                    'updated_by'=>$user_id
                );
                $result = $this->activity_details_model->editActivityDetails($data,$id);
                redirect('/setup/activityDetails/list');
            }
            $data['activityDetails'] = $this->activity_details_model->getActivityDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Activity';
            $this->loadViews("activity_details/edit", $this->global, $data, NULL);
        }
    }

    function delete()
    {
        if ($this->checkAccess('activity.delete') == 0)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->activity_details_model->deleteCountry($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }
}
