<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudyLevel extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('study_level_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('study_level.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['studyLevelList'] = $this->study_level_model->studyLevelListSearch($name);
            $this->global['pageTitle'] = 'School Management System : Education Level List';
            $this->loadViews("study_level/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('study_level.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->study_level_model->addNewStudyLevel($data);
                redirect('/setup/studyLevel/list');
            }
            $this->global['pageTitle'] = 'School Management System : Add Education Level';
            $this->loadViews("study_level/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('study_level.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/studyLevel/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->study_level_model->editStudyLevel($data,$id);
                redirect('/setup/studyLevel/list');
            }
            $data['studyLevel'] = $this->study_level_model->getStudyLevel($id);
            $this->global['pageTitle'] = 'School Management System : Edit Education Level';
            $this->loadViews("study_level/edit", $this->global, $data, NULL);
        }
    }
}
