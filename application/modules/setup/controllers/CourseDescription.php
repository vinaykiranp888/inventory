<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseDescription extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_description_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_description.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['courseDescriptionList'] = $this->course_description_model->courseDescriptionListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Course Description List';
            $this->loadViews("course_description/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course_description.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->course_description_model->addNewCourseDescription($data);
                redirect('/setup/courseDescription/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Course Description';
            $this->loadViews("course_description/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course_description.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/courseDescription/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'status' => $status
                );

                $result = $this->course_description_model->editCourseDescription($data,$id);
                redirect('/setup/courseDescription/list');
            }
            $data['courseDescription'] = $this->course_description_model->getCourseDescription($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Course Description';
            $this->loadViews("course_description/edit", $this->global, $data, NULL);
        }
    }
}
