<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FacultyProgram extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('faculty_program_model');
        $this->load->model('academic_year_model');
                        $this->load->model('role_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('faculty_program.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['facultyProgramList'] = $this->faculty_program_model->facultyProgramListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Department List';
            //print_r($subjectDetails);exit;
            $this->loadViews("faculty_program/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('faculty_program.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'id_partner_university' => $id_partner_university,
                    'description' => $description,
                    'status'=>$status,
                    'created_by' => $id_user
                );

                if($type == 'Internal')
                {
                    $id_partner_university = 0;
                }


                
                $result = $this->faculty_program_model->addNewDepartment($data);
                redirect('/setup/facultyProgram/edit/'.$result);
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('faculty_program_model');
            $data['partnerUniversityList'] = $this->faculty_program_model->partnerUniversityListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Department';
            $this->loadViews("faculty_program/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('faculty_program.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/facultyProgram/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'id_partner_university' => $id_partner_university,
                    'description' => $description,
                    'status'=>$status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                if($type == 'Internal')
                {
                    $id_partner_university = 0;
                }
                
                $result = $this->faculty_program_model->editDepartment($data,$id);
                redirect('/setup/facultyProgram/list');
            }

            $data['facultyProgram'] = $this->faculty_program_model->getDepartment($id);
            $data['facultyProgramHasStaff'] = $this->faculty_program_model->getFacultyProgramHasStaff($id);
            $data['partnerUniversityList'] = $this->faculty_program_model->partnerUniversityListByStatus('1');
            $data['staffList'] = $this->faculty_program_model->staffListByStatus('1');
        // echo "<Pre>"; print_r($data['facultyProgramHasStaff']);exit();



            $this->global['pageTitle'] = 'Inventory Management : Edit Department';
            $this->loadViews("faculty_program/edit", $this->global, $data, NULL);
        }
    }

    function addStaffDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
        // echo "<Pre>"; print_r($tempData);exit();
        
        $inserted_id = $this->faculty_program_model->addStaffDetails($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteStaffDetails($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->faculty_program_model->deleteStaffDetails($id);
        
        echo "Success"; 
    }
}
