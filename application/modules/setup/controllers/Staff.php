<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Staff extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('staff_model');
        $this->load->model('course_model');
        $this->load->model('department_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('staff.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_department'] = $this->security->xss_clean($this->input->post('id_department'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;
            $data['departmentList'] = $this->staff_model->getDepartmentByStatus('1');
            // echo "<Pre>";print_r($data['countryList']);exit;
            // $data['departmentList'] = $this->staff_model->getStateByStatus('1');
            $data['staffDetails'] = $this->staff_model->staffListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Staff List';
            $this->loadViews("staff/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('staff.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            

            if($this->input->post())
            {
                


                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $ic_no = $this->security->xss_clean($this->input->post('ic_no'));
                $staff_id = $this->security->xss_clean($this->input->post('staff_id'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $address_two = $this->security->xss_clean($this->input->post('address_two'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email')); 
                $job_type = $this->security->xss_clean($this->input->post('job_type'));
                $id_department = $this->security->xss_clean($this->input->post('id_department'));
                $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
                $dob = $this->security->xss_clean($this->input->post('dob'));
                $academic_type = $this->security->xss_clean($this->input->post('academic_type'));
                $status = $this->security->xss_clean($this->input->post('status'));
                     
                $salutationInfo = $this->staff_model->getSalutation($salutation);


                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                    'ic_no' => $ic_no,
                    'staff_id' => $staff_id,
                    'gender' => $gender,
                    'mobile_number' => $mobile_number,
                    'phone_number' => $phone_number,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'address' => $address,
                    'address_two' => $address_two,
                    'job_type' => $job_type,
                    'id_department' => $id_department,
                    'id_faculty_program' => $id_faculty_program,
                    'dob' => date('Y-m-d',strtotime($dob)),
                    'academic_type' => $academic_type,
                    'status' => $status
                );

                $duplicate_row = $this->staff_model->checkStaffDuplication($data);

                if($duplicate_row)
                {
                    echo "Duplicate Staff Data Not Allowed Check Mobile Number, IC No & Staff ID";exit();
                }

                $inserted_id = $this->staff_model->addNewStaff($data);

                $details = $this->staff_model->getTempStaff($id_session);
                 for($i=0;$i<count($details);$i++)
                 {
                    $id_course = $details[$i]->id_course;

                     $detailsData = array(
                        'id_staff' => $inserted_id,
                        'id_course' => $id_course,
                    );
                    //print_r($details);exit;
                    $result = $this->staff_model->addNewStaffCourse($detailsData);
                 }

                $this->staff_model->deleteTempDataBySession($id_session);
                redirect('/setup/staff/list');
            }
            else
            {
                $this->staff_model->deleteTempDataBySession($id_session);
            }
            
            // $type = array();
            // $type_data['type'] = 'Permanent';
            // $type_data1['type'] = 'Contract';
            // array_push($type, $type_data);
            // array_push($type, $type_data1);
            // $data['staffType'] = $type;
            $data['countryList'] = $this->staff_model->getCountryByStatus('1');
            $data['departmentList'] = $this->staff_model->getDepartmentByStatus('1');
            $data['facultyProgramList'] = $this->staff_model->getFacultyProgramListByStatus('1');
            $data['salutationList'] = $this->staff_model->salutationListByStatus('1');
            $data['courseList'] = $this->course_model->courseList();
            
            $this->global['pageTitle'] = 'Inventory Management : Add Staff';
            $this->loadViews("staff/add", $this->global, $data, NULL);
        }
    }


    function edit($id)
    {
        if ($this->checkAccess('staff.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/staff/list');
            }
            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $ic_no = $this->security->xss_clean($this->input->post('ic_no'));
                $staff_id = $this->security->xss_clean($this->input->post('staff_id'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $gender = $this->security->xss_clean($this->input->post('gender'));
                $address_two = $this->security->xss_clean($this->input->post('address_two'));
                $mobile_number = $this->security->xss_clean($this->input->post('mobile_number'));
                $phone_number = $this->security->xss_clean($this->input->post('phone_number'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email')); 
                $job_type = $this->security->xss_clean($this->input->post('job_type'));
                $id_department = $this->security->xss_clean($this->input->post('id_department'));
                $id_faculty_program = $this->security->xss_clean($this->input->post('id_faculty_program'));
                $dob = $this->security->xss_clean($this->input->post('dob'));
                $academic_type = $this->security->xss_clean($this->input->post('academic_type'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                     
                $salutationInfo = $this->staff_model->getSalutation($salutation);
                
                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'name' => $salutationInfo->name . ". " . $first_name . " " . $last_name,
                    'ic_no' => $ic_no,
                    'staff_id' => $staff_id,
                    'gender' => $gender,
                    'mobile_number' => $mobile_number,
                    'phone_number' => $phone_number,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'address' => $address,
                    'address_two' => $address_two,
                    'job_type' => $job_type,
                    'id_department' => $id_department,
                    'id_faculty_program' => $id_faculty_program,
                    'dob' =>  date('Y-m-d',strtotime($dob)),
                    'academic_type' => $academic_type,
                    'status' => $status
                );              
                $result = $this->staff_model->editStaff($data,$id);
                redirect('/setup/staff/list');
            }

            $data['id_staff'] = $id;
            $data['courseList'] = $this->course_model->courseList();
            $data['countryList'] = $this->staff_model->getCountryByStatus('1');
            $data['stateList'] = $this->staff_model->getStateByStatus('1');
            $data['departmentList'] = $this->department_model->departmentList();
            $data['staffDetails'] = $this->staff_model->getStaff($id);
            $data['getStaffCourse'] = $this->staff_model->getStaffCourse($id);
            $data['facultyProgramList'] = $this->staff_model->getFacultyProgramListByStatus('1');
            $data['salutationList'] = $this->staff_model->salutationListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Edit Staff';
            $this->loadViews("staff/edit", $this->global, $data, NULL);
        }
    }

    function delete()
    {
        if ($this->checkAccess('staff.delete') == 0)
        {
            echo (json_encode(array('status' => 'access')));
        }
        else
        {
            $countryId = $this->input->post('countryId');
            $countryInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
            $result = $this->staff_model->deleteSemmester($countryId, $countryInfo);
            if ($result > 0)
            {
                echo (json_encode(array('status' => TRUE)));
            }
            else
            {
                echo (json_encode(array('status' => FALSE)));
            }
        }
    }

    function tempadd()
    {
        $id_session = $this->session->my_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->staff_model->updateTempDetails($tempData,$id);
        }
        else
        {
            unset($tempData['id']);
            $inserted_id = $this->staff_model->addTempDetails($tempData);
// echo "<Pre>";  print_r($tempData);exit;
        }
        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->staff_model->getTempStaff($id_session); 
        // echo "<Pre>";print_r($details);exit;
         if(!empty($temp_details))
        {
            $table = "<table  class='table' id='list-table'>
                      <tr>
                        <th>Sl. No</th>
                        <th>Course Name</th>
                        <th>Action</th>
                    </tr>";
                        for($i=0;$i<count($temp_details);$i++)
                        {
                        $id = $temp_details[$i]->id;
                        $fee_name = $temp_details[$i]->name;
                        $j = $i+1;
                            $table .= "
                            <tr>
                                <td>$j</td>
                                <td>$fee_name</td>                       
                                <td>
                                    <span onclick='deleteTempData($id)'>Delete</a>
                                <td>
                            </tr>";
                        }
            $table.= "</table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_session_id;
        $inserted_id = $this->staff_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    }

    function getStateByCountry($id_country)
    {
            $results = $this->staff_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="<select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
            exit;
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_course'] =  $tempData['id_course'];
        $data['id_staff'] =  $tempData['id'];
        $inserted_id = $this->staff_model->addNewStaffCourse($data);
        
        echo $inserted_id;exit;
         // $temp_details = $this->staff_model->getStaffCourse($tempData['id']);

        // if(!empty($temp_details))
        // {

        //     $table = "<table  class='table' id='list-table'>
        //               <tr>
        //                 <th>Sl. No</th>
        //                 <th>Course Name</th>
        //                 <th>Action</th>
        //             </tr>";
        //                 for($i=0;$i<count($temp_details);$i++)
        //                 {
        //                 $id = $temp_details[$i]->id;
        //                 $coursename = $temp_details[$i]->coursename;
        //                 $j = $i+1;
        //                     $table .= "
        //                     <tr>
        //                         <td>$j</td>
        //                         <td>$coursename</td>                         
        //                         <td>
        //                             <span onclick='deleteCourseDetailData($id)'>Delete</a>
        //                         <td>
        //                     </tr>";
        //                 }
        //     $table.= "</table>";
        // }
        // else
        // {
        //     $table="";
        // }
        // echo $table;           
    }

    function deleteCourseDetailData($id_details)
    {
        $inserted_id = $this->staff_model->deleteCourseData($id_details);
        
        echo "Success"; 
    }
}