<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Skill extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('skill_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('skill.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['skillList'] = $this->skill_model->skillListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Skill List';
            $this->loadViews("skill/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('skill.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->skill_model->addNewSkill($data);
                redirect('/setup/skill/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Skill';
            $this->loadViews("skill/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('skill.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if ($id == null)
            {
                redirect('/setup/skill/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->skill_model->editSkill($data,$id);
                redirect('/setup/skill/list');
            }
            $data['skill'] = $this->skill_model->getSkill($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Skill';
            $this->loadViews("skill/edit", $this->global, $data, NULL);
        }
    }
}
