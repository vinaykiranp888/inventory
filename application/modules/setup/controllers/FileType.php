<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FileType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('file_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('file_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['fileTypeList'] = $this->file_type_model->fileTypeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : File Type List';
            $this->loadViews("file_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('file_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                // $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    // 'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->file_type_model->addNewFileType($data);
                redirect('/setup/fileType/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add FileType';
            $this->loadViews("file_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('file_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;

            if ($id == null)
            {
                redirect('/setup/fileType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                // $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    // 'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->file_type_model->editFileType($data,$id);
                redirect('/setup/fileType/list');
            }
            $data['fileType'] = $this->file_type_model->getFileType($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit FileType';
            $this->loadViews("file_type/edit", $this->global, $data, NULL);
        }
    }
}
