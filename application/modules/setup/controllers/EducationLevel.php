<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class EducationLevel extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('education_level_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('education_level.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['educationLevelList'] = $this->education_level_model->educationLevelListSearch($name);
            $this->global['pageTitle'] = 'School Management System : Education Level List';
            $this->loadViews("education_level/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('education_level.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;


            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->education_level_model->addNewEducationLevel($data);
                redirect('/setup/educationLevel/list');
            }
            $this->global['pageTitle'] = 'School Management System : Add Education Level';
            $this->loadViews("education_level/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('education_level.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_user = $this->session->userId;
            $id_session = $this->session->my_session_id;
            
            if ($id == null)
            {
                redirect('/setup/educationLevel/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $id_user,
                    'updated_dt_tm' => date('Y-m-d H:i:s')
                );

                $result = $this->education_level_model->editEducationLevel($data,$id);
                redirect('/setup/educationLevel/list');
            }
            $data['educationLevel'] = $this->education_level_model->getEducationLevel($id);
            $this->global['pageTitle'] = 'School Management System : Edit Education Level';
            $this->loadViews("education_level/edit", $this->global, $data, NULL);
        }
    }
}
