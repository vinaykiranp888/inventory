<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseWithdraw extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_withdraw_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('course_withdraw.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['courseWithdrawList'] = $this->course_withdraw_model->courseWithdrawListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Late Registration List';
            $this->loadViews("course_withdraw/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course_withdraw.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $effective_date_from = $this->security->xss_clean($this->input->post('effective_date_from'));
                $min_days = $this->security->xss_clean($this->input->post('min_days'));
                $max_days = $this->security->xss_clean($this->input->post('max_days'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'semester_type' => $semester_type,
                    'effective_date_from' => date("Y-m-d", strtotime($effective_date_from)),
                    'min_days' => $min_days,
                    'max_days' => $max_days,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->course_withdraw_model->addNewCourseWithdraw($data);
                redirect('/setup/courseWithdraw/list');
            }
            $this->global['pageTitle'] = 'Inventory Management : Add Late Registration';
            $this->loadViews("course_withdraw/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course_withdraw.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/courseWithdraw/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_session_id;
                
                $semester_type = $this->security->xss_clean($this->input->post('semester_type'));
                $effective_date_from = $this->security->xss_clean($this->input->post('effective_date_from'));
                $min_days = $this->security->xss_clean($this->input->post('min_days'));
                $max_days = $this->security->xss_clean($this->input->post('max_days'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'semester_type' => $semester_type,
                    'effective_date_from' => date("Y-m-d", strtotime($effective_date_from)),
                    'min_days' => $min_days,
                    'max_days' => $max_days,
                    'status' => $status,
                    'created_by' => $id_user
                );
                // echo "<Pre>"; print_r($data);
                // exit;

                $result = $this->course_withdraw_model->editCourseWithdraw($data,$id);
                redirect('/setup/courseWithdraw/list');
            }
            $data['courseWithdraw'] = $this->course_withdraw_model->getCourseWithdraw($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Late Registration';
            $this->loadViews("course_withdraw/edit", $this->global, $data, NULL);
        }
    }
}
