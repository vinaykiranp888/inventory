<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Intake extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('intake_model');
        $this->load->model('course_model');
                        $this->load->model('role_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('intake.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['intakeList'] = $this->intake_model->intakeListSearch($name);
            $this->global['pageTitle'] = 'Inventory Management : Intake List';
            $this->loadViews("intake/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('intake.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;


            if($this->input->post())
            {
                

                $name = $this->security->xss_clean($this->input->post('name'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $semester_sequence = $this->security->xss_clean($this->input->post('semester_sequence'));
                $is_sibbling_discount = $this->security->xss_clean($this->input->post('is_sibbling_discount'));
                $is_employee_discount = $this->security->xss_clean($this->input->post('is_employee_discount'));
                $is_alumni_discount = $this->security->xss_clean($this->input->post('is_alumni_discount'));
                $is_temp_offer_letter = $this->security->xss_clean($this->input->post('is_temp_offer_letter'));
            
                $data = array(
                    'name' => $name,
                    'year' => $year,
                    'name_in_malay' => $name_in_malay,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'semester_sequence' => $semester_sequence,
                    'is_sibbling_discount' => $is_sibbling_discount,
                    'is_employee_discount' => $is_employee_discount,
                    'is_alumni_discount' => $is_alumni_discount,
                    'is_temp_offer_letter' => $is_temp_offer_letter,
                    'status' => $status
                );

                $inserted_id = $this->intake_model->addNewIntake($data);
                $temp_details = $this->intake_model->getTempIntakeHasProgramme($id_session);
                 for($i=0;$i<count($temp_details);$i++)
                 {
                    // echo "<Pre>";print_r($temp_details[$i]);exit;

                    $id = $temp_details[$i]->id;
                    $id_programme = $temp_details[$i]->id_programme;

                     $detailsData = array(
                        'id_intake' => $inserted_id,
                        'id_programme' => $id_programme,
                        'created_by' => $id_user
                    );
                    // echo "<Pre>";print_r($detailsData);exit;
                    $result = $this->intake_model->addNewIntakeHasProgramme($detailsData);
                 }

                $this->intake_model->deleteTempDataBySession($id_session);
                redirect('/setup/intake/edit/'.$inserted_id);
            }
            else
            {
                $this->intake_model->deleteTempDataBySession($id_session);
                
            }
            $data['programmeList'] = $this->intake_model->programmeList();
            $data['courseList'] = $this->course_model->courseList();
            $data['yearList'] = $this->intake_model->academicYearList();

            $this->global['pageTitle'] = 'Inventory Management : Add Intake';
            $this->loadViews("intake/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('intake.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/intake/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $year = $this->security->xss_clean($this->input->post('year'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $semester_sequence = $this->security->xss_clean($this->input->post('semester_sequence'));
                $is_sibbling_discount = $this->security->xss_clean($this->input->post('is_sibbling_discount'));
                $is_employee_discount = $this->security->xss_clean($this->input->post('is_employee_discount'));
                $is_alumni_discount = $this->security->xss_clean($this->input->post('is_alumni_discount'));
                $is_temp_offer_letter = $this->security->xss_clean($this->input->post('is_temp_offer_letter'));
            
                $data = array(
                    'name' => $name,
                    'year' => $year,
                    'name_in_malay' => $name_in_malay,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'semester_sequence' => $semester_sequence,
                    'is_sibbling_discount' => $is_sibbling_discount,
                    'is_employee_discount' => $is_employee_discount,
                    'is_alumni_discount' => $is_alumni_discount,
                    'is_temp_offer_letter' => $is_temp_offer_letter,
                    'status' => $status
                );

                $result = $this->intake_model->editIntakeDetails($data,$id);
                redirect('/setup/intake/list');
            }
            $data['idIntake'] = $id;
            $data['intakeDetails'] = $this->intake_model->getIntakeDetails($id);
            $data['getIntakeHasProgramme'] = $this->intake_model->getIntakeHasProgramme($id);
            $data['getIntakeHasBranch'] = $this->intake_model->getIntakeHasBranch($id);
            $data['intakeHasScheme'] = $this->intake_model->intakeHasScheme($id);


            $data['yearList'] = $this->intake_model->academicYearList();

            $data['schemeList'] = $this->intake_model->schemeListByStatus('1');
            $data['programmeList'] = $this->intake_model->programmeList();
            $data['branchList'] = $this->intake_model->branchList();

            $this->global['pageTitle'] = 'Inventory Management : Edit Intake';
            // echo "<Pre>";print_r($data);exit;
            $this->loadViews("intake/edit", $this->global, $data, NULL);
        }
    }


    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $data['id_programme'] =  $tempData['id_programme'];
        $data['id_intake'] =  $tempData['id'];
        $inserted_id = $this->intake_model->addNewIntakeHasProgramme($data);

        echo "success";exit;
        
         $temp_details = $this->intake_model->getIntakeHasProgramme($tempData['id']);
         if(!empty($temp_details))
        {
            
        // echo "<Pre>";print_r($temp_details);exit;
        $table = " <div class='custom-table'>
                    <table  class='table'>
                    <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Programme Name</th>
                    <th>Action</th>
                </tr></thead>
                <tbody>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $programme = $temp_details[$i]->programme;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$programme</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteIntakeHasProgramme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
                    // <a onclick='deleteIntakeHasProgramme($id)'>Delete</a>

        $table.= "</tbody>
        </table>
        </div>";
        }
        else
        {
            $table="";
        }
        echo $table;           
    }



    function tempadd()
    {
        $id_session = $this->session->my_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;
        if($tempData['id'] && $tempData['id']>0)
        {
            $id =  $tempData['id'];
            unset($tempData['id']);
            $inserted_id = $this->intake_model->updateTempDetails($tempData,$id);
        }
        else
        {

            unset($tempData['id']);
            $inserted_id = $this->intake_model->addTempDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;
        }

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->intake_model->getTempIntakeHasProgramme($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Programme Name</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $programme = $temp_details[$i]->programme;
                    $programme_code = $temp_details[$i]->programme_code;

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$programme_code - $programme</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempIntakeHasProgramme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }


                                // <span onclick='deleteTempIntakeHasProgramme($id)'>Delete</a>
                            

            $table.= "
            </tbody>
            </table></div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempIntakeHasProgramme($id)
    {
        $inserted_id = $this->intake_model->deleteTempIntakeHasProgramme($id);
        if($inserted_id)
        {
            $data = $this->displaytempdata();
            echo $data;  
        }
        
        // echo "Success"; 
    }

    function deleteIntakeHasProgramme($id)
    {
        $inserted_id = $this->intake_model->deleteIntakeHasProgramme($id);
        // if($inserted_id)
        // {
        //     $data = $this->displaytempdata();
        //     echo $data;  
        // }
        
        echo "Success"; 
    }

    function tempDelete($id)
    {
        $inserted_id = $this->intake_model->deleteTempData($id);
        $data = $this->displaytempdata();
        echo $data; 
    } 

    function addIntakeHasBranch()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['registration_date'] =  date('Y-m-d', strtotime($tempData['registration_date']));
        $tempData['registration_time'] =  date('h:i A', strtotime($tempData['registration_time']));
        $inserted_id = $this->intake_model->addIntakeHasBranch($tempData);

        echo "success";exit;
    }

    function deleteIntakeTrainingCenter($id)
    {
        $inserted_id = $this->intake_model->deleteIntakeTrainingCenter($id);
        echo "Success";exit; 
    }

    function addIntakeHasScheme()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->intake_model->addIntakeHasScheme($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteIntakeHasScheme($id)
    {
        $inserted_id = $this->intake_model->deleteIntakeHasScheme($id);
        echo "Success";exit; 
    }
}
