<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class State_model extends CI_Model
{
    
    function stateListingCount()
    {
        $this->db->select('BaseTbl.id as stateId, BaseTbl.name,BaseTbl.id_country as idCountry, BaseTbl.status');
        $this->db->from('state as BaseTbl');
        $this->db->order_by("BaseTbl.name", "ASC");
        $query = $this->db->get();

        return $query->num_rows();
    }
    
    function latestStateListing()
    {
        $this->db->select('BaseTbl.id as stateId, BaseTbl.name,BaseTbl.id_country as idCountry, BaseTbl.status');
        $this->db->from('state as BaseTbl');
        $this->db->limit(10, 0);
        $this->db->order_by("name", "desc");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function stateListing($state, $country)
    {
        $this->db->select('BaseTbl.id as stateId, BaseTbl.name,BaseTbl.id_country as idCountry, CountryTbl.name as CountryName, BaseTbl.status');
        $this->db->from('state as BaseTbl');
        $this->db->join('country as CountryTbl', 'CountryTbl.id = BaseTbl.id_country');
        if (!empty($state))
        {
            $likeCriteria = "(BaseTbl.name  LIKE '%" . $state . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($country))
        {
            $likeCriteria = "(BaseTbl.id_country  = $country)";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        //echo "<Pre>"; print_r($result);exit;
        return $result;
    }

    function stateList()
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        //echo "<Pre>"; print_r($result);exit;
        return $result;
    }

    function addNewState($stateInfo)
    {
        $this->db->trans_start();
        $this->db->insert('state', $stateInfo);

        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();

        return $insert_id;
    }

    function getStateInfo($id)
    {
        $this->db->select('id as stateId, name,id_country as idCountry, status');
        $this->db->from('state');
        $this->db->where('id', $id);
        $query = $this->db->get();
        // echo "<pre>";print_r($query);die;

        return $query->row();
    }

    function editState($stateInfo, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return TRUE;
    }

    function deleteState($id, $stateInfo)
    {
        $this->db->where('id', $id);
        $this->db->update('state', $stateInfo);

        return $this->db->affected_rows();
    }

    function getCountryList()
    {
        $this->db->select('*');
        $this->db->from('country');
        $query = $this->db->get();
        $result = $query->result();
        //echo "<Pre>"; print_r($result);exit;
        return $result;
    }
}
