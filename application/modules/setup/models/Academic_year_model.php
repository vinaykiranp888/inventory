<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Academic_year_model extends CI_Model
{
    function academicYearList($search)
    {
        $this->db->select('ay.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('academic_year as ay');
        $this->db->join('users as cre','ay.created_by = cre.id','left');
        $this->db->join('users as upd','ay.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getAcademicYearDetails($id)
    {
        $this->db->select('*');
        $this->db->from('academic_year');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAcademicYear($data)
    {
        $this->db->trans_start();
        $this->db->insert('academic_year', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAcademicYearDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('academic_year', $data);
        return TRUE;
    }
    
    function deleteActivityDetails($id, $date)
    {
        $this->db->where('id', $countryId);
        $this->db->update('academic_year', $countryInfo);
        return $this->db->affected_rows();
    }
}

