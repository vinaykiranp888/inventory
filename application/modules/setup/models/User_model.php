<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class User_model extends CI_Model
{
    
    function userListingCount()
    {
        $this->db->select('BaseTbl.id as userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.created_dt_tm as createdDtm, Role.role');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Role', 'Role.id = BaseTbl.role_id','left');
       
        $this->db->where('BaseTbl.is_deleted', 0);
        // $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    function userListing($name,$email,$mobile,$role)
    {
        $this->db->select('usr.id as userId, usr.email, usr.name, usr.mobile, usr.created_dt_tm as createdDtm, Role.role,usr.created_dt_tm, usr.updated_dt_tm, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('users as usr');
        $this->db->join('roles as Role', 'Role.id = usr.role_id','left');
        $this->db->join('users as cre','usr.created_by = cre.id','left');
        $this->db->join('users as upd','usr.updated_by = upd.id','left');
        if (!empty($name)) {
            $likeCriteria = "(usr.name  LIKE '%" . $name . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($email)) {
            $likeCriteria = "(usr.email  LIKE '%" . $email . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($mobile)) {
            $likeCriteria = "(usr.mobile  LIKE '%" . $mobile . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($role)) {
            $likeCriteria = "(usr.role_id  = $role)";
            $this->db->where($likeCriteria);
        }
        $this->db->where('usr.is_deleted', 0);
        // $this->db->where('BaseTbl.role_id !=', 1);
        $this->db->order_by('usr.id', 'DESC');
        // $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    
    function getUserRoles()
    {
        $this->db->select('roleId, role');
        $this->db->from('tbl_roles');
        $this->db->where('roleId !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    function roleListForUserAdd()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('roles as BaseTbl');
        $this->db->where('id !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

    function roleList()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('roles as BaseTbl');
        $query = $this->db->get();
        
        return $query->result();
    }

    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("users");
        $this->db->where("email", $email);   
        $this->db->where("is_deleted", 0);
        if($userId != 0){
            $this->db->where("userId !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('users', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
    
    function getUserInfo($userId)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('is_deleted', 0);
		// $this->db->where('role_id !=', 1);
        $this->db->where('id', $userId);
        $query = $this->db->get();
        //print_r($query);exit;
        return $query->row();
    }
    
    function editUser($userInfo, $userId)
    {
        $this->db->where('id', $userId);
        $this->db->update('users', $userInfo);
        
        return TRUE;
    }
    
    
    function deleteUser($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('users', $userInfo);
        
        return $this->db->affected_rows();
    }

    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('id, password');
        $this->db->where('id', $userId);        
        $this->db->where('is_deleted', 0);
        $query = $this->db->get('users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('is_deleted', 0);
        $this->db->update('users', $userInfo);
        
        return $this->db->affected_rows();
    }


    function loginHistoryCount($userId, $searchText, $fromDate, $toDate)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.userId', $userId);
        }
        $this->db->from('tbl_last_login as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    
    function loginHistory($userId, $searchText, $fromDate, $toDate, $page, $segment)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        $this->db->from('tbl_last_login as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.userId', $userId);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    
    function getUserInfoById($userId)
    {
        $this->db->select('id as userId, name, email, mobile, roleId');
        $this->db->from('users');
        $this->db->where('is_deleted', 0);
        $this->db->where('id', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }

    
    function getUserInfoWithRole($userId)
    {
        $this->db->select('BaseTbl.id as userId, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.roleId, Roles.role');
        $this->db->from('users as BaseTbl');
        $this->db->join('tbl_roles as Roles','Roles.roleId = BaseTbl.roleId');
        $this->db->where('BaseTbl.id', $userId);
        $this->db->where('BaseTbl.is_deleted', 0);
        $query = $this->db->get();
        
        return $query->row();
    }

}

  