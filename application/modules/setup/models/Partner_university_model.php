<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_university_model extends CI_Model
{

    function countryListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('country as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function stateListByStatus($status)
    {
        $this->db->select('a.*');
        $this->db->from('state as a');
        $this->db->where('a.status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function partnerUniversityListSearch($data)
    {
        $this->db->select('d.*, c.name as country');
        $this->db->from('partner_university as d');
        $this->db->join('country as c','d.id_country = c.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(d.name  LIKE '%" . $data['name'] . "%' or d.short_name  LIKE '%" . $data['name'] . "%' or d.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_country']))
        {
            $this->db->where('d.id_country', $data['id_country']);
        }
        if (!empty($data['url']))
        {
            $likeCriteria = "(d.url  LIKE '%" . $data['url'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['email']))
        {
            $likeCriteria = "(d.email  LIKE '%" . $data['email'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['contact_number']))
        {
            $likeCriteria = "(d.contact_number  LIKE '%" . $data['contact_number'] . "%'";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getPartnerUniversity($id)
    {
        $this->db->select('d.*, pc.name as partner_category');
        $this->db->from('partner_university as d');
        $this->db->join('partner_category as pc','d.id_partner_category = pc.id');
        $this->db->where('d.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewPartnerUniversity($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editPartnerUniversity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_university', $data);
        return TRUE;
    }
    
    function deletePartnerUniversity($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('partner_university', $data);
        return $this->db->affected_rows();
    }

    function addComitee($data)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_comitee', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteComitee($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('partner_university_comitee');
        return TRUE;
    }

    function comiteeList($id_partner_university)
    {
        $this->db->select('a.*');
        $this->db->from('partner_university_comitee as a');
        $this->db->where('a.id_partner_university', $id_partner_university);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }




    function partnerUniversityListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('partner_university');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function partnerCategoryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function getPartnerUniversityCategory($id)
    {
        $this->db->select('*');
        $this->db->from('partner_category');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->row();
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function trainingCenterList($id_partner_university)
    {
        // This Table Is Not Using Now
        // $this->db->select('a.*');
        // $this->db->from('scholarship_partner_university_has_training_center as a');
        // $this->db->where('a.id_partner_university', $id_partner_university);
        // // $this->db->order_by("name", "ASC");
        // $query = $this->db->get();
        // $result = $query->result();
        // return $result;

        $this->db->select('a.*');
        $this->db->from('organisation_has_training_center as a');
        $this->db->where('a.id_organisation', $id_partner_university);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;

        
    }


    function getPartnerUniversityAggrementList($id_partner_university)
    {
        $this->db->select('aggr.*, cs.code as currency_code, cs.name as currency_name');
        $this->db->from('scholarship_partner_university_has_aggrement as aggr');
        $this->db->join('currency_setup as cs','aggr.id_currency = cs.id');
        $this->db->where('aggr.id_partner_university', $id_partner_university);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramDetails($id_partner_university)
    {
        $this->db->select('*');
        $this->db->from('scholarship_partner_university_has_program');
        $this->db->where('id_partner_university', $id_partner_university);
        $query = $this->db->get();
        return $query->row();
    }

    function partnerProgramStudyModeDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as training_code, c.name as training_name, c.location, c.city');
        $this->db->from('scholarship_partner_university_program_has_study_mode as pupd');
        $this->db->join('organisation_has_training_center as c','pupd.id_training_center = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramApprenticeshipDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as apprenticeship_code, c.name as apprenticeship_name, c.location, c.city');
        $this->db->from('scholarship_partner_university_program_has_apprenticeship as pupd');
        $this->db->join('organisation_has_training_center as c','pupd.id_apprenticeship_center = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramInternshipDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as internship_code, c.name as internship_name, c.location, c.city');
        $this->db->from('scholarship_partner_university_program_has_internship as pupd');
        $this->db->join('organisation_has_training_center as c','pupd.id_internship_center = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function partnerProgramSyllabusDetails($id_partner_university,$id_program)
    {
        $this->db->select('pupd.*, c.code as module_code, c.name as module_name');
        $this->db->from('scholarship_partner_university_program_has_syllabus as pupd');
        $this->db->join('scholarship_module_type_setup as c','pupd.id_module_type = c.id');
        $this->db->where('pupd.id_partner_university', $id_partner_university);
        $this->db->where('pupd.id_program_detail', $id_program);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteMoaAggrement($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_has_aggrement');
        return TRUE;
    }

    function addTrainingCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('organisation_has_training_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTrainingCenter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('organisation_has_training_center');
        return TRUE;
    }


    function addNewPartnerUniversityProgram($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

     function editPartnerUniversityProgram($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_partner_university_has_program', $data);
        return TRUE;
    }

    function addNewPartnerProgramStudyMode($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_study_mode', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewPartnerProgramInternship($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_internship', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewPartnerProgramApprenticeship($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_apprenticeship', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewPartnerProgramSyllabus($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_program_has_syllabus', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function deletePartnerProgramStudyMode($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_study_mode');
        return TRUE;
    }

    function deletePartnerProgramApprenticeship($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_apprenticeship');
        return TRUE;
    }


    function deletePartnerProgramInternship($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_internship');
        return TRUE;
    }


    function deletePartnerProgramSyllabus($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_partner_university_program_has_syllabus');
        return TRUE;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function moduleTypeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_module_type_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function addNewAggrement($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_partner_university_has_aggrement', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
}