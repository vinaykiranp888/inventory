<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Skill_model extends CI_Model
{
    function skillList()
    {
        $this->db->select('*');
        $this->db->from('skill');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function skillListSearch($search)
    {
        $this->db->select('s.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('skill as s');
        $this->db->join('users as cre','s.created_by = cre.id','left');
        $this->db->join('users as upd','s.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(s.name  LIKE '%" . $search . "%' or s.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("s.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getSkill($id)
    {
        $this->db->select('*');
        $this->db->from('skill');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewSkill($data)
    {
        $this->db->trans_start();
        $this->db->insert('skill', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editSkill($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('skill', $data);
        return TRUE;
    }
}

