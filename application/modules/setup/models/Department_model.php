<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model
{
    function departmentList()
    {
        $this->db->select('d.*');
        $this->db->from('department as d');
        $this->db->order_by("d.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function staffListByStatus($status)
    {
        $this->db->select('d.*');
        $this->db->from('staff as d');
        $this->db->where('d.status', $status);
        $this->db->order_by("d.name", "DESC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function departmentListSearch($search)
    {
        $this->db->select('d.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('department as d');
        $this->db->join('users as cre','d.created_by = cre.id','left');
        $this->db->join('users as upd','d.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(d.name  LIKE '%" . $search . "%' or d.description  LIKE '%" . $search . "%' or d.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("d.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function getDepartment($id)
    {
        $this->db->select('d.*');
        $this->db->from('department as d');
        $this->db->where('d.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewDepartment($data)
    {
        $this->db->trans_start();
        $this->db->insert('department', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDepartment($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('department', $data);
        return $this->db->affected_rows();
        return TRUE;
    }

    function getDepartmentHasStaff($id)
    {
        $this->db->select('d.*, s.ic_no, s.name as staff_name');
        $this->db->from('department_has_staff as d');
        $this->db->join('staff as s', 'd.id_staff = s.id');
        $this->db->where('d.id_department', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function addStaffDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('department_has_staff', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteStaffDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('department_has_staff');
        return $this->db->affected_rows();
    }
}