<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Study_level_model extends CI_Model
{
    function studyLevelList()
    {
        $this->db->select('*');
        $this->db->from('study_level');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function studyLevelListSearch($search)
    {
        $this->db->select('sl.*, cre.name as creater_name, upd.name as updater_name');
        $this->db->from('study_level as sl');
        $this->db->join('users as cre','sl.created_by = cre.id','left');
        $this->db->join('users as upd','sl.updated_by = upd.id','left');
        if (!empty($search))
        {
            $likeCriteria = "(sl.name  LIKE '%" . $search . "%' or sl.short_name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("sl.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudyLevel($id)
    {
        $this->db->select('*');
        $this->db->from('study_level');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewStudyLevel($data)
    {
        $this->db->trans_start();
        $this->db->insert('study_level', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editStudyLevel($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('study_level', $data);
        return TRUE;
    }
}