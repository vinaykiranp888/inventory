<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Checklist_visa_model extends CI_Model
{
    function checklist_visaList()
    {
        $this->db->select('*');
        $this->db->from('checklist_visa_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function checklistVisaListSearch($data)
    {
        $this->db->select('es.*, pu.code as university_code, pu.name as university_name, el.name as education_level_name');
        $this->db->from('checklist_visa_setup as es');
        $this->db->join('partner_university as pu','es.id_university = pu.id','left');
        $this->db->join('education_level as el','es.id_education_level = el.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(es.description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_university'] != '')
        {
            $this->db->where('es.id_university', $data['id_university']);
            $this->db->where($likeCriteria);
        }
        if ($data['id_education_level'] != '')
        {
            $this->db->where('es.id_education_level', $data['id_education_level']);
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("es.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getChecklistVisa($id)
    {
        $this->db->select('*');
        $this->db->from('checklist_visa_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewChecklistVisa($data)
    {
        $this->db->trans_start();
        $this->db->insert('checklist_visa_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editChecklistVisa($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('checklist_visa_setup', $data);
        return TRUE;
    }

    function educationLevelListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getUniversityListByStatus($status)
    {
        $organisation = $this->getOrganisaton();
        $details = array();

        if($organisation)
        {
            array_push($details, $organisation);
        }

        $this->db->select('ahemd.*');
        $this->db->from('partner_university as ahemd');
        $this->db->where('ahemd.status', $status);
        $query = $this->db->get();
        $results = $query->result();

        foreach ($results as $result)
        {
            array_push($details, $result);
        }
        return $details;
    }

    function getOrganisaton()
    {
        $this->db->select('a.*, a.short_name as code');
        $this->db->from('organisation as a');
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }
}

