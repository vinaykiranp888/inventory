<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add staff Has Course</h3>
        </div>
        <form id="programme_has_dean" action="" method="post">
            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Staff *</label>
                        <select name="id_staff" id="id_staff" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->salutation .". ".$record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>       

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Program *</label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Effective Start Date *</label>
                        <input type="date" class="form-control" id="effective_start_date" name="effective_start_date">
                    </div>
                </div>   
            </div>

            <div class="row">    

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $("#programme_has_dean").validate(
        {
            rules:
            {
                id_staff:
                {
                    required: true
                },
                id_programme:
                {
                    required: true
                },
                effective_start_date:
                {
                    required: true
                },
                status:
                {
                    required: true
                }
            },
            messages:
            {
                id_staff:
                {
                    required: "Select Staff",
                },
                id_programme:
                {
                    required: "Select Course",
                },
                effective_start_date:
                {
                    required: "Select Effectve Start Date",
                },
                status:
                {
                    required: "Select Status",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>