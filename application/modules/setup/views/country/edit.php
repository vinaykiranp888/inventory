<?php
$countryId = $countryInfo->countryId;
$name = $countryInfo->name;
?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Country</h3>
        </div>
        <form id="form" action="/setup/country/editCountry" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Country Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $name; ?>">
                        <input type="hidden" value="<?php echo $countryId; ?>" name="countryId" id="countryId" />
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status *</p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($countryInfo->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($countryInfo->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


            <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Country Name required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>