<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List New Student Checklist Visa</h3>
      <a href="add" class="btn btn-primary">+ Add Checklist Visa</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Checklist Visa </label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">University</label>
                      <div class="col-sm-8">
                        <select name="id_university" id="id_university" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($universityList)) {
                            foreach ($universityList as $record) {                          
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php if ($record->id == $searchParam['id_university'])
                                {
                                  echo 'selected';
                                }  ?>>
                                <?php echo $record->code . " - " .  $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>


                </div>


                <div class="row">
                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Education Level</label>
                      <div class="col-sm-8">
                        <select name="id_education_level" id="id_education_level" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($educationLevelList)) {
                            foreach ($educationLevelList as $record) {                          
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php if
                                ($record->id == $searchParam['id_education_level'])
                                {
                                  echo 'selected';
                                }  ?>>
                                <?php echo $record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>

              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>University</th>
            <th>Education Level</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($checklistVisaList)) {
            $i=1;
            foreach ($checklistVisaList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td>
                  <?php
                  if($record->id_university == 1)
                  {
                    echo $organisation->code . " - " . $organisation->name;
                  }
                  else
                  {
                   echo $record->university_code . " - " . $record->university_name;
                  }
                  ?>    
                </td>
                <td><?php echo $record->education_level_name ?></td>
                <td class="text-center">
                  <?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
</script>