<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit New Student Checklist Visa</h3>
        </div>
        <form id="form_sponser" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Checklist Visa Details</h4>



                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>University <span class='error-text'>*</span></label>
                            <select name="id_university" id="id_university" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($universityList))
                                {
                                    foreach ($universityList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($record->id == $checklistVisa->id_university)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    

                    <div class="col-sm-4">
                        <div class="form-group">
                          <label>Education Level <span class='error-text'>*</span></label>
                            <select name="id_education_level" id="id_education_level" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($educationLevelList))
                                {
                                    foreach ($educationLevelList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($record->id == $checklistVisa->id_education_level)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    >
                                    <?php echo  $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group shadow-textarea">
                            <label for="description">Description <span class='error-text'>*</span></label>
                            <textarea class="form-control z-depth-1" rows="3" placeholder="Write Description..." name="description" id="description"><?php echo $checklistVisa->description;?></textarea>
                        </div>
                    </div>

                </div>
                
                

                <div class="row">

                   <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($checklistVisa->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($checklistVisa->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                    </div>

                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

<script type="text/javascript">

// Initialize CKEditor

CKEDITOR.replace('message',{

  width: "800px",
  height: "200px"

}); 

</script>

<script>

    $('select').select2();

    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                description: {
                    required: true
                },
                status: {
                    required: true
                },
                id_university: {
                    required: true
                },
                id_education_level: {
                    required: true
                }
            },
            messages: {
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_university: {
                    required: "<p class='error-text'>Select University</p>",
                },
                id_education_level: {
                    required: "<p class='error-text'>Select Education Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
