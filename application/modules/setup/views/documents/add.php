
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Document</h3>
        </div>
        <form id="form_award" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Document Details</h4>

            <div class="row">
                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>
            </div>

             <div class="row">
                  <div class="col-sm-4">
                        <div class="form-group">
                            <p>Compulsory <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_required" id="is_required" value="1" checked="checked"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_required" id="is_required" value="0"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max File Size (in MB)<span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="file_size" name="file_size" required>
                    </div>
                </div>



               <div class="col-sm-4">
                    <div class="form-group">
                        <label>File Format <span class='error-text'>*</span></label>
                         <select name="file_format[]" id="file_format"  multiple="true" >
                            <?php
                            for($n=0;$n<count($fileType);$n++)
                            {
                            ?>
                            <option value='<?php echo $fileType[$n]->id;?>'>
                                <?php echo $fileType[$n]->name;?>        
                            </option>
                            <?php
                            }
                            ?> 
                           </select>
                    </div>
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
  $('#file_format').multiselect({
    buttonWidth : '160px',
    includeSelectAllOption : true,
        nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#file_format").val();
    for(var i=0; i<selectedVal.length; i++){
        function innerFunc(i) {
            setTimeout(function() {
                location.href = selectedVal[i];
            }, i*2000);
        }
        innerFunc(i);
    }
}


  
  
</script>
<script>
    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
