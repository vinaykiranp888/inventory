<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <!-- <div class="page-title clearfix">
            <h3>Welcome : Module SETUP</h3>
        </div> -->
    <?php

    if($id_role == 1)
    {
        ?>

        <!-- <div class="row">

            <div class="col-sm-3">
                <div class="stats-col">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_approval_icon.svg" />Company Pending Approvals
                    </div>
                    <div class="count">
                        <?php echo $company_pending_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/corporate/companyApproval/list" title="Redirects To Corporate Approvals">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="stats-col pending-acceptance">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/pending_acceptance_icon.svg"/>Active Company
                    </div>
                    <div class="count">
                        <?php echo $company_active_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/corporate/company/list" title="Redirects To Corporate List">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="stats-col active-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/active_students_icon.svg"/>Students
                    </div>
                    <div class="count">
                        <?php echo $student_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/records/studentRecord/list" title="Redirects To Records">More Info</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="stats-col inactive-students">
                    <div class="text">
                        <img src="<?php echo BASE_PATH; ?>assets/img/inactive_students_icon.svg"/>Employees
                    </div>
                    <div class="count">
                        <?php echo $employee_count; ?>
                    </div>
                    <div class="text-center">
                        <a href="/records/studentRecord/list" title="Redirects To Records">More Info</a>
                    </div>
                </div>
            </div>

        </div> -->

        <hr/>



    


        <div class="row">            
            <div class="col-sm-3 col-lg-2">
                <a href="/setup/welcome" class="dashboard-menu"><span class="icon"></span>System Setup</a>
            </div>
            <!-- <div class="col-sm-3 col-lg-2">
                <a href="/prdtm/welcome" class="dashboard-menu curriculm-management"><span class="icon"></span>Product Management</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/pm/welcome" class="dashboard-menu partners-management"><span class="icon"></span>Partners Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/records/welcome" class="dashboard-menu records"><span class="icon"></span>Records</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="/finance/welcome" class="dashboard-menu student-finance"><span class="icon"></span>Finance</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="/af/welcome" class="dashboard-menu academic-facilitator"><span class="icon"></span>Academic Facilitator Management</a>
            </div>   
            <div class="col-sm-3 col-lg-2">
                <a href="/communication/welcome" class="dashboard-menu communication-management"><span class="icon"></span>Communication Management</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/mrktngm/welcome" class="dashboard-menu pg-research"><span class="icon"></span>Marketing Manegement</a>
            </div> 
            <div class="col-sm-3 col-lg-2">
                <a href="/registration/welcome" class="dashboard-menu registration"><span class="icon"></span>Registration</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/examination/welcome" class="dashboard-menu exam-timetable"><span class="icon"></span>Examination</a>
            </div>
            <div class="col-sm-3 col-lg-2">
                <a href="/corporate/welcome" class="dashboard-menu admission"><span class="icon"></span>Corporate</a>
            </div>  
            <div class="col-sm-3 col-lg-2">
                <a href="#" class="dashboard-menu chatbot"><span class="icon"></span>Smart Chatbot</a>
            </div> 
            <div class="col-sm-3 col-lg-2">
                <a href="/reports/welcome" class="dashboard-menu reporting"><span class="icon"></span>Reporting</a>
            </div> -->
        </div>



        <?php
    }
    else
    {
        ?>

        <div class="welcome-container">
            <img src="<?php echo BASE_PATH; ?>assets/img/system_setup_icon.svg" alt="Partner Management Module">
            <h3>Welcome to <br/><strong>Setup Module</strong></h3>
            <p>Text........................</p>
        </div>


        <?php
    }
    ?>

        <footer class="footer-wrapper">
            <p>&copy; 2020 All rights, reserved</p>
        </footer>

    </div>
</div>