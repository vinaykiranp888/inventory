<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Organisation</h3>
        </div>
        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">Organisation Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $organisation->name; ?>">
                        <input type="hidden" class="form-control datepicker" id="id_organisation" name="id_organisation" value="<?php echo $id_organisation; ?>" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="short_name" name="short_name" value="<?php echo $organisation->short_name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language </label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay" value="<?php echo $organisation->name_in_malay; ?>">
                    </div>
                </div>

                
            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Url <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $organisation->url; ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>GST IN <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="gst_in" name="gst_in" value="<?php echo $organisation->gst_in; ?>">
                    </div>
                </div>





                <div class="col-sm-4">
                    <div class="form-group">
                        <label>CIN <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="cin" name="cin" value="<?php echo $organisation->cin; ?>">
                    </div>
                </div>



            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE 

                            <span class='error-text'>*</span>
                            <?php 
                            if($organisation->image != '')
                            {
                            ?>
                            <a href="<?php echo '/assets/images/' . $organisation->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $organisation->image; ?>)" title="<?php echo  $organisation->image; ?>"> View </a>

                            <?php
                            }
                            ?>


                        </label>
                        <input type="file" name="image" id="image">
                    </div>                    
                </div>


                             
            </div>

        </div>


        <!-- <div class="form-container">
            <h4 class="form-group-title">HEAD OF SPEED DETAILS</h4>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registrar <span class='error-text'>*</span></label>
                        <select name="id_registrar" id="id_registrar" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $organisation->id_registrar)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->ic_no . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Joining Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo date('d-m-Y', strtotime($organisation->date_time)); ?>" autocomplete="off">

                    </div>
                </div>



            </div>

        </div> -->

        <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $organisation->contact_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contact Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $organisation->email; ?>">
                    </div>
                </div>


                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $organisation->address1 ?>">
                    </div>
                </div>

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $organisation->address2 ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($organisation->id_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($organisation->id_state==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $organisation->city ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $organisation->zipcode ?>">
                    </div>
                </div>
            </div>
        </div>

        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <!-- <a href="../list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>


        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                short_name: {
                    required: true
                },
                url: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address1: {
                    required: true
                },
                id_registrar: {
                    required: true
                },
                date_time: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                gst_in: {
                    required: true
                },
                cin: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                url: {
                    required: "<p class='error-text'>URL Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                permanent_address: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                id_registrar: {
                    required: "<p class='error-text'>Select Registrar</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Joining Date</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                gst_in: {
                    required: "<p class='error-text'>GST IN Required</p>",
                },
                cin: {
                    required: "<p class='error-text'>CIN Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();



    function saveTrainingCenterData()
    {
        if($('#form_training').valid())
        {


        var tempPR = {};
        tempPR['code'] = $("#training_code").val();
        tempPR['name'] = $("#training_name").val();
        tempPR['complete_code'] = $("#training_complete_code").val();
        tempPR['status'] = $("#training_status").val();
        tempPR['contact_number'] = $("#training_contact_number").val();
        tempPR['email'] = $("#training_email").val();
        tempPR['fax'] = $("#training_fax").val();
        tempPR['address1'] = $("#training_address1").val();
        tempPR['address2'] = $("#training_address2").val();
        tempPR['location'] = $("#training_location").val();
        tempPR['id_country'] = $("#id_training_country").val();
        tempPR['id_state'] = $("#id_training_state").val();
        tempPR['city'] = $("#training_city").val();
        tempPR['zipcode'] = $("#training_zipcode").val();
        tempPR['id_contact_person'] = $("#id_training_contact_person").val();
        tempPR['status'] = 1;
        tempPR['id_organisation'] = <?php echo $organisation->id; ?>;


            $.ajax(
            {
               url: '/setup/organisation/addTrainingCenter',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view_aggrement_data").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }


    function deleteTrainingCenter(id) {
         $.ajax(
            {
               url: '/setup/organisation/deleteTrainingCenter/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert('Deleted Sessessfully');
                    // window.location.reload();
                    location.reload();
               }
            });
    }


    function getStateByCountry(id)
    {
        // alert(id);
        $.get("/setup/partnerUniversity/getStateByCountryForTraining/"+id, function(data, status){
            
            // alert(data);
            $("#view_state").html(data);
        });
    }



     $(document).ready(function() {
        $("#form_training").validate({
            rules: {
                training_code: {
                    required: true
                },
                training_name: {
                    required: true
                },
                training_status: {
                    required: true
                },
                training_contact_number: {
                    required: true
                },
                training_email: {
                    required: true
                },
                training_fax: {
                    required: true
                },
                training_address1: {
                    required: true
                },
                training_address2: {
                    required: true
                },
                id_training_country: {
                    required: true
                },
                training_city: {
                    required: true
                },
                training_zipcode: {
                    required: true
                },
                id_training_state: {
                    required: true
                },
                id_training_contact_person : {
                    required : true
                },
                training_location : {
                    required : true
                }
            },
            messages: {
                training_code: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                training_name: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                training_status: {
                    required: "<p class='error-text'>Training Status Required</p>",
                },
                training_contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                training_email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                training_fax: {
                    required: "<p class='error-text'>Fax Number Required</p>",
                },
                training_address1: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                training_address2: {
                    required: "<p class='error-text'>Address2 Required</p>",
                },
                id_training_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                training_city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                training_zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                id_training_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                id_training_contact_person: {
                    required: "<p class='error-text'>Select Contact Person</p>",
                },
                training_location: {
                    required: "<p class='error-text'>Training Location Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });





</script>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );
</script>