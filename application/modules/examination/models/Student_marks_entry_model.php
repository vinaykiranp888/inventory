<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_marks_entry_model extends CI_Model
{
    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }
    
    function getStudentListForCourseRegisteredStudent($data)
    {
        $this->db->select('DISTINCT(s.id) as id, s.*, p.code as programme_code, p.name as programme_name, c.registration_number as company_registration_number, c.name as company_name, ihs.id as id_student_has_programme');
        $this->db->from('student_has_programme as ihs');
        $this->db->join('student as s', 'ihs.id_student = s.id');
        $this->db->join('programme as p', 'ihs.id_programme = p.id');
        $this->db->join('company as c', 's.id_company = c.id','left');
        $this->db->where('ihs.status', 1);
        $this->db->where('ihs.id_marks_entry', 0);
        $this->db->where('ihs.id_marks_adjustment', 0);
        $this->db->where('ihs.marks', 0);
        $this->db->where('ihs.id_programme', $data['id_programme']);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentHasProgramme($id)
    {
        $this->db->select('md.*');
        $this->db->from('student_has_programme as md');
        $this->db->where('md.id', $id);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function addStudentMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateStudentHasProgramme($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_has_programme', $data);
        return TRUE;
    }
}