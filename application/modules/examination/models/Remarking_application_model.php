<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Remarking_application_model extends CI_Model
{

    function semesterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function intakeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }

    function gradeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grade');
        $this->db->where('status', $status);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($query);die;
        return $result;
    }



    function getStudentByStudent($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, p.code as programme_code, i.name as intake_name, i.year as intake_year, qs.name as qualification_name, qs.code as qualification_code, adv.ic_no, adv.name as advisor');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->join('staff as adv', 's.id_advisor = adv.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getSemesterByStudentId($id_student)
    {
        $this->db->select('DISTINCT(sem.id) as id, sem.name as semester_name, sem.code as semester_code');
        $this->db->from('exam_registration as er');
        $this->db->join('semester as sem', 'er.id_semester = sem.id');
        $this->db->where('er.id_student', $id_student);
        $this->db->order_by("sem.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    
    function getStudentInfo($data)
    {
        // echo "<Pre>";print_r($data);exit;
        $this->db->select('stu.id as id_student, stu.full_name, stu.nric, sem.id as id_semester, sem.name as semester_name, sem.code as semester_code');
        $this->db->from('exam_registration as er');
        $this->db->join('student as stu', 'er.id_student = stu.id');
        $this->db->join('semester as sem', 'er.id_semester = sem.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('er.id_programme', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('er.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('er.id_semester', $data['id_semester']);
        }
        if ($data['id_student'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('er.id_student', $data['id_student']);
        }
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }










    function getCoursesRegisteredLandscape()
    {
        $this->db->select('DISTINCT(i.id_course) as id_course,i.id');
        $this->db->from('add_course_to_program_landscape as i');
        // $this->db->where('i.id_semester', $id_semester);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();

        foreach ($results as $result)
        {
            $id_course = $result->id_course;

            $course = $this->getCourse($id_course);

            $value = new \stdClass();
            $value->code = $course->code;
            // echo "<Pre>";print_r($value);exit;
            $value->name = $course->name;
            $value->id_course_landscape = $result->id;
            $value->id = $result->id_course;
            $value->credit_hours = $course->credit_hours;
            

            array_push($details, $value);
        }

        return $details;
    }





    function getCoursesBySemester($id_semester)
    {
        $this->db->select('DISTINCT(i.id_course) as id_course,i.id');
        $this->db->from('add_course_to_program_landscape as i');
        $this->db->where('i.id_semester', $id_semester);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();

        foreach ($results as $result)
        {
            $id_course = $result->id_course;

            $course = $this->getCourse($id_course);

            $value = new \stdClass();
            $value->code = $course->code;
            // echo "<Pre>";print_r($value);exit;
            $value->name = $course->name;
            $value->id_course_landscape = $result->id;
            $value->id = $result->id_course;
            $value->credit_hours = $course->credit_hours;
            

            array_push($details, $value);
        }

        return $details;
    }

    function getCourse($id)
    {
        $this->db->select('i.*');
        $this->db->from('course as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getRemarkEntry($id)
    {
         $this->db->select('sre.*, c.code as course_code, c.name as course_name, c.credit_hours');
        $this->db->from('student_remarks_entry as sre');
        $this->db->join('add_course_to_program_landscape as aprl', 'sre.id_course_registered_landscape = aprl.id');
        $this->db->join('course as c', 'aprl.id_course = c.id');
        $this->db->where('sre.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getExamConfiguration()
    {
        $this->db->select('*');
        $this->db->from('exam_configuration');
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentMarksEntry($id)
    {
        $this->db->select('*');
        $this->db->from('student_marks_entry');
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }


    function getStudentMarkEntryList($data)
    {
        $exam_configuration = $this->getExamConfiguration();

        $max_count = $exam_configuration->max_count;

        $this->db->select('a.*, s.nric, s.full_name, c.code as course_code, c.name as course_name, c.credit_hours, i.year as intake_year, i.name as intake_name, p.code as programme_code, p.name as programme_name');
        $this->db->from('student_remarks_entry as a');
        $this->db->join('student as s', 'a.id_student = s.id');
        $this->db->join('add_course_to_program_landscape as acpl', 'a.id_course_registered_landscape = acpl.id');
        $this->db->join('course as c', 'acpl.id_course = c.id');
        $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('programme as p', 'a.id_program = p.id');
        // $this->db->where('a.id_semester', $id_semester);
        // $this->db->where('a.status', '1');
        // $this->db->where('a.is_remarking <', $max_count);
        // if ($data['id_semester'] != '')
        // {
        //     $this->db->where('a.id_semester', $data['id_semester']);
        // }
        if ($data['id_course_registered_landscape'] != '')
        {
            $this->db->where('a.id_course_registered_landscape', $data['id_course_registered_landscape']);
        }
         // $this->db->where('a.id_course_registered_landscape', $id_course_registered_landscape);
        $query = $this->db->get();
        $results = $query->result();

        // $details = array();

        //  foreach ($results as $result)
        //  {
        //         // $student_data = new \stdClass();

        //      $id_student = $result->id_student;
        //      $id_mark_entry = $result->id_mark_entry;

        //      $student_data = $this->getStudentByStudent($id_student);
        //      $mark_data = $this->getStudentMarksEntry($id_mark_entry);

        //      $student_data->id_mark_entry = $id_mark_entry;
        //      $student_data->is_remarking = $mark_data->is_remarking;
        //      $student_data->result = $mark_data->result;
        //      $student_data->grade = $mark_data->grade;
        //      $student_data->max_count = $max_count;
             
        //      array_push($details, $student_data);

        //  }
         return $results; 
    }


    function getStudentMarkEntryDetailsByIdSemesterNIdCourse($id_semester,$id_course_registered_landscape)
    {
        $exam_configuration = $this->getExamConfiguration();

        $max_count = $exam_configuration->max_count;

        $this->db->select('DISTINCT(a.id_student) as id_student, a.id as id_mark_entry');
        $this->db->from('student_marks_entry as a');
        // $this->db->where('a.id_semester', $id_semester);
        $this->db->where('a.status', '1');
        $this->db->where('a.is_remarking <', $max_count);
         $this->db->where('a.id_course_registered_landscape', $id_course_registered_landscape);
        $query = $this->db->get();
        $results = $query->result();

        $details = array();

         foreach ($results as $result)
         {
                // $student_data = new \stdClass();

             $id_student = $result->id_student;
             $id_mark_entry = $result->id_mark_entry;

             $student_data = $this->getStudentByStudent($id_student);
             $student_data->id_mark_entry = $id_mark_entry;
             array_push($details, $student_data);

         }
         return $details; 
    }

    function getIntakeByProgrammeId($id_programme)
    {
        $this->db->select('DISTINCT(i.id) as id, i.*');
        $this->db->from('intake_has_programme as ihs');
        $this->db->join('intake as i', 'ihs.id_intake = i.id');
        $this->db->where('ihs.id_programme', $id_programme);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentListForCourseRegisteredStudent($data)
    {
        $this->db->select('DISTINCT(s.id) as id, s.*, i.name as intake, p.code as program_code, p.name as program, ihs.id as id_course_registration');
        $this->db->from('course_registration as ihs');
        $this->db->join('student as s', 'ihs.id_student = s.id');
        $this->db->join('intake as i', 's.id_intake = i.id');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('mark_distribution as md', 'ihs.id_course_registered_landscape = md.id_course_registered');
        $this->db->where('md.id_program', $data['id_program']);
        $this->db->where('md.id_intake', $data['id_intake']);
        $this->db->where('md.id_course_registered', $data['id_course']);
        $this->db->where('md.status', 1);
        $this->db->where('ihs.is_exam_registered !=', 0);
        $this->db->where('ihs.is_result_announced', 0);
        $this->db->where('ihs.is_bulk_withdraw', 0);
        $this->db->where('ihs.id_programme', $data['id_program']);
        $this->db->where('ihs.id_intake', $data['id_intake']);
        $this->db->where('ihs.id_course_registered_landscape', $data['id_course']);
        $query = $this->db->get();
        return $query->result();
    }

    

    function getMarkDistributionByProgNIntakeNIdCourseRegLandscape($id_program, $id_intake, $id_course_registered_landscape)
    {
        $this->db->select('md.*');
        $this->db->from('mark_distribution as md');
        // $this->db->join('staff as adv', 's.id_advisor = adv.id');
        $this->db->where('md.id_program', $id_program);
        $this->db->where('md.id_intake', $id_intake);
        $this->db->where('md.id_course_registered', $id_course_registered_landscape);
        $this->db->where('md.status', '1');
        $query = $this->db->get();
        $result = $query->row(); 
                
        // echo "<Pre>";print_r($result);exit;
        
        $details= array();
        if($result != '')
        {
            $details = $this->getMarkDistributionDetailsByIdMarkDistribution($result->id);
        }

        return $details;
    }

    function getMarkDistributionDetailsByIdMarkDistribution($id_mark_distribution)
    {
        $this->db->select('tctd.*, ec.name as component_name, ec.code as component_code,');
        $this->db->from('mark_distribution_details as tctd');
        $this->db->join('examination_components as ec', 'tctd.id_component = ec.id');
        $this->db->where('tctd.id_mark_distribution', $id_mark_distribution);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCourseRegisteredDetails($id_course_registered_landscape)
    {
        $this->db->select('md.*, c.name as course_name, c.code as course_code');
        $this->db->from('add_course_to_program_landscape as md');
        $this->db->join('course as c', 'md.id_course = c.id');
        $this->db->where('md.id', $id_course_registered_landscape);
        $this->db->order_by('md.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function addStudentReMarksEntry($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_remarks_entry', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addStudentReMarksEntryDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_remarks_entry_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateMarkEntryCount($id,$id_remark)
    {
        $mark_entry = $this->getStudentMarksEntry($id);
        $data['is_remarking'] = $mark_entry->is_remarking + 1;


        $updated = $this->updateMarkEntry($data,$id);

        return TRUE;
    }

    function updateStudentReMarksEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_remarks_entry', $data);
        return TRUE;
    }

    function updateCourseRegistration($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('course_registration', $data);
        return TRUE;
    }

    function combineDataAddMarkHistoryOfStudent($data,$id_course_registration,$id_student,$id_student_remarks_entry)
    {
        $data['id_course_registration'] = $id_course_registration;
        $data['id_student'] = $id_student;
        $data['id_student_remarks_entry'] = $id_student_remarks_entry;
        $data['created_by'] = $data['updated_by'];
        unset($data['is_result_announced']);


        $inserted = $this->addMarkHistoryOfStudent($data);
        return $inserted;
    }

    function addMarkHistoryOfStudent($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_marks_entry_history', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getStudentList()
    {
        $this->db->select('er.*');
        $this->db->from('student as er');
        $this->db->where('er.applicant_status !=', 'Graduated');
        $this->db->order_by("er.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function marksReEntryListSearch($data)
    {
        $this->db->select('ssr.*, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('student_remarks_entry as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('ssr.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('ssr.id_intake', $data['id_intake']);
        }
        if ($data['name'] != '')
        {
            $likeCriteria = "(stu.full_name  LIKE '%" . $data['name'] . "%' or stu.nric  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['status'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('ssr.status', $data['status']);
        }
         if ($data['updated'] != '')
        {
            // echo "<Pre>";print_r($status);exit();
            $this->db->where('ssr.updated', $data['updated']);
        }
        $this->db->order_by("ssr.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function editStudentRemark($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_remarks_entry', $data);
        return TRUE;
    }

    function updateMarkEntry($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('student_marks_entry', $data);
        return TRUE;
    }

    // function getStudentMarksEntry($id)
    // {
    //     $this->db->select('ssr.*, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code');
    //     $this->db->from('student_marks_entry as ssr');
    //     $this->db->join('student as stu', 'ssr.id_student = stu.id');
    //     $this->db->join('intake as i', 'ssr.id_intake = i.id');
    //     $this->db->join('programme as p', 'ssr.id_program = p.id');
    //     $this->db->where('ssr.id', $id);

    //     $query = $this->db->get();
    //      $result = $query->row();  
    //      return $result;

    // }

    function getStudentMarksEntryDetailsByMasterId($id_marks_entry)
    {

        $this->db->select('md.*, c.name as component_name, c.code as component_code');
        $this->db->from('student_marks_entry_details as md');
        $this->db->join('examination_components as c', 'md.id_component = c.id');
        $this->db->where('md.id_student_marks_entry', $id_marks_entry);
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getStudentMarkingDetails($id_marks_entry)
    {

        $this->db->select('md.*');
        $this->db->from('student_marks_entry_details as md');
        $this->db->join('examination_components as c', 'md.id_component = c.id');
        $this->db->where('md.id', $id_marks_entry);
        $query = $this->db->get();
        $result = $query->row(); 

        return $result;
    }

    function getStudentReMarksEntryDetailsByMasterId($id_marks_entry)
    {
        $this->db->select('md.*, c.name as component_name, c.code as component_code');
        $this->db->from('student_remarks_entry_details as md');
        $this->db->join('examination_components as c', 'md.id_component = c.id');
        $this->db->where('md.id_student_remarks_entry', $id_marks_entry);
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function getStudentsListByMarksEntry($data)
    {
        $this->db->select('DISTINCT(md.id_student) as id_student');
        $this->db->from('student_marks_entry as md');
        $this->db->where('md.id_program', $data['id_program']);
        $this->db->where('md.id_intake', $data['id_intake']);
        $query = $this->db->get();
        $results = $query->result(); 
        $details = array();
        foreach ($results as $result)
        {
            $student = $this->getStudent($result->id_student);
            array_push($details, $student);
        }
        return $details;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id');
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getStudentCoursesByStudentId($data)
    {
        $this->db->select('DISTINCT(md.id_course_registered_landscape) as id_course_registered_landscape, md.id');
        $this->db->from('student_marks_entry as md');
        $this->db->where('md.id_program', $data['id_program']);
        $this->db->where('md.id_intake', $data['id_intake']);
        $this->db->where('md.id_student', $data['id_student']);
        $query = $this->db->get();
        $results = $query->result(); 
        $details = array();
        foreach ($results as $result)
        {
            $course = $this->getCourseByRegisteredlandscape($result->id_course_registered_landscape);
        // return $course;
            $course->id_course_registered_landscape = $result->id_course_registered_landscape;
            $course->id_mark_entry = $result->id;
            array_push($details, $course);
        }
        return $details;
    }

    function getCourseByRegisteredlandscape($id)
    {
        $this->db->select('c.*');
        $this->db->from('add_course_to_program_landscape as i');
        $this->db->join('course as c', 'i.id_course = c.id');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getStudentMarksEntryForDetails($id)
    {
        $exam_configuration = $this->getExamConfiguration();

        $max_count = $exam_configuration->max_count;

        $this->db->select('ssr.*, stu.full_name as student_name, stu.nric, i.year as intake_year, i.name as intake_name, p.name as programme_name, p.code as programme_code');
        $this->db->from('student_marks_entry as ssr');
        $this->db->join('student as stu', 'ssr.id_student = stu.id');
        $this->db->join('intake as i', 'ssr.id_intake = i.id');
        $this->db->join('programme as p', 'ssr.id_program = p.id');
        $this->db->where('ssr.id', $id);
        $this->db->where('ssr.is_remarking <', $max_count);

        $query = $this->db->get();
         $result = $query->row();  
         return $result;

    }

    function getStudentMarksEntryDetailsByMasterIdForDetails($id_marks_entry)
    {

        $this->db->select('md.*, c.name as component_name, c.code as component_code');
        $this->db->from('student_marks_entry_details as md');
        $this->db->join('examination_components as c', 'md.id_component = c.id');
        $this->db->where('md.id_student_marks_entry', $id_marks_entry);
        $query = $this->db->get();
        $result = $query->result(); 

        return $result;
    }

    function addMainInvoice($id_mark_entry,$id_remark_entry)
    {
        $user_id = $this->session->userId;

        $data_row = $this->getStudentMarksEntry($id_mark_entry);

        // echo "<Pre>";print_r($data_row);exit();

        $id_student = $data_row->id_student;
        $id_program = $data_row->id_program;
        $id_intake = $data_row->id_intake;

        $invoice_number = $this->generateMainInvoiceNumber();

        $details = $this->getFeeStructureByActivity();
        // echo "<Pre>";print_r($details);exit();

        $invoice['invoice_number'] = $invoice_number;
        $invoice['type'] = 'Student';
        $invoice['remarks'] = 'Student Apply For Remarking / Remarking';
        $invoice['id_application'] = '0';
        $invoice['id_program'] = $id_program;
        $invoice['id_intake'] = $id_intake;
        $invoice['id_student'] = $id_student;
        $invoice['total_amount'] = $details->amount;
        $invoice['invoice_total'] = $details->amount;
        $invoice['balance_amount'] = $details->amount;
        $invoice['paid_amount'] = '0';
        $invoice['status'] = '1';
        $invoice['created_by'] = $user_id;

        $inserted_id = $this->addNewMainInvoice($invoice);


        $data = array(
            'id_main_invoice' => $inserted_id,
            'id_fee_item' => $details->id_fee_setup,
            'amount' => $details->amount,
            'status' => '1',
            'created_by' => $user_id
        );
        $this->addNewMainInvoiceDetails($data);

        return TRUE;
    }

    

    function getFeeStructureByActivity()
    {
        $this->db->select('*');
        $this->db->from('fee_structure_activity');
        $this->db->order_by("id", "DESC");
        // $this->db->where('md.id_student_marks_entry', $id_marks_entry);
        $query = $this->db->get();
        return $query->row();
    }

    function editMainInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }




    function getFeeStructureActivityType($type,$trigger,$id_program)
    {
        $this->db->select('s.*');
        $this->db->from('fee_structure_activity as s');
        $this->db->join('activity_details as a', 's.id_activity = a.id');
        $this->db->where('a.name', $type);
        $this->db->where('s.trigger', $trigger);
        $this->db->where('s.id_program', $id_program);
        $this->db->where('s.status', 1);
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function generateMainInvoice($data,$id_credit_transfer)
    {
        $user_id = $this->session->userId;

        // echo "<Pre>";print_r($id_credit_transfer);exit();

        $id_student = $data['id_student'];
        $add = $data['add'];

        $student_data = $this->getStudent($id_student);


        $nationality = $student_data->nationality;
        $id_program = $student_data->id_program;
        $id_intake = $student_data->id_intake;



        // echo "<Pre>";print_r($application_type);exit();



        if($add == 1)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('EXAMINATION REMARKING','Application Level',$id_program);
        }
        elseif($add == 0)
        {
            $fee_structure_data = $this->getFeeStructureActivityType('EXAMINATION REMARKING','Approval Level',$id_program);
        }


        if($nationality == 'Malaysian')
        {
            $currency = 'MYR';
            $invoice_amount = $fee_structure_data->amount_local;
        }
        elseif($nationality == 'Other')
        {
            $currency = 'USD';
            $invoice_amount = $fee_structure_data->amount_international;
        }

        // echo "<Pre>";print_r($invoice_amount);exit();


        $invoice_number = $this->generateMainInvoiceNumber();


        $invoice['invoice_number'] = $invoice_number;
        $invoice['type'] = 'Student';
        $invoice['remarks'] = 'Student EXAMINATION REMARKING';
        $invoice['id_application'] = '0';
        $invoice['id_program'] = $id_program;
        $invoice['id_intake'] = $id_intake;
        $invoice['id_student'] = $id_student;
        $invoice['id_student'] = $id_student;
        $invoice['currency'] = $currency;
        $invoice['total_amount'] = $invoice_amount;
        $invoice['invoice_total'] = $invoice_amount;
        $invoice['balance_amount'] = $invoice_amount;
        $invoice['paid_amount'] = '0';
        $invoice['status'] = '1';
        $invoice['created_by'] = $user_id;


        // echo "<Pre>";print_r($invoice);exit();

        // $fee_structure_data = $this->getFeeStructure($id_program,$id_intake,$id_program_scheme);

        
        // $update = $this->editStudentData($id_program_scheme,$id_program,$id_intake,$id_student);
        
        $inserted_id = $this->addNewMainInvoice($invoice);

        if($inserted_id)
        {
            $data = array(
                    'id_main_invoice' => $inserted_id,
                    'id_fee_item' => $fee_structure_data->id_fee_setup,
                    'amount' => $invoice_amount,
                    'status' => 1,
                    'quantity' => 1,
                    'price' => $invoice_amount,
                    'id_reference' => $id_credit_transfer,
                    'description' => 'EXAMINATION REMARKING',
                    'created_by' => $user_id
                );

            $this->addNewMainInvoiceDetails($data);
        }
        return TRUE;
    }

    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('j.*');
            $this->db->from('main_invoice as j');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
           $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
           return $jrnumber;        
    }

    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewMainInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        // return $insert_id;
    }

    function getStudent($id_student)
    {
        $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }














    function deleteTempAmountDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_receipt_details');
    }
}