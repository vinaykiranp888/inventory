<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Publish_exam_result_model extends CI_Model
{
    function examResultList()
    {
        $this->db->select('pe.*, p.name as program, s.name as semester, in.name as intake');
        $this->db->from('publish_exam as pe');
        $this->db->join('programme as p', 'pe.id_program = p.id');
        $this->db->join('semester as s', 'pe.id_semester = s.id');
        $this->db->join('intake as in', 'pe.id_intake = in.id');
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function examResultListSearch($data)
    {
        $this->db->select('pe.*, p.name as program, s.name as semester, in.name as intake');
        $this->db->from('publish_exam as pe');
        $this->db->join('programme as p', 'pe.id_program = p.id');
        $this->db->join('semester as s', 'pe.id_semester = s.id');
        $this->db->join('intake as in', 'pe.id_intake = in.id');
        if ($data['id_programme'] != '')
        {
            $this->db->where('pe.id_program', $data['id_programme']);
        }
        if ($data['id_intake'] != '')
        {
            $this->db->where('pe.id_intake', $data['id_intake']);
        }
        if ($data['id_semester'] != '')
        {
            $this->db->where('pe.id_semester', $data['id_semester']);
        }
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function intakeList()
    {
        $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function programList()
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', '1');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getExamResultDetails($id)
    {
        $this->db->select('*');
        $this->db->from('publish_exam');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewExamResult($data)
    {
        $this->db->trans_start();
        $this->db->insert('publish_exam', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamResultDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('publish_exam', $data);
        return TRUE;
    }

    function getProgrammeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getIntakeListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('intake');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getSemesterListByStatus($status)
    {
         $this->db->select('*');
        $this->db->from('semester');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
}

