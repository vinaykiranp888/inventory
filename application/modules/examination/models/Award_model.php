<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Award_model extends CI_Model
{
    function awardList()
    {
        $this->db->select('a.*');
        $this->db->from('award as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardListSearch($formData)
    {
        $this->db->select('a.*,c.template_file,c.template_name');
        $this->db->from('award as a');
        $this->db->join('certificate_template as c', 'c.id = a.id_certificate_template','left');

        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.description  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($formData['id_programme']))
        {
            $likeCriteria = "(a.id_programme  LIKE '%" . $formData['id_programme'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAward($id)
    {
        $this->db->select('*');
        $this->db->from('award');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCertificate($id)
    {
        $this->db->select('*');
        $this->db->from('certificate_template');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getCerficateTemplate()
    {
        $this->db->select('*');
        $this->db->from('certificate_template');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewAward($data)
    {
        $this->db->trans_start();
        $this->db->insert('award', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAward($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('award', $data);
        return TRUE;
    }

    function addAwardCondition($data)
    {
        $this->db->trans_start();
        $this->db->insert('award_condition', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;        
    }

    function editAwardCondition($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('award_condition', $data);
        return TRUE;
    }

    function getConditionListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme_condition');
        $this->db->where('status', $status);
        $query = $this->db->get();
        return $query->result();        
    }

    function getAwardConditionList($id_award)
    {
        $this->db->select('ac.*, c.name as condition_name');
        $this->db->from('award_condition as ac');
        $this->db->join('programme_condition as c', 'ac.id_condition = c.id');
        $this->db->where('ac.status', 1);
        $this->db->where('ac.id_award', $id_award);
        $query = $this->db->get();
        return $query->result();        
    }

    function getAwardCondition($id)
    {
        $this->db->select('ac.*');
        $this->db->from('award_condition as ac');
        $this->db->where('ac.id', $id);
        $query = $this->db->get();
        return $query->row();        
    }

    function deleteAwardConditionDetails($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('award_condition');
        return TRUE;
    }
}

