<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Grade Setup</h3>
            <a href="../list" class="btn btn-link"> < Back</a>
        </div>

        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Grade Setup Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_program)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Program <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_intake)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->year . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Programme Landscape <span class='error-text'>*</span></label>
                        <select name="id_programme_landscape" id="id_programme_landscape" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeLandscapeList))
                            {
                                foreach ($programmeLandscapeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_programme_landscape)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->program_landscape_type . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_course)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>


        


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="test" class="form-control" id="status" name="status" value="<?php 
                        if($markDistribution->status=='0')
                        {
                             echo "Pending";
                          }
                          elseif($markDistribution->status=='1'){
                            echo "Approved";
                          }elseif($markDistribution->status=='2'){
                            echo "Rejected";
                          };?>

                        " readonly >
                    </div>
                </div>

        <?php
        if($markDistribution->status == '2')
        {
            ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $markDistribution->reason ?>" readonly >
                    </div>
                </div>

        <?php
        }
        ?>

            

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg" >Save</button> -->
                <!-- <a href="../list" class="btn btn-link">Back</a> -->
            </div>
        </div>


        </form>


    <?php
        if($markDistribution->status == '0')
        {
            ?>

        <form id="form_detail" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Add Distribution Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Exam Components <span class='error-text'>*</span></label>
                        <select name="id_component" id="id_component" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            {
                            if (!empty($componentList))
                                foreach ($componentList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="max_marks" id="max_marks" autocomplete="off">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Attendance Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="attendance_status" id="attendance_status" value="1" checked="checked"><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="attendance_status" id="attendance_status" value="0"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>


                
            </div>


            <div class="row">



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Pass Compulsary <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass_compulsary" id="is_pass_compulsary" value="1" checked="checked"><span class="check-radio" onclick="showPassMarks()"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_pass_compulsary" id="is_pass_compulsary" value="0" onclick="hidePassMarks()"><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>


                <div class="col-sm-4" id="view_pass_marks">
                    <div class="form-group">
                        <label>Pass Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" name="pass_marks" id="pass_marks" autocomplete="off" value="">
                        <!-- <span id='view_centers'></span> -->
                    </div>
                </div>


            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="button" class="btn btn-primary btn-lg" onclick="saveDetailData()">Add</button>
                    <!-- <a href="list" class="btn btn-link">Cancel</a> -->
                </div>
            </div>

            </div>

        </form>


        <?php
        }
    ?>




     <?php

                    if(!empty($markDistributionDetails))
                    {
                        ?>
                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Course Details</h4>
                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Exam Components</th>
                                        <th>Pass Compulsary</th>
                                        <th>Pass Marks</th>
                                        <th>Max. Marks</th>
                                        <th>Attendance Status</th>
                                        <?php
                                        if($markDistribution->status == '0')
                                        {
                                            ?>
                                         <th class="text-center">Action</th>

                                           <?php
                                            }
                                        ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($markDistributionDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $markDistributionDetails[$i]->component_code . " - " . $markDistributionDetails[$i]->component_name;?></td>
                                        <td><?php if($markDistributionDetails[$i]->is_pass_compulsary == 1)
                                        {
                                            echo 'Yes';
                                        }else
                                        {
                                            echo 'No';
                                        }
                                        ?></td>
                                        <td><?php echo $markDistributionDetails[$i]->pass_marks; ?></td>
                                        <td><?php echo $markDistributionDetails[$i]->max_marks; ?></td>
                                        <td><?php if($markDistributionDetails[$i]->attendance_status == 1)
                                        {
                                            echo 'Yes';
                                        }else
                                        {
                                            echo 'No';
                                        }
                                        ?></td>
                                        <?php
                                        if($markDistribution->status == '0')
                                        {
                                        ?>

                                        <td class="text-center">
                                        <a onclick="deleteDetailData(<?php echo $markDistributionDetails[$i]->id; ?>)">Delete</a>
                                        </td>

                                        <?php
                                            }
                                        ?>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function saveDetailData()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};

        tempPR['id_component'] = $("#id_component").val();
        tempPR['is_pass_compulsary'] = $("#is_pass_compulsary").val();
        tempPR['pass_marks'] = $("#pass_marks").val();
        tempPR['max_marks'] = $("#max_marks").val();
        tempPR['attendance_status'] = $("#attendance_status").val();
        tempPR['id_mark_distribution'] = <?php echo $markDistribution->id; ?>;

            $.ajax(
            {
               url: '/examination/markDistribution/saveDetailData',
                type: 'POST',
                // type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {

                // alert(result);
                // $('#myModal').modal('show');
                // $("#view_requirement_data").html(result);
                // $("#view_temp_details").html(result);

                // location.reload();
                window.location.reload();

               }
            });
        }
    }

    function deleteDetailData(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/examination/markDistribution/deleteDetailData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view_temp_details").html(result);
                    window.location.reload();
                    // alert(result);
                    // window.location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_program: {
                    required: true
                },
                id_intake: {
                    required: true
                },
                id_course_registered: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_course_registered: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
    

    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_component: {
                    required: true
                },
                is_pass_compulsary: {
                    required: true
                },
                pass_marks: {
                    required: true
                },
                 max_marks: {
                    required: true
                },
                 attendance_status: {
                    required: true
                }
            },
            messages: {
                id_component: {
                    required: "<p class='error-text'>Subject Component</p>",
                },
                is_pass_compulsary: {
                    required: "<p class='error-text'>Select Pass Compulsary</p>",
                },
                pass_marks: {
                    required: "<p class='error-text'>Pass Marks Required</p>",
                },
                max_marks: {
                    required: "<p class='error-text'>Ma. Marks Required</p>",
                },
                attendance_status: {
                    required: "<p class='error-text'>Select Attendence Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
  } );

    $('select').select2();

</script>