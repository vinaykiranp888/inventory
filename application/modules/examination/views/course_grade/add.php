<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Course Grade</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Course Grade Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo  $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo  $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade <span class='error-text'>*</span></label>
                        <select name="id_grade" id="id_grade" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($gradeList))
                            {
                                foreach ($gradeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo  $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Minimum Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="minmarks" name="minmarks">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Maximum Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="maxmarks" name="maxmarks">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label><br>
                            <label class="radio-inline">
                              <input type="radio" name="result" id="result" value="pass"><span class="check-radio"></span> Pass
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="result" id="result" value="fail"><span class="check-radio"></span> Fail
                            </label>
                    </div>
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_program: {
                    required: true
                },
                 id_course: {
                    required: true
                },
                id_grade: {
                    required: true
                },
                 minmarks: {
                    required: true
                },
                maxmarks: {
                    required: true
                },
                 result: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                },
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                minmarks: {
                    required: "<p class='error-text'>Enter Min Marks</p>",
                },
                maxmarks: {
                    required: "<p class='error-text'>Enter Max Marks</p>",
                },
                result: {
                    required: "<p class='error-text'>Select Result</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
