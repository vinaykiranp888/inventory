<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Grade</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Grade Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $grade->name; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_programme" id="id_programme" class="form-control" style="width: 408px">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    <?php
                                    if($record->id == $grade->id_programme)
                                    {
                                        echo 'selected';
                                    }
                                    ?>
                                    ><?php echo $record->code ." - ".$record->name; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Min. Range <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="min_range" name="min_range" min="0" max="99" value="<?php echo $grade->min_range; ?>">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Range <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_range" name="max_range" min="1" max="100" value="<?php echo $grade->max_range; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade Level Up <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="grade_level_up" name="grade_level_up" value="<?php echo $grade->grade_level_up; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($grade->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($grade->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 id_programme: {
                    required: true
                },
                min_range: {
                    required: true
                },
                max_range: {
                    required: true
                },
                grade_level_up: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Grade Name Required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                min_range: {
                    required: "<p class='error-text'>Min. Range Required</p>",
                },
                max_range: {
                    required: "<p class='error-text'>Max. Range Required</p>",
                },
                grade_level_up: {
                    required: "<p class='error-text'>Grade Level Up Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
