<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Award</h3>
        </div>
        <form id="form_award" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Award Details</h4>

            <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Title <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Starting Serial Format <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="starting_serial_alphabet" name="starting_serial_alphabet">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Starting Serial Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="starting_serial_number" name="starting_serial_number">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Certificate <span class='error-text'>*</span></label>
                       <select class="form-control" id="id_certificate_template" name="id_certificate_template" onchange="getCertificate(this.value)">
                            <option value="">Select</option>
                            <?php for($i=0;$i<count($certificate);$i++) { ?> 
                            <option value="<?php echo $certificate[$i]->id;?>"><?php echo $certificate[$i]->template_name;?></option>
                            <?php } ?>                            
                        </select>
                    </div>
                </div>
                

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

            <!-- <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Level <span class='error-text'>*</span></label>
                        <select class="form-control" id="level" name="level">
                            <option value="">Select</option>
                            <?php 
                             $start = 1;
                             $end = 11;
                             for($i=$start;$i<$end;$i++)
                                {
                                    ?>
                                    <option value=<?php echo $i;?> >
                                        <?php echo $i;?></option>
                                    <?php
                                } 
                                ?>
                        </select>
                    </div>
                </div>


                src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"
               
            </div> -->

            <div class="row" id="view_template_file" style="display: none;">
                <span><img align="center" height="480px" width="100%" src="" id="assign_template"></span>
            </div>

        </div>

        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();

    function getCertificate(id)
    {
        if(id >0)
        {
            $.ajax(
            {
               url: '/examination/award/getCertificate/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_template_file").show();
                  $("#assign_template").attr("src","<?php echo BASE_PATH; ?>assets/images/" + result);
               }
            });
        }
    }


    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                id_certificate_template: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                level: {
                    required: true
                },
                starting_serial_alphabet: {
                    required: true
                },
                starting_serial_number: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Award Title Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                id_certificate_template: {
                    required: "<p class='error-text'>Select Certificate</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                level: {
                    required: "<p class='error-text'>Select Level</p>",
                },
                starting_serial_alphabet: {
                    required: "<p class='error-text'>Serial Number Format Required</p>",
                },
                starting_serial_number: {
                    required: "<p class='error-text'>Serial Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
