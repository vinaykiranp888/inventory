
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <form id="form_main" action="" method="post" enctype="multipart/form-data">


        <ul class="page-nav-links">
            <li><a href="/examination/award/edit/<?php echo $id_award;?>">Award Details</a></li>
            <li><a href="/examination/award/condition/<?php echo $id_award;?>">Condition Details</a></li>
            <li><a href="/examination/award/assesment/<?php echo $id_award;?>">Assesment Details</a></li>
            <li class="active"><a href="/examination/award/activities/<?php echo $id_award;?>">Activities</a></li>


        </ul>


        <div class="form-container">
            <h4 class="form-group-title">Award Activities Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Condition <span class='error-text'>*</span></label>
                        <select name="id_condition" id="id_condition" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($conditionList))
                            {
                                foreach ($conditionList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                      <?php
                                      if($awardCondition->id_condition == $record->id)
                                      {
                                        echo 'selected';
                                      }
                                      ?>
                                      >
                                        <?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>

        </div>


        <div class="button-block clearfix">
           <div class="bttn-group">
              <button type="submit" class="btn btn-primary btn-lg" >Save</button>
              <?php 
              if($id_award_condition != NULL)
              {
              ?>
              
              <a href="../.../condition/<?php echo $id_award; ?>" class="btn btn-link">Cancel</a>
              
              <?php
              }
              ?>
           </div>
        </div>
    </form>

    <?php

      if(!empty($awardConditionList))
      {
          ?>
        <br>

        <div class="form-container">
                <h4 class="form-group-title">Award Condition Details</h4>

            

              <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>Condition Name</th>
                         <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($awardConditionList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $awardConditionList[$i]->condition_name;?></td>
                        </td>
                        <td>
                        <a href='/examination/award/condition/<?php echo $id_award;?>/<?php echo $awardConditionList[$i]->id;?>'>Edit</a> |
                        <a onclick="deleteAwardConditionDetails(<?php echo $awardConditionList[$i]->id; ?>)">Delete</a>
                        </td>

                         </tr>
                      <?php 
                  } 
                  ?>
                    </tbody>
                </table>
              </div>

            </div>




    <?php
    
    }
    ?>


   </div>




  
<footer class="footer-wrapper">
   <p>&copy; 2019 All rights, reserved</p>
</footer>
</div>
<script>

    $('select').select2();

   $(function()
   {
       $(".datepicker").datepicker({
       changeYear: true,
       changeMonth: true,
       });
   });

   function deleteAwardConditionDetails(id)
    {
      var cnf= confirm('Do you really want to delete?');

      if(cnf==true)
      {
        $.ajax(
            {
               url: '/examination/award/deleteAwardConditionDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  window.location.reload();
               }
            });
      }
    }



   $(document).ready(function()
    {
        $("#form_main").validate({
            rules: {
                id_condition: {
                    required: true
                }
            },
            messages: {
                id_condition: {
                    required: "<p class='error-text'>Select Condition</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });   
</script>