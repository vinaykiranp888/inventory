<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Exam Event</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Exam Event Details</h4>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Start Time <span class='error-text'>*</span></label>
                        <input type="time" class="form-control" id="from_tm" name="from_tm" autocomplete="off">
                    </div>
                </div>

                
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event End Time <span class='error-text'>*</span></label>
                        <input type="time" class="form-control" id="to_tm" name="to_tm" autocomplete="off">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Location <span class='error-text'>*</span></label>
                        <select name="id_location" id="id_location" class="form-control" onchange="getCentersByLocatioin(this.value)" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($locationList))
                            {
                                foreach ($locationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Exam Center <span class='error-text'>*</span></label>
                        <span id='view_centers'></span>
                    </div>
                </div>


                
                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Quota No. <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="max_count" name="max_count" min="1">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Event Type <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="type" name="type" value="<?php echo "Exam"; ?>" readonly>
                    </div>
                </div>
                
                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>


    function getCentersByLocatioin(id)
    {

        $.get("/registration/examEvent/getCentersByLocatioin/"+id, function(data, status){
       
            $("#view_centers").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }




    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 from_tm: {
                    required: true
                },
                 id_exam_center: {
                    required: true
                },
                 id_location: {
                    required: true
                },
                 max_count: {
                    required: true
                },
                 status: {
                    required: true
                },
                type : {
                    required: true
                },
                to_tm : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Event Date</p>",
                },
                from_tm: {
                    required: "<p class='error-text'>Select Event Start Time</p>",
                },
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                },
                max_count: {
                    required: "<p class='error-text'>Quota No. Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Event Type</p>",
                },
                to_tm: {
                    required: "<p class='error-text'>Select Event End Time</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

$( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );


</script>