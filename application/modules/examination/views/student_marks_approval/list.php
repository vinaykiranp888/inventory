        <form action="" method="post" id="searchForm">

<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Semester Result</h3>
      <a href="add" class="btn btn-primary">+ Add Course</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Programme</label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($programmeList)) {
                            foreach ($programmeList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_programme']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-4 control-label">Semester</label>
                      <div class="col-sm-8">
                        <select name="id_semester" id="id_semester" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($semesterDetails)) {
                            foreach ($semesterDetails as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_semester']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Intake</label>
                      <div class="col-sm-8">
                        <select name="id_intake" id="id_intake" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($intakeList)) {
                            foreach ($intakeList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_intake']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->year ."-".$record->name;  ?></option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
    
                  </div>
                </div>
                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>SL No</th>
            <th>Student</th>
            <th>Intake</th>
            <th>Programme</th>
            <th>Semester</th>
            <th>Grade</th>
            <th>Result</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($resultData))
          {
            foreach ($resultData as $record) 
            {
              
             ?>
              <tr>
                <td><input type='checkbox' name='results[]' id='results[]' value='<?php echo $record->id; ?>'/> </td>
                <td><?php echo $record->student_name; ?></td>
                <td><?php echo $record->intake_year . " - " .$record->intake_name; ?></td>
                <td><?php echo $record->programme_code . " - " .$record->programme_name; ?></td>
                <td><?php echo $record->semester_code . " - " .$record->semester_name; ?></td>
                <td><?php echo $record->grade; ?></td>
                <td><?php echo $record->result; ?></td>
                <td class="text-center">
                  <!-- <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit"> -->
                    <!-- Edit -->
                  <!-- </a> -->
                </td>
              </tr>
          <?php
            }
          }
          ?>
          <tr>
            <td>
                <button type="submit" class="btn btn-primary" name="Approve" value="Approve">Approve</button>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
        </form>

<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>