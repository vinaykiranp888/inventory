<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Exam Result</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Exam Result Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon"  onchange="getIntakeByProgramme(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Intake <span class='error-text'>*</span></label>
                        <span id="view_intake"></span>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>



            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Exam Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="exam_date" name="exam_date" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label><br>
                            <label class="radio-inline">
                              <input type="radio" name="status" checked="checked" id="status" value="1"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> In-Active
                            </label>
                    </div>
                </div>


            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function getIntakeByProgramme(id)
     {
        $.get("/examination/programGrade/getIntakeByProgramme/"+id, function(data, status){
       
            $("#view_intake").html(data);
            $("#view_intake").show();
        });
     }


     
    $(document).ready(function() {
        $("#form_grade").validate({
            rules: {
                id_intake: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                id_program: {
                    required: true
                },
                 exam_date: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                 id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    $('select').select2();
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        dateFormat: 'dd-mm-yy',
    });
  } );
</script>