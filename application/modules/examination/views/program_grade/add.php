<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Program Grade</h3>
        </div>
        <form id="form_programme_grade" action="" method="post">

        <div class="form-container">
                <h4 class="form-group-title">Program Grade Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon"
                         >
                         <!-- onchange="getIntakeByProgramme(this.value)" -->
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label> Intake <span class='error-text'>*</span></label>
                        <span id="view_intake"></span>
                    </div>
                </div> -->


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Grade <span class='error-text'>*</span></label>
                        <select name="id_grade" id="id_grade" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($gradeList))
                            {
                                foreach ($gradeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Minimum Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="minmarks" name="minmarks">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Maximum Marks <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="maxmarks" name="maxmarks">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Rank <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="rank" name="rank">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Result <span class='error-text'>*</span></label><br>
                            <label class="radio-inline">
                              <input type="radio" name="result" id="result" value="pass"  checked="checked"><span class="check-radio"></span> Pass
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="result" id="result" value="fail"><span class="check-radio"></span> Fail
                            </label>
                    </div>
                </div>
                
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();
</script>
<script>

    function getIntakeByProgramme(id)
     {
        $.get("/examination/programGrade/getIntakeByProgramme/"+id, function(data, status){
       
            $("#view_intake").html(data);
            $("#view_intake").show();
        });
     }




    $(document).ready(function() {
        $("#form_programme_grade").validate({
            rules: {
                id_intake: {
                    required: true
                },
                 id_program: {
                    required: true
                },
                 id_semester: {
                    required: true
                },
                 id_grade: {
                    required: true
                },
                 description: {
                    required: true
                },
                 minmarks: {
                    required: true
                },
                 maxmarks: {
                    required: true
                },
                 rank: {
                    required: true
                },
                 result: {
                    required: true
                }
            },
            messages: {
                id_intake: {
                    required: "<p class='error-text'>Select Intake</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Programme</p>",
                },
                id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Description</p>",
                },
                minmarks: {
                    required: "<p class='error-text'>Enter Min. Marks</p>",
                },
                maxmarks: {
                    required: "<p class='error-text'>Enter Max. Marks</p>",
                },
                rank: {
                    required: "<p class='error-text'>Enter Rank</p>",
                },
                result: {
                    required: "<p class='error-text'>Select Result</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
