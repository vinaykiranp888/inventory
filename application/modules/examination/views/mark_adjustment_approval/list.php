<div class="container-fluid page-wrapper">
  <form id="form_search" method="post" id="searchForm">


  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Students Marks Adjustment Approval</h3>
      <!-- <a href="edit" class="btn btn-primary">+ Add Applicant</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6">


                    <div class="form-group">
                      <label class="col-sm-4 control-label">Programme <span class='error-text'>*</span></label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control selitemIcon" required>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id ==$searchParam['id_programme'])
                              {
                                echo 'selected';
                              }
                              ?>>
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    </div>
                   
                    

                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary" name="btn_submit" value="search">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
      </div>
    </div>

  <?php
    if(!empty($applicantList))
    {

      ?>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Student Name</th>
            <th>Student NRIC</th>
            <th>Programme</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Organisation</th>
            <th>Old Marks</th>
            <th>New Marks</th>
            <th>Updated By</th>
          </tr>
        </thead>
        <tbody>

          <?php          
            $i=1;
            foreach ($applicantList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?>
                  <input type='checkbox' id='id_mark_adjustment[]' name='id_mark_adjustment[]' class='check' value="<?php echo $record->id ?>">
                  <input type='hidden' class='form-control' id='obtained_marks[]' name='obtained_marks[]' value="<?php echo $record->marks_obtained; ?>">
                </td>
                <td><?php echo ucwords($record->full_name); ?></td>
                <td><?php echo $record->nric ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php echo $record->email_id ?></td>
                <td><?php echo $record->phone ?></td>
                <td><?php echo $record->company_registration_number . " - " . $record->company_name ?></td>
                <td><?php echo $record->old_marks ?></td>
                <td><?php echo $record->marks_obtained ?></td>
                <td><?php echo $record->user ?></td>

              </tr>
          <?php
          $i++;
            }
          ?>
        </tbody>
      </table>
    </div>

    <!-- <div class="row">

        <div class="col-sm-10">
            <div class="form-group">
                <label>Reason <span class='error-text'>*</span></label>
                <input type="text" class="form-control" id="reason" name="reason">
            </div>
        </div>

      
    </div> -->

    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" id="btn_submit" value="approve">Approve</button>
        </div>
    </div>


    <?php
    }
    ?>

  </div>

  </form>

  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>


<script type="text/javascript">

    $('select').select2();
    
    function checkValidation()
    {
      if($('#form_search').valid())
        {
            $('#form_search').submit();    
        }
    }



    


    //  $(document).ready(function() {
    //     $("#form_search").validate({
    //         rules: {
    //             reason: {
    //                 required: true
    //             }
    //         },
    //         messages: {
    //             reason: {
    //                 required: "<p class='error-text'>Reason Required</p>",
    //             }
    //         },
    //         errorElement: "span",
    //         errorPlacement: function(error, element) {
    //             error.appendTo(element.parent());
    //         }

    //     });
    // });

</script>