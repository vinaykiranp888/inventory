<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Grade Setup</h3>
            <?php
            if($id_marks_distribution == NULL)
            {
              ?>
                <a href="../list" class="btn btn-link"> < Back</a>
              <?php
            }
            ?>

        </div>



       <div class="form-container">
          <h4 class="form-group-title">Programme Details</h4>

              <div class='data-list'>
                  <div class='row'>
  
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Programme Name :</dt>
                              <dd><?php echo ucwords($programme->name); ?></dd>
                          </dl>
                          <!-- <dl>
                              <dt>Optional Name :</dt>
                              <dd><?php echo $programme->name_optional_language ?></dd>
                          </dl> -->                          
                          <dl>
                              <dt>Max. Duration :</dt>
                              <dd><?php echo $programme->max_duration . " - " . $programme->duration_type; ?></dd>
                          </dl>
                      </div>        
                      
                      <div class='col-sm-6'>
                          <dl>
                              <dt>Programme Code :</dt>
                              <dd><?php echo $programme->code ?></dd>
                          </dl>         
                          <dl>
                              <dt>Type :</dt>
                              <dd><?php echo $programme->internal_external; ?></dd>
                          </dl>
                          <!-- <dl>
                              <dt>Trending In </dt>
                              <dd><?php echo $programme->trending; ?></dd>
                          </dl> -->
                          
                      </div>
  
                  </div>
              </div>


       </div>





    <form id="form_main" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Grade Setup Details</h4>


            <div class="row">

                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Min. Marks <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="min" name="min" min="1" value="<?php echo $markDistribution->min; ?>">
                      </div>
                    </div>


                    <div class="col-sm-4">
                      <div class="form-group">
                         <label>Max. Marks <span class='error-text'>*</span></label>
                         <input type="number" class="form-control" id="max" name="max" max="100" value="<?php echo $markDistribution->max; ?>">
                      </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Grade <span class='error-text'>*</span></label>
                            <select name="id_grade" id="id_grade" class="form-control selitemIcon">
                                <option value="">Select</option>
                                <?php
                                if (!empty($gradeList))
                                {
                                    foreach ($gradeList as $record)
                                    {?>
                                <option value="<?php echo $record->id;  ?>"
                                    <?php
                                    if($record->id == $markDistribution->id_grade)
                                    {
                                        echo 'selected';
                                    } 
                                    ?>>
                                    <?php echo $record->name;?>
                                </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Is Fail <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_fail" id="is_fail" value="1" <?php if($markDistribution->is_fail=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_fail" id="is_fail" value="0" <?php if($markDistribution->is_fail=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>



                </div>


                    


        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="saveMarksDistribution()">Save</button>
                <?php
                if($id_marks_distribution != NULL)
                {
                  ?>
                  <a href="<?php echo '../../add/'. $id_programme ?>" class="btn btn-link">Cancel</a>
                  <?php
                }
                ?>
            </div>
        </div>


    </form>



    <?php

      if(!empty($markDistributionByProgramme))
      {
          ?>
          <br>

          <div class="form-container">
                  <h4 class="form-group-title">Grade Details</h4>

              

                <div class="custom-table">
                  <table class="table">
                      <thead>
                          <tr>
                          <th>Sl. No</th>
                           <th>Min. Marks</th>
                           <th>Max. Marks</th>
                           <th>Grade</th>
                           <th>Is Fail</th>
                           <th style="text-align: center;">Action</th>
                          </tr>
                      </thead>
                      <tbody>
                           <?php
                       $total = 0;
                        for($i=0;$i<count($markDistributionByProgramme);$i++)
                       { ?>
                          <tr>
                          <td><?php echo $i+1;?></td>
                          <td><?php echo $markDistributionByProgramme[$i]->min;?></td>
                          <td><?php echo $markDistributionByProgramme[$i]->max ;?></td>
                          <td><?php echo $markDistributionByProgramme[$i]->grade ;?></td>
                          <td>
                          <?php if($markDistributionByProgramme[$i]->is_fail == 1)
                          {
                            echo 'Yes';
                          }
                          elseif ($markDistributionByProgramme[$i]->is_fail == 0)
                          {
                            echo 'No';
                          } ?>  
                          </td>
                          <td style="text-align: center;">
                          <a href='/examination/gradeSetup/add/<?php echo $id_programme;?>/<?php echo $markDistributionByProgramme[$i]->id;?>'>Edit</a> |
                           <a onclick="deleteMarksDistribution(<?php echo $markDistributionByProgramme[$i]->id; ?>)">Delete</a>
                          </td>

                          </tr>
                        <?php 
                    } 
                    ?>
                      </tbody>
                  </table>
                </div>

              </div>




      <?php
      
      }
       ?>









        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script type="text/javascript">
    
    $('select').select2();


    $(function(){
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2001"
    });
    });

    function saveMarksDistribution()
    {
        if($('#form_main').valid())
        {
            $('#form_main').submit();
        // var tempPR = {};

        // tempPR['id_programme'] = <?php echo $programme->id ?>;
        // tempPR['min'] = $("#min").val();
        // tempPR['max'] = $("#max").val();
        // tempPR['id_grade'] = $("#id_grade").val();
        // tempPR['is_fail'] = $("#is_fail").val();

        //     $.ajax(
        //     {
        //        url: '/examination/markDistribution/saveMarksDistribution',
        //         type: 'POST',
        //         // type: 'POST',
        //        data:
        //        {
        //         tempData: tempPR
        //        },
        //        error: function()
        //        {
        //         alert('Something is wrong');
        //        },
        //        success: function(result)
        //        {
        //         window.location.reload();
        //        }
        //     });
        }
    }

    function deleteMarksDistribution(id)
    {
        var cnf= confirm('Do you really want to delete?');
        if(cnf==true)
        {

         $.ajax(
            {
               url: '/examination/gradeSetup/deleteMarksDistribution/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
        }
    }



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                min: {
                    required: true
                },
                max: {
                    required: true
                },
                id_grade: {
                    required: true
                },
                is_fail: {
                    required: true
                }
            },
            messages: {
                min: {
                    required: "<p class='error-text'>Min Marks Required</p>",
                },
                max: {
                    required: "<p class='error-text'>Max Marks Required</p>",
                },
                id_grade: {
                    required: "<p class='error-text'>Select Grade</p>",
                },
                is_fail: {
                    required: "<p class='error-text'>Select Pass / Fail</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>