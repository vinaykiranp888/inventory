<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Approve Mark Distribution</h3>
            <a href="../approvalList" class="btn btn-link"> < Back</a>
        </div>


        <div class="form-container">
            <h4 class="form-group-title">Mark Distribution Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_program)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Program <span class='error-text'>*</span></label>
                        <select name="id_intake" id="id_intake" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_intake)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->year . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Programme Landscape <span class='error-text'>*</span></label>
                        <select name="id_programme_landscape" id="id_programme_landscape" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeLandscapeList))
                            {
                                foreach ($programmeLandscapeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_programme_landscape)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->program_landscape_type . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>


            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                     <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course" id="id_course" class="form-control selitemIcon" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php 
                                    if($record->id == $markDistribution->id_course)
                                    {
                                        echo "selected=selected";
                                    } ?>
                                >
                                <?php echo $record->code . " - " . $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            <!-- </div>


        


            <div class="row"> -->


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="test" class="form-control" id="status" name="status" value="<?php 
                        if($markDistribution->status=='0')
                        {
                             echo "Pending";
                          }
                          elseif($markDistribution->status=='1'){
                            echo "Approved";
                          }elseif($markDistribution->status=='2'){
                            echo "Rejected";
                          };?>

                        " readonly >
                    </div>
                </div>

            <?php
        if($markDistribution->status == '2')
        {
            ?>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reject Reason <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $markDistribution->reason ?>" readonly >
                    </div>
                </div>

        <?php
        }
        ?>

            

            </div>

        </div>


         
            <form id="form_applicant" action="" method="post">

                
            <?php
                if($markDistribution->status=='0')
                {
                ?>



            <div class="form-container">
                <h4 class="form-group-title">Approval Details</h4>

                <div class="row">

                                        
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Approval <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                    <input type="radio" id="sd1" name="status" value="1" ><span class="check-radio" onclick="hideRejectField()"></span> Approve
                                </label>
                                <label class="radio-inline">

                                    <input type="radio" id="sd2" name="status" value="2"><span class="check-radio" onclick="showRejectField()"></span> Reject
                                </label>
                                </div>
                            </div>

                             <div class="col-sm-4" id="view_reject" style="display: none">
                                <div class="form-group">
                                    <label>Reason <span class='error-text'>*</span></label>
                                    <input type="text" id="reason" name="reason" class="form-control">
                                </div>
                            </div>

                </div>

            </div>
            

                    <?php
                        }
                    ?>
            



            <div class="button-block clearfix">
                <div class="bttn-group">

            <?php
                if($markDistribution->status=='0')
                {
                ?>

                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>

                <?php
                }
            ?>
                    <!-- <a href="../approvalList" class="btn btn-link">Back</a> -->
                </div>
            </div>

    </form>




     <?php

                    if(!empty($markDistributionDetails))
                    {
                        ?>
                        <br>

                        <div class="form-container">
                                <h4 class="form-group-title">Course Details</h4>
                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                        <th>Exam Components</th>
                                        <th>Pass Compulsary</th>
                                        <th>Pass Marks</th>
                                        <th>Max. Marks</th>
                                        <th>Attendance Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($markDistributionDetails);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $markDistributionDetails[$i]->component_code . " - " . $markDistributionDetails[$i]->component_name;?></td>
                                        <td><?php if($markDistributionDetails[$i]->is_pass_compulsary == 1)
                                        {
                                            echo 'Yes';
                                        }else
                                        {
                                            echo 'No';
                                        }
                                        ?></td>
                                        <td><?php echo $markDistributionDetails[$i]->pass_marks; ?></td>
                                        <td><?php echo $markDistributionDetails[$i]->max_marks; ?></td>
                                        <td><?php if($markDistributionDetails[$i]->attendance_status == 1)
                                        {
                                            echo 'Yes';
                                        }else
                                        {
                                            echo 'No';
                                        }
                                        ?></td>
                                        

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>

                    <?php
                    
                    }
                     ?>



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    
    $('select').select2();


     function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }


    $(document).ready(function()
    {
        $("#form_applicant").validate({
            rules: {
                status: {
                    required: true
                },
                reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $('select').select2();

</script>