<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GpaCgpaSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('gpa_cgpa_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('gpa_cgpa.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['setup_by'] = $this->security->xss_clean($this->input->post('setup_by'));
            $data['searchParam'] = $formData;

            
            $data['semesterList'] = $this->gpa_cgpa_model->semesterListByStatus('1');
            $data['programList'] = $this->gpa_cgpa_model->programListByStatus('1');
            $data['awardList'] = $this->gpa_cgpa_model->awardListByStatus('1');
            $data['intakeList'] = $this->gpa_cgpa_model->intakeListByStatus('1');

            $data['gpaCgpaList'] = $this->gpa_cgpa_model->gpaCgpaListSearch($formData);
// awardListByStatus
            
            // echo "<Pre>"; print_r($data['gpaCgpaList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : GPA / CGPA Setup';
            $this->loadViews("gpa_cgpa/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('gpa_cgpa.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $setup_by = $this->security->xss_clean($this->input->post('setup_by'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $academic_status = $this->security->xss_clean($this->input->post('academic_status'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'setup_by' => $setup_by,
                    'id_semester' => $id_semester,
                    'id_award' => $id_award,
                    'id_program' => $id_program,
                    'academic_status' => $academic_status,
                    'id_intake' => $id_intake,
                    'status' => $status,
                    'created_by' => $user_id,
                );
            
                $result = $this->gpa_cgpa_model->addNewGpaCgpaSetup($data);
                if($result)
                {
                    $result = $this->gpa_cgpa_model->moveDetailDataFromTempToMain($result);
                }
                redirect('/examination/gpaCgpaSetup/list');
            }
            else
            {
                $added = $this->gpa_cgpa_model->deleteTempDetailBySessionId($id_session);

            }

            
            $data['semesterList'] = $this->gpa_cgpa_model->semesterListByStatus('1');
            $data['programList'] = $this->gpa_cgpa_model->programListByStatus('1');
            $data['awardList'] = $this->gpa_cgpa_model->awardListByStatus('1');
            $data['intakeList'] = $this->gpa_cgpa_model->intakeListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Add GPA / CGPA Setup';
            $this->loadViews("gpa_cgpa/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('gpa_cgpa.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/gpaCgpaSetup/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $setup_by = $this->security->xss_clean($this->input->post('setup_by'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $academic_status = $this->security->xss_clean($this->input->post('academic_status'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'setup_by' => $setup_by,
                    'id_semester' => $id_semester,
                    'id_award' => $id_award,
                    'id_program' => $id_program,
                    'academic_status' => $academic_status,
                    'id_intake' => $id_intake,
                    'status' => $status,
                    'updated_by' => $user_id,
                );
                
                $result = $this->gpa_cgpa_model->editGpaCgpaSetupDetails($data,$id);
                redirect('/examination/gpaCgpaSetup/list');
            }
            $data['semesterList'] = $this->gpa_cgpa_model->semesterListByStatus('1');
            $data['programList'] = $this->gpa_cgpa_model->programListByStatus('1');
            $data['awardList'] = $this->gpa_cgpa_model->awardListByStatus('1');
            $data['intakeList'] = $this->gpa_cgpa_model->intakeListByStatus('1');
            $data['gpaCgpa'] = $this->gpa_cgpa_model->getGpaCgpaSetup($id);
            $data['gpaCgpaDetails'] = $this->gpa_cgpa_model->getGpaCgpaSetupDetails($id);


            $this->global['pageTitle'] = 'Inventory Management : Edit GPA / CGPA Setup';
            $this->loadViews("gpa_cgpa/edit", $this->global, $data, NULL);
        }
    }

    function saveTempDetailData()
    {
        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->gpa_cgpa_model->saveTempDetailData($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->gpa_cgpa_model->getTempGPACGPADetailsBySessionId($id_session);

        // echo "<Pre>";print_r($temp_details);exit;

        $table = "
        <h5> Academic Setup Details</h5>
        ";

        if(!empty($temp_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table .= "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Min. Grade Point</th>
                    <th>Max. Grade Point</th>
                    <th>Status Description</th>
                    <th>Description Optional Language</th>
                    <th>Probation</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $min_grade_point = $temp_details[$i]->min_grade_point;
                        $max_grade_point = $temp_details[$i]->max_grade_point;
                        $description = $temp_details[$i]->description;
                        $status_optional_language = $temp_details[$i]->status_optional_language;
                        $probation = $temp_details[$i]->probation;

                        if($probation == 1)
                        {
                            $probation = "Yes";
                        }
                        else
                        {
                            $probation = "No";
                        }

                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$min_grade_point</td>
                            <td>$max_grade_point</td>
                            <td>$description</td>
                            <td>$status_optional_language</td>
                            <td>$probation</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table .= "
            <h6 class='text-center'>No Date Available</h6>
            <br>
            ";

        }


        return $table;
    }

    function deleteTempData($id)
    {
        $id_session = $this->session->my_session_id;

        $deleted = $this->gpa_cgpa_model->deleteTempDetailData($id);

        $data = $this->displayTempData();

        print_r($data);exit();
    }


    function saveDetailData()
    {        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->gpa_cgpa_model->saveGPACGPADetails($tempData);

        // $data = $this->displayTempData();

        echo "success";exit();
    }

    function deleteDetailData($id)
    {
        $deleted = $this->gpa_cgpa_model->deleteDetailData($id);
        echo 'success';exit;
    }
}
