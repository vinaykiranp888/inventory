<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GradeSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grade_setup_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('grade_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['based_on'] = $this->security->xss_clean($this->input->post('based_on'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['status'] = '';
 
            $data['searchParam'] = $formData;


            $data['semesterList'] = $this->grade_setup_model->semesterListByStatus('1');
            $data['intakeList'] = $this->grade_setup_model->intakeListByStatus('1');
            $data['gradeSetupList'] = $this->grade_setup_model->gradeSetupListSearch($formData);

            // print_r($data);exit;

            $this->global['pageTitle'] = 'Inventory Management : Grade Setup List';
            $this->loadViews("grade_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grade_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
                $based_on = $this->security->xss_clean($this->input->post('based_on'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'based_on' => $based_on,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'id_course' => $id_course,
                    'id_award' => $id_award,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $inserted_id = $this->grade_setup_model->addNewGradeSetup($data);
                $temp_details = $this->grade_setup_model->moveTempGradeSetupDetailsToMainDetails($inserted_id);
                redirect('/examination/gradeSetup/list');
            }
            else
            {
                $this->grade_setup_model->deleteTempGradeDetailsBySession($id_session);
            }

            $data['semesterList'] = $this->grade_setup_model->semesterListByStatus('1');
            $data['intakeList'] = $this->grade_setup_model->intakeListByStatus('1');
            $data['courseList'] = $this->grade_setup_model->courseListByStatus('1');
            $data['awardList'] = $this->grade_setup_model->awardListByStatus('1');
            $data['awardList'] = $this->grade_setup_model->awardListByStatus('1');
            $data['programList'] = $this->grade_setup_model->programListByStatus('1');
            $data['gradeList'] = $this->grade_setup_model->gradeListByStatus('1');

            

            // echo "<Pre>";print_r($data['partnerCategoryList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Programme';
            $this->loadViews("grade_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grade_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/grade_setup/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $based_on = $this->security->xss_clean($this->input->post('based_on'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_award = $this->security->xss_clean($this->input->post('id_award'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'based_on' => $based_on,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'id_course' => $id_course,
                    'id_award' => $id_award,
                    'status' => $status,
                    'created_by' => $id_user
                );

                // echo "<Pre>";print_r($data);exit;
                $result = $this->grade_setup_model->editGradeSetup($data,$id);
                redirect('/examination/gradeSetup/list');
            }
            $data['id_grade_setup'] = $id;
            $data['gradeSetup'] = $this->grade_setup_model->getGradeSetup($id);
            $data['getGradeSetupDetails'] = $this->grade_setup_model->getGradeSetupDetails($id);
            $data['semesterList'] = $this->grade_setup_model->semesterListByStatus('1');
            $data['intakeList'] = $this->grade_setup_model->intakeListByStatus('1');
            $data['courseList'] = $this->grade_setup_model->courseListByStatus('1');
            $data['awardList'] = $this->grade_setup_model->awardListByStatus('1');
            $data['awardList'] = $this->grade_setup_model->awardListByStatus('1');
            $data['programList'] = $this->grade_setup_model->programListByStatus('1');
            $data['gradeList'] = $this->grade_setup_model->gradeListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Edit Grade Setup';
            $this->loadViews("grade_setup/edit", $this->global, $data, NULL);
        }
    }


    function editdetails($idgrade = NULL,$idgradedetails = NULL)
    {
        if ($this->checkAccess('grade_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             $data['semesterList'] = $this->grade_setup_model->semesterListByStatus('1');
            $data['intakeList'] = $this->grade_setup_model->intakeListByStatus('1');
            $data['courseList'] = $this->grade_setup_model->courseListByStatus('1');
            $data['awardList'] = $this->grade_setup_model->awardListByStatus('1');
            $data['awardList'] = $this->grade_setup_model->awardListByStatus('1');
            $data['programList'] = $this->grade_setup_model->programListByStatus('1');
            $data['gradeList'] = $this->grade_setup_model->gradeListByStatus('1');


            $data['gradeSetup'] = $this->grade_setup_model->getGradeSetup($idgrade);

            $data['getGradeSetupDetailsInd'] = $this->grade_setup_model->getGradeSetupDetailsById($idgradedetails);

                       $data['getGradeSetupDetails'] = $this->grade_setup_model->getGradeSetupDetails($idgrade);


            $this->global['pageTitle'] = 'Inventory Management : Edit Grade Setup';
            $this->loadViews("grade_setup/editdetails", $this->global, $data, NULL);
        }
    }


     function addTempGradeDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $id_session = $this->session->my_session_id;
        $tempData['id_session'] = $id_session;
        
        $inserted_id = $this->grade_setup_model->addTempGradeDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;
        
        $data = $this->displayTempGradeDetails();
        echo $data;        
    }

    function displayTempGradeDetails()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->grade_setup_model->getTempGradeDetailsBySession($id_session); 

        // echo "<Pre>";print_r($temp_details);exit;
        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Grade</th>
                    <th>Point</th>
                    <th>Min. Marks</th>
                    <th>Max. Marks</th>
                    <th>Description</th>
                    <th>Description Optional Language</th>
                    <th>Pass</th>
                    <th>Rank</th>
                    <th class='text-center'>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {

                    $id = $temp_details[$i]->id;
                    $grade = $temp_details[$i]->grade_name . " - " . $temp_details[$i]->grade_description;
                    $point = $temp_details[$i]->point;
                    $min_marks = $temp_details[$i]->min_marks;
                    $max_marks = $temp_details[$i]->max_marks;
                    $description = $temp_details[$i]->description;
                    $description_optional_language = $temp_details[$i]->description_optional_language;
                    $pass = $temp_details[$i]->pass;
                    $rank = $temp_details[$i]->rank;

                    $j = $i+1;

                        $table .= "
                         <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$grade</td>
                            <td>$point</td>                      
                            <td>$min_marks</td>                      
                            <td>$max_marks</td>                      
                            <td>$description</td>                      
                            <td>$description_optional_language</td>                      
                            <td>$pass</td>                      
                            <td>$rank</td>                      
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempGradeDetails($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
        $table.= "
         </tbody>
         </table>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempGradeDetails($id)
    {
        $inserted_id = $this->grade_setup_model->deleteTempGradeDetails($id);
        $data = $this->displayTempGradeDetails();
        echo $data; 
    }

    function directadd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

         if($tempData['id']) {
        $inserted_id = $this->grade_setup_model->upGradeSetupDetails($tempData,$tempData['id']);

         } else {
        $inserted_id = $this->grade_setup_model->addNewGradeSetupDetails($tempData);

         }



        if($inserted_id)
        {
            echo "success";exit;
        }
    }

    function deleteGradeDetails($id)
    {
        $inserted_id = $this->grade_setup_model->deleteGradeDetails($id);
        echo "success";exit;
    }
}
