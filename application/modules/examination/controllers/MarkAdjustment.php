<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MarkAdjustment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mark_adjustment_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('mark_adjustment.student_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));    
            $data['searchParam'] = $formData;
            $data['applicantList'] = array();

            if($this->input->post())
            {
                $postData = $this->input->post();

                $btn_submit = $postData['btn_submit'];

                if($btn_submit == 'search')
                {
                // echo "<Pre>";print_r($this->input->post());exit;
        
                
                $data['applicantList'] = $this->mark_adjustment_model->getStudentListForCourseRegisteredStudent($formData);
                }
                elseif($btn_submit == 'marks')
                {
                    // echo "<Pre>";print_r($postData);exit;

                    // $id_student_has_programmes = $postData['id_student_has_programme'];

                    for($i=0;$i<count($postData['new_marks_obtained']);$i++)
                    {
                        $marks = $postData['new_marks_obtained'][$i];
                        $id_marks_entry = $postData['id_marks_entry'][$i];

                        if($marks > 0)
                        {
                            $student_marks_entry = $this->mark_adjustment_model->getStudentMarksEntry($id_marks_entry);

                            $id_student_has_programme = $student_marks_entry->id_student_has_programme;
                            $id_student = $student_marks_entry->id_student;
                            $id_programme = $student_marks_entry->id_programme;
                            $old_marks = $student_marks_entry->marks_obtained;
                            
                            // echo "<Pre>";print_r($student_marks_entry);exit;


                            $marks_adjustment_data = array(
                                'id_student_has_programme' => $id_student_has_programme,
                                'id_student' => $id_student,
                                'id_programme' => $id_programme,
                                'old_marks' => $old_marks,
                                'marks_obtained' => $marks,
                                'status' => 0,
                                'created_by' => $user_id
                            );

                            // echo "<Pre>";print_r($marks_adjustment_data);exit;


                            $id_mark_adjustment = $this->mark_adjustment_model->addStudentMarksAdjustment($marks_adjustment_data);

                            if($id_mark_adjustment)
                            {
                                $update_student_marks_entry = array(
                                    'id_student_marks_adjustment' => $id_mark_adjustment
                                );

                                $updated_student_has_programme = $this->mark_adjustment_model->updateStudentMarksEntry($update_student_marks_entry,$id_marks_entry);
                            }

                        }
                    }
                    
                    redirect('/examination/markAdjustment/list');
                }

            }

            $data['programList'] = $this->mark_adjustment_model->programListByStatus('1');


            // echo "<Pre>";print_r($data['applicantList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Receipt List';
            $this->loadViews("mark_adjustment/list", $this->global, $data, NULL);
        }
    }
}