<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PublishExamResult extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('publish_exam_result_model');
        $this->load->model('setup/semester_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('publish_exam_result.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));

            $data['searchParam'] = $formData;

            $data['programmeList'] = $this->publish_exam_result_model->getProgrammeListByStatus('1');
            $data['intakeList'] = $this->publish_exam_result_model->getIntakeListByStatus('1');
            $data['semesterList'] = $this->publish_exam_result_model->getSemesterListByStatus('1');


            $data['examResultList'] = $this->publish_exam_result_model->examResultListSearch($formData);
            $this->global['pageTitle'] = 'Inventory Management : Course Grade';
            //print_r($subjectDetails);exit;
            $this->loadViews("publish_exam_result/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('publish_exam_result.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $exam_date = $this->security->xss_clean($this->input->post('exam_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'exam_date' => date('Y-m-d',strtotime($exam_date)),
                    'status' => $status
                );
            
                $result = $this->publish_exam_result_model->addNewExamResult($data);
                redirect('/examination/publishExamResult/list');
            }

            $data['intakeList'] = $this->publish_exam_result_model->intakeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['programList'] = $this->publish_exam_result_model->programList();
            $this->global['pageTitle'] = 'Inventory Management : Add Exam Result';
            $this->loadViews("publish_exam_result/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('publish_exam_result.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/publishExamResult/list');
            }
            if($this->input->post())
            {
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $exam_date = $this->security->xss_clean($this->input->post('exam_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_program' => $id_program,
                    'exam_date' => date('Y-m-d',strtotime($exam_date)),
                    'status' => $status
                );

                $result = $this->publish_exam_result_model->editExamResultDetails($data,$id);
                redirect('/examination/publishExamResult/list');
            }
            $data['programList'] = $this->publish_exam_result_model->programList();
            $data['intakeList'] = $this->publish_exam_result_model->intakeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['examResultList'] = $this->publish_exam_result_model->getExamResultDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Exam Result';
            $this->loadViews("publish_exam_result/edit", $this->global, $data, NULL);
        }
    }
}
