<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class IpsProgress extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ips_progress_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('ips_progress.student_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->ips_progress_model->intakeListByStatus('1');
            $data['programList'] = $this->ips_progress_model->postGraduateprogramListByStatus('1');
            $data['studentDeliverablesList'] = $this->ips_progress_model->studentDeliverablesList();


            $formData['first_name'] = $this->security->xss_clean($this->input->post('first_name'));
            $formData['last_name'] = $this->security->xss_clean($this->input->post('last_name'));
            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['current_deliverable'] = $this->security->xss_clean($this->input->post('current_deliverable'));
 
            $data['searchParam'] = $formData;
            $data['studentList'] = $this->ips_progress_model->studentListSearch($formData);


            // echo "<Pre>";print_r($data['studentDeliverablesList']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Student List IPS Progress Entry';
            $this->loadViews("ips_progress/list", $this->global, $data, NULL);
        }
    }
    
    function edit($id = NULL)
    {
        if ($this->checkAccess('ips_progress.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/ips_progress/list');
            }
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $phd_duration = $this->security->xss_clean($this->input->post('phd_duration'));
                $current_deliverable = $this->security->xss_clean($this->input->post('current_deliverable'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'id_student' => $id,
                    'phd_duration' => $phd_duration,
                    'current_deliverable' => $current_deliverable,
                    'education_level' => 'POSTGRADUATE',
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->ips_progress_model->addPostgraduateStudentProgress($data,$id);

                redirect('/examination/ipsProgress/list');
            }

            $data['organisationDetails'] = $this->ips_progress_model->getOrganisation();
            $data['student'] = $this->ips_progress_model->getStudentDetails($id);
            $data['getStudentData'] = $this->ips_progress_model->getStudentByStudentId($id);
            $data['getPostgraduateStudentProgressByIdStudent'] = $this->ips_progress_model->getPostgraduateStudentProgressByIdStudent($id);
        
            // echo "<Pre>";print_r($data['getPostgraduateStudentProgressByIdStudent']);exit;
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Grade';
            $this->loadViews("ips_progress/edit", $this->global, $data, NULL);
        }
    }
}
