<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamCalender extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_calender_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_calender.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['activity'] = $this->security->xss_clean($this->input->post('activity'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));

            $data['searchParam'] = $formData;

            $data['examCalenderList'] = $this->exam_calender_model->examCalenderListSearch($formData);
            $data['semesterList'] = $this->exam_calender_model->semesterListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Exam Centers';
            $this->loadViews("exam_calender/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_calender.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $activity = $this->security->xss_clean($this->input->post('activity'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                
                $data = array(
                   'id_semester' => $id_semester,
                    'activity' => $activity,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 1,
                    'created_by' => $user_id
                );

                $result = $this->exam_calender_model->addExamCalender($data);
                redirect('/examination/examCalender/list');
            }

            $data['semesterList'] = $this->exam_calender_model->semesterListByStatus('1');
            // $data['activityList'] = $this->exam_calender_model->activityListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Exam Center';
            $this->loadViews("exam_calender/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_calender.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/examCalender/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $activity = $this->security->xss_clean($this->input->post('activity'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                
                $data = array(
                   'id_semester' => $id_semester,
                    'activity' => $activity,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 1,
                    'updated_by' => $user_id
                );

                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->exam_calender_model->editExamCalender($data,$id);
                redirect('/examination/examCalender/list');
            }
            $data['examCalender'] = $this->exam_calender_model->getExamCalender($id);
            $data['semesterList'] = $this->exam_calender_model->semesterListByStatus('1');
            // $data['activityList'] = $this->exam_calender_model->activityListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Exam Center';
            $this->loadViews("exam_calender/edit", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
            $results = $this->exam_calender_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
