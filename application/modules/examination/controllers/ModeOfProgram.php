<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ModeOfProgram extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mode_of_program_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('mode_of_program.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programModeList'] = $this->mode_of_program_model->programModeList();

            $this->global['pageTitle'] = 'Inventory Management : Mode of Program';
            //print_r($subjectDetails);exit;
            $this->loadViews("mode_of_program/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('mode_of_program.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));

            
                $data = array(
                    'name' => $name

                );
            
                $result = $this->mode_of_program_model->addNewProgramMode($data);
                redirect('/examination/modeOfProgram/list');
            }

            $this->global['pageTitle'] = 'Inventory Management : Add Mode of Program';
            $this->loadViews("mode_of_program/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('mode_of_program.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/modeOfProgram/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));

                $data = array(
                    'name' => $name
                );
                $result = $this->mode_of_program_model->editProgramModeDetails($data,$id);
                redirect('/examination/modeOfProgram/list');
            }
            $data['programModeDetails'] = $this->mode_of_program_model->getProgramModeDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Mode of Program';
            $this->loadViews("mode_of_program/edit", $this->global, $data, NULL);
        }
    }
}
