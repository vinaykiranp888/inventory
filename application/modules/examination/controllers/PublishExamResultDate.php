<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PublishExamResultDate extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('publish_exam_result_date_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('publish_exam_result_date.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));

            $data['searchParam'] = $formData;

            $data['publishExamResultDateList'] = $this->publish_exam_result_date_model->publishExamResultDateListSearch($formData);

            $data['semesterList'] = $this->publish_exam_result_date_model->semesterListByStatus('1');
            $data['programList'] = $this->publish_exam_result_date_model->programListByStatus('1');
            $data['courseList'] = $this->publish_exam_result_date_model->courseListByStatus('1');
            $data['intakeList'] = $this->publish_exam_result_date_model->intakeListByStatus('1');

            $this->global['pageTitle'] = 'Inventory Management : Publish Exam Result Date List';
            $this->loadViews("publish_exam_result_date/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('publish_exam_result_date.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                





                $data = array(
                   'id_program' => $id_program,
                    'id_intake' => $id_intake,
                    'id_semester' => $id_semester,
                    'id_course' => $id_course,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'status' => 1,
                    'created_by' => $user_id
                );


                $result = $this->publish_exam_result_date_model->addPublishExamResultDate($data);
                redirect('/examination/publishExamResultDate/list');
            }

            $data['semesterList'] = $this->publish_exam_result_date_model->semesterListByStatus('1');
            $data['programList'] = $this->publish_exam_result_date_model->programListByStatus('1');
            $data['courseList'] = $this->publish_exam_result_date_model->courseListByStatus('1');
            $data['intakeList'] = $this->publish_exam_result_date_model->intakeListByStatus('1');
            // $data['activityList'] = $this->publish_exam_result_date_model->activityListByStatus('1');


            $this->global['pageTitle'] = 'Inventory Management : Add Publish Exam Result Date';
            $this->loadViews("publish_exam_result_date/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('publish_exam_result_date.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/publishExamResultDate/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                redirect('/examination/publishExamResultDate/list');
            }

            $data['publishExamResultDate'] = $this->publish_exam_result_date_model->getPublishExamResultDate($id);
            $data['course'] = $this->publish_exam_result_date_model->getCourse($data['publishExamResultDate']->id_course);
            $data['semesterList'] = $this->publish_exam_result_date_model->semesterListByStatus('1');
            $data['programList'] = $this->publish_exam_result_date_model->programListByStatus('1');
            $data['courseList'] = $this->publish_exam_result_date_model->courseListByStatus('1');
            $data['intakeList'] = $this->publish_exam_result_date_model->intakeListByStatus('1');

            // $data['activityList'] = $this->publish_exam_result_date_model->activityListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Edit Publish Exam Result Date';
            $this->loadViews("publish_exam_result_date/edit", $this->global, $data, NULL);
        }
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->publish_exam_result_date_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getCoursesByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }

    function getCoursesByProgramIdNIntakeId()
    {
        
        $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
        $id_programme = $this->security->xss_clean($this->input->post('id_program'));
        $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
         // echo "<Pre>"; print_r($id_semester);exit();

        $temp_details = $this->publish_exam_result_date_model->getProgramLandscapeCourses($id_intake,$id_programme,$id_semester);

        // echo "<Pre>"; print_r($temp_details);exit();
        
        $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_course' id='id_course' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($temp_details);$i++)
            {

            // Id Landscape Course Id Taken As id_course_registered
            // $id = $results[$i]->id_procurement_category;
            $id = $temp_details[$i]->id;
            $id_course_registered = $temp_details[$i]->id_course_registered;
            $name = $temp_details[$i]->name;
            $code = $temp_details[$i]->code;

            $table.="<option value=".$id.">".$code . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $staffList = $this->publish_exam_result_date_model->staffListByStatus('1');
        
        $student_data = $this->publish_exam_result_date_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {


         $table = "

         <script type='text/javascript'>
             $('select').select2();
         </script>

         <h4>Advisor Tagging For Students</h4>


         <div class='row'>
            <div class='col-sm-4'>
                <div class='form-group'>
                <label>Select Advisor </label>
                <select name='id_advisor_for_tagging' id='id_advisor_for_tagging' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($staffList);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $staffList[$i]->id;
            $ic_no = $staffList[$i]->ic_no;
            $name = $staffList[$i]->name;
            $table.="<option value=".$id.">".$ic_no . " - " . $name .
                    "</option>";

            }
            $table .="
                </select>
                </div>
              </div>

            </div>
            ";


         $table .= "
         <br>
         <h4> Select Students For Advisor Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Program</th>
                    <th>Intake</th>
                    <th>Qualification</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th style='text-align: center;'>Advisor</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $program_code = $student_data[$i]->program_code;
                $program_name = $student_data[$i]->program_name;
                $intake_year = $student_data[$i]->intake_year;
                $intake_name = $student_data[$i]->intake_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $qualification_name = $student_data[$i]->qualification_name;
                $ic_no = $student_data[$i]->ic_no;
                $advisor_name = $student_data[$i]->advisor_name;
                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>                        
                    <td>$nric</td>                           
                    <td>$program_code - $program_name</td>                           
                    <td>$intake_year - $intake_name</td>                           
                    <td>$qualification_name - $qualification_name</td>                           
                    <td>$email_id</td>                      
                    <td>$phone</td>                      
                    <td style='text-align: center;'>$ic_no - $advisor_name</td>                  
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}
