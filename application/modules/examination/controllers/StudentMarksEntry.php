<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentMarksEntry extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_marks_entry_model');
        // $this->load->model('main_invoice_model');
        $this->isLoggedIn();
    }

    function studentList()
    {
        if ($this->checkAccess('student_marks_entry.student_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $user_id = $this->session->userId;

            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));    
            $data['searchParam'] = $formData;

            if($this->input->post())
            {
                $postData = $this->input->post();

                $btn_submit = $postData['btn_submit'];

                if($btn_submit == 'search')
                {
                // echo "<Pre>";print_r($this->input->post());exit;
        
                
                $data['applicantList'] = $this->student_marks_entry_model->getStudentListForCourseRegisteredStudent($formData);
                }
                elseif($btn_submit == 'marks')
                {
                    // echo "<Pre>";print_r($postData);exit;

                    // $id_student_has_programmes = $postData['id_student_has_programme'];

                    for($i=0;$i<count($postData['marks_obtained']);$i++)
                    {
                        $marks = $postData['marks_obtained'][$i];
                        $id_student_has_programme = $postData['id_student_has_programme'][$i];

                        if($marks > 0)
                        {
                            $student_has_programme = $this->student_marks_entry_model->getStudentHasProgramme($id_student_has_programme);
                            
                            // echo "<Pre>";print_r($student_has_programme);exit;


                            $marks_entry_data = array(
                                'id_student_has_programme' => $id_student_has_programme,
                                'id_student' => $student_has_programme->id_student,
                                'id_programme' => $student_has_programme->id_programme,
                                'marks_obtained' => $marks,
                                'status' => 0,
                                'created_by' => $user_id
                            );

                            // echo "<Pre>";print_r($marks_entry_data);exit;


                            $id_student_marks_entry = $this->student_marks_entry_model->addStudentMarksEntry($marks_entry_data);

                            if($id_student_marks_entry)
                            {
                                $update_student_has_programme = array(
                                    'id_marks_entry' => $id_student_marks_entry
                                );

                                $updated_student_has_programme = $this->student_marks_entry_model->updateStudentHasProgramme($update_student_has_programme,$id_student_has_programme);
                            }

                        }
                    }
                    
                    redirect('/examination/studentMarksEntry/studentList');
                }

            }

            $data['programList'] = $this->student_marks_entry_model->programListByStatus('1');
            

            // $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));        
            // $data['searchParam'] = $formData;
            // $data['applicantList'] = $this->student_marks_entry_model->getStudentListForCourseRegisteredStudent($formData);


            // echo "<Pre>";print_r($data['applicantList']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Receipt List';
            $this->loadViews("student_marks_entry/student_list", $this->global, $data, NULL);
        }
    }
}