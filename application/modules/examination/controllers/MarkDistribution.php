<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class MarkDistribution extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mark_distribution_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category_type'));
            $formData['id_category_setup'] = $this->security->xss_clean($this->input->post('id_category_setup'));
            $formData['id_programme_type'] = $this->security->xss_clean($this->input->post('id_programme_type'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));

            $data['searchParam'] = $formData;

            // $data['programmeList'] = $this->mark_distribution_model->programmeList();
            // $data['categoryTypeList'] = $this->mark_distribution_model->categoryTypeListByStatus('1');
            $data['categoryList'] = $this->mark_distribution_model->categoryListByStatus('1');
            $data['productTypeSetupList'] = $this->mark_distribution_model->productTypeSetupListByStatus('1');

            $data['programmeList'] = $this->mark_distribution_model->programmeListSearch($formData);

                // echo "<Pre>";print_r($data['programmeList']);exit();


            $this->global['pageTitle'] = 'Inventory Management : Program List';
            $this->loadViews("mark_distribution/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_programme,$id_marks_distribution=NULL)
    {
        if ($this->checkAccess('mark_distribution.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>";print_r($_POST);exit();

                $id_exam_component = $this->security->xss_clean($this->input->post('id_exam_component'));
                $max = $this->security->xss_clean($this->input->post('max'));
                $pass_marks = $this->security->xss_clean($this->input->post('pass_marks'));
                $is_pass = $this->security->xss_clean($this->input->post('is_pass'));
                $is_attendance = $this->security->xss_clean($this->input->post('is_attendance'));

                
                $data = array(
                   'id_programme' => $id_programme,
                   'id_exam_component' => $id_exam_component,
                   'max' => $max,
                   'pass_marks' => $pass_marks,
                   'is_attendance' => $is_attendance,
                   'is_pass' => $is_pass,
                   'status' => 1
                );

                // echo "<Pre>";print_r($id_marks_distribution);exit();
                if($id_marks_distribution > 0)
                {
                    $data['updated_by'] = $user_id;
                    $data['updated_dt_tm'] = date('Y-m-d H:i:s');
                    $result = $this->mark_distribution_model->editMarkDistribution($data,$id_marks_distribution);
                }
                else
                {
                    $data['created_by'] = $user_id;
                    $result = $this->mark_distribution_model->addMarkDistribution($data);
                }
                redirect('/examination/markDistribution/add/'.$id_programme);
            }

            $data['id_programme'] = $id_programme;
            $data['id_marks_distribution'] = $id_marks_distribution;
            $data['programme'] = $this->mark_distribution_model->getProgrammeById($id_programme);
            $data['markDistribution'] = $this->mark_distribution_model->getMarkDistribution($id_marks_distribution);
            $data['markDistributionByProgramme'] = $this->mark_distribution_model->getMarkDistributionByProgramme($id_programme);
            $data['examComponentsList'] = $this->mark_distribution_model->examComponentsListByStatus('1');



                // echo "<Pre>";print_r($data['programme']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Mark Distribution';
            $this->loadViews("mark_distribution/add", $this->global, $data, NULL);
        }
    }

    function deleteMarksDistribution($id)
    {
        $deleted = $this->mark_distribution_model->deleteMarksDistribution($id);
        echo $deleted;exit();
    }



    function edit($id = NULL)
    {
        if ($this->checkAccess('mark_distribution.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/markDistribution/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $to_tm_12hr  = date("g:i a", strtotime($to_tm));
                $from_tm_12hr  = date("g:i a", strtotime($from_tm));
                // echo "<Pre>";print_r($time_in_12_hour_format);exit();
                
                $data = array(
                   'name' => $name,
                    'from_dt' => date('Y-m-d', strtotime($from_dt)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'status' => $status,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->mark_distribution_model->editMarkDistribution($data,$id);
                redirect('/examination/markDistribution/list');
            }

            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['componentList'] = $this->mark_distribution_model->examComponentListByStatus('1');
            $data['programmeLandscapeList'] = $this->mark_distribution_model->programmeLandscapeListByStatus('1');

            $data['markDistribution'] = $this->mark_distribution_model->getMarkDistribution($id);
            $data['markDistributionDetails'] = $this->mark_distribution_model->getMarkDistributionDetailsByIdMarkDistribution($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Mark Distribution';
            $this->loadViews("mark_distribution/edit", $this->global, $data, NULL);
        }
    }

    function approvalList()
    {
        if ($this->checkAccess('mark_distribution.approval_list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['status'] = '0';

            $data['searchParam'] = $formData;

            $data['markDistributionList'] = $this->mark_distribution_model->markDistributionListSearch($formData);
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');

                // echo "<Pre>";print_r($data['markDistributionList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Mark Distribution';
            $this->loadViews("mark_distribution/approval_list", $this->global, $data, NULL);
        }
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('mark_distribution.approve') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/markDistribution/approvallist');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $status = $this->security->xss_clean($this->input->post('status'));
                $reason = $this->security->xss_clean($this->input->post('reason'));


                
                $data = array(
                   'status' => $status,
                    'reason' => $reason,
                    'updated_by' => $user_id
                );


                // echo "<Pre>"; print_r($id);
                // exit;

                $result = $this->mark_distribution_model->editMarkDistribution($data,$id);
                redirect('/examination/markDistribution/approvalList');
            }

            $data['courseList'] = $this->mark_distribution_model->courseListByStatus('1');
            $data['programList'] = $this->mark_distribution_model->programListByStatus('1');
            $data['intakeList'] = $this->mark_distribution_model->intakeListByStatus('1');
            $data['componentList'] = $this->mark_distribution_model->examComponentListByStatus('1');
            $data['programmeLandscapeList'] = $this->mark_distribution_model->programmeLandscapeListByStatus('1');



            $data['markDistribution'] = $this->mark_distribution_model->getMarkDistribution($id);
            $data['markDistributionDetails'] = $this->mark_distribution_model->getMarkDistributionDetailsByIdMarkDistribution($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Mark Distribution';
            $this->loadViews("mark_distribution/view", $this->global, $data, NULL);
        }
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->mark_distribution_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control' onchange='getLandscapeListByProgramIdNIntakeId()'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$year . " - " . $name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }


    function saveTempDetailData()
    {
        $id_session = $this->session->my_session_id;
        
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['id_session'] = $id_session;

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->mark_distribution_model->saveTempDetailData($tempData);

        $data = $this->displayTempData();

        print_r($data);exit();
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;

        $temp_details = $this->mark_distribution_model->getTempMarkDistributionDetailsBySessionId($id_session);

        // echo "<Pre>";print_r($temp_details);exit;



        if(!empty($temp_details))
        {
            
        $table ="
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Exam Components</th>
                    <th>Pass Compulsary</th>
                    <th>Pass Marks</th>
                    <th>Max. Marks</th>
                    <th>Attendance Status</th>
                    <th class='text-center'>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                        $id = $temp_details[$i]->id;
                        $is_pass_compulsary = $temp_details[$i]->is_pass_compulsary;
                        $pass_marks = $temp_details[$i]->pass_marks;
                        $max_marks = $temp_details[$i]->max_marks;
                        $attendance_status = $temp_details[$i]->attendance_status;
                        $component_code = $temp_details[$i]->component_code;
                        $component_name = $temp_details[$i]->component_name;

                        if($is_pass_compulsary == 1)
                        {
                            $is_pass_compulsary = 'Yes';
                        }else
                        {
                            $is_pass_compulsary = 'No';
                        }

                        if($attendance_status == 1)
                        {
                            $attendance_status = 'Yes';
                        }else
                        {
                            $attendance_status = 'No';
                        }


                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$component_code - $component_name</td>
                            <td>$is_pass_compulsary</td>
                            <td>$pass_marks</td>
                            <td>$max_marks</td>
                            <td>$attendance_status</td>
                            <td class='text-center'>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table = "";

        }


        return $table;
    }

    function deleteTempData($id)
    {
        $id_session = $this->session->my_session_id;

        $deleted = $this->mark_distribution_model->deleteTempMarkDistribution($id);

        $data = $this->displayTempData();

        print_r($data);exit();
    }
}
