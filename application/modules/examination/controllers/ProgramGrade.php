<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgramGrade extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('program_grade_model');
        $this->load->model('grade_model');
        $this->load->model('setup/semester_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('program_grade.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['intakeList'] = $this->program_grade_model->intakeList();
            $data['programList'] = $this->program_grade_model->programList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['gradeList'] = $this->grade_model->gradeList();

            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            // $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            // $formData['id_semester'] = $this->security->xss_clean($this->input->post('id_semester'));
            $formData['id_grade'] = $this->security->xss_clean($this->input->post('id_grade'));
            $formData['description'] = $this->security->xss_clean($this->input->post('description'));

            $data['searchParameters'] = $formData; 

            $data['programGradeList'] = $this->program_grade_model->programGradeListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : Program Grade';
            //print_r($subjectDetails);exit;
            $this->loadViews("program_grade/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('program_grade.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            // print_r(expression)

            if($this->input->post())
            {
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_grade = $this->security->xss_clean($this->input->post('id_grade'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $minmarks = $this->security->xss_clean($this->input->post('minmarks'));
                $maxmarks = $this->security->xss_clean($this->input->post('maxmarks'));
                $result = $this->security->xss_clean($this->input->post('result'));
                $rank = $this->security->xss_clean($this->input->post('rank'));

            
                $data = array(
                    'id_intake' => $id_intake,
                    'id_program' => $id_program,
                    'id_semester' => $id_semester,
                    'id_grade' => $id_grade,
                    'description' => $description,
                    'minmarks' => $minmarks,
                    'maxmarks' => $maxmarks,
                    'result' => $result,
                    'rank' => $rank

                );
            
                $result = $this->program_grade_model->addNewProgramGrade($data);
                redirect('/examination/programGrade/list');
            }

            $data['intakeList'] = $this->program_grade_model->intakeList();
            $data['programList'] = $this->program_grade_model->programList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['gradeList'] = $this->grade_model->gradeList();
            $this->global['pageTitle'] = 'Inventory Management : Add Program Grade';
            $this->loadViews("program_grade/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('program_grade.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/programGrade/list');
            }
            if($this->input->post())
            {
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_grade = $this->security->xss_clean($this->input->post('id_grade'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $minmarks = $this->security->xss_clean($this->input->post('minmarks'));
                $maxmarks = $this->security->xss_clean($this->input->post('maxmarks'));
                $result = $this->security->xss_clean($this->input->post('result'));
                $rank = $this->security->xss_clean($this->input->post('rank'));

                $data = array(
                    'id_intake' => $id_intake,
                    'id_program' => $id_program,
                    'id_semester' => $id_semester,
                    'id_grade' => $id_grade,
                    'description' => $description,
                    'minmarks' => $minmarks,
                    'maxmarks' => $maxmarks,
                    'result' => $result,
                    'rank' => $rank
                );
                $result = $this->program_grade_model->editProgramGradeDetails($data,$id);
                redirect('/examination/programGrade/list');
            }
            $data['intakeList'] = $this->program_grade_model->intakeList();
            $data['programList'] = $this->program_grade_model->programList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['gradeList'] = $this->grade_model->gradeList();
            $data['programGradeDetails'] = $this->program_grade_model->getProgramGradeDetails($id);
            $this->global['pageTitle'] = 'Inventory Management : Edit Program Grade';
            $this->loadViews("program_grade/edit", $this->global, $data, NULL);
        }
    }


    function getIntakeByProgramme($id_programme)
    {
            $intake_data = $this->program_grade_model->getIntakeByProgrammeId($id_programme);

            // echo "<Pre>"; print_r($intake_data);exit;

            $table="
            <script type='text/javascript'>
                $('select').select2();
            </script>


            <select name='id_intake' id='id_intake' class='form-control'>";
            $table.="<option value=''>Select</option>";

            for($i=0;$i<count($intake_data);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $intake_data[$i]->id;
            $name = $intake_data[$i]->name;
            $year = $intake_data[$i]->year;

            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="</select>";

            echo $table;
    }
}
