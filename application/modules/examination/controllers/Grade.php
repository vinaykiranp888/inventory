<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Grade extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grade_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('grade_setup.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programmeList'] = $this->grade_model->programmeListByStatus('1');

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $data['searchParam'] = $formData;

            //print_r($subjectDetails);exit;

            $data['gradeList'] = $this->grade_model->gradeListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : Grade';
            $this->loadViews("grade/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grade_setup.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $min_range = $this->security->xss_clean($this->input->post('min_range'));
                $max_range = $this->security->xss_clean($this->input->post('max_range'));
                $grade_level_up = $this->security->xss_clean($this->input->post('grade_level_up'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'id_programme' => $id_programme,
                    'min_range' => $min_range,
                    'max_range' => $max_range,
                    'grade_level_up' => $grade_level_up,
                    'status' => $status
                );
            
                $result = $this->grade_model->addNewGrade($data);
                redirect('/examination/grade/list');
            }

            $data['programmeList'] = $this->grade_model->programmeListByStatus('1');
            
            $this->global['pageTitle'] = 'Inventory Management : Add Grade';
            $this->loadViews("grade/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grade_setup.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/examination/grade/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $min_range = $this->security->xss_clean($this->input->post('min_range'));
                $max_range = $this->security->xss_clean($this->input->post('max_range'));
                $grade_level_up = $this->security->xss_clean($this->input->post('grade_level_up'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'id_programme' => $id_programme,
                    'min_range' => $min_range,
                    'max_range' => $max_range,
                    'grade_level_up' => $grade_level_up,
                    'status' => $status
                );
                
                $result = $this->grade_model->editGradeDetails($data,$id);
                redirect('/examination/grade/list');
            }
            
            $data['programmeList'] = $this->grade_model->programmeListByStatus('1');
            $data['grade'] = $this->grade_model->getGradeDetails($id);

            $this->global['pageTitle'] = 'Inventory Management : Edit Grade';
            $this->loadViews("grade/edit", $this->global, $data, NULL);
        }
    }
}
