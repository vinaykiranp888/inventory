<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CollectionReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('collection_report_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('collection_report.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['from_date'] = $this->security->xss_clean($this->input->post('from_date'));
            $formData['to_date'] = $this->security->xss_clean($this->input->post('to_date'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['status'] = $this->security->xss_clean($this->input->post('type'));
            
            $data['searchParam'] = $formData;

            // $data['collection_reportList'] = $this->collection_report_model->collection_reportList();

            $data['collection_reportList'] = $this->collection_report_model->collectionReportListSearch($formData);

            $this->global['pageTitle'] = 'Inventory Management : Corporate Company List';
            $this->loadViews("collection_report/approval_list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('collection_report.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {                

                // For file validation from 36 to 44 , file size validationss
                // echo "<Pre>";print_r($this->input->post());exit;


                // echo "<Pre>"; print_r($_FILES['image']);exit;
                // if($_FILES['image'])
                // {

                //     $certificate_name = $_FILES['image']['name'];
                //     $certificate_size = $_FILES['image']['size'];
                //     $certificate_tmp =$_FILES['image']['tmp_name'];
                    
                //     // echo "<Pre>"; print_r($certificate_tmp);exit();

                //     $certificate_ext=explode('.',$certificate_name);
                //     $certificate_ext=end($certificate_ext);
                //     $certificate_ext=strtolower($certificate_ext);


                //     $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Image File');

                //     $image_file = $this->uploadFile($certificate_name,$certificate_tmp,'Image File');
                // }



                $level = $this->security->xss_clean($this->input->post('level'));
                $id_parent_collection_report = $this->security->xss_clean($this->input->post('id_parent_collection_report'));
                $registration_number = $this->security->xss_clean($this->input->post('registration_number'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
                $website = $this->security->xss_clean($this->input->post('website'));
                $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $ceo = $this->security->xss_clean($this->input->post('ceo'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $country_code = $this->security->xss_clean($this->input->post('country_code'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $corporate_membership_status = $this->security->xss_clean($this->input->post('corporate_membership_status'));
                $staff_strength = $this->security->xss_clean($this->input->post('staff_strength'));
                $debtor_code = $this->security->xss_clean($this->input->post('debtor_code'));
                $credit_term = $this->security->xss_clean($this->input->post('credit_term'));
                $control_account = $this->security->xss_clean($this->input->post('control_account'));
                $credit_term_code = $this->security->xss_clean($this->input->post('credit_term_code'));
                $primary_contact_name = $this->security->xss_clean($this->input->post('primary_contact_name'));
                $primary_contact_designation = $this->security->xss_clean($this->input->post('primary_contact_designation'));
                $primary_contact_email = $this->security->xss_clean($this->input->post('primary_contact_email'));
                $primary_country_code = $this->security->xss_clean($this->input->post('primary_country_code'));
                $primary_phone = $this->security->xss_clean($this->input->post('primary_phone'));
                $second_contact_name = $this->security->xss_clean($this->input->post('second_contact_name'));
                $second_contact_designation = $this->security->xss_clean($this->input->post('second_contact_designation'));
                $second_contact_email = $this->security->xss_clean($this->input->post('second_contact_email'));
                $second_country_code = $this->security->xss_clean($this->input->post('second_country_code'));
                $second_phone = $this->security->xss_clean($this->input->post('second_phone'));
                $staff_credit = $this->security->xss_clean($this->input->post('staff_credit'));
                $status = $this->security->xss_clean($this->input->post('status'));



            
                $data = array(
                    'level' => $level,
                    'id_parent_collection_report' => $id_parent_collection_report,
                    'registration_number' => $registration_number,
                    'name' => $name,
                    'password' => md5($password),
                    'joined_date' => date('Y-m-d', strtotime($joined_date)),
                    'website' => $website,
                    'institution_type' => $institution_type,
                    'chairman' => $chairman,
                    'ceo' => $ceo,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'country_code' => $country_code,
                    'phone' => $phone,
                    'corporate_membership_status' => $corporate_membership_status,
                    'staff_strength' => $staff_strength,
                    'debtor_code' => $debtor_code,
                    'credit_term' => $credit_term,
                    'control_account' => $control_account,
                    'credit_term_code' => $credit_term_code,
                    'primary_contact_name' => $primary_contact_name,
                    'primary_contact_designation' => $primary_contact_designation,
                    'primary_contact_email' => $primary_contact_email,
                    'primary_country_code' => $primary_country_code,
                    'primary_phone' => $primary_phone,
                    'second_contact_name' => $second_contact_name,
                    'second_contact_designation' => $second_contact_designation,
                    'second_contact_email' => $second_contact_email,
                    'second_country_code' => $second_country_code,
                    'second_phone' => $second_phone,
                    'staff_credit' => $staff_credit,
                    'status' => $status
                );

                // echo "<Pre>"; print_r($image_file);exit;

                // if($image_file)
                // {
                //     $data['image'] = $image_file;
                // }

                $inserted_id = $this->collection_report_model->addNewProgrammeDetails($data);
                redirect('/corporate/collection_report/list');
            }
            // else
            // {
            //     $this->collection_report_model->deleteTempProgHasDeanDataBySession($id_session);
            // }

            $data['countryList'] = $this->collection_report_model->countryListByStatus('1');
            $data['parentCompanyList'] = $this->collection_report_model->parentCompanyList('Parent');
            $data['statusListByType'] = $this->collection_report_model->statusListByType('Company');

            // echo "<Pre>";print_r($data['partnerCategoryList']);exit();

            $this->global['pageTitle'] = 'Inventory Management : Add Programme';
            $this->loadViews("collection_report/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('collection_report.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/corporate/collection_reportApproval/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $id_user = $this->session->userId;

                $level = $this->security->xss_clean($this->input->post('level'));
                $id_parent_collection_report = $this->security->xss_clean($this->input->post('id_parent_collection_report'));
                $registration_number = $this->security->xss_clean($this->input->post('registration_number'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $joined_date = $this->security->xss_clean($this->input->post('joined_date'));
                $website = $this->security->xss_clean($this->input->post('website'));
                $institution_type = $this->security->xss_clean($this->input->post('institution_type'));
                $chairman = $this->security->xss_clean($this->input->post('chairman'));
                $ceo = $this->security->xss_clean($this->input->post('ceo'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $country_code = $this->security->xss_clean($this->input->post('country_code'));
                $phone = $this->security->xss_clean($this->input->post('phone'));
                $corporate_membership_status = $this->security->xss_clean($this->input->post('corporate_membership_status'));
                $staff_strength = $this->security->xss_clean($this->input->post('staff_strength'));
                $debtor_code = $this->security->xss_clean($this->input->post('debtor_code'));
                $credit_term = $this->security->xss_clean($this->input->post('credit_term'));
                $control_account = $this->security->xss_clean($this->input->post('control_account'));
                $credit_term_code = $this->security->xss_clean($this->input->post('credit_term_code'));
                $primary_contact_name = $this->security->xss_clean($this->input->post('primary_contact_name'));
                $primary_contact_designation = $this->security->xss_clean($this->input->post('primary_contact_designation'));
                $primary_contact_email = $this->security->xss_clean($this->input->post('primary_contact_email'));
                $primary_country_code = $this->security->xss_clean($this->input->post('primary_country_code'));
                $primary_phone = $this->security->xss_clean($this->input->post('primary_phone'));
                $second_contact_name = $this->security->xss_clean($this->input->post('second_contact_name'));
                $second_contact_designation = $this->security->xss_clean($this->input->post('second_contact_designation'));
                $second_contact_email = $this->security->xss_clean($this->input->post('second_contact_email'));
                $second_country_code = $this->security->xss_clean($this->input->post('second_country_code'));
                $second_phone = $this->security->xss_clean($this->input->post('second_phone'));
                $staff_credit = $this->security->xss_clean($this->input->post('staff_credit'));
                $status = $this->security->xss_clean($this->input->post('status'));



            
                $data = array(
                    'level' => $level,
                    'id_parent_collection_report' => $id_parent_collection_report,
                    'registration_number' => $registration_number,
                    'name' => $name,
                    'joined_date' => date('Y-m-d', strtotime($joined_date)),
                    'website' => $website,
                    'institution_type' => $institution_type,
                    'chairman' => $chairman,
                    'ceo' => $ceo,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'country_code' => $country_code,
                    'phone' => $phone,
                    'corporate_membership_status' => $corporate_membership_status,
                    'staff_strength' => $staff_strength,
                    'debtor_code' => $debtor_code,
                    'credit_term' => $credit_term,
                    'control_account' => $control_account,
                    'credit_term_code' => $credit_term_code,
                    'primary_contact_name' => $primary_contact_name,
                    'primary_contact_designation' => $primary_contact_designation,
                    'primary_contact_email' => $primary_contact_email,
                    'primary_country_code' => $primary_country_code,
                    'primary_phone' => $primary_phone,
                    'second_contact_name' => $second_contact_name,
                    'second_contact_designation' => $second_contact_designation,
                    'second_contact_email' => $second_contact_email,
                    'second_country_code' => $second_country_code,
                    'second_phone' => $second_phone,
                    'staff_credit' => $staff_credit,
                    'status' => $status
                );

                // echo "<Pre>";print_r($data);exit;
                
                $result = $this->collection_report_model->editProgrammeDetails($data,$id);
                redirect('/corporate/collection_report/list');
            }

            $data['id_collection_report'] = $id;
            
            $data['countryList'] = $this->collection_report_model->countryListByStatus('1');
            $data['collection_reportUserRoleList'] = $this->collection_report_model->collection_reportUserRoleListByStatus('1');
            $data['parentCompanyList'] = $this->collection_report_model->parentCompanyList('Parent');
            $data['statusListByType'] = $this->collection_report_model->statusListByType('Company');

            $data['collection_reportDetails'] = $this->collection_report_model->getCompanyDetails($id);
            $data['collection_reportUsersList'] = $this->collection_report_model->getCompanyUsersList($id);
            
            // echo "<Pre>";print_r($data['statusListByType']);exit;

            $this->global['pageTitle'] = 'Inventory Management : Edit Company';
            $this->loadViews("collection_report/edit", $this->global, $data, NULL);
        }
    }
}