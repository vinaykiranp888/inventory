<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class InvoiceReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice_report_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        $id_company = $this->session->id_company;
        
        $from_date = $this->security->xss_clean($this->input->post('from_date'));
        $to_date = $this->security->xss_clean($this->input->post('to_date'));
        $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
        $formData['type'] = $this->security->xss_clean($this->input->post('type'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));
        $formData['from_date'] = '';
        $formData['to_date'] = '';
        $formData['id_company'] = $id_company;

        if($from_date != '')
        {
            $formData['from_date'] = date('Y-m-d',strtotime($from_date));
        }
        if($to_date != '')
        {
            $formData['to_date'] = date('Y-m-d',strtotime($to_date));
        }
        
        $data['searchParam'] = $formData;


        if($this->input->post())
        {
            $formSubmit = $this->input->post();

            $btn_submit = $formSubmit['btn_submit'];
            
            // echo "<Pre>";print_r($formSubmit);exit;

            if($btn_submit == 'download')
            {
                $this->downloadInvoiceReportCSV($formData);
            }


            $data['invoiceReportList'] = $this->invoice_report_model->invoiceReportListSearch($formData);
        }
        else
        {
            $data['invoiceReportList'] = array();
        }

        $data['programmeList'] = $this->invoice_report_model->programmeListByStatus('1');

        $this->global['pageTitle'] = 'Inventory Management : Corporate Company List';
        $this->loadViews("invoice_report/list", $this->global, $data, NULL);
    }

    function view($id = NULL)
    {
        if ($this->checkAccess('invoice_report.view') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/company_user_reports/invoiceReport/list');
            }
            if($this->input->post())
            {
                redirect('/company_user_reports/invoiceReport/list');
            }

            $data['mainInvoice'] = $this->invoice_report_model->getMainInvoice($id);
            $data['mainInvoiceDetailsList'] = $this->invoice_report_model->getMainInvoiceDetails($id);
            $data['mainInvoiceDiscountDetailsList'] = $this->invoice_report_model->getMainInvoiceDiscountDetails($id);
            $data['mainInvoiceHasStudentList'] = $this->invoice_report_model->getMainInvoiceHasStudentList($id);
            
            if($data['mainInvoice']->type == 'Student')
            {
                $data['invoiceFor'] = $this->invoice_report_model->getMainInvoiceStudentData($data['mainInvoice']->id_student);
            }elseif($data['mainInvoice']->type == 'CORPORATE')
            {
                $data['invoiceFor'] = $this->invoice_report_model->getMainInvoiceCorporateData($data['mainInvoice']->id_student);
            }

            $data['degreeTypeList'] = $this->invoice_report_model->qualificationListByStatus('1');
            // echo "<Pre>";  print_r($data['mainInvoice']);exit;

            $this->global['pageTitle'] = 'Inventory Management : View Main Invoice';
            $this->loadViews("invoice_report/view", $this->global, $data, NULL);
        }
    }

    function downloadInvoiceReportCSV($tempData)
    {
        // $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";print_r($tempData);exit;
        $fileData = $this->invoice_report_model->downloadInvoiceReportCSVBtwnDates($tempData);


        $fileName = gmdate("YmdHis");

        $this->generateCSVInvoiceReport($fileData, $fileName, $tempData);
    }

    public function generateCSVInvoiceReport($fileData, $fileName, $data)
    {
        $fromDate = $data['from_date'];
        $toDate = $data['to_date'];
        $pay_date   = date("d/m/Y");


        // $organisationDetails = $this->invoice_report_model->getOrganisation();

        // echo "<Pre>";print_r($fileData);exit;


        // $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.svg";

        // if($organisationDetails->image != '')
        // {
        //     $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/" . $organisationDetails->image;
        // }



        $file_name = $fileName . "_InvoiceReport.csv";

        

        $fp = fopen('php://output', 'w');

        $header = array();

        array_push($header, "Invoice Report");

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=" . $file_name);
        header("Pragma: no-cache");
        header("Expires: 0");

        fputcsv($fp, $header);

        $empty_header = array();

        array_push($empty_header, " ");
        fputcsv($fp, $empty_header);


        $dtheader = array();

        array_push($dtheader, "No");
        array_push($dtheader, "Invoice Type");
        array_push($dtheader, "Invoice Number");
        array_push($dtheader, "Participant Name");
        array_push($dtheader, "IC / Passport No.");
        array_push($dtheader, "COURSE Name");
        array_push($dtheader, "Invoice Amount");
        array_push($dtheader, "GST Amount");
        array_push($dtheader, "Total Amount");
        array_push($dtheader, "Currency");
        array_push($dtheader, "Invoice Date");
        array_push($dtheader, "DESCRIPTION");

        fputcsv($fp, $dtheader);

        $i=1;
        foreach ($fileData as $data)
        {
            $invoice_number = $data->invoice_number;
            $date_time = date('d-m-Y', strtotime($data->date_time));
            $type = $data->type;
            $id_student = $data->id_student;
            $total_amount = $data->total_amount;
            $amount_before_gst = $data->amount_before_gst;
            $gst_amount = $data->gst_amount;
            $gst_percentage = $data->gst_percentage;
            $invoice_total = $data->invoice_total;
            $currency_name = $data->currency_name;
            $remarks = $data->remarks;
            $programme_code = $data->programme_code;
            $programme_name = $data->programme_name;

            $programme = $programme_code  . " - " . $programme_name;
            // $total_currency = $invoice_total . " ( " . $ . " ) ";

            $student_name = '';
            $student_nric = '';

            if($type == 'CORPORATE')
            {
                $company = $this->invoice_report_model->getCompany($id_student);
                if($company)
                {
                    $student_name = $company->name; 
                    $student_nric = $company->registration_number; 
                }
            }
            elseif($type == 'Student')
            {
                $student = $this->invoice_report_model->getStudent($id_student);
                if($student)
                {
                    $student_name = $student->full_name; 
                    $student_nric = $student->nric; 
                }
            }


            $data_array = array();


            array_push($data_array, $i);
            array_push($data_array, $type);
            array_push($data_array, $invoice_number);
            array_push($data_array, $student_name);
            array_push($data_array, $student_nric);
            array_push($data_array, $programme);
            array_push($data_array, $amount_before_gst);
            array_push($data_array, $gst_amount);
            array_push($data_array, $invoice_total);
            array_push($data_array, $currency_name);
            array_push($data_array, $date_time);
            array_push($data_array, $remarks);


            // array_push($data_array, $i);
            // array_push($data_array, $student_name);
            // array_push($data_array, $student_nric);
            // array_push($data_array, $programme_code . " - " . $$programme_name);
            // array_push($data_array, $invoice_total . " ( " . $currency_name . " ) ");
            // array_push($data_array, $date_time);
            // array_push($data_array, $remarks);

            fputcsv($fp, $data_array);
            $i++;
        }

        fclose($fp);
        exit;
        // echo $file_name;

    }
}