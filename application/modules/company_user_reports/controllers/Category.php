<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Category extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model');
        $this->isCompanyUserLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('category.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['categoryList'] = $this->category_model->categoryListSearch($formData);

            $this->global['pageTitle'] = 'Speed Management System : Category List';
            $this->global['pageCode'] = 'category.list';

            $this->loadViews("category/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('category.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->category_model->addNewCategory($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Category created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category creation failed');
                }
                redirect('/prdtm/category/list');
            }
           
            $this->global['pageTitle'] = 'Speed Management System : Add Category';
            $this->global['pageCode'] = 'category.add';

            $this->loadViews("category/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('category.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/category/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                
                $result = $this->category_model->editCategory($data,$id);
                if ($result)
                {
                    $this->session->set_flashdata('success', 'Category edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category edit failed');
                }
                redirect('/prdtm/category/list');
            }

            $data['category'] = $this->category_model->getCategory($id);

            $this->global['pageCode'] = 'category.list';
            $this->global['pageTitle'] = 'Speed Management System : Edit Category';
            
            $this->loadViews("category/edit", $this->global, $data, NULL);
        }
    }


    function addModule($id = NULL)
    {
        if ($this->checkAccess('category.add_module') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/prdtm/category/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // echo "<Pre>";print_r($this->input->post());exit;

                $id_course = $this->security->xss_clean($this->input->post('id_course'));
            
                $data = array(
                    'id_course' => $id_course, // Module Id
                    'id_category' => $id,
                    'status' => 1,
                    'created_by' => $user_id
                );
                
                $result = $this->category_model->addCategoryHasModule($data);
                // if ($result)
                // {
                //     $this->session->set_flashdata('success', 'Category edited successfully');
                // }
                // else
                // {
                //     $this->session->set_flashdata('error', 'Category edit failed');
                // }
                redirect('/prdtm/category/addModule/'.$id);
            }

            $data['courseList'] = $this->category_model->courseListByStatus('1');

            $data['category'] = $this->category_model->getCategory($id);
            $data['getCategoryHasModule'] = $this->category_model->getCategoryHasModule($id);

            // echo "<pre>";print_r($data['getCategoryHasModule']);die;


            $this->global['pageCode'] = 'category.add_module';
            $this->global['pageTitle'] = 'Speed Management System : Edit Category';
            
            $this->loadViews("category/add_module", $this->global, $data, NULL);
        }
    }

    function deleteCategoryHasModule($id)
    {
            $deleted_id = $this->category_model->deleteCategoryHasModule($id);
            echo "success";exit;
    }
}
