<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Welcome extends BaseController
{
	 public function __construct()
    {
        parent::__construct();
        $this->isCompanyUserLoggedIn();
        $this->load->model('role_model');

    }

    public function index()
    {
        
        $this->global['pageTitle'] = 'Inventory Management : Welcome To Curriculum Management';        
        $this->loadViews("includes/welcome", $this->global, NULL , NULL);
    }   
}