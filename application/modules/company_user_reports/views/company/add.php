<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Add Company</h3>
            </div>


    <form id="form_programme" action="" method="post" enctype="multipart/form-data">
        
    <div class="form-container">
        <h4 class="form-group-title">Company Details</h4>
            

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Company Level <span class='error-text'>*</span></label>
                        <select name="level" id="level" class="form-control" onchange="showParentCompany()">
                            <option value="">Select</option>
                            <option value="Parent">Parent</option>
                            <!-- <option value="Branch">Branch</option> -->
                        </select>
                    </div>
                </div>

                <div class="col-sm-4" id='partnerdropdown' style="display: none;">
                    <div class="form-group">
                        <label>Parent Company <span class='error-text'>*</span></label>
                        <select name="id_parent_company" id="id_parent_company" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($parentCompanyList))
                            {
                                foreach ($parentCompanyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                    <?php echo $record->registration_number . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                
            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registration Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="registration_number" name="registration_number">
                    </div>
                </div>


                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password <span class='error-text'>*</span></label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div> -->


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Joined Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="joined_date" name="joined_date" value="<?php echo date('d-m-Y'); ?>" readonly>
                    </div>
                </div>




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Website <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="website" name="website">
                    </div>
                </div>



            </div>

            <div class="row">





                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Chairman <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="chairman" name="chairman">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>CEO <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="ceo" name="ceo">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1">
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address2" name="address2">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control" onchange="getStateByCountry(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                   <div class="form-group">
                      <label>Select State <span class='error-text'>*</span></label>
                      <span id='view_state'>
                           <select name="id_state" id="id_state" class="form-control">
                            <option value=''>Select</option>
                           </select>
                      </span>
                   </div>
                </div>

            </div>

            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Postal Code <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="zipcode" name="zipcode">
                    </div>
                </div>


                
                <div class="col-sm-4">
                    <label>Telephone Number <span class='error-text'>*</span></label>
                      <div class="row">
                         <div class="col-sm-4">

                           <select name="country_code" id="country_code" class="form-control" required>
                            <option value="">Select</option>                    
                            <?php
                                if (!empty($countryList))
                                {
                                  foreach ($countryList as $record)
                                  {
                                ?>
                             <option value="<?php echo $record->phone_code;  ?>">
                                <?php echo $record->phone_code . "  " . $record->name;  ?>
                             </option>
                               <?php
                                }
                              }
                            ?>
                          </select>
                    </div>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" id="phone" name="phone" required>
                    </div>
                    </div>

                </div>




            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Staff Strength <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="staff_strength" name="staff_strength">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Corporate Membership Status <span class='error-text'>*</span></label>
                        <select name="corporate_membership_status" id="corporate_membership_status" class="form-control">
                            <option value="">Select</option>
                            <option value="Active">Active</option>
                            <option value="In-Active">In-Active</option>
                            </select>
                    </div>
                </div>

                

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Debtor Code <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="debtor_code" name="debtor_code">
                    </div>
                </div>

            </div>

            <div class="row">




                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Terms(DAYS) <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="credit_term" name="credit_term">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Control Account <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="control_account" name="control_account">
                    </div>
                </div>


                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Credit Term Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="credit_term_code" name="credit_term_code">
                    </div>
                </div>

            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Staff Credit Acceptable <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="staff_credit" id="staff_credit" value="1" checked="checked"><span class="check-radio"></span> Yes
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="staff_credit" id="staff_credit" value="0"><span class="check-radio"></span> No
                        </label>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <select name="status" id="status" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($statusListByType))
                            {
                                foreach ($statusListByType as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>


        </div>

        <br>



        <div class="form-container">
         <h4 class="form-group-title">Primary Contact Details</h4>

         <div class="row">
         
            <div class="col-sm-4">
               <div class="form-group">
                  <label>Name <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="primary_contact_name" name="primary_contact_name">
               </div>
            </div>

            <div class="col-sm-4">
               <div class="form-group">
                  <label>Designation <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="primary_contact_designation" name="primary_contact_designation">
               </div>
            </div>


            <div class="col-sm-4">
               <div class="form-group">
                  <label>Email <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="primary_contact_email" name="primary_contact_email">
               </div>
            </div>
            
            
         
         </div>

         <div class="row">
         
            <div class="col-sm-4">
                <label>Telephone Number <span class='error-text'>*</span></label>
                  <div class="row">
                     <div class="col-sm-4">

                       <select name="primary_country_code" id="primary_country_code" class="form-control" required>
                        <option value="">Select</option>                    
                        <?php
                            if (!empty($countryList))
                            {
                              foreach ($countryList as $record)
                              {
                            ?>
                         <option value="<?php echo $record->phone_code;  ?>">
                            <?php echo $record->phone_code . "  " . $record->name;  ?>
                         </option>
                           <?php
                            }
                          }
                        ?>
                      </select>
                </div>
                <div class="col-sm-8">
                  <input type="number" class="form-control" id="primary_phone" name="primary_phone" required>
                </div>
                </div>

            </div>


         </div>
      
      </div>




      <div class="form-container">
         <h4 class="form-group-title">Secondary Contact Details</h4>

         <div class="row">
         
            <div class="col-sm-4">
               <div class="form-group">
                  <label>Name <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="second_contact_name" name="second_contact_name">
               </div>
            </div>

            <div class="col-sm-4">
               <div class="form-group">
                  <label>Designation <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="second_contact_designation" name="second_contact_designation">
               </div>
            </div>


            <div class="col-sm-4">
               <div class="form-group">
                  <label>Email <span class='error-text'>*</span></label>
                  <input type="text" class="form-control" id="second_contact_email" name="second_contact_email">
               </div>
            </div>
            
            
         
         </div>

         <div class="row">
         
            <div class="col-sm-4">
                <label>Telephone Number <span class='error-text'>*</span></label>
                  <div class="row">
                     <div class="col-sm-4">

                       <select name="second_country_code" id="second_country_code" class="form-control" required>
                        <option value="">Select</option>                    
                        <?php
                            if (!empty($countryList))
                            {
                              foreach ($countryList as $record)
                              {
                            ?>
                         <option value="<?php echo $record->phone_code;  ?>">
                            <?php echo $record->phone_code . "  " . $record->name;  ?>
                         </option>
                           <?php
                            }
                          }
                        ?>
                      </select>
                </div>
                <div class="col-sm-8">
                  <input type="number" class="form-control" id="second_phone" name="second_phone" required>
                </div>
                </div>

            </div>


         </div>
      
      </div>



    <div class="button-block clearfix">
        <div class="bttn-group">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            <a href="list" class="btn btn-link">Cancel</a>
        </div>
    </div>


    </form>


        
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>

<script>

    $('select').select2();


    $(function()
    {
        $( ".datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
        });
    });

    function showParentCompany()
    {
        var value = $("#level").val();
        if(value=='Parent')
        {
            $("#partnerdropdown").hide();
        }
        else if(value=='Branch')
        {
            $("#partnerdropdown").show();
        }
    }



    function getStateByCountry(id)
    {
        if(id != '')
        {
            $.get("/corporate/company/getStateByCountry/"+id, function(data, status)
            {
                $("#view_state").html(data);
            });
        }
    }

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                level: {
                    required: true
                },
                id_parent_company: {
                    required: true
                },
                name: {
                    required: true
                },
                registration_number: {
                    required: true
                },
                joined_date: {
                    required: true
                },
                website: {
                    required: true
                },
                chairman: {
                    required: true
                },
                ceo: {
                    required: true
                },
                address1: {
                    required: true
                },
                address2: {
                    required: true
                },
                id_country: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                country_code: {
                    required: true
                },
                phone: {
                    required: true
                },
                staff_strength: {
                    required: true
                },
                corporate_membership_status: {
                    required: true
                },
                debtor_code: {
                    required: true
                },
                credit_term: {
                    required: true
                },
                control_account: {
                    required: true
                },
                credit_term_code: {
                    required: true
                },
                primary_contact_name: {
                    required: true
                },
                primary_contact_designation: {
                    required: true
                },
                primary_contact_email: {
                    required: true
                },
                primary_country_code: {
                    required: true
                },
                primary_phone: {
                    required: true
                },
                second_contact_name: {
                    required: true
                },
                second_contact_designation: {
                    required: true
                },
                second_contact_email: {
                    required: true
                },
                second_country_code: {
                    required: true
                },
                second_phone: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                level: {
                    required: "<p class='error-text'>Level Required</p>",
                },
                id_parent_company: {
                    required: "<p class='error-text'>Parent Company Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                registration_number: {
                    required: "<p class='error-text'>Registration Number Required</p>",
                },
                joined_date: {
                    required: "<p class='error-text'>Joined Date Required</p>",
                },
                website: {
                    required: "<p class='error-text'>Website Required</p>",
                },
                chairman: {
                    required: "<p class='error-text'>Chairman Required</p>",
                },
                ceo: {
                    required: "<p class='error-text'>CEO Required</p>",
                },
                address1: {
                    required: "<p class='error-text'>Address1 Reuired</p>",
                },
                address2: {
                    required: "<p class='error-text'>Address2 Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Reuired</p>",
                },
                country_code: {
                    required: "<p class='error-text'>Select Country Code</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                staff_strength: {
                    required: "<p class='error-text'>Staff Strength Required</p>",
                },
                corporate_membership_status: {
                    required: "<p class='error-text'>Select Corporate Membership Status</p>",
                },
                debtor_code: {
                    required: "<p class='error-text'>Debtor Code Required</p>",
                },
                credit_term: {
                    required: "<p class='error-text'>Credit Term Reuired</p>",
                },
                control_account: {
                    required: "<p class='error-text'>Control Account Required</p>",
                },
                credit_term_code: {
                    required: "<p class='error-text'>Credit Term Code Required</p>",
                },
                primary_contact_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                primary_contact_designation: {
                    required: "<p class='error-text'>Designation Required</p>",
                },
                primary_contact_email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                primary_country_code: {
                    required: "<p class='error-text'>Country Code Required</p>",
                },
                primary_phone: {
                    required: "<p class='error-text'>Phone Required</p>",
                },
                second_contact_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                second_contact_designation: {
                    required: "<p class='error-text'>Designation Required</p>",
                },
                second_contact_email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                second_country_code: {
                    required: "<p class='error-text'>Country Code Required</p>",
                },
                second_phone: {
                    required: "<p class='error-text'>Phone Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>