<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model
{
    function categoryListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('category as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function categoryListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('category as c');
        // $this->db->join('department as d', 'c.id_department = d.id');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_staff'] != '')
        // {
        //     $this->db->where('c.id_staff_coordinator', $data['id_staff']);
        // }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getCategory($id)
    {
        $this->db->select('c.*');
        $this->db->from('category as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCategory($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('category', $data);

        return $result;
    }

    function deleteCategory($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('category', $data);

        return $this->db->affected_rows();
    }

    function addCategoryHasModule($data)
    {
        $this->db->trans_start();
        $this->db->insert('category_has_module', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getCategoryHasModule($id_category)
    {
        $this->db->select('chm.*, c.code as course_code, c.name as course_name');
        $this->db->from('category_has_module as chm');
        $this->db->join('course as c', 'chm.id_course = c.id');
        $this->db->where('chm.id_category', $id_category);
        $query = $this->db->get();
        $result = $query->result();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function deleteCategoryHasModule($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('category_has_module');

        return $this->db->affected_rows();
    }
}
