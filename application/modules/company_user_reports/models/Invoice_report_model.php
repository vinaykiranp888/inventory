<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_report_model extends CI_Model
{
    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function invoiceReportListSearch($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        // $this->db->join('student as s', 'mi.id_student = s.id');
        // $this->db->join('intake as i', 's.id_intake = i.id');
        if ($data['type'] != '')
        {
            $this->db->where('mi.type', $data['type']);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('mi.id_program', $data['id_programme']);
        }
        if ($data['id_company'] != '')
        {
            $this->db->where('mi.id_student', $data['id_company']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.date_time >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.date_time <=', $data['to_date']);
        }
        $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function downloadInvoiceReportCSVBtwnDates($data)
    {
        $this->db->select('mi.*, p.code as programme_code, p.name as programme_name, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        if ($data['id_company'] != '')
        {
            $this->db->where('mi.id_student', $data['id_company']);
        }
        if ($data['from_date'] != '')
        {
            $this->db->where('mi.date_time >=', $data['from_date']);
        }
        if ($data['to_date'] != '')
        {
            $this->db->where('mi.date_time <=', $data['to_date']);
        }
        $this->db->where('mi.status', 1);
        $this->db->order_by("mi.id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*,  p.name as programme_name, p.code as programme_code, cs.name as currency_name');
        $this->db->from('main_invoice as mi');
        $this->db->join('programme as p', 'mi.id_program = p.id','left');
        $this->db->join('currency_setup as cs', 'mi.currency = cs.id','left');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getMainInvoiceDetails($id)
    {
        $this->db->select('mid.*, fstp.name as fee_setup');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('fee_setup as fstp', 'mid.id_fee_item = fstp.id');     
        $this->db->where('mid.id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceDiscountDetails($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice_discount_details');   
        $this->db->where('id_main_invoice', $id);
        $query = $this->db->get();
        return $query->result();
    }

    function getMainInvoiceHasStudentList($id_student)
    {
        $this->db->select('stu.full_name, stu.nric, stu.id_degree_type');
        $this->db->from('student as stu');
        $this->db->where('stu.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function qualificationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('education_level');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result(); 
         return $result;
    }

    function getMainInvoiceCorporateData($id)
    {
        $this->db->select('stu.name as full_name, stu.registration_number as nric');
        $this->db->from('company as stu');
        $this->db->where('stu.id', $id);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getOrganisation()
    {
        $this->db->select('*');
        $this->db->from('organisation');
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        return $query->row();
    }

    function getCompany($id)
    {
        $this->db->select('*');
        $this->db->from('company');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}