$(function () {
  // $('[data-toggle="tooltip"]').tooltip()

  // Filter menu checkbox/radio on click open sub menu
  // $('.filter-dropdown .dropdown-menu').on('click', function (e) {
  //   $(this).addClass('show')
  //   $(this).parent().addClass('show')
  // })
})

// Get filter Element
function getElement(selection) {
  const element = document.querySelector(selection)
  if (element) {
    return element
  }
  throw new Error(
    `Please check "${selection}" selector, no such element exists`
  )
}

// Reusable Counter class to update multi checkbox/radio count
class Counter {
  constructor(element) {
    this.counter = element
    this.value = 0
    this.clearbtn = element.querySelector('.clear-btn')
    this.allFields = element.querySelectorAll('.custom-control')
    this.inputField = element.querySelector('.custom-control-input')
    this.valueDOM = element.querySelector('.value')
    this.valueDOM.textContent = this.value

    this.init()

    // Update count on checkbox change
    this.allFields.forEach((field) => {
      const inputField = field.querySelector('.custom-control-input')
      inputField.addEventListener('change', () => {
        const inputType = inputField.getAttribute('type')
        if (inputField.checked && inputType === 'checkbox') {
          this.value++
        } else if (inputField.checked && inputType === 'radio') {
          this.value = 1
        } else {
          this.value--
        }

        if (this.value > 0) {
          this.valueDOM.style.display = 'inline-block'
        } else {
          this.valueDOM.style.display = 'none'
        }
        this.valueDOM.textContent = this.value
        this.counter.classList.add('show')
        this.counter.querySelector('.dropdown-menu').classList.add('show')
      })
    })

    // bind this to all function
    this.clear = this.clear.bind(this)
    this.clearbtn.addEventListener('click', this.clear)
  }

  // update intial selection
  init() {
    this.allFields.forEach((field) => {
      const inputField = field.querySelector('.custom-control-input')
      if (inputField.checked) {
        this.value++
        this.valueDOM.textContent = this.value
      }
    })
    if (this.value > 0) {
      this.valueDOM.style.display = 'inline-block'
    } else {
      this.valueDOM.style.display = 'none'
    }
  }

  // clear all checkbox/radio selection
  clear() {
    this.value = 0
    this.valueDOM.textContent = this.value
    this.valueDOM.style.display = 'none'
    this.allFields.forEach((field) => {
      const inputField = field.querySelector('.custom-control-input')
      inputField.checked = false
    })
    this.counter.classList.add('show')
    this.counter.querySelector('.dropdown-menu').classList.add('show')
  }
}

// Add Counter instance to multiple fields
const categoryFilter = new Counter(getElement('#categoryFilter'))
const productTypeFilter = new Counter(getElement('#productTypeFilter'))
const studyLevelFilter = new Counter(getElement('#studyLevelFilter'))
const skillLevelFilter = new Counter(getElement('#skillLevelFilter'))
