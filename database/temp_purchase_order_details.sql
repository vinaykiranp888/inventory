-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 19, 2021 at 10:50 AM
-- Server version: 10.2.38-MariaDB-log
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `temp_assembly_distribution_details`
--

CREATE TABLE `temp_assembly_distribution_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(256) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `received_quantity` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `temp_assembly_distribution_details`
--
ALTER TABLE `temp_assembly_distribution_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `temp_assembly_distribution_details`
--
ALTER TABLE `temp_assembly_distribution_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
