-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 06, 2021 at 01:29 AM
-- Server version: 10.2.38-MariaDB-log
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `assembly_distribution`
--

CREATE TABLE `assembly_distribution` (
  `id` int(20) NOT NULL,
  `reference_number` varchar(50) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `id_assembly` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly_distribution`
--

INSERT INTO `assembly_distribution` (`id`, `reference_number`, `description`, `id_assembly`, `total_amount`, `paid_amount`, `balance_amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'AD000001/2021', 'Description', 1, 1880.00, 0.00, 1880.00, 3, 1, '2021-05-21 14:03:24', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `assembly_distribution_details`
--

CREATE TABLE `assembly_distribution_details` (
  `id` int(20) NOT NULL,
  `id_assembly_distribution` int(20) DEFAULT 0,
  `id_assembly_return_detail` int(20) DEFAULT 0,
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `received_quantity` int(20) DEFAULT 0 COMMENT 'its Returned Quantity as Per the Flow',
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly_distribution_details`
--

INSERT INTO `assembly_distribution_details` (`id`, `id_assembly_distribution`, `id_assembly_return_detail`, `id_category`, `id_sub_category`, `id_item`, `quantity`, `balance_quantity`, `received_quantity`, `price`, `total_price`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 2, 1000, 612, 388, 1.88, 1880.00, 1, NULL, '2021-05-21 14:03:15', 1, '2021-05-22 02:34:21');

-- --------------------------------------------------------

--
-- Table structure for table `assembly_return`
--

CREATE TABLE `assembly_return` (
  `id` int(20) NOT NULL,
  `id_assembly_distribution` int(20) DEFAULT 0,
  `reference_number` varchar(256) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `id_assembly` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly_return`
--

INSERT INTO `assembly_return` (`id`, `id_assembly_distribution`, `reference_number`, `description`, `id_assembly`, `total_amount`, `paid_amount`, `balance_amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'AR000001/2021', 'DEsc', 1, 564.00, 0.00, 564.00, 7, 1, '2021-05-21 14:03:58', NULL, ''),
(2, 1, 'AR000002/2021', 'Returned', 1, 165.44, 0.00, 165.44, 7, 1, '2021-05-21 14:04:21', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `assembly_return_details`
--

CREATE TABLE `assembly_return_details` (
  `id` int(20) NOT NULL,
  `id_assembly_return` int(20) DEFAULT 0,
  `id_assembly_distribution_detail` int(20) DEFAULT 0,
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `total_quantity` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly_return_details`
--

INSERT INTO `assembly_return_details` (`id`, `id_assembly_return`, `id_assembly_distribution_detail`, `id_category`, `id_sub_category`, `id_item`, `total_quantity`, `quantity`, `balance_quantity`, `price`, `total_price`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 2, 1000, 300, 700, 1.88, 564.00, 1, 1, '2021-05-21 14:03:58', NULL, ''),
(2, 2, 1, 1, 1, 2, 1000, 88, 612, 1.88, 165.44, 1, 1, '2021-05-21 14:04:21', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `assembly_team`
--

CREATE TABLE `assembly_team` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `city` varchar(512) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `date_of_birth` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assembly_team`
--

INSERT INTO `assembly_team` (`id`, `name`, `code`, `email`, `phone`, `gender`, `nric`, `mobile`, `address_one`, `address_two`, `city`, `country`, `state`, `zipcode`, `date_of_birth`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Computer Tech Assembly', 'A00001-2021', 'comptechassembly@mail.com', '', '', '', '+918550078787', 'Bengaluru', '1', 'Bengaluru', '79', '95', '23323', '', 1, 1, '2021-05-19 12:34:13', 1, '2021-05-22 02:06:06');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Management', '1', '', 1, NULL, '2021-02-04 17:53:56', NULL, '2021-02-04 17:53:56'),
(2, 'Business', '2', 'BUSINESS', 1, NULL, '2021-02-24 08:10:10', NULL, '2021-02-24 08:10:10');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message`
--

CREATE TABLE `communication_group_message` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_message`
--

INSERT INTO `communication_group_message` (`id`, `id_template`, `name`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Group ne', 'Student', 1, 1, '', '2020-10-29 11:52:00', NULL, '2020-10-29 11:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message_recepients`
--

CREATE TABLE `communication_group_message_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_message_recepients`
--

INSERT INTO `communication_group_message_recepients` (`id`, `id_group`, `id_recepient`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 30, 'Student', 1, 1, '', '2020-10-29 11:52:15', NULL, '2020-10-29 11:52:15');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `id_university` int(20) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `id_university`, `id_education_level`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Offer Letter', 'Offer Letter', '<p>@studentname<br>\r\n@mail_address1<br>\r\n@mail_address2<br>\r\n@mailing_city , @mailing_zipcode<br>\r\n<br>\r\nDear Sir / Madam,<br>\r\n<strong>OFFER FOR ADMISSION INTO POSTGRADUATE PROGRAMME 2020/2021 SESSION<br>\r\nCongratulations! We are pleased to inform you that the University has decided to offer the following programme:</strong></p>\r\n\r\n<table border=\"0\" cellpadding=\"2\" cellspacing=\"2\">\r\n <tbody>\r\n  <tr>\r\n   <td><strong>PROGRAMME</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@program</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>FACULTY</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@faculty</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>LEARNING MODE</strong></td>\r\n   <td> </td>\r\n   <td><strong>@mode_of_program - @mode_of_study</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>DURATION</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@duration </strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>REGISTRATION DATE</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@created_dt </strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>TIME</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@created_tm</strong></td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<ol>\r\n <li>This offer is subject to the following conditions:\r\n <ol>\r\n  <li> Payable fees on the registration are <strong>RM@REGISTRATIONFEE</strong> . Kindly refer to the attachment for more details on the tuition fees and other related fees.</li>\r\n  <li>USAS has the right to cancel this offer immediately; if any of the information provided is not true, inaccurate or does not meet the eligibility requirements set by the University.</li>\r\n  <li>If you studying in Malaysia, you are required to bring the original documents on the Registration Day.  If you are on ODL or Online Study Mode, your documents must be Certified True Copy by the Embassy or your previous Education Institute and they must be mailed to our Admission Office. Please be reminded that you are not allowed to change your programme or postpone your registration. This offer will automatically be nullified if you are unable to fulfill our requirement(s).</li>\r\n </ol>\r\n </li>\r\n <li>You are kindly requested to read Appendix A for further details concerning the rules and regulations of admission. If you agree to the terms and conditions stated in the appendix, you are confirm  your acceptance at your application portal  not later than @intake via email to admission.usas @eag.com.my</li>\r\n <li>If you wish to study in Malaysia, kindly be informed that the approval of Student Pass requires minimum of <strong>60-90 days</strong> for processing. Therefore, you are also hereby reminded that prior to the approval of your Student pass, you are advised not to enter Malaysia to avoid any immigration issues that might affect your Student Pass application.</li>\r\n</ol>\r\n\r\n<p>Thank you.<br>\r\nYours truly,</p>\r\n\r\n<p><strong>(MOHD TAJUL SABKI BIN ABDUL LATIB)</strong><br>\r\nDean , Admission and Record Division Universiti Sultan Azlan Shah</p>\r\n\r\n<p>THIS IS A COMPUTER-GENERATED LETTER. NO SIGNATURE IS REQUIRED. “INOVASI NADI KESIAGAAN TRANSFORMASI”</p>\r\n\r\n<p>Name: @studentname (@nric)</p>\r\n\r\n<p><strong>UNIVERSITI SULTAN AZLAN SHAH</strong></p>\r\n\r\n<p><strong>FEE(S) STRUCTURE:</strong></p>\r\n\r\n<p><strong>**To be paid upfront before the Registration Date</strong></p>\r\n\r\n<p><strong>@fee_structure</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>APPENDIX A </strong><br>\r\n </p>\r\n\r\n<p><strong>ACCEPTANCE OF OFFER:</strong></p>\r\n\r\n<p>To accept our offer, you will need to submit the following before the offer deadline:</p>\r\n\r\n<ul>\r\n <li>Completed Confirmation of Acceptance at your and payment of initial fee(s) (including the EMGS Visa Processing Fee) outlined in this offer at your Applicnt Portal: http://eag-applicant.camsedu.com/applicantLogin </li>\r\n <li>If your are studying in Malaysia Campus, a list of required documents (filled and completed) - See <strong>CHECKLIST FOR STUDENT’S VISA APPLICATION.</strong></li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p><strong>GENERAL CONDITIONS:</strong></p>\r\n\r\n<ol>\r\n <li>If you are studying in Malaysia:\r\n <ol>\r\n  <li>you are to be personally present at the University campus during Registration and the Orientation prior to the commencement of your programme. You are also required to report to the Academic, Admission and Record Division on the day after of your arrival. If you would be you be delayed for your enrolment, please notify the Admission Unit in advance.</li>\r\n  <li>For International Student, enrolment and commencement of classes is subject to you obtaining your student pass or a Visa Approval Letter (VAL) from the Immigration Department of Malaysia.</li>\r\n </ol>\r\n </li>\r\n <li>In the unlikely event your programme is cancelled or the University is unable to provide the programme for which you applied for, you will either:\r\n <ol>\r\n  <li>Be given a full refund of all fees paid. Refunds shall be processed and paid within 21 days from notification to you of the cancellation of your programme; or</li>\r\n  <li>At your election, in writing, apply for an alternative programme, for which the</li>\r\n  <li>University shall determine your eligibility and proceed to issue a new offer accordingly.</li>\r\n </ol>\r\n </li>\r\n <li>Enrolment, upon your acceptance of the Offer of Admission and payment of the initial fee(s), may with the approval of the University be deferred to the next intake.</li>\r\n <li>Failure to accept this offer and/or to comply with the provisions for acceptance by the offer deadline shall render this offer to be withdrawn.</li>\r\n</ol>\r\n\r\n<p> </p>\r\n\r\n<p><strong>ABOUT YOUR FEES:</strong></p>\r\n\r\n<p>The semester tuition fee quoted is only applicable to the programme commencing in @inatke Intake: First Semester 2020/2021 Session (the exact date will be informed to you the soonest by email). In the event of a variation between the fees on the offer letter and the approved Universiti Sultan Azlan Shah&#39;s structure of fees, the approved will prevail. For the future years of your programme, the University reserves the right to adjust the annual tuition fees.</p>\r\n\r\n<p> </p>\r\n\r\n<h1><strong>REQUIREMENTS FOR INTERNATIONAL APPLICANTS STUDYING IN MALAYSIA</strong></h1>\r\n\r\n<ol>\r\n <li>APPLICANTS NEED TO APPLY FOR VISA 3 MONTHS PRIOR TO REGISTRATION DATE. APPLICANTS NEED TO BE IN THE CURRENT COUNTRY WHEN THE APPLICATION IS BEING MADE.<br>\r\n  </li>\r\n <li>IN TIME WHEN THE APPLICATION IS BEING MADE, THE PASSPORT MUST VALID MORE THAN 15 MONTHS.<br>\r\n  </li>\r\n <li>AFTER THE APPLICATION IS APPROVED BY THE UNIVERSITY, APPLICANTS WILL BE GIVEN AN OFFER LETTER AND MEDICAL CHECK- UP FORM. IT MUST BE COMPLETED IN THE CURRENT RESIDENTIAL COUNTRY.<br>\r\n  </li>\r\n <li>FOR THE APPLICANTS WHO CURRENTLY RESIDE IN MALAYSIA; ALREADY OWN VISA STUDY OR WORKING PERMIT, EITHER ONE NEEDS TO BE SHORTENED OR REVOKED BY THE INSTITUTION OR THE CURRENT EMPLOYER.<br>\r\n  </li>\r\n <li>UNIVERSITI SULTAN AZLAN SHAH WILL NOT ACCEPT ANY APPLICANTS WHO USES SOCIAL PASS VISA TO BE ENROLLED INTO THE UNIVERSITY.<br>\r\n  </li>\r\n <li>APPLICANTS WILL ONLY BE ALLOWED TO BE ENROLLED INTO UNIVERSITI SULTAN AZLAN SHAH AFTER STUDENT VISA APPLICATION IS APPROVED BY MALAYSIAN IMMIGRATION DEPARTMENT AND EDUCATION MALAYSIAN GLOBAL SERVICES (EMGS).</li>\r\n</ol>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<h1>CHECKLIST FOR STUDENT’S VISA APPLICATION (IF YOU ARE STUDYING IN MALAYSIA)</h1>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>NO</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>MATTER</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>?</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>1</p>\r\n   </td>\r\n   <td>\r\n   <p>Application Form</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>2</p>\r\n   </td>\r\n   <td>\r\n   <p>1 copy of all pages of passport</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>3</p>\r\n   </td>\r\n   <td>\r\n   <p>2 copies of passport sized pictures (white background)</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>4</p>\r\n   </td>\r\n   <td>\r\n   <p>Academic transcripts from original country</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>5</p>\r\n   </td>\r\n   <td>\r\n   <p>Translated academic transcripts</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>6</p>\r\n   </td>\r\n   <td>\r\n   <p>Health Declaration Form</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>7</p>\r\n   </td>\r\n   <td>\r\n   <p>Yellow Fever Vaccination Certificate (Countries at High Risk of Yellow Fever Transmission) – See Appendix 1</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>\r\n\r\n<p><strong>Appendix 1</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>COUNTRIES AT HIGH RISK OF YELLOW FEVER TRANSMISSION</strong></p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"3\">\r\n   <p><strong>AFRICA</strong></p>\r\n   </td>\r\n   <td>\r\n   <p><strong>CENTRAL AND SOUTH AMERICA</strong></p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Angola</p>\r\n   </td>\r\n   <td>\r\n   <p>Equatorial Guinea</p>\r\n   </td>\r\n   <td>\r\n   <p>Mauritania</p>\r\n   </td>\r\n   <td>\r\n   <p>Argentina</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Benin</p>\r\n   </td>\r\n   <td>\r\n   <p>Ethiopia</p>\r\n   </td>\r\n   <td>\r\n   <p>Niger</p>\r\n   </td>\r\n   <td>\r\n   <p>Bolivia</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Burkina Faso</p>\r\n   </td>\r\n   <td>\r\n   <p>Gabon</p>\r\n   </td>\r\n   <td>\r\n   <p>Nigeria</p>\r\n   </td>\r\n   <td>\r\n   <p>Brazil</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Burundi</p>\r\n   </td>\r\n   <td>\r\n   <p>Gambia, The</p>\r\n   </td>\r\n   <td>\r\n   <p>Rwanda</p>\r\n   </td>\r\n   <td>\r\n   <p>Colombia</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Cameroon</p>\r\n   </td>\r\n   <td>\r\n   <p>Ghana</p>\r\n   </td>\r\n   <td>\r\n   <p>Senegal</p>\r\n   </td>\r\n   <td>\r\n   <p>Ecuador</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Central African Republic</p>\r\n   </td>\r\n   <td>\r\n   <p>Guinea</p>\r\n   </td>\r\n   <td>\r\n   <p>Sierra Leone</p>\r\n   </td>\r\n   <td>\r\n   <p>French Guiana</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Chad</p>\r\n   </td>\r\n   <td>\r\n   <p>Guinea-Bissau</p>\r\n   </td>\r\n   <td>\r\n   <p>Sudan</p>\r\n   </td>\r\n   <td>\r\n   <p>Guyana</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Congo, Republic of the</p>\r\n   </td>\r\n   <td>\r\n   <p>Kenya</p>\r\n   </td>\r\n   <td>\r\n   <p>South Sudan</p>\r\n   </td>\r\n   <td>\r\n   <p>Panama</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Côte d’Ivoire</p>\r\n   </td>\r\n   <td>\r\n   <p>Liberia</p>\r\n   </td>\r\n   <td>\r\n   <p>Togo</p>\r\n   </td>\r\n   <td>\r\n   <p>Paraguay</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td rowspan=\"4\">\r\n   <p> </p>\r\n\r\n   <p>Democratic Republic of the Congo</p>\r\n   </td>\r\n   <td rowspan=\"4\">\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p>Mali</p>\r\n   </td>\r\n   <td rowspan=\"4\">\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p>Uganda</p>\r\n   </td>\r\n   <td>\r\n   <p>Peru</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Suriname</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Trinidad and Tobago</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Venezuela</p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><strong>*</strong>Countries or areas where “a risk of yellow fever transmission is present,” as defined by the World Health Organization, are countries or areas where “yellow fever has been reported currently or in the past, plus vectors and animal reservoirs currently exist” (see the current country list within the International <em>Travel and Health </em>publication (Annex 1) at <a href=\"http://www.who.int/ith/en/index.html)\">www.who.int/ith/en/index.html).</a></p>\r\n\r\n<p><em>Source: Education Malaysia Global Services (EMGS)</em></p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<h2><strong>HEALTH DECLARATION FORM FOR APPLICANTS STUDYING IN MALAYSIA</strong></h2>\r\n\r\n<p>I hereby declare that I am free from the following diseases/conditions:</p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td rowspan=\"2\">\r\n   <p> </p>\r\n\r\n   <p><strong>ITEMS</strong></p>\r\n   </td>\r\n   <td colspan=\"2\">\r\n   <p>SELF</p>\r\n   </td>\r\n   <td rowspan=\"2\">\r\n   <p>IF NO, PLEASE</p>\r\n\r\n   <p><strong>STATE</strong></p>\r\n   </td>\r\n   <td rowspan=\"17\">\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p>IFYOUHAVE4OUGHY CONSULTATION FOR <strong>ANY OF THE LISTED</strong></p>\r\n\r\n   <p>oiscasEe/cononon, YOU ARE REQUIRED TO SUBMIT YOUR MEDICAL HISTORY/REPORT FROM YOUR TREATING PHYSICIAN TO EDUCATION MALAYSIA <strong>GLOBAL SERVICES </strong>(EMGS) PANEL CLINIC/UNIVERSITY HEALTH CENTRE.</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>YES</p>\r\n   </td>\r\n   <td>\r\n   <p>NO</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Tuberculosis</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Hepatitis B</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Hepatitis C</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>HIV</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td rowspan=\"5\">\r\n   <p>Dtug uss/abuse of:</p>\r\n\r\n   <ol>\r\n    <li>Opiates</li>\r\n    <li>Cannabinoids</li>\r\n    <li>Amphetamine</li>\r\n    <li>Methamphetamine</li>\r\n   </ol>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Sexually Transmitted Diseases</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Congenital or Inherited Disorder</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Cancer</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Epilepsy</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Psychiatric Illness</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Other illness</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"5\">\r\n   <p>declare that I will submit myself for compulsory Post-Arrival Health Examination as per</p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>Malaysian regulations. In the event that I should be diagnosed with any condition that deems me UNSUITABLE for studies, I will bear the cost of leaving Malaysia and will adhere to the immigration requirements on the visit pass and exit before the pass expiration, or any deadline given to me whichever is earlier.</p>\r\n\r\n<p>I declare that in the event I should be diagnosed with any conditions that does not require my removal from Malaysia but requires medical treatment and I choose to remain in Malaysia to continue my studies, I will bear any and ali costs relating directly or indirectly towards the medical management of my medical condition.</p>\r\n\r\n<p>I confirm that EMGS Panel Clinic/University Health Centre shall not be responeible in any manner or whatsoever, arising out of EMGS Panel Clinic/University Health Centre certification of my medical status as suitable to study or reside in Malaysia despite the medical condition described above. I further undertake to hold EMGS Panel Clinic/University Health Centre harmless from any loss or liability arising from this decision and agree to indemnify and keep EMGS Panel Clinic/University Health Centre from any loss or liability arising from this decision.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Date (dd/mm/yyyy)</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p>Applicant’s signature</p>\r\n\r\n<p><strong>Name of applicant as </strong>lndlcated in the passport</p>\r\n\r\n<p>Applicant’s passport number</p>\r\n\r\n<p><strong>Kindly ensure all Information requested in this form is complete and updated In English Language.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n', 0, 0, 1, 1, '2020-08-10 06:37:43', NULL, '2020-08-10 06:37:43'),
(2, 'OFFER LETTER UNDERGRADUATE INTERNATIONAL', 'OFFER LETTER UNDERGRADUATE INTERNATIONAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Please note that the above offer is conditional since all students are required to fulfil their English Language Competency Requirement (ELCR). The ELCR requirement must be fulfilled within a period of two years. Failure to comply within two years after registration may result in the withdrawal of this letter of offer and admission into AeU.</p>\r\n\r\n<p> </p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p> </p>\r\n\r\n<p>This offer is valid for TWO years (6 consecutive intakes of January, May or September) only. It will automatically lapse after two years and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p> </p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 0, 0, 1, 1, '2020-08-10 07:23:22', NULL, '2020-08-10 07:23:22'),
(3, 'OFFER LETTER UNDERGRADUATE LOCAL', 'OFFER LETTER UNDERGRADUATE LOCAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p><br>\r\nThis offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 0, 0, 1, 1, '2020-09-01 16:34:08', NULL, '2020-09-01 16:34:08'),
(4, 'OFFER LETTER POSTGRADUATE LOCAL', 'OFFER LETTER POSTGRADUATE LOCAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p><br>\r\nThis offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 0, 0, 1, 1, '2020-09-01 16:34:30', NULL, '2020-09-01 16:34:30'),
(5, 'OFFER LETTER POSTGRADUATE INTERNATIONAL', 'OFFER LETTER POSTGRADUATE INTERNATIONAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Please note that the above offer is conditional since all students are required to fulfil their English Language Competency Requirement (ELCR). The ELCR requirement must be fulfilled within a period of one year. You are however allowed to register for both the ELCR and your course subjects simultaneously. Failure to comply within a year after registration may result in the withdrawal of this letter of offer and admission into AeU.</p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p>This offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 0, 0, 1, 1, '2020-09-01 16:34:52', NULL, '2020-09-01 16:34:52'),
(6, 'Registration Email', 'Registration Email', '<p>Dear @salutation @studentname,</p>\r\n\r\n<p> </p>\r\n\r\n<p>Welcome to East Asia Global Education. We are an Education Provider that develop and offer educational, academic, and professional programmes in collaboration with outstanding universities and training institutions.</p>\r\n\r\n<p>Please login to view and apply for the programmes offered by our partner universities.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Your login details are as follow:</p>\r\n\r\n<p>URL: http://eag-applicant.camsedu.com/applicantLogin</p>\r\n\r\n<p>Should you have any inquiry or assistance you can chat with our Academic Counsellor after your login to the Portal or email: admission.usas@eag.com.my</p>\r\n', 0, 0, 1, 1, '2021-01-17 15:20:33', NULL, '2021-01-17 15:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template_backup`
--

CREATE TABLE `communication_template_backup` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template_backup`
--

INSERT INTO `communication_template_backup` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Offer Letter', 'Offer Letter', '<p>@studentname<br>\r\n@mail_address1<br>\r\n@mail_address2<br>\r\n@mailing_city<br>\r\n@mailing_zipcode</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir / Madam,</p>\r\n\r\n<p><strong>OFFER FOR ADMISSION INTO POSTGRADUATE PROGRAMME 2020/2021 SESSION<br>\r\nCongratulations! We are pleased to inform you that the University has decided to offer the following programme:</strong></p>\r\n\r\n<table border=\"0\" cellpadding=\"5\" cellspacing=\"5\">\r\n <tbody>\r\n  <tr>\r\n   <td><strong>PROGRAMME</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@program</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>FACULTY</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@faculty</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>LEARNING MODE</strong></td>\r\n   <td> </td>\r\n   <td><strong>@mode_of_program - @mode_of_study</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>DURATION</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td><strong>@duration (IN YEARS)</strong></td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>REGISTRATION DATE</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td>@created_dt </td>\r\n  </tr>\r\n  <tr>\r\n   <td><strong>TIME</strong></td>\r\n   <td><strong>:</strong></td>\r\n   <td>@created_tm</td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>\r\n\r\n<ol>\r\n <li>This offer is subject to the following conditions:\r\n <ol>\r\n  <li> Payable fees on the registration are <strong>@REGISTRATIONFEE</strong> . Kindly refer to the attachment for more details on the tuition fees and other related fees.</li>\r\n  <li>USAS has the right to cancel this offer immediately; if any of the information provided is not true, inaccurate or does not meet the eligibility requirements set by the University.</li>\r\n  <li>If you studying in Malaysia, you are required to bring the original documents on the Registration Day.  If you are on ODL or Online Study Mode, your documents must be Certified True Copy by the Embassy or your previous Education Institute and they must be mailed to our Admission Office. Please be reminded that you are not allowed to change your programme or postpone your registration. This offer will automatically be nullified if you are unable to fulfill our requirement(s).</li>\r\n </ol>\r\n </li>\r\n <li>You are kindly requested to read Appendix A for further details concerning the rules and regulations of admission. If you agree to the terms and conditions stated in the appendix, you are confirm  your acceptance at your application portal  not later than @intake via email to admission.usas @eag.com.my</li>\r\n <li>If you wish to continue your study in Malaysia, kindly be informed that the approval of Student Pass requires minimum of <strong>60-90 days</strong> for processing. Therefore, you are also hereby reminded that prior to the approval of your Student pass, you are advised not to enter Malaysia to avoid any immigration issues that might affect your Student Pass application.</li>\r\n</ol>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p>Yours truly,<br>\r\n </p>\r\n\r\n<p><strong>(MOHD TAJUL SABKI BIN ABDUL LATIB)</strong><br>\r\nDean<br>\r\nAdmission and Record Division Universiti Sultan Azlan Shah</p>\r\n\r\n<p>THIS IS A COMPUTER-GENERATED LETTER. NO SIGNATURE IS REQUIRED.<br>\r\n“INOVASI NADI KESIAGAAN TRANSFORMASI”<br>\r\n </p>\r\n\r\n<p>Name: @studentname (@STUDENTPASSPORT OR @STUDENTNRIC) - kiran on new page</p>\r\n\r\n<p><strong>UNIVERSITI SULTAN AZLAN SHAH</strong></p>\r\n\r\n<p><strong>FEE(S) STRUCTURE:</strong></p>\r\n\r\n<p><strong>**To be paid upfront before the Registration Date</strong></p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p><strong>PAYMENT DETAILS</strong></p>\r\n   </td>\r\n   <td>\r\n   <p><strong>Ringgit Malaysia (RM)</strong></p>\r\n   </td>\r\n   <td>\r\n   <p><strong>Dollars (USD) RM 1 = $0.25 (Subject to change based on currency rate)</strong></p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>International Student EMGS - <strong>If you are studying in Malaysia</strong></p>\r\n\r\n   <ul>\r\n    <li>New Visa Processing Fee (one-off for the 1st Year).</li>\r\n   </ul>\r\n\r\n   <ul>\r\n    <li>Renewal Visa Processing Fee (to be paid the following year and onwards).</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>RM 2079.00</p>\r\n\r\n   <p>RM758.40</p>\r\n\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>$519.75</p>\r\n\r\n   <p>$189.60</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Immigration Fees (Yearly) - <strong>If you are studying in Malaysia</strong></p>\r\n\r\n   <ul>\r\n    <li> Student Pass & Visa Endorsement (Payment upon arrival).</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>RM80.00</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>$20.00</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Personal Bond -<strong> If you are studying in Malaysia</strong></p>\r\n\r\n   <ul>\r\n    <li>The charges for Personal Bond will be charged upon arrival and will be refunded upon completion of studies.</li>\r\n    <li>The Personal Bond will not be refunded should any situation arise where the student is found to have either misused their visa or violated any regulations applicable to International Students in Malaysia.</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>RM1500.00</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>$375.00</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Registration Fee (one-off)</p>\r\n\r\n   <ul>\r\n    <li>Payable in full before registration (non-refundable).</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>@REGISTRATIONFEE</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>@REGISTRATIONFEE / @CURRENCYRATE</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Semester Fee</p>\r\n\r\n   <ul>\r\n    <li> To be paid on second semester and onwards.</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p>@SEMESTERFEE</p>\r\n   </td>\r\n   <td>\r\n   <p>@SEMESTERFEE / @CURRENCYRATE</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Tuition Fee</p>\r\n\r\n   <ul>\r\n    <li> The fee is to be paid by the student each semester, according to the number of credit hours registered</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p>@TUTION FEE X @TOTALCREDITHOURS</p>\r\n\r\n   <p>hours</p>\r\n\r\n   <p>(MIN @MINCREDITHOURS – @MAXCREDITHOURS hours per semester)</p>\r\n   </td>\r\n   <td>\r\n   <p>(@TUTIONFEE/ @CURRENCYRATE) X @TOTALCREDITHOURS hours</p>\r\n\r\n   <p> </p>\r\n\r\n   <p>(MIN @MINCREDITHOURS – @MAXCREDITHOURS hours per semester)</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Accommodation fee (per semester) -<strong> if you are studying in Malaysia and require Accommodation</strong></p>\r\n\r\n   <ul>\r\n    <li>Payment upon arrival.</li>\r\n   </ul>\r\n   </td>\r\n   <td>\r\n   <p>RM 500.00 (OnCampus)</p>\r\n\r\n   <p>RM 800.00 (OffCampus)</p>\r\n   </td>\r\n   <td>\r\n   <p>$ 125.00 (OnCampus) $ 200.00 (OffCampus)</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>MUET (Malaysian University English Test)</p>\r\n   </td>\r\n   <td>\r\n   <p>NIL</p>\r\n   </td>\r\n   <td>\r\n   <p>NIL</p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>\r\n\r\n<p><strong>APPENDIX A - kiran on new page</strong><br>\r\n </p>\r\n\r\n<p><strong>ACCEPTANCE OF OFFER:</strong></p>\r\n\r\n<p>To accept our offer, you will need to submit the following before the offer deadline:</p>\r\n\r\n<ul>\r\n <li>Completed Confirmation of Acceptance at your and payment of initial fee(s) (including the EMGS Visa Processing Fee) outlined in this offer at your Applicnt Portal: http://eag-applicant.camsedu.com/applicantLogin </li>\r\n <li>If your are studying in Malaysia Campus, a list of required documents (filled and completed) - See <strong>CHECKLIST FOR STUDENT’S VISA APPLICATION.</strong></li>\r\n</ul>\r\n\r\n<p> </p>\r\n\r\n<p><strong>GENERAL CONDITIONS:</strong></p>\r\n\r\n<ol>\r\n <li>If you are studying in Malaysia:\r\n <ol>\r\n  <li>you are to be personally present at the University campus during Registration and the Orientation prior to the commencement of your programme. You are also required to report to the Academic, Admission and Record Division on the day after of your arrival. If you would be you be delayed for your enrolment, please notify the Admission Unit in advance.</li>\r\n  <li>For International Student, enrolment and commencement of classes is subject to you obtaining your student pass or a Visa Approval Letter (VAL) from the Immigration Department of Malaysia.</li>\r\n </ol>\r\n </li>\r\n <li>In the unlikely event your programme is cancelled or the University is unable to provide the programme for which you applied for, you will either:\r\n <ol>\r\n  <li>Be given a full refund of all fees paid. Refunds shall be processed and paid within 21 days from notification to you of the cancellation of your programme; or</li>\r\n  <li>At your election, in writing, apply for an alternative programme, for which the</li>\r\n  <li>University shall determine your eligibility and proceed to issue a new offer accordingly.</li>\r\n </ol>\r\n </li>\r\n <li>Enrolment, upon your acceptance of the Offer of Admission and payment of the initial fee(s), may with the approval of the University be deferred to the next intake.</li>\r\n <li>Failure to accept this offer and/or to comply with the provisions for acceptance by the offer deadline shall render this offer to be withdrawn.</li>\r\n</ol>\r\n\r\n<p> </p>\r\n\r\n<p><strong>ABOUT YOUR FEES:</strong></p>\r\n\r\n<p>The semester tuition fee quoted is only applicable to the programme commencing in @inatke Intake: First Semester 2020/2021 Session (the exact date will be informed to you the soonest by email). In the event of a variation between the fees on the offer letter and the approved Universiti Sultan Azlan Shah&#39;s structure of fees, the approved will prevail. For the future years of your programme, the University reserves the right to adjust the annual tuition fees.</p>\r\n\r\n<p> </p>\r\n\r\n<h1><strong>REQUIREMENTS FOR INTERNATIONAL APPLICANTS STUDYING IN MALAYSIA</strong></h1>\r\n\r\n<ol>\r\n <li>APPLICANTS NEED TO APPLY FOR VISA 3 MONTHS PRIOR TO REGISTRATION DATE. APPLICANTS NEED TO BE IN THE CURRENT COUNTRY WHEN THE APPLICATION IS BEING MADE.<br>\r\n  </li>\r\n <li>IN TIME WHEN THE APPLICATION IS BEING MADE, THE PASSPORT MUST VALID MORE THAN 15 MONTHS.<br>\r\n  </li>\r\n <li>AFTER THE APPLICATION IS APPROVED BY THE UNIVERSITY, APPLICANTS WILL BE GIVEN AN OFFER LETTER AND MEDICAL CHECK- UP FORM. IT MUST BE COMPLETED IN THE CURRENT RESIDENTIAL COUNTRY.<br>\r\n  </li>\r\n <li>FOR THE APPLICANTS WHO CURRENTLY RESIDE IN MALAYSIA; ALREADY OWN VISA STUDY OR WORKING PERMIT, EITHER ONE NEEDS TO BE SHORTENED OR REVOKED BY THE INSTITUTION OR THE CURRENT EMPLOYER.<br>\r\n  </li>\r\n <li>UNIVERSITI SULTAN AZLAN SHAH WILL NOT ACCEPT ANY APPLICANTS WHO USES SOCIAL PASS VISA TO BE ENROLLED INTO THE UNIVERSITY.<br>\r\n  </li>\r\n <li>APPLICANTS WILL ONLY BE ALLOWED TO BE ENROLLED INTO UNIVERSITI SULTAN AZLAN SHAH AFTER STUDENT VISA APPLICATION IS APPROVED BY MALAYSIAN IMMIGRATION DEPARTMENT AND EDUCATION MALAYSIAN GLOBAL SERVICES (EMGS).</li>\r\n</ol>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<h1>CHECKLIST FOR STUDENT’S VISA APPLICATION (IF YOU ARE STUDYING IN MALAYSIA)</h1>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>NO</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>MATTER</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n\r\n   <p>?</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>1</p>\r\n   </td>\r\n   <td>\r\n   <p>Application Form</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>2</p>\r\n   </td>\r\n   <td>\r\n   <p>1 copy of all pages of passport</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>3</p>\r\n   </td>\r\n   <td>\r\n   <p>2 copies of passport sized pictures (white background)</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>4</p>\r\n   </td>\r\n   <td>\r\n   <p>Academic transcripts from original country</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>5</p>\r\n   </td>\r\n   <td>\r\n   <p>Translated academic transcripts</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>6</p>\r\n   </td>\r\n   <td>\r\n   <p>Health Declaration Form</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>7</p>\r\n   </td>\r\n   <td>\r\n   <p>Yellow Fever Vaccination Certificate (Countries at High Risk of Yellow Fever Transmission) – See Appendix 1</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p> </p>\r\n\r\n<p><strong>Appendix 1</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>COUNTRIES AT HIGH RISK OF YELLOW FEVER TRANSMISSION</strong></p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td colspan=\"3\">\r\n   <p><strong>AFRICA</strong></p>\r\n   </td>\r\n   <td>\r\n   <p><strong>CENTRAL AND SOUTH AMERICA</strong></p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Angola</p>\r\n   </td>\r\n   <td>\r\n   <p>Equatorial Guinea</p>\r\n   </td>\r\n   <td>\r\n   <p>Mauritania</p>\r\n   </td>\r\n   <td>\r\n   <p>Argentina</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Benin</p>\r\n   </td>\r\n   <td>\r\n   <p>Ethiopia</p>\r\n   </td>\r\n   <td>\r\n   <p>Niger</p>\r\n   </td>\r\n   <td>\r\n   <p>Bolivia</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Burkina Faso</p>\r\n   </td>\r\n   <td>\r\n   <p>Gabon</p>\r\n   </td>\r\n   <td>\r\n   <p>Nigeria</p>\r\n   </td>\r\n   <td>\r\n   <p>Brazil</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Burundi</p>\r\n   </td>\r\n   <td>\r\n   <p>Gambia, The</p>\r\n   </td>\r\n   <td>\r\n   <p>Rwanda</p>\r\n   </td>\r\n   <td>\r\n   <p>Colombia</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Cameroon</p>\r\n   </td>\r\n   <td>\r\n   <p>Ghana</p>\r\n   </td>\r\n   <td>\r\n   <p>Senegal</p>\r\n   </td>\r\n   <td>\r\n   <p>Ecuador</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Central African Republic</p>\r\n   </td>\r\n   <td>\r\n   <p>Guinea</p>\r\n   </td>\r\n   <td>\r\n   <p>Sierra Leone</p>\r\n   </td>\r\n   <td>\r\n   <p>French Guiana</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Chad</p>\r\n   </td>\r\n   <td>\r\n   <p>Guinea-Bissau</p>\r\n   </td>\r\n   <td>\r\n   <p>Sudan</p>\r\n   </td>\r\n   <td>\r\n   <p>Guyana</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Congo, Republic of the</p>\r\n   </td>\r\n   <td>\r\n   <p>Kenya</p>\r\n   </td>\r\n   <td>\r\n   <p>South Sudan</p>\r\n   </td>\r\n   <td>\r\n   <p>Panama</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Côte d’Ivoire</p>\r\n   </td>\r\n   <td>\r\n   <p>Liberia</p>\r\n   </td>\r\n   <td>\r\n   <p>Togo</p>\r\n   </td>\r\n   <td>\r\n   <p>Paraguay</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td rowspan=\"4\">\r\n   <p> </p>\r\n\r\n   <p>Democratic Republic of the Congo</p>\r\n   </td>\r\n   <td rowspan=\"4\">\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p>Mali</p>\r\n   </td>\r\n   <td rowspan=\"4\">\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p>Uganda</p>\r\n   </td>\r\n   <td>\r\n   <p>Peru</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Suriname</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Trinidad and Tobago</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Venezuela</p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p><strong>*</strong>Countries or areas where “a risk of yellow fever transmission is present,” as defined by the World Health Organization, are countries or areas where “yellow fever has been reported currently or in the past, plus vectors and animal reservoirs currently exist” (see the current country list within the International <em>Travel and Health </em>publication (Annex 1) at <a href=\"http://www.who.int/ith/en/index.html)\">www.who.int/ith/en/index.html).</a></p>\r\n\r\n<p><em>Source: Education Malaysia Global Services (EMGS)</em></p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<h2><strong>HEALTH DECLARATION FORM FOR APPLICANTS STUDYING IN MALAYSIA</strong></h2>\r\n\r\n<p>I hereby declare that I am free from the following diseases/conditions:</p>\r\n\r\n<table border=\"1\" cellspacing=\"0\">\r\n <tbody>\r\n  <tr>\r\n   <td rowspan=\"2\">\r\n   <p> </p>\r\n\r\n   <p><strong>ITEMS</strong></p>\r\n   </td>\r\n   <td colspan=\"2\">\r\n   <p>SELF</p>\r\n   </td>\r\n   <td rowspan=\"2\">\r\n   <p>IF NO, PLEASE</p>\r\n\r\n   <p><strong>STATE</strong></p>\r\n   </td>\r\n   <td rowspan=\"17\">\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p> </p>\r\n\r\n   <p>IFYOUHAVE4OUGHY CONSULTATION FOR <strong>ANY OF THE LISTED</strong></p>\r\n\r\n   <p>oiscasEe/cononon, YOU ARE REQUIRED TO SUBMIT YOUR MEDICAL HISTORY/REPORT FROM YOUR TREATING PHYSICIAN TO EDUCATION MALAYSIA <strong>GLOBAL SERVICES </strong>(EMGS) PANEL CLINIC/UNIVERSITY HEALTH CENTRE.</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>YES</p>\r\n   </td>\r\n   <td>\r\n   <p>NO</p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Tuberculosis</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Hepatitis B</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Hepatitis C</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>HIV</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td rowspan=\"5\">\r\n   <p>Dtug uss/abuse of:</p>\r\n\r\n   <ol>\r\n    <li>Opiates</li>\r\n    <li>Cannabinoids</li>\r\n    <li>Amphetamine</li>\r\n    <li>Methamphetamine</li>\r\n   </ol>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Sexually Transmitted Diseases</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Congenital or Inherited Disorder</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Cancer</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Epilepsy</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Psychiatric Illness</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p>Other illness</p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n   <td>\r\n   <p> </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td colspan=\"5\">\r\n   <p>declare that I will submit myself for compulsory Post-Arrival Health Examination as per</p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n\r\n<p>Malaysian regulations. In the event that I should be diagnosed with any condition that deems me UNSUITABLE for studies, I will bear the cost of leaving Malaysia and will adhere to the immigration requirements on the visit pass and exit before the pass expiration, or any deadline given to me whichever is earlier.</p>\r\n\r\n<p>I declare that in the event I should be diagnosed with any conditions that does not require my removal from Malaysia but requires medical treatment and I choose to remain in Malaysia to continue my studies, I will bear any and ali costs relating directly or indirectly towards the medical management of my medical condition.</p>\r\n\r\n<p>I confirm that EMGS Panel Clinic/University Health Centre shall not be responeible in any manner or whatsoever, arising out of EMGS Panel Clinic/University Health Centre certification of my medical status as suitable to study or reside in Malaysia despite the medical condition described above. I further undertake to hold EMGS Panel Clinic/University Health Centre harmless from any loss or liability arising from this decision and agree to indemnify and keep EMGS Panel Clinic/University Health Centre from any loss or liability arising from this decision.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Date (dd/mm/yyyy)</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p>Applicant’s signature</p>\r\n\r\n<p><strong>Name of applicant as </strong>lndlcated in the passport</p>\r\n\r\n<p>Applicant’s passport number</p>\r\n\r\n<p><strong>Kindly ensure all Information requested in this form is complete and updated In English Language.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n', 1, 1, '2020-08-10 06:37:43', NULL, '2020-08-10 06:37:43'),
(2, 'OFFER LETTER UNDERGRADUATE INTERNATIONAL', 'OFFER LETTER UNDERGRADUATE INTERNATIONAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Please note that the above offer is conditional since all students are required to fulfil their English Language Competency Requirement (ELCR). The ELCR requirement must be fulfilled within a period of two years. Failure to comply within two years after registration may result in the withdrawal of this letter of offer and admission into AeU.</p>\r\n\r\n<p> </p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p> </p>\r\n\r\n<p>This offer is valid for TWO years (6 consecutive intakes of January, May or September) only. It will automatically lapse after two years and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p> </p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-08-10 07:23:22', NULL, '2020-08-10 07:23:22'),
(3, 'OFFER LETTER UNDERGRADUATE LOCAL', 'OFFER LETTER UNDERGRADUATE LOCAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p><br>\r\nThis offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-09-01 16:34:08', NULL, '2020-09-01 16:34:08'),
(4, 'OFFER LETTER POSTGRADUATE LOCAL', 'OFFER LETTER POSTGRADUATE LOCAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p><br>\r\nThis offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p> </p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-09-01 16:34:30', NULL, '2020-09-01 16:34:30'),
(5, 'OFFER LETTER POSTGRADUATE INTERNATIONAL', 'OFFER LETTER POSTGRADUATE INTERNATIONAL', '<p>@DATELETTERGENERATED 02 June 2020</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>@APPLICANTFULLNAME DENISSE ANANDA PRATAMA</strong></p>\r\n\r\n<p>&lt;@mail_address1> JLN.TERAMPIL NO.14 KOMP TNI AL KODAMAR KELAPA GADING BARAT 14240,</p>\r\n\r\n<p>&lt;@mailing_zipcode>14240,</p>\r\n\r\n<p>@MAILINGCITY JAKARTA UTARA,</p>\r\n\r\n<p>@MAILINGSTATE DKI JAKARTA, @MAILINGCOUNTRYINDONESIA</p>\r\n\r\n<p> </p>\r\n\r\n<p>Dear Sir/Madam,</p>\r\n\r\n<p> </p>\r\n\r\n<p><strong>OFFER FOR UNDERGRADUATE ADMISSION.</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>It is our pleasure to offer you a place in our undergraduate programme.</p>\r\n\r\n<p>The details of the offer are as follows:</p>\r\n\r\n<p>Programme: @program <strong>BACHELOR OF INFORMATION AND COMMUNICATION TECHNOLOGY (HONS) </strong>&lt;@ACCREDITATION CODE> <strong>(R2-DL/481/6/0482) (A9489) (05/26)</strong></p>\r\n\r\n<p>Intake: @intake <strong>May 2020 Semester</strong></p>\r\n\r\n<p>Commencement Date:   @REGISTRATIONDATE <strong>04 May 2020</strong></p>\r\n\r\n<p>School: @FACULTY <strong>SCHOOL OF SCIENCE & TECHNOLOGY</strong></p>\r\n\r\n<p>Duration of Study: <strong>Shall Normally be </strong>@PROGRAMMINIMUMDURATIONINYEARS 4 <strong>years</strong></p>\r\n\r\n<p>Mode of Study: @mode_of_study <strong>Blended</strong></p>\r\n\r\n<p>Programme Structure:  @PROGRAMMETYPE <strong>Coursework</strong></p>\r\n\r\n<p> </p>\r\n\r\n<p>Please note that the above offer is conditional since all students are required to fulfil their English Language Competency Requirement (ELCR). The ELCR requirement must be fulfilled within a period of one year. You are however allowed to register for both the ELCR and your course subjects simultaneously. Failure to comply within a year after registration may result in the withdrawal of this letter of offer and admission into AeU.</p>\r\n\r\n<p>For verification and record purposes, you are required to submit a certified true copy of citizenship card or passport, two most recent coloured passport size photographs and your academic certificates and transcripts during registration.</p>\r\n\r\n<p>This offer is valid for ONE year (3 consecutive intakes of January, May or September) only. It will automatically lapse after one year and you need to reapply again should you wish to seek admission to any of the programmes of the University.</p>\r\n\r\n<p>The University would like to congratulate and welcome you to the programme in your quest towards academic and career advancement. Should you have any further enquiries, kindly contact Admission & Records Department at 1300-300-238 or email to admission@aeu.edu.my.</p>\r\n\r\n<p>We wish you every success.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>YUSOFF MUSA</p>\r\n\r\n<p>Registrar</p>\r\n\r\n<p>-------------------------------------------------------------------------------------------------------------------------------------------------------------------------.</p>\r\n\r\n<p>                                            *****Computer Generated Document. No Signature Required*****</p>\r\n\r\n<p>                                           ------------------------------------------------------------------------------------------</p>\r\n\r\n<p>Admissions & Records, Asia e University, Wisma Subang Jaya, No 106, Jalan SS15/4, 47500 Subang Jaya, Selangor, Malaysia. Tel No: 1-300-300-238 Fax No: 03-2785 0001 Website: www.aeu.edu.my</p>\r\n', 1, 1, '2020-09-01 16:34:52', NULL, '2020-09-01 16:34:52'),
(6, 'Registration Email', 'Registration Email', '<p>Dear @salutation @studentname,</p>\r\n\r\n<p> </p>\r\n\r\n<p>Welcome to East Asia Global Education. We are an Education Provider that develop and offer educational, academic, and professional programmes in collaboration with outstanding universities and training institutions.</p>\r\n\r\n<p>Please login to view and apply for the programmes offered by our partner universities.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Your login details are as follow:</p>\r\n\r\n<p>URL: http://eag-applicant.camsedu.com/applicantLogin</p>\r\n\r\n<p>Should you have any inquiry or assistance you can chat with our Academic Counsellor after your login to the Portal or email: admission.usas@eag.com.my</p>\r\n', 1, 1, '2021-01-17 15:20:33', NULL, '2021-01-17 15:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template_message`
--

CREATE TABLE `communication_template_message` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `id_university` int(20) DEFAULT 0,
  `id_education_level` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template_message`
--

INSERT INTO `communication_template_message` (`id`, `name`, `subject`, `message`, `id_university`, `id_education_level`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Template One', 'Subject One', '<p>Dara</p>\r\n\r\n<p><strong>Data</strong></p>\r\n', 0, 0, 1, 1, '2020-10-29 11:50:56', NULL, '2020-10-29 11:50:56');

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `tax_no` varchar(120) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `id_vendor` varchar(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_users`
--

CREATE TABLE `company_users` (
  `id` int(20) NOT NULL,
  `id_company` int(20) DEFAULT 0,
  `name` varchar(2048) DEFAULT '',
  `phone` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `designation` varchar(1024) DEFAULT '',
  `user_name` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `id_user_role` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_users`
--

INSERT INTO `company_users` (`id`, `id_company`, `name`, `phone`, `email`, `designation`, `user_name`, `password`, `id_user_role`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Kiran AS', '95381030954', 'askiran123@gmail.com', 'User', '', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, '2021-02-17 07:52:10', NULL, '2021-02-17 07:52:10'),
(2, 2, 'Name', '123', 'un@gmail.com', 'kiran', 'kiran', 'b1a5b64256e27fa5ae76d62b95209ab3', 2, 1, NULL, '2021-02-17 08:16:05', NULL, '2021-02-17 08:16:05'),
(3, 3, 'Vk', '8888888888', 'vk@espeed.com', 'User', 'UN1111', '202cb962ac59075b964b07152d234b70', 2, 1, NULL, '2021-03-15 10:18:09', NULL, '2021-03-15 10:18:09'),
(4, 4, 'testing demo', '23123123123', 'testingdemo@gmail.com', 'demo', '', '1216e71493c50aa6c9a152dd9a83df8a', 2, 1, NULL, '2021-03-15 19:49:12', NULL, '2021-03-15 19:49:12'),
(5, 5, 'Kiran AS', '9538130954', 'sku1@skaya.com', 'User', '', '202cb962ac59075b964b07152d234b70', 2, 1, NULL, '2021-03-15 22:51:37', NULL, '2021-03-15 22:51:37'),
(6, 6, 'alif', '12323123', 'alif@creativebytes.com.my', 'CEO', '', '099a147c0c6bcd34009896b2cc88633c', 2, 1, NULL, '2021-03-20 02:13:29', NULL, '2021-03-20 02:13:29'),
(7, 7, 'Kiran AS', '09538130954', 'skaya1@skaya.com', 'User', '', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, '2021-03-21 03:15:09', NULL, '2021-03-21 03:15:09'),
(8, 8, 'Coffee', '9632587410', 'coffee@espresso.com', 'CEO', '', '6576dbdb7af7705722a2bb3115287e11', 0, 1, NULL, '2021-03-22 19:47:43', NULL, '2021-03-22 19:47:43'),
(9, 9, 'demo user one', '12312312', 'contact@demo.com', 'demo', '', 'fe01ce2a7fbac8fafaed7c982a04e229', 1, 1, NULL, '2021-03-22 20:03:10', NULL, '2021-03-22 20:03:10'),
(10, 8, 'Coffee', '9632587410', 'coffee@espresso.com', 'CEO', 'coffee@espresso.com', '6576dbdb7af7705722a2bb3115287e11', 1, 1, NULL, '2021-03-22 20:42:30', NULL, '2021-03-22 20:42:30'),
(11, 10, 'Purple', '020', 'orange@colours.com', 'CFO', '', '4cfb9bb32761b53bf9b5d85d16364524', 1, 1, NULL, '2021-03-23 08:09:14', NULL, '2021-03-23 08:09:14'),
(12, 11, 'alif', '12312312', 'finaltesting@alif.com', 'alif', '', '099a147c0c6bcd34009896b2cc88633c', 1, 1, NULL, '2021-03-29 00:31:38', NULL, '2021-03-29 00:31:38'),
(13, 12, 'User Name', '8787887878788', 'c@company1.com', 'DEsignation', '', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, '2021-04-03 11:59:55', NULL, '2021-04-03 11:59:55'),
(14, 13, 'Mars', '200', 'mars@g.com', 'CFO', '', '1c6c0a281f435192d0bfc0369f11a02b', 1, 1, NULL, '2021-04-10 10:19:24', NULL, '2021-04-10 10:19:24'),
(15, 14, 'Lily', '7892665', 'Orchid@g.com', 'CFO', '', '4bec4557f80c4f608fe37ed62fde199f', 0, 1, NULL, '2021-04-11 12:05:05', NULL, '2021-04-11 12:05:05'),
(16, 15, 'Tom', '789', 'Jerry@g.com', 'CFO', '', '44f3012b4b22439a7c02ce76eb1d2863', 0, 1, NULL, '2021-04-11 12:10:04', NULL, '2021-04-11 12:10:04'),
(17, 14, 'Tulip', '558', 'tulip@g.com', 'Head', '', '30e4ad00617d944e7e4757333d5bfde3', 1, 1, NULL, '2021-04-11 12:23:07', NULL, '2021-04-11 12:23:07'),
(18, 15, 'Hallah Bakash', '89879989', 'allahbakash@gmail.com', 'DEsignation', '', '202cb962ac59075b964b07152d234b70', 1, 1, NULL, '2021-04-11 12:47:16', NULL, '2021-04-11 12:47:16'),
(19, 16, 'Water', '200', 'air@g', 'CFO', '', 'dcdbaa1ae323ceddf20b04c998d62b45', 1, 1, NULL, '2021-04-16 11:22:48', NULL, '2021-04-16 11:22:48'),
(20, 17, 'Acid', '721', 'hydrogen@g', 'CFO', '', '06ae64742801bdacd0253b5a06cf8c09', 1, 1, NULL, '2021-04-16 11:25:09', NULL, '2021-04-16 11:25:09'),
(21, 17, 'Hydrogen', '120', 'hydrogen@g.com', 'CEO', '', '06ae64742801bdacd0253b5a06cf8c09', 1, 1, NULL, '2021-04-16 11:41:09', NULL, '2021-04-16 11:41:09'),
(22, 18, 'Vinay Kiran', '8942913010', 'itsme@vk.com', 'Designation', '', '94fb192a365267c153785bc570ef3804', 1, 1, NULL, '2021-04-26 12:18:12', NULL, '2021-04-26 12:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `company_user_last_login`
--

CREATE TABLE `company_user_last_login` (
  `id` bigint(20) NOT NULL,
  `id_company_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_user_last_login`
--

INSERT INTO `company_user_last_login` (`id`, `id_company_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-17 07:53:04'),
(2, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-17 07:57:54'),
(3, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-17 08:03:06'),
(4, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-17 08:06:40'),
(5, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '27.59.57.157', 'Chrome 88.0.4324.150', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36', 'Mac OS X', '2021-02-17 08:14:02'),
(6, 2, '{\"id_company\":\"2\",\"company_user_full_name\":\"Name\",\"company_user_name\":\"kiran\",\"company_user_email\":\"un@gmail.com\",\"company_user_phone\":\"123\",\"company_user_designation\":\"kiran\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-17 08:16:38'),
(7, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-17 20:02:13'),
(8, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-18 12:06:16'),
(9, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-18 17:40:23'),
(10, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '117.230.157.79', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-22 10:13:12'),
(11, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-23 19:45:38'),
(12, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-23 22:34:56'),
(13, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '106.217.109.107', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-02-24 08:51:46'),
(14, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-24 21:22:17'),
(15, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-11 10:53:03'),
(16, 3, '{\"id_company\":\"3\",\"company_user_full_name\":\"Vk\",\"company_user_name\":\"UN1111\",\"company_user_email\":\"vk@espeed.com\",\"company_user_phone\":\"8888888888\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '157.45.196.167', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-15 10:20:27'),
(17, 4, '{\"id_company\":\"4\",\"company_user_full_name\":\"testing demo\",\"company_user_name\":\"\",\"company_user_email\":\"testingdemo@gmail.com\",\"company_user_phone\":\"23123123123\",\"company_user_designation\":\"demo\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.15.166', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-15 19:50:55'),
(18, 5, '{\"id_company\":\"5\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"sku1@skaya.com\",\"company_user_phone\":\"9538130954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '157.45.205.174', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-15 22:53:57'),
(19, 5, '{\"id_company\":\"5\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"sku1@skaya.com\",\"company_user_phone\":\"9538130954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '157.45.205.62', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-17 09:32:58'),
(20, 6, '{\"id_company\":\"6\",\"company_user_full_name\":\"alif\",\"company_user_name\":\"\",\"company_user_email\":\"alif@creativebytes.com.my\",\"company_user_phone\":\"12323123\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-20 02:17:20'),
(21, 7, '{\"id_company\":\"7\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"skaya1@skaya.com\",\"company_user_phone\":\"09538130954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '42.105.124.132', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-21 03:18:14'),
(22, 9, '{\"id_company\":\"9\",\"company_user_full_name\":\"demo user one\",\"company_user_name\":\"\",\"company_user_email\":\"contact@demo.com\",\"company_user_phone\":\"12312312\",\"company_user_designation\":\"demo\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-22 20:04:31'),
(23, 10, '{\"id_company\":\"8\",\"company_user_full_name\":\"Coffee\",\"company_user_name\":\"coffee@espresso.com\",\"company_user_email\":\"coffee@espresso.com\",\"company_user_phone\":\"9632587410\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-22 20:44:32'),
(24, 9, '{\"id_company\":\"9\",\"company_user_full_name\":\"demo user one\",\"company_user_name\":\"\",\"company_user_email\":\"contact@demo.com\",\"company_user_phone\":\"12312312\",\"company_user_designation\":\"demo\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-22 20:45:09'),
(25, 9, '{\"id_company\":\"9\",\"company_user_full_name\":\"demo user one\",\"company_user_name\":\"\",\"company_user_email\":\"contact@demo.com\",\"company_user_phone\":\"12312312\",\"company_user_designation\":\"demo\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-22 23:52:39'),
(26, 9, '{\"id_company\":\"9\",\"company_user_full_name\":\"demo user one\",\"company_user_name\":\"\",\"company_user_email\":\"contact@demo.com\",\"company_user_phone\":\"12312312\",\"company_user_designation\":\"demo\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '42.190.18.147', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 00:10:45'),
(27, 11, '{\"id_company\":\"10\",\"company_user_full_name\":\"Purple\",\"company_user_name\":\"\",\"company_user_email\":\"orange@colours.com\",\"company_user_phone\":\"020\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 08:16:03'),
(28, 9, '{\"id_company\":\"9\",\"company_user_full_name\":\"demo user one\",\"company_user_name\":\"\",\"company_user_email\":\"contact@demo.com\",\"company_user_phone\":\"12312312\",\"company_user_designation\":\"demo\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '117.230.167.81', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-23 10:45:07'),
(29, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '117.230.167.81', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-23 11:02:09'),
(30, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '157.45.243.169', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-03-23 15:14:53'),
(31, 6, '{\"id_company\":\"6\",\"company_user_full_name\":\"alif\",\"company_user_name\":\"\",\"company_user_email\":\"alif@creativebytes.com.my\",\"company_user_phone\":\"12323123\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-29 00:30:39'),
(32, 12, '{\"id_company\":\"11\",\"company_user_full_name\":\"alif\",\"company_user_name\":\"\",\"company_user_email\":\"finaltesting@alif.com\",\"company_user_phone\":\"12312312\",\"company_user_designation\":\"alif\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-29 00:35:04'),
(33, 6, '{\"id_company\":\"6\",\"company_user_full_name\":\"alif\",\"company_user_name\":\"\",\"company_user_email\":\"alif@creativebytes.com.my\",\"company_user_phone\":\"12323123\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-01 10:01:13'),
(34, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '157.45.186.39', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-04-02 04:46:33'),
(35, 11, '{\"id_company\":\"10\",\"company_user_full_name\":\"Purple\",\"company_user_name\":\"\",\"company_user_email\":\"orange@colours.com\",\"company_user_phone\":\"020\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.11.105', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-04-02 11:37:46'),
(36, 11, '{\"id_company\":\"10\",\"company_user_full_name\":\"Purple\",\"company_user_name\":\"\",\"company_user_email\":\"orange@colours.com\",\"company_user_phone\":\"020\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.11.105', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-04-02 12:01:56'),
(37, 13, '{\"id_company\":\"12\",\"company_user_full_name\":\"User Name\",\"company_user_name\":\"\",\"company_user_email\":\"c@company1.com\",\"company_user_phone\":\"8787887878788\",\"company_user_designation\":\"DEsignation\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '157.45.254.227', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-04-03 12:07:52'),
(38, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"UN0001\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '157.45.67.182', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-04-05 06:00:51'),
(39, 6, '{\"id_company\":\"6\",\"company_user_full_name\":\"alif\",\"company_user_name\":\"\",\"company_user_email\":\"alif@creativebytes.com.my\",\"company_user_phone\":\"12323123\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D Admin\",\"id_company_user_role\":\"2\"}', '106.206.60.221', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-05 08:25:48'),
(40, 13, '{\"id_company\":\"12\",\"company_user_full_name\":\"User Name\",\"company_user_name\":\"\",\"company_user_email\":\"c@company1.com\",\"company_user_phone\":\"8787887878788\",\"company_user_designation\":\"DEsignation\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '117.230.16.138', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-04-05 10:22:22'),
(41, 14, '{\"id_company\":\"13\",\"company_user_full_name\":\"Mars\",\"company_user_name\":\"\",\"company_user_email\":\"mars@g.com\",\"company_user_phone\":\"200\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.12.61', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2021-04-10 10:24:53'),
(42, 14, '{\"id_company\":\"13\",\"company_user_full_name\":\"Mars\",\"company_user_name\":\"\",\"company_user_email\":\"mars@g.com\",\"company_user_phone\":\"200\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-10 11:31:58'),
(43, 17, '{\"id_company\":\"14\",\"company_user_full_name\":\"Tulip\",\"company_user_name\":\"\",\"company_user_email\":\"tulip@g.com\",\"company_user_phone\":\"558\",\"company_user_designation\":\"Head\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.10.161', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2021-04-11 12:23:19'),
(44, 14, '{\"id_company\":\"13\",\"company_user_full_name\":\"Mars\",\"company_user_name\":\"\",\"company_user_email\":\"mars@g.com\",\"company_user_phone\":\"200\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.10.161', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-11 22:55:43'),
(45, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '117.230.28.183', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-04-12 09:45:47'),
(46, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '42.105.124.154', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-04-12 14:47:35'),
(47, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '157.45.91.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-04-13 01:57:59'),
(48, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '117.230.59.160', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-04-14 00:43:40'),
(49, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '117.230.188.140', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-04-14 01:42:46'),
(50, 14, '{\"id_company\":\"13\",\"company_user_full_name\":\"Mars\",\"company_user_name\":\"\",\"company_user_email\":\"mars@g.com\",\"company_user_phone\":\"200\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-16 11:10:38'),
(51, 21, '{\"id_company\":\"17\",\"company_user_full_name\":\"Hydrogen\",\"company_user_name\":\"\",\"company_user_email\":\"hydrogen@g.com\",\"company_user_phone\":\"120\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.124', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '2021-04-16 11:41:25'),
(52, 14, '{\"id_company\":\"13\",\"company_user_full_name\":\"Mars\",\"company_user_name\":\"\",\"company_user_email\":\"mars@g.com\",\"company_user_phone\":\"200\",\"company_user_designation\":\"CFO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-17 03:59:46'),
(53, 21, '{\"id_company\":\"17\",\"company_user_full_name\":\"Hydrogen\",\"company_user_name\":\"\",\"company_user_email\":\"hydrogen@g.com\",\"company_user_phone\":\"120\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.124', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '2021-04-19 13:38:52'),
(54, 21, '{\"id_company\":\"17\",\"company_user_full_name\":\"Hydrogen\",\"company_user_name\":\"\",\"company_user_email\":\"hydrogen@g.com\",\"company_user_phone\":\"120\",\"company_user_designation\":\"CEO\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-19 21:47:42'),
(55, 1, '{\"id_company\":\"1\",\"company_user_full_name\":\"Kiran AS\",\"company_user_name\":\"\",\"company_user_email\":\"askiran123@gmail.com\",\"company_user_phone\":\"95381030954\",\"company_user_designation\":\"User\",\"company_user_role\":\"l&D User\",\"id_company_user_role\":\"1\"}', '117.230.34.159', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-04-21 00:14:31'),
(56, 22, '{\"id_company\":\"18\",\"company_user_full_name\":\"Vinay Kiran\",\"company_user_name\":\"\",\"company_user_email\":\"itsme@vk.com\",\"company_user_phone\":\"8942913010\",\"company_user_designation\":\"Designation\",\"company_user_role\":\"l&D User\",\"company_image\":\"a1d759d760fa53f62966352c73c85cff.jpeg\",\"id_company_user_role\":\"1\"}', '157.45.11.33', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '2021-04-26 12:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `company_user_role`
--

CREATE TABLE `company_user_role` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '0',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_user_role`
--

INSERT INTO `company_user_role` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'l&D User', 'l&D User', 1, NULL, '2021-02-14 10:42:46', NULL, '2021-02-14 10:42:46'),
(2, 'l&D Admin', 'l&D Admin', 0, NULL, '2021-02-14 10:42:47', NULL, '2021-02-14 10:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `phone_code` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `phone_code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'India', '+91', 1, 1, '2020-07-10 23:22:08', 0, '2020-07-10 23:22:08'),
(2, 'Afghanistan', '+93', 1, 1, '2020-07-10 23:22:19', 0, '2020-07-10 23:22:19'),
(3, 'Albania', '+355', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(4, 'Algeria', '+213', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(5, 'Andorra', '+376', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(6, 'Angola', '+244', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(7, 'Antigua and Barbuda', '+268', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(8, 'Argentina', '+54', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(9, 'Armenia', '+374', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(10, 'Australia', '+61', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(11, 'Austria', '+43', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(12, 'Azerbaijan', '+994', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(13, 'Bahamas', '+242', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(14, 'Bahrain', '+973', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(15, 'Bangladesh', '+880', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(16, 'Barbados', '+246', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(17, 'Belarus', '+375', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(18, 'Belgium', '+32', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(19, 'Belize', '+501', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(20, 'Benin', '+229', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(21, 'Bhutan', '+', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(22, 'Bolivia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(23, 'Bosnia and Herzegovina', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(24, 'Botswana', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(25, 'Brazil', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(26, 'Brunei', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(27, 'Bulgaria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(28, 'Burkina Faso', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(29, 'Burma', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(30, 'Burundi', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(31, 'Cambodia', '+855', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(32, 'Cameroon', '+237', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(33, 'Canada', '+1', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(34, 'Cape Verde', '+238', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(35, 'Central African Republic', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(36, 'Chad', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(37, 'Chile', '+56', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(38, 'China', '+86', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(39, 'Colombia', '+57', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(40, 'Comoros', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(41, 'Congo', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(42, 'Cook Islands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(43, 'Costa Rica', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(44, 'Croatia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(45, 'Cuba', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(46, 'Cyprus', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(47, 'Czech Republic', '+420', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(48, 'Côte d\'Ivoire', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(49, 'Denmark', '+45', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(50, 'Djibouti', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(51, 'Dominica', '+767', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(52, 'Dominican Republic', '+809', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(53, 'East Timor', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(54, 'Ecuador', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(55, 'Egypt', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(56, 'El Salvador', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(57, 'Equatorial Guinea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(58, 'Eritrea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(59, 'Estonia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(60, 'Ethiopia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(61, 'Fiji', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(62, 'Finland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(63, 'France', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(64, 'Gabon', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(65, 'Gambia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(66, 'Georgia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(67, 'Germany', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(68, 'Ghana', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(69, 'Greece', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(70, 'Grenada', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(71, 'Guatemala', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(72, 'Guinea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(73, 'Guinea-Bissau', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(74, 'Guyana', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(75, 'Haiti', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(76, 'Honduras', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(77, 'Hungary', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(78, 'Iceland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(79, 'Abkhazia', '+60', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(80, 'Indonesia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(81, 'Iran', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(82, 'Iraq', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(83, 'Ireland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(84, 'Israel', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(85, 'Italy', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(86, 'Ivory Coast', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(87, 'Jamaica', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(88, 'Japan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(89, 'Jordan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(90, 'Kazakhstan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(91, 'Kenya', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(92, 'Kiribati', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(93, 'Korea, North', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(94, 'Korea, South', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(95, 'Kosovo', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(96, 'Kuwait', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(97, 'Kyrgyzstan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(98, 'Laos', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(99, 'Latvia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(100, 'Lebanon', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(101, 'Lesotho', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(102, 'Liberia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(103, 'Libya', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(104, 'Liechtenstein', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(105, 'Lithuania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(106, 'Luxembourg', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(107, 'Macedonia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(108, 'Madagascar', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(109, 'Malawi', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(110, 'Malaysia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(111, 'Maldives', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(112, 'Mali', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(113, 'Malta', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(114, 'Marshall Islands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(115, 'Mauritania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(116, 'Mauritius', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(117, 'Mexico', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(118, 'Micronesia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(119, 'Moldova', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(120, 'Monaco', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(121, 'Mongolia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(122, 'Montenegro', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(123, 'Morocco', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(124, 'Mozambique', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(125, 'Myanmar / Burma', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(126, 'Nagorno-Karabakh', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(127, 'Namibia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(128, 'Nauru', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(129, 'Nepal', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(130, 'Netherlands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(131, 'New Zealand', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(132, 'Nicaragua', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(133, 'Niger', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(134, 'Nigeria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(135, 'Niue', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(136, 'Northern Cyprus', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(137, 'Norway', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(138, 'Oman', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(139, 'Pakistan', '+92', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(140, 'Palau', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(141, 'Palestine', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(142, 'Panama', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(143, 'Papua New Guinea', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(144, 'Paraguay', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(145, 'Peru', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(146, 'Philippines', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(147, 'Poland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(148, 'Portugal', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(149, 'Qatar', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(150, 'Romania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(151, 'Russia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(152, 'Rwanda', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(153, 'Sahrawi Arab Democratic Republic', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(154, 'Saint Kitts and Nevis', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(155, 'Saint Lucia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(156, 'Saint Vincent and the Grenadines', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(157, 'Samoa', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(158, 'San Marino', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(159, 'Saudi Arabia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(160, 'Senegal', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(161, 'Serbia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(162, 'Seychelles', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(163, 'Sierra Leone', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(164, 'Singapore', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(165, 'Slovakia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(166, 'Slovenia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(167, 'Solomon Islands', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(168, 'Somalia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(169, 'Somaliland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(170, 'South Africa', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(171, 'South Ossetia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(172, 'Spain', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(173, 'Sri Lanka', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(174, 'Sudan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(175, 'Suriname', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(176, 'Swaziland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(177, 'Sweden', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(178, 'Switzerland', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(179, 'Syria', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(180, 'São Tomé and Príncipe', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(181, 'Taiwan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(182, 'Tajikistan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(183, 'Tanzania', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(184, 'Thailand', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(185, 'Timor-Leste / East Timor', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(186, 'Togo', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(187, 'Tonga', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(188, 'Trinidad and Tobago', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(189, 'Tunisia', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(190, 'Turkey', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(191, 'Turkmenistan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(192, 'Tuvalu', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(193, 'Uganda', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(194, 'Ukraine', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(195, 'United Arab Emirates', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(196, 'United Kingdom', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(197, 'United States', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(198, 'Uruguay', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(199, 'Uzbekistan', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(200, 'Vanuatu', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(201, 'Vatican City', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(202, 'Venezuela', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(203, 'Vietnam', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(204, 'Yemen', NULL, 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(205, 'Zambia', '+', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05'),
(206, 'Zimbabwe', '+', 1, 1, '2020-08-27 13:08:05', 0, '2020-08-27 13:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_setup`
--

INSERT INTO `currency_setup` (`id`, `code`, `name`, `name_optional_language`, `prefix`, `suffix`, `decimal_place`, `default`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'MYR', 'MALAYSIA RINGGIT', 'Ringgit Malaysia', 'MYR', 'MYR', 2, 1, 1, 1, '2020-08-09 01:58:47', NULL, '2020-08-09 01:58:47'),
(2, 'USD', 'US Dollar', 'US Dollar', 'USD', 'USD', 2, 0, 1, 1, '2020-08-20 09:24:33', NULL, '2020-08-20 09:24:33'),
(3, 'LKR', 'SRI LANKA RUPEE', 'SRI LANKA RUPEE', 'LKR', 'LKR', 2, 0, 1, 1, '2020-09-02 00:44:50', NULL, '2020-09-02 00:44:50');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_mode`
--

CREATE TABLE `delivery_mode` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivery_mode`
--

INSERT INTO `delivery_mode` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Online', 1, NULL, '2021-02-25 23:13:26', NULL, '2021-02-25 23:13:26'),
(2, 'Blended', 1, NULL, '2021-02-25 23:13:26', NULL, '2021-02-25 23:13:26');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(20) NOT NULL,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Faculty of Management & Information Technology', 'FMIT', 'Faculty of Management & Information Technology', 1, NULL, '2020-07-10 23:27:51', NULL, '2020-07-10 23:27:51'),
(2, 'Finance', 'F&A', 'Accounts & Finance', 1, NULL, '2020-11-13 09:22:34', NULL, '2020-11-13 09:22:34'),
(3, 'Admission and Record Division', 'USAS_REG', 'Admission and Record Division', 0, NULL, '2021-01-19 01:03:47', NULL, '2021-01-19 01:03:47');

-- --------------------------------------------------------

--
-- Table structure for table `department_has_staff`
--

CREATE TABLE `department_has_staff` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_has_staff`
--

INSERT INTO `department_has_staff` (`id`, `id_staff`, `id_department`, `role`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, 'DEAN', '2020-08-10 00:00:00', NULL, NULL, '2020-08-10 01:36:44', NULL, '2020-08-10 01:36:44'),
(5, 1, 2, 'DEAN', '2020-11-01 00:00:00', NULL, NULL, '2020-11-13 09:24:34', NULL, '2020-11-13 09:24:34');

-- --------------------------------------------------------

--
-- Table structure for table `description`
--

CREATE TABLE `description` (
  `id` int(20) NOT NULL,
  `name` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `description`
--

INSERT INTO `description` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Received From GRN', 0, NULL, '2021-05-22 01:41:11', NULL, ''),
(2, 'To Assembly Distribution ', 0, NULL, '2021-05-22 01:42:14', NULL, ''),
(3, 'From Assembly Return', 0, NULL, '2021-05-22 01:42:14', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `disposal_type`
--

CREATE TABLE `disposal_type` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `financial_year`
--

CREATE TABLE `financial_year` (
  `id` int(20) NOT NULL,
  `year` varchar(20) DEFAULT '',
  `name` varchar(120) DEFAULT '',
  `to_year` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `frequency_mode`
--

CREATE TABLE `frequency_mode` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `frequency_mode`
--

INSERT INTO `frequency_mode` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'APPLICATION', 'APPLICATION', 'APPLICATION', 1, NULL, '2020-07-11 00:22:32', NULL, '2020-07-11 00:22:32'),
(3, 'PER SEMESTER', 'PER SEMESTER', 'PER SEMESTER', 1, NULL, '2020-07-11 00:23:20', NULL, '2020-07-11 00:23:20'),
(5, 'ONE TIME', 'ONE TIME', 'ONE TIME', 1, NULL, '2020-08-20 10:03:14', NULL, '2020-08-20 10:03:14'),
(6, 'PER INSTALLMENT', 'PER INSTALLMENT', 'PER INSTALLMENT', 1, NULL, '2020-08-22 21:56:27', NULL, '2020-08-22 21:56:27'),
(7, 'EVERY SEMESTER', '', 'EVERY SEMESTER', 1, NULL, '2020-12-18 07:41:07', NULL, '2020-12-18 07:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `grn`
--

CREATE TABLE `grn` (
  `id` int(20) NOT NULL,
  `id_po` int(20) DEFAULT 0,
  `grn_number` varchar(256) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `id_vendor` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grn`
--

INSERT INTO `grn` (`id`, `id_po`, `grn_number`, `description`, `id_vendor`, `total_amount`, `paid_amount`, `balance_amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'GR000001/2021', 'Grn Added ', 1, 247110.41, 0.00, 247110.41, 7, 1, '2021-05-21 13:55:53', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `grn_details`
--

CREATE TABLE `grn_details` (
  `id` int(20) NOT NULL,
  `id_grn` int(20) DEFAULT 0,
  `id_po_detail` int(20) DEFAULT 0,
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `total_quantity` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grn_details`
--

INSERT INTO `grn_details` (`id`, `id_grn`, `id_po_detail`, `id_category`, `id_sub_category`, `id_item`, `total_quantity`, `quantity`, `balance_quantity`, `price`, `total_price`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 2, 5, 4, 10, 8, 2, 30000.00, 240000.00, 1, 1, '2021-05-21 13:55:53', NULL, ''),
(2, 1, 2, 2, 3, 3, 4, 4, 0, 1000.00, 4000.00, 1, 1, '2021-05-21 13:55:53', NULL, ''),
(3, 1, 3, 1, 1, 2, 3000, 2880, 120, 1.08, 3110.40, 1, 1, '2021-05-21 13:55:53', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `city` varchar(512) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `name`, `code`, `email`, `phone`, `gender`, `nric`, `mobile`, `address_one`, `address_two`, `city`, `country`, `state`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Bosch', 'M00001-2021', '', '', '', '', '', '', '', '', '', '', '', 1, 1, '2021-05-21 13:36:29', NULL, ''),
(2, 'Hitachi', 'M00002-2021', '', '', '', '', '', '', '', '', '', '', '', 1, 1, '2021-05-21 13:36:44', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(20) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `parent_name` varchar(200) DEFAULT NULL,
  `order` int(20) DEFAULT NULL,
  `controller` varchar(200) DEFAULT NULL,
  `action` varchar(200) DEFAULT NULL,
  `parent_order` int(20) DEFAULT NULL,
  `id_module` varchar(20) DEFAULT NULL,
  `status` int(20) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`, `parent_name`, `order`, `controller`, `action`, `parent_order`, `id_module`, `status`) VALUES
(1, 'User', 'Setup', 'General Setup', 1, 'user', 'list', 1, '1', 1),
(2, 'Role', 'Setup', 'General Setup', 2, 'role', 'list', 1, '1', 1),
(3, 'Permission', 'Setup', 'General Setup', 3, 'permission', 'list', 1, '1', 1),
(4, 'Salutation', 'Setup', 'General Setup', NULL, 'salutation', 'list', 1, '1', 1),
(5, 'Country', 'Setup', 'country', NULL, 'country', 'list', NULL, '1', 1),
(6, 'State', 'Setup', 'state', NULL, 'state', 'list', NULL, '1', 1),
(12, 'Nationality', 'Setup', 'Demographic', 3, 'nationality', 'list', 2, '1', 1),
(13, 'Department', 'Setup', 'Organisation Setup', 3, 'department', 'list', 3, '1', 1),
(14, 'Organisation', 'Setup', 'Organisation Setup', 1, 'organisation', 'edit', 3, '1', 1),
(266, 'Category', 'Procurement', 'Setup', 1, 'procurementCategory', 'list', 1, '2', 1),
(267, 'Sub Category', 'Procurement', 'Setup', 2, 'procurementSubCategory', 'list', 1, '2', 1),
(268, 'Product', 'Procurement', 'Setup', 3, 'procurementItem', 'list', 1, '2', 1),
(269, 'Vendor Registration', 'Procurement', 'User Setup', 1, 'vendor', 'list', 2, '2', 1),
(270, 'Assembly Team', 'Procurement', 'User Setup', 2, 'assemblyTeam', 'list', 2, '2', 1),
(271, 'Purchase Order', 'Procurement', 'Purchase Order', 1, 'poEntry', 'list', 3, '2', 1),
(272, 'GRN Entry', 'Procurement', 'GRN', 1, 'grn', 'list', 4, '2', 1),
(273, 'Listing Product', 'Procurement', 'Product Listing', 1, 'productListing', 'list', 7, '2', 1),
(274, 'Assembly Distribution', 'Procurement', 'Distribution', 1, 'assemblyDistribution', 'list', 5, '2', 1),
(275, 'Manufacturer', 'Procurement', 'User Setup', 3, 'manufacturer', 'list', 2, '2', 1),
(276, 'Assembly Return', 'Procurement', 'Product Returns', 1, 'assemblyReturn', 'list', 6, '2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `module_name`
--

CREATE TABLE `module_name` (
  `id` int(20) NOT NULL,
  `module_name` varchar(200) DEFAULT NULL,
  `module_short_code` varchar(100) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_name`
--

INSERT INTO `module_name` (`id`, `module_name`, `module_short_code`, `status`) VALUES
(1, 'Setup', NULL, '1'),
(2, 'Procurement', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `id` bigint(20) NOT NULL,
  `name` varchar(2048) NOT NULL,
  `code` varchar(2048) NOT NULL,
  `country` varchar(2048) NOT NULL,
  `person` varchar(2048) NOT NULL,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nationality`
--

INSERT INTO `nationality` (`id`, `name`, `code`, `country`, `person`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysian', '', '', '', 1, NULL, '2021-01-17 09:15:28', NULL, '2021-01-17 09:15:28'),
(8, 'Afghan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(9, 'Albanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(10, 'Algerian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(11, 'American', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(12, 'Andorran', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(13, 'Angolan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(14, 'Anguillan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(15, 'Argentine', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(16, 'Armenian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(17, 'Australian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(18, 'Austrian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(19, 'Azerbaijani', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(20, 'Bahamian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(21, 'Bahraini', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(22, 'Bangladeshi', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(23, 'Barbadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(24, 'Belarusian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(25, 'Belgian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(26, 'Belizean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(27, 'Beninese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(28, 'Bermudian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(29, 'Bhutanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(30, 'Bolivian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(31, 'Botswanan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(32, 'Brazilian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(33, 'British', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(34, 'British Virgin Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(35, 'Bruneian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(36, 'Bulgarian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(37, 'Burkinan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(38, 'Burmese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(39, 'Burundian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(40, 'Cambodian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(41, 'Cameroonian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(42, 'Canadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(43, 'Cape Verdean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(44, 'Cayman Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(45, 'Central African', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(46, 'Chadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(47, 'Chilean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(48, 'Chinese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(49, 'Citizen of Antigua and Barbuda', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(50, 'Citizen of Bosnia and Herzegovina', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(51, 'Citizen of Guinea-Bissau', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(52, 'Citizen of Kiribati', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(53, 'Citizen of Seychelles', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(54, 'Citizen of the Dominican Republic', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(55, 'Citizen of Vanuatu', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(56, 'Colombian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(57, 'Comoran', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(58, 'Congolese (Congo)', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(59, 'Congolese (DRC)', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(60, 'Cook Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(61, 'Costa Rican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(62, 'Croatian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(63, 'Cuban', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(64, 'Cymraes', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(65, 'Cymro', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(66, 'Cypriot', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(67, 'Czech', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(68, 'Danish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(69, 'Djiboutian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(70, 'Dominican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(71, 'Dutch', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(72, 'East Timorese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(73, 'Ecuadorean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(74, 'Egyptian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(75, 'Emirati', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(76, 'English', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(77, 'Equatorial Guinean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(78, 'Eritrean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(79, 'Estonian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(80, 'Ethiopian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(81, 'Faroese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(82, 'Fijian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(83, 'Filipino', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(84, 'Finnish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(85, 'French', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(86, 'Gabonese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(87, 'Gambian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(88, 'Georgian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(89, 'German', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(90, 'Ghanaian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(91, 'Gibraltarian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(92, 'Greek', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(93, 'Greenlandic', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(94, 'Grenadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(95, 'Guamanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(96, 'Guatemalan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(97, 'Guinean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(98, 'Guyanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(99, 'Haitian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(100, 'Honduran', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(101, 'Hong Konger', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(102, 'Hungarian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(103, 'Icelandic', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(104, 'Indian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(105, 'Indonesian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(106, 'Iranian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(107, 'Iraqi', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(108, 'Irish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(109, 'Israeli', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(110, 'Italian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(111, 'Ivorian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(112, 'Jamaican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(113, 'Japanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(114, 'Jordanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(115, 'Kazakh', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(116, 'Kenyan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(117, 'Kittitian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(118, 'Kosovan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(119, 'Kuwaiti', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(120, 'Kyrgyz', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(121, 'Lao', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(122, 'Latvian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(123, 'Lebanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(124, 'Liberian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(125, 'Libyan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(126, 'Liechtenstein citizen', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(127, 'Lithuanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(128, 'Luxembourger', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(129, 'Macanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(130, 'Macedonian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(131, 'Malagasy', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(132, 'Malawian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(134, 'Maldivian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(135, 'Malian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(136, 'Maltese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(137, 'Marshallese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(138, 'Martiniquais', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(139, 'Mauritanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(140, 'Mauritian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(141, 'Mexican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(142, 'Micronesian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(143, 'Moldovan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(144, 'Monegasque', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(145, 'Mongolian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(146, 'Montenegrin', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(147, 'Montserratian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(148, 'Moroccan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(149, 'Mosotho', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(150, 'Mozambican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(151, 'Namibian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(152, 'Nauruan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(153, 'Nepalese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(154, 'New Zealander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(155, 'Nicaraguan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(156, 'Nigerian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(157, 'Nigerien', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(158, 'Niuean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(159, 'North Korean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(160, 'Northern Irish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(161, 'Norwegian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(162, 'Omani', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(163, 'Pakistani', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(164, 'Palauan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(165, 'Palestinian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(166, 'Panamanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(167, 'Papua New Guinean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(168, 'Paraguayan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(169, 'Peruvian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(170, 'Pitcairn Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(171, 'Polish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(172, 'Portuguese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(173, 'Prydeinig', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(174, 'Puerto Rican', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(175, 'Qatari', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(176, 'Romanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(177, 'Russian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(178, 'Rwandan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(179, 'Salvadorean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(180, 'Sammarinese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(181, 'Samoan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(182, 'Sao Tomean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(183, 'Saudi Arabian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(184, 'Scottish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(185, 'Senegalese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(186, 'Serbian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(187, 'Sierra Leonean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(188, 'Singaporean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(189, 'Slovak', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(190, 'Slovenian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(191, 'Solomon Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(192, 'Somali', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(193, 'South African', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(194, 'South Korean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(195, 'South Sudanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(196, 'Spanish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(197, 'Sri Lankan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(198, 'St Helenian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(199, 'St Lucian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(200, 'Stateless', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(201, 'Sudanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(202, 'Surinamese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(203, 'Swazi', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(204, 'Swedish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(205, 'Swiss', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(206, 'Syrian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(207, 'Taiwanese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(208, 'Tajik', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(209, 'Tanzanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(210, 'Thai', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(211, 'Togolese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(212, 'Tongan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(213, 'Trinidadian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(214, 'Tristanian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(215, 'Tunisian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(216, 'Turkish', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(217, 'Turkmen', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(218, 'Turks and Caicos Islander', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(219, 'Tuvaluan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(220, 'Ugandan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(221, 'Ukrainian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(222, 'Uruguayan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(223, 'Uzbek', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(224, 'Vatican citizen', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(225, 'Venezuelan', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(226, 'Vietnamese', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(227, 'Vincentian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(228, 'Wallisian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(229, 'Welsh', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(230, 'Yemeni', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(231, 'Zambian', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35'),
(232, 'Zimbabwean', '', '', '', 1, NULL, '2021-01-17 09:15:35', NULL, '2021-01-17 09:15:35');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL,
  `type` varchar(1024) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `type`, `name`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Student', 'Attention For all To Attend Seminar', 'Attention For all To Attend Seminar', 1, 1, '2021-01-18 11:29:19', NULL, '2021-01-18 11:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `organisation`
--

CREATE TABLE `organisation` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation`
--

INSERT INTO `organisation` (`id`, `name`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'SPEED TRAINING SDN BHD', 'SPEED', 'SPEED', 'http://speed.aeu.edu.my/', 1, 1, '2021-01-01 00:00:00', 2147483647, 'sheila.cheng@aeu.edu.my', 'Wisma Subang Jaya, No.106,', 'Jalan SS 15/4, ', 5, 'Subang Jaya', 47500, '33ba34ceddd6433e85befb5c978f1e09.svg', 1, 1, '2020-07-10 23:25:36', NULL, '2020-07-10 23:25:36');

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', '', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `id_menu` int(20) DEFAULT 0,
  `module` varchar(512) DEFAULT '',
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `id_menu`, `module`, `code`, `description`, `status`) VALUES
(7, 3, 'Setup', 'permission.add', 'Add Permission', 1),
(8, 3, 'Setup', 'permission.list', 'View Permission', 1),
(9, 3, 'Setup', 'permission.edit', 'Edit Permission', 1),
(10, 1, 'Setup', 'user.edit', 'Edit User', 1),
(11, 1, 'Setup', 'user.add', 'Add User', 1),
(12, 1, 'Setup', 'user.list', 'View User', 1),
(13, 2, 'Setup', 'role.list', 'View Role', 1),
(14, 2, 'Setup', 'role.add', 'Add Role', 1),
(15, 2, 'Setup', 'role.edit', 'Edit Role', 1),
(70, 4, 'Setup', 'salutation.list', 'Salutation List', 1),
(71, 4, 'Setup', 'salutation.add', 'Salutation Add', 1),
(72, 4, 'Setup', 'salutation.edit', 'Salutation Edit', 1),
(73, 5, 'Setup', 'country.list', 'Country List', 1),
(74, 5, 'Setup', 'country.add', 'Country Add', 1),
(75, 5, 'Setup', 'country.edit', 'Country Edit', 1),
(76, 6, 'Setup', 'state.list', 'State List', 1),
(77, 7, 'Setup', 'education_level.list', 'Education Level List', 1),
(78, 7, 'Setup', 'education_level.add', 'Education Level Add', 1),
(79, 7, 'Setup', 'education_level.edit', 'Education Level Edit', 1),
(80, 8, 'Setup', 'academic_year.list', 'Academic Year List', 1),
(81, 8, 'Setup', 'academic_year.add', 'Academic Year Add', 1),
(82, 8, 'Setup', 'academic_year.edit', 'Academic Year Edit', 1),
(86, 10, 'Setup', 'race.list', 'Race List', 1),
(87, 10, 'Setup', 'race.add', 'Race Add', 1),
(88, 10, 'Setup', 'race.edit', 'Race Edit', 1),
(89, 11, 'Setup', 'religion.list', 'Religion List', 1),
(90, 11, 'Setup', 'religion.add', 'Religion Add', 1),
(91, 11, 'Setup', 'religion.edit', 'Religion Edit', 1),
(92, 12, 'Setup', 'nationality.list', 'Nationality List', 1),
(93, 12, 'Setup', 'nationality.add', 'Nationality Add', 1),
(94, 12, 'Setup', 'nationality.edit', 'Nationality Edit', 1),
(95, 13, 'Setup', 'department.list', 'Department List', 1),
(96, 13, 'Setup', 'department.add', 'Department Add', 1),
(97, 13, 'Setup', 'department.edit', 'Department Edit', 1),
(98, 14, 'Setup', 'organisation.edit', 'Organisation Edit', 1),
(105, 17, 'Setup', 'faculty_program.list', 'Faculty Program List', 1),
(106, 17, 'Setup', 'faculty_program.add', 'Faculty Program Add', 1),
(107, 17, 'Setup', 'faculty_program.edit', 'Faculty Program Edit', 1),
(108, 18, 'Setup', 'course_description.list', 'course Major / Minor List', 1),
(109, 18, 'Setup', 'course_description.add', 'course Major / Minor Add', 1),
(110, 18, 'Setup', 'course_description.edit', 'course Major / Minor Edit', 1),
(111, 19, 'Setup', 'course_type.list', 'Course Type list', 1),
(112, 19, 'Setup', 'course_type.add', 'Course Type Add', 1),
(113, 19, 'Setup', 'course_type.edit', 'Course Type Edit', 1),
(126, 24, 'Setup', 'scheme.list', 'Scheme List', 1),
(127, 24, 'Setup', 'scheme.add', 'Scheme Add', 1),
(128, 24, 'Setup', 'scheme.edit', 'Scheme Edit', 1),
(153, 33, 'Setup', 'documents_program.list', 'Documents Reuired Program List', 1),
(154, 33, 'Setup', 'documents_program.add', 'Documents Reuired Program Add', 1),
(155, 33, 'Setup', 'documents_program.edit', 'Documents Reuired Program Edit', 1),
(156, 34, 'Setup', 'staff.list', 'Staff List', 1),
(157, 34, 'Setup', 'staff.add', 'Staff Add', 1),
(158, 34, 'Setup', 'staff.edit', 'Staff Edit', 1),
(165, 37, 'Setup', 'report.student_by_intake', 'Report Student By Intake', 1),
(166, 38, 'Setup', 'report.applicant_lead_student', 'Report Applicant Lead Student', 1),
(167, 39, 'Setup', 'report.course_wise_data', 'Report Course Wise Data', 1),
(168, 40, 'Admission', 'group_setup.list', 'Group Setup List', 1),
(169, 40, 'Admission', 'group_setup.add', 'Group Setup Add', 1),
(170, 40, 'Admission', 'group_setup.edit', 'Group Setup Edit', 1),
(171, 41, 'Admission', 'specialization_setup.list', 'Specialization List', 1),
(172, 41, 'Admission', 'specialization_setup.add', 'Specialization Add', 1),
(173, 41, 'Admission', 'specialization_setup.edit', 'Specialization Edit', 1),
(174, 42, 'Admission', 'applicant.list', 'Applicant List', 1),
(175, 42, 'Admission', 'applicant.view', 'Applicant View', 1),
(176, 43, 'Admission', 'apel_approval.list', 'Apel Approval List', 1),
(177, 43, 'Admission', 'apel_approval.approve', 'Apel Approve', 1),
(178, 44, 'Admission', 'sibbling_discount_approval.list', 'Sibbling Discount Approval List', 1),
(179, 44, 'Admission', 'sibbling_discount_approval.approve', 'Sibbling Discount Approve', 1),
(180, 45, 'Admission', 'employee_discount_approval.list', 'Employee Discount Approval List', 1),
(181, 45, 'Admission', 'employee_discount_approval.approve', 'Employee Discount Approve', 1),
(182, 46, 'Admission', 'alumni_discount_approval.list', 'Alumni Discount Approval List', 1),
(183, 46, 'Admission', 'alumni_discount_approval.approve', 'Alumni Discount Approve', 1),
(184, 47, 'Admission', 'applicant_approval.list', 'Applicant Approval List', 1),
(185, 47, 'Admission', 'applicant_approval.approve', 'Applicant Approve', 1),
(186, 48, 'Registration', 'student_approval.list', 'Student Approval List', 1),
(187, 48, 'Registration', 'student_approval.approve', 'Student Approval Approve', 1),
(188, 49, 'Registration', 'advisor_tagging.add', 'Academic Advisor Tagging Add/ Edit', 1),
(189, 50, 'Registration', 'course_registration.list', 'Course Registration List', 1),
(190, 50, 'Registration', 'course_registration.add', 'Course Registration Add', 1),
(191, 50, 'Registration', 'course_registration.view', 'Course Registration View', 1),
(192, 51, 'Registration', 'bulk_withdraw.list', 'Bulk Withdraw List', 1),
(193, 51, 'Registration', 'bulk_withdraw.add', 'Bulk Withdraw Add', 1),
(194, 51, 'Registration', 'bulk_withdraw.view', 'Bulk Withdraw View', 1),
(195, 52, 'Registration', 'credit_transfer.list', 'Credit Transfer / Excemption Application List', 1),
(196, 52, 'Registration', 'credit_transfer.add', 'Credit Transfer / Excemption Application Add', 1),
(197, 52, 'Registration', 'credit_transfer.view', 'Credit Transfer / Excemption Application View', 1),
(198, 52, 'Registration', 'credit_transfer.approval_list', 'Credit Transfer / Excemption Application Approval List', 1),
(199, 52, 'Registration', 'credit_transfer.approve', 'Credit Transfer / Excemption Application Approve', 1),
(200, 54, 'Registration', 'attendence_setup.list', 'Attendence Setup List', 1),
(201, 54, 'Registration', 'attendence_setup.add', 'Attendence Setup Add', 1),
(202, 54, 'Registration', 'attendence_setup.view', 'Attendence Setup View', 1),
(203, 55, 'Registration', 'report.course_count', 'Report Course Count', 1),
(204, 56, 'Records', 'barring_type.list', ' Barring Type List', 1),
(205, 56, 'Records', 'barring_type.add', ' Barring Type Add', 1),
(206, 56, 'Records', 'barring_type.edit', ' Barring Type Edit', 1),
(207, 57, 'Records', 'barring.list', 'Barring List', 1),
(208, 57, 'Records', 'barring.add', 'Barring Add', 1),
(209, 57, 'Records', 'barring.edit', 'Barring Edit', 1),
(210, 58, 'Records', 'release.list', 'Release List', 1),
(211, 58, 'Records', 'release.add', 'Release Add', 1),
(212, 58, 'Records', 'release.dedit', 'Release Edit', 1),
(213, 59, 'Records', 'student.list', 'Student List', 1),
(214, 59, 'Records', 'student.edit', 'Student Edit', 1),
(215, 60, 'Records', 'student_record.list', 'Student Record List', 1),
(216, 60, 'Records', 'student_record.view', 'Student Record View', 1),
(217, 61, 'Records', 'change_status.list', 'Change Status List', 1),
(218, 61, 'Records', 'change_status.add', 'Change Status Add', 1),
(219, 61, 'Records', 'change_status.edit', 'Change Status Edit', 1),
(220, 62, 'Records', 'apply_change_status.list', 'Apply Change Status List', 1),
(221, 62, 'Records', 'apply_change_status.add', 'Apply Change Status Add', 1),
(222, 62, 'Records', 'apply_change_status.view', 'Apply Change Status View', 1),
(223, 63, 'Records', 'apply_change_status.approval_list', 'Apply Change Status Approval List', 1),
(224, 63, 'Records', 'apply_change_status.approve', 'Apply Change Status Approve', 1),
(225, 64, 'Records', 'visa_details.student_list', 'Visa Details Student List', 1),
(226, 64, 'Records', 'visa_details.add', 'Visa Details Student Add', 1),
(227, 65, 'Records', 'apply_change_programme.list', 'Apply Change Programme List', 1),
(228, 65, 'Records', 'apply_change_programme.add', 'Apply Change Programme Add', 1),
(229, 65, 'Records', 'apply_change_programme.view', 'Apply Change Programme View', 1),
(230, 66, 'Records', 'apply_change_programme.approve', 'Apply Change Programme Approve', 1),
(231, 66, 'Records', 'apply_change_programme.approval_list', 'Apply Change Programme Approval List', 1),
(232, 67, 'Records', 'apply_change_scheme.list', 'Apply Change Scheme List', 1),
(233, 67, 'Records', 'apply_change_scheme.add', 'Apply Change Scheme Add', 1),
(234, 67, 'Records', 'apply_change_scheme.view', 'Apply Change Scheme View', 1),
(235, 68, 'Records', 'apply_change_scheme.approval_list', 'Apply Change Scheme Approval List', 1),
(236, 68, 'Records', 'apply_change_scheme.approve', 'Apply Change Scheme Approve', 1),
(237, 69, 'Records', 'change_branch.list', 'Change Branch List', 1),
(238, 69, 'Records', 'change_branch.add', 'Change Branch Add', 1),
(239, 69, 'Records', 'change_branch.view', 'Change Branch View', 1),
(240, 70, 'Records', 'change_branch.approval_list', 'Change Branch Approval List', 1),
(241, 70, 'Records', 'change_branch.approve', 'Change Branch Approve', 1),
(242, 71, 'Examination', 'exam_configuration.edit', 'Exam Configuration Edit', 1),
(243, 72, 'Examination', 'exam_configuration.edit', 'Exam Configuration Edit', 1),
(244, 73, 'Examination', 'gpa_cgpa.list', 'Gpa Cgpa List', 1),
(245, 73, 'Examination', 'gpa_cgpa.add', 'Gpa Cgpa Add', 1),
(246, 73, 'Examination', 'gpa_cgpa.edit', 'Gpa Cgpa Edit', 1),
(247, 74, 'Examination', 'assesment_type.list', 'Assesment Type List', 1),
(248, 74, 'Examination', 'assesment_type.add', 'Assesment Type Add', 1),
(249, 74, 'Examination', 'assesment_type.edit', 'Assesment Type Edit', 1),
(250, 75, 'Examination', 'grade_setup.list', 'Grade Setup List', 1),
(251, 75, 'Examination', 'grade_setup.add', 'Grade Setup Add', 1),
(252, 75, 'Examination', 'grade_setup.edit', 'Grade Setup Edit', 1),
(253, 76, 'Examination', 'course_grade.list', 'Course Grade List', 1),
(254, 76, 'Examination', 'course_grade.add', 'Course Grade Add', 1),
(255, 76, 'Examination', 'course_grade.edit', 'Course Grade Edit', 1),
(256, 77, 'Examination', 'publish_exam_result.list', 'Publish Exam Result List', 1),
(257, 77, 'Examination', 'publish_exam_result.add', 'Publish Exam Result Add', 1),
(258, 77, 'Examination', 'publish_exam_result.edit', 'Publish Exam Result Edit', 1),
(259, 78, 'Examination', 'location.list', 'Exam Locaction Setup', 1),
(260, 78, 'Examination', 'location.add', 'Exam Locaction Add', 1),
(261, 78, 'Examination', 'location.edit', 'Exam Locaction Edit', 1),
(262, 79, 'Examination', 'exam_center.list', 'Exam Center List', 1),
(263, 79, 'Examination', 'exam_center.add', 'Exam Center Add', 1),
(264, 79, 'Examination', 'exam_center.edit', 'Exam Center Edit', 1),
(265, 80, 'Examination', 'exam_event.list', 'Exam Event List', 1),
(266, 80, 'Examination', 'exam_event.add', 'Exam Event Add', 1),
(267, 80, 'Examination', 'exam_event.edit', 'Exam Event Edit', 1),
(268, 81, 'Examination', 'exam_registration.list', 'Exam Registration List', 1),
(269, 81, 'Examination', 'exam_registration.add', 'Exam Registration Add', 1),
(270, 82, 'Examination', 'exam_registration.tag_student', 'Exam Registration Tag Student', 1),
(271, 82, 'Examination', 'exam_registration.tag_list', 'Exam Registration Tag List', 1),
(272, 83, 'Examination', 'exam_slip.list', 'Release Exam Slip List', 1),
(273, 83, 'Examination', 'exam_slip.add', 'Release Exam Slip Add', 1),
(274, 83, 'Examination', 'exam_slip.view', 'Release Exam Slip View', 1),
(275, 84, 'Examination', 'publish_exam_result_date.list', 'Publish Exam Result Schedule List', 1),
(276, 84, 'Examination', 'publish_exam_result_date.add', 'Publish Exam Result Schedule Add', 1),
(277, 84, 'Examination', 'publish_exam_result_date.view', 'Publish Exam Result Schedule View', 1),
(278, 85, 'Examination', 'publish_assesment_result_date.list', 'Publish Assesment Result Schedule List', 1),
(279, 85, 'Examination', 'publish_assesment_result_date.add', 'Publish Assesment Result Schedule Add', 1),
(280, 85, 'Examination', 'publish_assesment_result_date.view', 'Publish Assesment Result Schedule View', 1),
(281, 86, 'Examination', 'mark_distribution.list', 'Mark Distribution List', 1),
(282, 86, 'Examination', 'mark_distribution.add', 'Mark Distribution Add', 1),
(283, 86, 'Examination', 'mark_distribution.view', 'Mark Distribution View', 1),
(284, 87, 'Examination', 'mark_distribution.approval_list', 'Mark Distribution Approval List', 1),
(285, 87, 'Examination', 'mark_distribution.approve', 'Mark Distribution Approve', 1),
(286, 88, 'Examination', 'student_marks_entry.approval_list', 'Student Marks Entry Approval List', 1),
(287, 88, 'Examination', 'student_marks_entry.approve', 'Student Marks Entry Approve', 1),
(288, 187, 'Examination', 'student_marks_entry.student_list', 'Student Marks Entry List', 1),
(289, 187, 'Examination', 'student_marks_entry.add', 'Student Marks Entry Add', 1),
(290, 89, 'Examination', 'student_marks_entry.summary_list', 'Student Marks Entry Summary List', 1),
(291, 89, 'Examination', 'student_marks_entry.summary_view', 'Student Marks Entry Summary View', 1),
(292, 90, 'Examination', 'remarking_application.entry_list', 'Remarks Entry List', 1),
(293, 90, 'Examination', 'remarking_application.add', 'Add Remarks', 1),
(294, 91, 'Examination', 'remarking_application.approval_list', 'Remark Entry Approval List', 1),
(295, 91, 'Examination', 'remarking_application.approve', 'Remark Entry Approve', 1),
(296, 92, 'Examination', 'remarking_application.summary_list', 'Student Remarks Application Summary List', 1),
(297, 92, 'Examination', 'remarking_application.view', 'Student Remarks Application Summary View', 1),
(298, 93, 'Examination', 'transcript.list', 'Transcript', 1),
(299, 93, 'Examination', 'transcript.view', 'Transcript View', 1),
(300, 94, 'Examination', 'result_slip.list', 'Result Slip', 1),
(301, 94, 'Examination', 'result_slip.view', 'Result Slip View', 1),
(302, 95, 'Graduation', 'award.list', 'Award List', 1),
(303, 95, 'Graduation', 'award.add', 'Award Add', 1),
(304, 95, 'Graduation', 'award.edit', 'Award Edit', 1),
(305, 96, 'Graduation', 'convocation.list', 'Award List', 1),
(306, 97, 'Graduation', 'guest.list', 'Guest List', 1),
(307, 97, 'Graduation', 'guest.add', 'Guest Add', 1),
(308, 97, 'Graduation', 'guest.edit', 'Guest Edit', 1),
(309, 98, 'Graduation', 'graduation.list', 'Graduation List', 1),
(310, 98, 'Graduation', 'graduation.add', 'Graduation Add', 1),
(311, 98, 'Graduation', 'graduation.view', 'Graduation View', 1),
(312, 98, 'Graduation', 'graduation.add_student', 'Graduation Add / Edit Students', 1),
(313, 99, 'Graduation', 'graduation.approval_list', 'Graduation Approval List', 1),
(314, 99, 'Graduation', 'graduation.approve', 'Graduation Approve', 1),
(315, 100, 'Finance', 'currency.list', 'Currency List', 1),
(316, 100, 'Finance', 'currency.add', 'Currency Add', 1),
(317, 100, 'Finance', 'currency.edit', 'Currency Edit', 1),
(318, 101, 'Finance', 'currency_rate_setup.list', 'Currency Rate Setup List', 1),
(319, 101, 'Finance', 'currency_rate_setup.add', 'Currency Rate Setup Add', 1),
(320, 101, 'Finance', 'currency_rate_setup.edit', 'Currency Rate Setup Edit', 1),
(321, 102, 'Finance', 'credit_note_type.list', 'Credit Note Type', 1),
(322, 102, 'Finance', 'credit_note_type.add', 'Credit Note Add', 1),
(323, 102, 'Finance', 'credit_note_type.edit', 'Credit Note Edit', 1),
(324, 103, 'Finance', 'fee_structure_activity.list', 'Fee Structure Activity List', 1),
(325, 103, 'Finance', 'fee_structure_activity.add', 'Fee Structure Activity Add', 1),
(326, 103, 'Finance', 'fee_structure_activity.edit', 'Fee Structure Activity Edit', 1),
(327, 104, 'Finance', 'account_code.list', 'Account Code List', 1),
(328, 104, 'Finance', 'account_code.add', 'Account Code Add', 1),
(329, 104, 'Finance', 'account_code.edit', 'Account Code Edit', 1),
(330, 105, 'Finance', 'bank_registration.list', 'Bank Registration List', 1),
(331, 105, 'Finance', 'bank_registration.add', 'Bank Registration Add', 1),
(332, 105, 'Finance', 'bank_registration.edit', 'Bank Registration Edit', 1),
(339, 108, 'Finance', 'payment_type.list', 'Payment Type List', 1),
(340, 108, 'Finance', 'payment_type.add', 'Payment Type Add', 1),
(341, 108, 'Finance', 'payment_type.edit', 'Payment Type Edit', 1),
(342, 109, 'Finance', 'fee_category.list', 'Fee Category List', 1),
(343, 109, 'Finance', 'fee_category.add', 'Fee Category Add', 1),
(344, 109, 'Finance', 'fee_category.edit', 'Fee Category Edit', 1),
(345, 110, 'Finance', 'fee_setup.list', 'Fee Setup List', 1),
(346, 110, 'Finance', 'fee_setup.add', 'Fee Setup Add', 1),
(347, 110, 'Finance', 'fee_setup.edit', 'Fee Setup Edit', 1),
(348, 111, 'Finance', 'fee_structure.programme_list', 'Fee Structure Program List', 1),
(349, 111, 'Finance', 'fee_structure.programme_landscape_list', 'Fee Structure Program Landscape List', 1),
(350, 111, 'Finance', 'fee_structure.add', 'Fee Structure Add', 1),
(351, 112, 'Finance', 'partne_university_fee.list', 'Partner University Fee List', 1),
(352, 112, 'Finance', 'partne_university_fee.add', 'Partner University Fee Add', 1),
(353, 112, 'Finance', 'partne_university_fee.edit', 'Partner University Fee Edit', 1),
(354, 195, 'Finance', 'discount_type.list', 'Discount Type List', 1),
(355, 195, 'Finance', 'discount_type.add', 'Discount Type Add', 1),
(356, 195, 'Finance', 'discount_type.edit', 'Discount Type Edit', 1),
(357, 114, 'Finance', 'employee_discount.list', 'Employee Discount List', 1),
(358, 114, 'Finance', 'employee_discount.add', 'Employee Discount Add', 1),
(359, 114, 'Finance', 'employee_discount.edit', 'Employee Discount Edit', 1),
(360, 115, 'Finance', 'discount.list', 'Discount List', 1),
(361, 115, 'Finance', 'discount.add', 'Discount Add', 1),
(362, 115, 'Finance', 'discount.edit', 'Discount Edit', 1),
(363, 116, 'Finance', 'statement_of_account.list', 'Statement Of Accounts List', 1),
(364, 116, 'Finance', 'statement_of_account.view', 'Statement Of Accounts View', 1),
(365, 119, 'Finance', 'main_invoice.list', 'Invoice Generation List', 1),
(366, 117, 'Finance', 'main_invoice.add', 'Add Invoice', 1),
(367, 117, 'Finance', 'main_invoice.view', 'View Invoice', 1),
(368, 118, 'Finance', 'main_invoice.cancel_invoice', 'Cancel Wrong Invoice List', 1),
(369, 118, 'Finance', 'main_invoice.cancel', 'Cancel Wrong Invoice Generated', 1),
(370, 119, 'Finance', 'partner_university_invoice.list', 'Partner University Invoice List', 1),
(371, 119, 'Finance', 'partner_university_invoice.add', 'Partner University Invoice Add', 1),
(372, 119, 'Finance', 'partner_university_invoice.view', 'Partner University Invoice View', 1),
(373, 120, 'Finance', 'receipt.list', 'Receipt List', 1),
(374, 120, 'Finance', 'receipt.add', 'Receipt Add', 1),
(375, 120, 'Finance', 'receipt.view', 'Receipt View', 1),
(376, 121, 'Finance', 'receipt.approval_list', 'Receipt Approval List', 1),
(377, 121, 'Finance', 'receipt.approve', 'Receipt Approve', 1),
(378, 122, 'Finance', 'sponser_main_invoice.list', 'Sponsor Invoice List', 1),
(379, 122, 'Finance', 'sponser_main_invoice.add', 'Sponsor Invoice Add', 1),
(380, 122, 'Finance', 'sponser_main_invoice.view', 'Sponsor Invoice View', 1),
(381, 123, 'Finance', 'sponser_main_invoice.cancel_list', 'Sponsor Invoice Cancellation List', 1),
(382, 123, 'Finance', 'sponser_main_invoice.cancel', 'Sponser Invoice Cancellation  ', 1),
(383, 124, 'Finance', 'advance_payment.list', 'Advance Payment List', 1),
(384, 124, 'Finance', 'advance_payment.add', 'Advance Payment Add', 1),
(385, 124, 'Finance', 'advance_payment.view', 'Advance Payment View', 1),
(386, 124, 'Finance', 'advance_payment.approval_list', 'Advance Payment Approval List', 1),
(387, 124, 'Finance', 'advance_payment.approva', 'Advance Payment Approve', 1),
(388, 125, 'Finance', 'credit_note.list', 'Credit Note Entry List', 1),
(389, 125, 'Finance', 'credit_note.add', 'Credit Note Add', 1),
(390, 125, 'Finance', 'credit_note.view', 'Credit Note View', 1),
(391, 125, 'Finance', 'credit_note.approval_list', 'Credit Note Approval List', 1),
(392, 124, 'Finance', 'credit_note.approve', 'Credit Note Approve', 1),
(393, 126, 'Finance', 'debit_note.list', 'Debit Note List', 1),
(394, 126, 'Finance', 'debit_note.add', 'Debit Note Add', 1),
(395, 126, 'Finance', 'debit_note.view', 'Debit Note View', 1),
(396, 126, 'Finance', 'debit_note.approval_list', 'Debit Note Approval List', 1),
(397, 126, 'Finance', 'debit_note.approve', 'Debit Note Approve', 1),
(398, 127, 'Finance', 'refund_entry.list', 'Refund Entry List', 1),
(399, 127, 'Finance', 'refund_entry.add', 'Refund Entry Add', 1),
(400, 127, 'Finance', 'refund_entry.view', 'Refund Entry View', 1),
(401, 127, 'Finance', 'refund_entry.approval_list', 'Refund Entry Approval List', 1),
(402, 127, 'Finance', 'refund_entry.approve', 'Refund Entry Approve', 1),
(403, 128, 'Sponsor', 'sponser.list', 'Sponsor List', 1),
(404, 128, 'Sponsor', 'sponser.add', 'Sponsor Add', 1),
(405, 128, 'Sponsor', 'sponser.edit', 'Sponsor Edit', 1),
(406, 129, 'Sponsor', 'sponser_has_students.sponser_list', 'Sponsor Has Students List', 1),
(407, 129, 'Sponsor', 'sponser_has_students.add', 'Sponsor Has Students Add', 1),
(408, 130, 'Hostel', 'room_type.list', 'Room Type List', 1),
(409, 130, 'Hostel', 'room_type.add', 'Room Type Add', 1),
(410, 130, 'Hostel', 'room_type.edit', 'Room Type Edit', 1),
(411, 131, 'Hostel', 'hostel_inventory.list', 'Hostel Inventory List', 1),
(412, 131, 'Hostel', 'hostel_inventory.add', 'Hostel Inventory Add', 1),
(413, 131, 'Hostel', 'hostel_inventory.edit', 'Hostel Inventory Edit', 1),
(414, 132, 'Hostel', 'hostel_registration.list', 'Hostel Registration List', 1),
(415, 132, 'Hostel', 'hostel_registration.add', 'Hostel Registration Add', 1),
(416, 132, 'Hostel', 'hostel_registration.edit', 'Hostel Registration Edit', 1),
(417, 133, 'Hostel', 'building_setup.list', 'Apartment Setup List', 1),
(418, 133, 'Hostel', 'building_setup.add', 'Apartment Setup Add', 1),
(419, 133, 'Hostel', 'building_setup.edit', 'Apartment Setup Edit', 1),
(420, 134, 'Hostel', 'block_setup.list', 'Unit Setup List', 1),
(421, 134, 'Hostel', 'block_setup.add', 'Unit Setup Add', 1),
(422, 134, 'Hostel', 'block_setup.edit', 'Unit Setup Edit', 1),
(423, 135, 'Hostel', 'room_setup.list', 'Hostel Room Setup List', 1),
(424, 135, 'Hostel', 'room_setup.add', 'Hostel Room Setup Add', 1),
(425, 135, 'Hostel', 'room_setup.edit', 'Hostel Room Setup Edit', 1),
(426, 136, 'Hostel', 'room_allotment.list', 'Room Allotment List', 1),
(427, 136, 'Hostel', 'room_allotment.add', 'Room Allotment Add', 1),
(428, 136, 'Hostel', 'room_allotment.edit', 'Room Allotment Edit', 1),
(429, 137, 'Hostel', 'room_allotment.summary_list', 'Room Allotment Summary List', 1),
(430, 137, 'Hostel', 'room_allotment.summary_view', 'Room Allotment Summary View', 1),
(431, 138, 'Hostel', 'room_vacancy.list', 'Room Vacancy List', 1),
(432, 138, 'Hostel', 'room_vacancy.allot_room', 'Room Vacancy Allot Room', 1),
(433, 138, 'Hostel', 'room_vacancy.view', 'Room Vacancy View', 1),
(434, 139, 'Facility', 'equipment.list', 'Equipments List', 1),
(435, 139, 'Facility', 'equipment.add', 'Equipments Add', 1),
(436, 139, 'Facility', 'equipment.edit', 'Equipments Edit', 1),
(437, 140, 'Facility', 'facility_room_type.list', 'Facility Room Type List', 1),
(438, 140, 'Facility', 'facility_room_type.add', 'Facility Room Type Add', 1),
(439, 140, 'Facility', 'facility_room_type.edit', 'Facility Room Type Edit', 1),
(440, 141, 'Facility', 'facility_building_registration.list', 'Facility Building List', 1),
(441, 141, 'Facility', 'facility_building_registration.add', 'Facility Building Add', 1),
(442, 141, 'Facility', 'facility_building_registration.edit', 'Facility Building Edit', 1),
(443, 142, 'Facility', 'facility_room_setup.list', 'Facility Room Setup List', 1),
(444, 142, 'Facility', 'facility_room_setup.add', 'Facility Room Setup Add', 1),
(445, 142, 'Facility', 'facility_room_setup.edit', 'Facility Room Setup Edit', 1),
(446, 143, 'Facility', 'room_booking.list', 'Apply For Booking List', 1),
(447, 143, 'Facility', 'room_booking.book_room', 'Room Booking Add', 1),
(448, 144, 'Facility', 'room_booking.summary_list', 'Room Booking Summary List', 1),
(449, 144, 'Facility', 'room_booking.view', 'View Room Booking', 1),
(450, 145, 'Internship', 'internship_limit.edit', 'Max. Internship Limit Per Student Edit', 1),
(451, 146, 'Internship', 'company_type.list', 'Internship Company Type List', 1),
(452, 146, 'Internship', 'company_type.add', 'Internship Company Type Add', 1),
(453, 146, 'Internship', 'company_type.edit', 'Internship Company Type Edit', 1),
(454, 147, 'Internship', 'internship_company_registration.list', 'Intrernship Company Registration List', 1),
(455, 147, 'Internship', 'internship_company_registration.add', 'Internship Company Registration Add', 1),
(456, 147, 'Internship', 'internship_company_registration.edit', 'Internship Company Registration Edit', 1),
(457, 148, 'Internship', 'internship_application.list', 'Internship Application List', 1),
(458, 148, 'Internship', 'internship_application.view', 'Internship Application View', 1),
(459, 149, 'Internship', 'internship_application.approval_list', 'Internship Application Approval List', 1),
(460, 149, 'Internship', 'internship_application.approve', 'Internship Application Approve', 1),
(461, 150, 'Internship', 'internship_application.reporting', 'Reporting List', 1),
(462, 150, 'Internship', 'internship_application.reporting_view', 'Reporting View', 1),
(463, 151, 'Internship', 'placement_form.list', 'Placement Form List', 1),
(464, 151, 'Internship', 'placement_form.add', 'Placement Form Add', 1),
(465, 151, 'Internship', 'placement_form.view', 'Placement Form VIew', 1),
(466, 152, 'Alumni', 'allumini.list', 'Allumini List', 1),
(467, 152, 'Alumni', 'allumini.view', 'Alumni View', 1),
(468, 153, 'Communication', 'email_template.list', 'Communication Email Templates List', 1),
(469, 153, 'Communication', 'email_template.add', 'Communication Email Templates Add', 1),
(470, 153, 'Communication', 'email_template.edit', 'Communication Email Templates Edit', 1),
(471, 154, 'Communication', 'email_communication_group.list', 'Communication Email Group List', 1),
(472, 154, 'Communication', 'email_communication_group.add', 'Communication Email Group Add', 1),
(473, 154, 'Communication', 'email_communication_group.edit', 'Communication Email Group Edit', 1),
(474, 155, 'Communication', 'email_communication_group.recepients_list', 'Communication Email Group Recepients List', 1),
(475, 155, 'Communication', 'email_communication_group.add_recepients', 'Communication Email Group Recepients Add', 1),
(476, 156, 'Communication', 'template_message.list', 'Communication Messaging Template List', 1),
(477, 156, 'Communication', 'template_message.add', 'Communication Messaging Template Add', 1),
(478, 156, 'Communication', 'template_message.edit', 'Communication Messaging Template Edit', 1),
(479, 157, 'Communication', 'communication_message_group.list', 'Communication Messaging Group List', 1),
(480, 157, 'Communication', 'communication_message_group.add', 'Communication Messaging Group Add', 1),
(481, 157, 'Communication', 'communication_message_group.edit', 'Communication Messaging Group Edit', 1),
(482, 158, 'Communication', 'communication_message_group.recepients_list', 'Communication Message Group Recepients List', 1),
(483, 155, 'Communication', 'email_communication_group.view', 'Communication Email Group Recepients View', 1),
(484, 158, 'Communication', 'communication_message_group.add_recepients', 'Communication Message Group Recepients Add', 1),
(485, 158, 'Communication', 'communication_message_group.view', 'Communication Message Group Recepients View', 1),
(486, 159, 'IPS', 'topic.list', 'Research Topic List', 1),
(487, 159, 'IPS', 'topic.add', 'Research Topic Add', 1),
(488, 159, 'IPS', 'topic.edit', 'Research Topic Edit', 1),
(489, 160, 'IPS', 'research_status.list', 'Research Status List', 1),
(490, 160, 'IPS', 'research_status.add', 'Research Status Add', 1),
(491, 160, 'IPS', 'research_status.edit', 'Research Status Edit', 1),
(492, 161, 'IPS', 'research_fieldofinterest.list', 'Research Field Of Interest List', 1),
(493, 161, 'IPS', 'research_fieldofinterest.add', 'Research Field Of Interest Add', 1),
(494, 161, 'IPS', 'research_fieldofinterest.edit', 'Research Field Of Interest Edit', 1),
(495, 162, 'IPS', 'research_reason_applying.list', 'Reason For Applying List', 1),
(496, 162, 'IPS', 'research_reason_applying.add', 'Reason For Applying Add', 1),
(497, 162, 'IPS', 'research_reason_applying.edit', 'Reason For Applying Edit', 1),
(498, 163, 'IPS', 'research_category.list', 'Research Category List', 1),
(499, 163, 'IPS', 'research_category.add', 'Research Category Add', 1),
(500, 163, 'IPS', 'research_category.edit', 'Research Category Edit', 1),
(501, 164, 'IPS', 'research_colloquium.list', 'Colloquium Setup List', 1),
(502, 164, 'IPS', 'research_colloquium.add', 'Colloquium Setup Add', 1),
(503, 164, 'IPS', 'research_colloquium.edit', 'Colloquium Setup Edit', 1),
(504, 165, 'IPS', 'research_advisory.list', 'Research Advisory List', 1),
(505, 165, 'IPS', 'research_advisory.add', 'Research Advisory Add', 1),
(506, 165, 'IPS', 'research_advisory.edit', 'Research Advisory Edit', 1),
(507, 166, 'IPS', 'research_readers.list', 'Research Readers List', 1),
(508, 166, 'IPS', 'research_readers.add', 'Research Readers Add', 1),
(509, 166, 'IPS', 'research_readers.edit', 'Research Readers Edit', 1),
(510, 167, 'IPS', 'research_comitee.list', 'Research Proposal Defense Comitee List', 1),
(511, 167, 'IPS', 'research_comitee.add', 'Research Proposal Defense Comitee Add', 1),
(512, 167, 'IPS', 'research_comitee.edit', 'Research Proposal Defense Comitee Edit', 1),
(513, 168, 'IPS', 'research_chapter.list', 'Research Chapter List', 1),
(514, 168, 'IPS', 'research_chapter.add', 'Research Chapter Add', 1),
(515, 168, 'IPS', 'research_chapter.edit', 'Research Chapter Edit', 1),
(516, 169, 'IPS', 'research_deliverables.list', 'Research Deliverables Setup List', 1),
(517, 169, 'IPS', 'research_deliverables.add', 'Research Deliverables Setup Add', 1),
(518, 169, 'IPS', 'research_deliverables.edit', 'Research Deliverables Setup Edit', 1),
(519, 170, 'IPS', 'submitted_deliverables.list', 'Research Submitted Deliverables Application List', 1),
(520, 170, 'IPS', 'submitted_deliverables.add', 'Research Submitted Deliverables Application Add', 1),
(521, 170, 'IPS', 'submitted_deliverables.approve', 'Research Submitted Deliverables Application Approve', 1),
(522, 170, 'IPS', 'submitted_deliverables.view', 'Research Submitted Deliverables Application View', 1),
(523, 171, 'IPS', 'student_duration_tagging.edit', 'Student Duration Tagging Edit', 1),
(524, 172, 'IPS', 'research_stage.list', 'Research Stage List', 1),
(525, 172, 'IPS', 'research_stage.add', 'Research Stage Add', 1),
(526, 172, 'IPS', 'research_stage.edit', 'Research Stage Edit', 1),
(527, 172, 'IPS', 'research_stage.add_semester', 'Research Stage Add Semester', 1),
(528, 173, 'IPS', 'research_mile_stone.list', 'Research Milestone List', 1),
(529, 173, 'IPS', 'research_mile_stone.add', 'Research Mile Stone Add', 1),
(530, 173, 'IPS', 'research_mile_stone.edit', 'Research Mile Stone Edit', 1),
(531, 174, 'IPS', 'mile_stone_semester.list', 'Tag Mile Stone To Semester List', 1),
(532, 174, 'IPS', 'mile_stone_semester.add', 'Tag Mile Stone To Semester Add', 1),
(533, 174, 'IPS', 'mile_stone_semester.edit', 'Tag Mile Stone To Semester Edit', 1),
(534, 175, 'IPS', 'research_stage.overview', 'Research Stages Overwiew', 1),
(535, 176, 'IPS', 'research_supervisor_role.list', 'Research Supervisor Role List', 1),
(536, 176, 'IPS', 'research_supervisor_role.add', 'Research Supervisor Role Add', 1),
(537, 176, 'IPS', 'research_supervisor_role.edit', 'Research Supervisor Role Edit', 1),
(538, 177, 'IPS', 'research_supervisor.list', 'Research Supervisor List', 1),
(539, 177, 'IPS', 'research_supervisor.add', 'Research Supervisor Add', 1),
(540, 177, 'IPS', 'research_supervisor.edit', 'Research Supervisor Edit', 1),
(541, 178, 'IPS', 'research_supervisor_tagging.add', 'Research Supervisor Tagging Add / Edit', 1),
(542, 179, 'IPS', 'research_examiner_role.list', 'Research Examiner Role List', 1),
(543, 179, 'IPS', 'research_examiner_role.add', 'Research Examiner Role Add', 1),
(544, 179, 'IPS', 'research_examiner_role.edit', 'Research Examiner Role Edit', 1),
(545, 180, 'IPS', 'research_examiner.list', 'Research Examiner List', 1),
(546, 180, 'IPS', 'research_examiner.add', 'Research Examiner Add', 1),
(547, 180, 'IPS', 'research_examiner.edit', 'Research Examiner Edit', 1),
(548, 181, 'IPS', 'research_proposal.list', 'Research Proposal List', 1),
(549, 181, 'IPS', 'research_proposal.add', 'Research Proposal Add', 1),
(550, 181, 'IPS', 'research_proposal.view', 'Research Proposal View', 1),
(551, 182, 'IPS', 'research_articleship.list', 'Research Articleship', 1),
(552, 182, 'IPS', 'research_articleship.add', 'Research Articleship Add', 1),
(553, 182, 'IPS', 'research_articleship.view', 'Research Articleship View', 1),
(554, 181, 'IPS', 'research_proposal.edit', 'Research Proposal Edit', 1),
(555, 182, 'IPS', 'research_articleship.edit', 'Research Articleship Edit', 1),
(556, 183, 'IPS', 'research_professionalpracricepaper.list', 'Research Professional Practice Paper List', 1),
(557, 183, 'IPS', 'research_professionalpracricepaper.add', 'Research Professional Practice Paper Add', 1),
(558, 183, 'IPS', 'research_professionalpracricepaper.edit', 'Research Professional Practice Paper Edit', 1),
(559, 183, 'IPS', 'research_professionalpracricepaper.view', 'Research Professional Practice Paper View', 1),
(560, 184, 'IPS', 'research_proposal.approval_list', 'Research Proposal Approval List', 1),
(561, 184, 'IPS', 'research_proposal.approve', 'Research Proposal Approve', 1),
(562, 185, 'IPS', 'research_articleship.approval_list', 'Research Articleship Approval List', 1),
(563, 185, 'IPS', 'research_articleship.approve', 'Research Articleship Approve', 1),
(564, 186, 'IPS', 'research_professionalpracricepaper.approval_list', 'Research Professional Practice Paper Approval List', 1),
(565, 186, 'IPS', 'research_professionalpracricepaper.approve', 'Research Professional Practice Paper Approve', 1),
(566, 188, 'IPS', 'research_supervisor_change_application.list', 'Change Supervisor Applicaton List', 1),
(567, 188, 'IPS', 'research_supervisor_change_application.approve', 'Change Supervisor Application Approve', 1),
(568, 188, 'IPS', 'research_supervisor_change_application.view', 'Change Supervisor Application View', 1),
(569, 189, 'Examination', 'ips_progress.student_list', 'IPS Progress Student List', 1),
(570, 189, 'Examination', 'ips_progress.add', 'IPS Progress Add', 1),
(571, 111, 'Finance', 'fee_structure.copy_fee_structure', 'Copy Existing Fee Structure', 1),
(573, 208, 'prdtm', 'product_type.list', 'Product Type List', 1),
(574, 208, 'prdtm', 'product_type.add', 'Product Type Add', 1),
(575, 208, 'prdtm', 'product_type.edit', 'Product Type Edit', 1),
(576, 209, 'prdtm', 'category_type.list', 'Category Type List', 1),
(577, 209, 'prdtm', 'category_type.add', 'Category Type Add', 1),
(578, 209, 'prdtm', 'category_type.edit', 'Category Type Edit', 1),
(579, 210, 'prdtm', 'programme.list', 'Programme List', 1),
(580, 210, 'prdtm', 'programme.add', 'Programme Add', 1),
(581, 210, 'prdtm', 'programme.edit', 'Programme Edit', 1),
(582, 241, 'prdtm', 'programme.approval_list', 'Partner Programme Approval List', 1),
(583, 241, 'prdtm', 'programme.approve', 'Partner Programme Approve', 1),
(584, 16, 'pm', 'partner_university.list', 'Partner University List', 1),
(585, 16, 'pm', 'partner_university.add', 'Partner University Add', 1),
(586, 16, 'pm', 'partner_university.edit', 'Partner University Edit', 1),
(587, 244, 'pm', 'partner_university.approval_list', 'Partner University Approval List', 1),
(588, 244, 'pm', 'partner_university.approve', 'Partner University Approve', 1),
(589, 112, 'pm', 'partner_university_invoice.list', 'Partner University Invoice List', 1),
(590, 112, 'pm', 'partner_university_invoice.view', 'Partner University Invoice View', 1),
(591, 220, 'pm', 'partner_university_receipt.list', 'Partner University Receipt List', 1),
(592, 220, 'pm', 'partner_university_receipt.view', 'Partner University Receipt View', 1),
(593, 196, 'Records', 'apply_change_learning_mode.list', 'Change Learning Mode List', 1),
(594, 196, 'Records', 'apply_change_learning_mode.add', 'Change Learning Mode Add', 1),
(595, 196, 'Records', 'apply_change_learning_mode.view', 'Change Learning Mode View', 1),
(596, 197, 'Records', 'apply_change_learning_mode.approval_list', 'Change Learning Mode Approval List', 1),
(597, 197, 'Records', 'apply_change_learning_mode.approve', 'Change Learning Mode Approve', 1);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_category`
--

CREATE TABLE `procurement_category` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_category`
--

INSERT INTO `procurement_category` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Electronics', 'ELe', '', 1, 1, '2021-05-14 00:07:55', 1, '2021-05-22 02:08:18'),
(2, 'Computer', 'Com', '', 1, 1, '2021-05-21 13:37:37', 1, '2021-05-22 02:08:53'),
(3, 'Cables', 'Cab', '', 1, 1, '2021-05-21 13:38:30', NULL, ''),
(4, 'Meters', 'Met', '', 1, 1, '2021-05-21 13:38:43', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `procurement_item`
--

CREATE TABLE `procurement_item` (
  `id` int(20) NOT NULL,
  `id_procurement_category` int(20) DEFAULT NULL,
  `id_procurement_sub_category` int(20) DEFAULT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `spq` varchar(512) DEFAULT '',
  `moq` varchar(512) DEFAULT '',
  `alternative_part_number` varchar(256) DEFAULT '',
  `price` float(20,2) DEFAULT 0.00,
  `part_number` varchar(256) DEFAULT '',
  `value` varchar(256) DEFAULT '',
  `package` varchar(256) DEFAULT '',
  `manufacturer` varchar(512) DEFAULT '',
  `quantity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_item`
--

INSERT INTO `procurement_item` (`id`, `id_procurement_category`, `id_procurement_sub_category`, `name`, `code`, `description`, `spq`, `moq`, `alternative_part_number`, `price`, `part_number`, `value`, `package`, `manufacturer`, `quantity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, 'Resistor 1K', 'R-1K', '<p>Descrionva</p>\r\n', '123', '123', '123', 0.00, '1334', '12', '11', '1', 2268, 1, 1, '2021-05-14 00:59:31', 1, '2021-05-22 02:07:08'),
(3, 2, 3, 'Moniter 32\"', 'Mon 32', '<p>DES</p>\r\n', 'S', 'M', '', 1.00, 'MMM123', '1', '1', '', 4, 1, 1, '2021-05-21 13:41:49', NULL, ''),
(4, 2, 5, 'Dell Latitude 460', 'DELL ', '<p>DAADA</p>\r\n', '1', '1', '1', 1.00, '1', '1', '1', '2', 8, 1, 1, '2021-05-21 13:51:06', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `procurement_item_has_vendors`
--

CREATE TABLE `procurement_item_has_vendors` (
  `id` int(20) NOT NULL,
  `id_item` int(20) DEFAULT 0,
  `id_vendor` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_item_has_vendors`
--

INSERT INTO `procurement_item_has_vendors` (`id`, `id_item`, `id_vendor`, `price`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 5, 1, 1, '2021-05-21 13:30:59', NULL, ''),
(2, 4, 1, 38000, 1, 1, '2021-05-21 13:51:23', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `procurement_sub_category`
--

CREATE TABLE `procurement_sub_category` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `id_procurement_category` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_sub_category`
--

INSERT INTO `procurement_sub_category` (`id`, `name`, `code`, `description`, `id_procurement_category`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'REsistors', 'RES', '', 1, 1, 1, '2021-05-14 00:59:09', 1, '2021-05-14 00:59:12'),
(2, 'Capacitors', 'Cap', '', 1, 1, 1, '2021-05-21 13:39:32', NULL, '2021-05-21 13:39:32'),
(3, 'Moniter', 'Mon', '', 2, 1, 1, '2021-05-21 13:40:06', NULL, '2021-05-21 13:40:06'),
(4, 'Central Processing Unit', 'CPU', '', 2, 1, 1, '2021-05-21 13:40:32', NULL, '2021-05-21 13:40:32'),
(5, 'Laptop', 'Lap', '', 2, 1, 1, '2021-05-21 13:40:46', NULL, '2021-05-21 13:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `product_quantity`
--

CREATE TABLE `product_quantity` (
  `id` int(20) NOT NULL,
  `id_item` int(20) DEFAULT 0,
  `id_description` int(20) DEFAULT 0,
  `id_grn` int(20) DEFAULT 0,
  `id_grn_detail` int(20) DEFAULT 0,
  `id_assembly` int(20) DEFAULT 0,
  `id_assembly_detail` int(20) DEFAULT 0,
  `id_assembly_return` int(20) DEFAULT 0,
  `id_assembly_return_detail` int(20) DEFAULT 0,
  `previous_quantity` int(20) DEFAULT 0,
  `grn_quantity` int(20) DEFAULT 0,
  `assembly_quantity` int(20) DEFAULT 0,
  `assembly_return_quantity` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_quantity`
--

INSERT INTO `product_quantity` (`id`, `id_item`, `id_description`, `id_grn`, `id_grn_detail`, `id_assembly`, `id_assembly_detail`, `id_assembly_return`, `id_assembly_return_detail`, `previous_quantity`, `grn_quantity`, `assembly_quantity`, `assembly_return_quantity`, `quantity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 4, 1, 1, 1, 0, 0, 0, 0, 0, 8, 0, 0, 8, 1, 1, '2021-05-21 13:55:53', NULL, ''),
(2, 3, 1, 1, 2, 0, 0, 0, 0, 0, 4, 0, 0, 4, 1, 1, '2021-05-21 13:55:53', NULL, ''),
(3, 2, 1, 1, 3, 0, 0, 0, 0, 0, 2880, 0, 0, 2880, 1, 1, '2021-05-21 13:55:53', NULL, ''),
(4, 2, 2, 0, 0, 1, 1, 0, 0, 2880, 0, 1000, 0, 1880, 1, 1, '2021-05-21 14:03:24', NULL, ''),
(5, 2, 3, 0, 0, 0, 0, 1, 1, 1880, 0, 0, 300, 2180, 1, 1, '2021-05-21 14:03:58', NULL, ''),
(6, 2, 3, 0, 0, 0, 0, 2, 2, 2180, 0, 0, 88, 2268, 1, 1, '2021-05-21 14:04:21', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `product_type`
--

CREATE TABLE `product_type` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT '',
  `id_parent_product` bigint(20) DEFAULT NULL,
  `id_child_product` bigint(20) DEFAULT NULL,
  `moodle_id` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_type`
--

INSERT INTO `product_type` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_parent_product`, `id_child_product`, `moodle_id`) VALUES
(1, 'Module', '', '', 1, 1, '2021-02-04 17:51:59', 1, '2021-02-04 17:51:59', NULL, NULL, NULL),
(2, 'Programme', '', '', 1, 1, '2021-02-04 17:52:17', 1, '2021-02-04 17:52:17', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `publish_exam_result_date`
--

CREATE TABLE `publish_exam_result_date` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publish_exam_result_date`
--

INSERT INTO `publish_exam_result_date` (`id`, `id_program`, `id_intake`, `id_semester`, `id_course`, `date_time`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 3, '2020-08-17 00:00:00', 1, 1, '2020-08-16 23:29:58', NULL, '2020-08-16 23:29:58');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` int(20) NOT NULL,
  `po_number` varchar(50) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `id_vendor` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `po_number`, `description`, `id_vendor`, `total_amount`, `paid_amount`, `balance_amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'PO000001/2021', '', 1, 307240.00, 0.00, 307240.00, 3, 1, '2021-05-21 13:55:06', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_details`
--

CREATE TABLE `purchase_order_details` (
  `id` int(20) NOT NULL,
  `id_po` int(20) DEFAULT 0,
  `id_grn_detail` int(20) DEFAULT 0,
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `received_quantity` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order_details`
--

INSERT INTO `purchase_order_details` (`id`, `id_po`, `id_grn_detail`, `id_category`, `id_sub_category`, `id_item`, `quantity`, `balance_quantity`, `received_quantity`, `price`, `total_price`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 2, 5, 4, 10, 2, 8, 30000.00, 300000.00, 1, NULL, '2021-05-21 13:54:08', 1, '2021-05-22 02:25:53'),
(2, 1, 1, 2, 3, 3, 4, 0, 4, 1000.00, 4000.00, 1, NULL, '2021-05-21 13:54:32', 1, '2021-05-22 02:25:53'),
(3, 1, 1, 1, 1, 2, 3000, 120, 2880, 1.08, 3240.00, 1, NULL, '2021-05-21 13:55:02', 1, '2021-05-22 02:25:53');

-- --------------------------------------------------------

--
-- Table structure for table `religion_setup`
--

CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religion_setup`
--

INSERT INTO `religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Islam', 'ISLAM', 1, NULL, '2020-07-10 23:24:44', NULL, '2020-07-10 23:24:44'),
(2, 'Buddhism', 'Buddhism', 1, NULL, '2020-07-10 23:24:56', NULL, '2020-07-10 23:24:56'),
(3, 'Hindu', 'Hindu', 1, NULL, '2020-11-13 09:10:41', NULL, '2020-11-13 09:10:41'),
(4, 'Atheists', 'Atheists', 1, NULL, '2021-01-14 20:04:02', NULL, '2021-01-14 20:04:02'),
(5, 'Agnostics', 'Agnostics', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(6, 'Bahais', 'Bahais', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(7, 'Buddhists', 'Buddhists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(8, 'Chinese folk-religionists', 'Chinese folk-religionists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(9, 'Christians', 'Christians', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(10, 'Confucianists', 'Confucianists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(11, 'Daoists', 'Daoists', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(12, 'Hindus', 'Hindus', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(13, 'Jains', 'Jains', 1, NULL, '2021-01-14 20:05:17', NULL, '2021-01-14 20:05:17'),
(14, 'Shintoists', 'Shintoists', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34'),
(15, 'Spiritists', 'Spiritists', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34'),
(16, 'Sikhs', 'Sikhs', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34'),
(17, 'Zoroastrians', 'Zoroastrians', 1, NULL, '2021-01-14 20:06:34', NULL, '2021-01-14 20:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `research_advisor`
--

CREATE TABLE `research_advisor` (
  `id` int(20) NOT NULL,
  `type` int(2) DEFAULT 0,
  `salutation` varchar(500) DEFAULT '',
  `id_specialisation` int(20) DEFAULT 0,
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `start_date` varchar(500) DEFAULT '',
  `end_date` varchar(500) DEFAULT '',
  `id_staff` int(20) DEFAULT 0,
  `full_name` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `contact_no` varchar(50) DEFAULT '',
  `address` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research_advisor`
--

INSERT INTO `research_advisor` (`id`, `type`, `salutation`, `id_specialisation`, `first_name`, `last_name`, `start_date`, `end_date`, `id_staff`, `full_name`, `email`, `password`, `contact_no`, `address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, '', 0, '', '', '2020-11-03', '', 3, 'DR. Shinchan Noahara', 'int-adv1@cms.com', '202cb962ac59075b964b07152d234b70', '0', '', 1, NULL, '2020-11-22 05:13:23', NULL, '2020-11-22 05:13:23');

-- --------------------------------------------------------

--
-- Table structure for table `research_advisory`
--

CREATE TABLE `research_advisory` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `email` varchar(2048) DEFAULT '',
  `course` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `research_advisory`
--

INSERT INTO `research_advisory` (`id`, `name`, `email`, `course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'PostGraduation Board 1', 'adb@cms.comn', '', 1, NULL, '2020-10-16 13:39:57', NULL, '2020-10-16 13:39:57'),
(2, 'Comitee Phd Unit', 'unitphd@cms.com', '', 1, NULL, '2020-10-16 22:01:42', NULL, '2020-10-16 22:01:42');

-- --------------------------------------------------------

--
-- Table structure for table `reset_user_password`
--

CREATE TABLE `reset_user_password` (
  `id` bigint(20) NOT NULL,
  `email` varchar(512) NOT NULL DEFAULT '',
  `password` varchar(1024) NOT NULL DEFAULT '',
  `type` varchar(128) NOT NULL DEFAULT '',
  `activation_id` varchar(512) NOT NULL DEFAULT '',
  `agent` varchar(512) NOT NULL DEFAULT '',
  `agent_string` varchar(512) NOT NULL DEFAULT '',
  `platform` varchar(512) NOT NULL DEFAULT '',
  `client_ip` varchar(32) NOT NULL DEFAULT '',
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `status` int(20) NOT NULL DEFAULT 0,
  `created_by` bigint(20) NOT NULL DEFAULT 0,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `updated_dtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resources`
--

CREATE TABLE `resources` (
  `id` int(20) NOT NULL,
  `name` varchar(512) DEFAULT '',
  `code` varchar(512) DEFAULT '',
  `status` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resources`
--

INSERT INTO `resources` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Online E-Book', 'Ebook', 1, NULL, '2021-05-06 23:50:18', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(18, 'Account Manager', 1),
(19, 'Product Manager', 1),
(21, 'Recors Manager', 1),
(22, 'Academic Facilitator', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14),
(548, 14, 10),
(549, 14, 11),
(550, 14, 12),
(551, 14, 13),
(552, 14, 14),
(553, 14, 15),
(554, 14, 7),
(555, 14, 8),
(556, 14, 9),
(2047, 16, 10),
(2048, 16, 11),
(2049, 16, 12),
(2050, 16, 13),
(2051, 16, 14),
(2052, 16, 15),
(2053, 16, 242),
(2054, 16, 243),
(2055, 16, 244),
(2056, 16, 245),
(2057, 16, 246),
(2058, 16, 247),
(2059, 16, 248),
(2060, 16, 249),
(2061, 16, 250),
(2062, 16, 251),
(2063, 16, 252),
(2064, 16, 253),
(2065, 16, 254),
(2066, 16, 255),
(2067, 16, 256),
(2068, 16, 257),
(2069, 16, 258),
(2070, 16, 259),
(2071, 16, 260),
(2072, 16, 261),
(2073, 16, 262),
(2074, 16, 263),
(2075, 16, 264),
(2076, 16, 265),
(2077, 16, 266),
(2078, 16, 267),
(2079, 16, 268),
(2080, 16, 269),
(2081, 16, 270),
(2082, 16, 271),
(2083, 16, 272),
(2084, 16, 273),
(2085, 16, 274),
(2086, 16, 275),
(2087, 16, 276),
(2088, 16, 277),
(2089, 16, 278),
(2090, 16, 279),
(2091, 16, 280),
(2092, 16, 281),
(2093, 16, 282),
(2094, 16, 283),
(2095, 16, 284),
(2096, 16, 285),
(2097, 16, 286),
(2098, 16, 287),
(2099, 16, 290),
(2100, 16, 291),
(2101, 16, 292),
(2102, 16, 293),
(2103, 16, 294),
(2104, 16, 295),
(2105, 16, 296),
(2106, 16, 297),
(2107, 16, 298),
(2108, 16, 299),
(2109, 16, 300),
(2110, 16, 301),
(2111, 16, 569),
(2112, 16, 570),
(5649, 15, 10),
(5650, 15, 11),
(5651, 15, 12),
(5652, 15, 13),
(5653, 15, 14),
(5654, 15, 15),
(5655, 15, 7),
(5656, 15, 8),
(5657, 15, 9),
(5658, 15, 70),
(5659, 15, 71),
(5660, 15, 72),
(5661, 15, 73),
(5662, 15, 74),
(5663, 15, 75),
(5664, 15, 76),
(5665, 15, 77),
(5666, 15, 78),
(5667, 15, 79),
(5668, 15, 80),
(5669, 15, 81),
(5670, 15, 82),
(5671, 15, 83),
(5672, 15, 84),
(5673, 15, 85),
(5674, 15, 86),
(5675, 15, 87),
(5676, 15, 88),
(5677, 15, 89),
(5678, 15, 90),
(5679, 15, 91),
(5680, 15, 92),
(5681, 15, 93),
(5682, 15, 94),
(5683, 15, 95),
(5684, 15, 96),
(5685, 15, 97),
(5686, 15, 98),
(5687, 15, 99),
(5688, 15, 100),
(5689, 15, 101),
(5690, 15, 102),
(5691, 15, 103),
(5692, 15, 104),
(5693, 15, 105),
(5694, 15, 106),
(5695, 15, 107),
(5696, 15, 108),
(5697, 15, 109),
(5698, 15, 110),
(5699, 15, 111),
(5700, 15, 112),
(5701, 15, 113),
(5702, 15, 114),
(5703, 15, 115),
(5704, 15, 116),
(5705, 15, 117),
(5706, 15, 118),
(5707, 15, 119),
(5708, 15, 120),
(5709, 15, 121),
(5710, 15, 122),
(5711, 15, 123),
(5712, 15, 124),
(5713, 15, 125),
(5714, 15, 126),
(5715, 15, 127),
(5716, 15, 128),
(5717, 15, 129),
(5718, 15, 130),
(5719, 15, 131),
(5720, 15, 132),
(5721, 15, 133),
(5722, 15, 134),
(5723, 15, 135),
(5724, 15, 136),
(5725, 15, 137),
(5726, 15, 138),
(5727, 15, 139),
(5728, 15, 140),
(5729, 15, 141),
(5730, 15, 142),
(5731, 15, 143),
(5732, 15, 144),
(5733, 15, 145),
(5734, 15, 146),
(5735, 15, 147),
(5736, 15, 148),
(5737, 15, 149),
(5738, 15, 572),
(5739, 15, 150),
(5740, 15, 151),
(5741, 15, 152),
(5742, 15, 153),
(5743, 15, 154),
(5744, 15, 155),
(5745, 15, 156),
(5746, 15, 157),
(5747, 15, 158),
(5748, 15, 159),
(5749, 15, 160),
(5750, 15, 161),
(5751, 15, 162),
(5752, 15, 163),
(5753, 15, 164),
(5754, 15, 165),
(5755, 15, 166),
(5756, 15, 167),
(5757, 15, 168),
(5758, 15, 169),
(5759, 15, 170),
(5760, 15, 171),
(5761, 15, 172),
(5762, 15, 173),
(5763, 15, 174),
(5764, 15, 175),
(5765, 15, 176),
(5766, 15, 177),
(5767, 15, 178),
(5768, 15, 179),
(5769, 15, 180),
(5770, 15, 181),
(5771, 15, 182),
(5772, 15, 183),
(5773, 15, 184),
(5774, 15, 185),
(5775, 15, 186),
(5776, 15, 187),
(5777, 15, 188),
(5778, 15, 189),
(5779, 15, 190),
(5780, 15, 191),
(5781, 15, 192),
(5782, 15, 193),
(5783, 15, 194),
(5784, 15, 195),
(5785, 15, 196),
(5786, 15, 197),
(5787, 15, 198),
(5788, 15, 199),
(5789, 15, 200),
(5790, 15, 201),
(5791, 15, 202),
(5792, 15, 203),
(5793, 15, 204),
(5794, 15, 205),
(5795, 15, 206),
(5796, 15, 207),
(5797, 15, 208),
(5798, 15, 209),
(5799, 15, 210),
(5800, 15, 211),
(5801, 15, 212),
(5802, 15, 213),
(5803, 15, 214),
(5804, 15, 215),
(5805, 15, 216),
(5806, 15, 217),
(5807, 15, 218),
(5808, 15, 219),
(5809, 15, 220),
(5810, 15, 221),
(5811, 15, 222),
(5812, 15, 223),
(5813, 15, 224),
(5814, 15, 225),
(5815, 15, 226),
(5816, 15, 227),
(5817, 15, 228),
(5818, 15, 229),
(5819, 15, 230),
(5820, 15, 231),
(5821, 15, 232),
(5822, 15, 233),
(5823, 15, 234),
(5824, 15, 235),
(5825, 15, 236),
(5826, 15, 237),
(5827, 15, 238),
(5828, 15, 239),
(5829, 15, 240),
(5830, 15, 241),
(5831, 15, 242),
(5832, 15, 243),
(5833, 15, 244),
(5834, 15, 245),
(5835, 15, 246),
(5836, 15, 247),
(5837, 15, 248),
(5838, 15, 249),
(5839, 15, 250),
(5840, 15, 251),
(5841, 15, 252),
(5842, 15, 253),
(5843, 15, 254),
(5844, 15, 255),
(5845, 15, 256),
(5846, 15, 257),
(5847, 15, 258),
(5848, 15, 259),
(5849, 15, 260),
(5850, 15, 261),
(5851, 15, 262),
(5852, 15, 263),
(5853, 15, 264),
(5854, 15, 265),
(5855, 15, 266),
(5856, 15, 267),
(5857, 15, 268),
(5858, 15, 269),
(5859, 15, 270),
(5860, 15, 271),
(5861, 15, 272),
(5862, 15, 273),
(5863, 15, 274),
(5864, 15, 275),
(5865, 15, 276),
(5866, 15, 277),
(5867, 15, 278),
(5868, 15, 279),
(5869, 15, 280),
(5870, 15, 281),
(5871, 15, 282),
(5872, 15, 283),
(5873, 15, 284),
(5874, 15, 285),
(5875, 15, 286),
(5876, 15, 287),
(5877, 15, 290),
(5878, 15, 291),
(5879, 15, 292),
(5880, 15, 293),
(5881, 15, 294),
(5882, 15, 295),
(5883, 15, 296),
(5884, 15, 297),
(5885, 15, 298),
(5886, 15, 299),
(5887, 15, 300),
(5888, 15, 301),
(5889, 15, 302),
(5890, 15, 303),
(5891, 15, 304),
(5892, 15, 305),
(5893, 15, 306),
(5894, 15, 307),
(5895, 15, 308),
(5896, 15, 309),
(5897, 15, 310),
(5898, 15, 311),
(5899, 15, 312),
(5900, 15, 315),
(5901, 15, 316),
(5902, 15, 317),
(5903, 15, 318),
(5904, 15, 319),
(5905, 15, 320),
(5906, 15, 321),
(5907, 15, 322),
(5908, 15, 323),
(5909, 15, 324),
(5910, 15, 325),
(5911, 15, 326),
(5912, 15, 327),
(5913, 15, 328),
(5914, 15, 329),
(5915, 15, 330),
(5916, 15, 331),
(5917, 15, 332),
(5918, 15, 333),
(5919, 15, 334),
(5920, 15, 335),
(5921, 15, 336),
(5922, 15, 337),
(5923, 15, 338),
(5924, 15, 339),
(5925, 15, 340),
(5926, 15, 341),
(5927, 15, 342),
(5928, 15, 343),
(5929, 15, 344),
(5930, 15, 345),
(5931, 15, 346),
(5932, 15, 347),
(5933, 15, 348),
(5934, 15, 349),
(5935, 15, 350),
(5936, 15, 571),
(5937, 15, 351),
(5938, 15, 352),
(5939, 15, 353),
(5940, 15, 354),
(5941, 15, 355),
(5942, 15, 356),
(5943, 15, 357),
(5944, 15, 358),
(5945, 15, 359),
(5946, 15, 360),
(5947, 15, 361),
(5948, 15, 362),
(5949, 15, 363),
(5950, 15, 364),
(5951, 15, 366),
(5952, 15, 367),
(5953, 15, 368),
(5954, 15, 369),
(5955, 15, 365),
(5956, 15, 370),
(5957, 15, 371),
(5958, 15, 372),
(5959, 15, 373),
(5960, 15, 374),
(5961, 15, 375),
(5962, 15, 376),
(5963, 15, 377),
(5964, 15, 378),
(5965, 15, 379),
(5966, 15, 380),
(5967, 15, 381),
(5968, 15, 382),
(5969, 15, 383),
(5970, 15, 384),
(5971, 15, 385),
(5972, 15, 386),
(5973, 15, 387),
(5974, 15, 392),
(5975, 15, 388),
(5976, 15, 389),
(5977, 15, 390),
(5978, 15, 391),
(5979, 15, 393),
(5980, 15, 394),
(5981, 15, 395),
(5982, 15, 396),
(5983, 15, 397),
(5984, 15, 398),
(5985, 15, 399),
(5986, 15, 400),
(5987, 15, 401),
(5988, 15, 402),
(5989, 15, 403),
(5990, 15, 404),
(5991, 15, 405),
(5992, 15, 406),
(5993, 15, 407),
(5994, 15, 408),
(5995, 15, 409),
(5996, 15, 410),
(5997, 15, 411),
(5998, 15, 412),
(5999, 15, 413),
(6000, 15, 414),
(6001, 15, 415),
(6002, 15, 416),
(6003, 15, 417),
(6004, 15, 418),
(6005, 15, 419),
(6006, 15, 420),
(6007, 15, 421),
(6008, 15, 422),
(6009, 15, 423),
(6010, 15, 424),
(6011, 15, 425),
(6012, 15, 426),
(6013, 15, 427),
(6014, 15, 428),
(6015, 15, 429),
(6016, 15, 430),
(6017, 15, 431),
(6018, 15, 432),
(6019, 15, 433),
(6020, 15, 434),
(6021, 15, 435),
(6022, 15, 436),
(6023, 15, 437),
(6024, 15, 438),
(6025, 15, 439),
(6026, 15, 440),
(6027, 15, 441),
(6028, 15, 442),
(6029, 15, 443),
(6030, 15, 444),
(6031, 15, 445),
(6032, 15, 446),
(6033, 15, 447),
(6034, 15, 448),
(6035, 15, 449),
(6036, 15, 450),
(6037, 15, 451),
(6038, 15, 452),
(6039, 15, 453),
(6040, 15, 454),
(6041, 15, 455),
(6042, 15, 456),
(6043, 15, 457),
(6044, 15, 458),
(6045, 15, 459),
(6046, 15, 460),
(6047, 15, 461),
(6048, 15, 462),
(6049, 15, 463),
(6050, 15, 464),
(6051, 15, 465),
(6052, 15, 466),
(6053, 15, 467),
(6054, 15, 468),
(6055, 15, 469),
(6056, 15, 470),
(6057, 15, 471),
(6058, 15, 472),
(6059, 15, 473),
(6060, 15, 474),
(6061, 15, 475),
(6062, 15, 483),
(6063, 15, 476),
(6064, 15, 477),
(6065, 15, 478),
(6066, 15, 479),
(6067, 15, 480),
(6068, 15, 481),
(6069, 15, 482),
(6070, 15, 484),
(6071, 15, 485),
(6072, 15, 489),
(6073, 15, 490),
(6074, 15, 491),
(6075, 15, 492),
(6076, 15, 493),
(6077, 15, 494),
(6078, 15, 495),
(6079, 15, 496),
(6080, 15, 497),
(6081, 15, 498),
(6082, 15, 499),
(6083, 15, 500),
(6084, 15, 501),
(6085, 15, 502),
(6086, 15, 503),
(6087, 15, 504),
(6088, 15, 505),
(6089, 15, 506),
(6090, 15, 507),
(6091, 15, 508),
(6092, 15, 509),
(6093, 15, 510),
(6094, 15, 511),
(6095, 15, 512),
(6096, 15, 513),
(6097, 15, 514),
(6098, 15, 515),
(6099, 15, 516),
(6100, 15, 517),
(6101, 15, 518),
(6102, 15, 519),
(6103, 15, 520),
(6104, 15, 521),
(6105, 15, 522),
(6106, 15, 523),
(6107, 15, 524),
(6108, 15, 525),
(6109, 15, 526),
(6110, 15, 527),
(6111, 15, 528),
(6112, 15, 529),
(6113, 15, 530),
(6114, 15, 531),
(6115, 15, 532),
(6116, 15, 533),
(6117, 15, 534),
(6118, 15, 535),
(6119, 15, 536),
(6120, 15, 537),
(6121, 15, 538),
(6122, 15, 539),
(6123, 15, 540),
(6124, 15, 541),
(6125, 15, 542),
(6126, 15, 543),
(6127, 15, 544),
(6128, 15, 545),
(6129, 15, 546),
(6130, 15, 547),
(6131, 15, 548),
(6132, 15, 549),
(6133, 15, 550),
(6134, 15, 554),
(6135, 15, 551),
(6136, 15, 552),
(6137, 15, 553),
(6138, 15, 555),
(6139, 15, 556),
(6140, 15, 557),
(6141, 15, 558),
(6142, 15, 559),
(6143, 15, 560),
(6144, 15, 561),
(6145, 15, 562),
(6146, 15, 563),
(6147, 15, 564),
(6148, 15, 565),
(6149, 15, 288),
(6150, 15, 289),
(6151, 15, 566),
(6152, 15, 567),
(6153, 15, 568),
(6154, 15, 569),
(6155, 15, 570),
(6257, 17, 10),
(6258, 17, 11),
(6259, 17, 12),
(6260, 17, 13),
(6261, 17, 14),
(6262, 17, 15),
(6263, 17, 7),
(6264, 17, 8),
(6265, 17, 9),
(6266, 17, 70),
(6267, 17, 71),
(6268, 17, 72),
(6269, 17, 73),
(6270, 17, 74),
(6271, 17, 75),
(6272, 17, 86),
(6273, 17, 321),
(6274, 17, 322),
(6275, 17, 323),
(6276, 17, 324),
(6277, 17, 325),
(6278, 17, 326),
(6279, 17, 327),
(6280, 17, 328),
(6281, 17, 329),
(6282, 17, 330),
(6283, 17, 331),
(6284, 17, 332),
(6285, 17, 333),
(6286, 17, 334),
(6287, 17, 335),
(6288, 17, 336),
(6289, 17, 337),
(6290, 17, 338),
(6291, 17, 339),
(6292, 17, 340),
(6293, 17, 341),
(6294, 17, 342),
(6295, 17, 343),
(6296, 17, 344),
(6297, 17, 345),
(6298, 17, 346),
(6299, 17, 347),
(6300, 17, 348),
(6301, 17, 349),
(6302, 17, 350),
(6303, 17, 571),
(6304, 17, 351),
(6305, 17, 352),
(6306, 17, 353),
(6307, 17, 354),
(6308, 17, 355),
(6309, 17, 356),
(6310, 17, 360),
(6311, 17, 361),
(6312, 17, 362),
(6313, 17, 363),
(6314, 17, 364),
(6315, 17, 366),
(6316, 17, 367),
(6317, 17, 388),
(6318, 17, 389),
(6319, 17, 390),
(6320, 17, 391),
(6321, 17, 393),
(6322, 17, 394),
(6323, 17, 395),
(6324, 17, 396),
(6325, 17, 397),
(6326, 17, 486),
(6327, 17, 487),
(6328, 17, 488),
(6329, 17, 501),
(6330, 17, 502),
(6331, 17, 503),
(6340, 18, 10),
(6341, 18, 11),
(6342, 18, 12),
(6343, 18, 86),
(6344, 18, 87),
(6345, 18, 88),
(6346, 18, 95),
(6347, 18, 96),
(6348, 18, 97),
(6349, 18, 153),
(6350, 18, 154),
(6351, 18, 155),
(6352, 18, 80),
(6353, 18, 81),
(6354, 18, 82),
(6355, 18, 213),
(6356, 18, 214),
(6357, 19, 573),
(6358, 19, 574),
(6359, 19, 575),
(6360, 21, 10),
(6361, 21, 11),
(6362, 21, 12),
(6363, 21, 86),
(6364, 21, 87),
(6365, 21, 88),
(6366, 21, 95),
(6367, 21, 96),
(6368, 21, 97),
(6369, 21, 213),
(6370, 21, 214),
(6371, 22, 10),
(6372, 22, 11),
(6373, 22, 12),
(6374, 22, 86),
(6375, 22, 87),
(6376, 22, 88),
(6377, 22, 95),
(6378, 22, 96),
(6379, 22, 97),
(6380, 22, 153),
(6381, 22, 154),
(6382, 22, 155),
(6383, 22, 573),
(6384, 22, 574),
(6385, 22, 575),
(6386, 22, 213),
(6387, 22, 214);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-08-10 03:08:19', NULL, '2020-08-10 03:08:19'),
(2, 'Mrs', 2, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(3, 'Ms', 3, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(4, 'Dr', 4, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(5, 'Dato', 5, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(6, 'Ir', 6, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(20) NOT NULL,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `id_type` varchar(512) DEFAULT '',
  `id_university` int(20) DEFAULT 0,
  `nationality` varchar(200) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `joined_date` varchar(200) DEFAULT '',
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0 COMMENT 'as similar to department',
  `id_education_level` int(20) DEFAULT 0,
  `whatsapp_number` varchar(512) DEFAULT '',
  `linked_in` varchar(512) DEFAULT '',
  `facebook_id` varchar(512) DEFAULT '',
  `twitter_id` varchar(512) DEFAULT '',
  `ig_id` varchar(512) DEFAULT '',
  `image` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT '',
  `degree_details` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `salutation`, `name`, `first_name`, `last_name`, `id_type`, `id_university`, `nationality`, `ic_no`, `dob`, `joined_date`, `mobile_number`, `phone_number`, `id_country`, `id_state`, `zipcode`, `gender`, `staff_id`, `email`, `address`, `address_two`, `job_type`, `academic_type`, `id_department`, `id_faculty_program`, `id_education_level`, `whatsapp_number`, `linked_in`, `facebook_id`, `twitter_id`, `ig_id`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `degree_details`) VALUES
(1, '4', 'Dr. Darshan Brungee', 'Darshan', 'Brungee', 'NRIC', 9, '1', 'NDASDFi', '2021-03-31', '2021-03-25', '85394892', '88882323183', 79, 95, 123, 'Male', '8881', 'db11@espeed.com', 'KL', 'KL', '0', '1', 0, 0, 0, '879989987997', '', '', '', '', '41633a79ad9ac7f0b1a781db7009a1fa.jpeg', 1, NULL, '2021-03-12 22:46:52', NULL, '2021-03-12 22:46:52', NULL),
(2, '1', 'Mr. Muhammad Hakim', 'Muhammad', 'Hakim', 'PASSPORT', 0, '1', '7612948373', '2020-07-11', '2021-04-20', '93873737373', '637363636', 1, 1, 573663, 'Male', '1234', 'mhd@g.co.my', 'Address one ', 'Address two', '1', '1', 0, 0, 0, '`123456', '', '', '', '', '', 1, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07', NULL),
(3, '1', 'Mr. Asadullah Bag', 'Asadullah', 'Bag', 'NRIC', 0, '1', 'IC88909', '1980-08-01', '2021-02-11', '89786756', '123', 1, 1, 23432, 'Male', 'F22312', 'asad@cms.com', 'KL', 'KL', '1', '1', 0, 0, 0, '88888888', '', '', '', '', '', 1, NULL, '2020-08-06 09:26:31', NULL, '2020-08-06 09:26:31', NULL),
(4, '16', 'Professor John Arul Phillips', 'John Arul Phillips', '', '', 0, '', 'MF01', '1989-01-01', '', '12345', '12345', 110, 9, 75000, 'Male', '005', '11@mail.com', 'Malacca', 'Malacca', '1', '1', 2, 1, 0, '', '', '', '', '', 'education_john.jpg', 7, NULL, '2020-11-14 11:56:38', 1, '2020-11-14 11:56:38', 'B.A , Dip.Ed. M.Ed. M.S, PhD'),
(5, '16', 'DR. ABDULSALAM KAED', 'ABDULSALAM', 'KAED', '', 0, '', 'NA', '2020-12-01', '', '912', '0123995943', 110, 5, 68000, 'Male', '1', 'hazzna@gmail.com', '57 JALAN SWEI INTAN 3', 'TAMAN SERI INTAN', '1', '1', 1, 7, 0, '', '', '', '', '', '', 1, NULL, '2020-12-22 22:54:38', NULL, '2020-12-22 22:54:38', NULL),
(6, '4', 'Dr. aeu faculty', 'aeu', 'faculty', 'PASSPORT', 11, '1', '123123123', '2021-03-23', '2021-03-23', '123123', '123123', 1, 8, 123123, 'Male', '123', 'aeufaculty@gmail.com', 'address', 'address', '0', '1', 0, 0, 0, '231231231', '', '', '', '', 'a1f11d5000a4a43f806b4db666229390.jpg', 1, NULL, '2021-03-22 20:55:27', NULL, '2021-03-22 20:55:27', NULL),
(7, '5', 'Dato.  John Phillipies', ' John', 'Phillipies', 'NRIC', 17, '1', '345342523453245234534', '2021-04-28', '2021-04-28', '23535353434534534', '1', 1, 12, 1, 'Male', '123456765432', 'j234567@a.com', '1', '1', '0', '0', 0, 0, 0, '1', '1', '1', '1', '1', '6683dd0560dd03139b4df728b0d83fd7.jpg', 1, NULL, '2021-04-27 19:32:10', NULL, '2021-04-27 19:32:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff_bank_details`
--

CREATE TABLE `staff_bank_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `bank_account_name` varchar(500) DEFAULT '',
  `bank_code` varchar(500) DEFAULT '',
  `bank_account_number` varchar(500) DEFAULT '',
  `bank_address` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_bank_details`
--

INSERT INTO `staff_bank_details` (`id`, `id_staff`, `id_bank`, `bank_account_name`, `bank_code`, `bank_account_number`, `bank_address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 'Saving', 'CIM-KL', '1009284371783', 'Jalan no 9', 1, 1, '2020-12-09 17:50:17', NULL, '2020-12-09 17:50:17'),
(2, 0, 1, '23312312', '584329472934', '589342809248', 'KL', 1, NULL, '2021-03-12 09:25:55', NULL, '2021-03-12 09:25:55'),
(3, 1, 1, '2131', '312312', '2387193', '31231', 1, NULL, '2021-03-12 09:29:56', NULL, '2021-03-12 09:29:56'),
(4, 1, 1, '2131', '123', '123', '1223', 1, NULL, '2021-03-12 09:30:36', NULL, '2021-03-12 09:30:36'),
(5, 1, 1, '123', '123', '123', '12', 1, NULL, '2021-03-12 09:33:04', NULL, '2021-03-12 09:33:04'),
(6, 1, 1, '2131', '123', '123', '123', 1, NULL, '2021-03-12 09:36:42', NULL, '2021-03-12 09:36:42'),
(7, 6, 1, 'asdf', 'asdf', '23423423', 'asfasdfasdf', 1, 1, '2021-04-20 11:12:56', NULL, '2021-04-20 11:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `staff_change_status_details`
--

CREATE TABLE `staff_change_status_details` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_change_status` int(20) DEFAULT 0,
  `from_dt` varchar(200) DEFAULT '0',
  `to_dt` varchar(200) DEFAULT '0',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_change_status_details`
--

INSERT INTO `staff_change_status_details` (`id`, `id_staff`, `id_change_status`, `from_dt`, `to_dt`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, '0', '0', '', 1, 1, '2020-12-08 08:30:47', NULL, '2020-12-08 08:30:47'),
(2, 2, 5, '0', '0', '', 1, 1, '2020-12-08 08:30:58', NULL, '2020-12-08 08:30:58'),
(3, 3, 7, '2020-12-11', '2020-12-11', 'wertyuytrew', 1, 1, '2020-12-10 20:06:12', NULL, '2020-12-10 20:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `staff_status`
--

CREATE TABLE `staff_status` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_status`
--

INSERT INTO `staff_status` (`id`, `code`, `name`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Active', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(2, '', 'Inactive', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(3, '', 'Terminated', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(4, '', 'Suspended', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(5, '', 'Deceased', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(6, '', 'Quit', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(7, '', 'Sabbatical Leave', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(8, '', 'Long MC', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37'),
(9, '', 'Maternity', '', 1, 1, '2020-12-08 06:10:37', NULL, '2020-12-08 06:10:37');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Karnataka', 1, 1, NULL, NULL, NULL, '2020-07-10 23:22:53'),
(2, 'Andhra Pradesh', 1, 1, NULL, NULL, NULL, '2020-07-10 23:23:18'),
(3, 'Maharastra', 1, 1, NULL, NULL, NULL, '2020-07-10 23:23:41'),
(4, 'Tamilnadu', 1, 1, NULL, NULL, NULL, '2020-08-27 13:05:28'),
(5, 'Kerala', 1, 1, NULL, NULL, NULL, '2020-08-27 13:05:39'),
(6, 'Delhi', 1, 1, NULL, NULL, NULL, '2020-08-27 13:05:58'),
(7, 'Bihar', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:12'),
(8, 'Uttar Pradesh', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:30'),
(9, 'Punjab', 1, 1, NULL, NULL, NULL, '2020-08-27 13:06:43'),
(10, 'Pahang', 1, 0, NULL, NULL, NULL, '2020-08-27 13:06:53'),
(11, 'Terengganu', 1, 0, NULL, NULL, NULL, '2020-08-27 13:07:08'),
(12, 'Kelantan', 1, 0, NULL, NULL, NULL, '2020-08-27 13:07:20'),
(13, 'Terengganu', 1, 0, NULL, NULL, NULL, '2020-08-27 13:07:32'),
(14, 'Labuan', 1, 0, NULL, NULL, NULL, '2020-08-27 13:07:53'),
(15, 'Province 1', 129, 1, NULL, NULL, NULL, '2020-09-01 19:07:09'),
(16, 'Sri Lanka', 173, 1, NULL, NULL, NULL, '2020-09-01 19:33:57'),
(17, 'Others', 110, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(18, 'Others', 2, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(19, 'Others', 3, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(20, 'Others', 4, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(21, 'Others', 5, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(22, 'Others', 6, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(23, 'Others', 7, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(24, 'Others', 8, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(25, 'Others', 9, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(26, 'Others', 10, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(27, 'Others', 11, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(28, 'Others', 12, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(29, 'Others', 13, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(30, 'Others', 14, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(31, 'Others', 15, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(32, 'Others', 16, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(33, 'Others', 17, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(34, 'Others', 18, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(35, 'Others', 19, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(36, 'Others', 20, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(37, 'Others', 21, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(38, 'Others', 22, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(39, 'Others', 23, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(40, 'Others', 24, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(41, 'Others', 25, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(42, 'Others', 26, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(43, 'Others', 27, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(44, 'Others', 28, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(45, 'Others', 29, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(46, 'Others', 30, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(47, 'Others', 31, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(48, 'Others', 32, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(49, 'Others', 33, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(50, 'Others', 34, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(51, 'Others', 35, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(52, 'Others', 36, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(53, 'Others', 37, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(54, 'Others', 38, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(55, 'Others', 39, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(56, 'Others', 40, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(57, 'Others', 41, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(58, 'Others', 42, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(59, 'Others', 43, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(60, 'Others', 44, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(61, 'Others', 45, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(62, 'Others', 46, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(63, 'Others', 47, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(64, 'Others', 48, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(65, 'Others', 49, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(66, 'Others', 50, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(67, 'Others', 51, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(68, 'Others', 52, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(69, 'Others', 53, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(70, 'Others', 54, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(71, 'Others', 55, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(72, 'Others', 56, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(73, 'Others', 57, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(74, 'Others', 58, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(75, 'Others', 59, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(76, 'Others', 60, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(77, 'Others', 61, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(78, 'Others', 62, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(79, 'Others', 63, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(80, 'Others', 64, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(81, 'Others', 65, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(82, 'Others', 66, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(83, 'Others', 67, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(84, 'Others', 68, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(85, 'Others', 69, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(86, 'Others', 70, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(87, 'Others', 71, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(88, 'Others', 72, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(89, 'Others', 73, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(90, 'Others', 74, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(91, 'Others', 75, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(92, 'Others', 76, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(93, 'Others', 77, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(94, 'Others', 78, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(95, 'Others', 79, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(96, 'Others', 80, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(97, 'Others', 81, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(98, 'Others', 82, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(99, 'Others', 83, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(100, 'Others', 84, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(101, 'Others', 85, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(102, 'Others', 86, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(103, 'Others', 87, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(104, 'Others', 88, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(105, 'Others', 89, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(106, 'Others', 90, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(107, 'Others', 91, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(108, 'Others', 92, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(109, 'Others', 93, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(110, 'Others', 94, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(111, 'Others', 95, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(112, 'Others', 96, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(113, 'Others', 97, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(114, 'Others', 98, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(115, 'Others', 99, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(116, 'Others', 100, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(117, 'Others', 101, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(118, 'Others', 102, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(119, 'Others', 103, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(120, 'Others', 104, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(121, 'Others', 105, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(122, 'Others', 106, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(123, 'Others', 107, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(124, 'Others', 108, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(125, 'Others', 109, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(126, 'Others', 1, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(127, 'Others', 111, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(128, 'Others', 112, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(129, 'Others', 113, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(130, 'Others', 114, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(131, 'Others', 115, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(132, 'Others', 116, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(133, 'Others', 117, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(134, 'Others', 118, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(135, 'Others', 119, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(136, 'Others', 120, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(137, 'Others', 121, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(138, 'Others', 122, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(139, 'Others', 123, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(140, 'Others', 124, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(141, 'Others', 125, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(142, 'Others', 126, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(143, 'Others', 127, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(144, 'Others', 128, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(145, 'Others', 129, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(146, 'Others', 130, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(147, 'Others', 131, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(148, 'Others', 132, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(149, 'Others', 133, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(150, 'Others', 134, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(151, 'Others', 135, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(152, 'Others', 136, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(153, 'Others', 137, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(154, 'Others', 138, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(155, 'Others', 139, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(156, 'Others', 140, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(157, 'Others', 141, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(158, 'Others', 142, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(159, 'Others', 143, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(160, 'Others', 144, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(161, 'Others', 145, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(162, 'Others', 146, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(163, 'Others', 147, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(164, 'Others', 148, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(165, 'Others', 149, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(166, 'Others', 150, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(167, 'Others', 151, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(168, 'Others', 152, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(169, 'Others', 153, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(170, 'Others', 154, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(171, 'Others', 155, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(172, 'Others', 156, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(173, 'Others', 157, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(174, 'Others', 158, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(175, 'Others', 159, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(176, 'Others', 160, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(177, 'Others', 161, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(178, 'Others', 162, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(179, 'Others', 163, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(180, 'Others', 164, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(181, 'Others', 165, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(182, 'Others', 166, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(183, 'Others', 167, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(184, 'Others', 168, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(185, 'Others', 169, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(186, 'Others', 170, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(187, 'Others', 171, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(188, 'Others', 172, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(189, 'Others', 173, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(190, 'Others', 174, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(191, 'Others', 175, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(192, 'Others', 176, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(193, 'Others', 177, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(194, 'Others', 178, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(195, 'Others', 179, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(196, 'Others', 180, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(197, 'Others', 181, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(198, 'Others', 182, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(199, 'Others', 183, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(200, 'Others', 184, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(201, 'Others', 185, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(202, 'Others', 186, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(203, 'Others', 187, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(204, 'Others', 188, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(205, 'Others', 189, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(206, 'Others', 190, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(207, 'Others', 191, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(208, 'Others', 192, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(209, 'Others', 193, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(210, 'Others', 194, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(211, 'Others', 195, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(212, 'Others', 196, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(213, 'Others', 197, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(214, 'Others', 198, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(215, 'Others', 199, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(216, 'Others', 200, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(217, 'Others', 201, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(218, 'Others', 202, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(219, 'Others', 203, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(220, 'Others', 204, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(221, 'Others', 205, 1, NULL, NULL, NULL, '2020-12-08 00:38:42'),
(222, 'Others', 206, 1, NULL, NULL, NULL, '2020-12-08 00:38:42');

-- --------------------------------------------------------

--
-- Table structure for table `status_table`
--

CREATE TABLE `status_table` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `type` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_table`
--

INSERT INTO `status_table` (`id`, `name`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Approved', 'Company', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(2, 'Reject', 'Company', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(3, 'Active', 'PartnerUniversity', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(4, 'Reject', 'PartnerUniversity', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(5, 'Active', 'Programme', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(6, 'Reject', 'Programme', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(7, 'Approved', 'PartnerUniversity', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(8, 'Pending', 'PartnerUniversity', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(9, 'Inactive', 'Programme', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22'),
(10, 'Draft', 'Programme', 1, 1, '2021-03-15 00:11:22', NULL, '2021-03-15 00:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `student_last_login`
--

CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_last_login`
--

INSERT INTO `student_last_login` (`id`, `id_student`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 3, '{\"student_name\":\"Mr. Mohd Khairul\",\"email_id\":\"askiran123+3@gmail.com\",\"nric\":\"23424242324\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-11 12:56:50'),
(2, 4, '{\"student_name\":\"Mr. Bhasha Zahir\",\"email_id\":\"bz@cms.com\",\"nric\":\"NR33445\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-11 13:15:46'),
(3, 4, '{\"student_name\":\"Mr. Bhasha Zahir\",\"email_id\":\"bz@cms.com\",\"nric\":\"NR33445\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-19 17:29:29'),
(4, 6, '{\"student_name\":\"Mr. Nabasha Ali\",\"email_id\":\"nab@cms.com\",\"nric\":\"5612345\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-10 17:49:31'),
(5, 6, '{\"student_name\":\"Mr. Nabasha Ali\",\"email_id\":\"nab@cms.com\",\"nric\":\"5612345\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '211.25.82.226', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '2020-08-10 20:03:00'),
(6, 17, '{\"student_name\":\"AG. Vasanthi Krishnan\",\"email_id\":\"vask@cms.com\",\"nric\":\"78031098778\",\"id_intake\":\"1\",\"id_program\":\"4\",\"id_qualification\":\"1\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-27 19:25:21'),
(7, 17, '{\"student_name\":\"AG. Vasanthi Krishnan\",\"email_id\":\"vask@cms.com\",\"nric\":\"78031098778\",\"id_intake\":\"1\",\"id_program\":\"4\",\"id_qualification\":\"1\"}', '42.190.31.233', 'Chrome 84.0.4147.135', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'Windows 10', '2020-08-27 20:10:26'),
(8, 13, '{\"student_name\":\"DATIN. Testing Two\",\"email_id\":\"t2@cms.com\",\"nric\":\"t2\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_qualification\":\"1\",\"id_program_scheme\":\"8\"}', '112.79.85.46', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-30 14:02:58'),
(9, 16, '{\"student_name\":\"AG. Jury Tise\",\"email_id\":\"jt@cms.com\",\"nric\":\"NTJT88990\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_qualification\":\"1\",\"id_program_scheme\":\"11\"}', '157.45.63.59', 'Chrome 81.0.4044.111', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.111 Safari/537.36', 'Linux', '2020-09-02 19:20:32'),
(10, 14, '{\"student_name\":\"DATIN. Testing Four\",\"email_id\":\"t4@cms.com\",\"nric\":\"t4\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_qualification\":\"1\",\"id_program_scheme\":\"8\",\"isStudentAdminLoggedIn\":true}', '157.45.44.99', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-08 06:34:32'),
(11, 18, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"isStudentAdminLoggedIn\":true}', '157.49.189.60', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-10 14:36:57'),
(12, 18, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"isStudentAdminLoggedIn\":true}', '157.49.189.60', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-10 14:40:04'),
(13, 23, '{\"student_name\":\"DATIN. Student Eight\",\"email_id\":\"s8@cms.com\",\"nric\":\"TIQWE123\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.161.86', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-09-12 13:14:25'),
(14, 24, '{\"student_name\":\"DATIN. Sadhullah Beeg\",\"email_id\":\"sb@cms.com\",\"nric\":\"9074567090\",\"id_intake\":\"1\",\"id_program\":\"4\",\"id_program_landscape\":\"18\",\"id_qualification\":\"7\",\"id_program_scheme\":\"15\",\"student_education_level\":\"Master\",\"isStudentAdminLoggedIn\":true}', '42.190.19.126', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Windows 10', '2020-10-07 21:46:14'),
(15, 27, '{\"student_name\":\"DATIN. Ragina Federar\",\"email_id\":\"rf@cms.com\",\"nric\":\"119088\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"7\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Master\"}', '157.49.126.255', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-08 22:15:21'),
(16, 27, '{\"student_name\":\"DATIN. Ragina Federar\",\"email_id\":\"rf@cms.com\",\"nric\":\"119088\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"7\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Master\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-10-08 22:32:33'),
(17, 28, '{\"student_name\":\"AG. Ragina Federer\",\"email_id\":\"rf@cms.com\",\"nric\":\"NF888\",\"id_intake\":\"4\",\"id_program\":\"3\",\"id_program_landscape\":\"11\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.64.231', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-17 21:30:38'),
(18, 29, '{\"student_name\":\"DATINDR. Naani Haris\",\"email_id\":\"nh@cms.com\",\"nric\":\"SADJAKKA7878\",\"id_intake\":\"4\",\"id_program\":\"4\",\"id_program_landscape\":\"17\",\"id_qualification\":\"7\",\"id_program_scheme\":\"16\",\"student_education_level\":\"Master\"}', '157.49.75.119', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-18 13:07:46'),
(19, 30, '{\"student_name\":\"DATINDR. Khaleel Ahmed\",\"email_id\":\"ah@cms.com\",\"nric\":\"SA6863821\",\"id_intake\":\"4\",\"id_program\":\"3\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"17\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.185.42', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-10-27 07:32:16'),
(20, 30, '{\"student_name\":\"DATINDR. Khaleel Ahmed\",\"email_id\":\"ah@cms.com\",\"nric\":\"SA6863821\",\"id_intake\":\"4\",\"id_program\":\"3\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"17\",\"student_education_level\":\"POSTGRADUATE\"}', '157.49.94.176', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-07 12:41:43'),
(21, 31, '{\"student_name\":\"MR. Student One\",\"email_id\":\"s1@cms.com\",\"nric\":\"NR000001\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '117.230.141.80', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-08 11:50:46'),
(22, 32, '{\"student_name\":\"MDM. Student  Five\",\"email_id\":\"s5@cms.com\",\"nric\":\"NR05\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"22\",\"id_qualification\":\"1\",\"id_program_scheme\":\"22\",\"student_education_level\":\"POSTGRADUATE\"}', '117.230.12.209', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '2020-11-09 12:14:33'),
(23, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\",\"isStudentAdminLoggedIn\":true}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-11-10 19:58:50'),
(24, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-11-30 00:13:03'),
(25, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-11-30 03:09:26'),
(26, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-11-30 07:21:03'),
(27, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '2020-11-30 18:30:22'),
(28, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.190.11.118', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Windows 10', '2020-12-02 23:20:23'),
(29, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-07 22:14:14'),
(30, 35, '{\"student_name\":\"AG. aeutesting2 2\",\"email_id\":\"aeutesting2@gmail.com\",\"nric\":\"7373737337377\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.190.11.118', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-07 22:16:51'),
(31, 40, '{\"student_name\":\"HAJI. CC C\",\"email_id\":\"aeudemo0803@gmail.com\",\"nric\":\"0987\",\"id_intake\":\"1\",\"id_program\":\"7\",\"id_program_landscape\":\"24\",\"id_qualification\":\"7\",\"id_program_scheme\":\"24\",\"student_education_level\":\"Master\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-08 09:10:10'),
(32, 42, '{\"student_name\":\"MR. Kiran  testing\",\"email_id\":\"11@gmail.com\",\"nric\":\"1111122332111\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-12-09 02:30:10'),
(33, 44, '{\"student_name\":\"DRHAJI. DD D\",\"email_id\":\"aeudemo1001@gmail.com\",\"nric\":\"3456\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-09 21:02:42'),
(34, 31, '{\"student_name\":\"MR. Student One\",\"email_id\":\"s1@cms.com\",\"nric\":\"NR000001\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '42.105.116.181', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-10 22:26:18'),
(35, 45, '{\"student_name\":\"MR. Developper Testing5\",\"email_id\":\"dt5@cms.com\",\"nric\":\"DTNRIC05\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 02:51:46'),
(36, 33, '{\"student_name\":\"AG. Student Four\",\"email_id\":\"s4@cms.com\",\"nric\":\"NR04\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 13:04:55'),
(37, 33, '{\"student_name\":\"AG. Student Four\",\"email_id\":\"s4@cms.com\",\"nric\":\"NR04\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"21\",\"id_qualification\":\"1\",\"id_program_scheme\":\"21\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-11 20:37:07'),
(38, 46, '{\"student_name\":\"MR. Student 6\",\"email_id\":\"s6@cms.com\",\"nric\":\"NRICS8006\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '42.105.117.54', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-12 10:51:51'),
(39, 49, '{\"student_name\":\"DR. Student 8\",\"email_id\":\"s8@cms.com\",\"nric\":\"NRICS8008\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"0\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '42.105.117.179', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-12 12:01:27'),
(40, 51, '{\"student_name\":\"MR. Developper Testing 6\",\"email_id\":\"dt6@cms.com\",\"nric\":\"NRICS8006\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"26\",\"student_education_level\":\"POSTGRADUATE\"}', '157.45.79.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-15 10:43:17'),
(41, 51, '{\"student_name\":\"MR. Developper Testing 6\",\"email_id\":\"dt6@cms.com\",\"nric\":\"NRICS8006\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.167.117', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-16 12:03:38'),
(42, 54, '{\"student_name\":\"DR. Developper Testing 7\",\"email_id\":\"dt7@cms.com\",\"nric\":\"NRICS8007\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '42.105.116.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-18 13:12:59'),
(43, 55, '{\"student_name\":\"HAJI. Developper Testing 8\",\"email_id\":\"dt8@cms.com\",\"nric\":\"NRICS8008\",\"id_intake\":\"1\",\"id_program\":\"8\",\"id_program_landscape\":\"28\",\"id_qualification\":\"1\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '42.105.116.144', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-18 13:21:07'),
(44, 62, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '2020-12-23 08:59:53'),
(45, 63, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '157.45.71.122', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-23 09:15:28'),
(46, 63, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '117.230.51.163', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 03:13:06'),
(47, 61, '{\"student_name\":\"MR. AA 01\",\"email_id\":\"aa01@gmail.com\",\"nric\":\"01\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '117.230.154.46', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 03:20:24'),
(48, 63, '{\"student_name\":\"MRS. AA 02\",\"email_id\":\"aa02@gmail.com\",\"nric\":\"2\",\"id_intake\":\"1\",\"id_program\":\"9\",\"id_program_landscape\":\"33\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"UNDERGRADUATE\",\"student_semester\":\"1\"}', '42.105.125.15', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 08:16:14'),
(49, 64, '{\"student_name\":\"EN. Developper Testing 17\",\"email_id\":\"dt17@cms.com\",\"nric\":\"NRIC8017\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '42.105.125.15', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-24 08:32:19'),
(50, 71, '{\"student_name\":\"DR. PHD Testing 1\",\"email_id\":\"phdt1@cms.com\",\"nric\":\"NRICS88001\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.8.126', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-25 12:06:48'),
(51, 72, '{\"student_name\":\"HE. PHD Testing 2\",\"email_id\":\"phdt2@cms.com\",\"nric\":\"NRICSPHDT8002\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.8.126', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-12-25 12:07:55'),
(52, 73, '{\"student_name\":\"MS. pHD Testing 3\",\"email_id\":\"phdt3@cms.com\",\"nric\":\"NRICSPHD8003\",\"id_intake\":\"1\",\"id_program\":\"6\",\"id_program_landscape\":\"29\",\"id_qualification\":\"1\",\"id_program_scheme\":\"2\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.8.126', 'Firefox 84.0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0', 'Linux', '2020-12-25 12:14:44'),
(53, 40, '{\"student_name\":\"HAJI. CC C\",\"email_id\":\"aeudemo0803@gmail.com\",\"nric\":\"0987\",\"id_intake\":\"1\",\"id_program\":\"7\",\"id_program_landscape\":\"24\",\"id_qualification\":\"7\",\"id_program_scheme\":\"1\",\"student_education_level\":\"MASTER\",\"student_semester\":\"1\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-10 22:54:14'),
(54, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '157.45.180.227', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2021-01-13 03:25:53'),
(55, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"PHD\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-13 23:26:54'),
(56, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"POSTGRADUATE\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-14 19:32:48'),
(57, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"email_id\":\"dt5@eag.com\",\"nric\":\"80005\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\"}', '117.230.21.197', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-15 11:06:43'),
(58, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"POSTGRADUATE\",\"student_profile_pic\":\"default_profile.jpg\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 01:08:33'),
(59, 74, '{\"student_name\":\"Mr. Developper Testing One\",\"email_id\":\"dt1@eag.com\",\"nric\":\"80001\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"29\",\"student_education_level\":\"POSTGRADUATE\",\"student_profile_pic\":\"default_profile.jpg\",\"isStudentAdminLoggedIn\":true}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-17 19:41:14'),
(60, 79, '{\"student_name\":\"Mr. MM 01\",\"email_id\":\"mm01@g.com\",\"nric\":\"LM05789\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"3\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"93f27d23afd21ee4f7a5d6a8b8794984.jpg\"}', '157.45.91.198', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-18 11:32:02'),
(61, 82, '{\"student_name\":\"Mr. Developper Testing Seven\",\"email_id\":\"dt7@eag.com\",\"nric\":\"80007\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"560785ed38e2bbab173b37bd35a9ab47.png\"}', '157.45.73.8', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 04:10:08'),
(62, 80, '{\"student_name\":\"Ms. PP 01\",\"email_id\":\"pp01@g.com\",\"nric\":\"7198\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"0eb157a070a376642ef4b14b8ee24769.jpg\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-19 07:48:18'),
(63, 82, '{\"student_name\":\"Mr. Developper Testing Seven\",\"email_id\":\"dt7@eag.com\",\"nric\":\"80007\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"560785ed38e2bbab173b37bd35a9ab47.png\"}', '157.45.67.229', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 08:50:28'),
(64, 82, '{\"student_name\":\"Mr. Developper Testing Seven\",\"email_id\":\"dt7@eag.com\",\"nric\":\"80007\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"560785ed38e2bbab173b37bd35a9ab47.png\"}', '49.206.15.129', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-01-19 08:52:49'),
(65, 82, '{\"student_name\":\"Mr. Developper Testing Seven\",\"email_id\":\"dt7@eag.com\",\"nric\":\"80007\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"560785ed38e2bbab173b37bd35a9ab47.png\"}', '157.45.67.229', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 08:53:56'),
(66, 83, '{\"student_name\":\"Mr. Developper Testing Eight\",\"email_id\":\"dt8@eag.com\",\"nric\":\"80008\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"default_profile.jpg\"}', '157.45.67.229', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 09:16:08'),
(67, 85, '{\"student_name\":\"Mr. Developper Testing Nine\",\"email_id\":\"dt9@eag.com\",\"nric\":\"80009\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"3\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"default_profile.jpg\"}', '157.45.186.137', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 10:06:26'),
(68, 84, '{\"student_name\":\"Mr. Shoaib Akhtar\",\"email_id\":\"akhtar@gmail.com\",\"nric\":\"PA012345\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"881ae639105d849b5386856468724871.jpg\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-19 10:09:23'),
(69, 86, '{\"student_name\":\"Mr. Developper Testing Ten\",\"email_id\":\"dt10@cms.com\",\"nric\":\"80010\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"3\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"default_profile.jpg\"}', '157.45.186.137', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '2021-01-19 10:21:06'),
(70, 89, '{\"student_name\":\"Ms. Zara Lee\",\"email_id\":\"zara@gmail.com\",\"nric\":\"PA753654\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"3\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"aae657f411ff24af8a47eceda68579a8.jpg\"}', '49.206.15.129', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-19 11:36:54'),
(71, 84, '{\"student_name\":\"Mr. Shoaib Akhtar\",\"email_id\":\"akhtar@gmail.com\",\"nric\":\"PA012345\",\"id_intake\":\"3\",\"id_program\":\"10\",\"id_program_landscape\":\"34\",\"id_qualification\":\"2\",\"id_program_scheme\":\"1\",\"student_education_level\":\"POSTGRADUATE\",\"student_semester\":\"1\",\"student_profile_pic\":\"881ae639105d849b5386856468724871.jpg\"}', '42.190.11.118', 'Chrome 87.0.4280.141', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36', 'Windows 10', '2021-01-19 16:48:29'),
(72, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"student_first_name\":\"Developper\",\"student_last_name\":\"Testing Five\",\"student_email\":null,\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-26 10:18:19'),
(73, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"student_first_name\":\"Developper\",\"student_last_name\":\"Testing Five\",\"student_email\":null,\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-26 10:18:57'),
(74, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"student_first_name\":\"Developper\",\"student_last_name\":\"Testing Five\",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-26 10:20:53'),
(75, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"student_first_name\":\"Developper\",\"student_last_name\":\"Testing Five\",\"student_email\":null,\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-26 10:37:40'),
(76, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"student_first_name\":\"Developper\",\"student_last_name\":\"Testing Five\",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-02-26 11:01:39'),
(77, 78, '{\"student_name\":\"Mr. Developper Testing Five\",\"student_first_name\":\"Developper\",\"student_last_name\":\"Testing Five\",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2021-03-01 17:32:22'),
(78, 102, '{\"student_name\":\"poi\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"poi@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 00:17:57'),
(79, 103, '{\"student_name\":\"testing\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"test1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 00:27:16'),
(80, 103, '{\"student_name\":\"testing\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"test1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 00:28:46'),
(81, 104, '{\"student_name\":\"student2\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"student2@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 00:56:54'),
(82, 104, '{\"student_name\":\"student2\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"student2@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 00:59:31'),
(83, 105, '{\"student_name\":\"student3\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"student3@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 03:45:01'),
(84, 106, '{\"student_name\":\"kiran\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"kiran@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-03 03:52:52'),
(85, 107, '{\"student_name\":\"desginer\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"designer@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-06 01:20:47'),
(86, 107, '{\"student_name\":\"desginer\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"designer@gmail.com\",\"studentLoggedIn\":true}', '1.39.166.210', 'Chrome 88.0.4324.192', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36', 'Mac OS X', '2021-03-06 01:33:38'),
(87, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-10 09:53:27'),
(88, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student one\",\"student_last_name\":\"Student \",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-10 10:11:56'),
(89, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student one\",\"student_last_name\":\"Student \",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-10 17:28:53'),
(90, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student one\",\"student_last_name\":\"Student \",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '42.190.18.147', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '2021-03-10 18:28:05'),
(91, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student one\",\"student_last_name\":\"Student \",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-16 12:00:41'),
(92, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student one\",\"student_last_name\":\"Student \",\"student_email\":\"student1@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-22 19:32:15'),
(93, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-22 19:36:03'),
(94, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '42.190.18.147', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-22 23:55:01'),
(95, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-22 23:56:07'),
(96, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:39:41'),
(97, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:41:55'),
(98, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:44:03'),
(99, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:45:01'),
(100, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:51:35'),
(101, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:54:16'),
(102, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 07:54:41'),
(103, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"Chen\",\"student_last_name\":\"Lee\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 08:02:21'),
(104, 118, '{\"student_name\":\"chen lee\",\"student_first_name\":\"Chen\",\"student_last_name\":\"Lee\",\"student_email\":\"chen@g.com\",\"studentLoggedIn\":true}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '2021-03-23 08:03:46'),
(105, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '106.206.60.221', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-05 09:39:15'),
(106, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '106.206.4.140', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-05 11:09:44'),
(107, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '42.190.14.23', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '2021-04-05 19:05:54'),
(108, 128, '{\"student_name\":\"kiran1\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"kiran1@gmail.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 09:05:25'),
(109, 128, '{\"student_name\":\"kiran1\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"kiran1@gmail.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 09:08:10'),
(110, 128, '{\"student_name\":\"kiran1\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"kiran1@gmail.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 09:36:00'),
(111, 129, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"t@a.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 09:44:59'),
(112, 129, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"t@a.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 09:47:34'),
(113, 131, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"t@a.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 09:58:13'),
(114, 132, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"t@a.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 10:00:54'),
(115, 108, '{\"student_name\":\"student1\",\"student_first_name\":\"Student\",\"student_last_name\":\"1\",\"student_email\":\"student12@gmail.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 10:14:03'),
(116, 132, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"t@a.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 10:14:40');
INSERT INTO `student_last_login` (`id`, `id_student`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(117, 132, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"t@a.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 10:29:41'),
(118, 132, '{\"student_name\":\"testingdemo\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"kiran@gmail.com\",\"studentLoggedIn\":true}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-04-07 18:46:54'),
(119, 137, '{\"student_name\":\"hazna ahmad\",\"student_first_name\":\"\",\"student_last_name\":\"\",\"student_email\":\"hazzna@gmail.com\",\"studentLoggedIn\":true}', '211.25.82.226', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36 Edg/89.0.774.75', 'Windows 10', '2021-04-13 19:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `student_note`
--

CREATE TABLE `student_note` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `note` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_note`
--

INSERT INTO `student_note` (`id`, `id_student`, `note`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 6, 'Fee Paymet Alert Need To Add', NULL, 1, '2020-08-10 13:49:18', NULL, '2020-08-10 13:49:18'),
(3, 4, 'Fee Paymet Alert Need To Add', NULL, 1, '2020-08-10 13:49:18', NULL, '2020-08-10 13:49:18'),
(4, 7, 'Payment Added & Migration As Student Done', NULL, 1, '2020-08-10 14:28:03', NULL, '2020-08-10 14:28:03');

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `percentage` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_assembly_distribution_details`
--

CREATE TABLE `temp_assembly_distribution_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(256) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `received_quantity` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_cart`
--

CREATE TABLE `temp_cart` (
  `id` int(20) NOT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `amount` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_cart`
--

INSERT INTO `temp_cart` (`id`, `id_programme`, `id_session`, `created_date`, `amount`) VALUES
(1, 1, 'smactu27j252g52b49m8ta70739d85p9', '2021-02-26 13:47:00', '100'),
(2, 2, 'smactu27j252g52b49m8ta70739d85p9', '2021-02-26 13:48:46', '100'),
(3, 14, '04170187325a12183ea710dafa14c8ad05b91218', '2021-02-26 10:15:53', '100'),
(4, 14, '6be32e77086ce3a718180346f5e042f62115e310', '2021-02-26 17:30:15', '250'),
(5, 14, '133cbcc27d461b9c733e5e1e043bb99c68ad2830', '2021-03-03 00:17:39', '250'),
(6, 14, '05d963417ebfb8105209e36424e2cb10bd5a510a', '2021-03-03 03:44:39', '250'),
(7, 14, 'a7cf651a6eea4248cc3e54af227d4446679ec444', '2021-03-04 17:00:34', '250'),
(8, 14, 'd9cbed052e51016135d01a35d71cb2451bbb7f4f', '2021-03-06 01:20:30', '250'),
(9, 14, '4938be7ea18b5fe9022f22bbe476b525f634b3a4', '2021-03-07 18:42:47', '250'),
(10, 15, 'b21992a06e96e0fdbec6360a6c276c865e43a1e0', '2021-03-08 04:59:16', '0'),
(11, 14, '53d8dce2fa1dd96d0eb3e57d7d7495981357e127', '2021-03-08 12:10:58', '250'),
(13, 15, 'ef6df5c83f23193d4af9971a82d8980eae2adf9d', '2021-03-10 08:00:06', '0'),
(14, 16, '6b72ed9b7ab3a949895a141422a5a775070bfc46', '2021-03-10 09:53:11', '149'),
(15, 14, 'c12d876565782aec4c8ab16a4fc5b101dbc407f8', '2021-03-10 10:16:24', '149'),
(17, 19, '08ceedd736c08e0ba96c6455677ece05d3ddf6c9', '2021-03-10 12:11:47', '149'),
(18, 18, '263da0e49c7e057fadf34fc2a55a20fd01579888', '2021-03-10 18:27:13', '149'),
(19, 15, '713f7d057dc4ba6366fd900ec930b1d0c54cc419', '2021-03-22 10:37:55', '149'),
(23, 15, 'e9be373c6a73aae43b314b3f042444cbd960599b', '2021-03-23 07:51:20', '149'),
(24, 25, 'e9be373c6a73aae43b314b3f042444cbd960599b', '2021-03-23 07:54:06', '700'),
(25, 28, '098b78036b737136797ae2d2274b6544c1fdbf8d', '2021-04-05 20:15:39', 'undefined'),
(26, 14, '253106389de1a02e4b63967d6989df46bdf78d8e', '2021-04-05 22:43:27', '149'),
(27, 26, '5368a143110cf96fd13ddeff51391a743964d361', '2021-04-06 21:04:36', '745'),
(28, 30, 'ccfea9f0e958b926dad34328d0466d17c790dda9', '2021-04-07 09:04:57', 'undefined'),
(29, 30, '0b3688f86fc8bb3b66e359193538e600dd61f841', '2021-04-07 09:35:48', 'undefined'),
(31, 17, '2e50d8f195f314434bc98dcce8d4bfcf331361b2', '2021-04-08 07:42:04', '149'),
(32, 26, 'a89396cda861de399ec23dd1e0b23d8760ee6d4a', '2021-04-10 10:28:39', '745'),
(33, 30, 'a89396cda861de399ec23dd1e0b23d8760ee6d4a', '2021-04-10 11:19:03', 'undefined'),
(34, 17, '0d1220b25f0d2795c304ea6166ace4f7b8b9939c', '2021-04-13 19:51:19', '149'),
(35, 16, '0d1220b25f0d2795c304ea6166ace4f7b8b9939c', '2021-04-13 19:54:50', '149'),
(36, 24, '318340edb580bc03b077497a1b645a26fdb0bb0b', '2021-04-14 17:17:31', '149'),
(37, 17, '5a2b780ba2511f9c4e4d4a4fa5c731a612595409', '2021-04-14 18:01:43', '149'),
(38, 14, '01b566d6d10121e88384e45bc91e2496e3622e14', '2021-04-19 17:43:16', '149'),
(39, 28, '6734928525ba9fb1d1f8dadb1ed4230b2747bed9', '2021-04-19 21:45:01', 'undefined'),
(40, 26, '6734928525ba9fb1d1f8dadb1ed4230b2747bed9', '2021-04-19 21:45:28', '745'),
(41, 25, '3594c5b898059aadeeab55f5a2143dbb7da10ece', '2021-05-05 08:08:18', '700'),
(42, 15, '3594c5b898059aadeeab55f5a2143dbb7da10ece', '2021-05-05 08:09:00', '159'),
(43, 22, '3594c5b898059aadeeab55f5a2143dbb7da10ece', '2021-05-05 08:15:29', '149'),
(44, 25, 'da505e36712002ee4219904467a4e507dc083919', '2021-05-07 01:27:07', '700');

-- --------------------------------------------------------

--
-- Table structure for table `temp_purchase_order_details`
--

CREATE TABLE `temp_purchase_order_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(256) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_sub_category` int(20) DEFAULT 0,
  `id_item` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `balance_quantity` int(20) DEFAULT 0,
  `received_quantity` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT 0.00,
  `total_price` float(20,2) DEFAULT 0.00,
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `id_role` int(20) DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `image` varchar(512) DEFAULT '',
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `id_role`, `is_deleted`, `image`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'admin@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Administrator', '9890098901', 1, 1, 0, '83d3d9b5dd020af2bf98a12880628bef.jpeg', 0, '2015-07-01 18:56:49', 1, '2020-01-30 01:48:49'),
(17, 'records@gmail.com', '6e52c40bb8fc91ff39ee5c79b4211f67', 'Records Manager', '123123123', 21, 0, 0, '', 1, '2021-04-25 21:24:24', NULL, NULL),
(15, 'product.user@aeu.com', '$2y$10$sBkOeYD4I5osafa3CqHIdO3DofCEZAwhQmvhjtAylyr9ldB2nnor6', 'Product Manager', '890890890', 19, 0, 0, '', 1, '2021-04-25 19:40:49', NULL, NULL),
(14, 'finance@aeu.edu.my', '81dc9bdb52d04dc20036dbd8313ed055', 'Finance User', '12312312312', 18, 0, 0, '', 1, '2021-04-25 18:35:46', NULL, NULL),
(18, 'af@gmail.com', 'f0357a3f154bc2ffe2bff55055457068', 'Af', '123123123', 22, 0, 0, '', 1, '2021-05-04 20:03:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_last_login`
--

CREATE TABLE `user_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(11, 1, '{\"role\":\"2\",\"roleText\":\"Ex\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.159.118', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(40, 1, '{\"role\":\"1\",\"roleText\":null,\"name\":\"Mr. Virat Kohli\"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.104.95', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.98.97', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(56, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(57, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '2020-08-17 12:30:52'),
(58, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.160.222', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(59, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.10.50', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(60, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(61, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(62, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.167.29', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(63, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(64, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.165.171', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(65, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(66, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.172.50', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(67, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(68, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.167.25', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(69, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.170.70', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(70, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.171.206', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(71, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(72, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.206.63.4', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(73, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(74, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '27.59.26.2', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(75, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.237.199.118', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(76, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(77, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(78, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(79, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(80, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(81, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(82, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(83, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.157.79', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(84, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(85, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(86, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(87, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(88, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(89, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(90, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(91, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.217.109.107', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(92, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(93, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.182', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(94, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(95, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '223.186.95.105', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(96, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(97, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(98, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(99, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.172.230', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(100, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(101, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(102, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.52.112', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(103, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(104, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.8.244', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(105, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(106, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(107, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(108, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.109.211', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(109, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(110, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.18.147', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(111, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.18.147', 'Chrome 88.0.4324.190', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(112, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(113, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(114, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(115, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.196.167', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(116, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.166', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(117, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.205.174', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(118, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.166', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(119, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.201.119', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(120, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(121, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(122, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.193.89', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(123, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(124, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.211.232', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(125, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.145.115', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(126, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(127, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(128, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.124.132', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(129, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.209.249', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(130, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.233.127', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(131, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(132, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(133, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(134, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(135, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(136, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.154', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(137, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.237.160', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(138, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.18.147', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(139, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(140, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(141, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(142, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.186.122', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(143, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.164.222', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(144, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(145, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.11.105', 'Chrome 89.0.4389.90', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(146, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.254.227', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(147, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '106.206.60.221', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(148, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.138.154', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(149, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(150, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(151, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.15.40', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(152, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(153, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(154, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(155, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.80.129', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(156, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.78.149', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(157, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '157.45.78.149', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(158, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(159, 13, '{\"role\":\"17\",\"roleText\":\"Finance Administrator\",\"name\":\"Finance User\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(160, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(161, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.67.41', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(162, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.64.116', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(163, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.73.193', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(164, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(165, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.12.61', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(166, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.10.161', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(167, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.10.161', 'Chrome 89.0.4389.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(168, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(169, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(170, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(171, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.85.131', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(172, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.105.124.154', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(173, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.91.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(174, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.91.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(175, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.59.160', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(176, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.143.10', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(177, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(178, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(179, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '117.230.146.133', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(180, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.131.109', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(181, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(182, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.128.46', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(183, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00');
INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(184, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(185, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(186, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.151.145', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(187, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(188, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(189, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.159.145', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(190, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(191, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(192, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(193, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.130.146', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(194, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(195, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(196, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.131.206', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(197, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(198, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(199, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(200, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(201, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(202, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(203, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.175.71', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(204, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.85', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(205, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 90.0.4430.85', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(206, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.148.31', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(207, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(208, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.161.193', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(209, 17, '{\"role\":\"21\",\"roleText\":\"Recors Manager\",\"name\":\"Records Manager\"}', '157.45.161.193', 'Chrome 89.0.4381.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4381.0 Safari/537.36 Edg/89.0.760.0', 'Linux', '0000-00-00 00:00:00'),
(210, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.152.183', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(211, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(212, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(213, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(214, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(215, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.25.64', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(216, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.157.17', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(217, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.11.33', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(218, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(219, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.72', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(220, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(221, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.30.32', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(222, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.30.32', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(223, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.124', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(224, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.19.116', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(225, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(226, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(227, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.0.25', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(228, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(229, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(230, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(231, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(232, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 90.0.4430.85', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36 Edg/90.0.818.49', 'Windows 10', '0000-00-00 00:00:00'),
(233, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(234, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(235, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.186.88', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(236, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '157.45.185.57', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(237, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(238, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Administrator\"}', '49.206.13.94', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(239, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"\",\"name\":\"Administrator\"}', '157.45.183.133', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(240, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '157.45.183.133', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(241, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(242, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '157.45.27.195', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(243, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '157.45.140.198', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(244, 18, '{\"role\":\"22\",\"roleText\":\"Academic Facilitator\",\"user_image\":\"\",\"name\":\"Af\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(245, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"1f5595ae25bf33bf6d4b052d905aae58.jpeg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(246, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.23.255', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(247, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(248, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(249, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.15.65', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(250, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.15.65', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(251, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(252, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '117.230.11.189', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(253, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '117.230.50.141', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(254, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.4.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(255, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.4.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(256, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.4.246', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(257, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.190.14.23', 'Chrome 90.0.4430.93', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(258, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.9.103', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(259, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(260, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(261, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(262, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.28.79', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(263, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.14.121', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(264, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(265, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.5.238', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(266, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.99', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(267, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(268, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '127.0.0.1', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(269, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.225.169', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(270, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.12', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 14_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/14.0.3 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(271, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.206.10.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(272, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '157.45.237.30', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(273, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.124.3', 'Chrome 89.0.4389.105', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.105 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(274, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.124.3', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(275, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.37.162.24', 'Firefox 88.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Windows 10', '0000-00-00 00:00:00'),
(276, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '42.105.116.29', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(277, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '49.37.166.192', 'Firefox 88.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Windows 10', '0000-00-00 00:00:00'),
(278, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"5878a39730c9cb579ad3fe4ad1f3f120.svg\",\"name\":\"Administrator\"}', '1.39.138.216', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(279, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(280, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Firefox 88.0', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Linux', '0000-00-00 00:00:00'),
(281, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.37.160.66', 'Firefox 88.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Windows 10', '0000-00-00 00:00:00'),
(282, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.37.160.66', 'Firefox 88.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Windows 10', '0000-00-00 00:00:00'),
(283, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(284, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.37.160.66', 'Firefox 88.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Windows 10', '0000-00-00 00:00:00'),
(285, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '42.105.121.178', 'Chrome 91.0.4456.0', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4456.0 Safari/537.36 Edg/91.0.845.2', 'Linux', '0000-00-00 00:00:00'),
(286, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.206.6.162', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(287, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '157.45.214.224', 'Firefox 88.0', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0', 'Windows 10', '0000-00-00 00:00:00'),
(288, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"user_image\":\"83d3d9b5dd020af2bf98a12880628bef.jpeg\",\"name\":\"Administrator\"}', '49.206.2.235', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_details`
--

CREATE TABLE `vendor_details` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `city` varchar(512) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `date_of_birth` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` varchar(256) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_details`
--

INSERT INTO `vendor_details` (`id`, `name`, `code`, `email`, `phone`, `gender`, `nric`, `mobile`, `address_one`, `address_two`, `city`, `country`, `state`, `zipcode`, `date_of_birth`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Dakson Electronic', 'V00001-2021', 'Dacksonele@gmail.com', '', '', '', '09880369683', 'NEERALAKOD', '', 'Bengaluru', '1', '1', '585310', '', 1, 1, '2021-05-16 16:10:51', 1, '2021-05-22 02:05:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assembly_distribution`
--
ALTER TABLE `assembly_distribution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assembly_distribution_details`
--
ALTER TABLE `assembly_distribution_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assembly_return`
--
ALTER TABLE `assembly_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assembly_return_details`
--
ALTER TABLE `assembly_return_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assembly_team`
--
ALTER TABLE `assembly_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template_backup`
--
ALTER TABLE `communication_template_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vendor` (`id_vendor`);

--
-- Indexes for table `company_users`
--
ALTER TABLE `company_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_user_last_login`
--
ALTER TABLE `company_user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_user_role`
--
ALTER TABLE `company_user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_mode`
--
ALTER TABLE `delivery_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `department_has_staff`
--
ALTER TABLE `department_has_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disposal_type`
--
ALTER TABLE `disposal_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_year`
--
ALTER TABLE `financial_year`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frequency_mode`
--
ALTER TABLE `frequency_mode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `grn`
--
ALTER TABLE `grn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grn_details`
--
ALTER TABLE `grn_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_name`
--
ALTER TABLE `module_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation`
--
ALTER TABLE `organisation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_category`
--
ALTER TABLE `procurement_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_item_has_vendors`
--
ALTER TABLE `procurement_item_has_vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_sub_category`
--
ALTER TABLE `procurement_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_quantity`
--
ALTER TABLE `product_quantity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publish_exam_result_date`
--
ALTER TABLE `publish_exam_result_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religion_setup`
--
ALTER TABLE `religion_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `research_advisor`
--
ALTER TABLE `research_advisor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `research_advisory`
--
ALTER TABLE `research_advisory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_user_password`
--
ALTER TABLE `reset_user_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources`
--
ALTER TABLE `resources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_bank_details`
--
ALTER TABLE `staff_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_change_status_details`
--
ALTER TABLE `staff_change_status_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_status`
--
ALTER TABLE `staff_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `status_table`
--
ALTER TABLE `status_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_last_login`
--
ALTER TABLE `student_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_note`
--
ALTER TABLE `student_note`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_assembly_distribution_details`
--
ALTER TABLE `temp_assembly_distribution_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_cart`
--
ALTER TABLE `temp_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_purchase_order_details`
--
ALTER TABLE `temp_purchase_order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_last_login`
--
ALTER TABLE `user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_details`
--
ALTER TABLE `vendor_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assembly_distribution`
--
ALTER TABLE `assembly_distribution`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assembly_distribution_details`
--
ALTER TABLE `assembly_distribution_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assembly_return`
--
ALTER TABLE `assembly_return`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assembly_return_details`
--
ALTER TABLE `assembly_return_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assembly_team`
--
ALTER TABLE `assembly_team`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `communication_template_backup`
--
ALTER TABLE `communication_template_backup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company_users`
--
ALTER TABLE `company_users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `company_user_last_login`
--
ALTER TABLE `company_user_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `company_user_role`
--
ALTER TABLE `company_user_role`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `department_has_staff`
--
ALTER TABLE `department_has_staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `description`
--
ALTER TABLE `description`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `disposal_type`
--
ALTER TABLE `disposal_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `financial_year`
--
ALTER TABLE `financial_year`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frequency_mode`
--
ALTER TABLE `frequency_mode`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `grn`
--
ALTER TABLE `grn`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `grn_details`
--
ALTER TABLE `grn_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=277;

--
-- AUTO_INCREMENT for table `module_name`
--
ALTER TABLE `module_name`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organisation`
--
ALTER TABLE `organisation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=598;

--
-- AUTO_INCREMENT for table `procurement_category`
--
ALTER TABLE `procurement_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `procurement_item`
--
ALTER TABLE `procurement_item`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `procurement_item_has_vendors`
--
ALTER TABLE `procurement_item_has_vendors`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `procurement_sub_category`
--
ALTER TABLE `procurement_sub_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_quantity`
--
ALTER TABLE `product_quantity`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_type`
--
ALTER TABLE `product_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `publish_exam_result_date`
--
ALTER TABLE `publish_exam_result_date`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `religion_setup`
--
ALTER TABLE `religion_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `research_advisor`
--
ALTER TABLE `research_advisor`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `research_advisory`
--
ALTER TABLE `research_advisory`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reset_user_password`
--
ALTER TABLE `reset_user_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resources`
--
ALTER TABLE `resources`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6388;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff_bank_details`
--
ALTER TABLE `staff_bank_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `staff_change_status_details`
--
ALTER TABLE `staff_change_status_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `staff_status`
--
ALTER TABLE `staff_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;

--
-- AUTO_INCREMENT for table `status_table`
--
ALTER TABLE `status_table`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student_last_login`
--
ALTER TABLE `student_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `student_note`
--
ALTER TABLE `student_note`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_assembly_distribution_details`
--
ALTER TABLE `temp_assembly_distribution_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_cart`
--
ALTER TABLE `temp_cart`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `temp_purchase_order_details`
--
ALTER TABLE `temp_purchase_order_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_last_login`
--
ALTER TABLE `user_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=289;

--
-- AUTO_INCREMENT for table `vendor_details`
--
ALTER TABLE `vendor_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
